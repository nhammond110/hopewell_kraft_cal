static const char *rcsid = "$Id: rfListRFAdjustInventory.c 159159 2008-06-09 14:03:37Z mzais $";
/*#START***********************************************************************
 *  Copyright (c) 2004 RedPrairie Corporation. All rights reserved
 *#END************************************************************************/

#include <moca_app.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "rflib.h"
#include "applib.h"

LIBEXPORT
RETURN_STRUCT *rfListRFAdjustInventory (char *stoloc_i,
                                        char *wh_id_i,
                                        char *lodnum_i,
                                        char *subnum_i,
                                        char *dtlnum_i,
                                        char *oprcod_i,
                                        char *devcod_i,
                                        char *invid_i,
                                        char *colnam_i,
                                        char *prtnum_i,
                                        char *prt_client_id_i,
                                        char *lodlvl_i,
                                        long *lincnt_i,
                                        char *mode_i)
{
    RETURN_STRUCT *AdjRes, *CurPtr;
    mocaDataRes   *invadjres;
    mocaDataRow   *invadjrow;
    long ret_status;
    
    char list [41]; /* used for displaying combined text to RF screen. */
    
    /* The default number of columns to be displayed is 2, 
     * that means if 1st column is not long  enough 2nd column will be 
     * combined and displayed. So we add a blank line to make sure only
     * list can be displayed.
     */
    char fill_line[41]; 
    char stoloc [STOLOC_LEN+1];
    char wh_id  [WH_ID_LEN+1];
    char lodnum [LODNUM_LEN+1];
    char subnum [SUBNUM_LEN+1];
    char dtlnum [DTLNUM_LEN+1];
    char devcod [DEVCOD_LEN+1];
    char oprcod [OPRCOD_LEN+1];
    char invid  [LODNUM_LEN+1];
    char prtnum [PRTNUM_LEN+1];
    char prt_client_id [CLIENT_ID_LEN+1];
    char lodlvl [LODLVL_LEN+1];
    char invsts [INVSTS_LEN+1];
    char lotnum [LOTNUM_LEN+1];
    char orgcod [ORGCOD_LEN+1];
    char revlvl [REVLVL_LEN+1];
    char colnam [9];
    char id_where_clause[100];
    char lod_where_clause[100];
    char sub_where_clause[100];
    char dtl_where_clause[100];
    char prt_where_clause[100];
    long summary_mode=FALSE;
    long norm_flg=TRUE;
    long untqty = 0;
    long scanned_qty = 0;
    char lincnt_txt [6];
    long lincnt=0;
    long lincnt_hold=0;
    long untcas = 0;
    long untpak = 0;
    long change_lodlvl_flg = FALSE;
    double catch_qty = 0.0;
    double scanned_ctch_qty = 0.0;
    char catch_cod [CATCH_COD_LEN+1];
    long rowcnt = 0;
    long audit_lvl = 0;
    char supnum [SUPNUM_LEN+1];
    
    /* MGP Custom variable to house original or fully qualified LPN.*/
    char orig_lodnum [LODNUM_LEN+1];

	/* MGP Custom Variable for Code Date */
	char codeDate [15];
	RETURN_STRUCT *codePtr;
    mocaDataRes   *codeRes;
	mocaDataRow	  *codeRow;

    /* Initialize internal structures */

    sprintf(lincnt_txt, "%ld", lincnt);

    memset (stoloc, 0, sizeof(stoloc));
    memset (wh_id, 0, sizeof(wh_id));
    memset (lodnum, 0, sizeof(lodnum));
    memset (subnum, 0, sizeof(subnum));
    memset (dtlnum, 0, sizeof(dtlnum));
    memset (lodlvl, 0, sizeof(lodlvl));
    memset (catch_cod, 0, sizeof(catch_cod));
    memset (oprcod, 0, sizeof(oprcod));
    memset (devcod, 0, sizeof(devcod));    
    memset (list, 0, sizeof(list));
    memset (fill_line, ' ',sizeof(fill_line));
    fill_line[sizeof(fill_line)-1]=0;
    memset (id_where_clause, 0, sizeof (id_where_clause));
    memset (lod_where_clause, 0, sizeof (lod_where_clause));
    memset (sub_where_clause, 0, sizeof (sub_where_clause));
    memset (dtl_where_clause, 0, sizeof (dtl_where_clause));
    memset (prt_where_clause, 0, sizeof (prt_where_clause));
    memset (supnum, 0, sizeof(supnum));
    
    /* MGP Custom variable to house original or fully qualified LPN.*/
    memset (orig_lodnum, 0, sizeof(lodnum));

    if (mode_i && misTrimLen(mode_i, FLAG_LEN))
    {
        if(!misCiStrncmp(mode_i, "S", 1))
        {
            /* This means that we are in summary mode.
             * When a lincnt is sent in, and the inventory may be broken down
             * to a more detailed level, we could return multiple rows.
             * If we are in summary mode we will just display the same
             * information as before, but only the one row (i.e. summarized)
             */
            
            summary_mode = TRUE;
        }
    }
    
    
    /* Grab some of the input parameters. */
    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
    {
        misTrimcpy(lodnum, lodnum_i, LODNUM_LEN);
        sprintf(lod_where_clause,
                " and lodnum = '%s' ",
                lodnum);
    }

    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
    {
        misTrimcpy(subnum, subnum_i, SUBNUM_LEN);
        sprintf(sub_where_clause,
                " and subnum = '%s' ",
                subnum);
    }

    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
    {
        misTrimcpy(dtlnum, dtlnum_i, DTLNUM_LEN);
        sprintf(dtl_where_clause,
                " and dtlnum = '%s' ",
                dtlnum);
    }

    if (lodlvl_i && misTrimLen(lodlvl_i, LODNUM_LEN))
    {
        misTrimcpy(lodlvl, lodlvl_i, LODLVL_LEN);
    }

    /* Verify required arguments are present */

    if (!stoloc_i || !misTrimLen (stoloc_i, STOLOC_LEN))
    {
        return (APPMissingArg("stoloc"));
    }
    misTrimcpy (stoloc, stoloc_i, STOLOC_LEN);

    if (!wh_id_i || !misTrimLen(wh_id_i, WH_ID_LEN))
    {
        return (APPMissingArg("wh_id"));
    }
    misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

    if (!devcod_i || !misTrimLen (devcod_i, DEVCOD_LEN))
    {
        return (APPMissingArg("devcod"));
    }
    misTrimcpy (devcod, devcod_i, DEVCOD_LEN);

    if (!oprcod_i || !misTrimLen (oprcod_i, OPRCOD_LEN))
    {
        return (APPMissingArg("oprcod"));
    }
    misTrimcpy (oprcod, oprcod_i, OPRCOD_LEN);

    /* 
     * IF we were not passed in the lodnum, subnum, or dtlnum,
     * AND we have the invid with a column name...
     * THEN use that in the where clause.
     */
    if (!misTrimLen(lodnum, LODNUM_LEN) &&
        !misTrimLen(subnum, SUBNUM_LEN) &&
        !misTrimLen(dtlnum, DTLNUM_LEN) &&
        invid_i && misTrimLen(invid_i, LODNUM_LEN) &&
        colnam_i && misTrimLen(colnam_i, sizeof(colnam)))
    {
        misTrimcpy (colnam, colnam_i, sizeof(colnam));
        misTrimcpy (invid, invid_i, LODNUM_LEN);

        sprintf(id_where_clause, " and %s = '%s'", colnam, invid);
    }
   
    /* If we were passed the prtnum/prt_client_id, use it. */
    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN) &&
             prt_client_id_i && misTrimLen(prt_client_id_i, CLIENT_ID_LEN))
    {
        misTrimcpy (prtnum, prtnum_i, PRTNUM_LEN);
        misTrimcpy (prt_client_id, prt_client_id_i, CLIENT_ID_LEN);

        sprintf(prt_where_clause,
                " and prtnum = '%s' and prt_client_id = '%s'", 
                prtnum,
                prt_client_id);
 
    }
    
    if(lincnt_i != NULL && *lincnt_i != 0)
    {
        lincnt_hold = *lincnt_i;        
    }

    CurPtr = NULL;


    /*
     * Now we need to determine which command to use.  This is based
     * on the lodlvl that was passed in. The lodlvl that was passed
     * in is different from the lodlvl of the product.  This one gives
     * us an idea of what we are expected to display.
     */
    if (!misTrimStrncmp(lodlvl, LODLVL_DETAIL, LODLVL_LEN))
    {
        ret_status = srvInitiateInlineFormat(&AdjRes,
                "list rf adjust inventory details "
                " where stoloc = '%s' "
                "   and wh_id  = '%s' "
                "   and devcod = '%s' "
                "   and oprcod = '%s' "
                "%s" /* Lodnum information */
                "%s" /* subnum information */
                "%s" /* dtlnum information */
                "%s" /* Inventory Identifier information */
                "%s" /* Part / part client ID information */
                "",
                stoloc,
                wh_id,
                devcod,
                oprcod,
                lod_where_clause,
                sub_where_clause,
                dtl_where_clause,
                id_where_clause,
                prt_where_clause);
    }
    else if (!misTrimStrncmp(lodlvl, LODLVL_SUB, LODLVL_LEN))
    {
        ret_status = srvInitiateInlineFormat(&AdjRes,
                "list rf adjust inventory subloads "
                " where stoloc = '%s' "
                "   and wh_id  = '%s' "
                "   and devcod = '%s' "
                "   and oprcod = '%s' "
                "%s" /* Lodnum information */
                "%s" /* subnum information */
                "%s" /* dtlnum information */
                "%s" /* Inventory Identifier information */
                "%s" /* Part / part client ID information */
                "",
                stoloc,
                wh_id,
                devcod,
                oprcod,
                lod_where_clause,
                sub_where_clause,
                dtl_where_clause,
                id_where_clause,
                prt_where_clause);
    }
    else
    {
        ret_status = srvInitiateInlineFormat(&AdjRes,
                "list rf adjust inventory loads "
                " where stoloc = '%s' "
                "   and wh_id  = '%s' "
                "   and devcod = '%s' "
                "   and oprcod = '%s' "
                "%s" /* Lodnum information */
                "%s" /* Inventory Identifier information */
                "%s" /* Part / part client ID information */
                "",
                stoloc,
                wh_id,
                devcod,
                oprcod,
                lod_where_clause,
                sub_where_clause,
                dtl_where_clause,
                id_where_clause,
                prt_where_clause);
    }
    
    if (eOK != ret_status)
    {
        srvFreeMemory (SRVRET_STRUCT, AdjRes);
        return (srvResults(ret_status, NULL));
    }

    invadjres = srvGetResults(AdjRes);
    
    /* 
     * If we have a line count and we are in summary mode,
     * we will only look at one row as it is here.
     */
    if (lincnt_hold > 0 && summary_mode == TRUE)
    {

        for (invadjrow = sqlGetRow(invadjres), lincnt = 1;
                invadjrow && lincnt < lincnt_hold; 
                invadjrow = sqlGetNextRow(invadjrow))
        {
            lincnt++;    
        }
    }

    /* 
     * If we have a line count, we will only have one row.
     * This may need to be expanded to the next lodlvl down.
     */
    else if (lincnt_hold > 0)
    {

        for (invadjrow = sqlGetRow(invadjres), lincnt = 1;
                invadjrow && lincnt < lincnt_hold; 
                invadjrow = sqlGetNextRow(invadjrow))
        {
            lincnt++;    
        }

        if (invadjrow)
        {
            sprintf(lincnt_txt, "%ld", lincnt);

            /* grab the subnum, prtnum, invsts and untqty */
            memset(invid, 0, sizeof(invid));
            memset(lodnum, 0, sizeof(lodnum));
            memset(subnum, 0, sizeof(subnum));
            memset(dtlnum, 0, sizeof(dtlnum));
            memset(prtnum, 0, sizeof(prtnum));
            memset(prt_client_id, 0, sizeof(prt_client_id));
            memset(invsts, 0, sizeof(invsts));
            memset(lotnum, 0, sizeof(lotnum));
            memset(revlvl, 0, sizeof(revlvl));
            memset(orgcod, 0, sizeof(orgcod));
            memset(colnam, 0, sizeof(colnam));
            memset(lodlvl, 0, sizeof(lodlvl));
            memset(supnum, 0, sizeof(supnum));
            
            /* MGP custom variable to house the original or fully qualified load number.*/
            memset(orig_lodnum, 0, sizeof(lodnum));

            if (!sqlIsNull(invadjres, invadjrow, "lodnum"))
            {
                misTrimcpy(lodnum,
                           sqlGetString(invadjres, invadjrow, "lodnum"),
                           LODNUM_LEN);

                misTrimcpy(invid, lodnum, sizeof(invid)-1);
                sprintf(colnam, "lodnum");

                memset(lodlvl, 0, sizeof(lodlvl));
                misTrimcpy(lodlvl, LODLVL_LOAD, LODLVL_LEN);
            }
            
            /* MGP set the custom variable with the value that comes out of the command.*/
            misTrc(T_FLOW, "About to set the custom variable now for lincnt > 0...");
            
            if (!sqlIsNull(invadjres, invadjrow, "orig_lodnum"))
            {
            		misTrc(T_FLOW, "Inside custom if branch now for lincnt > 0...");
                misTrimcpy(orig_lodnum,
                           sqlGetString(invadjres, invadjrow, "orig_lodnum"),
                           LODNUM_LEN);

                //misTrimcpy(invid, orig_lodnum, sizeof(invid)-1);
                sprintf(colnam, "lodnum");
            }

            if (!sqlIsNull(invadjres, invadjrow, "subnum"))
            {
                misTrimcpy(subnum,
                           sqlGetString(invadjres, invadjrow, "subnum"),
                           SUBNUM_LEN);

                memset(invid, 0, sizeof(invid));
                misTrimcpy(invid, subnum, sizeof(invid)-1);
                sprintf(colnam, "subnum");

                memset(lodlvl, 0, sizeof(lodlvl));
                misTrimcpy(lodlvl, LODLVL_SUB, LODLVL_LEN);
            }
            
            if (!sqlIsNull(invadjres, invadjrow, "dtlnum"))
            {
                misTrimcpy(dtlnum,
                           sqlGetString(invadjres, invadjrow, "dtlnum"),
                           DTLNUM_LEN);

                memset(invid, 0, sizeof(invid));
                misTrimcpy(invid, dtlnum, sizeof(invid)-1);
                sprintf(colnam, "dtlnum");

                memset(lodlvl, 0, sizeof(lodlvl));
                misTrimcpy(lodlvl, LODLVL_DETAIL, LODLVL_LEN);
            }

            if (!sqlIsNull(invadjres, invadjrow, "prtnum"))
            {
                misTrimcpy(prtnum, sqlGetString(invadjres, invadjrow,"prtnum"),
                                   PRTNUM_LEN);
            }

            if (!sqlIsNull(invadjres, invadjrow, "prt_client_id"))
            {
                misTrimcpy(prt_client_id, 
                           sqlGetString(invadjres, invadjrow, "prt_client_id"),
                           CLIENT_ID_LEN);
            }

            if (!sqlIsNull(invadjres, invadjrow, "invsts"))
            {
                misTrimcpy(invsts, sqlGetString(invadjres, invadjrow, "invsts"),
                           INVSTS_LEN);
            }

            if (!sqlIsNull(invadjres, invadjrow, "lotnum"))
            {
                misTrimcpy(lotnum, sqlGetString(invadjres, invadjrow, "lotnum"),
                           LOTNUM_LEN);
            }

            if (!sqlIsNull(invadjres, invadjrow, "revlvl"))
            {
                misTrimcpy(revlvl, sqlGetString(invadjres, invadjrow, "revlvl"),
                           REVLVL_LEN);
            }

            if (!sqlIsNull(invadjres, invadjrow, "orgcod"))
            {
                misTrimcpy(orgcod, sqlGetString(invadjres, invadjrow, "orgcod"),
                           ORGCOD_LEN);
            }
            
            untcas = 0;
            if (!sqlIsNull(invadjres, invadjrow, "untcas"))
            {
                untcas = sqlGetLong(invadjres, invadjrow, "untcas");
            }
            
            untpak = 0;
            if (!sqlIsNull(invadjres, invadjrow, "untpak"))
            {
                untpak = sqlGetLong(invadjres, invadjrow, "untpak");
            }

            untqty = 0;
            if (!sqlIsNull(invadjres, invadjrow, "untqty"))
            {
                untqty = sqlGetLong(invadjres, invadjrow, "untqty");
            }

            scanned_qty = 0;
            if (!sqlIsNull(invadjres, invadjrow, "scanned_qty"))
            {
                scanned_qty = sqlGetLong(invadjres, invadjrow, "scanned_qty");
            }

            catch_qty = 0;
            if (!sqlIsNull(invadjres, invadjrow, "catch_qty"))
            {
                catch_qty = sqlGetFloat(invadjres, invadjrow, "catch_qty");
            }
            
            scanned_ctch_qty = 0;
            if (!sqlIsNull(invadjres, invadjrow, "scanned_ctch_qty"))
            {
                scanned_ctch_qty = 
                        sqlGetFloat(invadjres, invadjrow, "scanned_ctch_qty");
            }
            
            memset(catch_cod, 0, sizeof(catch_cod));
            if (!sqlIsNull(invadjres, invadjrow, "catch_cod"))
            {
                misTrimcpy(catch_cod,
                           sqlGetString(invadjres, invadjrow, "catch_cod"),
                           CATCH_COD_LEN);
            }

            if (!sqlIsNull(invadjres, invadjrow, "supnum"))
            {
                misTrimcpy(supnum, sqlGetString(invadjres, invadjrow, "supnum"),
                           SUPNUM_LEN);
            }
            
            if (!misTrimStrncmp(lodlvl, LODLVL_SUB, LODLVL_LEN))
            {
                /* 
                 * At this point we know that we have a subnum value, because
                 * that is how we determine to set the lodlvl to S.
                 */
                ret_status = srvInitiateCommandFormat(NULL,
                                "[select 1 from invdtl "
                                "  where subnum = '%s' "
                                "    and phdflg = %ld "
                                "]",
                                subnum,
                                MOCA_TRUE);

                if (ret_status == eOK)
                {
                    /*
                     * We found some invdtl record on this sub that
                     * is serialized (phdflg = 1)... so we can assume
                     * that the load level will be dtl (although we
                     * might have some product that is not serialized
                     * at the detail level on this subload.
                     */
                    change_lodlvl_flg = TRUE;
                    memset(lodlvl, 0, sizeof(lodlvl));
                    misTrimcpy(lodlvl, LODLVL_DETAIL, LODLVL_LEN);
                }
                else if (eDB_NO_ROWS_AFFECTED != ret_status)
                {
                    srvFreeMemory (SRVRET_STRUCT, AdjRes);
                    return (srvResults(ret_status, NULL));
                }
            }
            
            if (!misTrimStrncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN))
            {
                /* 
                 * We will only drop in here if the lodlvl was not determined
                 * to be a subload or detail level.
                 * 
                 * At this point we know that we have a lodnum value, because
                 * that is how we determine to set the lodlvl to L.
                 * 
                 * NOTE: From a practical standpoint we will always have
                 *       a lodnum value.
                 */
                
                /* MGP JRZ 12/7/13 Start - Changed to add a like and percent as the lodnum passed in will be the final ten digits.*/
                ret_status = srvInitiateCommandFormat(NULL,
                                "[select 1 from dual "
                                " where exists ("
                                " select 1 from invsub "
                                "  where lodnum like '%%%s' "
                                "    and phyflg = %ld) "
                                "]",
                                lodnum,
                                MOCA_TRUE);

                if (ret_status == eOK)
                {
                    /*
                     * We found some invsub record on this load that
                     * is serialized (phyflg = 1)... so we can assume
                     * that the load level will be either sub or dtl
                     * or some of both.
                     */
                    change_lodlvl_flg = TRUE;
                    memset(lodlvl, 0, sizeof(lodlvl));
                    misTrimcpy(lodlvl, LODLVL_SUB, LODLVL_LEN);
                }
                else if (eDB_NO_ROWS_AFFECTED != ret_status)
                {
                    srvFreeMemory (SRVRET_STRUCT, AdjRes);
                    return (srvResults(ret_status, NULL));
                }
            }
            srvFreeMemory(SRVRET_STRUCT, AdjRes);
            AdjRes = NULL;

            memset(sub_where_clause, 0, sizeof(dtl_where_clause));
            if(misTrimLen(subnum, SUBNUM_LEN))
            {
                sprintf(sub_where_clause, "   and subnum = '%s' ", subnum);
            }
            
            memset(dtl_where_clause, 0, sizeof(dtl_where_clause));
            if (misTrimLen(dtlnum, DTLNUM_LEN))
            {
                sprintf(dtl_where_clause, "   and dtlnum = '%s' ", dtlnum);
            }
            
            if (!misTrimStrncmp(lodlvl, LODLVL_DETAIL, LODLVL_LEN))
            {
                ret_status = srvInitiateCommandFormat(&AdjRes,
                                    "list rf adjust inventory details "
                                    " where stoloc = '%s' "
                                    "   and wh_id  = '%s' "
                                    "   and devcod = '%s' "
                                    "   and oprcod = '%s' "
                                    "   and lodnum = '%s' " 
                                    "%s" /* subnum information */
                                    "%s" /* dtlnum information */
                                    "   and prtnum = '%s' " 
                                    "   and prt_client_id = '%s' " 
                                    "   and invsts = '%s' " 
                                    "   and lotnum = '%s' " 
                                    "   and revlvl = '%s' " 
                                    "   and supnum = '%s' " 
                                    "   and orgcod = '%s' " 
                                    "",
                                    stoloc,
                                    wh_id,
                                    devcod,
                                    oprcod,
                                    lodnum,
                                    sub_where_clause,
                                    dtl_where_clause,
                                    prtnum,
                                    prt_client_id,
                                    invsts,
                                    lotnum,
                                    revlvl,
                                    supnum,
                                    orgcod);
            }
            else if (!misTrimStrncmp(lodlvl, LODLVL_SUB, LODLVL_LEN))
            {
                ret_status = srvInitiateCommandFormat(&AdjRes,
                                    "list rf adjust inventory details "
                                    " where stoloc = '%s' "
                                    "   and wh_id  = '%s' "
                                    "   and devcod = '%s' "
                                    "   and oprcod = '%s' "
                                    "   and lodnum = '%s' " 
                                    "%s" /* subnum information */
                                    "%s" /* dtlnum information */
                                    "   and prtnum = '%s' " 
                                    "   and prt_client_id = '%s' " 
                                    "   and invsts = '%s' " 
                                    "   and lotnum = '%s' " 
                                    "   and revlvl = '%s' " 
                                    "   and supnum = '%s' "
                                    "   and orgcod = '%s' " 
                                    "",
                                    stoloc,
                                    wh_id,
                                    devcod,
                                    oprcod,
                                    lodnum,
                                    sub_where_clause,
                                    dtl_where_clause,
                                    prtnum,
                                    prt_client_id,
                                    invsts,
                                    lotnum,
                                    revlvl,
                                    supnum,
                                    orgcod);
            }
            else
            {
                ret_status = srvInitiateCommandFormat(&AdjRes,
                                    "list rf adjust inventory details "
                                    " where stoloc = '%s' "
                                    "   and wh_id  = '%s' "
                                    "   and devcod = '%s' "
                                    "   and oprcod = '%s' "
                                    "   and lodnum = '%s' " 
                                    "%s" /* subnum information */
                                    "%s" /* dtlnum information */
                                    "   and prtnum = '%s' " 
                                    "   and prt_client_id = '%s' " 
                                    "   and invsts = '%s' " 
                                    "   and lotnum = '%s' " 
                                    "   and revlvl = '%s' " 
                                    "   and supnum = '%s' " 
                                    "   and orgcod = '%s' " 
                                    "",
                                    stoloc,
                                    wh_id,
                                    devcod,
                                    oprcod,
                                    lodnum,
                                    sub_where_clause,
                                    dtl_where_clause,
                                    prtnum,
                                    prt_client_id,
                                    invsts,
                                    lotnum,
                                    revlvl,
                                    supnum,
                                    orgcod);
            }
            
            if (eOK != ret_status)
            {
                srvFreeMemory (SRVRET_STRUCT, AdjRes);
                return (srvResults(ret_status, NULL));
            }

            invadjres = srvGetResults(AdjRes);
        }

        lincnt_hold = 0;
    }
    

    /* Now we need to generate the results */
    CurPtr = srvResultsInit(eOK,
                            "list", COMTYP_CHAR, 40,
                            "fill_line", COMTYP_CHAR, 40,
                            "invid", COMTYP_CHAR, LODNUM_LEN,
                            "lodnum", COMTYP_CHAR, LODNUM_LEN,
                            //MGP Custom column
                            "orig_lodnum", COMTYP_CHAR, LODNUM_LEN,
                            "subnum", COMTYP_CHAR, SUBNUM_LEN,
                            "dtlnum", COMTYP_CHAR, DTLNUM_LEN,
                            "prtnum", COMTYP_CHAR, PRTNUM_LEN,
                            "prt_client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                            "invsts", COMTYP_CHAR, INVSTS_LEN,
                            "lotnum", COMTYP_CHAR, LOTNUM_LEN,
                            "revlvl", COMTYP_CHAR, REVLVL_LEN,
                            "supnum", COMTYP_CHAR, SUPNUM_LEN,
                            "orgcod", COMTYP_CHAR, ORGCOD_LEN,
                            "untcas", COMTYP_INT, sizeof(long),
                            "untpak", COMTYP_INT, sizeof(long),
                            "untqty", COMTYP_INT, sizeof(long),
                            "scanned_qty", COMTYP_INT, sizeof(long),
                            "catch_qty", COMTYP_FLOAT, sizeof(double),
                            "scanned_ctch_qty",COMTYP_FLOAT,sizeof(double),
                            "catch_cod", COMTYP_CHAR, CATCH_COD_LEN,
                            "colnam", COMTYP_CHAR, 8,
                            "lodlvl", COMTYP_CHAR, LODLVL_LEN,
                            "lincnt", COMTYP_INT, sizeof(long),
                            "lincnt_txt", COMTYP_CHAR, 5,
                            "rowcnt", COMTYP_INT, sizeof(long),
                            "audit_lvl", COMTYP_INT, sizeof(long),
                            NULL);

    lincnt = 0;
    
    rowcnt = sqlGetNumRows(invadjres);
    
    invadjrow = sqlGetRow(invadjres);
    
    if (lincnt_hold > 0 && summary_mode == TRUE)
    {
        /* 
         * If we have a line count and we are in summary mode,
         * we will only look at one row, but we need to point to
         * the particular row of interest.
         */
        for ( ; invadjrow && lincnt + 1 < lincnt_hold; 
                invadjrow = sqlGetNextRow(invadjrow))
        {
            lincnt++;    
        }
    }
    
    for (invadjrow = sqlGetRow(invadjres); invadjrow && norm_flg; 
                                   invadjrow = sqlGetNextRow(invadjrow))
    {
        /* 
         * If we have a line count and we are in summary mode,
         * we will only look at one row as it is here.
         */

        if (lincnt_hold > 0 && summary_mode == TRUE)
        {

            for (invadjrow = sqlGetRow(invadjres), lincnt = 1;
                    invadjrow && lincnt < lincnt_hold; 
                    invadjrow = sqlGetNextRow(invadjrow))
            {
                lincnt++;    
            }
        }
        else
        {
            lincnt++;    
        }
        
        sprintf(lincnt_txt, "%ld", lincnt);

        /* grab the subnum, prtnum, invsts and untqty */
        memset(invid, 0, sizeof(invid));
        memset(lodnum, 0, sizeof(lodnum));
        memset(subnum, 0, sizeof(subnum));
        memset(dtlnum, 0, sizeof(dtlnum));
        memset(prtnum, 0, sizeof(prtnum));
        memset(prt_client_id, 0, sizeof(prt_client_id));
        memset(invsts, 0, sizeof(invsts));
        memset(lotnum, 0, sizeof(lotnum));
        memset(revlvl, 0, sizeof(revlvl));
        memset(supnum, 0, sizeof(supnum));
        memset(orgcod, 0, sizeof(orgcod));		
        memset(colnam, 0, sizeof(colnam));
        memset(lodlvl, 0, sizeof(lodlvl));
        
        /* MGP variable.*/
        memset(orig_lodnum, 0, sizeof(lodnum));

        if (!sqlIsNull(invadjres, invadjrow, "lodnum"))
        {
            misTrimcpy(lodnum,
                       sqlGetString(invadjres, invadjrow, "lodnum"),
                       LODNUM_LEN);

            misTrimcpy(invid, lodnum, sizeof(invid)-1);
            sprintf(colnam, "lodnum");

            memset(lodlvl, 0, sizeof(lodlvl));
            misTrimcpy(lodlvl, LODLVL_LOAD, LODLVL_LEN);
        }
        
 				/* MGP set the custom variable with the value that comes out of the command.*/
        misTrc(T_FLOW, "About to set the custom variable now for lincnt = 0...");
            
        if (!sqlIsNull(invadjres, invadjrow, "orig_lodnum"))
        {
            misTrc(T_FLOW, "Inside custom if branch now for lincnt = 0...");
            misTrimcpy(orig_lodnum,
                      sqlGetString(invadjres, invadjrow, "orig_lodnum"),
                      LODNUM_LEN);

            //misTrimcpy(invid, orig_lodnum, sizeof(invid)-1);
            sprintf(colnam, "lodnum");
        }       

        if (!sqlIsNull(invadjres, invadjrow, "subnum"))
        {
            misTrimcpy(subnum,
                       sqlGetString(invadjres, invadjrow, "subnum"),
                       SUBNUM_LEN);

            memset(invid, 0, sizeof(invid));
            misTrimcpy(invid, subnum, sizeof(invid)-1);
            sprintf(colnam, "subnum");

            memset(lodlvl, 0, sizeof(lodlvl));
            misTrimcpy(lodlvl, LODLVL_SUB, LODLVL_LEN);
        }
        
        if (!sqlIsNull(invadjres, invadjrow, "dtlnum"))
        {
            misTrimcpy(dtlnum,
                       sqlGetString(invadjres, invadjrow, "dtlnum"),
                       DTLNUM_LEN);

            memset(invid, 0, sizeof(invid));
            misTrimcpy(invid, dtlnum, sizeof(invid)-1);
            sprintf(colnam, "dtlnum");

            memset(lodlvl, 0, sizeof(lodlvl));
            misTrimcpy(lodlvl, LODLVL_DETAIL, LODLVL_LEN);
        }

        if(!misTrimStrncmp(lodlvl, LODLVL_SUB, LODLVL_LEN))
        {
            /* 
             * At this point we know that we have a subnum value, because
             * that is how we determine to set the lodlvl to S.
             */

            ret_status = srvInitiateCommandFormat(NULL,
                            "[select 1 from invdtl "
                            "  where subnum = '%s' "
                            "    and phdflg = %ld "
                            "]",
                            subnum,
                            MOCA_TRUE);
            if (ret_status == eOK)
            {
                /*
                 * We found some invdtl record on this sub that
                 * is serialized (phdflg = 1)... so we can assume
                 * that the load level will be dtl (although we
                 * might have some product that is not serialized
                 * at the detail level on this subload.
                 */
                memset(lodlvl, 0, sizeof(lodlvl));
                misTrimcpy(lodlvl, LODLVL_DETAIL, LODLVL_LEN);
            }
        }
        
        if (!misTrimStrncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN))
        {
            /* 
             * We will only drop in here if the lodlvl was not determined
             * to be a subload or detail level.
             * 
             * At this point we know that we have a lodnum value, because
             * that is how we determine to set the lodlvl to L.
             * 
             * NOTE: From a practical standpoint we will always have
             *       a lodnum value.
             */
             
            /* MGP JRZ 12/7/13 Start - Changed to add a like and percent as the lodnum passed in will be the final ten digits.*/
            ret_status = srvInitiateCommandFormat(NULL,
                            "[select 1 from dual "
                            " where exists ("
                            " select 1 from invsub "
                            "  where lodnum like '%%%s' " 
                            "    and phyflg = %ld) "
                            "]",
                            lodnum,
                            MOCA_TRUE);
            if (ret_status == eOK)
            {
                /*
                 * We found some invsub record on this load that
                 * is serialized (phyflg = 1)... so we can assume
                 * that the load level will be either sub or dtl
                 * or some of both.
                 */
                memset(lodlvl, 0, sizeof(lodlvl));
                misTrimcpy(lodlvl, LODLVL_SUB, LODLVL_LEN);
            }
        }
        
        misTrimcpy(prtnum, sqlGetString(invadjres, invadjrow,"prtnum"),
                           PRTNUM_LEN);

        misTrimcpy(prt_client_id, 
                   sqlGetString(invadjres, invadjrow, "prt_client_id"),
                   CLIENT_ID_LEN);

        misTrimcpy(invsts, sqlGetString(invadjres, invadjrow, "invsts"),
                   INVSTS_LEN);

        misTrimcpy(lotnum, sqlGetString(invadjres, invadjrow, "lotnum"),
                   LOTNUM_LEN);

        misTrimcpy(revlvl, sqlGetString(invadjres, invadjrow, "revlvl"),
                   REVLVL_LEN);

        misTrimcpy(supnum, sqlGetString(invadjres, invadjrow, "supnum"),
                   SUPNUM_LEN);

        misTrimcpy(orgcod, sqlGetString(invadjres, invadjrow, "orgcod"),
                   ORGCOD_LEN);
        
        untcas = sqlGetLong(invadjres, invadjrow, "untcas");
        untpak = sqlGetLong(invadjres, invadjrow, "untpak");
        untqty = sqlGetLong(invadjres, invadjrow, "untqty");
        scanned_qty = sqlGetLong(invadjres, invadjrow, "scanned_qty");
        catch_qty = sqlGetFloat(invadjres, invadjrow, "catch_qty");
        scanned_ctch_qty = sqlGetFloat(invadjres,invadjrow,"scanned_ctch_qty");
        audit_lvl = sqlGetLong(invadjres, invadjrow, "audit_lvl");
        
        memset(catch_cod, 0, sizeof(catch_cod));
        if (!sqlIsNull(invadjres, invadjrow, "catch_cod"))
        {
            misTrimcpy(catch_cod,
                       sqlGetString(invadjres, invadjrow, "catch_cod"),
                       CATCH_COD_LEN);
        }

        /* Test to see if we are outputing all rows or a specific one. */
        if (lincnt_hold == 0 || lincnt_hold == lincnt)
        {			
			/* Now we need to generate the results */
            sprintf(list, 
                    "%-14.14s %5ld %-15.15s",
                    invid, 
                    scanned_qty, 
                    prtnum);
					
		/* MGP Start - Prior to generating the results, we need to get the code date for the inventory and update 'list'*/

            ret_status = srvInitiateInlineFormat(&codePtr,
                            "get za code date for load "
                            "  where lodnum = '%s' "
							" and prtnum = '%s'",
                            orig_lodnum,
							prtnum);
            if (ret_status == eOK)
            {
                codeRes = srvGetResults(codePtr);
				codeRow = sqlGetRow(codeRes);
				misTrimcpy(codeDate,sqlGetString(codeRes, codeRow, "codeDate7WithPart7"),15);
				/* update list variable */
				sprintf(list, 
                    "%-14.14s %5ld %-15.15s",
                    invid, 
                    scanned_qty, 
                    codeDate);
            }
			
		/* MGP End*/
            ret_status = srvResultsAdd (CurPtr,
                                        list,
                                        fill_line,
                                        invid,
                                        lodnum,
                                        //MGP custom column
                                        orig_lodnum,
                                        subnum,
                                        dtlnum,
                                        prtnum,
                                        prt_client_id,
                                        invsts,
                                        lotnum,
                                        revlvl,
                                        supnum,
                                        orgcod,
                                        untcas,
                                        untpak,
                                        untqty,
                                        scanned_qty,
                                        catch_qty,
                                        scanned_ctch_qty,
                                        catch_cod,
                                        colnam,
                                        lodlvl,
                                        lincnt,
                                        lincnt_txt,
                                        rowcnt,
                                        audit_lvl,
                                        NULL);
        }
        
        /* 
         * If we have a line count and we are in summary mode,
         * we will only look at one row, so skip over the rest.
         */
        if (lincnt_hold > 0 && summary_mode == TRUE)
        {
            /* Stop this after a single loop. */
            norm_flg = FALSE;

        }

    }
    
    /* Normal successful completion */

    if (AdjRes) srvFreeMemory (SRVRET_STRUCT, AdjRes);

	/* MGP Free up memory for code date */
	if (codePtr) srvFreeMemory (SRVRET_STRUCT, codePtr);
	
    if (CurPtr)
        return(CurPtr);
    else
        return(srvResults(eOK, NULL));
}
