static const char *rcsid = "$Id: listListAvailablePickListsToBuild.c 162023 2008-07-03 07:07:27Z bli $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2006 RedPrairie Corporation.  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>


LIBEXPORT
RETURN_STRUCT *listListAvailablePickListsToBuild( char *list_typ,
                                                  char *break_on_combined,
                                                  char *pick_order_by,
                                                  char *pick_order_by_combined,
                                                  moca_bool_t *labor_futures_installed_i,
                                                  char *list_id_i,
                                                  moca_bool_t *one_pass_only_installed_i)
{
    RETURN_STRUCT *CurPtr = NULL;

    long ret_status = 0;
    static moca_bool_t labor_futures_installed;

    char buffer[8000];
    char tabbuff[1000];
    char wherebuff[3000];
    char listwhere[200];
    char prtmstbuff[500];
    char pckmovbuff[200];
    char locmstbuff[500];
    char aremstbuff[200];
    
    memset(buffer, 0, sizeof(buffer));
    memset(tabbuff, 0, sizeof(tabbuff));
    memset(wherebuff, 0, sizeof(wherebuff));
    memset(listwhere, 0, sizeof(listwhere));
    memset(prtmstbuff, 0, sizeof(prtmstbuff));
    memset(locmstbuff, 0, sizeof(locmstbuff));
    memset(aremstbuff, 0, sizeof(aremstbuff));
    memset(pckmovbuff, 0, sizeof(pckmovbuff));
    
    if (labor_futures_installed_i)
        labor_futures_installed = *labor_futures_installed_i;
    else
        labor_futures_installed = BOOLEAN_FALSE;
        
    if (list_id_i && misTrimLen(list_id_i, LIST_ID_LEN))
    {
        sprintf(listwhere, "and pl.list_id = '%s' ", list_id_i);
    }

    if (one_pass_only_installed_i && *one_pass_only_installed_i == 1)
    {
        strcat(listwhere, " and nvl(pl.one_pass_only, 0) = 0 ");       
    }
    /*
     * We will select pick lists already existing in the system.
     */

    /* For performance reasons, we're only going to include joins
     * to tables that we actually need in this select.
     */

    if (misCiStrncmp(list_typ, LIST_TYP_ORDER, LIST_TYP_LEN) == 0) 
    {
        if ((break_on_combined && strstr(break_on_combined, "shipment_line")) ||
            (pick_order_by && strstr(pick_order_by, "shipment_line")))
        {
            sprintf(tabbuff,
                    " join shipment "
                    "   on (pckwrk.ship_id = shipment.ship_id) "
                    " join shipment_line "
                    "   on (shipment.ship_id = shipment_line.ship_id) "
                    "  and (pckwrk.ship_line_id = shipment_line.ship_line_id) "
                    "  and (pckwrk.wh_id = shipment_line.wh_id) ");
        }
        else if ((break_on_combined && strstr(break_on_combined, "shipment")) ||
                 (pick_order_by && strstr(pick_order_by, "shipment")))
        {
            sprintf(tabbuff,
                    " join shipment "
                    "   on (pckwrk.ship_id = shipment.ship_id) ");
        }

        sprintf(wherebuff,
                " and pckwrk.ordnum is not null ");
    }

    if (misCiStrncmp(list_typ, LIST_TYP_WORK_ORDER, LIST_TYP_LEN) == 0)
    {

        if ((break_on_combined && strstr(break_on_combined, "wkodtl")) ||
            (pick_order_by && strstr(pick_order_by, "wkodtl")))
        {
            sprintf(tabbuff, 
                    " join wkohdr "
                    "   on (pckwrk.wkonum = wkohdr.wkonum) "
                    "  and (pckwrk.wkorev = wkohdr.wkorev) "
                    "  and (pckwrk.client_id = wkohdr.client_id) "
                    "  and (pckwrk.wh_id = wkohdr.wh_id) "
                    " join wkodtl "
                    "   on (wkohdr.wkonum = wkodtl.wkonum) "
                    "  and (wkohdr.client_id = wkodtl.client_id) "
                    "  and (wkohdr.wkorev = wkodtl.wkorev) "
                    "  and (wkohdr.wh_id = wkodtl.wh_id) ");
        }
        else if ((break_on_combined && strstr(break_on_combined, "wkohdr")) ||
                 (pick_order_by && strstr(pick_order_by, "wkohdr")))
        {
            sprintf(tabbuff,
                    " join wkohdr "
                    "   on (pckwrk.wkonum = wkohdr.wkonum) "
                    "  and (pckwrk.wkorev = wkohdr.wkorev) "
                    "  and (pckwrk.client_id = wkohdr.client_id) "
                    "  and (pckwrk.wh_id = wkohdr.wh_id) ");
        }

        sprintf(wherebuff,                       
                " and pckwrk.wkonum is not null ");
    }

    if ((break_on_combined && strstr(break_on_combined, "prtfam")) ||
        (pick_order_by && strstr(pick_order_by, "prtfam")))
    {
        sprintf(prtmstbuff,
                "  join prtmst_view "
                "    on (pckwrk.prtnum = prtmst_view.prtnum) "
                "   and (pckwrk.prt_client_id = prtmst_view.prt_client_id) "
                "   and (pckwrk.wh_id = prtmst_view.wh_id) "
                "  left outer join prtfam "
                "    on (prtmst_view.prtfam = prtfam.prtfam) ");
    }
    else if ((break_on_combined && strstr(break_on_combined, "prtmst_view")) ||
        (pick_order_by && strstr(pick_order_by, "prtmst_view")))
    {
        sprintf(prtmstbuff,
                "  join prtmst_view "
                "    on (pckwrk.prtnum = prtmst_view.prtnum) "
                "   and (pckwrk.prt_client_id = prtmst_view.prt_client_id) "
                "   and (pckwrk.wh_id = prtmst_view.wh_id) ");
    }

    if ((break_on_combined && strstr(break_on_combined, "zonmst")) ||
        (pick_order_by && strstr(pick_order_by, "zonmst")))
    {
        sprintf(locmstbuff,
                "  join locmst "
                "    on (pckwrk.srcloc = locmst.stoloc) "
                "   and (pckwrk.wh_id = locmst.wh_id) "
                "  join zonmst "
                "    on (locmst.wrkzon = zonmst.wrkzon) "
                "   and (locmst.wh_id = zonmst.wh_id) ");
    }
    else if ((break_on_combined && strstr(break_on_combined, "locmst")) ||
             (pick_order_by && strstr(pick_order_by, "locmst")))
    {
        sprintf(locmstbuff,
                "  join locmst "
                "    on (pckwrk.srcloc = locmst.stoloc) "
                "   and (pckwrk.wh_id = locmst.wh_id) ");
    }

    if ((break_on_combined && strstr(break_on_combined, "aremst")) ||
        (pick_order_by && strstr(pick_order_by, "aremst")))
    {
        sprintf(aremstbuff,
                "  join aremst "
                "    on (pckwrk.srcare = aremst.arecod) "
                "   and (pckwrk.wh_id = aremst.wh_id) ");
    }

    if ((break_on_combined && strstr(break_on_combined, "pckmov")) ||
        (pick_order_by && strstr(pick_order_by, "pckmov")))
    {
        sprintf(pckmovbuff,
                "  join pckmov "
                "    on pckwrk.cmbcod = pckmov.cmbcod "
                "   and pckwrk.dstare = pckmov.arecod "
                "   and pckwrk.wh_id = pckmov.wh_id ");
    }


    /* Okay now let's put this select all together. */
    
    /* JRZ MGP Start 12/3/2013 - Cleaning up the below query for performance concerns. */
    
    /* Original Start
    sprintf(buffer,
            "[ "
            "select pckwrk.wh_id wh_id, "
            "       pcklst_ext.list_id list_id, "
            "       pcklst_ext.max_list_cube max_list_cube, "
            "       pcklst_ext.list_cube_wgt_thr list_cube_wgt_thr, "
            "       pcklst_ext.list_max_add_caswgt list_max_add_caswgt, "
            "       nvl(pcklst_ext.totvol, 0.0) totvol, "
            "       nvl(pcklst_ext.totwgt, 0.0) totwgt, "
            "       nvl(pcklst_ext.extvol, 0) extvol, "
            "       nvl(pcklst_ext.intvol, 0) intvol, "
            "      nvl(pcklst_ext.one_pass_only, 0) one_pass_only, "
            "       (select count('x') "
            "          from pckwrk pw1 "
            "         where pw1.list_id = pcklst_ext.list_id "
            "           and pw1.lodlvl = '%s') load_picks_cnt, "    
            "       %s, "         
            "       %s "   
            "  from pckwrk "
            "       %s "    
            "       %s "   
            "       %s "   
            "       %s "  
            "  join (select pl.*, "
            "               (select pw2.wrkref "
            "                  from pckwrk pw2 "
            "                 where pw2.wrktyp = '%s' " 
            "                   and (pw2.list_id = pl.list_id or "
            "                        exists " 
            "                             (select 'x' "
            "                                from pckwrk pw3 "
            "                               where pw3.wh_id = pw2.wh_id "
            "                                 and pw3.wrktyp = '%s' "  
            "                                  and pw3.list_id = pl.list_id "
            "                                  and pw2.ctnnum = pw3.subnum)) "
            "                   and rownum <= 1) first_wrkref "
            "          from pcklst pl "
            "         where (pl.list_sts = '%s' " 
            "            or pl.list_sts = '%s'  " 
            "            or pl.list_sts = '%s') " 
            "            %s) pcklst_ext " 
            "    on pckwrk.wrkref = pcklst_ext.first_wrkref "
            "  %s "  
            " where 0 = %ld " 
            "   %s "         
            " order by wh_id, "
            "          break_on_combined, "  
            "          %s "   
            " ]",
            LODLVL_LOAD,
            break_on_combined,
            pick_order_by_combined,
            prtmstbuff,
            locmstbuff,
            aremstbuff,
            pckmovbuff,
            WRKTYP_PICK,
            WRKTYP_KIT,
            LIST_STS_PENDING, LIST_STS_RELEASED, LIST_STS_PICKING_IN_PROCESS,
            listwhere,
            tabbuff,
            labor_futures_installed,
            wherebuff, 
            pick_order_by);
            Original End */
      
      /* New Tuned Start */      
      sprintf(buffer,
            "[ "
            "select pckwrk.wh_id wh_id, "
            "       pcklst_ext.list_id list_id, "
            "       pcklst_ext.max_list_cube max_list_cube, "
            "       pcklst_ext.list_cube_wgt_thr list_cube_wgt_thr, "
            "       pcklst_ext.list_max_add_caswgt list_max_add_caswgt, "
            "       nvl(pcklst_ext.totvol, 0.0) totvol, "
            "       nvl(pcklst_ext.totwgt, 0.0) totwgt, "
            "       nvl(pcklst_ext.extvol, 0) extvol, "
            "       nvl(pcklst_ext.intvol, 0) intvol, "
            "       nvl(pcklst_ext.one_pass_only, 0) one_pass_only, "
            "       (select count('x') "
            "          from pckwrk pw1 "
            "         where pw1.list_id = pcklst_ext.list_id "
            "           and pw1.lodlvl = '%s') load_picks_cnt, "    /* L */
            "       %s, "   /* @break_on_combined */       
            "       %s "    /* @pick_order_by_combined */ 
            "  from pckwrk "
            "       %s "    /* prtmst_view, prtfam buffer */
            "       %s "    /* locmst, zonmst buffer */
            "       %s "    /* aremst buffer */
            "       %s "    /* pckmov buffer */
            "    join (select pl.list_id, "
            "                 pl.max_list_cube, "
            "                 pl.list_cube_wgt_thr, "
            "                 pl.list_max_add_caswgt, "
            "                 pl.totvol, "
            "                 pl.totwgt, "
            "                 pl.extvol, "
            "                 pl.intvol, "
            "                 pl.one_pass_only, "
            "             max(pw.wrkref) first_wrkref "
            "            from pcklst pl "
            "            join pckwrk pw on pw.list_id = pl.list_id left "
            "            join pckwrk pw2 on pw.ctnnum = pw2.subnum "
            "          where (pw.wrktyp = '%s' or pw2.wrktyp = '%s') " /* P, K */ 
            "          and   (pl.list_sts = '%s' or pl.list_sts = '%s' or pl.list_sts = '%s') " /* P, R, I */ 
            "          %s " /* list_id where */
            "           group by pl.list_id, "
            "                    pl.max_list_cube, "
            "                    pl.list_cube_wgt_thr, "
            "    								 pl.list_max_add_caswgt, "
            "    						     pl.totvol, "
            "    							   pl.totwgt, "
            "    								 pl.extvol, "
            "    								 pl.intvol, "
            "    								 pl.one_pass_only) pcklst_ext "
            "    on pckwrk.wrkref = pcklst_ext.first_wrkref "
            "    %s "    /* @tabbuff */
            /* If Labor Futures is installed, then never try to find an
             * existing list to attach the pick to, so get an empty structure.
             */
            " where 0 = %ld " /* @labor_futures_installed */
            "   %s "          /* @wherebuff */
            " order by wh_id, "
            "          break_on_combined, "  
            "          %s "    /* @pick_order_by */
            " ]",
            LODLVL_LOAD,
            break_on_combined,
            pick_order_by_combined,
            prtmstbuff,
            locmstbuff,
            aremstbuff,
            pckmovbuff,
            WRKTYP_PICK,
            WRKTYP_KIT,
            LIST_STS_PENDING, LIST_STS_RELEASED, LIST_STS_PICKING_IN_PROCESS,
            listwhere,
            tabbuff,
            labor_futures_installed,
            wherebuff, 
            pick_order_by);
            /* New Tuned End */ 

    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return(srvResults(ret_status, NULL));
    }

    return(CurPtr);
}
