static const char *rcsid = "$Id: varRfValidateRFPickupLodnum.c,v 1.5 2010/11/15 20:40:52 dkells Exp $";
/*#START***********************************************************************
*  Copyright (c) 2002 RedPrairie Corporation. All rights reserved
*  Waukesha, Wisconsin,  U.S.A.
*  All rights reserved.
*#END************************************************************************/

#include <moca_app.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "rflib.h"

#define MSG_HDR "RFLIB: (rfValidateRFPickLodnum) "

LIBEXPORT
RETURN_STRUCT *varRfValidateRFPickupLodnum ( char *dsplod_i,
                                          char *lodid_i,
                                          char *wrkref_i,
                                          char *dsploc_i,
                                          char *wh_id_i)
{
    RETURN_STRUCT *CurPtr;
    mocaDataRes   *Res=NULL;
    mocaDataRes   *Res1=NULL;
    mocaDataRow   *Row=NULL;
    mocaDataRow   *Row1=NULL;
    long           ret_status;
    char           sqlBuffer [5000];
    char           tmpBuffer [1000];

    char           dsplod[LODNUM_LEN+1];
    char           lodid[LODNUM_LEN+1];
    char           wrkref[WRKREF_LEN+1];
    char           dsploc[STOLOC_LEN+1];

    char           lodnum[LODNUM_LEN+1];
    char           subnum[SUBNUM_LEN+1];
    char           dtlnum[DTLNUM_LEN+1];
    char           lodlvl[LODLVL_LEN+1];

    char           prtnum[PRTNUM_LEN+1];
    char           prt_client_id[CLIENT_ID_LEN+1];
    char           stoloc[STOLOC_LEN+1];
    char           wh_id[WH_ID_LEN+1];

    
    moca_bool_t    invmov_typ_null = BOOLEAN_TRUE;
    char           invmov_typ[INVMOV_TYP_LEN+1];
    moca_bool_t    wko_pick = BOOLEAN_FALSE;

    long           untqty;
    double         catch_qty;

    moca_bool_t    cmpflg;
    moca_bool_t    qtyflg;
    moca_bool_t    locflg;
    moca_bool_t    prtflg;
    moca_bool_t    lodflg;
    moca_bool_t    catch_flg;
    moca_bool_t    phyflg;
    moca_bool_t    phdflg;

    /* Initialize local variables */
    memset(dsplod, 0, LODNUM_LEN+1);
    memset(lodid,  0, LODNUM_LEN+1);
    memset(wrkref, 0, WRKREF_LEN+1);
    memset(dsploc, 0, STOLOC_LEN+1);
    memset(wh_id, 0 , WH_ID_LEN+1);
    if (dsplod_i && misTrimLen (dsplod_i, LODNUM_LEN))
    {
        misTrimcpy (dsplod, dsplod_i, LODNUM_LEN);
    }
    if (lodid_i && misTrimLen (lodid_i, LODNUM_LEN))
    {
        misTrimcpy (lodid, lodid_i, LODNUM_LEN);
    }
    if (wrkref_i && misTrimLen (wrkref_i, WRKREF_LEN))
    {
        misTrimcpy (wrkref, wrkref_i, WRKREF_LEN);
    }
    if (dsploc_i && misTrimLen (dsploc_i, STOLOC_LEN))
    {
        misTrimcpy (dsploc, dsploc_i, STOLOC_LEN);
    }
    if (wh_id_i && misTrimLen (wh_id_i, WH_ID_LEN))
    {
        misTrimcpy (wh_id, wh_id_i, WH_ID_LEN);
    }
    
    /* Verify required arguments are present */

    /* Verify if any optional arguments are present */

/*-----------------------------------------------------------------*/

    cmpflg = BOOLEAN_FALSE;
    qtyflg = BOOLEAN_TRUE;
    locflg = BOOLEAN_TRUE;
    prtflg = BOOLEAN_TRUE;
    lodflg = BOOLEAN_TRUE;
    catch_flg = BOOLEAN_TRUE;
    phyflg = BOOLEAN_FALSE;
    phdflg = BOOLEAN_FALSE;
    memset(stoloc, 0, sizeof(stoloc));
    memset(lodnum, 0, sizeof(lodnum));
    memset(subnum, 0, sizeof(subnum));
    memset(dtlnum, 0, sizeof(dtlnum));
    strcpy(lodlvl, LODLVL_LOAD);
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(tmpBuffer, 0, sizeof(tmpBuffer));
    memset(invmov_typ, 0, sizeof(invmov_typ));
    untqty = 0;
    catch_qty = 0.0;

    /* Lets see what was input, load, sub-load, or detail */
    sprintf(sqlBuffer,
            "get translated inventory identifier where id = \"%s\" " 
            " and wh_id = '%s'",
            lodid, wh_id);
    CurPtr = NULL;
    ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
    if (ret_status == eOK && sqlGetNumRows(srvGetResults(CurPtr)) == 1)
    {
        Res = srvGetResults(CurPtr);
        Row = sqlGetRow(Res);
        if ((!sqlIsNull(Res, Row, "stoloc")) &&
            (misTrimLen(sqlGetString(Res, Row, "stoloc"), STOLOC_LEN) > 0))
            misTrimcpy(stoloc,
                    sqlGetString(Res, Row, "stoloc"),
                    STOLOC_LEN);
        if ((!sqlIsNull(Res, Row, "lodnum")) &&
            (misTrimLen(sqlGetString(Res, Row, "lodnum"), LODNUM_LEN) > 0))
        {
            misTrimcpy(lodnum,
                    sqlGetString(Res, Row, "lodnum"),
                    LODNUM_LEN);
            sprintf (tmpBuffer, "and invlod.lodnum = '%s' ", lodnum);
        }
        if ((!sqlIsNull(Res, Row, "subnum")) &&
            (misTrimLen(sqlGetString(Res, Row, "subnum"), SUBNUM_LEN) > 0))
        {
            misTrimcpy(subnum,
                    sqlGetString(Res, Row, "subnum"),
                    SUBNUM_LEN);
            sprintf (tmpBuffer, "and invsub.subnum = '%s' ", subnum);
        }
        if ((!sqlIsNull(Res, Row, "dtlnum")) &&
            (misTrimLen(sqlGetString(Res, Row, "dtlnum"), DTLNUM_LEN) > 0))
        {
            misTrimcpy(dtlnum,
                    sqlGetString(Res, Row, "dtlnum"),
                    DTLNUM_LEN);
            sprintf (tmpBuffer, "and invdtl.dtlnum = '%s' ", dtlnum);
        }
        
        if (strncmp(sqlGetString(Res, Row, "colnam"),
                    "subucc", 6) == 0)
        {
            strcpy(lodid,
                   sqlGetString(Res, Row, "subnum"));
        }
        else if (strncmp(sqlGetString(Res, Row, "colnam"), "loducc", 6) == 0)
        {
            strcpy(lodid,
                   sqlGetString(Res, Row, "lodnum"));
        }

        srvFreeMemory(SRVRET_STRUCT, CurPtr);
    }
    else
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
    }

    if (!strlen(lodnum) && 
        !strlen(subnum) && 
        !strlen(dtlnum) &&
        !strlen(stoloc))
    {
        return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));
    }

    /* Get the physical flag. */
    sprintf(sqlBuffer,
            " list inventory " 
            "  where wh_id = '%s'"
            "  %s ",
            wh_id, tmpBuffer);
    CurPtr = NULL;
    ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
    if (ret_status == eOK)
    {
        Res = srvGetResults(CurPtr);
        Row = sqlGetRow(Res);
        if (sqlGetBoolean(Res, Row, "phyflg") == BOOLEAN_TRUE)
            phyflg = BOOLEAN_TRUE;
        if (sqlGetBoolean(Res, Row, "phdflg") == BOOLEAN_TRUE)
            phdflg = BOOLEAN_TRUE;
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
    }
    else
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
    }


/*-----------------------------------------------------------------*/

    /* If there is no validate load number and no wrkref, */
    /* but there is a source location ... */
    /* make sure we are picking out of the source location */
    if (!strlen(dsplod) && !strlen(wrkref) && strlen(dsploc))
    {
        if (strcmp(misTrim(stoloc),
                   misTrim(dsploc)) != 0)
        {
            return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));
        }
    }

/*-----------------------------------------------------------------*/

    /* Else if there is a validation string, but no work reference */
    /* make sure we are picking up the correct load */
    else if (strlen(dsplod) && !strlen(wrkref))
    {
        if (strcmp(dsplod, lodid) != 0 && strcmp(dsplod, lodnum) != 0)
        {
            return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));
        }
    }
    /* Else if we have a wrkref, and maybe a validation string */
    else if (strlen(wrkref))
    {
        sprintf(sqlBuffer,
                "select srcloc, lodnum, subnum, dtlnum, lodlvl, "
                "       prtnum, prt_client_id, "
                "       lotnum, orgcod, revlvl, invsts, invsts_prg, "
                "       untcas, untpak, locflg, lodflg, subflg, "
                "       dtlflg, prtflg, orgflg, revflg, lotflg, "
                "       qtyflg, catch_qty_flg, wrktyp,"
        "       sum(pckqty - appqty) pckqty, "
        "       sum(nvl(pck_catch_qty, 0) "
        "         - nvl(app_catch_qty, 0)) pck_catch_qty "
                "  from pckwrk "
                " where cmbcod = "
                "  (select cmbcod from pckwrk "
                "    where wrkref = '%s') "
                " group by srcloc, lodnum, subnum, dtlnum, lodlvl, "
                "          prtnum, prt_client_id, "
                "          lotnum, orgcod, revlvl, invsts, invsts_prg, "
                "          untcas, untpak, locflg, lodflg, subflg, "
                "          dtlflg, prtflg, orgflg, revflg, lotflg, "
                "          qtyflg, catch_qty_flg, wrktyp ",
                wrkref);
        ret_status = sqlExecStr(sqlBuffer, &Res);
        if (ret_status != eOK)
        {
            sqlFreeResults(Res);
            return (srvResults(ret_status, NULL));
        }
        Row = sqlGetRow(Res);

        /* If we requested a specific load, sub, or detail, */
        /* make sure we have it */
        misTrimcpy(lodlvl,
                sqlGetString(Res, Row, "lodlvl"),
                LODLVL_LEN);

        if (strncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0 &&
            !sqlIsNull(Res, Row, "lodnum") &&
             strncmp(misTrim(sqlGetString(Res, Row, "lodnum")), lodnum, 
                     LODNUM_LEN) != 0)
        {
            sqlFreeResults(Res);
            return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));
        }

        if (strncmp(lodlvl, LODLVL_SUB, LODLVL_LEN) == 0 &&
            !sqlIsNull(Res, Row, "subnum") &&
            strncmp(misTrim(sqlGetString(Res, Row, "subnum")), subnum, 
                    SUBNUM_LEN) != 0)
        {
            sqlFreeResults(Res);
            return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));
        }

        if (strncmp(lodlvl, LODLVL_DTL, LODLVL_LEN) == 0 &&
            !sqlIsNull(Res, Row, "dtlnum") &&
            strncmp(misTrim(sqlGetString(Res, Row, "dtlnum")), dtlnum,  
                    DTLNUM_LEN) != 0)
        {
            sqlFreeResults(Res);
            return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));
        }

        /* Now, if we are looking for a load, make sure we have */
        /* or a sub-load and we scanned a load with multiple cases */
        /* or a detail and we scanned a load */
        /* a load number and that load matches in quantity, etc. */
        /* And do the same for sub-load, detail, etc. */
        if (strncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0 ||
            (strncmp(lodlvl, LODLVL_SUB, LODLVL_LEN) == 0 &&
             !strlen(subnum)) ||
            (strncmp(lodlvl, LODLVL_DTL, LODLVL_LEN) == 0 &&
             !strlen(dtlnum) && !strlen(subnum)))
        {
            if (!strlen(lodnum))
            {
                sqlFreeResults(Res);
                return (srvResults(ret_status, NULL));
            }
            else
            {
                sprintf(sqlBuffer,
                        "select invlod.stoloc, invdtl.prtnum, "
                        "       invdtl.prt_client_id, invdtl.invsts, "
                        "       sum(invdtl.untqty) untqty, "
                        "       sum(invdtl.catch_qty) catch_qty "
                        "  from invdtl, invsub, invlod "
                        " where invdtl.subnum = invsub.subnum "
                        "   and invsub.lodnum = invlod.lodnum "
                        "   and invlod.lodnum = '%s' "
                        " group by invlod.stoloc, invdtl.prtnum, "
                        "          invdtl.prt_client_id, invdtl.invsts ",
                        lodnum);
            }
        }
        else if (strncmp(lodlvl, LODLVL_SUB, LODLVL_LEN) == 0 ||
                 (strncmp(lodlvl, LODLVL_DTL, LODLVL_LEN) == 0 &&
                  !strlen(dtlnum)))
        {
            if (!strlen(subnum))
            {
                sqlFreeResults(Res);
                return (srvResults(ret_status, NULL));
            }
            else
            {
                sprintf(sqlBuffer,
                        "select invlod.stoloc, invdtl.prtnum, "
                        "       invdtl.prt_client_id, invdtl.invsts, "
                        "       sum(invdtl.untqty) untqty, "
                        "       sum(invdtl.catch_qty) catch_qty "
                        "  from invdtl, invlod, invsub "
                        " where invdtl.subnum = invsub.subnum "
                        "   and invsub.lodnum = invlod.lodnum "
                        "   and invsub.subnum = '%s' "
                        " group by invlod.stoloc, invdtl.prtnum, "
                        "          invdtl.prt_client_id, invdtl.invsts ",
                        subnum);
            }
        }
        else if (strncmp(lodlvl, LODLVL_DTL, LODLVL_LEN) == 0)
        {
            if (!strlen(dtlnum))
            {
                sqlFreeResults(Res);
                return (srvResults(ret_status, NULL));
            }
            else
            {
                sprintf(sqlBuffer,
                        "select invlod.stoloc, invdtl.prtnum, "
                        "       invdtl.prt_client_id, invdtl.invsts, "
                        "       sum(invdtl.untqty) untqty, "
                        "       sum(invdtl.catch_qty) catch_qty "
                        "  from invlod, invsub, invdtl "
                        " where invlod.lodnum = invsub.lodnum "
                        "   and invsub.subnum = invdtl.subnum "
                        "   and invdtl.dtlnum = '%s' "
                        " group by invlod.stoloc, invdtl.prtnum, "
                        "          invdtl.prt_client_id, invdtl.invsts ",
                        dtlnum);
            }
        }
        ret_status = sqlExecStr(sqlBuffer, &Res1);
        if (ret_status != eOK)
        {
            sqlFreeResults(Res);
            sqlFreeResults(Res1);
            return (srvResults(ret_status, NULL));
        }

        Row1 = sqlGetRow(Res1);

        /* If we are working on anything other than a kit ... and */
        /* If the loc, part, status, or quantity does not match, */
        /* get out of here */
        if (strncmp(misTrim(sqlGetString(Res, Row, "wrktyp")),
                    WRKTYP_KIT, 
                    WRKTYP_LEN) != 0)
        {
            if (strncmp(lodlvl, LODLVL_SUB, LODLVL_LEN) == 0 ||
                strncmp(lodlvl, LODLVL_DTL, LODLVL_LEN) == 0)
            {
                if (strncmp(sqlGetString(Res, Row, "srcloc"),
                            sqlGetString(Res1, Row1, "stoloc"),
                            STOLOC_LEN) != 0)
                {
                    sqlFreeResults(Res);
                    sqlFreeResults(Res1);
                    return (srvResults(ePAL_SRCLOC_NOTMATCH_PICK_SRCLOC, NULL));
                }
                else
                if (strncmp(sqlGetString(Res, Row, "prtnum"),
                            sqlGetString(Res1, Row1, "prtnum"),
                            PRTNUM_LEN) != 0)
                {
                    sqlFreeResults(Res);
                    sqlFreeResults(Res1);
                    return (srvResults(ePAL_PART_NOTMATCH_PICK_PART, NULL));
                }
                else
                if (strncmp(sqlGetString(Res, Row, "prt_client_id"),
                            sqlGetString(Res1, Row1, "prt_client_id"),
                            CLIENT_ID_LEN) != 0)
                {
                    sqlFreeResults(Res);
                    sqlFreeResults(Res1);
                    return (srvResults(ePAL_CLIENT_NOTMATCH_PICK_CLIENT, NULL));
                }
                else
                if (sqlGetFloat(Res, Row, "pckqty") < 
                    sqlGetFloat(Res1, Row1, "untqty") &&
                    (sqlGetBoolean(Res, Row, "qtyflg") == BOOLEAN_FALSE) &&
                    (strncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0))
                {
                    sqlFreeResults(Res);
                    sqlFreeResults(Res1);
                    return (srvResults(ePAL_QTY_NOTMATCH_PICK_QTY, NULL));
                }
                else 
		if (sqlGetFloat(Res, Row, "pckqty") <
                    sqlGetFloat(Res1, Row1, "untqty") &&
                   (sqlGetBoolean(Res, Row, "qtyflg") == BOOLEAN_TRUE))
                {
		     misTrc(T_FLOW, "Special Handling for case picking "
				    "partial inventory from a non-mixed load "
				    "with multiple sub-loads");

                    /* Here we handle a specific scenario:
                     * If we pick partial inventory from a non-mixed load with
                     * multiple sub-loads, then we pubish prtnum, prt_client_id,
                     * and pckqty for process inventory move to move specified
                     * qty from the load.                     */ 
                    if (sqlGetNumRows(Res1) == 1)
                    {
                         strncpy (prtnum,
                             sqlGetString(Res, Row, "prtnum"), PRTNUM_LEN);
                         strncpy(prt_client_id,
                             sqlGetString(Res, Row, "prt_client_id"),
                             CLIENT_ID_LEN);
                         untqty = sqlGetFloat(Res, Row, "pckqty");
                     }
                }
                else if (sqlGetBoolean(Res, Row, "lodflg") == BOOLEAN_TRUE &&
                         sqlGetBoolean(Res, Row, "prtflg") != BOOLEAN_TRUE &&
                         sqlGetBoolean(Res, Row, "qtyflg") == BOOLEAN_TRUE)
                {
                    misTrc(T_FLOW, "Special Handling for case picking "
                                   "inventory without specifying part");
                    /* Here we handle a specific scenario:
                     * This is a sub/detail level pick and we have the
                     * lodflg, qtyflg, and no prtflg for this pick.
                     * In this case we need to make sure the prtnum gets
                     * set out.  If it does not 'process inventory move'
                     * will only see a srclod, qty, and maybe dstlod making
                     * this look like a transfer vs a pick and will error
                     * with needs to relabel.  That is not valid for list
                     * picking. For Example: Picking 10 cases off a pallet
                     * (pallet has only 10 cases) as a subload pick, only
                     * scanning the srclod and qty, and this is the first
                     * pick in the list.  The dstlod will be entered but
                     * will not yet exists on the device resulting in the
                     * invalid relabel error.                     */
                    if (sqlGetNumRows(Res1) == 1)
                    {
                        misTrc(T_FLOW, "Special Handling for case picking: "
                                       "Setting prtnum to '%s'",
                                       sqlGetString(Res, Row, "prtnum"));
                        strncpy(prtnum,
                                        sqlGetString(Res, Row, "prtnum"),
                                        PRTNUM_LEN);
                        misTrc(T_FLOW, "Special Handling for case picking: "
                                       "Setting prt_client_id to '%s'",
                                       sqlGetString(Res, Row, "prt_client_id"));
                        strncpy(prt_client_id,
                                        sqlGetString(Res, Row, "prt_client_id"),
                                        CLIENT_ID_LEN);
                    }
                } 
                
                /* 
                 * All other values match, but let's see if the invsts matches.
                 * Remember now that we can match an entire progression.
                 */
                if (strncmp(sqlGetString(Res, Row, "invsts"),
                            sqlGetString(Res1, Row1, "invsts"),
                            INVSTS_LEN))
                {
                    /* If there is an invsts_prg, see if the invsts is valid */
                    if (!sqlIsNull(Res, Row, "invsts_prg"))
                    {
                        sprintf(sqlBuffer,
                                "select 1 "
                                "  from prgmst "
                                " where invsts_prg = '%s' "
                                "   and invsts = '%s' ",
                                sqlGetString(Res, Row, "invsts_prg"),
                                sqlGetString(Res1, Row1, "invsts"));

                        ret_status = sqlExecStr(sqlBuffer, NULL);
                        if (eOK != ret_status)
                        {
                            sqlFreeResults(Res);
                            sqlFreeResults(Res1);
                            return (srvResults(ePAL_INVSTS_NOTMATCH_PICK_PART, NULL));
                        }
                    }
                    else
                    {
                        sqlFreeResults(Res);
                        sqlFreeResults(Res1);
                        return (srvResults(ePAL_INVSTS_NOTMATCH_PICK_PART, NULL));
                    }
                }
            }
            else
            {
                if (strncmp(sqlGetString(Res, Row, "srcloc"),
                            sqlGetString(Res1, Row1, "stoloc"),
                            STOLOC_LEN) != 0)
                {
                    sqlFreeResults(Res);
                    sqlFreeResults(Res1);
                    return (srvResults(ePAL_SRCLOC_NOTMATCH_PICK_SRCLOC, NULL));
                }
                else
                if (strncmp(sqlGetString(Res, Row, "prtnum"),
                            sqlGetString(Res1, Row1, "prtnum"),
                            PRTNUM_LEN) != 0)
                {
                    sqlFreeResults(Res);
                    sqlFreeResults(Res1);
                    return (srvResults(ePAL_PART_NOTMATCH_PICK_PART, NULL));
                }
                else
                if (strncmp(sqlGetString(Res, Row, "prt_client_id"),
                            sqlGetString(Res1, Row1, "prt_client_id"),
                            CLIENT_ID_LEN) != 0)
                {
                    sqlFreeResults(Res);
                    sqlFreeResults(Res1);
                    return (srvResults(ePAL_CLIENT_NOTMATCH_PICK_CLIENT, NULL));
                }
                else
                if (sqlGetFloat(Res, Row, "pckqty") !=
                    sqlGetFloat(Res1, Row1, "untqty") &&
                   (sqlGetBoolean(Res, Row, "qtyflg") == BOOLEAN_FALSE) &&
                   (strncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0) &&
                    sqlGetFloat(Res, Row, "pck_catch_qty") <= 0)
                {
                    sqlFreeResults(Res);
                    sqlFreeResults(Res1);
                    return (srvResults(ePAL_QTY_NOTMATCH_PICK_QTY, NULL));
                }
                /* 
                 * All other values match, but let's see if the invsts matches.
                 * Remember now that we can match an entire progression.
                 */
                if (strncmp(sqlGetString(Res, Row, "invsts"),
                            sqlGetString(Res1, Row1, "invsts"),
                            INVSTS_LEN))
                {
                    /* If there is an invsts_prg, see if the invsts is valid */
                    if (!sqlIsNull(Res, Row, "invsts_prg"))
                    {
                        sprintf(sqlBuffer,
                                "select 1 "
                                "  from prgmst "
                                " where invsts_prg = '%s' "
                                "   and invsts = '%s' ",
                                sqlGetString(Res, Row, "invsts_prg"),
                                sqlGetString(Res1, Row1, "invsts"));

                        ret_status = sqlExecStr(sqlBuffer, NULL);
                        if (eOK != ret_status)
                        {
                            sqlFreeResults(Res);
                            sqlFreeResults(Res1);
                            return (srvResults(ePAL_INVSTS_NOTMATCH_PICK_PART, NULL));
                        }
                    }
                    else
                    {
                        sqlFreeResults(Res);
                        sqlFreeResults(Res1);
                        return (srvResults(ePAL_INVSTS_NOTMATCH_PICK_PART, NULL));
                    }
                }
            }
        }
        sqlFreeResults(Res1);

        /* Finally, see if we need any of the other validators */
        if (sqlGetBoolean(Res, Row, "qtyflg") == BOOLEAN_TRUE )
            qtyflg = BOOLEAN_FALSE;
        if (sqlGetBoolean(Res, Row, "locflg") == BOOLEAN_TRUE )
            locflg = BOOLEAN_FALSE;
        if (sqlGetBoolean(Res, Row, "prtflg") == BOOLEAN_TRUE )
            prtflg = BOOLEAN_FALSE;
        if (sqlGetBoolean(Res, Row, "catch_qty_flg") == BOOLEAN_TRUE )
            catch_flg = BOOLEAN_FALSE;

        sqlFreeResults(Res);
    }
    /* If we're in RF Undirected Transfer, get the prt/qty info. */
    else if (!strlen(dsplod) && !strlen(wrkref) && strlen(lodid))
    {

        /* The lodid entered may be a storage location or load(sub,detail) */
        /* identifier.  Get the load info according to what they entered. */

        if (!strlen(lodnum) &&
            !strlen(subnum) &&
            !strlen(dtlnum))
        {
            sprintf (tmpBuffer, "and invlod.stoloc = '%s'", stoloc);
        }

        sprintf(sqlBuffer,
                "select invlod.stoloc, invdtl.prtnum, "
                "       invdtl.prt_client_id, invdtl.invsts, "
                "       prtmst_view.catch_cod, "
                "       sum(invdtl.untqty) untqty, "
                "       sum(invdtl.catch_qty) catch_qty "
                "  from prtmst_view, invdtl, invsub, invlod "
                " where prtmst_view.prtnum = invdtl.prtnum "
                "   and prtmst_view.prt_client_id = invdtl.prt_client_id "
                "   and invlod.lodnum = invsub.lodnum "
                "   and invlod.wh_id  = prtmst_view.wh_id "
                "   and invsub.subnum = invdtl.subnum "
                "   and invlod.wh_id  = '%s' "
                "   %s "
                " group by invlod.wh_id, invlod.stoloc, invdtl.prtnum, "
                "          invdtl.prt_client_id, invdtl.invsts, "
                "           prtmst_view.catch_cod ",
                wh_id, tmpBuffer);
        ret_status = sqlExecStr(sqlBuffer, &Res);
        if (ret_status != eOK)
        {
            sqlFreeResults(Res);
            return (srvResults(ret_status, NULL));
        }
        if (sqlGetNumRows(Res) > 1)
        {
            sprintf (sqlBuffer, 
                     "get mls text where mls_id='%s' "
                     "and locale_id=@@locale_id",
                     "lbl_mixed_part");

            ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
            if (ret_status != eOK)
            {
                sprintf (prtnum, "lbl_mixed_part");
            }
            else
            {
                Res1 = srvGetResults(CurPtr);
                Row1 = sqlGetRow(Res1);
                strncpy (prtnum, sqlGetString(Res1,Row1,"mls_text"), PRTNUM_LEN);
            }
            srvFreeMemory (SRVRET_STRUCT, CurPtr);
            for (Row = sqlGetRow(Res); Row; Row = sqlGetNextRow(Row))
            {
                untqty = untqty + (long) sqlGetFloat(Res, Row, "untqty");
                if (!strncmp(CATCH_COD_RECEIVING,
                             sqlGetString(Res, Row, "catch_cod"),
                             CATCH_COD_LEN))
                {
                    catch_qty += sqlGetFloat(Res, Row, "catch_qty");
                    catch_flg = BOOLEAN_FALSE;
                }
            }
        }
        else
        {
            Row = sqlGetRow(Res);
            strncpy(prtnum, sqlGetString(Res, Row, "prtnum"), PRTNUM_LEN);
            strncpy(prt_client_id, 
                    sqlGetString(Res, Row, "prt_client_id"), 
                    CLIENT_ID_LEN);
            untqty = (long) sqlGetFloat(Res, Row, "untqty");
            if (!strncmp(CATCH_COD_RECEIVING,
                         sqlGetString(Res, Row, "catch_cod"),
                         CATCH_COD_LEN))
            {
                catch_qty = sqlGetFloat(Res, Row, "catch_qty");
                catch_flg = BOOLEAN_FALSE;
            }
        }
        sqlFreeResults(Res);
    }

    /* If the load level is load, and we have a permanent load */
    /* move down to sub-load level */
    if (strncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0 &&
        strlen(lodnum) && strlen(subnum))
    {
        sprintf(sqlBuffer,
                "select lodnum "
                "  from invlod "
                " where prmflg = %ld "
                "   and mvlflg = %ld "
                "   and lodnum = '%s' ",
                BOOLEAN_TRUE, BOOLEAN_FALSE, lodnum);
        ret_status = sqlExecStr(sqlBuffer, NULL);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return (srvResults(ret_status, NULL));
        }
        if (ret_status == eOK)
        {
            strncpy(lodlvl, LODLVL_SUB, LODLVL_LEN);
        }
    }
    /* If the load level is sub-load, and we have a permanent sub */
    /* move down to detail level */
    if (strncmp(lodlvl, LODLVL_SUB, LODLVL_LEN) == 0 &&
        strlen(subnum) && strlen(dtlnum))
    {
        sprintf(sqlBuffer,
                "select subnum "
                "  from invsub "
                " where prmflg = %ld "
                "   and mvsflg = %ld "
                "   and subnum = '%s' ",
                BOOLEAN_TRUE, BOOLEAN_FALSE, subnum);
        ret_status = sqlExecStr(sqlBuffer, NULL);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return (srvResults(ret_status, NULL));
        }
        if (ret_status == eOK)
        {
            strncpy(lodlvl, LODLVL_DTL, LODLVL_LEN);
        }
    }
    /* If the load level is load, then clear the sub and detail */
    /* or if sub-load, then clear the detail and allow a destination */
    /* This process of clearing these values is essential to */
    /* the process of full pallet or physical case picking. */
    if (strncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0)
    {
        memset(subnum, 0, sizeof(subnum));
        memset(dtlnum, 0, sizeof(dtlnum));
    }
    else if (strncmp(lodlvl, LODLVL_SUB, LODLVL_LEN) == 0)
    {
        memset(dtlnum, 0, sizeof(dtlnum));
        lodflg = BOOLEAN_FALSE;
    }
    else if (strncmp(lodlvl, LODLVL_DTL, LODLVL_LEN) == 0)
    {
        lodflg = BOOLEAN_FALSE;
    }

    /* Now we have the lodlvl and we can check if the operator input the
     * right inventory identify.
     */
    if ((strncmp(lodlvl, LODLVL_SUB, LODLVL_LEN) == 0)
        && phyflg && !strlen(subnum))
    {
        /* If they are picking physical cases, and the lodlvl is
         * Sub-Load (Case), then a subnum is a valid entry only.
         */
         return (srvResults(eINT_INVALID_SUB_SOURCE_ID, NULL));
    }
    if ((strncmp(lodlvl, LODLVL_DTL, LODLVL_LEN) == 0)
        && phdflg && !strlen(dtlnum))
    {
        /* If they are picking physical boxes, and the lodlvl is
         * Detail (Box), then a dtlnum is a valid entry only.
         */
         return (srvResults(eINT_INVALID_DTL_SOURCE_ID, NULL));
    }

/*-----------------------------------------------------------------*/
/*
  * Query out the inventory move type against the dtlnum
  */
  
    sprintf(sqlBuffer,
            "  get inventory move type "
            "where invtid = '%s' "
            "  and  wh_id = '%s' ",
            lodid,
            wh_id);
            
    invmov_typ_null = BOOLEAN_TRUE;
    ret_status = srvInitiateCommand(sqlBuffer, &CurPtr); 
    if (ret_status == eOK)
    {
        Res = srvGetResults(CurPtr);
        Row = sqlGetRow(Res);
        
        if (!sqlIsNull(Res, Row, "invmov_typ"))
        {
            misTrimcpy(invmov_typ,
                       sqlGetString(Res, Row, "invmov_typ"),
                       INVMOV_TYP_LEN);
            invmov_typ_null = BOOLEAN_FALSE;
        }        
    }
    srvFreeMemory (SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;
    
    if (invmov_typ_null == BOOLEAN_FALSE)
    {
        if (misCiStrncmp(invmov_typ,
                         INVMOV_TYP_PICK,
                         INVMOV_TYP_LEN) == 0)
        {
            wko_pick = BOOLEAN_FALSE;
            
            sprintf(sqlBuffer,
                    "[select wrkref "
                    "   from invdtl "
                    "  where dtlnum = '%s'] "
                    "|"
                    "[select wkonum "
                    "   from pckwrk "
                    "  where wrkref = @wrkref] ",
                    dtlnum);
                    
            ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
            if (ret_status == eOK)
            {
                Res = srvGetResults(CurPtr);
                Row = sqlGetRow(Res);
                
                if (!sqlIsNull(Res, Row, "wkonum"))
                    wko_pick = BOOLEAN_TRUE;
            }
            srvFreeMemory (SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
        }
    }
    

/*-----------------------------------------------------------------*/

    CurPtr = srvResultsInit (eOK,
            "lodnum",    COMTYP_CHAR, LODNUM_LEN,
            "subnum",    COMTYP_CHAR, SUBNUM_LEN,
            "dtlnum",    COMTYP_CHAR, DTLNUM_LEN,
            "lodlvl",    COMTYP_CHAR, LODLVL_LEN,
            "stoloc",    COMTYP_CHAR, STOLOC_LEN,
            "movprt",    COMTYP_CHAR, PRTNUM_LEN,
            "movprt_client_id", COMTYP_CHAR, CLIENT_ID_LEN,
            "movqty",    COMTYP_INT, sizeof(int),
            "cmpflg",    COMTYP_BOOLEAN, sizeof(moca_bool_t),
            "untqtypro", COMTYP_BOOLEAN, sizeof(moca_bool_t),
            "stolocpro", COMTYP_BOOLEAN, sizeof(moca_bool_t),
            "prtnumpro", COMTYP_BOOLEAN, sizeof(moca_bool_t),
            "dstlodpro", COMTYP_BOOLEAN, sizeof(moca_bool_t),
            "catch_qty", COMTYP_FLOAT,   sizeof(double),
            "catch_qty_pro", COMTYP_BOOLEAN, sizeof(moca_bool_t),
            "invmov_typ", COMTYP_CHAR, INVMOV_TYP_LEN,
            "wko_pick", COMTYP_BOOLEAN, sizeof(moca_bool_t),
            NULL);

    ret_status = srvResultsAdd (CurPtr,
                                lodnum,
                                subnum,
                                dtlnum,
                                lodlvl,
                                stoloc,
                                prtnum,
                                prt_client_id,
                                untqty,
                                cmpflg,
                                qtyflg,
                                locflg,
                                prtflg,
                                lodflg,
                                catch_qty,
                                catch_flg,
                                invmov_typ_null? NULL: invmov_typ,
                                wko_pick);

    /* Normal successful completion */

    return (CurPtr);

}

