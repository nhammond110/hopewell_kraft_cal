static const char *rcsid = "$Id: intProcessInventoryStatusChange.c 153926 2008-04-25 08:44:46Z rguo $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2004 RedPrairie Corporation. All rights reserved
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "intlib.h"

/*
 * HISTORY
 * JJS 02/23/2009 - Allow inventory status changes "backwards" within
 *                  an aging profile
 *
 */

typedef struct _locinfo
{
    char stoloc[STOLOC_LEN+1];
    char wh_id[WH_ID_LEN+1];
    struct _locinfo *next;
}LOC_INFO;

static char *hold_invsts = NULL;

static long sChangeStatus(char        *to_invsts,
                          char        *res_invsts,
                          long         eval_dtcflg,
                          long         validate_invsum,
                          mocaDataRes *res,
                          mocaDataRow *row)
{
    RETURN_STRUCT *CmdPtr;
    mocaDataRes   *tmpres;
    mocaDataRow   *tmprow;
    char           buffer[4096];
    long           status;
    long           tmp_status;

    memset(res_invsts, 0, sizeof(res_invsts));

    if (!sqlIsNull(res, row, "dtlnum"))
    {
        /* 
         * If they are changing FROM a hold invsts, check to make sure there
         * are no more holds applied to this detail.
         */
        if (!strncmp(hold_invsts, sqlGetString(res, row, "invsts"), INVSTS_LEN))
        {
            misTrc(T_FLOW,
                   "Attempting to change FROM a HOLD inventory status. "
                   "Making sure they can do this.");

            sprintf(buffer,
                    "select 'x' "
                    "  from dual "
                    " where exists "
                    " (select 'x' "
                    "    from invhld "
                    "   where dtlnum = '%s') ",
                    sqlGetString(res, row, "dtlnum"));

            status = sqlExecStr(buffer, NULL);
            if (eOK == status)
            {
                return(eHOLD_EXISTS_ON_INV);
            }
        }

        /*
         * JJS 02/23/2009 - Hopewell wants to be allowed to go "backwards"
         *                  in the aging profile.  Standard product does not
         *                  allow this, but if we circumvent the if clause
         *                  below, then we can allow it.
         */
        if (!sqlIsNull(res, row, "age_pflnam") && BOOLEAN_FALSE)
        {
            /* It is date controlled, so there are special circumstances */
            misTrc(T_FLOW,
                   "Detail %s is date controlled.  Checking inventory "
                   "status for validity in the aging profile %s.",
                   sqlGetString(res, row, "dtlnum"),
                   sqlGetString(res, row, "age_pflnam"));

            sprintf(buffer,
                    "  get inventory status "
                    "where mandte = '%s' "
                    "  and expire_dte = '%s' "
                    "  and age_pflnam = '%s' "
                    "  and cur_invsts = '%s' "
                    "  and new_invsts = '%s' ",
                    sqlGetString(res, row, "mandte"),
                    sqlGetString(res, row, "expire_dte"),
                    sqlGetString(res, row, "age_pflnam"),
                    eval_dtcflg ? "" : sqlGetString(res, row, "invsts"),
                    eval_dtcflg ? "" : to_invsts);

            status = srvInitiateInline(buffer, &CmdPtr);
            if (eOK != status)
            {
                srvFreeMemory(SRVRET_STRUCT, CmdPtr);
                return (status);
            }

            /* 
             * Compare the result with the to_invsts.  If they are the same,
             * things are OK and we can change it.  If they aren't the same,
             * we have to check the NEW inventory status to see if it is 
             * valid.
             *
             * If the eval_dtcflg is TRUE, we will just take the results of
             * the first retrieval and make the new inventory status that
             * status.
             */
            tmpres = srvGetResults(CmdPtr);
            tmprow = sqlGetRow(tmpres);
            if (eval_dtcflg || 
                !strncmp(sqlGetString(tmpres, tmprow, "new_invsts"),
                         to_invsts,
                         INVSTS_LEN))
            {
                misTrimcpy(res_invsts, 
                           sqlGetString(tmpres, tmprow, "new_invsts"),
                           INVSTS_LEN);
            }
            else 
            {
                srvFreeMemory(SRVRET_STRUCT, CmdPtr);
                misTrc(T_FLOW,
                       "Cannot change inventory status for detail %s to "
                       "%s due to an aging profile restriction.",
                       sqlGetString(res, row, "dtlnum"),
                       to_invsts);
                return(eINVSTS_CHANGE_RESTRICT_AGEPFL);
            }
            srvFreeMemory(SRVRET_STRUCT, CmdPtr);
            CmdPtr = NULL;
        }
        else
        {
            misTrimcpy(res_invsts, to_invsts, INVSTS_LEN);
        }

        /* If we got to this point, we can update the status */
        misTrc(T_FLOW,
               "Updating the inventory status for detail %s to %s.",
               sqlGetString(res, row, "dtlnum"),
               res_invsts);

        if (sqlGetBoolean(res, row, "in_transit_flg") == BOOLEAN_FALSE)
            sprintf(buffer,
                    "update invdtl "
                    "   set invsts = '%s' "
                    " where dtlnum = '%s' ",
                    res_invsts,
                    sqlGetString(res, row, "dtlnum"));
        else
            sprintf(buffer,
                    "update inv_intransit "
                    "   set invsts = '%s' "
                    " where dtlnum = '%s' ",
                    res_invsts,
                    sqlGetString(res, row, "dtlnum"));
                    

        status = sqlExecStr(buffer, NULL);

        if (validate_invsum && eOK == status)
        {
            /* We have to re-evaluate the inventory summary entries now */
            sprintf(buffer,
                    "validate inventory summary for location "
                    "   where begloc = '%s' "
                    "     and wh_id =  '%s' "
                    "     and lckmod = 'NONE' "
                    "| "
                    "if (@action != '') "
                    "execute server command "
                    "  where cmd = @action ",
                    sqlGetString(res, row, "stoloc"),
                    sqlGetString(res, row, "wh_id"));

            tmp_status = srvInitiateCommand(buffer, NULL);
            /* Don't really care about this status */
        }
    }
    else 
    {
        misTrimcpy(res_invsts, to_invsts, INVSTS_LEN);
        misTrc(T_FLOW,
               "Updating inventory status for work order "
               "%s/%s/%s line %s to %s.",
               sqlGetString(res, row, "wkonum"),
               sqlGetString(res, row, "client_id"),
               sqlGetString(res, row, "wkorev"),
               sqlGetString(res, row, "wkolin"),
               res_invsts);

        /* We have a work order line, so just change the status. */
        sprintf(buffer,
                "update wkodtl "
                "   set invsts = '%s' "
                " where wkonum = '%s' "
                "   and client_id = '%s' "
                "   and wkorev = '%s' "
                "   and wkolin = '%s' "
                "   and seqnum = %ld  "
                "   and wh_id  = '%s' ",
                res_invsts,
                sqlGetString(res, row, "wkonum"),
                sqlGetString(res, row, "client_id"),
                sqlGetString(res, row, "wkorev"),
                sqlGetString(res, row, "wkolin"),
                sqlGetLong(res, row, "seqnum"),
                sqlGetString(res, row, "wh_id"));

        status = sqlExecStr(buffer, NULL);
    }

    return(status);
}

static void sAddToReturn(long           status, 
                         char          *to_invsts,
                         char          *reacod,
                         RETURN_STRUCT *RetPtr,
                         mocaDataRes   *res,
                         mocaDataRow   *row)
{
    srvResultsAdd(RetPtr,
                  status,
                  sqlGetString(res, row, "bldg_id"),
                  sqlGetString(res, row, "arecod"),
                  sqlGetString(res, row, "stoloc"),
                  sqlGetString(res, row, "wh_id"),
                  sqlGetString(res, row, "lodnum"),
                  sqlGetString(res, row, "subnum"),
                  sqlGetString(res, row, "dtlnum"),
                  sqlGetString(res, row, "prtnum"),
                  sqlGetString(res, row, "prt_client_id"),
                  sqlGetString(res, row, "wkonum"),
                  sqlGetString(res, row, "client_id"),
                  sqlGetString(res, row, "wkorev"),
                  sqlGetString(res, row, "wkolin"),
                  sqlGetLong(res, row, "seqnum"),
                  sqlGetLong(res, row, "untqty"),
                  sqlGetString(res, row, "invsts"),
                  to_invsts,
                  sqlGetString(res, row, "lotnum"),
                  sqlGetString(res, row, "revlvl"),
                  sqlGetString(res, row, "orgcod"),
                  reacod,
                  sqlGetFloat(res, row, "catch_qty")
                  );

}

static LOC_INFO *sAddLocToValidateList(LOC_INFO *locptr_top,
                                           char *stoloc,
                                           char *wh_id)
{
    LOC_INFO *ptr, *endptr;

    /* If no stoloc was sent in, just return what was sent to us. */
    if((misTrimLen(stoloc, STOLOC_LEN)==0)
        && (misTrimLen(stoloc, STOLOC_LEN)==0))
        return(locptr_top);

    /* If the pointer was sent in NULL, then we have no list yet...
     * so lets start one here.
     * */
    if(locptr_top == NULL)
    {
        ptr = (LOC_INFO *)calloc(1, sizeof(LOC_INFO));
        memset(ptr->stoloc, 0, sizeof(ptr->stoloc));
        misTrimcpy(ptr->stoloc, stoloc, STOLOC_LEN);
        memset(ptr->wh_id, 0, sizeof(ptr->wh_id));
        misTrimcpy(ptr->wh_id, wh_id, WH_ID_LEN);
        ptr->next = NULL;
        return(ptr); /* List started... we can get out at this point. */
    }
    
    for(ptr = locptr_top; ptr; ptr = ptr->next)
    {
        if((!misTrimStrncmp(ptr->stoloc, stoloc, STOLOC_LEN))
            && (!misTrimStrncmp(ptr->wh_id, wh_id, WH_ID_LEN)))
        {
            return(locptr_top);
        }
        endptr = ptr; /* save this in case this is the end of the list. */
    }
    
    /* If we fell through the loop, we don't have this stoloc in the list.
     * We need to add it.
     * */
    ptr = (LOC_INFO *)calloc(1, sizeof(LOC_INFO));
    endptr->next = ptr;
    memset(ptr->stoloc, 0, sizeof(ptr->stoloc));
    misTrimcpy(ptr->stoloc, stoloc, STOLOC_LEN);
    memset(ptr->wh_id, 0, sizeof(ptr->wh_id));
    misTrimcpy(ptr->wh_id, wh_id, WH_ID_LEN);
    ptr->next = NULL;
    return(locptr_top);
}

static long sValidateChangedLocations(long validate_invsum,
                                       LOC_INFO *locptr_top)
{
    LOC_INFO *locptr;
    char buffer[2000];
    long status;
    
    if(locptr_top == NULL)
        return(eOK);  /* Nothing to do.  Just return. */

    if(validate_invsum == 1)
    {
        /* To avoid running the validate location many times on
         * a single location, we saved off the locations and 
         * will now validate each location only once. 
         * */
        for(locptr = locptr_top; locptr; locptr = locptr->next)
        {
            /* We have to re-evaluate the inventory 
             * summary entries now. */
            sprintf(buffer,
                    "validate inventory summary for location "
                    "   where begloc = '%s' "
                    "     and wh_id = '%s' "
                    "     and lckmod = 'NONE' "
                    "| "
                    "if (@action != '') "
                    "execute server command "
                    "  where cmd = @action ",
                    locptr->stoloc,
                    locptr->wh_id);
            status = srvInitiateCommand(buffer, NULL);
            /* Don't really care about this status */
        }
    }

    /* We need to make sure that we free the linked list. */
    for(locptr = locptr_top; locptr; locptr_top = locptr)
    {
        locptr=locptr->next;
        free(locptr_top);
    }
    return(eOK);
}

LIBEXPORT
RETURN_STRUCT *varProcessInventoryStatusChange(char *to_invsts_i,
                                               char *prc_hldpfx_i,
                                               char *prc_hldnum_i,
                                               char *dtlnum_i,
                                               char *wkonum_i,
                                               char *client_id_i,
                                               char *wkorev_i,
                                               char *wkolin_i,
                                               long *seqnum_i,
                                               char *dtlnum_list_i,
                                               char *wkonum_list_i,
                                               char *client_id_list_i,
                                               char *wkorev_list_i,
                                               char *wkolin_list_i,
                                               char *seqnum_list_i,
                                               moca_bool_t *eval_dtcflg_i,
                                               moca_bool_t *validate_invsum_i,
                                               char *prc_reacod_i,
                                               char *wh_id_i)
{
    RETURN_STRUCT *CmdPtr;
    RETURN_STRUCT *RetPtr;
    mocaDataRes   *res;
    mocaDataRow   *row;
    
    LOC_INFO      *locptr = NULL;
    LOC_INFO      *locptr_top = NULL;

    char           to_invsts[INVSTS_LEN + 1];
    char           res_invsts[INVSTS_LEN + 1];

    char          *dtlnum_list, *dtlnum_ptr, *dtlnum_save;
    char          *wkonum_list, *wkonum_ptr;
    char          *client_id_list, *client_id_ptr;
    char          *wkorev_list, *wkorev_ptr;
    char          *wkolin_list, *wkolin_ptr;
    char          *seqnum_list, *seqnum_ptr;
    char           client_id[CLIENT_ID_LEN + 1];
    char           prc_reacod[REACOD_LEN + 1];
    char           wh_id[WH_ID_LEN + 1];
    long           seqnum;
    long           eval_dtcflg;
    long           validate_invsum;

    char           savepoint[100];
    char           buffer[2048];
    long           status;
    
    /*
     *---------------------------------------------------------------------
     * Format the incoming variables 
     *---------------------------------------------------------------------
     */

    memset(to_invsts, 0, sizeof(to_invsts));
    memset(buffer, 0, sizeof(buffer));
    memset(prc_reacod, 0, sizeof(prc_reacod));
    memset(wh_id, 0, sizeof(wh_id));

    /* Get the hold_invsts if we haven't yet */
    if (!hold_invsts)
    {
        sprintf(buffer,
                "get hold inventory status ");

        status = srvInitiateCommand(buffer, &CmdPtr);

        if (eOK != status)
        {
            return(CmdPtr);
        }

        res = srvGetResults(CmdPtr);
        row = sqlGetRow(res);
        
        hold_invsts = calloc(sizeof(char), INVSTS_LEN + 1);
        if (!hold_invsts)
        {
            srvFreeMemory(SRVRET_STRUCT, CmdPtr);
            return(srvResults(eNO_MEMORY, NULL));
        }
        /* Flag it to MOCA so it is freed when we stop the server */
        misFlagCachedMemory((OSFPTR) free, hold_invsts);

        misTrimcpy(hold_invsts, 
                   sqlGetString(res, row, "hold_invsts"), 
                   INVSTS_LEN);
        srvFreeMemory(SRVRET_STRUCT, CmdPtr);
        CmdPtr = NULL;
    }

    CmdPtr = RetPtr = NULL;
    res = NULL;
    dtlnum_list = wkonum_list = client_id_list = wkorev_list = wkolin_list = 
        seqnum_list = dtlnum_save = NULL;

    /* 
     * We will not re-evaluate the inventory status for date control parts
     * by default.
     */
    eval_dtcflg = eval_dtcflg_i ? *eval_dtcflg_i : 0;

    /* We will by default revalidate the storage location */
    validate_invsum = validate_invsum_i ? *validate_invsum_i : 1;

    if (!to_invsts_i || !misTrimLen(to_invsts_i, INVSTS_LEN))
        return(APPMissingArg("to_invsts"));
    misTrimcpy(to_invsts, to_invsts_i, INVSTS_LEN);
    
    if (prc_reacod_i && misTrimLen(prc_reacod_i, REACOD_LEN))
        misTrimcpy (prc_reacod, prc_reacod_i, REACOD_LEN);
        
    if (wh_id_i || misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
        
    /* 
     * If the new invsts is the HOLD one, they have to provide the HOLD 
     * NUMBER to which it is being applied.  We don't validate it here, but
     * just make sure they are indeed changing it through hold processing
     * and not just by chance changing it to HOLD.
     */
    if (!strncmp(to_invsts, hold_invsts, INVSTS_LEN))
    {
        if (!prc_hldpfx_i || !misTrimLen(prc_hldpfx_i, HLDPFX_LEN) ||
            !prc_hldnum_i || !misTrimLen(prc_hldnum_i, HLDNUM_LEN))
            return(srvResults (eINT_HLDNUM_REQUIRED, NULL));
    }

    /* The discrete parameter takes precendence over the list */
    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
    {
        /* Allocate the memory */
        dtlnum_list = (char *) calloc(1, strlen(dtlnum_i) + 1);

        /* 
         * Since the routine I'm using the iterate through this structure
         * is destructive, I have to save a pointer to it for freeing later.
         */
        dtlnum_save = dtlnum_list;

        strcpy(dtlnum_list, dtlnum_i);
        dtlnum_ptr = dtlnum_list - 1;
    }
    else if(dtlnum_list_i && misTrimLen(dtlnum_list_i, DTLNUM_LEN))
    {
        dtlnum_list = (char *) calloc(1, strlen(dtlnum_list_i) + 1);

        /* 
         * Since the routine I'm using the iterate through this structure
         * is destructive, I have to save a pointer to it for freeing later.
         */
        dtlnum_save = dtlnum_list;

        strcpy(dtlnum_list, dtlnum_list_i);
        dtlnum_ptr = dtlnum_list - 1;
    }

    if (wkonum_i && misTrimLen(wkonum_i, WKONUM_LEN))
    {
        /* Allocate memory to copy to it */
        wkonum_list = (char *) calloc(1, strlen(wkonum_i) + 1);
        strcpy(wkonum_list, wkonum_i);

        if (!wkorev_i || !misTrimLen(wkorev_i, WKOREV_LEN))
        {
            if (dtlnum_list)
                free(dtlnum_list);
            free(wkonum_list);
            return(APPMissingArg("wkorev"));
        }

        wkorev_list = (char *) calloc(1, strlen(wkorev_i) + 1);
        strcpy(wkorev_list, wkorev_i);

        if (!wkolin_i || !misTrimLen(wkolin_i, WKOLIN_LEN))
        {
            if (dtlnum_list)
                free(dtlnum_list);
            free(wkonum_list);
            free(wkorev_list);
            return(APPMissingArg("wkolin"));
        }

        wkolin_list = (char *) calloc(1, strlen(wkolin_i) + 1);
        strcpy(wkolin_list, wkolin_i);

        if(client_id_i && misTrimLen(client_id_i, CLIENT_ID_LEN))
        {
            client_id_list = (char *) calloc(1, strlen(client_id_i) + 1);
            strcpy(client_id_list, client_id_i);
        }

        if (!seqnum_i)
        {
            if (dtlnum_list)
                free(dtlnum_list);
            free(wkonum_list);
            free(wkorev_list);
            free(wkolin_list);
            return(APPMissingArg("seqnum"));
        }

        sprintf(buffer, "%ld", *seqnum_i);
        seqnum_list = (char *) calloc(1, strlen(buffer) + 1);
        strcpy(seqnum_list, buffer);

    }
    else if(wkonum_list_i && misTrimLen(wkonum_list_i, WKONUM_LEN))
    {
        wkonum_list = (char *) calloc(1, strlen(wkonum_list_i) + 1);
        strcpy(wkonum_list, wkonum_list_i);

        if (!wkorev_list_i || !misTrimLen(wkorev_list_i, WKOREV_LEN))
        {
            if (dtlnum_list)
                free(dtlnum_list);
            free(wkonum_list);
            return(APPMissingArg("wkorev"));
        }

        wkorev_list = (char *) calloc(1, strlen(wkorev_list_i) + 1);
        strcpy(wkorev_list, wkorev_list_i);

        if (!wkolin_list_i || !misTrimLen(wkolin_list_i, WKOLIN_LEN))
        {
            if (dtlnum_list)
                free(dtlnum_list);
            free(wkonum_list);
            free(wkorev_list);
            return(APPMissingArg("wkolin"));
        }

        wkolin_list = (char *) calloc(1, strlen(wkolin_list_i) + 1);
        strcpy(wkolin_list, wkolin_list_i);

        if (client_id_list_i && misTrimLen(client_id_list_i, CLIENT_ID_LEN))
        {
            client_id_list = (char *) calloc(1, strlen(client_id_list_i) + 1);
            strcpy(client_id_list, client_id_list_i);
        }

        if (!seqnum_list_i)
        {
            if (dtlnum_list)
                free(dtlnum_list);
            free(wkonum_list);
            free(wkorev_list);
            free(wkolin_list);
            return(APPMissingArg("seqnum"));
        }

        seqnum_list = (char *) calloc(1, strlen(seqnum_list_i) + 1);
        strcpy(seqnum_list, seqnum_list_i);
    }

    /* Setup the return structure, as it will be the same for all cases */
    RetPtr = srvResultsInit(eOK, 
                            "exec_sts",      COMTYP_INT, sizeof(long),
                            "bldg_id",       COMTYP_CHAR, BLDG_ID_LEN,
                            "arecod",        COMTYP_CHAR, ARECOD_LEN,
                            "stoloc",        COMTYP_CHAR, STOLOC_LEN,
                            "wh_id",         COMTYP_CHAR, WH_ID_LEN,
                            "lodnum",        COMTYP_CHAR, LODNUM_LEN,
                            "subnum",        COMTYP_CHAR, SUBNUM_LEN,
                            "dtlnum",        COMTYP_CHAR, DTLNUM_LEN,
                            "prtnum",        COMTYP_CHAR, PRTNUM_LEN,
                            "prt_client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                            "wkonum",        COMTYP_CHAR, WKONUM_LEN,
                            "client_id",     COMTYP_CHAR, CLIENT_ID_LEN,
                            "wkorev",        COMTYP_CHAR, WKOREV_LEN,
                            "wkolin",        COMTYP_CHAR, WKOLIN_LEN,
                            "seqnum",        COMTYP_INT,  sizeof(long),
                            "trnqty",        COMTYP_INT,  sizeof(long),
                            "fr_invsts",     COMTYP_CHAR, INVSTS_LEN,
                            "to_invsts",     COMTYP_CHAR, INVSTS_LEN,
                            "lotnum",        COMTYP_CHAR, LOTNUM_LEN,
                            "revlvl",        COMTYP_CHAR, REVLVL_LEN,
                            "orgcod",        COMTYP_CHAR, ORGCOD_LEN,
                            "reacod",        COMTYP_CHAR, REACOD_LEN,
			    "catch_qty",     COMTYP_FLOAT, sizeof(double),
                            NULL);

    if (!dtlnum_list && !wkonum_list)
    {
        /* 
         * Neither detail was passed, so we have to call "list inventory change
         * information" to get the details we are supposed to change.
         */

        sprintf(buffer, " list inventory change information ");

        status = srvInitiateInline(buffer, &CmdPtr);
        if (eOK != status)
        {
            srvFreeMemory(SRVRET_STRUCT, RetPtr);
            return(CmdPtr);
        }

        /* Loop around the details and perform the change */
        res = srvGetResults(CmdPtr);
        for(row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            if (!sqlIsNull(res, row, "dtlnum"))
            {
                sprintf(savepoint, 
                        "INVSTS%s",
                        sqlGetString(res, row, "dtlnum"));
            }
            else
            {
                sprintf(savepoint, 
                        "INVSTS%s",
                        sqlGetString(res, row, "wkonum"));
            }
            
            status = sqlSetSavepoint(savepoint);

            status = sChangeStatus(to_invsts,
                                   res_invsts,
                                   eval_dtcflg,
                                   0, /* don't validate_invsum yet. */
                                   res,
                                   row);

            sAddToReturn(status, 
                         res_invsts,
                         prc_reacod,
                         RetPtr,
                         res,
                         row);

            if (eOK != status)
            {
                status = sqlRollbackToSavepoint(savepoint);
            }
            else if (validate_invsum)
            {
                locptr_top = sAddLocToValidateList(locptr_top,
                                               sqlGetString(res,row,"stoloc"),
                                               sqlGetString(res,row,"wh_id"));
            }
        }
        
        if(validate_invsum && locptr_top != NULL)
        {
            /* To avoid running the validate location many times on
             * a single location, we saved off the locations and 
             * will now validate each location only once. 
             * */
            status = sValidateChangedLocations(validate_invsum, locptr_top);
        }
        
        srvFreeMemory(SRVRET_STRUCT, CmdPtr);
        CmdPtr = NULL;
        res = NULL;
    }

    if (dtlnum_list)
    {
        /* Process the detail numbers */
        for (dtlnum_ptr = misStrsep(&dtlnum_list, ","); 
             dtlnum_ptr; 
             dtlnum_ptr = misStrsep(&dtlnum_list, ","))
        {
            /* 
             * For each detail number, get all of the information from the 
             * list command so that we can populate the result set
             */
            sprintf(buffer,
                    " list inventory change information "
                    "where dtlnum = '%s' ",
                    dtlnum_ptr);

            status = srvInitiateCommand(buffer, &CmdPtr);
            if (eOK != status)
            {
                free(dtlnum_save);
                srvFreeMemory(SRVRET_STRUCT, RetPtr);
                if (wkonum_list) free(wkonum_list);
                if (client_id_list) free(client_id_list);
                if (wkorev_list) free(wkorev_list);
                if (wkolin_list) free(wkolin_list);
                if (seqnum_list) free(seqnum_list);
                return(CmdPtr);
            }
            /* Loop around the details and perform the change */
            res = srvGetResults(CmdPtr);
            for(row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                sprintf(savepoint,
                        "INVSTS%s",
                        sqlGetString(res, row, "dtlnum"));
                status = sqlSetSavepoint(savepoint);

                status = sChangeStatus(to_invsts,
                                       res_invsts,
                                       eval_dtcflg,
                                       0, /* don't validate_invsum yet. */
                                       res,
                                       row);

                sAddToReturn(status, 
                             res_invsts,
                             prc_reacod,
                             RetPtr,
                             res,
                             row);

                if (eOK != status)
                {
                    status = sqlRollbackToSavepoint(savepoint);
                }
                else if(validate_invsum)
                {
                    locptr_top = sAddLocToValidateList(locptr_top,
                                                sqlGetString(res,row,"stoloc"),
                                                sqlGetString(res,row,"wh_id"));
                }
            }
            
            srvFreeMemory(SRVRET_STRUCT, CmdPtr);
            CmdPtr = NULL;
            res = NULL;
        }
        if(validate_invsum && locptr_top != NULL)
        {
            /* To avoid running the validate location many times on
             * a single location, we saved off the locations and 
             * will now validate each location only once. 
             * */
            status = sValidateChangedLocations(validate_invsum, locptr_top);
        }
    }

    if (wkonum_list)
    {
        /* Process the work orders */
        /* First, figure out how many parameters were passed */
        for (wkonum_ptr = misStrsep(&wkonum_list, ","); 
             wkonum_ptr; 
             wkonum_ptr = misStrsep(&wkonum_list, ","))
        {
            wkorev_ptr = misStrsep(&wkorev_list, ",");
            wkolin_ptr = misStrsep(&wkolin_list, ",");
            seqnum_ptr = misStrsep(&seqnum_list, ",");
            if (client_id_list_i && misTrimLen(client_id_list_i, CLIENT_ID_LEN))
            {
                client_id_ptr = misStrsep(&client_id_list, ",");
            }
            else
            {
                client_id_ptr = client_id_i;
            }

            seqnum = atoi(seqnum_ptr);

            status = appGetClient(client_id_ptr, client_id);
            if (eOK != status)
            {
                srvFreeMemory(SRVRET_STRUCT, RetPtr);
                if (dtlnum_save) free(dtlnum_save);
                if (wkonum_list) free(wkonum_list);
                if (client_id_list) free(client_id_list);
                if (wkorev_list) free(wkorev_list);
                if (wkolin_list) free(wkolin_list);
                if (seqnum_list) free(seqnum_list);
                return(srvResults(status, NULL));
            }

            /* 
             * Now call the list command to get the stuff we need.
             * NOTE: This is all crazy because we can't outer join to
             * two tables in succsesion in SQL Server in the same manner
             * as we can with Oracle, so this was the trade-off.
             */
            sprintf(buffer,
                    " publish data "
                    "  where lodnum = null"
                    "    and subnum = null "
                    "    and dtlnum = null "
                    " | "
                    "[select wd.prcloc, "
                    "        wd.prtnum, "
                    "        wd.prt_client_id, "
                    "        wh.wkonum, "
                    "        wh.client_id, "
                    "        wh.wh_id,  "
                    "        wh.wkorev, "
                    "        wd.wkolin, "
                    "        wd.seqnum, "
                    "        wd.tot_dlvqty trnqty, "
                    "        wd.invsts "
                    "   from wkohdr wh, "
                    "        wkodtl wd "
                    "  where wh.wkonum = wd.wkonum "
                    "    and wh.client_id = wd.client_id "
                    "    and wh.wh_id = wd.wh_id "
                    "    and wh.wkorev = wd.wkorev "
                    "    and wd.wkonum = '%s' "
                    "    and wd.wh_id = '%s' "
                    "    and wd.client_id = '%s' "
                    "    and wd.wkorev = '%s' "
                    "    and wd.wkolin = '%s' "
                    "    and wd.seqnum = %ld ] "
                    " | "
                    " if (@prcloc = '') "
                    " { "
                    "    publish data "
                    "      where bldg_id = '' and aremst = '' and stoloc = '' "
                    "        and lodnum = @lodnum and subnum = @subnum "
                    "        and dtlnum = @dtlnum and prtnum = @prtnum "
                    "        and prt_client_id = @prt_client_id "
                    "        and wkonum = @wkonum and client_id = @client_id "
                    "        and wkorev = @wkorev and wkolin = @wkolin "
                    "        and seqnum = @seqnum and trnqty = @trnqty "
                    "        and invsts = @invsts and wh_id = @wh_id "
                    " } "
                    " else "
                    " { "
                    "    [select am.bldg_id, am.arecod, lm.stoloc,"
                    "            @lodnum lodnum, @subnum subnum, "
                    "            @dtlnum dtlnum, @prtnum prtnum, "
                    "            @prt_client_id prt_client_id, "
                    "            @wkonum wkonum, @client_id client_id, "
                    "            @wkorev wkorev, @wkolin wkolin, "
                    "            @seqnum seqnum, @trnqty trnqty, "
                    "            @invsts invsts, lm.wh_id "
                    "       from aremst am, locmst lm "
                    "      where am.arecod = lm.arecod "
                    "        and am.wh_id = lm.wh_id "
                    "        and lm.stoloc = @prcloc "
                    "        and lm.wh_id = @wh_id] "
                    " } ",
                    wkonum_ptr,
                    wh_id,
                    client_id,
                    wkorev_ptr,
                    wkolin_ptr,
                    seqnum);

            status = srvInitiateCommand(buffer, &CmdPtr);
            if (eOK != status)
            {
                if (dtlnum_save) free(dtlnum_save);
                srvFreeMemory(SRVRET_STRUCT, RetPtr);
                if (wkonum_list) free(wkonum_list);
                if (client_id_list) free(client_id_list);
                if (wkorev_list) free(wkorev_list);
                if (wkolin_list) free(wkolin_list);
                if (seqnum_list) free(seqnum_list);
                return(CmdPtr);
            }
            /* There will only be one detail, so process it */
            res = srvGetResults(CmdPtr);
            row = sqlGetRow(res);

            sprintf(savepoint,
                    "INVSTS%s",
                    sqlGetString(res, row, "wkonum"));
            status = sqlSetSavepoint(savepoint);

            status = sChangeStatus(to_invsts,
                                   res_invsts,
                                   eval_dtcflg,
                                   0, /* don't validate_invsum yet. */
                                   res,
                                   row);

            sAddToReturn(status, 
                         res_invsts,
                         prc_reacod,
                         RetPtr,
                         res,
                         row);

            if (eOK != status)
            {
                status = sqlRollbackToSavepoint(savepoint);
            }
            else if(validate_invsum)
            {
                locptr_top = sAddLocToValidateList(locptr_top,
                                            sqlGetString(res,row,"stoloc"),
                                            sqlGetString(res,row,"wh_id"));
            }

            srvFreeMemory(SRVRET_STRUCT, CmdPtr);
            CmdPtr = NULL;
            res = NULL;
        }
        if(validate_invsum && locptr_top != NULL)
        {
            /* To avoid running the validate location many times on
             * a single location, we saved off the locations and 
             * will now validate each location only once. 
             * */
            status = sValidateChangedLocations(validate_invsum, locptr_top);
        }
    }

    /* Free up all the memory we've allocated and leave */
    if (dtlnum_save) free(dtlnum_save);
    if (wkonum_list) free(wkonum_list);
    if (client_id_list) free(client_id_list);
    if (wkorev_list) free(wkorev_list);
    if (wkolin_list) free(wkolin_list);
    if (seqnum_list) free(seqnum_list);

    return(RetPtr);
}
