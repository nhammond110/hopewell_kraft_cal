static char *rcsid = "$Id: pckAllocateEmergencyReplenishment.c 167775 2008-09-01 12:17:53Z kzhao $";
/*#START***********************************************************************
*  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
*
*      Purpose:  This routine is called to complete any outstanding 
*                replenishments which now have product in the appropriate
*                areas for allocation.
*
*#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <applib.h>
#include <dcserr.h>
#include <dcsgendef.h>
#include <dcscolwid.h>
#include <math.h>

#include "pckRPLlib.h"

/*
 * HISTORY
 * JJS 08/21/2009 - Code to stop "allocate inventory and location" from running
 *                  multiple times (specifically, created and used the new
 *                  utility function sSaveOrCheckSkipAllocInvAndLoc()).
 *
 *
 */

static long     ErrorLevel;
static char     ErrorText[RPLMSG_LEN + 1];

#define _ERROR_UNKNOWN            10
#define _ERROR_NO_INVENTORY 20
#define _ERROR_NO_LOCATIONS 30
#define _ERROR_PRODUCTION_LINE_BUSY 40

static void sSetError(long level, char *text)
{
    if (ErrorLevel < level)
    {
        ErrorLevel = level;
        if (text && misTrimLen(text, 10))
            strncpy(ErrorText, text, sizeof(ErrorText) - 1);
        else
            memset(ErrorText, 0, sizeof(ErrorText));
    }
}

static void sClearError(void)
{
    ErrorLevel = 0;
    memset(ErrorText, 0, sizeof(ErrorText));
}



/*
*  This routine attempts to process the replenishment request for the
*  order.  This is a "last ditch", avoid a replenishment, type of thing.
*  Normally, this will generally fail, but doing the check before firing
*  the replenishment can sometimes save the work.
*/
static long sPrcReplenDemand(mocaDataRes *res, 
                             mocaDataRow *row, 
                             long *processed,
                             double *catch_qty_processed)
{
    char            pick_type[20];
    char            srv_command[2000];
    char            buffer[1000];
    char            srcare[ARECOD_LEN + 1];
    char            prtnum[PRTNUM_LEN + 1];
    char            prt_client_id[CLIENT_ID_LEN + 1];
    char            orgcod[ORGCOD_LEN + 1];
    char            revlvl[REVLVL_LEN + 1];
    char            lotnum[LOTNUM_LEN + 1];
    char            dstare[ARECOD_LEN + 1];
    char            dstloc[STOLOC_LEN + 1];
    char            lodlvl[LODLVL_LEN + 1];
    char            invsts[INVSTS_LEN + 1];
    char            invsts_prg[INVSTS_PRG_LEN + 1];
    char            concod[CONCOD_LEN + 1];
    char            stcust[ADRNUM_LEN + 1];
    char            rtcust[ADRNUM_LEN + 1];
    char            schbat[SCHBAT_LEN + 1];
    char            ship_line_id[SHIP_LINE_ID_LEN + 1];
    char            ship_id[SHIP_ID_LEN + 1];
    char            ordnum[ORDNUM_LEN + 1];
    char            client_id[CLIENT_ID_LEN + 1];
    char            ordlin[ORDLIN_LEN + 1];
    char            ordsln[ORDSLN_LEN + 1];
    char            pcksts[PCKSTS_LEN + 1];
    char            wkonum[WKONUM_LEN + 1];
    char            wkorev[WKOREV_LEN + 1];
    char            wkolin[WKOLIN_LEN + 1];
    char            wh_id[WH_ID_LEN +1];
    char            supnum[SUPNUM_LEN + 1];
    char            alc_search_path[ALC_SEARCH_PATH_LEN + 1];
    moca_bool_t     splflg;
    moca_bool_t     frsflg;

    long            orgqty;
    double          org_catch_qty;
    long            pckqty;
    double          pck_catch_qty;
    long            untcas, untpak;
    long            alloc_qty;
    double          alloc_catch_qty;
    long            min_shelf_hrs;
    long            ret_status;

    RETURN_STRUCT  *CmdRes;
    mocaDataRow    *trow;
    mocaDataRes    *tres;

    memset(pick_type, 0, sizeof(pick_type));

    strcpy(pick_type, ALLOCATE_PICKNSHIP);

    memset(orgcod, 0, sizeof(orgcod));
    memset(lotnum, 0, sizeof(lotnum));
    memset(revlvl, 0, sizeof(revlvl));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(dstare, 0, sizeof(dstare));
    memset(dstloc, 0, sizeof(dstloc));
    memset(lodlvl, 0, sizeof(lodlvl));
    memset(invsts, 0, sizeof(invsts));
    memset(invsts_prg, 0, sizeof(invsts_prg));
    memset(srcare, 0, sizeof(srcare));
    memset(concod, 0, sizeof(concod));
    memset(stcust, 0, sizeof(stcust));
    memset(rtcust, 0, sizeof(rtcust));
    memset(ship_line_id, 0, sizeof(ship_line_id));
    memset(ship_id, 0, sizeof(ship_id));
    memset(ordnum, 0, sizeof(ordnum));
    memset(client_id, 0, sizeof(client_id));
    memset(ordlin, 0, sizeof(ordlin));
    memset(ordsln, 0, sizeof(ordsln));
    memset(schbat, 0, sizeof(schbat));
    memset(pcksts, 0, sizeof(pcksts));
    memset(wkonum, 0, sizeof(wkonum));
    memset(wkorev, 0, sizeof(wkorev));
    memset(wkolin, 0, sizeof(wkolin));
    memset(wh_id,  0, sizeof(wh_id));
    memset(alc_search_path,  0, sizeof(alc_search_path));
    memset(supnum, 0, sizeof(supnum));

    misTrimcpy(prtnum, sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
    misTrimcpy(prt_client_id,
               sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);

    if (!sqlIsNull(res, row, "invsts"))
    misTrimcpy(invsts, sqlGetString(res, row, "invsts"), INVSTS_LEN);
    if (!sqlIsNull(res, row, "invsts_prg"))
    misTrimcpy(invsts_prg, 
               sqlGetString(res, row, "invsts_prg"), 
               INVSTS_PRG_LEN);

    pckqty = sqlGetLong(res, row, "pckqty");
    if(!sqlIsNull(res, row, "pck_catch_qty"))
    {
        pck_catch_qty = sqlGetFloat(res, row, "pck_catch_qty");
    }
    else
    {
        pck_catch_qty = 0;
    }
    misTrimcpy(pcksts, sqlGetString(res, row, "pcksts"), PCKSTS_LEN);
    untcas = sqlGetLong(res, row, "untcas");
    untpak = sqlGetLong(res, row, "untpak");

    if (!sqlIsNull(res, row, "orgcod"))
        misTrimcpy(orgcod, sqlGetString(res, row, "orgcod"), ORGCOD_LEN);
    if (!sqlIsNull(res, row, "lotnum"))
        misTrimcpy(lotnum, sqlGetString(res, row, "lotnum"), LOTNUM_LEN);
    if (!sqlIsNull(res, row, "revlvl"))
        misTrimcpy(revlvl, sqlGetString(res, row, "revlvl"), REVLVL_LEN);
    if (!sqlIsNull(res, row, "supnum"))
        misTrimcpy(supnum, sqlGetString(res, row, "supnum"), SUPNUM_LEN);
    if (!sqlIsNull(res, row, "schbat"))
        misTrimcpy(schbat, sqlGetString(res, row, "schbat"), SCHBAT_LEN);
    if (!sqlIsNull(res, row, "concod"))
        misTrimcpy(concod, sqlGetString(res, row, "concod"), CONCOD_LEN);
    if (!sqlIsNull(res, row, "stcust"))
        misTrimcpy(stcust, sqlGetString(res, row, "stcust"), ADRNUM_LEN);
    if (!sqlIsNull(res, row, "rtcust"))
        misTrimcpy(rtcust, sqlGetString(res, row, "rtcust"), ADRNUM_LEN);
    if (!sqlIsNull(res, row, "ship_line_id"))
        misTrimcpy(ship_line_id,
                   sqlGetString(res, row, "ship_line_id"), SHIP_LINE_ID_LEN);
    if (!sqlIsNull(res, row, "ship_id"))
        misTrimcpy(ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
    if (!sqlIsNull(res, row, "ordnum"))
        misTrimcpy(ordnum, sqlGetString(res, row, "ordnum"), ORDNUM_LEN);
    if (!sqlIsNull(res, row, "client_id"))
        misTrimcpy(client_id, sqlGetString(res, row, "client_id"), 
                   CLIENT_ID_LEN);
    if (!sqlIsNull(res, row, "ordlin"))
        misTrimcpy(ordlin, sqlGetString(res, row, "ordlin"), ORDLIN_LEN);
    if (!sqlIsNull(res, row, "ordsln"))
        misTrimcpy(ordsln, sqlGetString(res, row, "ordsln"), ORDSLN_LEN);
    if (!sqlIsNull(res, row, "dstare"))
        misTrimcpy(dstare, sqlGetString(res, row, "dstare"), ARECOD_LEN);
    if (!sqlIsNull(res, row, "dstloc"))
        misTrimcpy(dstloc, sqlGetString(res, row, "dstloc"), STOLOC_LEN);
    if (!sqlIsNull(res, row, "wkonum"))
        misTrimcpy(wkonum, sqlGetString(res, row, "wkonum"), WKONUM_LEN);
    if (!sqlIsNull(res, row, "wkorev"))
        misTrimcpy(wkorev, sqlGetString(res, row, "wkorev"), WKOREV_LEN);
    if (!sqlIsNull(res, row, "wkolin"))
        misTrimcpy(wkolin, sqlGetString(res, row, "wkolin"), WKOLIN_LEN);
    if (!sqlIsNull(res, row, "wh_id"))
        misTrimcpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
    if (!sqlIsNull(res, row, "alc_search_path"))
        misTrimcpy(alc_search_path, sqlGetString(res, row, "alc_search_path"),
                   ALC_SEARCH_PATH_LEN);
    if (!sqlIsNull(res, row, "min_shelf_hrs"))
        min_shelf_hrs = sqlGetLong(res, row, "min_shelf_hrs");
    else
        min_shelf_hrs = -1;

    splflg = sqlGetBoolean(res, row, "splflg");
    frsflg = sqlGetBoolean(res, row, "frsflg");

    if (!sqlIsNull(res, row, "wkonum"))
    {
        pckrpl_WriteTrc("  Attempting one more allocate "
                        "(for work order), rpl: %s ",
                        sqlGetString(res, row, "rplref"));
        pckrpl_WriteTrc("    wkonum: %s, wkorev: %s, wkolin: %s, "
                        "client_id: %s ",
                        wkonum, wkorev, wkolin, client_id);
    }
    else 
    {
        pckrpl_WriteTrc("  Attempting one more allocate "
                        "(for shipment order), rpl: %s ",
                        sqlGetString(res, row, "rplref"));
        pckrpl_WriteTrc("    ship_id: %s, ordnum: %s, ordlin: %s, "
                        "client_id: %s, ordsln: %s, ship_line_id: %s ",
                        ship_id, ordnum, ordlin, client_id, 
                        ordsln, ship_line_id);
    }

    pckrpl_WriteTrc(
        "prtnum: %s, prt_client_id: %s, "
        "pckqty: %ld, pck_catch_qty: %f, splflg: %ld ",
        prtnum, prt_client_id, pckqty, pck_catch_qty, splflg);

    pckrpl_WriteTrc("    frsflg: %ld, min_shelf_hrs: %ld ",
                    frsflg, min_shelf_hrs);

    /* Set a savepoint in case we need to rollback... */
    sqlSetSavepoint("OrderPickStart");
    
    /* Dstloc and dstare must be set for emergency replenishement for 
     * work order after work order allocation enhancement. We should
     * pass them to allocate inventory to generate pick with correct destination
     * because system could not find dst ONLY according work order now.
     */
    if (!sqlIsNull(res, row, "wkonum"))
    {
        sprintf(srv_command,
                "allocate inventory "
                "  where wkonum = '%s' and wkorev = '%s' "
                "   and wkolin = '%s' and client_id = '%s' "
                "   and prtnum = '%s' and prt_client_id = '%s' "
                "   and orgcod = '%s' and revlvl = '%s' "
                "   and supnum = '%s' and lotnum = '%s' "
                "   and untpak = %d   and untcas = %d "
                "   and pckqty = %d   and segqty = '%d' "
                "   and pck_catch_qty = %f "
                "   and invsts = '%s' and invsts_prg = '%s' "
                "   and pcksts = '%s' "
                "   and pcktyp = '%s' and schbat = '%s' "
                "   and pipcod = 'N'  and wh_id = '%s' "
                "   and dstloc = '%s' and dstare = '%s' ",
                wkonum, wkorev, wkolin, client_id,
                prtnum, prt_client_id,
                orgcod, revlvl, supnum, lotnum,
                untpak, untcas, 
                pckqty, pckqty,
                pck_catch_qty,
                invsts, invsts_prg, pcksts, pick_type, 
                schbat, wh_id, dstloc, dstare);
    }
    else
    {
        /* call allocate */
        sprintf(srv_command,
                "allocate inventory "
                "where pcktyp = '%s' "
                " and prtnum = '%s' and prt_client_id = '%s' "
                " and quantity = '%ld' "
                " and pck_catch_qty = %f "
                " and orgcod = '%s' and revlvl = '%s' "
                " and supnum = '%s' "
                " and lotnum = '%s' and invsts = '%s' "
                " and invsts_prg = '%s' "
                " and ship_line_id = '%s' "
                " and ship_id = '%s' "
                " and ordnum = '%s' and client_id = '%s' "
                " and ordlin = '%s' and ordsln = '%s' "
                " and stcust = '%s' and rtcust = '%s' "
                " and segqty = '%d' "
                " and dstare = '%s' and dstloc = '%s' "
                " and wkonum = '%s' and wkorev = '%s' "
                " and wkolin = '%s' "
                " and schbat = '%s' and concod = '%s' "
                " and retmov = 'NO' and pcksts = '%s' " 
                " and splflg = '%ld' "
                " and untcas = '%ld' and untpak = '%ld' "
                " and frsflg = '%ld' and min_shelf_hrs = '%ld' "
                " and pipcod = 'N' and wh_id = '%s' "
                " and alc_search_path = '%s' ",
                pick_type, prtnum, prt_client_id, 
                pckqty, 
                pck_catch_qty,
                orgcod, revlvl, supnum,
                lotnum, invsts, invsts_prg, ship_line_id, 
                ship_id, ordnum, client_id,
                ordlin, ordsln,        stcust, rtcust, pckqty, 
                dstare, dstloc,        wkonum, wkorev,
                ordlin, schbat, concod, 
                pcksts, splflg,        untcas, untpak,
                frsflg, min_shelf_hrs, wh_id,
                alc_search_path);
    }

    CmdRes = NULL;

    ret_status = srvInitiateCommand(srv_command, &CmdRes);
    if (ret_status == eDB_DEADLOCK)
    {
        pckrpl_WriteTrc(" --->>>>  DEAD LOCK existing");
        return ret_status;
    }
    if (ret_status == eOK)
    {
        pckrpl_WriteTrc("\nCall to allocate inventory was successful");
        tres = srvGetResults(CmdRes);
        alloc_qty = 0;
        alloc_catch_qty = 0;
        for (trow = sqlGetRow(tres); trow; trow = sqlGetNextRow(trow))
        {
            alloc_qty += sqlGetLong(tres, trow, "pckqty");
            if(!sqlIsNull(tres, trow, "pck_catch_qty"))
            {
                alloc_catch_qty += sqlGetFloat(tres, trow, "pck_catch_qty");
            }
            if (!strlen(invsts))
            {
                misTrimcpy(invsts, 
                           sqlGetString(tres, trow, "invsts"), 
                           INVSTS_LEN);
            }
        }

        orgqty = sqlGetLong(res, row, "pckqty");
        if(!sqlIsNull(res, row, "pck_catch_qty"))
        {
            org_catch_qty = sqlGetFloat(res, row, "pck_catch_qty");
        }
        else
        {
            org_catch_qty = 0;
        }
        
        /* If we didn't allocate enough catch quantities
         * set the allocated unit quantity to be 0.  We still want to
         * allocate more unit quantities to obtain our desired catch quanitity.
         */
        if(org_catch_qty > 0 &&
           alloc_catch_qty < org_catch_qty)
        {
            alloc_qty = 0;
        }

        if (alloc_qty >= orgqty && alloc_catch_qty >= org_catch_qty)
        {
            alloc_qty = orgqty;
            alloc_catch_qty = org_catch_qty;

            pckrpl_WriteTrc("  Order successfully allocated complete - "
                            "deleting replen request");

            ret_status = 
                pckrpl_CleanupCrossDock(sqlGetString(res, row, "rplref"),
                                        alloc_qty,
                                        1, /* delete */
                                        !sqlIsNull(res, row, "ship_line_id") ?
                                        sqlGetString(res,row,"ship_line_id") :
                                        NULL,
                                        sqlGetString(res, row, "wkonum"),
                                        sqlGetString(res, row, "wkolin"),
                                        sqlGetString(res, row, "wkorev"),
                                        wh_id);
            
            if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
            {
                pckrpl_WriteTrc("  Failed (%d) deleting the xdkwrk ",
                                ret_status);
                if (CmdRes)
                    srvFreeMemory(SRVRET_STRUCT, CmdRes);
                CmdRes = NULL;
                sqlRollbackToSavepoint("OrderPickStart");
                *processed = 0;
                *catch_qty_processed = 0.0;
                return (ret_status);
            }

            sprintf(buffer,
                    "delete from rplwrk where rplref = '%s'",
                    sqlGetString(res, row, "rplref"));
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("Error %d deleting replen request!", 
                                ret_status);
                if (CmdRes)
                    srvFreeMemory(SRVRET_STRUCT, CmdRes);
                CmdRes = NULL;
                sqlRollbackToSavepoint("OrderPickStart");
                *processed = 0;
                *catch_qty_processed = 0.0;
                return (ret_status);
            }
            *processed = alloc_qty;
            *catch_qty_processed = alloc_catch_qty;
        }
        else
        {
            pckrpl_WriteTrc("  Order partially allocated  - "
                            "updating replen request");

            if(org_catch_qty <= 0)
            {
                /* Only cross dock for non catch quantity allocations */
                ret_status = 
                    pckrpl_CleanupCrossDock(sqlGetString(res, row, "rplref"),
                                            alloc_qty,
                                            0, /* update what was left */
                                            !sqlIsNull(res, row, "ship_line_id") ?
                                            sqlGetString(res,row,"ship_line_id") :
                                            NULL,
                                            sqlGetString(res, row, "wkonum"),
                                            sqlGetString(res, row, "wkolin"),
                                            sqlGetString(res, row, "wkorev"),
                                            wh_id);

                if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
                {
                    pckrpl_WriteTrc("Error (%d) updating xdkwrk, rplref= %s",
                                    ret_status, 
                                    sqlGetString(res, row, "rplref"));
                    if (CmdRes)
                        srvFreeMemory(SRVRET_STRUCT, CmdRes);
                    CmdRes = NULL;
                    sqlRollbackToSavepoint("OrderPickStart");
                    *processed = 0;
                    *catch_qty_processed = 0.0;
                    return (ret_status);
                }
            }

            sprintf(buffer,
                    "update rplwrk "
                    "   set pckqty = pckqty - %ld , "
                    "       pck_catch_qty = pck_catch_qty - %f "
                    " where rplref = '%s'"
                    "        and pckqty > %ld ",
                    alloc_qty,
                    alloc_catch_qty,
                    sqlGetString(res, row, "rplref"),
                    alloc_qty);
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("ERROR Failed upd of rplwrk to negative (1)");

                if (CmdRes)
                    srvFreeMemory(SRVRET_STRUCT, CmdRes);
                CmdRes = NULL;
                sqlRollbackToSavepoint("OrderPickStart");
                *processed = 0;
                *catch_qty_processed = 0.0;
                return (ret_status);
            }
            *processed = alloc_qty;
            *catch_qty_processed = alloc_catch_qty;
        }

        if (CmdRes)
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
        CmdRes = NULL;
        return (eOK);
    }
    else
    {
        pckrpl_WriteTrc("  Unable to allocate inventory - continuing\n");
        if (CmdRes)
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
        CmdRes = NULL;
        sqlRollbackToSavepoint("OrderPickStart");
        *processed = 0;
        *catch_qty_processed = 0.0;
        if (ret_status == eINT_NO_INVENTORY ||
            ret_status == eDB_NO_ROWS_AFFECTED)
            ret_status = eRPL_NORMAL_FAILURE;

        return (ret_status);
    }
}
static long sGlomReplenishment(long  reqqty, 
                               double req_catch_qty,
                               long *tmp_prcqty,
                               double *tmp_prc_catch_qty,
                               char *arecod_i,
                               char *prtnum_i,
                               char *prt_client_id_i,
                               char *orgcod_i,
                               char *invsts_i,
                               char *invsts_prg_i,
                               char *revlvl_i,
                               char *supnum_i,
                               char *lotnum_i,
                               char *pcksts_i,
                               long  subpck,
                               long  dtlpck,
                               long  feeder_i,
                               long *untcas_i,
                               long *untpak_i,
                               moca_bool_t *splflg_i,
                               char *alloc_dstloc_o,
                               char *wh_id)
{
    long            ret_status;
    char            buffer[2000];

    mocaDataRes    *res;
    mocaDataRow    *row;

    char            orgcod_clause[80];
    char            lotnum_clause[80];
    char            revlvl_clause[80];
    char            untcas_clause[80];
    char            untpak_clause[80];
    char            supnum_clause[80];
    moca_bool_t     splflg;

    long            avail_to_caspck;
    double          catch_avail_to_pck;
    long            avail_to_pcepck;
    long            untcas, untpak;
    long            prcqty;
    double          prc_catch_qty;
    long            excess_qty;
    double          excess_catch_qty;
    long            qty_to_pick;
    double          catch_qty_to_pick;
    long            remqty;
    double          rem_catch_qty;

    /* On the rplwrk:  the pckqty is the amount we need.  The pckqty
       is the amount we have headed there... */

    /* Attempt to find a replenishment, heading to the
       specified area that has an excess of quantity on
       It.  We can snatch this quantity from it and use it
       for another replenishment... */

    /* Which replenishment(s) we grab quantity from doesn't
       really matter...what matters is that after we grab what
       we need, we must update the rplwrk record to show that
       that much more quantity has been grabbed. */

    memset(lotnum_clause, 0, sizeof(lotnum_clause));
    memset(revlvl_clause, 0, sizeof(revlvl_clause));
    memset(orgcod_clause, 0, sizeof(orgcod_clause));
    memset(untcas_clause, 0, sizeof(untcas_clause));
    memset(untpak_clause, 0, sizeof(untpak_clause));
    memset(alloc_dstloc_o, 0, sizeof(alloc_dstloc_o));
    memset(supnum_clause, 0, sizeof(supnum_clause));

    if (orgcod_i && misTrimLen(orgcod_i, ORGCOD_LEN))
        sprintf(orgcod_clause, " and orgcod = '%.*s' ",
                (int) misTrimLen(orgcod_i, ORGCOD_LEN), orgcod_i);

    if (revlvl_i && misTrimLen(revlvl_i, REVLVL_LEN))
        sprintf(revlvl_clause, " and revlvl = '%.*s' ",
                (int) misTrimLen(revlvl_i, REVLVL_LEN), revlvl_i);

    if (lotnum_i && misTrimLen(lotnum_i, LOTNUM_LEN))
        sprintf(lotnum_clause, " and lotnum = '%.*s' ",
                (int) misTrimLen(lotnum_i, LOTNUM_LEN), lotnum_i);
    
    if (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN))
        sprintf(supnum_clause, " and supnum = '%.*s' ",
                (int) misTrimLen(supnum_i, SUPNUM_LEN), supnum_i);

    if (*untcas_i > 0)
        sprintf(untcas_clause, " and untcas = '%ld'", *untcas_i );
    if (*untpak_i > 0)
        sprintf(untpak_clause, " and untpak = '%ld'", *untpak_i );

    /* If this is an allocation for a catch quantity product
     * select the replenishments with left over catch quantity.
     * Otherwise get the ones with leftover unit quantity
     */
     
    /* WMD-37603  remove "union" for "union" and "for update" 
     * can not be used in the same sql statement.
     */ 
    sprintf(buffer,
            "select rplref, "
                   "alcqty, pckqty, "
                   "alc_catch_qty, pck_catch_qty, "
                   "untcas, untpak, splflg, "
                   "dstloc "
              "from rplwrk "
             "where ((alcqty > pckqty and %f <= 0.0) "
               "  or (alc_catch_qty > pck_catch_qty and %f > 0.0)) "
               "and prtnum = '%s' "
               "and prt_client_id = '%s' "
               "and wh_id = '%s' "
               "and (invsts = '%s' or invsts_prg = '%s') "
               " %s %s %s %s "
               " %s "   /*  Untcas Allocation */
               " %s "   /*  Untpak Allocation */
               "and dstare = '%.*s' "
              " for update of pckqty ",
            req_catch_qty,
            req_catch_qty,
            prtnum_i, 
            prt_client_id_i, 
            wh_id, 
            invsts_i, invsts_prg_i, 
            orgcod_clause, revlvl_clause, supnum_clause,
            lotnum_clause,
            untcas_clause, 
            untpak_clause,
            (int) misTrimLen(arecod_i, ARECOD_LEN), arecod_i);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        *tmp_prcqty = 0;
        *tmp_prc_catch_qty = 0;
        sqlFreeResults(res);
        return (ret_status);
    }

    prcqty = 0;
    prc_catch_qty = 0;
    remqty = reqqty;
    rem_catch_qty = req_catch_qty;

    splflg =  *splflg_i;

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {

        excess_qty = (sqlGetLong(res, row, "alcqty") -
                      sqlGetLong(res, row, "pckqty"));
        if(!sqlIsNull(res, row, "alc_catch_qty") && 
           !sqlIsNull(res, row, "pck_catch_qty"))
        {
            excess_catch_qty = (sqlGetFloat(res, row, "alc_catch_qty") -
                                sqlGetFloat(res, row, "pck_catch_qty"));
        }
        else
        {
            excess_catch_qty = 0.0;
        }
        untcas = sqlGetLong(res, row, "untcas");
        untpak = sqlGetLong(res, row, "untpak");

        *untcas_i = untcas;
        *untpak_i = untpak;

        if (untcas == 0)
            continue;

        avail_to_caspck = (excess_qty / untcas) * untcas;
        avail_to_pcepck = excess_qty;
        catch_avail_to_pck = excess_catch_qty;

        qty_to_pick = 0;
        catch_qty_to_pick = 0;

        /* This was a request for a catch quantity of product so 
         * try to glom by catch quantity */
        if(rem_catch_qty > 0.0 && 
           catch_avail_to_pck > 0)
        {
            if(catch_avail_to_pck > rem_catch_qty)
            {
                /* If we found more than we need only use what we need */
                catch_qty_to_pick = rem_catch_qty;
            }
            else
            {
                /* Didn't find enough so use what we can */
                catch_qty_to_pick = catch_avail_to_pck;
            }

            sprintf(buffer,
                "update rplwrk "
                "   set alc_catch_qty = alc_catch_qty - %f "
                " where rplref = '%s' "
                "   and alc_catch_qty > %f ",
                catch_qty_to_pick,
                sqlGetString(res, row, "rplref"),
                catch_qty_to_pick);

            ret_status = sqlExecStr(buffer, NULL);

            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("ERROR Failed upd of rplwrk to "
                                "negative (4)");

                sqlFreeResults(res);
                return (ret_status);
            }

            prc_catch_qty += catch_qty_to_pick;
            /* We don't care about unit quantity so just set it
             * as processed */
            prcqty = reqqty;
            catch_avail_to_pck -= catch_qty_to_pick;
        }
        rem_catch_qty -= catch_qty_to_pick;

        /* If we can subpck and there is an available quantity
         * to case pick and this was not a catch quantity allocation */
        if (subpck && 
            avail_to_caspck > 0 &&
            req_catch_qty <= 0.0)
        {
            if (avail_to_caspck > remqty)
            {
                /* For feeder replenishments, we'll round up to the next
                   full case to get to pcepck...so...if we're attempting
                   to feed, we change what we are looking for... */
                if (!feeder_i)
                {
                    qty_to_pick = (remqty / untcas) * untcas;
                }
                else
                {
                    qty_to_pick = ((remqty / untcas) + 1) * untcas;
                    if (qty_to_pick > avail_to_caspck)
                        qty_to_pick = avail_to_caspck;
                }
            }
            else
            {
                qty_to_pick = avail_to_caspck;
            }

            /* If we've got a quantity to pick - then grab it */
            sprintf(buffer,
                    "update rplwrk "
                    "   set alcqty = alcqty - %ld "
                    " where rplref = '%s' "
                    "   and alcqty > %ld ",
                    qty_to_pick, 
                    sqlGetString(res, row, "rplref"),
                    qty_to_pick);
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("ERROR Failed upd of rplwrk to "
                                "negative (4)");

                sqlFreeResults(res);
                return (ret_status);
            }
            prcqty += qty_to_pick;
            avail_to_caspck -= qty_to_pick;
            avail_to_pcepck -= qty_to_pick;
        }

        /* Did we glom what we needed to? */
        if (prcqty >= reqqty && 
            prc_catch_qty >= req_catch_qty)
        {
            /* Get out... */
            *tmp_prcqty = prcqty;
            *tmp_prc_catch_qty = prc_catch_qty;
            misTrc(T_FLOW, "Found case replen to glom on to. ");

            if (!sqlIsNull(res, row, "dstloc"))
            {
                misTrc(T_FLOW, "Dstloc is %s", 
                               sqlGetString(res, row, "dstloc"));
                misTrimcpy(alloc_dstloc_o,
                           sqlGetString(res, row, "dstloc"), STOLOC_LEN);
            }
            sqlFreeResults(res);
            return (eOK);
        }

        remqty -= qty_to_pick;
        qty_to_pick = 0;

        /* If we can dtlpck and a piece pick quantity is available
         * and we can split and this is not a catch quantity allocation */
        if (dtlpck && 
            avail_to_pcepck &&
            splflg == BOOLEAN_TRUE &&
            req_catch_qty <= 0.0)
        {
            if (avail_to_pcepck > remqty)
            {
                qty_to_pick = remqty;
            }
            else
            {
                qty_to_pick = avail_to_pcepck;
            }

            sprintf(buffer,
                    "update rplwrk "
                    "        set alcqty = alcqty - %ld"
                    " where rplref = '%s'"
                    "        and alcqty > %ld ",
                    qty_to_pick, sqlGetString(res, row, "rplref"),
                    qty_to_pick);
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("ERROR Failed upd of rplwrk to "
                                "negative (5)");

                sqlFreeResults(res);
                return (ret_status);
            }
            prcqty += qty_to_pick;
        }
        remqty -= qty_to_pick;

        /* Did we glom what we needed to? */
        if (prcqty >= reqqty &&
            prc_catch_qty >= req_catch_qty)
        {
            /* Get out... */
            *tmp_prcqty = prcqty;
            *tmp_prc_catch_qty = prc_catch_qty;
            if (!sqlIsNull(res, row, "dstloc"))
                misTrimcpy(alloc_dstloc_o, sqlGetString(res, row, "dstloc"),
                           STOLOC_LEN);
            sqlFreeResults(res);
            return (eOK);
        }
        if (!sqlIsNull(res, row, "dstloc"))
            misTrimcpy(alloc_dstloc_o, 
                       sqlGetString(res, row, "dstloc"), STOLOC_LEN);

    }

    misTrc(T_FLOW, "got here ... ");

    *tmp_prcqty = prcqty;
    *tmp_prc_catch_qty = prc_catch_qty;
    sqlFreeResults(res);
    return (eOK);
}


/* 
 * JJS - This mimics the sSaveOrCheckSkipAllocateLocation function in trnAllocInv
 * (which was created as a fix just recently for us by Product).  The idea is to
 * save the previous values used in a call to "allocate inventory and location"
 * to potentially skip calling it over and over with the same parameters.  If we
 * know it fails, we don't have to keep calling it with the same parameters.
 *
 * This funcation used to:
 * 1.If savflg =1, than save the parameters that passed in to record the
 *   failure of 'allocate inventory and location'.
 * 2.If savflg = 0, than we check if we can skip 'allocate location'.
 * This will improve the performance for 'allocate inventory' especially when
 * there are many loads but each of them failed because of allocate location.
 */
static long sSaveOrCheckSkipAllocInvAndLoc(char *prtnum,
                                           char *prt_client_id,
                                           long reqqty,
                                           float req_catch_qty,
                                           char *orgcod,
                                           char *revlvl,
                                           char *supnum,
                                           char *lotnum,
                                           char *invsts,
                                           char *invsts_prg,
                                           char *arecod,
                                           char *pcklvl,
                                           long splflg,
                                           long untcas,
                                           long untpak,
                                           char *wh_id,
                                           long min_shelf_hrs,
                                           moca_bool_t Savflg)
{
        static char prtnum_cp[PRTNUM_LEN+1];
        static char prt_client_id_cp[CLIENT_ID_LEN+1];
        static long reqqty_cp;
        static float req_catch_qty_cp;
        static char orgcod_cp[ORGCOD_LEN+1];
        static char revlvl_cp[REVLVL_LEN+1];
        static char supnum_cp[SUPNUM_LEN+1];
        static char lotnum_cp[LOTNUM_LEN+1];
        static char invsts_cp[INVSTS_LEN+1];
        static char invsts_prg_cp[INVSTS_PRG_LEN+1];
        static char arecod_cp[ARECOD_LEN+1];
        static char pcklvl_cp[37];
        static long splflg_cp;
        static long untcas_cp;
        static long untpak_cp;
        static char wh_id_cp[WH_ID_LEN+1];
        static long min_shelf_hrs_cp;

        moca_bool_t skipflg = BOOLEAN_FALSE;

        /*
         * We are saving the "allocate inventory and location" parameters.
         */
        if (Savflg)
        {

                misTrimcpy(prtnum_cp, prtnum, PRTNUM_LEN);
                misTrimcpy(prt_client_id_cp, prt_client_id, CLIENT_ID_LEN);
                reqqty_cp = reqqty;
                req_catch_qty_cp = req_catch_qty;
                misTrimcpy(orgcod_cp, orgcod, ORGCOD_LEN);
                misTrimcpy(revlvl_cp, revlvl, REVLVL_LEN);
                misTrimcpy(supnum_cp, supnum, SUPNUM_LEN);
                misTrimcpy(lotnum_cp, lotnum, LOTNUM_LEN);
                misTrimcpy(invsts_cp, invsts, INVSTS_LEN);
                misTrimcpy(invsts_prg_cp, invsts_prg, INVSTS_PRG_LEN);
                misTrimcpy(arecod_cp, arecod, ARECOD_LEN);
                misTrimcpy(pcklvl_cp, pcklvl, 37);
                splflg_cp = splflg;
                untcas_cp = untcas;
                untpak_cp = untpak;
                misTrimcpy(wh_id_cp, wh_id, WH_ID_LEN);
                min_shelf_hrs_cp = min_shelf_hrs;

                misTrc(T_FLOW, "Alloc inventory and loc failed, infor saved!");

                return BOOLEAN_FALSE;
        }
        else
        {

      skipflg= (misCiStrcmp(prtnum_cp, prtnum) == 0 &&
                misCiStrcmp(prt_client_id_cp, prt_client_id) == 0 &&
                misCiStrcmp(orgcod_cp, orgcod) == 0 &&
                misCiStrcmp(revlvl_cp, revlvl) == 0 &&
                misCiStrcmp(supnum_cp, supnum) == 0 &&
                misCiStrcmp(lotnum_cp, lotnum) == 0 &&
                misCiStrcmp(invsts_cp, invsts) == 0 &&
                misCiStrcmp(invsts_prg_cp, invsts_prg) == 0 &&
                misCiStrcmp(arecod_cp, arecod) == 0 &&
                misCiStrcmp(pcklvl_cp, pcklvl) == 0 &&
                misCiStrcmp(wh_id_cp, wh_id) == 0 &&
                reqqty_cp == reqqty &&
                req_catch_qty_cp == req_catch_qty &&
                splflg_cp == splflg &&
                untcas_cp == untcas &&
                untpak_cp == untpak &&
                min_shelf_hrs_cp == min_shelf_hrs);

              misTrc(T_FLOW, "VALUES FOR skipflg CALCULATION in sSaveOrCheckSkipAllocInvAndLoc:");
              misTrc(T_FLOW, "prtnum_cp='%s',prtnum='%s'", prtnum_cp, prtnum);
              misTrc(T_FLOW, "prtnum_cp='%s',prtnum='%s'", prtnum_cp, prtnum);
              misTrc(T_FLOW, "prt_client_id_cp='%s',prt_client_id='%s'", prt_client_id_cp, prt_client_id);
              misTrc(T_FLOW, "orgcod_cp='%s',orgcod='%s'", orgcod_cp, orgcod);
              misTrc(T_FLOW, "revlvl_cp='%s',revlvl='%s'", revlvl_cp, revlvl);
              misTrc(T_FLOW, "supnum_cp='%s',supnum='%s'", supnum_cp, supnum);
              misTrc(T_FLOW, "lotnum_cp='%s',lotnum='%s'", lotnum_cp, lotnum);
              misTrc(T_FLOW, "invsts_cp='%s',invsts='%s'", invsts_cp, invsts);
              misTrc(T_FLOW, "invsts_prg_cp='%s',invsts_prg='%s'", invsts_prg_cp, invsts_prg);
              misTrc(T_FLOW, "arecod_cp='%s',arecod='%s'", arecod_cp, arecod);
              misTrc(T_FLOW, "pcklvl_cp='%s',pcklvl='%s'", pcklvl_cp, pcklvl);
              misTrc(T_FLOW, "wh_id_cp='%s',wh_id='%s'", wh_id_cp, wh_id);
              misTrc(T_FLOW, "reqqty_cp='%ld',reqqty='%ld'", reqqty_cp, reqqty);
              misTrc(T_FLOW, "req_catch_qty_cp='%f',req_catch_qty='%f'", req_catch_qty_cp, req_catch_qty);
              misTrc(T_FLOW, "splflg_cp='%ld',splflg='%ld'", splflg_cp, splflg);
              misTrc(T_FLOW, "untcas_cp='%ld',untcas='%ld'", untcas_cp, untcas);
              misTrc(T_FLOW, "untpak_cp='%ld',untpak='%ld'", untpak_cp, untpak);
              misTrc(T_FLOW, "min_shelf_hrs_cp='%ld',min_shelf_hrs='%ld'", min_shelf_hrs_cp, min_shelf_hrs);
              misTrc(T_FLOW, "skipflg = %ld", skipflg);

             return skipflg;
        }

}



static long sReplenishArea(long reqqty_i,
                           double req_catch_qty_i,
                           long *prcqty_i,
                           double *prc_catch_qty_i,
                           long *pckqty_i,
                           double *pck_catch_qty_i,
                           char *pcklvl,
                           char *arecod_i,
                           char *wh_id_i,
                           char *prtnum_i,
                           char *prt_client_id_i,
                           char *orgcod_i,
                           char *revlvl_i,
                           char *supnum_i,
                           char *lotnum_i,
                           char *invsts_i,
                           char *invsts_prg_i,
                           char *parref_i,
                           long feeder_i,
                           RPL_AREA_LIST *alist,
                           char *pcksts_i,
                           moca_bool_t *splflg_i,
                           long *untcas_i,
                           long *untpak_i,
                           moca_bool_t *glom_sts_i, 
                           long min_shelf_hrs_i)
{
    char            buffer[2000];
    char            srv_command[2000];
    char            rplref[RPLREF_LEN + 1];
    char            arecod[ARECOD_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    char            alloc_dstloc[STOLOC_LEN + 1];
    long            lodpck, subpck, dtlpck;
    long            tmp_prcqty, tmp_pckqty, alcqty;
    double          tmp_prc_catch_qty, tmp_pck_catch_qty, alc_catch_qty;
    long            sum_alcqty;
    double          sum_alc_catch_qty;
    long            ret_status;
    long            untcas;
    long            untpak;
    moca_bool_t     splflg;
    moca_bool_t     glom_sts = BOOLEAN_FALSE;

    RETURN_STRUCT  *CurPtr;
    mocaDataRes    *res;
    mocaDataRow    *row, *prvrow;

 misTrc(T_FLOW, "JJSTMPDEBUG10>>> Beginning of sReplenishArea");

    *prcqty_i = *pckqty_i = 0;
    *prc_catch_qty_i = *pck_catch_qty_i = 0.0;
    untcas = *untcas_i;
    untpak = *untpak_i;
    memset(arecod, 0, sizeof(arecod));
    memset(alloc_dstloc, 0, sizeof(alloc_dstloc));
    memset(wh_id, 0, sizeof(wh_id));
    strncpy(arecod, arecod_i, misTrimLen(arecod_i, ARECOD_LEN));
    strncpy(wh_id, wh_id_i, misTrimLen(wh_id_i, WH_ID_LEN));
    splflg = *splflg_i;
    tmp_prc_catch_qty = tmp_pck_catch_qty = alc_catch_qty = 0.0;

    sprintf(buffer,
            "select lodflg, subflg, dtlflg"
            "  from aremst "
            " where arecod = '%s'"
            "   and wh_id  = '%s'",
            arecod, wh_id);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        pckrpl_WriteTrc("  Could not determine area characteristics (%s), "
                        " skipping...",
                           arecod);
 
        /* Here we're going to error with a eRPL_NORMAL_FAILURE so that */
        /* the process can go to the next area. */
        return (eRPL_NORMAL_FAILURE);
    }

    lodpck = subpck = dtlpck = 0;
    row = sqlGetRow(res);

    if (sqlGetBoolean(res, row, "lodflg") == BOOLEAN_TRUE)
        lodpck = 1;
    if (sqlGetBoolean(res, row, "subflg") == BOOLEAN_TRUE)
        subpck = 1;
    if (sqlGetBoolean(res, row, "dtlflg") == BOOLEAN_TRUE)
        dtlpck = 1;

    sqlFreeResults(res);

    /* Attempt to Glom */

    sqlSetSavepoint("GLOMSTART");

    pckrpl_WriteTrc("  Attempting to 'glom' replenishment to "
                    "requests already in process...");

    alcqty = 0;

    ret_status = sGlomReplenishment(reqqty_i, 
                                    req_catch_qty_i,
                                    &tmp_prcqty,
                                    &tmp_prc_catch_qty,
                                    arecod,
                                    prtnum_i,
                                    prt_client_id_i,
                                    orgcod_i,
                                    invsts_i,
                                    invsts_prg_i,
                                    revlvl_i,
                                    supnum_i,
                                    lotnum_i,
                                    pcksts_i,
                                    subpck,
                                    dtlpck,
                                    feeder_i,
                                    &untcas,
                                    &untpak,
                                    &splflg,
                                    alloc_dstloc,
                                    wh_id);
    if (ret_status == eOK)
    {
        /* If the glomming succeeded...then we simply need to
           create an rplwrk just like we would
           in the normal course of action below...
           This then is split off from the original replenishment... */

        /* Generate new rplref */
        memset(rplref, 0, sizeof(rplref));
        appNextNum(NUMCOD_RPLREF, rplref);

        tmp_pckqty = (reqqty_i > tmp_prcqty) ? tmp_prcqty : reqqty_i;
        tmp_pck_catch_qty = (req_catch_qty_i > tmp_prc_catch_qty) ? 
            tmp_prc_catch_qty: req_catch_qty_i;

        if (tmp_pckqty > 0)
        {
            sprintf(buffer,
                    "insert into rplwrk (rplref, parref, rplcnt,"
                    "  pckqty, alcqty, "
                    "  pck_catch_qty, alc_catch_qty, "
                    "  rplsts,"
                    "  prtnum, prt_client_id, "
                    "  invsts, invsts_prg, adddte,"
                    "  orgcod, revlvl, supnum, lotnum,"
                    "  alcdte, dstare, dstloc, untcas,"
                    "  untpak, pcksts, splflg, wh_id ) "
                    " values ('%s', '%s', %d, "
                    "  %ld, %ld, "
                    "  %f, %f, "
                    "  '%s', "
                    "  '%s', '%s', "   /* prtnum, prt_client_id */
                    "  '%s', '%s', %s, "
                    "  '%s', '%s', '%s', '%s', "
                    "  %s, '%.*s', '%s', '%ld', "
                    "  '%ld', '%s','%ld', '%s')",
                    rplref, parref_i, 0,
                    tmp_pckqty, tmp_prcqty, 
                    tmp_pck_catch_qty, tmp_prc_catch_qty,
                    RPLSTS_PENDDEP,
                    prtnum_i, prt_client_id_i, 
                    invsts_i, invsts_prg_i, "sysdate",
                    misTrimLen(orgcod_i, ORGCOD_LEN) ? orgcod_i : "",
                    misTrimLen(revlvl_i, REVLVL_LEN) ? revlvl_i : "",
                    misTrimLen(supnum_i, SUPNUM_LEN) ? supnum_i : "",
                    misTrimLen(lotnum_i, LOTNUM_LEN) ? lotnum_i : "",
                    "sysdate", ARECOD_LEN, arecod, 
                    misTrimLen(alloc_dstloc, STOLOC_LEN) ? alloc_dstloc : "",
                    untcas, untpak,
                    (misTrimLen(pcksts_i, PCKSTS_LEN) ? pcksts_i : 
                     PCKSTS_PENDING),
                    splflg, wh_id);

            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                sqlRollbackToSavepoint(parref_i);
                pckrpl_WriteTrc("Error (%d) inserting glommed rplwrk (%s)",
                                ret_status, rplref);
                return (ret_status);
            }

            pckrpl_WriteTrc("\nSuccessfully 'glommed' replenishment "
                            "for qty of %ld catch_qty %f\n",
                            tmp_prcqty, tmp_prc_catch_qty);

            *prcqty_i = tmp_prcqty;
            *prc_catch_qty_i = tmp_prc_catch_qty;
            *pckqty_i = tmp_pckqty;
            *pck_catch_qty_i = tmp_pck_catch_qty;

            glom_sts = BOOLEAN_TRUE;
            *glom_sts_i = glom_sts;

            return (eOK);
        }
        else
        {
            sqlRollbackToSavepoint(parref_i);
            
            pckrpl_WriteTrc(
                "ERROR Was about to insert rplwrk of '%ld' '%f'! (3)",
                tmp_pckqty, tmp_pck_catch_qty);
        }
    }
    else
    {
        /* Glomming failed... */
        pckrpl_WriteTrc("  Failed to find any requests for glom attempt");
        sqlRollbackToSavepoint("GLOMSTART");
    }

    sqlSetSavepoint(parref_i);
    
    /* JJS - This is called to check if we should run "allocate inventory
     * and location".  If the previous call to that command failed, and
     * we are trying to call it again with the same paramters, then
     * it's not worth calling again with the same parameters and chewing
     * up a bunch of CPU and database processing (which is currently a big
     * cause of db locks for Hopewell).
     * NOTE: The last paramter, Savflg, tells us whether we are saving or
     * checking the values last used.  Here we are checking
     */
    if (!sSaveOrCheckSkipAllocInvAndLoc(prtnum_i,
                                        prt_client_id_i,
                                        reqqty_i,
                                        req_catch_qty_i,
                                        orgcod_i,
                                        revlvl_i,
                                        supnum_i,
                                        lotnum_i,
                                        invsts_i,
                                        invsts_prg_i,
                                        arecod,
                                        pcklvl,
                                        splflg,
                                        untcas,
                                        untpak,
                                        wh_id,
                                        min_shelf_hrs_i,
                                        BOOLEAN_FALSE))
    {
        /* call allocate */
        sprintf(srv_command,
                "allocate inventory and location where type = \"%s\" "
                " and prtnum = \"%s\" and prt_client_id = \"%s\" "
                " and quantity = %ld and pck_catch_qty = %f"
                " and orgcod = \"%s\" and revlvl = \"%s\" and supnum = \"%s\" "
                " and lotnum = \"%s\" and invsts = \"%s\" "
                " and invsts_prg = \"%s\" "
                " and segqty = \"%d\" "
                " and dstare = \"%s\" "
                " and pcklvl = \"%s\" "
                " and splflg = \"%ld\" "
                " and untcas = '%ld'  "
                " and untpak = '%ld'  "
                " and retmov = \"NO\" "
                " and pipcod = 'N'    "
                " and wh_id = \"%s\"   "
                " and wrktyp = '%s' "
                " and min_shelf_hrs = '%ld' ",
                ALLOCATE_REPLENISH, prtnum_i, prt_client_id_i,
                reqqty_i, req_catch_qty_i,
                orgcod_i, revlvl_i, supnum_i, lotnum_i,
                invsts_i,
                invsts_prg_i,
                reqqty_i,
                arecod,
                pcklvl,
                splflg,
                untcas,
                untpak,
                wh_id,
                WRKTYP_EMERGENCY,
                min_shelf_hrs_i);

        CurPtr = NULL;

        ret_status = srvInitiateCommand(srv_command, &CurPtr);
    }
    else
    {
        /* JJS - Just set the return status to some bogus value.
	 * We didn't run "allocate inventory and location" but we know
	 * it would fail with the parameters it would have run with.
	 */

      /*!!!!!!!!!!!might need to put CurPtr == NULL here?????  Actually....I think we don't free the memory if ret_status = 89666 ...actually, maybe just setting NULL is what we want here*/
        ret_status = 89666;
        CurPtr = NULL;  /* TRY IT!!!!!!*/
    }

    if (ret_status == eDB_DEADLOCK)
    {
        pckrpl_WriteTrc(" --->>>>  DEAD LOCK existing");
        return ret_status;
    }
    /* If failed - attempt feeder if not already feeding... */
    if (ret_status != eOK)
    {
        /* JJS - "allocate inventory and location" failed for some reason.
         * So, save the values which caused it to fail.  These are being
         * saved because it's possible it will get called over and over
         * again with the same values and fail each time.  This is causing
         * the replen manager to run for a long time and cause database
         * locking in the system
         */
        sSaveOrCheckSkipAllocInvAndLoc(prtnum_i,
                                       prt_client_id_i,
                                       reqqty_i,
                                       req_catch_qty_i,
                                       orgcod_i,
                                       revlvl_i,
                                       supnum_i,
                                       lotnum_i,
                                       invsts_i,
                                       invsts_prg_i,
                                       arecod,
                                       pcklvl,
                                       splflg,
                                       untcas,
                                       untpak,
                                       wh_id,
                                       min_shelf_hrs_i,
                                       BOOLEAN_TRUE);

        /* rollback what was done in allocate... */
        sqlRollbackToSavepoint(parref_i); 
        
 misTrc(T_FLOW, "JJSTMPDEBUG50>>> Just before free memory");
        if (CurPtr)
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
 misTrc(T_FLOW, "JJSTMPDEBUG52>>> Just after free memory");

        CurPtr = NULL;
 misTrc(T_FLOW, "JJSTMPDEBUG54>>> Just after setting CurPtr NULL");

        /* IF this is already a feeder call or we don't have another area 
         * to try, then we can't do anything more 
         */
        if (feeder_i || !alist || strlen(alist->feeder_arecod) == 0)
        {
            sSetError(_ERROR_NO_INVENTORY, "");
            return (eRPL_NORMAL_FAILURE);
        }

        /* We're going to call ourselves again to attempt to fill
         *  the feeder area.... 
         */
        sqlSetSavepoint("FeederReplen");

        pckrpl_WriteTrc("Allocate call failed - attempting to replenish"
                        "feeder area");

        ret_status = sReplenishArea(reqqty_i,
                                    req_catch_qty_i,
                                    &tmp_prcqty,
                                    &tmp_prc_catch_qty,
                                    &tmp_pckqty,
                                    &tmp_pck_catch_qty,
                                    alist->feeder_lodlvl,
                                    alist->feeder_arecod,
                                    wh_id,
                                    prtnum_i,
                                    prt_client_id_i,
                                    orgcod_i,
                                    revlvl_i,
                                    supnum_i,
                                    lotnum_i,
                                    invsts_i,
                                    invsts_prg_i,
                                    "TMPGENREF",
                                    1,        /* This is a feeder */
                                    NULL,
                                    pcksts_i,
                                    &splflg,
                                    &untcas,
                                    &untpak,
                                    &glom_sts,
                                    min_shelf_hrs_i);
   
        if (ret_status != eOK && ret_status != eRPL_NORMAL_FAILURE)
        {
            sqlRollbackToSavepoint("FeederReplen");
            pckrpl_WriteTrc("Error (%d) attempting feeder replenishment");
            return(ret_status);
        }

        /* So if the call to GenReplen worked, we must create
           a new rplwrk record, with the parent reference of
           parref, and a rplref of a generated value.  We then
           must go and update our child parref where the parref
           was TMPGENREF...   */
        alcqty = 0;
        alc_catch_qty = 0;
        if (tmp_pckqty > 0)
        {
            memset(rplref, 0, sizeof(rplref));
            ret_status = appNextNum(NUMCOD_RPLREF, rplref);
            if (ret_status != eOK)
            {
                sqlRollbackToSavepoint("FeederReplen");
                pckrpl_WriteTrc("Error getting rplref (%s)", rplref);

                return (ret_status);
            }
            sprintf(buffer,
                    "update rplwrk "
                    "   set parref = '%s' "
                    " where parref = 'TMPGENREF'", 
                    rplref);

            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                sqlRollbackToSavepoint("FeederReplen");
                pckrpl_WriteTrc("Error updating child rplwrk (%s)", rplref);
                return (ret_status);
            }

            if (tmp_pckqty > reqqty_i)
                alcqty = reqqty_i;
            else
                alcqty = tmp_pckqty;

            if(tmp_pck_catch_qty > req_catch_qty_i)
            {
                alc_catch_qty = req_catch_qty_i;
            }
            else
            {
                alc_catch_qty = tmp_pck_catch_qty;
            }

            if (reqqty_i < 0)
            {
                sqlRollbackToSavepoint("FeederReplen");
                pckrpl_WriteTrc("ERROR Was about to insert a "
                                "rplwrk of %ld (4)",
                                reqqty_i);
                return (eERROR);
            }

            sprintf(buffer,
                    "insert into rplwrk (rplref, parref, rplcnt,"
                    "                         pckqty, alcqty, "
                    "                         pck_catch_qty, alc_catch_qty, "
                    "                         rplsts,"
                    "                         prtnum, prt_client_id, "
                    "                    invsts, invsts_prg, adddte, orgcod,"
                    "                         revlvl, supnum, lotnum, alcdte,"
                    "                         untcas, untpak, dstare, pcksts,"
                    "                    splflg, wh_id) "
                    " values ('%s', '%s', %d, "
                    "              %ld, %ld, "
                    "              %f, %f, "
                    "              '%s', "
                    "              '%s', '%s', "  /* prtnum, prt_client_id */
                    "         '%s',  '%s', %s, '%s', "
                    "              '%s', '%s', '%s', %s,"
                    "              %d, %d, '%.*s', '%s', '%ld', '%s')",
                    rplref, parref_i, 0,
                    reqqty_i, tmp_pckqty, 
                    req_catch_qty_i, tmp_pck_catch_qty,
                    RPLSTS_PENDDEP,
                    prtnum_i, prt_client_id_i, invsts_i, invsts_prg_i, 
                    "sysdate",
                    misTrimLen(orgcod_i, ORGCOD_LEN) ? orgcod_i : "",
                    misTrimLen(revlvl_i, REVLVL_LEN) ? revlvl_i : "",
                    misTrimLen(supnum_i, SUPNUM_LEN) ? supnum_i : "",
                    misTrimLen(lotnum_i, LOTNUM_LEN) ? lotnum_i : "",
                    "sysdate", untcas, untpak, ARECOD_LEN, arecod,
                    misTrimLen(pcksts_i, PCKSTS_LEN) ?
                    pcksts_i : PCKSTS_PENDING,
                    splflg, wh_id);

            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                sqlRollbackToSavepoint("FeederReplen");
                pckrpl_WriteTrc("Error inserting rplwrk (%s)", rplref);
                return (ret_status);
            }
        }
        *prcqty_i = tmp_prcqty;
        *prc_catch_qty_i = tmp_prc_catch_qty;
        *pckqty_i = alcqty;
        *pck_catch_qty_i = alc_catch_qty;
        return (tmp_prcqty > 0 ? eOK : eRPL_NORMAL_FAILURE);
    }

    /* Allocate inventory succeeded...now we must choose where
       we are going to deposit the inventory.... */

    pckrpl_WriteTrc("\n        Inventory successfully allocated ");

    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);
    prvrow = row;

    sum_alcqty = 0;
    sum_alc_catch_qty = 0;

    /* Spin through the result set and allocate a location
       for each combo code */

    do
    {        /* The inside loop will advance the row for this loop */
        char            prtnum[PRTNUM_LEN + 1];
        char            prt_client_id[CLIENT_ID_LEN + 1];
        char            lotnum[LOTNUM_LEN + 1];
        char            revlvl[REVLVL_LEN + 1];
        char            orgcod[ORGCOD_LEN + 1];
        char            invsts[INVSTS_LEN + 1];
        char            supnum[SUPNUM_LEN + 1];

        alcqty = 0;
        alc_catch_qty = 0;
        memset(prtnum, 0, sizeof(prtnum));
        memset(prt_client_id, 0, sizeof(prt_client_id));
        memset(lotnum, 0, sizeof(lotnum));
        memset(revlvl, 0, sizeof(revlvl));
        memset(orgcod, 0, sizeof(orgcod));
        memset(invsts, 0, sizeof(invsts));
        memset(alloc_dstloc, 0, sizeof(alloc_dstloc));
        memset(supnum, 0, sizeof(supnum));
        strncpy(prtnum,
                sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
        strncpy(prt_client_id,
                sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
        strncpy(orgcod,
                sqlGetString(res, row, "orgcod"), ORGCOD_LEN);
        strncpy(revlvl,
                sqlGetString(res, row, "revlvl"), REVLVL_LEN);
        strncpy(lotnum,
                sqlGetString(res, row, "lotnum"), LOTNUM_LEN);
        strncpy(invsts,
                sqlGetString(res, row, "invsts"), INVSTS_LEN);
        strncpy(supnum,
                sqlGetString(res, row, "supnum"), SUPNUM_LEN);

        if (!sqlIsNull(res, row, "alloc_dstloc"))
        {
            misTrimcpy(alloc_dstloc, 
                       sqlGetString(res, row, "alloc_dstloc"), STOLOC_LEN);
        }

        /* Sum the allocation quantity for a combo code */
        do
        {
            alcqty += sqlGetLong(res, row, "pckqty");
            if(!sqlIsNull(res, row, "pck_catch_qty"))
            {
                alc_catch_qty += sqlGetFloat(res, row, "pck_catch_qty");
            }
            prvrow = row;
            row = sqlGetNextRow(row);
        } while (row != NULL &&
                 strncmp(sqlGetString(res, prvrow, "cmbcod"),
                         sqlGetString(res, row, "cmbcod"), CMBCOD_LEN) == 0);

        sum_alcqty += alcqty;
        sum_alc_catch_qty += alc_catch_qty;

        untcas = sqlGetLong(res, prvrow, "untcas");
        untpak = sqlGetLong(res, prvrow, "untpak");

        tmp_prcqty = alcqty;
        tmp_prc_catch_qty = alc_catch_qty;
        if (dtlpck)
        {
            tmp_pckqty = reqqty_i;
            tmp_pck_catch_qty = req_catch_qty_i;
        }
        else if (subpck)
        {
            if (feeder_i)
            {
                tmp_pckqty = ((reqqty_i / untcas) + 1) * untcas;
                tmp_pck_catch_qty = req_catch_qty_i;
            }
            else
            {
                tmp_pckqty = (reqqty_i / untcas) * untcas;
                tmp_pck_catch_qty = req_catch_qty_i;
            }
        }
        else
        {
            /* don't know what to do here.... */
        }

        /* Ok...now put out the rplwrk record.... */
        memset(rplref, 0, sizeof(rplref));
        appNextNum(NUMCOD_RPLREF, rplref);

        if (tmp_pckqty > 0)
        {
            sprintf(buffer,
                    "insert into rplwrk (rplref, parref, rplcnt,"
                    "                    pckqty, alcqty, "
                    "                    pck_catch_qty, alc_catch_qty, "
                    "                    rplsts,"
                    "                         prtnum, prt_client_id, "
                    "                    invsts, invsts_prg, adddte, orgcod,"
                    "                         revlvl, supnum, lotnum, alcdte,"
                    "                         untcas, untpak, dstare, dstloc,"
                    "                    pcksts, splflg, wh_id ) "
                    " values ('%s', '%s', %d, "
                    "         %ld, %ld, "
                    "         %f, %f, "
                    "         '%s', "
                    "         '%s', '%s', "   /* prtnum, prt_client_id */
                    "         '%s',  '%s', %s, '%s', "
                    "         '%s', '%s', '%s',%s,"
                    "         %ld, %ld, '%.*s', '%s', '%s','%ld', '%s')",
                    rplref, parref_i, 0,
                    tmp_pckqty, tmp_prcqty, 
                    tmp_pck_catch_qty, tmp_prc_catch_qty, 
                    RPLSTS_PENDDEP,
                    prtnum_i, prt_client_id_i, 
                    invsts, invsts_prg_i, "sysdate",
                    misTrimLen(orgcod_i, ORGCOD_LEN) ? orgcod_i : "",
                    misTrimLen(revlvl_i, REVLVL_LEN) ? revlvl_i : "",
                    misTrimLen(supnum_i, SUPNUM_LEN) ? supnum_i : "",
                    misTrimLen(lotnum_i, LOTNUM_LEN) ? lotnum_i : "",
                    "sysdate", untcas, untpak, ARECOD_LEN, arecod,
                    misTrimLen(alloc_dstloc, STOLOC_LEN) ? alloc_dstloc: "",
                    misTrimLen(pcksts_i, PCKSTS_LEN) ?
                    pcksts_i : PCKSTS_PENDING,
                    splflg, wh_id);

            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                sqlRollbackToSavepoint(parref_i);                 

                pckrpl_WriteTrc("Error inserting main rplwrk (%s)", rplref);
                if (CurPtr)
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;

                return (ret_status);
            }

            /* Since we may have multiple returned rows from the allocate */
            /* calls on the very rare occassion, we need to just set the */
            /* pckqty = tmp_pckqty because it always represents the entire */
            /* quantity being requested ... and bump the prcqty by */
            /* tmp_prcqty because that is what we got for this pick work ref */
            *prcqty_i += tmp_prcqty;
            *prc_catch_qty_i += tmp_prc_catch_qty;
            *pckqty_i = tmp_pckqty;
            *pck_catch_qty_i = tmp_pck_catch_qty;
        }
        else
        {
            sqlRollbackToSavepoint(parref_i);
            pckrpl_WriteTrc("ERROR Was about to insert rplwrk of %ld (5)!",
                            tmp_pckqty);
            if (CurPtr)
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;

            return (eERROR);
        }

    } while (row != NULL);

    if (tmp_pckqty == 0)
    {
        /* Nothing useful came of this round...reset the world.... */
        *pckqty_i = *prcqty_i = 0;
        *pck_catch_qty_i = *prc_catch_qty_i = 0;
        sqlRollbackToSavepoint(parref_i);            

        if (CurPtr)
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
        return (eRPL_NORMAL_FAILURE);
    }
    if (CurPtr)
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;

    return (eOK);
}

static long sIsPolicyAreaToReplenish(RPL_AREA_LIST *alist,
                                     mocaDataRes *dstres,
                                     mocaDataRow *dstrow,
                                     char *alc_search_path,
                                     char *prtnum,
                                     char *prtfam,
                                     char *client_id,
                                     char *prt_client_id)
{
    int ret = 0;

    char buffer[512];
    char bldg_id[BLDG_ID_LEN + 1];
    char tmp_cfgval[CFGVAL_LEN + 1];
    long ret_status = eOK;

    mocaDataRes * res;
    mocaDataRow * row;

    memset (buffer, 0, sizeof(buffer));
    memset (bldg_id, 0, sizeof(bldg_id));
    memset (tmp_cfgval, 0, sizeof(tmp_cfgval));

    pckrpl_WriteTrc("Repl Info - Dest area: %s, "
                    "Alloc Search Path: %s, "
                    "Part: %s/%s, "
                    "Part family: %s, "
                    "Client: %s ",
                    sqlGetString(dstres, dstrow, "dstare"),
                    alc_search_path,
                    prtnum, prt_client_id,
                    prtfam,
                    client_id);

    pckrpl_WriteTrc("Test Repl Path Cfg - Dest area: %s, "
                    "Config type: %s, Config value: %s ",
                    alist->final_arecod,
                    alist->cfgtyp, alist->cfgval);
    
    if (strncmp(alist->final_arecod,
                sqlGetString(dstres, dstrow, "dstare"), 
                ARECOD_LEN) != 0)
    {
        pckrpl_WriteTrc("skipping - Dest area not match,"
                        " config: %s, replen: %s",
                        alist->final_arecod,
                        sqlGetString(dstres, dstrow, "dstare"));

        return 0;
    }

    /* From 2007.2, the replenishment path policy has been moved
     * to rpl_path_cfg table. It's enhanced to support configuring
     * different paths by different types. The types are corresponding
     * to the configuration types of pick policies. Then we can have
     * replenishment policies work well with pick policies.
     *
     * The types are in the following priorities:
     *    alc_search_path      : by allocation seach path
     *    prtnum/prt_client_id : by part
     *    prtfam               : by part family
     *    client_id            : by client
     *    None                 : this is the default setting, if
     *                           none of above was matching.
     */

    /* First, check for allocation search path. */
    if (strlen(alc_search_path))
    {
        if (misCiStrcmp(alist->cfgtyp, CFGTYP_ALC_SEARCH_PATH) == 0 &&
            misCiStrcmp(alist->cfgval, alc_search_path) != 0)
        {
            pckrpl_WriteTrc("skipping - Alloc search path not match,"
                            " config: %s, replen: %s",
                            alist->cfgval,
                            alc_search_path);

            return 0;
        }
    }

    /* Next, check for part number/part client */
    if (strlen(prtnum) && strlen(prt_client_id))
    {
        memset(tmp_cfgval, 0, sizeof(tmp_cfgval));
        sprintf(tmp_cfgval, "%s|%s", prtnum, prt_client_id);

        if (misCiStrcmp(alist->cfgtyp, CFGTYP_PRTNUM) == 0 &&
            misCiStrcmp(alist->cfgval, tmp_cfgval) != 0)
        {
            pckrpl_WriteTrc("skipping - Part not match,"
                            " config: %s, replen: %s",
                            alist->cfgval,
                            tmp_cfgval);
            return 0;
        }
    }

    /* Then, check for part family */
    if (strlen(prtfam))
    {
        if (misCiStrcmp(alist->cfgtyp, CFGTYP_PRTFAM) == 0 &&
            misCiStrcmp(alist->cfgval, prtfam) != 0)
        {
            pckrpl_WriteTrc("skipping - Part family not match,"
                            " config: %s, replen: %s",
                            alist->cfgval,
                            prtfam);

            return 0;
        }
    }

    /* Last, check for client */
    if (strlen(client_id))
    {
        /* Check the policies of Area list. */
        if (misCiStrcmp(alist->cfgtyp, CFGTYP_CLIENT) == 0 &&
            misCiStrcmp(alist->cfgval, client_id) != 0)
        {
            pckrpl_WriteTrc("skipping - Client not match,"
                            " config: %s, replen: %s",
                            alist->cfgval,
                            client_id);

            return 0;
        }
    }

    misTrimcpy(bldg_id, 
               sqlIsNull(dstres, dstrow, "src_bldg_id") ? 
                "" : 
                sqlGetString(dstres, dstrow, "src_bldg_id"),
               BLDG_ID_LEN);

    if (misTrimLen(bldg_id, BLDG_ID_LEN) == 0)
    {
        ret = 1;
        return ret;
    }

    /*
     * get the bldg_id for the replen_arecod
     */

    sprintf(buffer,
            "select bldg_id " 
            "  from aremst  "
            " where arecod = '%s' "
            "   and wh_id = '%s' ",
            alist->replen_arecod, 
            alist->wh_id);

    ret_status = sqlExecStr(buffer, &res);

    if (ret_status == eOK)
    {
        row = sqlGetRow(res);
        misTrimcpy(bldg_id, 
                   sqlGetString(res, row, "bldg_id"),
                   BLDG_ID_LEN);

        if (strncmp(bldg_id,
                    sqlGetString(dstres, dstrow, "src_bldg_id"),
                    BLDG_ID_LEN) == 0)
        {
            ret = 1;
        }
    }

    sqlFreeResults(res);
    res = NULL;

    return ret;
}

/*
 * the processed quantity returned should be the quantity that can
 * be effectively picked...for example....if we require 15, but all we
 * find is a pallet of 20, U/C: 10, then we get out of this routine
 * as we have sent more than enough qty to this area, but we can only
 * pick 10 of it...(meaning that the other 10 should possibly get transferred
 * to be processed as a piece pick for 5 later)...
 */
static long sPrcReplenToCase(mocaDataRes * res,
                             mocaDataRow * row,
                             mocaDataRes * dstres,
                             mocaDataRow * dstrow,
                             long qty_to_process,
                             double catch_qty_to_process,
                             long *processed_o,
                             double *catch_qty_processed_o,
                             moca_bool_t *glom_sts_o)
{
    long            ret_status;
    char            buffer[3000];
    char            rplref[RPLREF_LEN + 1];
    mocaDataRes    *u_res;
    mocaDataRow    *u_row;

    RPL_POLICY *policy;
    RPL_AREA_LIST *alist;
    RETURN_STRUCT *ListRes;
    long found_area;
    long            qtyreq, orgqty, tmpqty;
    double          catch_qty_req, org_catch_qty, tmp_catch_qty;
    long            tmp_prcqty, tmp_pckqty;
    double          tmp_prc_catch_qty, tmp_pck_catch_qty;
    long            sum_prcqty;
    double          sum_prc_catch_qty;
    long            untcas;
    long            untpak;
    
    long            min_shelf_hrs = -1L;

    char            orgcodClause[100];
    char            lotnumClause[100];
    char            revlvlClause[100];
    char            untcasClause[100];
    char            untpakClause[100];
    char            supnumClause[100];

    char            alc_search_path[ALC_SEARCH_PATH_LEN + 1];
    char            prtnum[PRTNUM_LEN + 1];
    char            prtfam[PRTFAM_LEN + 1];
    char            client_id[CLIENT_ID_LEN + 1];
    char            prt_client_id[CLIENT_ID_LEN + 1];

    char            orgcod[ORGCOD_LEN + 1];
    char            revlvl[REVLVL_LEN + 1];
    char            lotnum[LOTNUM_LEN + 1];
    char            supnum[SUPNUM_LEN + 1];
    moca_bool_t     splflg;
    moca_bool_t     glom_sts;

    memset(alc_search_path, 0, sizeof(alc_search_path));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prtfam, 0, sizeof(prtfam));
    memset(client_id, 0, sizeof(client_id));
    memset(prt_client_id, 0, sizeof(prt_client_id));

    if (!sqlIsNull(res, row, "alc_search_path"))
        strncpy(alc_search_path,
                sqlGetString(res, row, "alc_search_path"), ALC_SEARCH_PATH_LEN);
    if (!sqlIsNull(res, row, "prtnum"))
        strncpy(prtnum, sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
    if (!sqlIsNull(res, row, "prt_client_id"))
        strncpy(prt_client_id,
                sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
    if (!sqlIsNull(res, row, "prtfam"))
        strncpy(prtfam, sqlGetString(res, row, "prtfam"), PRTFAM_LEN);
    if (!sqlIsNull(res, row, "client_id"))
        strncpy(client_id, sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);

    policy = pckrpl_GetConfig( sqlGetString(res, row, "wh_id") );

    pckrpl_WriteTrc("  Attempting to process case replenishments");

    memset(orgcod, 0, sizeof(orgcod));
    memset(lotnum, 0, sizeof(lotnum));
    memset(revlvl, 0, sizeof(revlvl));
    memset(supnum, 0, sizeof(supnum));

    if (!sqlIsNull(res, row, "orgcod"))
        strncpy(orgcod, sqlGetString(res, row, "orgcod"), ORGCOD_LEN);
    if (!sqlIsNull(res, row, "lotnum"))
        strncpy(lotnum, sqlGetString(res, row, "lotnum"), LOTNUM_LEN);
    if (!sqlIsNull(res, row, "revlvl"))
        strncpy(revlvl, sqlGetString(res, row, "revlvl"), REVLVL_LEN);
    if (!sqlIsNull(res, row, "supnum"))
        strncpy(supnum, sqlGetString(res, row, "supnum"), SUPNUM_LEN);
    if (!sqlIsNull(res, row, "splflg"))
        splflg =  sqlGetBoolean(res, row, "splflg");
    else
        splflg = BOOLEAN_TRUE;

    if (!sqlIsNull(res, row, "min_shelf_hrs"))
        min_shelf_hrs = sqlGetLong(res, row, "min_shelf_hrs");

    untcas = sqlGetLong(res, row, "untcas");
    untpak = sqlGetLong(res, row, "untpak");

    memset(lotnumClause, 0, sizeof(lotnumClause));
    memset(orgcodClause, 0, sizeof(orgcodClause));
    memset(revlvlClause, 0, sizeof(revlvlClause));
    memset(untcasClause, 0, sizeof(untcasClause));
    memset(untpakClause, 0, sizeof(untpakClause));
    memset(supnumClause, 0, sizeof(supnumClause));

    if (!sqlIsNull(res, row, "lotnum"))
        sprintf(lotnumClause, " and lotnum = '%s'",
                sqlGetString(res, row, "lotnum"));
    if (!sqlIsNull(res, row, "orgcod"))
        sprintf(orgcodClause, " and orgcod = '%s'",
                sqlGetString(res, row, "orgcod"));
    if (!sqlIsNull(res, row, "revlvl"))
        sprintf(revlvlClause, " and revlvl = '%s'",
                sqlGetString(res, row, "revlvl"));
    if (!sqlIsNull(res, row, "supnum"))
        sprintf(supnumClause, " and supnum = '%s'",
                sqlGetString(res, row, "supnum"));
    if (untcas > 0 )
        sprintf(untcasClause, " and untcas = %ld", untcas );

    if (untpak > 0 )
        sprintf(untpakClause, " and untpak = %ld", untpak );


    qtyreq = qty_to_process;
    catch_qty_req = catch_qty_to_process;
    orgqty = qtyreq;
    org_catch_qty = catch_qty_req;

    /* Determine what the unique possibilities for untcas... 
     *   we publish out dstare first, just in case someone
     *   has overlayed our standard command and is interested
     *   in what our ultimate dest area is...
     */

    sprintf(buffer,
            "publish data where dstare = '%s' | "
            " list valid untcas for case replenishment "
            " where prtnum = '%s' "
            "   and prt_client_id = '%s' "
            "   and wh_id = '%s' "
            "   and invsts_prg = '%s' "
            "   and untcas <= %d "
            "   %s %s %s %s "
            "   %s "
            "   %s ",
            sqlGetString(dstres, dstrow, "dstare"),
            sqlGetString(res, row, "prtnum"),
            sqlGetString(res, row, "prt_client_id"),
            sqlGetString(res, row, "wh_id"),
            sqlGetString(res, row, "invsts_prg"),
            qty_to_process,
            revlvlClause, supnumClause, lotnumClause, orgcodClause,
            untcasClause, untpakClause);

    ret_status = srvInitiateInline(buffer, &ListRes);
    if (ret_status != eOK)
    {
        pckrpl_WriteTrc("  Could not find any inventory for case replen");
        srvFreeMemory(SRVRET_STRUCT, ListRes);
        *processed_o = 0;
        *catch_qty_processed_o = 0.0;
        /*  If we error'd on the search for product and we are not
        **  to break cases, then we should error with NO_INVENTORY.
        */
        if (splflg == BOOLEAN_FALSE)
        {
            sSetError(_ERROR_NO_INVENTORY, "");
        }
        return (ret_status == eDB_NO_ROWS_AFFECTED ? 
                eRPL_NORMAL_FAILURE : ret_status);
    }
    
    u_res = srvGetResults(ListRes);

    /*
     *        A word about the quantities here...
     *      orgqty = the original quantity of the replenishment
     *      casprc = the amount processed that is case pickable...
     *      pceprc = the amount processed that is piece pickable...
     *      processed = the cumulative amount processed by this routine
     *      qtyreq = the current remaining requested quantity.
     *      tmpqty = the requested quantity (as defined by the untcas
     *               of the current loop)
     */
    sum_prcqty = 0;
    sum_prc_catch_qty = 0.0;
    found_area = 0;
    for (alist = policy->CasePath; alist; alist = alist->next)
    {
        if (sIsPolicyAreaToReplenish(alist,
                                     dstres,
                                     dstrow,
                                     alc_search_path,
                                     prtnum,
                                     prtfam,
                                     client_id,
                                     prt_client_id) == 0)
        {
            continue;
        }
        pckrpl_WriteTrc("     Found match - attempting to replen: %s",
                        alist->replen_arecod);
        found_area++;
        for (u_row = sqlGetRow(u_res); u_row; u_row = sqlGetNextRow(u_row))
        {

            /* Use this untcas to attempt to replenish... */
            untcas = sqlGetLong(u_res, u_row, "untcas");

            tmpqty = qtyreq / untcas;
            tmpqty = tmpqty * untcas;
            tmp_catch_qty = catch_qty_req;

            if (tmpqty <= 0 )
            {
                pckrpl_WriteTrc("  Skipping iteration for u/c = %d as it is"
                                " larger than what is needed (%d)",
                                untcas, qtyreq);
                continue;
            }

            pckrpl_WriteTrc("   Processing %s as a case replenishment. \n"
                            "   Part: %s  PrtClient: %s WH_ID: %s "
                            "   Qty: %ld  Catch Qty: %f U/C: %ld",
                            sqlGetString(res, row, "rplref"),
                            sqlGetString(res, row, "prtnum"),
                            sqlGetString(res, row, "prt_client_id"),
                            sqlGetString(res, row, "wh_id"),
                            tmpqty, tmp_catch_qty, untcas);

            if (!sqlIsNull(res, row, "wkonum"))
            {
                pckrpl_WriteTrc("  Wkonum: %s, wkorev: %s, wkolin: %s "
                                " client_id: %s",
                                sqlGetString(res, row, "wkonum"),
                                sqlGetString(res, row, "wkorev"),
                                sqlGetString(res, row, "wkolin"),
                                sqlGetString(res, row, "client_id"),
                                sqlGetString(res, row, "wh_id"));
            }
            else if (!sqlIsNull(res, row, "ship_id"))
            {
                pckrpl_WriteTrc("   ship_id: %s, ordnum: %s, ordlin: %s "
                                " ordsln: %s, client_id: %s",
                                sqlGetString(res, row, "ship_id"),
                                sqlGetString(res, row, "ordnum"),
                                sqlGetString(res, row, "ordlin"),
                                sqlGetString(res, row, "ordsln"),
                                sqlGetString(res, row, "client_id"),
                                sqlGetString(res, row, "wh_id"));
            }

            sqlSetSavepoint("prcCasePickStart");

            tmp_prcqty = tmp_pckqty = 0;
            tmp_prc_catch_qty = tmp_pck_catch_qty = 0.0;
            ret_status = sReplenishArea(tmpqty,
                                        tmp_catch_qty,
                                        &tmp_prcqty,
                                        &tmp_prc_catch_qty,
                                        &tmp_pckqty,
                                        &tmp_pck_catch_qty,
                                        alist->replen_lodlvl,
                                        alist->replen_arecod,
                                        sqlGetString(res, row, "wh_id"),
                                        sqlGetString(res, row, "prtnum"),
                                        sqlGetString(res, 
                                                     row, "prt_client_id"),
                                        orgcod,
                                        revlvl,
                                        supnum,
                                        lotnum,
                                        sqlIsNull(res, row, "invsts") ?
                                            "":sqlGetString(res, row, "invsts"),
                                        sqlGetString(res, row, "invsts_prg"),
                                        "TMPCASREF",
                                        0,
                                        alist,
                                        sqlGetString(res, row, "pcksts"),
                                        &splflg,
                                        &untcas,
                                        &untpak,
                                        &glom_sts,
                                        min_shelf_hrs);
            
            if (ret_status == eRPL_NORMAL_FAILURE)
            {
                sqlRollbackToSavepoint("prcCasePickStart");
                pckrpl_WriteTrc("  Unable to replenish area....continuing...");
                continue;
            }
            if (ret_status != eOK)
            {
                sqlRollbackToSavepoint("prcCasePickStart");
                pckrpl_WriteTrc(" FAILURE (%d) while attempting to replenish"
                                " area - getting out",
                                ret_status);
                return(ret_status);
            }
                
            sum_prcqty += tmp_prcqty;
            sum_prc_catch_qty += tmp_prc_catch_qty;

            if (tmp_pckqty <= 0)
            {
                sqlRollbackToSavepoint("prcCasePickStart");
                continue;
            }
            else
            {
                qtyreq -= tmp_pckqty;
                catch_qty_req -= tmp_pck_catch_qty;

                /* We successfully processed some...go ahead and
                   create the parent... */
                memset(rplref, 0, sizeof(rplref));
                ret_status = appNextNum(NUMCOD_RPLREF, rplref);
                if (ret_status != eOK)
                {
                    pckrpl_WriteTrc("    Failed on generating rplref (%ld)!",
                                    ret_status);

                    sqlRollbackToSavepoint("prcCasePickStart");
                    srvFreeMemory(SRVRET_STRUCT, ListRes);
                    return (ret_status);
                }
                /* The parent in this case looks like an order... */

                /* Copy the original replen...changing the rplref
                   and quantity... */
                if (tmp_pckqty > 0)
                {
                    sprintf(buffer,
                            "insert into rplwrk ( "
                                "rplref, schbat, "
                                "ship_line_id, dstare, "
                                "stcust, rtcust, concod, "
                                "pckqty, alcqty, "
                                "pck_catch_qty, alc_catch_qty, "
                                "rplcnt, untcas, untpak, "
                                "rplsts, prtnum, prt_client_id, "
                                "orgcod, "
                                "revlvl, supnum, lotnum, invsts, "
                                "invsts_prg, "
                                "lodlvl, adddte, alcdte, "
                                "pcksts, splflg, dstloc, "
                                "ship_id, ordnum, ordlin, "
                                "ordsln, client_id, "
                                "wkonum, wkorev, wkolin, wh_id, "
                                "min_shelf_hrs) "
                            "select '%s', schbat, "
                                "ship_line_id, dstare, "
                                "stcust, rtcust, concod, "
                                "%ld, %d, "
                                "%f, %f, "
                                "%d, %ld, %ld, "
                                "'%s', prtnum, prt_client_id, orgcod, "
                                "revlvl, supnum, lotnum, invsts, invsts_prg, "
                                "lodlvl, adddte, %s, pcksts, splflg, dstloc, "
                                "ship_id, ordnum, ordlin, ordsln, client_id, "
                                "wkonum, wkorev, wkolin, wh_id, min_shelf_hrs "
                              "from rplwrk "
                             "where rplref = '%s' ",
                            rplref,
                            tmp_pckqty, 0,
                            tmp_pck_catch_qty, 0.0,
                            0, untcas, untpak,
                            RPLSTS_PENDDEP, "sysdate",
                            sqlGetString(res, row, "rplref"));

                    ret_status = sqlExecStr(buffer, NULL);
                    if (ret_status != eOK)
                    {
                        pckrpl_WriteTrc("   Unable to create new parent(%d)! ",
                                        ret_status);
                        sqlRollbackToSavepoint("prcCasePickStart");
                        srvFreeMemory(SRVRET_STRUCT, ListRes);
                        return (ret_status);
                    }
                }
                else
                {

                    pckrpl_WriteTrc("ERROR! Was about to insert "
                                    "rplwrk of %ld!\n",
                                    tmp_pckqty);

                    sqlRollbackToSavepoint("prcCasePickStart");
                    srvFreeMemory(SRVRET_STRUCT, ListRes);
                    ret_status = eERROR;
                    return (ret_status);
                }

                /* Next change the child rplref that was
                   pointing at our temp holder... */
                sprintf(buffer,
                        "update rplwrk "
                        "   set parref = '%s' "
                        " where parref = 'TMPCASREF' ", 
                        rplref);
                ret_status = sqlExecStr(buffer, NULL);
                if (ret_status != eOK)
                {
                    pckrpl_WriteTrc("Unable to update temp parref (%d)!",
                                    ret_status);
                    sqlRollbackToSavepoint("prcCasePickStart");
                    srvFreeMemory(SRVRET_STRUCT, ListRes);
                    return (ret_status);
                }


                /* Update the original rplref - attempt delete first */
                sprintf(buffer,
                        "delete from rplwrk "
                        " where rplref = '%s' "
                        "   and pckqty = %ld "
                        "   and (pck_catch_qty = %f or "
                        "        pck_catch_qty is null) ",
                        sqlGetString(res, row, "rplref"),
                        tmp_pckqty,
                        tmp_pck_catch_qty);

                ret_status = sqlExecStr(buffer, NULL);
                if (ret_status != eOK)
                {

                    /* If that didn't work, then update... */
                    sprintf(buffer,
                            "update rplwrk "
                               "set pckqty = pckqty - %ld, "
                                   "pck_catch_qty = pck_catch_qty - %f "
                             "where rplref = '%s' "
                               "and pckqty > %ld ",
                            tmp_pckqty,
                            tmp_pck_catch_qty,
                            sqlGetString(res, row, "rplref"),
                            tmp_pckqty);

                    ret_status = sqlExecStr(buffer, NULL);
                    if (ret_status != eOK)
                    {
                        pckrpl_WriteTrc("\nERROR Failed upd of rplwrk to "
                                        "negative (2)");

                        pckrpl_WriteTrc("Update of orig rplwrk failed "
                                        " and so did the delete! (%d)",
                                        ret_status);
                        sqlRollbackToSavepoint("prcCasePickStart");
                        srvFreeMemory(SRVRET_STRUCT, ListRes);
                        return (ret_status);
                    }

                    if (glom_sts == BOOLEAN_TRUE)
                    {
                        pckrpl_WriteTrc("\n  Replenishment generated.  "
                            "Qty: %ld Catch Qty: %f New Parent: %s ",
                            tmp_pckqty, tmp_pck_catch_qty, rplref);
                        /* We glommed a portion of this onto another */
                        /* replen, so we're going to get out of here */
                        /* now and let this replenishment process run */
                        /* against the newly modified rplwrk with the */
                        /* next pass through. */

                        *processed_o += tmp_pckqty;
                        *catch_qty_processed_o += tmp_pck_catch_qty;
                        *glom_sts_o = glom_sts;
                        srvFreeMemory(SRVRET_STRUCT, ListRes);
                        return (eOK);
                    }
                }
                pckrpl_WriteTrc("\n    Replenishment generated.  "
                    "Qty: %ld Catch Qty: %f"
                    " New Parent: %s ", tmp_pckqty, tmp_pck_catch_qty, rplref);

                /* Update processed */
                *processed_o += tmp_pckqty;
                *catch_qty_processed_o += tmp_pck_catch_qty;
                if (sum_prcqty >= orgqty && 
                    sum_prc_catch_qty >= org_catch_qty)
                {
                    srvFreeMemory(SRVRET_STRUCT, ListRes);
                    return (eOK);
                }
            }
        }
    }
    srvFreeMemory(SRVRET_STRUCT, ListRes);
    if (found_area == 0)
    {
        pckrpl_WriteTrc("\n  WARNING! Dest Area: %s has no REPLEN_PATH "
                        "policies configured \n",
                        sqlGetString(res, row, "dstare"));
        sSetError(_ERROR_NO_LOCATIONS, "");
    }
    pckrpl_WriteTrc("  Done with piece replenishment iteration");


    return (eOK);
}

static long sPrcReplenToPiece(mocaDataRes * res,
                              mocaDataRow * row,
                              mocaDataRes * dstres,
                              mocaDataRow * dstrow,
                              long qty_to_process,
                              double catch_qty_to_process,
                              long *processed,
                              double *catch_qty_processed,
                              moca_bool_t *glom_sts_o)
{
    long            ret_status;
    char            buffer[3000];
    char            rplref[RPLREF_LEN + 1];

    long            qtyreq, orgqty;
    double          catch_qty_req, org_catch_qty;
    long            sum_prcqty = 0;
    double          sum_prc_catch_qty = 0;
    long            tmp_prcqty, tmp_pckqty;
    double          tmp_prc_catch_qty, tmp_pck_catch_qty;
    long            untcas, untpak;
    long            min_shelf_hrs = -1L;

    long found_area;
    char            orgcodClause[100];
    char            lotnumClause[100];
    char            revlvlClause[100];
    char            supnumClause[100];
    char            orgcod[ORGCOD_LEN + 1];
    char            revlvl[REVLVL_LEN + 1];
    char            lotnum[LOTNUM_LEN + 1];
    char            supnum[SUPNUM_LEN + 1];

    char            alc_search_path[ALC_SEARCH_PATH_LEN + 1];
    char            prtnum[PRTNUM_LEN + 1];
    char            prtfam[PRTFAM_LEN + 1];
    char            client_id[CLIENT_ID_LEN + 1];
    char            prt_client_id[CLIENT_ID_LEN + 1];

    moca_bool_t     splflg;
    RPL_AREA_LIST   *alist;
    RPL_POLICY *policy;
    moca_bool_t     glom_sts;

    memset(orgcod, 0, sizeof(orgcod));
    memset(lotnum, 0, sizeof(lotnum));
    memset(revlvl, 0, sizeof(revlvl));
    memset(supnum, 0, sizeof(supnum));

    if (!sqlIsNull(res, row, "orgcod"))
        strncpy(orgcod, sqlGetString(res, row, "orgcod"), ORGCOD_LEN);
    if (!sqlIsNull(res, row, "lotnum"))
        strncpy(lotnum, sqlGetString(res, row, "lotnum"), LOTNUM_LEN);
    if (!sqlIsNull(res, row, "revlvl"))
        strncpy(revlvl, sqlGetString(res, row, "revlvl"), REVLVL_LEN);
    if (!sqlIsNull(res, row, "supnum"))
        strncpy(supnum, sqlGetString(res, row, "supnum"), SUPNUM_LEN);
    if (!sqlIsNull(res, row, "splflg"))
        splflg =  sqlGetBoolean(res, row, "splflg");
    else
        splflg = BOOLEAN_TRUE;

    if (!sqlIsNull(res, row, "min_shelf_hrs"))
        min_shelf_hrs = sqlGetLong(res, row, "min_shelf_hrs");

    untcas = sqlGetLong(res, row, "untcas");
    untpak = sqlGetLong(res, row, "untpak");

    memset(lotnumClause, 0, sizeof(lotnumClause));
    memset(orgcodClause, 0, sizeof(orgcodClause));
    memset(revlvlClause, 0, sizeof(revlvlClause));
    memset(supnumClause, 0, sizeof(supnumClause));

    if (!sqlIsNull(res, row, "lotnum"))
        sprintf(lotnumClause, " and lotnum = '%s'",
                sqlGetString(res, row, "lotnum"));
    if (!sqlIsNull(res, row, "orgcod"))
        sprintf(orgcodClause, " and orgcod = '%s'",
                sqlGetString(res, row, "orgcod"));
    if (!sqlIsNull(res, row, "revlvl"))
        sprintf(revlvlClause, " and revlvl = '%s'",
                sqlGetString(res, row, "revlvl"));
    if (!sqlIsNull(res, row, "supnum"))
        sprintf(supnumClause, " and supnum = '%s'",
                sqlGetString(res, row, "supnum"));

    qtyreq = qty_to_process;
    catch_qty_req = catch_qty_to_process;
    orgqty = qtyreq;
    org_catch_qty = catch_qty_req;

    memset(alc_search_path, 0, sizeof(alc_search_path));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(prtfam, 0, sizeof(prtfam));
    memset(client_id, 0, sizeof(client_id));

    if (!sqlIsNull(res, row, "alc_search_path"))
        strncpy(alc_search_path,
                sqlGetString(res, row, "alc_search_path"), ALC_SEARCH_PATH_LEN);
    if (!sqlIsNull(res, row, "prtnum"))
        strncpy(prtnum, sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
    if (!sqlIsNull(res, row, "prt_client_id"))
        strncpy(prt_client_id,
                sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
    if (!sqlIsNull(res, row, "prtfam"))
        strncpy(prtfam, sqlGetString(res, row, "prtfam"), PRTFAM_LEN);
    if (!sqlIsNull(res, row, "client_id"))
        strncpy(client_id, sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);

    policy = pckrpl_GetConfig( sqlGetString(res, row, "wh_id") );
    pckrpl_WriteTrc("  Attempting to process piece replenishment");

    found_area = 0;
    for (alist = policy->PiecePath; alist; alist = alist->next)
    {
        if (sIsPolicyAreaToReplenish(alist,
                                     dstres,
                                     dstrow,
                                     alc_search_path,
                                     prtnum,
                                     prtfam,
                                     client_id,
                                     prt_client_id) == 0)
        {
            continue;
        }
        found_area++;

        pckrpl_WriteTrc("   Processing %s as a piece replenishment. "
            "Part: %s  PrtClient: %s  Qty: %ld  Catch Qty: %f ",
                        sqlGetString(res, row, "rplref"),
                        sqlGetString(res, row, "prtnum"),
                        sqlGetString(res, row, "prt_client_id"),
                        qtyreq,
                        catch_qty_req);

        if (!sqlIsNull(res, row, "wkonum"))
        {
            pckrpl_WriteTrc("  Wkonum: %s, wkorev: %s, wkolin: %s "
                            " client_id: %s",
                            sqlGetString(res, row, "wkonum"),
                            sqlGetString(res, row, "wkorev"),
                            sqlGetString(res, row, "wkolin"),
                            sqlGetString(res, row, "client_id"),
                            sqlGetString(res, row, "wh_id"));
        }
        else if (!sqlIsNull(res, row, "ship_id"))
        {
            pckrpl_WriteTrc("   ship_id: %s, ordnum: %s, ordlin: %s "
                            " ordsln: %s, client_id: %s",
                            sqlGetString(res, row, "ship_id"),
                            sqlGetString(res, row, "ordnum"),
                            sqlGetString(res, row, "ordlin"),
                            sqlGetString(res, row, "ordsln"),
                            sqlGetString(res, row, "client_id"),
                            sqlGetString(res, row, "wh_id"));
        }

        sqlSetSavepoint("prcPiecePickStart");

        tmp_prcqty = tmp_pckqty = 0;
        ret_status = sReplenishArea(qtyreq,
                                    catch_qty_req,
                                    &tmp_prcqty,
                                    &tmp_prc_catch_qty,
                                    &tmp_pckqty,
                                    &tmp_pck_catch_qty,
                                    alist->replen_lodlvl,
                                    alist->replen_arecod,
                                    alist->wh_id,
                                    sqlGetString(res, row, "prtnum"),
                                    sqlGetString(res, row, "prt_client_id"),
                                    orgcod,
                                    revlvl,
                                    supnum,
                                    lotnum,
                                    sqlIsNull(res, row, "invsts") ?
                                        "" : sqlGetString(res, row, "invsts"),
                                    sqlGetString(res, row, "invsts_prg"),
                                    "TMPPCEREF",
                                    0,
                                    alist,
                                    sqlGetString(res, row, "pcksts"),
                                    &splflg,
                                    &untcas,
                                    &untpak,
                                    &glom_sts, 
                                    min_shelf_hrs);
        
        if (ret_status != eOK)
        {
            sqlRollbackToSavepoint("prcPiecePickStart");
            pckrpl_WriteTrc("Unable to replenish area (%s) for piece pick",
                            alist->replen_arecod);
            continue;
        }
        /* if we get any product there (case or otherwise) we can pcepck it */
        sum_prcqty += tmp_prcqty;
        sum_prc_catch_qty += tmp_prc_catch_qty;

        if (tmp_pckqty > 0)
        {
            qtyreq -= tmp_pckqty;
            catch_qty_req -= tmp_pck_catch_qty;

            /* We successfully processed some...go ahead and
               create the parent... */
            memset(rplref, 0, sizeof(rplref));
            ret_status = appNextNum(NUMCOD_RPLREF, rplref);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("Unable to get next rplref! (%d)",
                                ret_status);
                sqlRollbackToSavepoint("prcPiecePickStart");
                return (ret_status);
            }
            /* The parent in this case looks like an order... */

            /* Copy the original replen...changing the rplref
               and quantity... */
            sprintf(buffer,
                    "insert into rplwrk (rplref, schbat, "
                    "                         ship_line_id, dstare, "
                    "                         stcust, rtcust, concod, "
                    "                         pckqty, alcqty, "
                    "                    pck_catch_qty, alc_catch_qty, "
                    "                         rplcnt, untcas, untpak, "
                    "                         rplsts, prtnum, prt_client_id, orgcod, "
                    "                         revlvl, supnum, lotnum, invsts, "
                    "                    invsts_prg, "
                    "                         lodlvl, adddte, alcdte, "
                    "                         pcksts, splflg, dstloc, "
                    "                    ship_id, ordnum, ordlin, ordsln, "
                    "                    client_id, "
                    "                         wkonum, wkorev, wkolin, wh_id, "
                    "                         min_shelf_hrs) "
                    " select '%s', schbat, "
                    "             ship_line_id, dstare, "
                    "             stcust, rtcust, concod, "
                    "             %ld, %d, %f, %f, "
                    "             %d, untcas, untpak, "
                    "             '%s', prtnum, prt_client_id, orgcod, "
                    "             revlvl, supnum, lotnum, invsts, "
                    "        invsts_prg, "
                    "        lodlvl, adddte, %s, pcksts, splflg, dstloc, "
                    "        ship_id, ordnum, ordlin, ordsln, "
                    "        client_id, "
                    "             wkonum, wkorev, wkolin, wh_id, min_shelf_hrs "
                    "  from rplwrk "
                    " where rplref = '%s' ",
                    rplref,
                    tmp_pckqty, 0, tmp_pck_catch_qty, 0.0,
                    0, 
                    RPLSTS_PENDDEP, "sysdate",
                    sqlGetString(res, row, "rplref"));

            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("Unable to create new parent (%d)!",
                                ret_status);
                sqlRollbackToSavepoint("prcPiecePickStart");
                return (ret_status);
            }

            /* Next change the child rplref that was
               pointing at our temp holder... */
            sprintf(buffer,
                    "update rplwrk "
                    "   set parref = '%s' "
                    " where parref = 'TMPPCEREF' ", rplref);
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("Unable to update temp parref (%d)!",
                                ret_status);
                sqlRollbackToSavepoint("prcPiecePickStart");
                return (ret_status);
            }


            /* Update the original rplref - attempt delete first */
            /* if remaining pckqty is zero, we can delete original repl, 
               otherwise update pckqty to remaining pckqty. */
            sprintf(buffer,
                    "delete from rplwrk "
                    " where rplref = '%s' "
                    "   and pckqty = %ld "
                    "   and (pck_catch_qty = %f or "
                    "        pck_catch_qty is null) ",
                    sqlGetString(res, row, "rplref"),
                    tmp_pckqty,
                    tmp_pck_catch_qty);

            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {

                /* If that didn't work, then update... */
                sprintf(buffer,
                        "update rplwrk "
                        "   set pckqty = pckqty - %ld, "
                        "       pck_catch_qty = pck_catch_qty - %f "
                        " where rplref = '%s'"
                        "   and pckqty > %ld ",
                        tmp_pckqty,
                        tmp_pck_catch_qty,
                        sqlGetString(res, row, "rplref"),
                        tmp_pckqty);

                ret_status = sqlExecStr(buffer, NULL);
                if (ret_status != eOK)
                {
                    /* 
                     * Try one more time assuming we are overallocating pckqty
                     * due to catch quantity allocations 
                     */
                    if(tmp_pck_catch_qty > 0.0)
                    {
                        sprintf(buffer,
                            "update rplwrk "
                               "set pck_catch_qty = pck_catch_qty - %f "
                             "where rplref = '%s' "
                               "and pck_catch_qty > %f ",
                            tmp_pck_catch_qty,
                            sqlGetString(res, row, "rplref"),
                            tmp_pck_catch_qty);

                        ret_status = sqlExecStr(buffer, NULL);
                    }

                    if(ret_status != eOK)
                    {
                        pckrpl_WriteTrc("ERROR Failed upd of rplwrk to "
                                        "negative (3)");

                        pckrpl_WriteTrc("Update of orig rplwrk failed "
                                        " and so did the delete! (%d)",
                                        ret_status);
                        sqlRollbackToSavepoint("prcPiecePickStart");
                        return (ret_status);
                    }
                }

                if (glom_sts == BOOLEAN_TRUE)
                {
                    pckrpl_WriteTrc("\n  Replenishment generated.  Qty: %ld "
                        " Catch Qty: %f New Parent: %s ", 
                        tmp_pckqty, tmp_pck_catch_qty, rplref);
                    pckrpl_WriteTrc("\n Glommed a portion of rplref %s. "
                                    "We'll leave the decremented portion "
                                    "of this rplref out there and re-process"
                                    "it with the next run of the "
                                    "replenishment manager. ",
                                    sqlGetString(res, row, "rplref"));

                    /* We glommed a portion of this onto another */
                    /* replen, so we're going to get out of here */
                    /* now and let this replenishment process run */
                    /* against the newly modified rplwrk with the */
                    /* next pass through. */

                    *glom_sts_o = glom_sts;

                    return (eOK);
                }
            }
            pckrpl_WriteTrc("\n  Replenishment generated.  Qty: %ld "
                "Catch Qty: %f New Parent: %s ", 
                tmp_pckqty, tmp_pck_catch_qty, rplref);

            /* Update processed */
            *processed += tmp_pckqty;
            *catch_qty_processed += tmp_pck_catch_qty;
            if (sum_prcqty >= orgqty && 
                sum_prc_catch_qty >= org_catch_qty)
            {
                return (eOK);
            }
        }
        else if (tmp_pckqty < 0 || 
            tmp_pck_catch_qty < 0.0)
        {
            pckrpl_WriteTrc("ERROR Was about to insert a rplwrk of '%ld' '%f' (2)!",
                            tmp_pckqty,
                            tmp_pck_catch_qty);
        }
    }
    if (found_area == 0)
    {
        pckrpl_WriteTrc("\n  WARNING! Dest Area: %s has no REPLEN_PATH "
                        "policies configured \n",
                        sqlGetString(res, row, "dstare"));
        sSetError(_ERROR_NO_LOCATIONS, "");
    }
    pckrpl_WriteTrc("  Done with piece replenishment iteration");
    return (eOK);
}
static long sProcessWorkOrderGeneration(char *rplref)
{

    long ret_status;
    char buffer[2000];

    sprintf(buffer,
            "process replenishment work order "
            " where rplref = '%s' ",
            rplref);

    ret_status = srvInitiateInline(buffer, NULL);

    if (ret_status == eOK)
    {
        pckrpl_WriteTrc("\nSuccessfully generated work order for rplref: %s",
                        rplref);
        sClearError();

        return(eOK);
    }
    else
    {
        if (ret_status == eRPL_PART_IS_NOT_BOM)
        {
            pckrpl_WriteTrc("  Part requested is not a BOM part "
                            "- skipping work order");
            return(eRPL_NORMAL_FAILURE);
        }
        else if (ret_status == eRPL_BOM_IS_NOT_AUTO_GENERATE)
        {
            pckrpl_WriteTrc("  BOM for part requested is set "
                            "to not auto-generate - skipping");
            return(eRPL_NORMAL_FAILURE);
        }
        else if (ret_status == eINT_RESCOD_ALREADY_ASSIGNED)
        {
            char buffer[2000];
            long ret_status;
            char msg[100];
            RETURN_STRUCT *CmdRes;
            mocaDataRes *tres;
            mocaDataRow *trow;
            
            pckrpl_WriteTrc("  BOM requires specific location"
                            "  but it is currently busy");

            /* Have to find the inventory status to use */
            sprintf(buffer, 
                    "[select prtnum, prt_client_id, invsts, invsts_prg, "
                    "        revlvl, supnum, orgcod, lotnum, wh_id "
                    "   from rplwrk "
                    "  where rplref = '%s'] "
                    "| "
                    "if (@invsts = '') "
                    "{"
                    "    [select invsts "
                    "       from prgmst "
                    "      where invsts_prg = @invsts_prg "
                    "        and alcflg = 1 "
                    "        and rownum = 1 "
                    "   order by srtseq] "
                    "} "
                    "| "
                    " get matching bom ",
                    rplref);
            ret_status = srvInitiateCommand(buffer, &CmdRes);
            if (ret_status != eOK)
            {
                srvFreeMemory(SRVRET_STRUCT, CmdRes);
                sSetError(_ERROR_PRODUCTION_LINE_BUSY, 
                          "Production line is busy");
                return(eRPL_NORMAL_FAILURE);
            }
            else
            {
                tres = srvGetResults(CmdRes);
                trow = sqlGetRow(tres);

                if (!sqlIsNull(tres, trow, "prdlin"))
                {
                    sprintf(msg, 
                            "Production line: %s is busy",
                            sqlGetString(tres, trow, "prdlin"));
                    srvFreeMemory(SRVRET_STRUCT, CmdRes);
                    sSetError(_ERROR_PRODUCTION_LINE_BUSY, 
                              "Production line is busy");
                    return(eRPL_NORMAL_FAILURE);
                }
                else
                {
                    srvFreeMemory(SRVRET_STRUCT, CmdRes);
                    sSetError(_ERROR_PRODUCTION_LINE_BUSY, 
                              "Production line is busy");
                    return(eRPL_NORMAL_FAILURE);
                }
            }
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            pckrpl_WriteTrc("  Replenishment is missing or the shipment "
                            "is missing - looks like it got cancelled "
                            "out from under us. ");
            return(eRPL_NORMAL_FAILURE);
        }
        return(ret_status);
    }

}

LIBEXPORT
RETURN_STRUCT *varAllocateEmergencyReplenishment(char *rplref_i,
                                                 char *wh_id_i,
                                                 moca_bool_t *comflg_i)
{
    char            buffer[2000];
    char            rplref_wherebuf[300];
    char            pcksts_wherebuf[300];
    char            dstare[ARECOD_LEN + 1];
    mocaDataRes    *res;
    mocaDataRow    *row;
    mocaDataRes    *dstres;
    mocaDataRow    *dstrow;
    RETURN_STRUCT  *CurPtr;

    moca_bool_t     bLookingSpecific;
    moca_bool_t     bCommitAsWeGo;
    char            rplref[RPLREF_LEN+1];
    char            wh_id[WH_ID_LEN+1];
    char            error_status[RPLSTS_LEN + 1];
    long            pckqty;
    double          pck_catch_qty;
    long            ret_status;
    long            qty_processed;
    double          catch_qty_processed;
    long            tmp_pckqty;
    double          tmp_pck_catch_qty;
    long            tmp_qty_processed;
    double          tmp_catch_qty_processed;
    moca_bool_t     glom_sts;

    RPL_POLICY *policy;

    if (wh_id_i && misTrimLen(wh_id_i, RPLREF_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

    policy = pckrpl_GetConfig(wh_id);

    if (!policy)
    {
        return(APPError(eRPL_NOT_INSTALLED));
    }

    pckrpl_WriteTrc("\n*** Beginning Allocation of "
                    "Issued Replenishments ***\n");

    memset(rplref, 0, sizeof(rplref));
    if (rplref_i && misTrimLen(rplref_i, RPLREF_LEN))
        misTrimcpy(rplref, rplref_i, RPLREF_LEN);

    if (comflg_i && *comflg_i)
        bCommitAsWeGo = BOOLEAN_TRUE;
    else
        bCommitAsWeGo = BOOLEAN_FALSE;
        

    /* Select the outstanding replenishment work records to find what 
     * we need to process 
     */

    memset(rplref_wherebuf, 0, sizeof(rplref_wherebuf));
    if (strlen(rplref))
    {
        bLookingSpecific = BOOLEAN_TRUE;
        sprintf(rplref_wherebuf, " and rplwrk.rplref = '%s' ",
                rplref);
    }

    memset(pcksts_wherebuf, 0, sizeof(pcksts_wherebuf));
    if (policy->skip_held_pcksts)
    {
        sprintf(pcksts_wherebuf,
                " and rplwrk.pcksts != '%s' ",
                PCKSTS_HOLD);
    }

    sprintf(buffer,
            "select rplwrk.*, ord_line.alc_search_path, prtmst_view.prtfam "
            "  from rplwrk "
            "   left outer join ord_line "
            "           on rplwrk.ordnum = ord_line.ordnum "
            "          and rplwrk.ordlin = ord_line.ordlin "
            "          and rplwrk.ordsln = ord_line.ordsln "
            "          and rplwrk.client_id = ord_line.client_id "
            "          and rplwrk.wh_id = ord_line.wh_id "
            "   left outer join prtmst_view "
            "           on rplwrk.prtnum = prtmst_view.prtnum "
            "          and rplwrk.prt_client_id = prtmst_view.prt_client_id "
            "          and rplwrk.wh_id = prtmst_view.wh_id "
            " where rplwrk.rplsts = '%s' %s %s "
            "   and rplwrk.wh_id = '%s' "
            " order by rplwrk.wh_id, rplwrk.rplref ",
            RPLSTS_ISSUED, rplref_wherebuf, pcksts_wherebuf, wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        pckrpl_WriteTrc("   No outstanding replenishment "
                        "requests require allocation processing");
        if (bLookingSpecific)
        {
            pckrpl_WriteTrc("  Failed to get information for specific "
                            "rplref: %s - returning status %d",
                            rplref, ret_status);
            sqlFreeResults(res);
            return(APPError(ret_status));
        }
    }

    /* Process all the replenishment work records */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        sClearError();

        /* We only care about replenishments which are either for
         * and order/shipment or for a work order
         *
         * The most reliable way to check this is to first consider
         * if the replenishment has a wkonum filled in, if not, then
         * check if there is a ship_id.  If both are NULL, we don't have
         * to do anything
         */
        if (sqlIsNull(res, row, "ship_id") && sqlIsNull(res, row, "wkonum"))
        {
            pckrpl_WriteTrc("Rplref: %s is not for a shipment or work order"
                            " - nothing to do, skipping...",
                            sqlGetString(res, row, "rplref"));
            continue;
        }
        /* Since we're sticking around to process, let's go ahead and
         * lock the record.
         */
        sprintf(buffer,
                "select 1 "
                "  from rplwrk "
                " where rplref = '%s' "
                " for update of pckqty",
                sqlGetString(res, row, "rplref"));
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            pckrpl_WriteTrc("Failed(%d) to lock rplref: %s for processing."
                            " We're going to skip it for now.",
                            ret_status, sqlGetString(res, row, "rplref"));
            continue;
        }

        pckqty = sqlGetLong(res, row, "pckqty");
        if(!sqlIsNull(res, row, "pck_catch_qty"))
        {
            pck_catch_qty = sqlGetFloat(res, row, "pck_catch_qty");
        }
        else
        {
            pck_catch_qty = 0.0;
        }

        /* Attempt to allocate for the shipment/work order.. */
        
        ret_status = sPrcReplenDemand(res, row, 
            &qty_processed,
            &catch_qty_processed);
        if (ret_status == eRPL_NORMAL_FAILURE)
        {
            qty_processed = 0;
            catch_qty_processed = 0;
        }
        else if (ret_status != eOK)
        {
            pckrpl_WriteTrc("Error %d returned attempt to "
                            "allocate to order - skipping", ret_status);
            continue;
        }


        if (qty_processed >= pckqty && 
            catch_qty_processed >= pck_catch_qty)
        {
            if (bCommitAsWeGo)
                srvCommit();

            pckrpl_WriteTrc("Finished allocation processing of rplref: %s",
                            sqlGetString(res, row, "rplref"));
            continue;
        }

        pckqty -= qty_processed;
        pck_catch_qty -= catch_qty_processed;

        qty_processed = 0;
        catch_qty_processed = 0;

        /* Note - if we glom a portion of a rplwrk onto another rplwrk, */
        /* we will create a new rplwrk for the glommed portion and */
        /* decrement the original. Since the original now looks completely */
        /* different from when we started, we're not going to process */
        /* it further at this time. Instead we'll leave it out there in */
        /* its Issued status and let the replenishment manager re-process */
        /* it with its new, updated quantity on the next run. */
           
        glom_sts = BOOLEAN_FALSE;
        
        misTrimcpy(dstare, sqlGetString(res, row, "dstare"), ARECOD_LEN);
        
        if (misTrimLen(dstare, ARECOD_LEN))
        {
            sprintf(buffer,
                    "publish data where dstare = '%s'",
                    sqlGetString(res, row, "dstare"));
        }
        else
        {
            sprintf(buffer,
                    " list shipment destination locations "
                    "where ship_id = '%s' "
                    "  and wh_id = '%s' ",
                    sqlGetString(res, row, "ship_id"),
                    sqlGetString(res, row, "wh_id"));
        }
        
        ret_status = srvInitiateCommand(buffer, &CurPtr);
        
        if (ret_status != eOK)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            sqlFreeResults(res);
            return (srvResults(ret_status, NULL));
        }
        
        dstres = srvGetResults(CurPtr);
        tmp_pckqty = pckqty;
        tmp_pck_catch_qty = pck_catch_qty;
        
        /* Loop through destination locations until we have either
         * allocated enough to them or we have run out of destinations
         */
        for (dstrow = sqlGetRow(dstres); 
                dstrow && (tmp_pckqty > 0 || tmp_pck_catch_qty > 0); 
                dstrow = sqlGetNextRow(dstrow))
        {
            tmp_qty_processed = 0;
            tmp_catch_qty_processed = 0;
            ret_status = sPrcReplenToCase(
                res,
                row,
                dstres,
                dstrow,
                /* unit quantity asked for */
                tmp_pckqty,
                /* catch quantity asked for */
                tmp_pck_catch_qty,
                /* unit quantity found */
                &tmp_qty_processed,
                /* catch quantity found */
                &tmp_catch_qty_processed,
                &glom_sts);
                                            
            if (ret_status == eRPL_NORMAL_FAILURE)
            {
                tmp_qty_processed = 0;
                tmp_catch_qty_processed = 0;
            }
            else if(ret_status != eOK)
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                sqlFreeResults(res);
                return (APPError(ret_status));
            }
            
            qty_processed += tmp_qty_processed;
            catch_qty_processed += tmp_catch_qty_processed;
            
            tmp_pckqty -= tmp_qty_processed;
            tmp_pck_catch_qty -= tmp_catch_qty_processed;
            
            if (glom_sts == BOOLEAN_TRUE)
                break;
        }
        

        if ((qty_processed >= pckqty && 
             catch_qty_processed >= pck_catch_qty) || 
            glom_sts == BOOLEAN_TRUE)
        {
            if (bCommitAsWeGo)
                srvCommit();

            if (glom_sts == BOOLEAN_TRUE)
            {
                pckrpl_WriteTrc("Glommed a portion of rplref %s. We'll leave "
                                "the decremented portion of this rplref out "
                                "there and reprocess it with the next run of "
                                "the replenishment manager. ",
                                sqlGetString(res, row, "rplref"));
            }
            else
            {
                pckrpl_WriteTrc("Finished allocation processing of rplref: "
                                "%s", sqlGetString(res, row, "rplref"));
            }
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            continue;
        }

        pckqty -= qty_processed;
        pck_catch_qty -= catch_qty_processed;

        qty_processed = 0;
        catch_qty_processed = 0;

        if (sqlGetBoolean(res, row, "splflg") == BOOLEAN_TRUE)
        {
            /* We are allowed to split.  Try to piece replenish this */
            glom_sts = BOOLEAN_FALSE;

            tmp_pckqty = pckqty;
            tmp_pck_catch_qty = pck_catch_qty;

            for (dstrow = sqlGetRow(dstres); 
                 dstrow && (tmp_pckqty > 0 || tmp_pck_catch_qty > 0); 
                 dstrow = sqlGetNextRow(dstrow))
            {
                /* Loop through the destinations and try to allocate to them 
                 * at the piece level */
                tmp_qty_processed = 0;
                tmp_catch_qty_processed = 0;
                ret_status = sPrcReplenToPiece(res,
                                               row,
                                               dstres,
                                               dstrow,
                                               pckqty,
                                               pck_catch_qty,
                                               &tmp_qty_processed,
                                               &tmp_catch_qty_processed,
                                               &glom_sts);

                if (ret_status == eRPL_NORMAL_FAILURE)
                {
                    tmp_qty_processed = 0;
                    tmp_catch_qty_processed = 0;
                }
                else if (ret_status != eOK)
                {
                    sqlFreeResults(res);
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    return(APPError(ret_status));
                }

                qty_processed += tmp_qty_processed;
                catch_qty_processed += tmp_catch_qty_processed;

                tmp_pckqty -= tmp_qty_processed;
                tmp_pck_catch_qty -= tmp_catch_qty_processed;

                if (glom_sts == BOOLEAN_TRUE)
                    break;
            }

            if (qty_processed >= pckqty && 
                catch_qty_processed >= pck_catch_qty)
            {
                if (bCommitAsWeGo)
                    srvCommit();

                pckrpl_WriteTrc("Finished allocation processing of rplref: %s",
                                sqlGetString(res, row, "rplref"));

                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
                continue;
            }
        }

        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;

        pckqty -= qty_processed;
        pck_catch_qty -= catch_qty_processed;

        /* The only way we are still here is if we failed to process
         * some quantity.
         */
        pckrpl_WriteTrc("Error Level is %d ", ErrorLevel);        
        if (ErrorLevel == _ERROR_NO_INVENTORY || 
            ErrorLevel == _ERROR_NO_LOCATIONS)
        {
            /* Now we must check to see if we want to generate a 
             * work order automatically
             */
            ret_status = sProcessWorkOrderGeneration(sqlGetString(res,
                                                                  row, 
                                                                  "rplref"));

            if (ret_status != eOK && ret_status != eRPL_NORMAL_FAILURE)
            {
                pckrpl_WriteTrc("Failed (%d) while processing work order "
                                "generation", ret_status);
                sqlFreeResults(res);
                return(APPError(ret_status));
            }
        }

        /* Continue through with normal error processing */
        if (ErrorLevel > 0)
        {
            memset(error_status, 0, sizeof(error_status));
            if (ErrorLevel == _ERROR_NO_INVENTORY)
            {
                pckrpl_WriteTrc("\n   *** Setting rplref %s as "
                                "FAILED TO ALLOCATE ***\n",
                                sqlGetString(res, row, "rplref"));
                strcpy(error_status, RPLSTS_ALLOCFAIL);
            }
            else if (ErrorLevel == _ERROR_NO_LOCATIONS)
            {
                pckrpl_WriteTrc("\n   *** Setting rplref %s as "
                                "NO STORAGE LOCATIONS ***\n",
                                sqlGetString(res, row, "rplref"));
                strcpy(error_status, RPLSTS_STOLOCFAIL);
            }
            else if (ErrorLevel == _ERROR_PRODUCTION_LINE_BUSY)
            {
                pckrpl_WriteTrc("\n   *** Setting rplref %s as "
                                "PRODUCTION LINE BUSY ***\n",
                                sqlGetString(res, row, "rplref"));
                strcpy(error_status, RPLSTS_PROD_LINE_BUSY);
            }
            else
            {
                pckrpl_WriteTrc("\n   *** Setting rplref %s as "
                                "UNKNOWN ERROR ***\n",
                                sqlGetString(res, row, "rplref"));
                strcpy(error_status, RPLSTS_UNKNOWN_FAIL);
            }

            sprintf(buffer,
                    "update rplwrk "
                    "   set rplsts = '%s', "
                    "       alcdte = sysdate, "
                    "       rplcnt = rplcnt + 1, "
                    "       rplmsg = '%s' "
                    " where rplref = '%s'",
                    error_status, ErrorText,
                    sqlGetString(res, row, "rplref"));
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("\n  UPDATE FAILED - %ld\n", ret_status);
            }
        }
        if (bCommitAsWeGo)
            srvCommit();

    }

    if (res)
        sqlFreeResults(res);

    pckrpl_WriteTrc("\n*** Done with Replenishment Allocation "
                    "Processing Cycle ***\n");
    return (srvResults(eOK, NULL));
}

