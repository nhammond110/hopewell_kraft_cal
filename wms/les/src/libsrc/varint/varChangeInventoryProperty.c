static const char *rcsid = "$Id: intChangeInventoryProperty.c$";
/******************************************************************************
 *
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved
 *
 *****************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "intlib.h"

/*
 * HISTORY
 * JJS 04/16/2009 - Allow inventory with asnflg = 1 to be changed
 *
 */ 

static long sCheckInvAttChgOK(mocaDataRes *res,
                           mocaDataRow *row)
{
    long   status;
    char   buffer[1000];

    /* 
     * Location has no active cycle counts
     */

    misTrc(T_FLOW,
           "Checking if the location has active cycle counts  %s.",
           sqlGetString(res, row, "stoloc"));

    sprintf(buffer, 
            "select 1"
            "  from locmst "
            " where cipflg        = %d "
            "   and locmst.stoloc = '%s' "
            "   and locmst.wh_id  = '%s' ",
            BOOLEAN_TRUE,
            sqlGetString(res, row, "stoloc"),
            sqlGetString(res, row, "wh_id"));

    status = sqlExecStr(buffer, NULL);

    if (status == eOK)
        return (eINT_CHG_INV_ATTR_ACT_CYC_CNT);
    else if (status != eDB_NO_ROWS_AFFECTED)
        return (status);

    /* 
     * Inventory is not assocoated with a distribution id
     */

    misTrc(T_FLOW,
           "Checking if Inventory is not assocoated with a distribution id  %s.",
           sqlGetString(res, row, "dtlnum"));

    sprintf(buffer, 
            "select 1"
            "  from invdtl "
            " where invdtl.distro_id is not null  "
            "   and invdtl.dtlnum = '%s' ",
            sqlGetString(res, row, "dtlnum"));

    status = sqlExecStr(buffer, NULL);

    if (status == eOK)
        return (eINT_CHG_INV_ATTR_DISTRO);
    else if (status != eDB_NO_ROWS_AFFECTED)
        return (status);

    /* 
     * Inventory does not have the asn flag set
     */

    misTrc(T_FLOW,
           "VAR:  We are NOT Checking if Inventory has the asn flag set  %s.",
           sqlGetString(res, row, "dtlnum"));

    /**************************************************************
     * JJS 04/16/2009 - Comment this out.  Who cares if this is ASN
     *                   inventory???  We want to be able to change it
     *
     *      sprintf(buffer, 
     *              "select 1"
     *              "  from invdtl "
     *              " where invdtl.asnflg = %d "
     *              "   and invdtl.dtlnum = '%s' ",
     *              BOOLEAN_TRUE,
     *              sqlGetString(res, row, "dtlnum"));
     *  
     *      status = sqlExecStr(buffer, NULL);
     *  
     *      if (status == eOK)
     *          return (eINT_CHG_INV_ATTR_ASN);
     *      else if (status != eDB_NO_ROWS_AFFECTED)
     *          return (status);
    **************************************************************/

   /* 
    * Inventory is not on any active receipts
    */

    misTrc(T_FLOW,
           "Checking if Inventory is not on any active receipts  %s.",
           sqlGetString(res, row, "dtlnum"));

    sprintf(buffer, 
            "select 1"
            "  from invdtl, "
            "       rcvlin, "
            "       rcvtrk, "
            "       trlr    "
            " where invdtl.rcvkey = rcvlin.rcvkey "
            "   and rcvtrk.trknum = rcvlin.trknum "
            "   and rcvtrk.wh_id  = rcvlin.wh_id "
            "   and trlr.trlr_id  = rcvtrk.trlr_id "
            "   and trlr.trlr_stat in ('%s','%s','%s') "
            "   and invdtl.dtlnum = '%s' "
            "   and rcvlin.wh_id  = '%s' ",
            TRLSTS_CHECKED_IN,
            TRLSTS_OPEN_FOR_RCV,
            TRLSTS_RECEIVING,
            sqlGetString(res, row, "dtlnum"),
            sqlGetString(res, row, "wh_id"));

    status = sqlExecStr(buffer, NULL);

    if (status == eOK)
        return (eINT_CHG_INV_ATTR_ACT_RCPT);
    else if (status != eDB_NO_ROWS_AFFECTED)
        return (status);

    /* 
     * Inventory is not on an RF Device
     */

    misTrc(T_FLOW,
           "Checking if Inventory is on an RF Device  %s.",
           sqlGetString(res, row, "dtlnum"));

    sprintf(buffer,
                " list inventory "
                " where dtlnum = '%s' "
                "   and wh_id  = '%s' "
                "| "
                " list devices " 
                " where devcod = @stoloc "
                "   and wh_id  = '%s' ",
                sqlGetString(res, row, "dtlnum"),
                sqlGetString(res, row, "wh_id"),
                sqlGetString(res, row, "wh_id"));

        status = srvInitiateCommand(buffer, NULL);
        if (status == eOK)
            return (eINT_CHG_INV_ATTR_RF_DEV);
        else if (status != eDB_NO_ROWS_AFFECTED)
            return (status);

    /* 
     * Inventory is not in transit
     */

    misTrc(T_FLOW,
           "Checking if Inventory is in transit  %s.",
           sqlGetString(res, row, "dtlnum"));

    sprintf(buffer, 
            "select 1"
            "  from inv_intransit "
            " where inv_intransit.dtlnum = '%s' "
            "   and inv_intransit.wh_id  = '%s' ",
            sqlGetString(res, row, "dtlnum"),
            sqlGetString(res, row, "wh_id"));

    status = sqlExecStr(buffer, NULL);

    if (status == eOK)
        return (eINVENTORY_IN_TRANSIT);
    else if (status != eDB_NO_ROWS_AFFECTED)
        return (status);

    return eOK;
}

static long sWriteDlyTrn(char     *dtlnum,
                      char        *client_id,
                      char         dlytrnrcd[][RTSTR1_LEN],
                      int          size,
                      char        *actcod,
                      char        *reacod,
                      char        *cmnt,
                      char        *wh_id)
{
    mocaDataRes   *resMisc;
    mocaDataRow   *rowMisc;

    char           buffer[1000];
    char           lodnum[LODNUM_LEN];
    char           subnum[SUBNUM_LEN];
    long           status;
    int            i = 0;

    sprintf(buffer,
        "  select invlod.lodnum,"
        "         invsub.subnum "
        "    from invlod ,"
        "         invsub ,"
        "         invdtl  "
        "   where invdtl.dtlnum = '%s'          "
        "     and invlod.wh_id  = '%s'          "
        "     and invdtl.subnum = invsub.subnum "
        "     and invsub.lodnum = invlod.lodnum ",
        dtlnum,
        wh_id);

    status = sqlExecStr(buffer, &resMisc);
    if (status != eOK)
    {
        sqlFreeResults(resMisc);
        return (status);
    }

    rowMisc = sqlGetRow(resMisc);

    strncpy(lodnum, sqlGetValue(resMisc, rowMisc, "lodnum"), LODNUM_LEN);
    strncpy(subnum, sqlGetValue(resMisc, rowMisc, "subnum"), SUBNUM_LEN);
    sqlFreeResults(resMisc);

    misTrc(T_FLOW,"Writing daily transaction records ....");

    for (i = 0; i < size; i++)
    {
        sprintf(buffer,
            " write daily transaction "
            "   where lodnum        = '%s' "
            "     and subnum        = '%s' "
            "     and dtlnum        = '%s' "
            "     and client_id     = '%s' "
            "     and devcod        = '%s' "
            "     and actcod        = '%s' "
            "     and reacod        = '%s' "
            "     and cmnt          = '%s' "
            "     and wh_id         = '%s' "
            "     and                  %s ",
            lodnum,
            subnum,
            dtlnum,
            client_id,
            osGetVar(LESENV_DEVCOD) ? osGetVar(LESENV_DEVCOD) : "",
            actcod,
            reacod,
            cmnt,
            wh_id,
            dlytrnrcd[i]);
                
        status = srvInitiateCommand (buffer, NULL);

        if (status != eOK)
            return (status);
    }
    return eOK;
}

static long sCheckChangeOK(mocaDataRes *res,
                           mocaDataRow *row)
{
    long   status;
    char   buffer[1000];

    /* 
     * Check if detail is not potentially allocated
     */

    misTrc(T_FLOW,
           "Checking if inventory is allocated for detail %s.",
           sqlGetString(res, row, "dtlnum"));

    sprintf(buffer, 
            "select 1"
            "  from invsum, "
            "       invlod, "
            "       invsub, "
            "       invdtl  "
            " where invsum.comqty > 0 "
            "   and invsum.prtnum = invdtl.prtnum "
            "   and invsum.prt_client_id = invdtl.prt_client_id "
            "   and invsum.stoloc = invlod.stoloc "
            "   and invlod.lodnum = invsub.lodnum "
            "   and invsub.subnum = invdtl.subnum "
            "   and invdtl.dtlnum = '%s' ",
            sqlGetString(res, row, "dtlnum"));

    status = sqlExecStr(buffer, NULL);

    if (status == eOK)
        return (eINT_CHG_INV_ATTR_ALLOCATED);
    else if (status != eDB_NO_ROWS_AFFECTED)
        return (status);
    else 
        status = eOK;

    /* 
     * Check if detail is not picked by checking if we have a wrkref value 
     */

    misTrc(T_FLOW,
           "Checking if inventory is picked for detail %s.",
           sqlGetString(res, row, "dtlnum"));

    sprintf(buffer, 
            "select 1"
            "  from invdtl  "
            " where invdtl.dtlnum = '%s' "
            "   and wrkref is not null ",
            sqlGetString(res, row, "dtlnum"));

    status = sqlExecStr(buffer, NULL);

    if (status == eOK)
        return (eINT_CHG_INV_ATTR_PICKED);
    else if (status != eDB_NO_ROWS_AFFECTED)
        return (status);
    else 
        status = eOK;

    /* 
     * Check if location has nothing pending against it
     * This may cause a mixed location that might not be valid.
     * We are just taking the safe route here and be restrictive.
     */

    misTrc(T_FLOW,
           "Checking if location has anything pending against it for detail %s.",
           sqlGetString(res, row, "dtlnum"));
	
    sprintf(buffer, 
            "select 1 "
            "  from invdtl, "
            "       invsub, "
            "       invlod, "
            "       locmst  "
            " where locmst.pndqvl > 0 "
            "   and locmst.stoloc = invlod.stoloc "
            "   and locmst.wh_id  = invlod.wh_id  "
            "   and invlod.lodnum = invsub.lodnum "
            "   and invsub.subnum = invdtl.subnum "
            "   and invdtl.dtlnum = '%s' ",
            sqlGetString(res, row, "dtlnum"));

    status = sqlExecStr(buffer, NULL);

    if (status == eOK)
        return (eINT_CHG_INV_ATTR_LOC_PND_QTY);
    else if (status != eDB_NO_ROWS_AFFECTED)
        return (status);
    else status = eOK;

    /* 
     * Check if this inventory is not going anywhere. This may cause 
     * mixed inventory that we might not want 
     */

    misTrc(T_FLOW,
           "Checking if inventory is going anywhere for detail %s.",
           sqlGetString(res, row, "dtlnum"));

    sprintf(buffer, 
            "select 1 "
            "  from nxtloc_view "
            " where dtlnum = '%s' ",
             sqlGetString(res, row, "dtlnum"));

    status = sqlExecStr(buffer, NULL);

    if (status == eOK)
        return (eINT_CHG_INV_ATTR_PEND_MOV);
    else if (status != eDB_NO_ROWS_AFFECTED)
        return (status);
    else status = eOK;

    return (status);
}


static long sChangePackaging(char        *to_ftpcod,
                             long         to_untcas,
                             long         to_untpak,
                             char        *to_prtnum,
                             char        *to_client_id,
                             char        *to_lotnum,
                             char        *to_orgcod,
                             char        *to_revlvl,
                             double       to_catch_qty,
                             char        *to_mandte,
                             char        *to_expire_dte,
                             char        *to_supnum,
                             moca_bool_t to_cnsg_flg,
                             char        *reacod,
                             char        *cmnt,
                             mocaDataRes *res,
                             mocaDataRow *row)
{
    RETURN_STRUCT *TmpRes = NULL;
    mocaDataRes   *resMisc;
    mocaDataRow   *rowMisc;

    char           buffer[2000];
    char           actcod[ACTCOD_LEN];
    char           dlytrnval[11][RTSTR1_LEN];
    char           sesnum[SESNUM_LEN +1];
    char           wrtFromHostTran[1000];
    char           wrtToHostTran[1000];
    long           status;
    int            i = 0;
    moca_bool_t    log_hsttrn = BOOLEAN_FALSE;


    memset(wrtFromHostTran, 0, sizeof(wrtFromHostTran));
    memset(wrtToHostTran, 0, sizeof(wrtToHostTran));

    /* 
    ** Do the following checks when we are changing
    ** any attribute except the ftpcod 
    */

    if (to_untcas > 0 || to_untpak > 0 || strlen(to_prtnum) || strlen(to_client_id) ||
        strlen(to_lotnum) || strlen(to_orgcod) || strlen(to_revlvl) || to_catch_qty > 0.0 ||
        strlen(to_mandte) || strlen(to_expire_dte))
    {
        status = sCheckChangeOK(res,row);

        if (eOK != status)
            return (status);
    }

    /* 
    ** Do the following checks when we are changing
    ** any attribute except the ftpcod or untcas or untpak
    */

    if (strlen(to_prtnum) || strlen(to_client_id) || strlen(to_lotnum) || strlen(to_orgcod) ||
        strlen(to_revlvl) || to_catch_qty > 0.0 || strlen(to_mandte) || strlen(to_expire_dte))
    {
        /* 
        * If catch quantity is passed then check if the part is catch enabled.
        */
        if(to_catch_qty > 0.0)
        {
            misTrc(T_FLOW,
            "Checking if the part is Catch Enabled  %s.",
             sqlGetString(res, row, "prtnum"));

            sprintf(buffer,
                    "select 1"
                    "  from prtmst_view "
                    " where prtmst_view.catch_cod is not null "
                    "   and prt_client_id      = '%s' "
                    "   and prtmst_view.wh_id  = '%s' "
                    "   and prtmst_view.prtnum = '%s' ",
                    sqlGetString(res, row, "prt_client_id"),
                    sqlGetString(res, row, "wh_id"),
                    sqlGetString(res, row, "prtnum"));

            status = sqlExecStr(buffer, NULL);

            if (status != eDB_NO_ROWS_AFFECTED &&
               status != eOK)
            {
                return (status);
            }
            else if (status == eDB_NO_ROWS_AFFECTED)
            {
                return (eINT_CHG_INV_ATTR_CATCH_ENABLED);
            }
        }

        /* 
         * If Lot Number is passed then part is Lot Tracked.
         */
        if(strlen(to_lotnum)) 
        {
            misTrc(T_FLOW,
            "Checking if the part is Lot Tracked  %s.",
            sqlGetString(res, row, "prtnum")); 

            sprintf(buffer,
                  "select 1"
                  "  from prtmst_view "
                  " where prtnum        = '%s' "
                  "   and prt_client_id = '%s' "
                  "   and lotflg        = %d "
                  "   and wh_id         = '%s' ",  
                  sqlGetString(res, row, "prtnum"),
                  sqlGetString(res, row, "prt_client_id"),
                  BOOLEAN_TRUE,  
                  sqlGetString(res, row, "wh_id"));

            status = sqlExecStr(buffer, NULL);

            if (status != eDB_NO_ROWS_AFFECTED &&
               status != eOK)
            {
                return (status);
            }
            else if (status == eDB_NO_ROWS_AFFECTED)
            {
                return (eINT_CHG_INV_ATTR_LOT_TRKD);
            }
        }

       /* 
        * If Revision Level is passed then part is Revision Tracked.
        */
        if(strlen(to_revlvl))
        {
            misTrc(T_FLOW,
            "Checking if the part is Revision Tracked  %s.",
            sqlGetString(res, row, "prtnum"));

            sprintf(buffer, 
                  "select 1"
                  "  from prtmst_view "
                  " where prtnum        = '%s' "
                  "   and prt_client_id = '%s' "
                  "   and revflg        = '%d' "
                  "   and wh_id         = '%s' ",  
                  sqlGetString(res, row, "prtnum"),
                  sqlGetString(res, row, "prt_client_id"),
                  BOOLEAN_TRUE,
                  sqlGetString(res, row, "wh_id"));

            status = sqlExecStr(buffer, NULL);

            if (status != eDB_NO_ROWS_AFFECTED &&
                status != eOK)
            {
                return (status);
            }
            else if (status == eDB_NO_ROWS_AFFECTED)
            {
                return (eINT_CHG_INV_ATTR_RVS_TRKD);
            }
        }

       /* 
        * If Origin Code is passed then part is Origin Tracked.
        */
        if(strlen(to_orgcod))
        {
            misTrc(T_FLOW,
            "Checking if the part is Origin Tracked  %s.",
            sqlGetString(res, row, "prtnum"));
           
            sprintf(buffer,
            "select 1"
            "  from prtmst_view "
            " where prtnum        = '%s' "
            "   and prt_client_id = '%s' "
            "   and orgflg        = %d "
            "   and wh_id         = '%s' ",
            sqlGetString(res, row, "prtnum"),
            sqlGetString(res, row, "prt_client_id"),
            BOOLEAN_TRUE,
            sqlGetString(res, row, "wh_id"));

            status = sqlExecStr(buffer, NULL);

            if (status != eDB_NO_ROWS_AFFECTED &&
                status != eOK)
            {
                return (status);
            }
            else if (status == eDB_NO_ROWS_AFFECTED)
            {
                return (eINT_CHG_INV_ATTR_ORG_TRKD);
            }
        }

       /* 
        * If Manufacturing Date or Expiration Date is passed then part has an Aging Profile.
        */
       if(strlen(to_mandte) || strlen(to_expire_dte))
       {
            misTrc(T_FLOW,
            "Checking if the part has an Aging Profile  %s.",
            sqlGetString(res, row, "prtnum"));

            sprintf(buffer,
                    "select 1"
                    "  from prtmst_view "
                    " where prtnum        = '%s' "
                    "   and prt_client_id = '%s' "
                    "   and age_pflnam is not null "
                    "   and wh_id         = '%s' ",
                    sqlGetString(res, row, "prtnum"),
                    sqlGetString(res, row, "prt_client_id"),
                    sqlGetString(res, row, "wh_id"));

            status = sqlExecStr(buffer, NULL);

            if (status != eDB_NO_ROWS_AFFECTED &&
                status != eOK)
            {
                return (status);
            }
            else if (status == eDB_NO_ROWS_AFFECTED)
            {
                return (eINT_CHG_INV_ATTR_AGEPFL);
            }
        }

        status = sCheckInvAttChgOK(res,row);
        if (eOK != status)
        {
            return (status);
        }
        log_hsttrn = BOOLEAN_TRUE;
    }

   /* 
    * When part number is changed then the original
    * or the new part is not a seralized part.
    */
    if ( strlen(to_prtnum) &&
         strcmp(to_prtnum, sqlGetString(res, row, "prtnum")) != 0)
    {
        sprintf(buffer,
                "select 1"
                "  from prtmst_view "
                " where prtmst_view.ser_typ is not null "
                "   and prtmst_view.wh_id = '%s' "
                "   and prtmst_view.prtnum in ('%s','%s') ",
                sqlGetString(res, row, "wh_id"),
                sqlGetString(res, row, "prtnum"),
                to_prtnum);

        status = sqlExecStr(buffer, NULL);

        if (status == eOK)
        {
            return (eINT_CHG_INV_ATTR_SRLZD_PRT);
        }
        else if (status != eDB_NO_ROWS_AFFECTED)
        {
            return (status);
        }
        else 
        {
            status = eOK;
        }
    }

    /* 
     * Before updating the inventory detail check if we are changing
     * the part number or client id. If yes, then check if we have a 
     * billing charge schedule for that client and part/allparts for 
     * blng_serv_typ of 'INITSTG'. If yes, then generate an invoice.
     */

    if ( (strlen(to_client_id) &&
         strcmp(to_client_id, sqlGetString(res, row, "prt_client_id"))) ||
         (strlen(to_prtnum) &&
         strcmp(to_prtnum, sqlGetString(res, row, "prtnum"))) )
    {
        misTrc(T_FLOW,
               "Checking for any billing charge schedule for %s for service Initial Storage "
               "and generating the invoice",
               sqlGetString(res, row, "prt_client_id"));
        sprintf(buffer,
            " list billing schedules "
            "   where blng_serv_typ = '%s' "
            "     and client_id     = '%s' "
            "     and wh_id         = '%s' catch(-1403)"
            " | "
            " if (@? = 0)"
            " { "
            "     list inventory change information "
            "         where dtlnum        = '%s' "
            "           and prtnum        = '%s' "
            "           and prt_client_id = '%s' "
            "     | "
            "     process billing event "
            "         where dtlnum        = '%s' "
            "           and prtnum        = '%s' "
            "           and client_id     = '%s' "
            "           and blng_serv_typ = '%s' "
            "           and wh_id         = '%s' "
            " } ",
            BLNG_SERV_TYP_INITSTG,
            sqlGetString(res, row, "prt_client_id"),
            sqlGetString(res, row, "wh_id"),
            sqlGetString(res, row, "dtlnum"),
            sqlGetString(res, row, "prtnum"),
            sqlGetString(res, row, "prt_client_id"),
            sqlGetString(res, row, "dtlnum"),
            sqlGetString(res, row, "prtnum"),
            sqlGetString(res, row, "prt_client_id"),
            BLNG_SERV_TYP_INITSTG,
            sqlGetString(res, row, "wh_id"));

        status = srvInitiateCommand (buffer, NULL);

        if (status != eOK)
            return (status);
    }

    /* 
    ** If we got to this point, we can update the inventory detail 
    */

    misTrc(T_FLOW,
           "Updating the inventory for detail %s.",
           sqlGetString(res, row, "dtlnum"));


    sprintf(buffer, 
            "update invdtl set ");

    if (misTrimLen(to_ftpcod, FTPCOD_LEN) && 
        strcmp(to_ftpcod, sqlGetString(res, row, "ftpcod")))
    {
        sprintf(buffer,
                "%s ftpcod = '%s', ",
                buffer,
                to_ftpcod);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'ftpcod' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "ftpcod"),
                to_ftpcod);
    }

    if (to_untcas > 0 &&
        to_untcas != sqlGetLong(res, row, "untcas"))
    {
        sprintf(buffer,
                "%s untcas = %d, ",
                buffer, 
                to_untcas);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'untcas' and "
                "fr_value = '%d' and "
                "to_value = '%d'",
                sqlGetLong(res, row, "untcas"),
                to_untcas);
    }

    if (to_untpak > 0 &&
        to_untpak != sqlGetLong(res, row, "untpak"))
    {
        sprintf(buffer,
                "%s untpak = %d, ",
                buffer,
                to_untpak);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'untpak' and "
                "fr_value = '%d' and "
                "to_value = '%d'",
                sqlGetLong(res, row, "untpak"),
                to_untpak);
    }

    if (misTrimLen(to_prtnum, PRTNUM_LEN) &&
        strcmp(to_prtnum, sqlGetString(res, row, "prtnum")))
    {
        sprintf(buffer,
                "%s prtnum = '%s', ",
                buffer,
                to_prtnum);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'prtnum' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "prtnum"),
                to_prtnum);

        sprintf(wrtFromHostTran, " and prtnum = '%s' ",
                sqlGetString(res, row, "prtnum"));
        sprintf(wrtToHostTran, " and prtnum = '%s' ",
                to_prtnum);
    }

    if (misTrimLen(to_client_id, CLIENT_ID_LEN) &&
        strcmp(to_client_id, sqlGetString(res, row, "prt_client_id")))
    {
        sprintf(buffer,
                "%s prt_client_id = '%s', ",
                buffer,
                to_client_id);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'prt_client_id' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "prt_client_id"),
                to_client_id);

        sprintf(wrtFromHostTran, " and prt_client_id = '%s' ",
                sqlGetString(res, row, "prt_client_id"));
        sprintf(wrtToHostTran, " and prt_client_id = '%s' ",
                to_client_id);
    }

    if (misTrimLen(to_lotnum, LOTNUM_LEN) &&
        strcmp(to_lotnum, sqlGetString(res, row, "lotnum")))
    {
        sprintf(buffer,
                "%s lotnum = '%s', ",
                buffer,
                to_lotnum);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'lotnum' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "lotnum"),
                to_lotnum);

        sprintf(wrtFromHostTran, " and lotnum = '%s' ",
                sqlGetString(res, row, "lotnum"));
        sprintf(wrtToHostTran, " and lotnum = '%s' ",
                to_lotnum);
    }

    if (misTrimLen(to_orgcod, ORGCOD_LEN) &&
        strcmp(to_orgcod, sqlGetString(res, row, "orgcod")))
    {
        sprintf(buffer,
                "%s orgcod = '%s', ",
                buffer,
                to_orgcod);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'orgcod' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "orgcod"),
                to_orgcod);

        sprintf(wrtFromHostTran, " and orgcod = '%s' ",
                sqlGetString(res, row, "orgcod"));
        sprintf(wrtToHostTran, " and orgcod = '%s' ",
                to_orgcod);
    }

    if (misTrimLen(to_revlvl, REVLVL_LEN) &&
        strcmp(to_revlvl, sqlGetString(res, row, "revlvl")))
    {
        sprintf(buffer,
                "%s revlvl = '%s', ",
                buffer,
                to_revlvl);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'revlvl' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "revlvl"),
                to_revlvl);

        sprintf(wrtFromHostTran, " and revlvl = '%s' ",
                sqlGetString(res, row, "revlvl"));
        sprintf(wrtToHostTran, " and revlvl = '%s' ",
                to_revlvl);
    }

    if (to_catch_qty > 0.0 &&
        to_catch_qty != sqlGetFloat(res, row, "catch_qty"))
    {
        sprintf(buffer,
                "%s catch_qty = %lf, ",
                buffer,
                to_catch_qty);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'catch_qty' and "
                "fr_value = '%lf' and "
                "to_value = '%lf'",
                sqlGetFloat(res, row, "catch_qty"),
                to_catch_qty);

        sprintf(wrtFromHostTran, " and catch_qty = '%s' ",
                sqlGetString(res, row, "catch_qty"));
        sprintf(wrtToHostTran, " and catch_qty = '%s' ",
                to_catch_qty);
    }

    if (misTrimLen(to_mandte, MOCA_STD_DATE_LEN) &&
        strcmp(to_mandte, sqlGetString(res, row, "mandte")))
    {
        sprintf(buffer,
                "%s mandte = to_date('%s', '%s'), ",
                buffer,
                to_mandte,
                MOCA_STD_DATE_FORMAT);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'mandte' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "mandte"),
                to_mandte);

        sprintf(wrtFromHostTran, " and mandte = '%s' ",
                sqlGetString(res, row, "mandte"));
        sprintf(wrtToHostTran, " and mandte = '%s' ",
                to_mandte);
    }

    if (misTrimLen(to_expire_dte, MOCA_STD_DATE_LEN) &&
        strcmp(to_expire_dte, sqlGetString(res, row, "expire_dte")))
    {
        sprintf(buffer,
                "%s expire_dte = to_date('%s', '%s'), ",
                buffer,
                to_expire_dte,
                MOCA_STD_DATE_FORMAT);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'expire_dte' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "expire_dte"),
                to_expire_dte);

        sprintf(wrtFromHostTran, " and expire_dte = '%s' ",
                sqlGetString(res, row, "expire_dte"));
        sprintf(wrtToHostTran, " and expire_dte = '%s' ",
                to_expire_dte);
    }

    if (misTrimLen(to_supnum, SUPNUM_LEN) &&
        strcmp(to_supnum, sqlGetString(res, row, "supnum")))
    {
        sprintf(buffer,
                "%s supnum = '%s', ",
                buffer,
                to_supnum);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'supnum' and "
                "fr_value = '%s' and "
                "to_value = '%s'",
                sqlGetString(res, row, "supnum"),
                to_supnum);

        sprintf(wrtFromHostTran, " and supnum = '%s' ",
                sqlGetString(res, row, "supnum"));
        sprintf(wrtToHostTran, " and supnum = '%s' ",
                to_supnum);
    }

    if ((to_cnsg_flg != BOOLEAN_NOTSET) &&
        (to_cnsg_flg != sqlGetBoolean(res, row, "cnsg_flg")))
    {
        sprintf(buffer,
                "%s cnsg_flg = %d, ",
                buffer, 
                to_cnsg_flg);

        memset(dlytrnval[i], 0, RTSTR1_LEN);
        sprintf(dlytrnval[i++],
                "var_nam  = 'consigned' and "
                "fr_value = '%d' and "
                "to_value = '%d'",
                sqlGetBoolean(res, row, "cnsg_flg"),
                to_cnsg_flg);
    }

    /* 
     * Logging daily transaction records. We do this before updating the
     * invdtl table to get the original values for the parameters before
     * it is updated with the chnaged values.
     */

    if (i == 0)
    {
        /* We have not changed any attribute. Return eOK */
        return eOK;
    }

    /* Remove the last comma */
    buffer[strlen(buffer) - 2] = '\0';

    if ((misTrimLen(to_client_id, CLIENT_ID_LEN) &&
        strcmp(to_client_id, sqlGetString(res, row, "prt_client_id")))
        || (misTrimLen(to_prtnum, PRTNUM_LEN) &&
        strcmp(to_prtnum, sqlGetString(res, row, "prtnum"))))
    {
        strcpy(actcod, "INVOWNCHG");
    }
    else
    {
        strcpy(actcod, "INVATTCHG");
    }

    status = sWriteDlyTrn(sqlGetString(res, row, "dtlnum"),
                          sqlGetString(res, row, "prt_client_id"),
                          dlytrnval,
                          i,
                          actcod,
                          reacod,
                          cmnt,
                          sqlGetString(res, row, "wh_id"));

    if (eOK != status)
    {
        return (status);
    }

    sprintf(buffer,
            "%s where dtlnum = '%s'", 
            buffer, 
            sqlGetString(res, row, "dtlnum"));

    status = sqlExecStr(buffer, NULL);

    if (eOK != status)
    {
        return (status);
    }

    /* Logging host transaction records */

    if (log_hsttrn)
    {
        /* Generate session no for logging the host transactions */

        sprintf(buffer, "generate next number where numcod = 'sesnum'");

        status = srvInitiateCommand (buffer, &TmpRes);

        if (status == eOK)
        {
            resMisc = srvGetResults(TmpRes);
            rowMisc = sqlGetRow(resMisc);
            misTrimcpy(sesnum, sqlGetString(resMisc, rowMisc, "nxtnum"),
                         SESNUM_LEN);
        }
        srvFreeMemory(SRVRET_STRUCT, TmpRes);
        
        /* Write host transaction for increase of inventory with the changed attribute*/
        misTrc(T_FLOW,"Writing host transaction record ....");
        sprintf(buffer,
            " write inventory adjust "
            "   where sesnum    = '%s' "
            "     and srcare    = '%s' "
            "     and dstare    = '%s' "
            "     and srcdtl    = '%s' "
            "     and untqty    = %ld  "
            "     and catch_qty = %lf "
            "     and reacod    = '%s' "
            "     and wh_id     = '%s' "
            " %s ",
            sesnum,
            sqlGetString(res, row, "arecod"),
            ARECOD_INV_ADJUST,
            sqlGetString(res, row, "dtlnum"),
            sqlGetLong(res, row, "untqty"),
            sqlGetFloat(res, row, "catch_qty"),
            reacod,
            sqlGetString(res, row, "wh_id"),
            wrtToHostTran);

        status = srvInitiateCommand (buffer, NULL);

        if (status != eOK)
        {
            return (status);
        }

    /* Write host transaction for decrease of inventory with the original attribute*/
        misTrc(T_FLOW,"Writing host transaction record ....");
        sprintf(buffer,
            " write inventory adjust "
            "   where sesnum    = '%s'"
            "     and srcare    = '%s'"
            "     and dstare    = '%s'"
            "     and srcdtl    = '%s'"
            "     and untqty    = %ld "
            "     and catch_qty = %lf "
            "     and reacod    = '%s'"
            "     and wh_id     = '%s'"
            " %s ",
            sesnum,
            ARECOD_INV_ADJUST,
            sqlGetString(res, row, "arecod"),
            sqlGetString(res, row, "dtlnum"),
            - sqlGetLong(res, row, "untqty"),
            sqlGetFloat(res, row, "catch_qty"),
            reacod,
            sqlGetString(res, row, "wh_id"),
            wrtFromHostTran);

        status = srvInitiateCommand (buffer, NULL);

        if (status != eOK)
        {
            return (status);
        }
    }

    /* 
    ** Attempt to update the qvl levels for the location 
    ** and correct the invsum table if necessary 
    */

    misTrc(T_FLOW,
           "Validating location %s",
           sqlGetString(res, row, "stoloc"));

    sprintf (buffer,
             " validate location "
             "   where stoloc = '%s' "
             "      and wh_id = '%s' "
             " | "
             " if (@action != '') "
             " { "
             "    execute server command "
             "      where cmd = @action "
             " } ",
             sqlGetString(res, row, "stoloc"),
             sqlGetString(res, row, "wh_id"));

    status = srvInitiateCommand (buffer, NULL);

    if (status != eOK)
        return (status);

    /* 
    ** check if the change we made did not violate the mixing rules 
    */

    misTrc(T_FLOW,
           "Checking if we did not violate the mixing rules for location %s.",
           sqlGetString(res, row, "stoloc"));
	
    sprintf(buffer, 
            "check inventory mix at location "
            "  where dstloc = '%s' "
            "     and wh_id = '%s' "
            "    and dtlnum = '%s' ",
            sqlGetString(res, row, "stoloc"),
            sqlGetString(res, row, "wh_id"),
            sqlGetString(res, row, "dtlnum"));

    status = srvInitiateCommand(buffer, NULL);

    if (status != eOK)
        return (status);

    return (status);
}


static void sAddToReturn(long           status,
                         char          *to_ftpcod,
                         long           to_untcas,
                         long           to_untpak,
                         char          *to_prtnum,
                         char          *to_client_id,
                         char          *to_lotnum,
                         char          *to_orgcod,
                         char          *to_revlvl,
                         double         to_catch_qty,
                         char          *to_mandte,
                         char          *to_expire_dte,
                         char          *to_supnum,
                         moca_bool_t   to_cnsg_flg,
                         RETURN_STRUCT *RetPtr,
                         mocaDataRes   *res,
                         mocaDataRow   *row)
{

    /* 
    ** If we don't have a to_ value for a variable, set it
    ** to what it currently is 
    */

    if (!misTrimLen(to_ftpcod, FTPCOD_LEN))
        to_ftpcod = sqlGetString(res, row, "ftpcod");

    if (!to_untcas)
        to_untcas = sqlGetLong(res, row, "untcas");

    if (!to_untpak)
        to_untpak = sqlGetLong(res, row, "untpak");

    if (!misTrimLen(to_prtnum, PRTNUM_LEN))
        to_prtnum = sqlGetString(res, row, "prtnum");

    if (!misTrimLen(to_client_id, CLIENT_ID_LEN))
        to_client_id = sqlGetString(res, row, "prt_client_id");

    if (!misTrimLen(to_lotnum, LOTNUM_LEN))
        to_lotnum = sqlGetString(res, row, "lotnum");

    if (!misTrimLen(to_orgcod, ORGCOD_LEN))
        to_orgcod = sqlGetString(res, row, "orgcod");

    if (!misTrimLen(to_revlvl, REVLVL_LEN))
        to_revlvl = sqlGetString(res, row, "revlvl");

    if (to_catch_qty == 0.0)
        to_catch_qty = sqlGetFloat(res, row, "catch_qty");

    if (!misTrimLen(to_mandte, MOCA_STD_DATE_LEN))
        to_mandte = sqlGetString(res, row, "mandte");

    if (!misTrimLen(to_expire_dte, MOCA_STD_DATE_LEN))
        to_expire_dte = sqlGetString(res, row, "expire_dte");

    if (!misTrimLen(to_supnum, SUPNUM_LEN))
        to_supnum = sqlGetString(res, row, "supnum");

    if (to_cnsg_flg)
        to_cnsg_flg = sqlGetBoolean(res, row, "cnsg_flg");

    srvResultsAdd(RetPtr,
                  status,
                  sqlGetString(res, row, "bldg_id"),
                  sqlGetString(res, row, "arecod"),
                  sqlGetString(res, row, "stoloc"),
                  sqlGetString(res, row, "wh_id"),
                  sqlGetString(res, row, "lodnum"),
                  sqlGetString(res, row, "subnum"),
                  sqlGetString(res, row, "dtlnum"),
                  sqlGetLong(res, row, "untqty"),
                  sqlGetString(res, row, "prtnum"),
                  to_prtnum,
                  sqlGetString(res, row, "prt_client_id"),
                  to_client_id,
                  sqlGetString(res, row, "ftpcod"),
                  to_ftpcod,
                  sqlGetLong(res, row, "untcas"),
                  to_untcas,
                  sqlGetLong(res, row, "untpak"),
                  to_untpak,
                  sqlGetString(res, row, "lotnum"),
                  to_lotnum,
                  sqlGetString(res, row, "orgcod"),
                  to_orgcod,
                  sqlGetString(res, row, "revlvl"),
                  to_revlvl,
                  sqlGetFloat(res, row, "catch_qty"),
                  to_catch_qty,
                  sqlGetString(res, row, "mandte"),
                  to_mandte,
                  sqlGetString(res, row, "expire_dte"),
                  to_expire_dte,
                  sqlGetString(res, row, "supnum"),
                  to_supnum,
                  sqlGetBoolean(res, row, "cnsg_flg"),
                  to_cnsg_flg);

}

LIBEXPORT
RETURN_STRUCT *varChangeInventoryProperty(char *dtlnum_i,
                                          char *dtlnum_list_i,
                                          char *to_ftpcod_i,
                                          long *to_untcas_i,
                                          long *to_untpak_i,
                                          char *wh_id_i,
                                          char *to_prtnum_i,
                                          char *to_client_id_i,
                                          char *to_lotnum_i,
                                          char *to_orgcod_i,
                                          char *to_revlvl_i,
                                          double *to_catch_qty_i,
                                          char *to_mandte_i,
                                          char *to_expire_dte_i,
                                          char *reacod_i,
                                          char *cmnt_i,
                                          char *to_supnum_i,
                                          moca_bool_t *to_cnsg_flg_i)
{
    RETURN_STRUCT *CmdPtr = NULL;
    RETURN_STRUCT *RetPtr = NULL;
    mocaDataRes   *res = NULL;
    mocaDataRow   *row = NULL;

    char          *dtlnum_list = NULL; 
    char          *dtlnum_ptr = NULL;
    char          *dtlnum_save = NULL;
    char           to_ftpcod [FTPCOD_LEN + 1];
    long           to_untcas = 0;
    long           to_untpak = 0;
    char           wh_id[WH_ID_LEN + 1];
    char           to_prtnum [PRTNUM_LEN + 1];
    char           to_client_id [CLIENT_ID_LEN + 1];
    char           to_lotnum [LOTNUM_LEN + 1];
    char           to_orgcod [ORGCOD_LEN + 1];
    char           to_revlvl [REVLVL_LEN + 1];
    double         to_catch_qty = 0.0;
    char           to_mandte [MOCA_STD_DATE_LEN + 1];
    char           to_expire_dte [MOCA_STD_DATE_LEN + 1];
    char           reacod [REACOD_LEN + 1];
    char           cmnt [CMNT_LEN +1];

    char           savepoint[100];
    char           buffer[2048];
    long           status;
    char           to_supnum[SUPNUM_LEN + 1];
    moca_bool_t    to_cnsg_flg =BOOLEAN_NOTSET;

    memset (savepoint, 0, sizeof (savepoint));
    memset (to_ftpcod, 0, sizeof (to_ftpcod));
    memset (buffer, 0, sizeof (buffer));
    memset(wh_id, 0, sizeof(wh_id));
    /* wh_id is required because 'list inventory change information'
     * uses default wh_id as criterion if no wh_id is passed in. Then if user 
     * works in no-default wh, they can't find the inventory.
     */
    
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
    {    
        /*
        *  Validate the warehouse id. If null, the command
        *  should come back with the default, otherwise we error.
        */

        sprintf(buffer,
                "  get warehouse id "
                "  where wh_id = '%s' ",
                wh_id_i);

        status = srvInitiateCommand(buffer, &RetPtr);

        if (status != eOK)
        {
            srvFreeMemory(SRVRET_STRUCT, RetPtr);
            return (srvResults(status, NULL));
        }
  
        res = srvGetResults(RetPtr);
        row = sqlGetRow(res);
        misTrimcpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
        srvFreeMemory(SRVRET_STRUCT, RetPtr);
        RetPtr = NULL;
        res = NULL;
    }
    else
    {
        return(APPMissingArg("wh_id"));
    }

    memset (to_prtnum, 0, sizeof (to_prtnum));
    memset (to_client_id, 0, sizeof (to_client_id));
    memset (to_lotnum, 0, sizeof (to_lotnum));
    memset (to_orgcod, 0, sizeof (to_orgcod));
    memset (to_revlvl, 0, sizeof (to_revlvl));
    memset (to_mandte, 0, sizeof (to_mandte));
    memset (to_expire_dte, 0, sizeof (to_expire_dte));
    memset (reacod, 0, sizeof (reacod));
    memset (cmnt, 0, sizeof (cmnt));
    memset (to_supnum, 0, sizeof(to_supnum));

    if (to_ftpcod_i && misTrimLen(to_ftpcod_i, FTPCOD_LEN))
        misTrimcpy (to_ftpcod, to_ftpcod_i, FTPCOD_LEN);

    if (to_untcas_i)
        to_untcas = *to_untcas_i;

    if (to_untpak_i)
        to_untpak = *to_untpak_i;

    if (to_prtnum_i && misTrimLen(to_prtnum_i, PRTNUM_LEN))
        misTrimcpy (to_prtnum, to_prtnum_i, PRTNUM_LEN);

    if (to_client_id_i && misTrimLen(to_client_id_i, CLIENT_ID_LEN))
        misTrimcpy (to_client_id, to_client_id_i, CLIENT_ID_LEN);

    if (to_lotnum_i && misTrimLen(to_lotnum_i, LOTNUM_LEN))
        misTrimcpy (to_lotnum, to_lotnum_i, LOTNUM_LEN);

    if (to_revlvl_i && misTrimLen(to_revlvl_i, REVLVL_LEN))
        misTrimcpy (to_revlvl, to_revlvl_i, REVLVL_LEN);

    if (to_orgcod_i && misTrimLen(to_orgcod_i, ORGCOD_LEN))
        misTrimcpy (to_orgcod, to_orgcod_i, ORGCOD_LEN);

    if (to_catch_qty_i)
        to_catch_qty = *to_catch_qty_i;

    if (to_mandte_i && misTrimLen(to_mandte_i, MOCA_STD_DATE_LEN))
        misTrimcpy (to_mandte, to_mandte_i, MOCA_STD_DATE_LEN);

    if (to_expire_dte_i && misTrimLen(to_expire_dte_i, MOCA_STD_DATE_LEN))
        misTrimcpy (to_expire_dte, to_expire_dte_i, MOCA_STD_DATE_LEN);

    if (reacod_i && misTrimLen(reacod_i, REACOD_LEN))
        misTrimcpy (reacod, reacod_i, REACOD_LEN);

    if (cmnt_i && misTrimLen(cmnt_i, CMNT_LEN))
        misTrimcpy (cmnt, cmnt_i, CMNT_LEN);

    if (to_supnum_i && misTrimLen(to_supnum_i, SUPNUM_LEN))
        misTrimcpy (to_supnum, to_supnum_i, SUPNUM_LEN);

     if (to_cnsg_flg_i)
        to_cnsg_flg = *to_cnsg_flg_i;

    /* -- Validation has been done in the GUI. We can remove it here.
    if (!misTrimLen(to_ftpcod, FTPCOD_LEN) && !to_untcas && !to_untpak)
        return(srvResults(eINT_MISSING_ARG_CHANGE_INVENTORY_PACKAGING, NULL));
    */


    /* 
     * If they passed a single detail number, use that, otherwise use the list
     */

    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
    {
        /* 
         * Allocate the memory 
         */

        dtlnum_list = (char *) calloc(1, misTrimLen(dtlnum_i, DTLNUM_LEN) + 1);

        /* 
         * Since the routine I'm using the iterate through this structure
         * is destructive, I have to save a pointer to it for freeing later.
         */

        dtlnum_save = dtlnum_list;

        strcpy(dtlnum_list, dtlnum_i);
        dtlnum_ptr = dtlnum_list - 1;
    }
    else if(dtlnum_list_i && misTrimLen(dtlnum_list_i, DTLNUM_LEN))
    {
        dtlnum_list = (char *) calloc(1, strlen(dtlnum_list_i) + 1);

        /* 
         * Since the routine I'm using the iterate through this structure
         * is destructive, I have to save a pointer to it for freeing later.
         */

        dtlnum_save = dtlnum_list;

        strcpy(dtlnum_list, dtlnum_list_i);
        dtlnum_ptr = dtlnum_list - 1;
    }

    /* 
     * Setup the return structure, as it will be the same for all cases 
     */

    RetPtr = srvResultsInit(eOK, 
                            "exec_sts",      COMTYP_INT,    sizeof(long),
                            "bldg_id",       COMTYP_CHAR,   BLDG_ID_LEN,
                            "arecod",        COMTYP_CHAR,   ARECOD_LEN,
                            "stoloc",        COMTYP_CHAR,   STOLOC_LEN,
                            "wh_id",         COMTYP_CHAR,   WH_ID_LEN,
                            "lodnum",        COMTYP_CHAR,   LODNUM_LEN,
                            "subnum",        COMTYP_CHAR,   SUBNUM_LEN,
                            "dtlnum",        COMTYP_CHAR,   DTLNUM_LEN,
                            "trnqty",        COMTYP_INT,    sizeof(long),
                            "fr_prtnum",     COMTYP_CHAR,   PRTNUM_LEN,
                            "to_prtnum",     COMTYP_CHAR,   PRTNUM_LEN,
                            "fr_client_id",  COMTYP_CHAR,   CLIENT_ID_LEN,
                            "to_client_id",  COMTYP_CHAR,   CLIENT_ID_LEN,
                            "fr_ftpcod",     COMTYP_CHAR,   FTPCOD_LEN,
                            "to_ftpcod",     COMTYP_CHAR,   FTPCOD_LEN,
                            "fr_untcas",     COMTYP_INT,    sizeof(long),
                            "to_untcas",     COMTYP_INT,    sizeof(long),
                            "fr_untpak",     COMTYP_INT,    sizeof(long),
                            "to_untpak",     COMTYP_INT,    sizeof(long),
                            "fr_lotnum",     COMTYP_CHAR,   LOTNUM_LEN,
                            "to_lotnum",     COMTYP_CHAR,   LOTNUM_LEN,
                            "fr_orgcod",     COMTYP_CHAR,   ORGCOD_LEN,
                            "to_orgcod",     COMTYP_CHAR,   ORGCOD_LEN,
                            "fr_revlvl",     COMTYP_CHAR,   REVLVL_LEN,
                            "to_revlvl",     COMTYP_CHAR,   REVLVL_LEN,
                            "fr_catch_qty",  COMTYP_FLOAT,  sizeof(double),
                            "to_catch_qty",  COMTYP_FLOAT,  sizeof(double),
                            "fr_mandte",     COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                            "to_mandte",     COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                            "fr_expire_dte", COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                            "to_expire_dte", COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                            "fr_supnum",     COMTYP_CHAR,   SUPNUM_LEN,
                            "to_supnum",     COMTYP_CHAR,   SUPNUM_LEN,
                            "fr_cnsg_flg",   COMTYP_INT,    sizeof(long),
                            "to_cnsg_flg",   COMTYP_INT,    sizeof(long),
                            NULL);

    if (!dtlnum_list)
    {
        /* 
         * If we don't have any details, call "list inventory change 
         * information" to get the details we are supposed to change.
         */

        sprintf(buffer, 
                " list inventory change information where wh_id = '%s' ",
                wh_id);

        status = srvInitiateInline(buffer, &CmdPtr);
        if (eOK != status)
        {
            srvFreeMemory(SRVRET_STRUCT, RetPtr);
            return(CmdPtr);
        }

        /* 
         * Loop around the details and perform the change 
         */

        res = srvGetResults(CmdPtr);
        for(row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            sprintf(savepoint, 
                    "INVFTP%s",
                        sqlGetString(res, row, "dtlnum"));

            status = sqlSetSavepoint(savepoint);

            status = sChangePackaging(to_ftpcod,
                                      to_untcas,
                                      to_untpak,
                                      to_prtnum,
                                      to_client_id,
                                      to_lotnum,
                                      to_orgcod,
                                      to_revlvl,
                                      to_catch_qty,
                                      to_mandte,
                                      to_expire_dte,
                                      to_supnum,
                                      to_cnsg_flg,
                                      reacod,
                                      cmnt,
                                      res,
                                      row);

            sAddToReturn(status, 
                         to_ftpcod,
                         to_untcas,
                         to_untpak,
                         to_prtnum,
                         to_client_id,
                         to_lotnum,
                         to_orgcod,
                         to_revlvl,
                         to_catch_qty,
                         to_mandte,
                         to_expire_dte,
                         to_supnum,
                         to_cnsg_flg,
                         RetPtr,
                         res,
                         row);

            if (eOK != status)
            {
                status = sqlRollbackToSavepoint(savepoint);
            }
        }
        srvFreeMemory(SRVRET_STRUCT, CmdPtr);
        CmdPtr = NULL;
        res = NULL;
    }

    if (dtlnum_list)
    {
        /* Process the detail numbers */
        for (dtlnum_ptr = misStrsep(&dtlnum_list, ","); 
             dtlnum_ptr; 
             dtlnum_ptr = misStrsep(&dtlnum_list, ","))
        {
            /* 
             * For each detail number, get all of the information from the 
             * list command so that we can populate the result set
             */
            sprintf(buffer,
                    " list inventory change information "
                    "where dtlnum = '%s' "
                    "  and wh_id = '%s'",
                    dtlnum_ptr, wh_id);

            status = srvInitiateCommand(buffer, &CmdPtr);
            if (eOK != status)
            {
                if (dtlnum_save) free(dtlnum_save);
                srvFreeMemory(SRVRET_STRUCT, RetPtr);
                return(CmdPtr);
            }

            /* 
             * Loop around the details and perform the change 
             */

            res = srvGetResults(CmdPtr);
            for(row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                sprintf(savepoint,
                        "INVFTP%s",
                        sqlGetString(res, row, "dtlnum"));
                status = sqlSetSavepoint(savepoint);

                status = sChangePackaging(to_ftpcod,
                                          to_untcas,
                                          to_untpak,
                                          to_prtnum,
                                          to_client_id,
                                          to_lotnum,
                                          to_orgcod,
                                          to_revlvl,
                                          to_catch_qty,
                                          to_mandte,
                                          to_expire_dte,
                                          to_supnum,
                                          to_cnsg_flg,
                                          reacod,
                                          cmnt,
                                          res,
                                          row);

                sAddToReturn(status, 
                             to_ftpcod,
                             to_untcas,
                             to_untpak,
                             to_prtnum,
                             to_client_id,
                             to_lotnum,
                             to_orgcod,
                             to_revlvl,
                             to_catch_qty,
                             to_mandte,
                             to_expire_dte,
                             to_supnum,
                             to_cnsg_flg,
                             RetPtr,
                             res,
                             row);

                if (eOK != status)
                {
                    status = sqlRollbackToSavepoint(savepoint);
                }
            }
            srvFreeMemory(SRVRET_STRUCT, CmdPtr);
            CmdPtr = NULL;
            res = NULL;
        }
    }

    /*
     * Now that we're done with the details, loop through our saved list
     * of storage locations and re-evaluate the qvl levels
     */

    /* 
     * Free up all the memory we've allocated and leave
     */
    if (dtlnum_save) free(dtlnum_save);

    return(RetPtr);
}
