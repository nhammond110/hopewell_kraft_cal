/*#START***********************************************************************
 *
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/
#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>


/* This is an helper function to create a where-clause for the column names
 * colnam[] if the column exists in the result set and the value is not null
 * for the row. The format is of the form " and colnam1='x' and colnam2=y". The
 * value is quoted if the data type is string. The returned clause uses
 * allocated memory; use free(clause) release the memory.
 */
static long sGetWhereClause(mocaDataRes *res, mocaDataRow *row, char **clause)
{
    typedef struct
    {
        char *x; /* rf_inv_adj column name */
        char *y; /* argument to create_inventory */
    } COLNAM;
    COLNAM colnam[] =
    {
        {"catch_qty", "catch_qty"},
        {"mod_usr_id", "usr_id"},
        {"devcod", "devcod"},
        /*{"lodnum", "lodnum"},*/
        {"prt_client_id", "prt_client_id"},
        {"prtnum", "prtnum"},
        {"stoloc", "srcloc"},
        {"untqty", "untqty"},
        {"wh_id", "wh_id"},
        {"dtlnum", "dtlnum"},
        {"invsts", "invsts"},
        {"lotnum", "lotnum"},
        {"orgcod", "orgcod"},
        {"phyflg", "phyflg"},
        {"asset_typ", "asset_typ"},
        {"sub_asset_typ", "sub_asset_typ"},
        {"revlvl", "revlvl"},
        /*{"subnum", "subnum"},*/
        {"untcas", "untcas"},
        {"untpak", "untpak"},
        {"mandte", "mandte"},
        {"expire_dte", "expire_dte"},
        {NULL, NULL}
    };
    int n;
    char buffer[200];
    *clause=NULL;
    for (n=0; colnam[n].x != NULL; ++n)
    {
        if (sqlIsNull(res, row, colnam[n].x))
            continue;
        switch (sqlGetDataType(res, colnam[n].x))
        {
        case COMTYP_INT:
            sprintf (buffer,
                     " and %s=%ld",
                     colnam[n].y,
                     sqlGetLong(res, row, colnam[n].x));
        break;

        case COMTYP_FLOAT:
            sprintf (buffer,
                     " and %s=%f",
                     colnam[n].y,
                     sqlGetFloat(res, row, colnam[n].x));
        break;

        default:
            sprintf (buffer,
                     " and %s='%s'",
                     colnam[n].y,
                     sqlGetString(res, row, colnam[n].x));
        }

        if (!misDynStrcat (clause, buffer))
        {
            if (*clause)
                free (*clause);
            *clause=NULL;
            return eNO_MEMORY;
         }
    }
    return eOK;
}

/*
 * Check whether asset has been identified for this
 * inventory. If yes we need associate the asset with
 * the inventory.
 * There can be the following scenarios.
 *  1. Asset associcated with a load level part (L)
 *     rf_invasset_adj can have 1 record for the load. 
 *  2. Asset associated with a Case/Detail level part (S/D)    
 *     i.  Load may or may not be on an asset.
 *     ii. Each sub can have individual or multiple subs can be in
 *         a single asset. In this scenario subnum will be populated in 
 *         rf_invadj. We will use this logic to create assets.(assetsub).
 */
static long sCreateRfAssetForInventory(char *devcod,
                                       char *wh_id,
                                       char *lodnum,
                                       char *assetSub_i)
{
    char buffer[4000];
    long ret_status;
    char assetSub[200];

    memset(assetSub, 0, sizeof(assetSub));
    memset(buffer, 0, sizeof(buffer));

    if (assetSub_i && 
        misTrimLen(assetSub_i, sizeof(assetSub)))
    {
        misTrimcpy(assetSub, assetSub_i, sizeof(assetSub));
    }

    sprintf(buffer, "[select count(1) cnt "
                    "   from rf_invasset_adj "
                    "  where lodnum = '%s' "
                    "    and asset_num = '%s' "
                    "    and devcod = '%s' "
                    "    and wh_id = '%s' ] "
                    "| "
                    "if (@cnt > 0) { "
                    "create rf asset for inventory "
                    " where lodnum = '%s' "
                    "   and devcod = '%s' "
                    "   and wh_id  = '%s' "
                    "   and asset_num = '%s'  "
                    "   and mode   = '%s'} "
                    "else { "
                    " set return status "
                    " where status = %ld }",
                    lodnum,
                    lodnum,
                    devcod,
                    wh_id,
                    lodnum,
                    devcod,
                    wh_id,
                    lodnum,
                    CREATE_ASSET_MODE_INV,
                    eDB_NO_ROWS_AFFECTED);
    ret_status = srvInitiateCommand(buffer, NULL);
    if (ret_status != eOK &&
        ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return ret_status;
    }

    if (ret_status == eOK)
    {
        /* After we associate the assets with inventory
         * delete those asset entries from the temporary table.
         */
        sprintf(buffer, "remove rf inventory asset adjust "
                        " where lodnum = '%s' "
                        "   and devcod = '%s' "
                        "   and wh_id  = '%s' "
                        "   and asset_num = '%s' ",
                        lodnum,
                        devcod,
                        wh_id,
                        lodnum);
        ret_status = srvInitiateCommand(buffer, NULL);
        if (ret_status != eOK &&
            ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return ret_status;
        }
    }

    if (strlen(assetSub))
    {
        sprintf(buffer, "[select count(1) cnt "
                        "   from rf_invasset_adj "
                        "  where lodnum = '%s' %s "
                        "    and devcod = '%s' "
                        "    and wh_id  = '%s' ] "
                        "| "
                        "if (@cnt > 0) { "
                        "create rf asset for inventory "
                        " where lodnum = '%s' "
                        "   and devcod = '%s' "
                        "   and wh_id  = '%s' "
                        " %s "
                        "   and mode   = '%s'} "
                        "else { "
                        " set return status "
                        " where status = %ld }",
                        lodnum,
                        assetSub,
                        devcod,
                        wh_id,
                        lodnum,
                        devcod,
                        wh_id,
                        assetSub,
                        CREATE_ASSET_MODE_INV,
                        eDB_NO_ROWS_AFFECTED);
        ret_status = srvInitiateCommand(buffer, NULL);
        if (ret_status != eOK &&
            ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return ret_status;
        }
        if (ret_status == eOK)
        {
            /* After we associate the assets with inventory
             * delete those asset entries from the temporary table.
             */
            sprintf(buffer, "remove rf inventory asset adjust "
                            " where lodnum = '%s' "
                            "   and devcod = '%s' "
                            "   and wh_id  = '%s' "
                            " %s ",
                            lodnum,
                            devcod,
                            wh_id,
                            assetSub);

            ret_status = srvInitiateCommand(buffer, NULL);

            if (ret_status != eOK &&
                ret_status != eDB_NO_ROWS_AFFECTED)
            {
                return ret_status;
            }
        }
    }
    return eOK;
}

LIBEXPORT
RETURN_STRUCT *intAdjustInventoryQuantitiesForCountDiscrepancy (char *devcod_i,
                                                                char *stoloc_i,
                                                                char *wh_id_i,
                                                                char *cntbat_i,
                                                                char *oprcod_i,
                                                                char *sesnum_i)
{
    mocaDataRes *invadjres,  *res, *seradjres, *cntres;
    mocaDataRow *invadjrow,  *row, *seradjrow;
    RETURN_STRUCT *CmdRes = NULL;

    char devcod  [DEVCOD_LEN + 1];
    char stoloc  [STOLOC_LEN + 1];
    char wh_id   [WH_ID_LEN + 1];
    char cntbat  [CNTBAT_LEN + 1];
    char oprcod  [OPRCOD_LEN + 1];
    char srcloc  [STOLOC_LEN + 1];
    char dstloc  [DSTLOC_LEN + 1];
    char seradj_id  [SERADJ_ID_LEN + 1];
    char ser_lvl    [SER_LVL_LEN + 1];
    char cnttyp  [CNTTYP_LEN + 1];
    char buffer[4000];
    char where_clause[500];
    char createStartString[1500];
    char createDestString[500];
    char deleteInvString[500];
    char createSerString[200];
    char updateRfAdjString[500];
    char sesnum  [SESNUM_LEN + 1];
    char whereAdjRefString[200];

    char tmp_buffer[150];
    char tmplod [STOLOC_LEN + 1];
    long ret_status;
    char ems_buffer[2000];
    char assetSub[200];

    long untqty = 0;
    long scanned_qty = 0;
    double catch_qty = 0;
    double scanned_ctch_qty = 0;
    long diff_untqty = 0;
    double diff_catch_qty = 0;
    long max_cnt_seq_num = 0;
    moca_bool_t sernum_req = BOOLEAN_FALSE;
    /* a flag to indicate whether it is LD/SD serial number process
     * for adding inventory in audit count 
     */
    moca_bool_t exists = BOOLEAN_TRUE;

    /* Initialize local variables */

    memset (devcod, 0, sizeof(devcod));
    memset (stoloc, 0, sizeof(stoloc));
    memset (wh_id, 0, sizeof(wh_id));
    memset (cntbat, 0, sizeof(cntbat));
    memset (oprcod, 0, sizeof(oprcod));
    memset (dstloc, 0, sizeof(dstloc));
    memset (srcloc, 0, sizeof(srcloc));
    memset (sesnum, 0, sizeof(sesnum));
    memset (cnttyp, 0, sizeof(cnttyp));
    memset (ems_buffer, 0, sizeof(ems_buffer));
    memset (seradj_id, 0, sizeof(seradj_id));
    memset (ser_lvl, 0, sizeof(ser_lvl));
    
    invadjres = res = seradjres = cntres = NULL;
   
    if (devcod_i && misTrimLen(devcod_i, DEVCOD_LEN))
        misTrimcpy(devcod, devcod_i, DEVCOD_LEN);
    else
        strncpy(devcod,
                osGetVar(LESENV_DEVCOD) ? osGetVar(LESENV_DEVCOD) : "",
                DEVCOD_LEN);

    if (!stoloc_i || misTrimLen(stoloc_i, STOLOC_LEN) == 0)
        return (APPMissingArg("stoloc"));

    misTrimcpy(stoloc, stoloc_i, STOLOC_LEN);

    if (!wh_id_i || misTrimLen(wh_id_i, WH_ID_LEN) == 0)
        return (APPMissingArg("wh_id"));

    misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

    if (!cntbat_i || misTrimLen(cntbat_i, CNTBAT_LEN) == 0)
        return (APPMissingArg("cntbat"));

    misTrimcpy(cntbat, cntbat_i, CNTBAT_LEN);

    if (!oprcod_i || misTrimLen(oprcod_i, OPRCOD_LEN) == 0)
        return (APPMissingArg("oprcod"));

    misTrimcpy(oprcod, oprcod_i, OPRCOD_LEN);

    if (sesnum_i && misTrimLen(sesnum_i, SESNUM_LEN))
        misTrimcpy(sesnum, sesnum_i, SESNUM_LEN);

    /* Get the lost location defined on the source area,
     * it defines the destination for unscanned inventory.
     * If it is not defined, we'll send it to the standard
     * count adjustment area.
     */

    sprintf(buffer,
            "select lost_loc dstloc "
            "  from aremst, "
            "       locmst "
            " where aremst.arecod = locmst.arecod "
            "   and aremst.wh_id = locmst.wh_id "
            "   and locmst.stoloc = '%s' "
            "   and locmst.wh_id = '%s' "
            "   and aremst.lost_loc is not null ",
            stoloc,
            wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        row = sqlGetRow(res);
        misTrimcpy(dstloc, sqlGetString(res, row, "dstloc"), STOLOC_LEN);
        /* Now validate that the location exists */
        ret_status = appValidateEntity("locmst", NULL,
                                       "stoloc", dstloc, COMTYP_STRING,
                                        STOLOC_LEN,
                                       "wh_id", wh_id, COMTYP_STRING,
                                        WH_ID_LEN,
                                        NULL); 
        sqlFreeResults(res);
        res = NULL;

        if (eDB_NO_ROWS_AFFECTED == ret_status)
        {
            return APPInvalidArg(dstloc, "stoloc");
        }
        else if (eOK != ret_status)
        {
            return(srvResults(ret_status, NULL));
        }
    }
    else
    {
         sqlFreeResults(res);
         res = NULL;
         misTrimcpy(dstloc, STOLOC_PERM_COUNTS, STOLOC_LEN);    
    }

    /* Now get the rf_invadj rows for this count audit */
    /* If none exist, then that means there's no inventory there so */
    /* just complete the audit count. */

    /* If it is zero count then insert rf_invadj with 0 count for cnt_seq_num 1 
       if there is no entry */
    sprintf(buffer,
        "   check rf invadj for max cntseq "
        "    where devcod = '%s'           "
        "      and stoloc = '%s'           "
        "      and wh_id  = '%s'           "
        "      and oprcod = '%s'           "
        "      and cntbat = '%s'           ",
        devcod,
        stoloc,
        wh_id,
        oprcod,
        cntbat);
    ret_status = srvInitiateCommand(buffer, &CmdRes);
    res = srvGetResults(CmdRes);
    row = sqlGetRow(res);
    max_cnt_seq_num = sqlGetLong(res, row, "cnt_seq_num");
    srvFreeMemory (SRVRET_STRUCT, CmdRes);
    CmdRes = NULL;
    res = NULL;

    misTrc(T_FLOW, "Getting the rf_invadj rows for this audit... ");
    
    sprintf(buffer,
            "   select rf_invadj.*,                     "
            "          prtmst_view.catch_cod,           "
            "          prtmst_view.lodlvl,              "
            "          prtmst_view.ser_lvl              "
            "     from rf_invadj,                       "
            "          prtmst_view                      "
            "    where rf_invadj.prtnum = prtmst_view.prtnum "
            "      and rf_invadj.prt_client_id = prtmst_view.prt_client_id "
            "      and rf_invadj.wh_id  = prtmst_view.wh_id "
            "      and rf_invadj.devcod = '%s'          "
            "      and rf_invadj.stoloc = '%s'          "
            "      and rf_invadj.wh_id  = '%s'          "
            "      and rf_invadj.oprcod = '%s'          "
            "      and rf_invadj.cntbat = '%s'          "
            "      and rf_invadj.cnt_seq_num = '%d'     "
            "      and app_req_flg <> 1                 "
            " order by cnt_seq_num desc                 ",
            devcod,
            stoloc,
            wh_id,
            oprcod,
            cntbat,
            max_cnt_seq_num);

    ret_status = sqlExecStr(buffer, &invadjres);

    if ((eOK != ret_status) && (eDB_NO_ROWS_AFFECTED != ret_status))
    {
        sqlFreeResults(invadjres);
        return (srvResults(ret_status, NULL));
    }

    /* Okay, now we can start processing each row. If the actual and scanned */
    /* quantities don't match up, we'll manipulate the inventory until they  */
    /* do.                                                                   */

    misTrc(T_FLOW, "For each row in the rf_invadj we need to see if "
                   "we need to adjust the inventory - add or delete "
                   "in the location based on what they scanned.     ");

    for (invadjrow=sqlGetRow(invadjres); invadjrow; 
                                         invadjrow=sqlGetNextRow(invadjrow))
    {

        untqty = 0;
        scanned_qty = 0;
        catch_qty = 0.0;
        scanned_ctch_qty = 0.0;
        diff_untqty = 0;
        diff_catch_qty = 0;
        exists = BOOLEAN_TRUE;

        untqty = sqlGetLong(invadjres, invadjrow, "untqty");
        scanned_qty = sqlGetLong(invadjres, invadjrow, "scanned_qty");
        catch_qty = sqlGetFloat(invadjres, invadjrow, "catch_qty");
        scanned_ctch_qty = sqlGetFloat(invadjres, invadjrow,"scanned_ctch_qty");

        misTrc(T_FLOW, "Lodnum: %s, Subnum: %s, Dtlnum: %s, Prtnum: %s, "
                       "Prt_client: %s, Untqty: %ld, Scanned qty: %ld, "
                        "catch qty: %ld, scanned catch qty: %ld, ",
                       sqlGetString(invadjres, invadjrow, "lodnum"),
                       sqlGetString(invadjres, invadjrow, "subnum"),
                       sqlGetString(invadjres, invadjrow, "dtlnum"),
                       sqlGetString(invadjres, invadjrow, "prtnum"),
                       sqlGetString(invadjres, invadjrow, "prt_client_id"),
                       untqty, scanned_qty, catch_qty, scanned_ctch_qty);

        memset(where_clause, 0, sizeof(where_clause));
        memset(tmplod, 0, sizeof(tmplod));
        memset(createStartString, 0, sizeof(createStartString));
        memset(createDestString, 0, sizeof(createDestString));
        memset(deleteInvString, 0, sizeof(deleteInvString));
        memset(updateRfAdjString, 0, sizeof(updateRfAdjString));
        memset(whereAdjRefString, 0, sizeof(whereAdjRefString));
        memset(assetSub, 0, sizeof(assetSub));

        if (untqty > 0 && (!sqlIsNull(invadjres, invadjrow, "lodnum")))
        {
            sprintf (tmp_buffer, " and invlod.lodnum = '%s' ",
                     sqlGetString(invadjres, invadjrow, "lodnum") );
            strcat(where_clause, tmp_buffer);

            sprintf (tmp_buffer, " and dstlod = '%s' ",
                     sqlGetString(invadjres, invadjrow, "lodnum"));
            strcat(createDestString, tmp_buffer);

            sprintf (tmp_buffer, " and lodnum = '%s' ",
                     sqlGetString(invadjres, invadjrow, "lodnum") );
            strcat(updateRfAdjString, tmp_buffer);
        }
        else
        {
            ret_status = appNextNum(NUMCOD_LODNUM, tmplod);

            if (ret_status != eOK)
            {
                sqlFreeResults(invadjres);
                return (srvResults(ret_status, NULL));
            }

            sprintf (tmp_buffer, " and lodnum = '%s' ", tmplod);
            strcat(createDestString, tmp_buffer);

            if (untqty == 0 && (!sqlIsNull(invadjres, invadjrow, "lodnum")))
                sprintf (tmp_buffer, " and lodnum = '%s' ",
                     sqlGetString(invadjres, invadjrow, "lodnum") );
            else
                sprintf (tmp_buffer, " and lodnum is null ");
            strcat(updateRfAdjString, tmp_buffer);

        }


        if (untqty > 0 && (!sqlIsNull(invadjres, invadjrow, "subnum")))
        {
            sprintf (tmp_buffer, " and invsub.subnum = '%s' ",
                     sqlGetString(invadjres, invadjrow, "subnum") );
            strcat(where_clause, tmp_buffer);

            sprintf (tmp_buffer, " and dstsub = '%s' ",
                     sqlGetString(invadjres, invadjrow, "subnum") );
            strcat(createDestString, tmp_buffer);

            sprintf (tmp_buffer, " and subnum = '%s' ",
                     sqlGetString(invadjres, invadjrow, "subnum") );
            strcat(updateRfAdjString, tmp_buffer);

        }
        else 
        if(untqty == 0 && (!sqlIsNull(invadjres, invadjrow, "subnum")))
        {
            /* record subnum to avoid over-updating rf_invadj */
            sprintf (tmp_buffer, " and subnum = '%s' ",
                 sqlGetString(invadjres, invadjrow, "subnum") );
            strcat(updateRfAdjString, tmp_buffer);
        }

        if (!sqlIsNull(invadjres, invadjrow, "subnum"))
        {
            sprintf (assetSub, " and asset_num = '%s' ",
                     sqlGetString(invadjres, invadjrow, "subnum") );
        }

        if (!sqlIsNull(invadjres, invadjrow, "dtlnum"))
        {
            sprintf (tmp_buffer, " and invdtl.dtlnum = '%s' ",
                     sqlGetString(invadjres, invadjrow, "dtlnum") );
            strcat(where_clause, tmp_buffer);

            sprintf (tmp_buffer, " and dtlnum = '%s' ",
                     sqlGetString(invadjres, invadjrow, "dtlnum") );
            strcat(updateRfAdjString, tmp_buffer);

        }

        if (!sqlIsNull(invadjres, invadjrow, "lotnum") &&
             misCiStrcmp(sqlGetString(invadjres, invadjrow, "lotnum"),
                         DEFAULT_INV_ATTR))
        {
            sprintf (tmp_buffer, " and lotnum = '%s' ",
                     sqlGetString(invadjres, invadjrow, "lotnum") );
            strcat(where_clause, tmp_buffer);
        }

        if (!sqlIsNull(invadjres, invadjrow, "orgcod") &&
            misCiStrcmp(sqlGetString(invadjres, invadjrow, "orgcod"),
                        DEFAULT_INV_ATTR))
        {
            sprintf (tmp_buffer, " and orgcod = '%s' ",
                     sqlGetString(invadjres, invadjrow, "orgcod") );
            strcat(where_clause, tmp_buffer);
        }

        if (!sqlIsNull(invadjres, invadjrow, "revlvl") &&
            misCiStrcmp(sqlGetString(invadjres, invadjrow, "revlvl"),
                        DEFAULT_INV_ATTR))
        {
            sprintf (tmp_buffer, " and revlvl = '%s' ",
                     sqlGetString(invadjres, invadjrow, "revlvl") );
            strcat(where_clause, tmp_buffer);
        }

        if (!sqlIsNull(invadjres, invadjrow, "reacod"))
        {
            sprintf (tmp_buffer, " and reacod = '%s' ",
                     sqlGetString(invadjres, invadjrow, "reacod") );
            strcat(whereAdjRefString, tmp_buffer);
        }

        if (!sqlIsNull(invadjres, invadjrow, "adj_ref1"))
        {
            sprintf (tmp_buffer, " and adj_ref1 = '%s' ",
                     sqlGetString(invadjres, invadjrow, "adj_ref1") );
            strcat(whereAdjRefString, tmp_buffer);
        }

        if (!sqlIsNull(invadjres, invadjrow, "adj_ref2"))
        {
            sprintf (tmp_buffer, " and adj_ref2 = '%s' ",
                     sqlGetString(invadjres, invadjrow, "adj_ref2") );
            strcat(whereAdjRefString, tmp_buffer);
        }
        
        misTrimcpy(ser_lvl, sqlGetString(invadjres, invadjrow, "ser_lvl"), 
                           SER_LVL_LEN);
        misTrimcpy(seradj_id, sqlGetString(invadjres, invadjrow, "seradj_id"), 
                           SERADJ_ID_LEN);

        if ((catch_qty != scanned_ctch_qty) &&
            misCiStrcmp (sqlGetString(invadjres, invadjrow, "catch_cod"),
                          CATCH_COD_RECEIVING) == 0)
        {
            /* They are telling                                         */
            /* us that the catch_qty is wrong so we'll just update the  */
            /* catch qty for this record.                               */
            /* Fortunately, for catch_cod inventory, we should always   */
            /* have an inventory identifier.                            */  
               
            misTrc(T_FLOW, "Catch quantity is different, we'll update it...");

            sprintf(buffer,
                    "  [select invdtl.dtlnum  "
                    "     from invdtl,        "
                    "          invsub,        "
                    "          invlod         "
                    "    where invdtl.subnum = invsub.subnum "
                    "      and invsub.lodnum = invlod.lodnum "
                    "      and invlod.stoloc = '%s'          "
                    "      and invlod.wh_id  = '%s'          "
                    "       %s                               "
                    "      and invdtl.prtnum = '%s'          "
                    "      and invdtl.prt_client_id = '%s'   "
                    "      and invdtl.invsts = '%s'          "
                    "      and nvl(lotnum,'----') = nvl('%s','----') "
                    "      and nvl(orgcod,'----') = nvl('%s','----') "
                    "      and nvl(revlvl,'----') = nvl('%s','----') "
                    "      and invdtl.untcas = %ld           "
                    "      and invdtl.untpak = %ld           "
                    "      and rownum < 2]                   "
                    "   |                                    "
                    "   [update invdtl                       "
                    "       set catch_qty = %lf              "
                    "     where dtlnum = @dtlnum]            ",
                    stoloc,
                    wh_id,
                    where_clause ? where_clause : "",
                    sqlGetString(invadjres, invadjrow, "prtnum"),
                    sqlGetString(invadjres, invadjrow, "prt_client_id"),
                    sqlGetString(invadjres, invadjrow, "invsts"),
                    sqlGetString(invadjres, invadjrow, "lotnum"),
                    sqlGetString(invadjres, invadjrow, "orgcod"),
                    sqlGetString(invadjres, invadjrow, "revlvl"),
                    sqlGetLong(invadjres, invadjrow, "untcas"),
                    sqlGetLong(invadjres, invadjrow, "untpak"),
                    scanned_ctch_qty);

            ret_status = srvInitiateCommand(buffer, NULL);
             
            if (eOK != ret_status) 
            {
                sqlFreeResults (invadjres);
                return (srvResults(ret_status, NULL));
            }
        }

        if (scanned_qty != untqty)
        {
            /* Get the close location method */
            sprintf(buffer,
                    "select cls_loc_cnt_mthd, "
                    "       cnt_usr_id "
                    "  from cnttyp, "
                    "       cntwrk "
                    " where cnttyp.cnttyp = cntwrk.cnttyp "
                    "   and cntwrk.cntbat = '%s' "
                    "   and cntwrk.stoloc = '%s' ",
                    cntbat,
                    stoloc);

            ret_status = sqlExecStr(buffer, &cntres);

            if (ret_status != eOK)
            {
                sqlFreeResults(invadjres);
                sqlFreeResults(cntres);
                return(srvResults(ret_status, NULL));
            }
            else
            {
                row = sqlGetRow(cntres);

                /* adjustment is for a count with close location 
                 * method of standard/manual 
                 */
                if (misCiStrcmp(sqlGetString(cntres, row, "cls_loc_cnt_mthd"),
                                                                    "M") == 0)
                {
                    /* call validate inventory adjustment for user */
                    sprintf(buffer,
                        "validate inventory adjustment for user "
                        "   where inv_adj_qty   = %ld  "
                        "     and wh_id         = '%s' "
                        "     and prtnum        = '%s' "
                        "     and prt_client_id = '%s' "
                        "     and usr_id        = '%s' ",
                        abs(scanned_qty - untqty),
                        wh_id,
                        sqlGetString(invadjres, invadjrow, "prtnum"),
                        sqlGetString(invadjres, invadjrow, "prt_client_id"),
                        sqlGetString(invadjres, invadjrow, "mod_usr_id"));

                    ret_status = srvInitiateCommand(buffer, &CmdRes);
                    if (ret_status != eOK)
                    {
                        sqlFreeResults(invadjres);
                        sqlFreeResults(cntres);
                        srvFreeMemory (SRVRET_STRUCT, CmdRes);
                        return srvResults(ret_status, NULL);
                    }
                    else
                    {
                        res = srvGetResults(CmdRes);
                        row = sqlGetRow(res);

                        /* difference is outside user's tolerance */
                        if (sqlGetLong(res, row, "userallowed") == 0) 
                        {
                            /* update location master */
                            sprintf(buffer,
                                    "  update locmst              "
                                    "     set locsts = 'L'        "
                                    "   where stoloc = '%s'       "
                                    "     and wh_id  = '%s'       ",
                                    stoloc,
                                    wh_id);
                            
                            ret_status = sqlExecStr(buffer, NULL);
                            if (eOK != ret_status) 
                            {
                                sqlFreeResults(invadjres);
                                sqlFreeResults(cntres);
                                srvFreeMemory (SRVRET_STRUCT, CmdRes);
                                return srvResults(ret_status, NULL);
                            }

                            /* update app_req_flag of rf_invadj */
                            sprintf(buffer,
                                    "  update rf_invadj            "
                                    "     set app_req_flg = 1      "
                                    "   where devcod = '%s'        "
                                    "     and cntbat = '%s'        "
                                    "     and stoloc = '%s'        "
                                    "     and wh_id  = '%s'        "
                                    "     and oprcod = '%s'        "
                                    "     and prtnum = '%s'        "
                                    "     and prt_client_id = '%s' "
                                    "     and invsts = '%s'        "
                                    "     and nvl(lotnum,'----') = nvl('%s','----') "
                                    "     and nvl(orgcod,'----') = nvl('%s','----') "
                                    "     and nvl(revlvl,'----') = nvl('%s','----') "
                                    "     and untcas = %ld         "
                                    "     and untpak = %ld         "
                                    "     %s                       "
                                    "     %s                       "
                                    "     and cnt_seq_num = %ld    ",
                                    devcod,
                                    cntbat,
                                    stoloc,
                                    wh_id,
                                    oprcod,
                                    sqlGetString(invadjres, invadjrow, "prtnum"),
                                    sqlGetString(invadjres, invadjrow, "prt_client_id"),
                                    sqlGetString(invadjres, invadjrow, "invsts"),
                                    sqlGetString(invadjres, invadjrow, "lotnum"),
                                    sqlGetString(invadjres, invadjrow, "orgcod"),
                                    sqlGetString(invadjres, invadjrow, "revlvl"),
                                    sqlGetLong(invadjres, invadjrow, "untcas"),
                                    sqlGetLong(invadjres, invadjrow, "untpak"),
                                    updateRfAdjString ? updateRfAdjString : "",
                                    whereAdjRefString ? whereAdjRefString : "",
                                    sqlGetLong(invadjres, invadjrow,"cnt_seq_num"));

                            ret_status = sqlExecStr(buffer, NULL);
     
                            if (eOK != ret_status) 
                            {
                                sqlFreeResults(invadjres);
                                sqlFreeResults(cntres);
                                srvFreeMemory (SRVRET_STRUCT, CmdRes);
                                return srvResults(ret_status, NULL);
                            }

                            /* trigger new approval ems event */
                            sprintf(buffer,
                                "raise ems event for adjustment approval "
                                "    where usr_id        = '%s'          "
                                "      and stoloc        = '%s'          "
                                "      and inv_adj_qty   = '%ld'         "
                                "      and prtnum        = '%s'          "
                                "      and prt_client_id = '%s'          "
                                "      and wh_id         = '%s'          ",
                                sqlGetString(invadjres, invadjrow, "mod_usr_id"),
                                stoloc,
                                abs(scanned_qty - untqty),
                                sqlGetString(invadjres, invadjrow, "prtnum"),
                                sqlGetString(invadjres, invadjrow, "prt_client_id"),
                                sqlGetString(invadjres, invadjrow, "wh_id"));
                                
                            ret_status = srvInitiateCommand(buffer, NULL);
                            if (eOK != ret_status)
                            {
                                sqlFreeResults(invadjres);
                                sqlFreeResults(cntres);
                                srvFreeMemory (SRVRET_STRUCT, CmdRes);
                                return srvResults(ret_status, NULL);
                            }
                            
                            sqlFreeResults(cntres);
                            srvFreeMemory (SRVRET_STRUCT, CmdRes);
                            CmdRes = NULL;
                            continue;
                        }
                        else
                        {
                            if(CmdRes)
                            {
                                srvFreeMemory (SRVRET_STRUCT, CmdRes);
                            }
                        }
                    }
                }
            }
            sqlFreeResults(cntres);
            cntres = NULL;
        }

        if ((scanned_qty != untqty) && 
             strcmp(oprcod,OPRCOD_COUNT) == 0)
        {
           misTrc(T_FLOW, "Raising EMS to tell there is a dicrepancy in the count...");

           sprintf(ems_buffer,
                    " raise ems event for adjustment discrepancy below threshold "
                    "  where evt_nam     = 'WMD-ADJ-DISCREPANCY'"
                    "    and stoloc      = '%s' "
                    "    and prtnum      = '%s' "
                    "    and devcod      = '%s' "
                    "    and cntbat      = '%s' "
                    "    and untqty      = '%d' "
                    "    and scanned_qty = '%d' "
                    "    and wh_id       = '%s' ",
                    sqlGetString(invadjres, invadjrow, "stoloc"),
                    sqlGetString(invadjres, invadjrow, "prtnum"),
                    sqlGetString(invadjres, invadjrow, "devcod"),
                    sqlGetString(invadjres, invadjrow, "cntbat"),
                    untqty,
                    scanned_qty,
                    sqlGetString(invadjres, invadjrow, "wh_id")); 
                    
            ret_status = srvInitiateCommand(ems_buffer, NULL);
        }

        if (scanned_qty > untqty) 
        {
            /* They're telling us that the quantity has increased, 
             * so we're going to add inventory to match.
             * We can only do this for load tracked and subload tracked 
             * inventory because detail tracked inventory will only ever
             * have a quantity of 1. They would have had to add it manually
             * through the RF form. 
             */
            misTrc(T_FLOW, "Scanned quantity is greater than untqty, "
                           "we're going to add inventory here. ");
         
            diff_untqty = (scanned_qty - untqty);

            diff_catch_qty = (scanned_ctch_qty - catch_qty);

            /* First, we'll pick up some of the inventory values */
            /* using existing inventory as a template. */
            
            sprintf(createStartString,
                    "  [select prtnum,     "
                    "          prt_client_id, "
                    "          ftpcod,     "
                    "          fifdte,     "
                    "          age_pflnam, "
                    "          expire_dte, "
                    "          mandte,     "
                    "          invsts,     "
                    "          lotnum,     "
                    "          revlvl,     "
                    "          orgcod,     "
                    "          untcas,     "
                    "          untpak,     "
                    "          invlod.asset_typ,    "
                    "          invsub.asset_typ sub_asset_typ "
                    "     from invdtl,              "
                    "          invsub,              "
                    "          invlod               "
                    "    where invdtl.subnum = invsub.subnum "
                    "      and invsub.lodnum = invlod.lodnum "
                    "      and invlod.stoloc = '%s'          "
                    "      and invlod.wh_id  = '%s'          "
                    "       %s                               "
                    "      and prtnum = '%s'          "
                    "      and prt_client_id = '%s'   "
                    "      and invsts = '%s'          "
                    "      and untcas = %ld           "
                    "      and untpak = %ld           "
                    "      and rownum < 2             "
                    " group by prtnum,                "
                    "          prt_client_id,         "
                    "          ftpcod,                "
                    "          fifdte,                "
                    "          age_pflnam,            "
                    "          expire_dte,            "
                    "          mandte,                "
                    "          invsts,                "
                    "          lotnum,                "
                    "          revlvl,                "
                    "          orgcod,                "
                    "          untcas,                "
                    "          untpak]                ",
                    stoloc,
                    wh_id,
                    where_clause ? where_clause : "",
                    sqlGetString(invadjres, invadjrow, "prtnum"),
                    sqlGetString(invadjres, invadjrow, "prt_client_id"),
                    sqlGetString(invadjres, invadjrow, "invsts"),
                    sqlGetLong(invadjres, invadjrow, "untcas"),
                    sqlGetLong(invadjres, invadjrow, "untpak"));

			/* untqty == 0 indicate this process is adding inventory
			 * and now only process LD/SD adding inventory for audit count
			 */
			if(untqty == 0 &&
		       !misTrimStrncmp(ser_lvl,SER_LVL_DETAIL,SER_LVL_LEN) &&
			   (!misTrimStrncmp(sqlGetString(invadjres, invadjrow, "lodlvl"), 
			                    LODLVL_LOAD, LODLVL_LEN) || 
				!misTrimStrncmp(sqlGetString(invadjres, invadjrow, "lodlvl"), 
			                    LODLVL_SUBLOAD, LODLVL_LEN)))
			{
				exists = BOOLEAN_FALSE;
				sprintf(createStartString,
					 " publish data "
					 "    where prtnum        = '%s' "
					 "      and prt_client_id = '%s' "
                     "      and invsts        = '%s' "
                     "      and lotnum        = '%s' "
                     "      and revlvl        = '%s' "
                     "      and orgcod        = '%s' "
                     "      and untcas        = %ld "
                     "      and untpak        = %ld ",
                     sqlGetString(invadjres, invadjrow, "prtnum"),
                     sqlGetString(invadjres, invadjrow, "prt_client_id"),
                     sqlGetString(invadjres, invadjrow, "invsts"),
                     sqlGetString(invadjres, invadjrow, "lotnum"),
                     sqlGetString(invadjres, invadjrow, "revlvl"),
                     sqlGetString(invadjres, invadjrow, "orgcod"),
                     sqlGetLong(invadjres, invadjrow, "untcas"),
                     sqlGetLong(invadjres, invadjrow, "untpak"));

                /* mandte and expire_dte can be NULL, so we need to 
                 * check them before using. 
                 */
                if (!sqlIsNull(invadjres, invadjrow, "mandte"))
                {
                    strcat(createStartString, " and mandte = '");
                    strcat(createStartString, 
                           sqlGetString(invadjres, invadjrow, "mandte"));
                    strcat(createStartString, "' ");
                }
                if (!sqlIsNull(invadjres, invadjrow, "expire_dte"))
                {
                    strcat(createStartString, " and expire_dte = '");
                    strcat(createStartString, 
                           sqlGetString(invadjres, invadjrow, "expire_dte"));
                    strcat(createStartString, "' ");
                }
            }

            if (sqlIsNull(invadjres, invadjrow, "dtlnum"))
            {

                /* It's not detail tracked inventory. */

                if (scanned_ctch_qty < catch_qty)
                    diff_catch_qty = 0.00;
                    
                misTrimcpy(srcloc, STOLOC_PERM_ADJUST, STOLOC_LEN);

                /*
                 * WMD-20065
                 * deal with serialized part audit
                 */
                sprintf(buffer,
                        " check serial number capture required "
                        "        where prtnum = '%s' "
                        "          and prt_client_id = '%s' "
                        "          and wh_id = '%s' "
                        "          and cur_frm_typ = '%s' ",
                        sqlGetString(invadjres, invadjrow, "prtnum"),
                        sqlGetString(invadjres, invadjrow, "prt_client_id"),
                        wh_id,
                        CUR_FRM_TYP_INBOUND);

                ret_status = srvInitiateCommand(buffer, NULL);
                
                sernum_req = BOOLEAN_FALSE;
                if (ret_status == eOK)
                {
                    /* The following gathers the RF_SERADJ Serial number 
                     * adjustment rows and update the Serial Number in
                     * INV_SER_NUM with the RF_SERADJ Serial number.
                     * If the Serial number in RF_SERADJ is null, we delete
                     * the inventory and related record in INV_SER_NUM.
                     */
                     
                     sernum_req = BOOLEAN_TRUE;

                    sprintf(buffer,
                            " select * from rf_seradj "
                            "  where seradj_id = '%s' "
                            "    and devcod = '%s' "
                            "    and stoloc = '%s' "
                            "    and wh_id  = '%s' "
                            "    and lodnum = nvl('%s',lodnum) "
                            "    and subnum = nvl('%s',subnum) "
                            "    and dtlnum = nvl('%s',dtlnum) "
                            "    and ser_num is not null "
                            "    and not exists "
                            "       (select 'x' "
                            "          from inv_ser_num "
                            "         where ser_num_typ_id = rf_seradj.ser_num_typ_id "
                            "           and invtid = rf_seradj.lodnum "
                            "           and ser_lvl = rf_seradj.ser_lvl "
                            "           and ser_lvl = 'L') "
                            "    and not exists "
                            "       (select 'x' "
                            "          from inv_ser_num "
                            "         where ser_num_typ_id = rf_seradj.ser_num_typ_id "
                            "           and invtid = rf_seradj.subnum "
                            "           and ser_lvl = rf_seradj.ser_lvl "
                            "           and ser_lvl = 'S') "
                            "    and not exists "
                            "       (select 'x' "
                            "          from inv_ser_num "
                            "         where ser_num_typ_id = rf_seradj.ser_num_typ_id "
                            "           and invtid = rf_seradj.dtlnum "
                            "           and ser_lvl = rf_seradj.ser_lvl "
                            "           and ser_lvl = 'D') ",
                            seradj_id,
                            devcod,
                            stoloc,
                            wh_id,
                            sqlGetString(invadjres, invadjrow, "lodnum"),
                            sqlGetString(invadjres, invadjrow, "subnum"),
                            sqlGetString(invadjres, invadjrow, "dtlnum"));

                    ret_status = sqlExecStr(buffer, &seradjres);
             
                    if (ret_status != eOK)
                    {
                        sqlFreeResults(seradjres);
                        sqlFreeResults(invadjres);
                        return srvResults(ret_status, NULL);
                    }

                    for (seradjrow=sqlGetRow(seradjres); seradjrow;
                                   seradjrow=sqlGetNextRow(seradjrow))
                    {
                        /* Need to redo the Inventory destination string 
                         * with the IDs in the rf_seradj table as we already
                         * have generated them.
                         */
                         
                        memset(createDestString, 0, sizeof(createDestString));
                        memset(createSerString, 0, sizeof(createSerString));
                        
                        if(exists)
						{
	                        sprintf (createDestString, " and dstlod = '%s' ",
	                             sqlGetString(seradjres, seradjrow, "lodnum"));
						}
						else
						{
							sprintf (createDestString, " and lodnum = '%s' ",
	                             sqlGetString(seradjres, seradjrow, "lodnum"));
						}

                        if (!misTrimStrncmp(ser_lvl,SER_LVL_LOAD,SER_LVL_LEN))
                        {
                            /* Serial number ties to a lodnum */
                            sprintf (createSerString, " and invtid = '%s' ",
                                 sqlGetString(seradjres, seradjrow, "lodnum"));
                        }
                       
                        if (!misTrimStrncmp(ser_lvl,SER_LVL_SUBLOAD,SER_LVL_LEN))
                        {
                            /* Serial number ties to a subnum */
                            sprintf (createSerString, " and invtid = '%s' ",
                                 sqlGetString(seradjres, seradjrow, "subnum"));
                                 
                            sprintf (tmp_buffer, " and subnum = '%s' ",
                                   sqlGetString(seradjres,seradjrow,"subnum"));
                            strcat(createDestString, tmp_buffer);
                        }
                        
                        if (!misTrimStrncmp(ser_lvl,SER_LVL_DETAIL,SER_LVL_LEN))
                        {
                            /* Serial number ties to a dtlnum */
                            diff_untqty = 1;
                            
                            sprintf (createSerString, " and invtid = '%s' ",
                                 sqlGetString(seradjres, seradjrow, "dtlnum"));
                                 
                            sprintf (tmp_buffer, " and subnum = '%s' ",
                                   sqlGetString(seradjres,seradjrow,"subnum"));
                            strcat(createDestString, tmp_buffer);
                                   
                            sprintf (tmp_buffer, " and dtlnum = '%s' ",
                                   sqlGetString(seradjres,seradjrow,"dtlnum")); 
                            strcat(createDestString, tmp_buffer);
                        }

                        /* We're going to check the existence of the detail 
                         * The rf_seradj.dtlnum field is part of the PK so 
                         * we know it's there and it's unique across the
                         * inventory in this table so we will make a check 
                         * so we don't try to create it more then once, which 
                         * would occur with multiple serial number types.
                         */

                        sprintf(buffer,
                                " select 'x' from invdtl where dtlnum = '%s' ",
                                sqlGetString(invadjres,invadjrow,"dtlnum"));

                        ret_status = sqlExecStr(buffer, NULL);

                        if (ret_status == eDB_NO_ROWS_AFFECTED)
                        { 
                            sprintf(buffer,
                                    " %s                                    "
                                    " |                                     "
                                    " create inventory                      "
                                    "  where prtnum = @prtnum               "
                                    "    and prt_client_id = @prt_client_id "
                                    "    and invsts = @invsts               "
                                    "    and untcas = @untcas               "
                                    "    and untpak = @untpak               "
                                    "    and lotnum = @lotnum               "
                                    "    and orgcod = @orgcod               "
                                    "    and asset_typ = @asset_typ         "
                                    "    and sub_asset_typ = @sub_asset_typ "
                                    "    and revlvl = @revlvl               "
                                    "    and age_pflnam = @age_pflnam       "
                                    "    and fifdte = @fifdte               "
                                    "    and expire_dte = @expire_dte       "
                                    "    and mandte = @mandte               "
                                    "    and ftpcod = @ftpcod               "
                                    "    and srcloc = '%s'                  "
                                    "    and dstloc = '%s'                  "
                                    "    and wh_id  = '%s'                  "
                                    "    %s                                 "
                                    "    and usr_id = '%s'                  "
                                    "    and devcod = '%s'                  "
                                    "    and movref = '%s'                  "
                                    "    and client_id = @prt_client_id     "
                                    "    and oprcod = '%s'                  "
                                    "    and catch_qty = %lf                "
                                    "    and untqty = %ld                   "
                                   "    and create_missing_ids = %ld       "
                                    "    %s                                 ",
                                    createStartString,
                                    srcloc,
                                    stoloc,
                                    wh_id,
                                    createDestString,
                                    osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "",
                                    devcod,
                                    sesnum,
                                    oprcod,
                                    diff_catch_qty,
                                    diff_untqty,
                                    BOOLEAN_TRUE, 
                                    whereAdjRefString ? whereAdjRefString : "");

                                ret_status = srvInitiateCommand(buffer, NULL);

                                if (ret_status != eOK)
                                {
                                    sqlFreeResults(invadjres);
                                    sqlFreeResults(seradjres);
                                    return (srvResults(ret_status, NULL));
                                }
                                /*
                                 * Check whether asset has been identified for this
                                 * inventory. If yes we need associate the asset with
                                 * the inventory.
                                 */
                                ret_status = sCreateRfAssetForInventory(
                                                sqlGetString(invadjres, invadjrow, "devcod"),
                                                sqlGetString(invadjres, invadjrow, "wh_id"),
                                                sqlGetString(invadjres, invadjrow, "lodnum"),
                                                assetSub);
                                if (ret_status != eOK)
                                {
                                    sqlFreeResults(invadjres);
                                    sqlFreeResults(seradjres);
                                    return (srvResults(ret_status, NULL));
                                }
                        }

                        /* Store the Serial Number in the inv_ser_num table 
                         * thus tying it to the inventory
                         */

                        sprintf(buffer,
                                " create inventory serial number "
                                "  where ser_num = '%s' "
                                "    and ser_num_typ_id = '%s' "
                                "    and ser_lvl = '%s' "
                                "    and wh_id = '%s' "
                                "  %s ",
                                sqlGetString(seradjres, seradjrow, "ser_num"),
                                sqlGetString(seradjres, seradjrow, "ser_num_typ_id"),
                                ser_lvl,
                                wh_id,
                                createSerString);
                        ret_status = srvInitiateCommand(buffer, NULL);

                        if (ret_status != eOK)
                        {
                            sqlFreeResults(invadjres);
                            sqlFreeResults(seradjres);
                            return (srvResults(ret_status, NULL));
                        }
                    }
                }                

                /* Now we'll update the row in the rf_invadj table. */

                sprintf(buffer,
                        "  update rf_invadj            "
                        "     set untqty = %ld,        "
                        "         catch_qty = %lf      "
                        "   where devcod = '%s'        "
                        "     and stoloc = '%s'        "
                        "     and wh_id  = '%s'        "
                        "     and oprcod = '%s'        "
                        "     and prtnum = '%s'        "
                        "     and prt_client_id = '%s' "
                        "     and invsts = '%s'        "
                        "     and nvl(lotnum,'----') = nvl('%s','----') "
                        "     and nvl(orgcod,'----') = nvl('%s','----') "
                        "     and nvl(revlvl,'----') = nvl('%s','----') "
                        "     and untcas = %ld         "
                        "     and untpak = %ld         "
                        "     %s                       "
                        "     %s                       ",
                        scanned_qty,
                        scanned_ctch_qty,
                        devcod,
                        stoloc,
                        wh_id,
                        oprcod,
                        sqlGetString(invadjres, invadjrow, "prtnum"),
                        sqlGetString(invadjres, invadjrow, "prt_client_id"),
                        sqlGetString(invadjres, invadjrow, "invsts"),
                        sqlGetString(invadjres, invadjrow, "lotnum"),
                        sqlGetString(invadjres, invadjrow, "orgcod"),
                        sqlGetString(invadjres, invadjrow, "revlvl"),
                        sqlGetLong(invadjres, invadjrow, "untcas"),
                        sqlGetLong(invadjres, invadjrow, "untpak"),
                        updateRfAdjString ? updateRfAdjString : "",
                        whereAdjRefString ? whereAdjRefString : "");

                ret_status = sqlExecStr(buffer, NULL);
 
                if (eOK != ret_status) 
                {
                    sqlFreeResults(invadjres);
                    return srvResults(ret_status, NULL);
                }
            }
            
            if (sernum_req == BOOLEAN_FALSE)
            {
                if (untqty == 0)
                {
                    /* This is a new load */
                    char *clause=NULL;
                    ret_status = sGetWhereClause(invadjres, invadjrow, &clause);
                    if (ret_status != eOK)
                    {
                        sqlFreeResults(invadjres);
                        return (srvResults(ret_status, NULL));
                    }
                    sprintf(createStartString,
                        " publish data "
                        "    where dstloc='%s' "
                        "      and lodnum='%s' "
                        "      and subnum='%s' "
                        "      and dtlnum='%s' ",
                        stoloc, sqlGetString(invadjres, invadjrow, "lodnum"), 
                        sqlGetString(invadjres, invadjrow, "subnum"),
                        sqlGetString(invadjres, invadjrow, "dtlnum"));
                    if (clause)
                    {
                        strcat(createStartString, clause);
                        free(clause);
                    }
                }
                else
                {
                    sprintf(createStartString,
                        "  [select invsub.subnum dstsub, "
                        "          invdtl.ftpcod ftpcod, "
			"          invdtl.supnum dansupnum "
                        "     from invlod, invsub, invdtl"
                        "    where invlod.lodnum = invsub.lodnum"
                        "      and invsub.subnum = invdtl.subnum"
                        "      and invlod.stoloc = '%s'"
                        "      and invlod.lodnum = nvl('%s', invlod.lodnum) "
                        "      and invlod.wh_id = '%s'"
                        "      and rownum < 2]",
                        stoloc,
                        sqlGetString(invadjres, invadjrow, "lodnum"),
                        wh_id);
                }
    
    
                /* It's not detail tracked inventory. */
    
                if (scanned_ctch_qty < catch_qty)
                    diff_catch_qty = 0.00;
    
                sprintf(buffer,
                    " %s"
                    " |"
                    " create inventory"
                    "  where dstsub = @dstsub"
                    "    and ftpcod = @ftpcod"
                    "    and srcloc = '%s'"
                    "    and movref = '%s'"
                    "    and wh_id  = '%s'"
                    "    and prtnum = '%s'"
                    "    and prt_client_id = '%s'"
                    "    and untpak = %ld"
                    "    and untcas = %ld"
                    "    and mandte = '%s'"
                    "    and expire_dte = '%s'"
                    "    and untqty = %ld"
                    "    and invsts = '%s'"
                    "    and lotnum = '%s'"
                    "    and orgcod = '%s'"
                    "    and revlvl = '%s'"
		    "    and supnum = nvl(@dansupnum,'----') "
                    "    and devcod = '%s'"
                    "    and create_missing_ids = 0"
                    "    %s",
                    createStartString,
                    STOLOC_PERM_ADJUST,
                    sesnum,
                    wh_id,
                    sqlGetString(invadjres, invadjrow, "prtnum"),
                    sqlGetString(invadjres, invadjrow, "prt_client_id"),
                    sqlGetLong(invadjres, invadjrow, "untpak"),
                    sqlGetLong(invadjres, invadjrow, "untcas"),
                    sqlIsNull(invadjres, invadjrow, "mandte")? "" : 
                        sqlGetString(invadjres, invadjrow, "mandte"),
                    sqlIsNull(invadjres, invadjrow, "expire_dte")? "" :
                        sqlGetString(invadjres, invadjrow, "expire_dte"),
                    diff_untqty,
                    sqlGetString(invadjres, invadjrow, "invsts"),
                    sqlGetString(invadjres, invadjrow, "lotnum"),
                    sqlGetString(invadjres, invadjrow, "orgcod"),
                    sqlGetString(invadjres, invadjrow, "revlvl"),
                    sqlGetString(invadjres, invadjrow, "devcod"),
                    whereAdjRefString ? whereAdjRefString : "");
                ret_status = srvInitiateCommand(buffer, NULL);
                if (ret_status != eOK)
                { 
                    sqlFreeResults(invadjres);
                    return (srvResults(ret_status, NULL));
                }

                /*
                 * Check whether asset has been identified for this
                 * inventory. If yes we need associate the asset with
                 * the inventory.
                 */
                ret_status = sCreateRfAssetForInventory(
                                sqlGetString(invadjres, invadjrow, "devcod"),
                                sqlGetString(invadjres, invadjrow, "wh_id"),
                                sqlGetString(invadjres, invadjrow, "lodnum"),
                                assetSub);
                if (ret_status != eOK)
                {
                    sqlFreeResults(invadjres);
                    return (srvResults(ret_status, NULL));
                }

                /* Now we'll update the row in the rf_invadj table. */
    
                sprintf(buffer,
                    "  update rf_invadj            "
                    "     set untqty = %ld,        "
                    "         catch_qty = %lf      "
                    "   where devcod = '%s'        "
                    "     and cntbat = '%s'        "
                    "     and stoloc = '%s'        "
                    "     and wh_id  = '%s'        "
                    "     and oprcod = '%s'        "
                    "     and prtnum = '%s'        "
                    "     and prt_client_id = '%s' "
                    "     and invsts = '%s'        "
                    "     and nvl(lotnum,'----') = nvl('%s','----') "
                    "     and nvl(orgcod,'----') = nvl('%s','----') "
                    "     and nvl(revlvl,'----') = nvl('%s','----') "
                    "     and untcas = %ld         "
                    "     and untpak = %ld         "
                    "     %s                       "
                    "     %s                       ",
                    scanned_qty,
                    scanned_ctch_qty,
                    devcod,
                    cntbat,
                    stoloc,
                    wh_id,
                    oprcod,
                    sqlGetString(invadjres, invadjrow, "prtnum"),
                    sqlGetString(invadjres, invadjrow, "prt_client_id"),
                    sqlGetString(invadjres, invadjrow, "invsts"),
                    sqlGetString(invadjres, invadjrow, "lotnum"),
                    sqlGetString(invadjres, invadjrow, "orgcod"),
                    sqlGetString(invadjres, invadjrow, "revlvl"),
                    sqlGetLong(invadjres, invadjrow, "untcas"),
                    sqlGetLong(invadjres, invadjrow, "untpak"),
                    updateRfAdjString ? updateRfAdjString : "",
                    whereAdjRefString ? whereAdjRefString : "");
                ret_status = sqlExecStr(buffer, NULL);
     
                if (eOK != ret_status) 
                {
                    sqlFreeResults(invadjres);
                    return srvResults(ret_status, NULL);
                }
            }
        }
        else if (scanned_qty < untqty)
        {
            /* They're telling us that the quantity has decreased,
             * so we're going to remove inventory to match.
             */
            misTrc(T_FLOW, "Scanned quantity is less than untqty, we'll "
                           "remove inventory here. ");
            
            /* WMD-20065: Try to retrieve serial number adjustments that 
             * may have been made (add/delete) when a user modified inventory.
             *
             * If the user did adjust SNC inventory, we need remove that 
             * specific inventory identifiers. When SNC inventory is removed, 
             * the user scans the remaining serial numbers leaving the 
             * inventory that is missing w/o a serial number value.  
             * These are the records we need to adjust out.
             *
             * We also need update INV_SER_NUM after the user scanned the
             * remaining serial numbers based on what the user scannded.
             */

            sprintf(buffer,
                    " select * from rf_seradj "
                    "  where seradj_id = '%s' "
                    "    and devcod = '%s' "
                    "    and stoloc = '%s' "
                    "    and wh_id = '%s' "
                    "    and lodnum = nvl('%s',lodnum) "
                    "    and subnum = nvl('%s',subnum) "
                    "    and dtlnum = nvl('%s',dtlnum) ",
                    seradj_id,
                    devcod,
                    stoloc,
                    wh_id,
                    sqlGetString(invadjres, invadjrow, "lodnum"),
                    sqlGetString(invadjres, invadjrow, "subnum"),
                    sqlGetString(invadjres, invadjrow, "dtlnum"));

            ret_status = sqlExecStr(buffer, &seradjres);
            
            if (ret_status != eOK)
            {
                /* The part is non-serialied */
                if (ret_status == eDB_NO_ROWS_AFFECTED)
                {
                    if (scanned_qty > 0)
                    {
                        diff_untqty = (untqty - scanned_qty);
                        memset (tmp_buffer, 0, sizeof(tmp_buffer));
                        sprintf (tmp_buffer, " and untqty = %ld "
                                             " and scanned_qty = %ld "
                                             " and scanned_ctch_qty = %lf", 
                                             diff_untqty, 
                                             scanned_qty, 
                                             scanned_ctch_qty);
                        strcat(deleteInvString, tmp_buffer);
                    }
        
                    if (!sqlIsNull(invadjres, invadjrow, "dtlnum"))
                    {
                        sprintf (tmp_buffer, " and lodlvl = 'D' and dtlnum = '%s' ",
                                 sqlGetString(invadjres, invadjrow, "dtlnum") );
                        strcat(deleteInvString, tmp_buffer);
                    }
                    else if (!sqlIsNull(invadjres, invadjrow, "subnum"))
                    {
                       sprintf (tmp_buffer, " and lodlvl = 'S' and subnum = '%s' ",
                                sqlGetString(invadjres, invadjrow, "subnum") );
                       strcat(deleteInvString, tmp_buffer);
                    }
                    else if (!sqlIsNull(invadjres, invadjrow, "lodnum"))
                    {
                        sprintf (tmp_buffer, " and lodlvl = 'L' and lodnum = '%s' ",
                                 sqlGetString(invadjres, invadjrow, "lodnum") );
                        strcat(deleteInvString, tmp_buffer);
                    }
                    if (scanned_qty == 0)
                    {
                        sprintf (tmp_buffer, " and untqty = %ld ", untqty);
                        strcat(deleteInvString, tmp_buffer);
                    }
        
                    sprintf(buffer,
                            "   process rf inventory adjust delete inventory " 
                            "        where dstloc = '%s'                     "
                            "          and sesnum = '%s'                     "
                            "          %s                                    "
                            "          and devcod = '%s'                     "
                            "          and cntbat = '%s'                     "
                            "          and stoloc = '%s'                     "
                            "          and wh_id  = '%s'                     "
                            "          and oprcod = '%s'                     "
                            "          and prtnum = '%s'                     "
                            "          and prt_client_id = '%s'              "
                            "          and invsts = '%s'                     "
                            "          and lotnum = '%s'                     "
                            "          and revlvl = '%s'                     "
                            "          and orgcod = '%s'                     "
                            "          and untcas = %ld                      "
                            "          and untpak = %ld                      "
                            "          %s                                    ",
                            dstloc,
                            sesnum,
                            deleteInvString,
                            devcod,
                            cntbat,
                            stoloc,
                            wh_id,
                            oprcod,        
                            sqlGetString(invadjres, invadjrow, "prtnum"),
                            sqlGetString(invadjres, invadjrow, "prt_client_id"),
                            sqlGetString(invadjres, invadjrow, "invsts"),
                            sqlGetString(invadjres, invadjrow, "lotnum"),
                            sqlGetString(invadjres, invadjrow, "revlvl"),
                            sqlGetString(invadjres, invadjrow, "orgcod"),
                            sqlGetLong(invadjres, invadjrow, "untcas"),
                            sqlGetLong(invadjres, invadjrow, "untpak"),
                            whereAdjRefString ? whereAdjRefString : "");        
        
                     ret_status = srvInitiateCommand(buffer, NULL);
        
                     if (ret_status != eOK)
                     {                         
                         sqlFreeResults(seradjres);
                         sqlFreeResults(invadjres);
                         return (srvResults(ret_status, NULL));
                     }                    
                }
                else
                {
                    sqlFreeResults(seradjres);
                    sqlFreeResults(invadjres);
                    return srvResults(ret_status, NULL);
                }
            }
            /* WMD-20065: Procesing serialized parts */
            else
            {
                for (seradjrow=sqlGetRow(seradjres); seradjrow;
                               seradjrow=sqlGetNextRow(seradjrow))
                {  
                    if(!misTrimStrncmp("",
                        sqlGetString(seradjres, seradjrow, "ser_num"),
                        SER_NUM_LEN ))
                    {
                        /* The user didn't capture this serial number. SO,
                         * the serial number in rf_seradj is null, we delete 
                         * the inventory.
                         */
                        memset (tmp_buffer, 0, sizeof(tmp_buffer));
                        memset (deleteInvString, 0, sizeof(deleteInvString));
                            
                        if (!misTrimStrncmp(ser_lvl, SER_LVL_DETAIL, SER_LVL_LEN))
                        {
                            sprintf (tmp_buffer, " and lodlvl = 'D' "
                                                    " and dtlnum = '%s' "
                                                    " and untqty = %ld ",
                                            sqlGetString(seradjres, seradjrow, "dtlnum"),
                                            sqlGetLong(invadjres, invadjrow, "untpak"));
                            strcat(deleteInvString, tmp_buffer);
                        }
                        else if (!misTrimStrncmp(ser_lvl, SER_LVL_SUBLOAD, 
                                                                SER_LVL_LEN))
                        {
                            sprintf (tmp_buffer, " and lodlvl = 'S' "
                                                    " and subnum = '%s' "
                                                    " and untqty = %ld ",
                                            sqlGetString(seradjres, seradjrow, "subnum"),
                                            sqlGetLong(invadjres, invadjrow, "untcas"));
                            strcat(deleteInvString, tmp_buffer);
                        }
                        else if (!misTrimStrncmp(ser_lvl, SER_LVL_LOAD, 
                                                                SER_LVL_LEN))
                        {
                            sprintf (tmp_buffer, " and untqty = %ld ", untqty);
                            strcat(deleteInvString, tmp_buffer);
                        }

                        sprintf(buffer,
                                "   process rf inventory adjust delete inventory "
                                "        where dstloc = '%s'                     "
                                "          and sesnum = '%s'                     "
                                "          %s                                    "
                                "          and devcod = '%s'                     "
                                "          and stoloc = '%s'                     "
                                "          and wh_id = '%s'                     "
                                "          and oprcod = '%s'                     "
                                "          and prtnum = '%s'                     "
                                "          and prt_client_id = '%s'              "
                                "          and invsts = '%s'                     "
                                "          and lotnum = '%s'                     "
                                "          and revlvl = '%s'                     "
                                "          and orgcod = '%s'                     "
                                "          and untcas = %ld                      "
                                "          and untpak = %ld                      "
                                "          %s                                    ",
                                dstloc,
                                sesnum,
                                deleteInvString,
                                devcod,
                                stoloc,
                                wh_id,
                                oprcod,
                                sqlGetString(invadjres, invadjrow, "prtnum"),
                                sqlGetString(invadjres, invadjrow, "prt_client_id"),
                                sqlGetString(invadjres, invadjrow, "invsts"),
                                sqlGetString(invadjres, invadjrow, "lotnum"),
                                sqlGetString(invadjres, invadjrow, "revlvl"),
                                sqlGetString(invadjres, invadjrow, "orgcod"),
                                sqlGetLong(invadjres, invadjrow, "untcas"),
                                sqlGetLong(invadjres, invadjrow, "untpak"),
                                whereAdjRefString ? whereAdjRefString : "");
                           
                        ret_status = srvInitiateCommand(buffer, NULL);

                        if (ret_status != eOK)
                        {
                                sqlFreeResults(seradjres);
                                sqlFreeResults(invadjres);
                                return (srvResults(ret_status, NULL));
                        }
                    }
                    else
                    {
                        /* WMD-20065: The user re-captures the serial number. 
                         * We need update the serial number in inv_ser_num 
                         * table to the new captured serial number in 
                         * rf_seradj.
                         */
                        memset (tmp_buffer, 0, sizeof(tmp_buffer));
                        memset (buffer, 0, sizeof(buffer));

                        if (!misTrimStrncmp(ser_lvl, SER_LVL_DETAIL, SER_LVL_LEN))
                        {
                            sprintf (tmp_buffer, " where ser_lvl = 'D' "
                                                 "   and invtid = '%s' ",
                                        sqlGetString(seradjres, seradjrow, "dtlnum"));
                        }
                        else if (!misTrimStrncmp(ser_lvl, SER_LVL_SUBLOAD, 
                                                                SER_LVL_LEN))
                        {
                            sprintf (tmp_buffer, " where ser_lvl = 'S' "
                                                 "   and invtid = '%s' ",
                                        sqlGetString(seradjres, seradjrow, "subnum"));
                        }
                        else if (!misTrimStrncmp(ser_lvl, SER_LVL_LOAD, 
                                                                SER_LVL_LEN))
                        {
                            sprintf (tmp_buffer, " where ser_lvl = 'L' "
                                                 "   and invtid = '%s' ",
                                        sqlGetString(seradjres, seradjrow, "lodnum"));
                        }
                    
                        sprintf (buffer, 
                                 " update inv_ser_num "
                                 "    set ser_num = '%s' "
                                 "  %s ",
                                 sqlGetString(seradjres, seradjrow, "ser_num"),
                                 tmp_buffer);
                        ret_status = sqlExecStr(buffer, NULL);
         
                        if (eOK != ret_status) 
                        {
                            sqlFreeResults(seradjres);
                            return srvResults(ret_status, NULL);
                        }
                    }
                } /* End For Loop */
                
                sqlFreeResults(seradjres);

                /* WMD-20065: We are updating the row in the rf_invadj table. 
                 * The process rf inventory adjusment delete inventory 
                 * does this, however as we were cycling through SNC entries
                 * we choose not to pass scanned_qty to the command and have 
                 * it update over and over so we do the update here.
                 */

                sprintf(buffer,
                        "  update rf_invadj            "
                        "     set untqty = %ld,        "
                        "         catch_qty = %lf      "
                        "   where devcod = '%s'        "
                        "     and stoloc = '%s'        "
                        "     and wh_id = '%s'        "
                        "     and oprcod = '%s'        "
                        "     and prtnum = '%s'        "
                        "     and prt_client_id = '%s' "
                        "     and invsts = '%s'        "
                        "     and nvl(lotnum,'----') = nvl('%s','----') "
                        "     and nvl(orgcod,'----') = nvl('%s','----') "
                        "     and nvl(revlvl,'----') = nvl('%s','----') "
                        "     and untcas = %ld         "
                        "     and untpak = %ld         "
                        "     %s                       "
                        "     %s                       ",
                        scanned_qty,
                        scanned_ctch_qty,
                        devcod,
                        stoloc,
                        wh_id,
                        oprcod,
                        sqlGetString(invadjres, invadjrow, "prtnum"),
                        sqlGetString(invadjres, invadjrow, "prt_client_id"),
                        sqlGetString(invadjres, invadjrow, "invsts"),
                        sqlGetString(invadjres, invadjrow, "lotnum"),
                        sqlGetString(invadjres, invadjrow, "orgcod"),
                        sqlGetString(invadjres, invadjrow, "revlvl"),
                        sqlGetLong(invadjres, invadjrow, "untcas"),
                        sqlGetLong(invadjres, invadjrow, "untpak"),
                        updateRfAdjString ? updateRfAdjString : "",
                        whereAdjRefString ? whereAdjRefString : "");

                ret_status = sqlExecStr(buffer, NULL);
 
                /*
                 * add catch(-1403) due to process rf inventory adjust
                 * delete inventory command
                 */
                if (eOK != ret_status && 
                	eDB_NO_ROWS_AFFECTED != ret_status) 
                {
                    sqlFreeResults(invadjres);
                    return srvResults(ret_status, NULL);
                }
            }
        }
    }
 
    /* Cleaning up. */

    sqlFreeResults(invadjres);

    /* All done... */
    return (srvResults(eOK, NULL));
}

