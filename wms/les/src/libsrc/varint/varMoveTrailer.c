static const char *rcsid = "$Id: varMoveTrailer.c 403528 2013-05-16 21:07:59Z gbane $";
/*#START***********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *
 *  $URL: https://athena.redprairie.com/svn/proj/sceservices/hopewell-bra/tags/REL-185-HOPWLL-199-010/wm/les/src/libsrc/varint/varMoveTrailer.c $
 *  $Revision: 403528 $
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "intlib.h"

LIBEXPORT 
RETURN_STRUCT *varMoveTrailer(char *trlr_id_i,
                              char *yard_loc_i,
                              moca_bool_t *turn_flg_i,
                              char *devcod_i,
                              char *yard_loc_wh_id_i)
{
    RETURN_STRUCT  *CurPtr;

    char trlr_id           [TRLR_ID_LEN+1];
    char new_yard_loc      [YARD_LOC_LEN+1];
    char current_yard_loc  [YARD_LOC_LEN+1];
    char trlr_cod          [TRLR_COD_LEN+1];
    char trlr_stat         [TRLR_STAT_LEN+1];
    char rcv_trknum        [TRKNUM_LEN+1];
    char wrkzon            [WRKZON_LEN+1];
    char devcod            [DEVCOD_LEN+1];
    char yard_loc_wh_id    [WH_ID_LEN + 1];
    char stoloc_wh_id      [WH_ID_LEN + 1];

    long ret_status;
    char buffer[2500];

    moca_bool_t turn_flg = BOOLEAN_FALSE;
    moca_bool_t dock_loc = BOOLEAN_FALSE;
    moca_bool_t update_devcod = BOOLEAN_FALSE;

    mocaDataRes *res;
    mocaDataRow *row;
    
    res = NULL;
    row = NULL;

    memset(trlr_id, 0, sizeof(trlr_id));
    memset(new_yard_loc, 0, sizeof(new_yard_loc));
    memset(current_yard_loc, 0, sizeof(current_yard_loc));
    memset(trlr_cod, 0, sizeof(trlr_cod));
    memset(trlr_stat, 0, sizeof(trlr_stat));
    memset(rcv_trknum, 0, sizeof(rcv_trknum));
    memset(wrkzon, 0, sizeof(wrkzon));
    memset(devcod, 0, sizeof(devcod));
    memset(yard_loc_wh_id, 0, sizeof(yard_loc_wh_id));
    memset(stoloc_wh_id, 0, sizeof(stoloc_wh_id));

    if (!trlr_id_i || misTrimLen(trlr_id_i, TRLR_ID_LEN) == 0)
        return (APPMissingArg("trlr_id"));
    
    misTrimcpy(trlr_id, trlr_id_i, TRLR_ID_LEN);

    if (!yard_loc_i || misTrimLen(yard_loc_i, YARD_LOC_LEN) == 0)
        return (APPMissingArg("yard_loc"));

    misTrimcpy(new_yard_loc, yard_loc_i, YARD_LOC_LEN);

    if (!yard_loc_wh_id_i || misTrimLen (yard_loc_wh_id_i, WH_ID_LEN) == 0)
        return (APPMissingArg ("yard_loc_wh_id"));
    else
        misTrimcpy (yard_loc_wh_id, yard_loc_wh_id_i, WH_ID_LEN);

    if (turn_flg_i)
    {
        turn_flg = *turn_flg_i;
    }

    if (devcod_i && misTrimLen (devcod_i, DEVCOD_LEN))
        misTrimcpy (devcod, devcod_i, DEVCOD_LEN);
    else
        misTrimcpy(devcod,
                   osGetVar(LESENV_DEVCOD) ?
                      osGetVar(LESENV_DEVCOD) : "",
                   DEVCOD_LEN);

    /* First, find the current location and status of this trailer. */

    misTrc(T_FLOW,
           "Getting trlr information ... ");

    sprintf(buffer,
	    "select * "
            "  from trlr " 
	    " where trlr_id = '%s' ",
	    trlr_id);

    ret_status = sqlExecStr (buffer, &res);

    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }

    row = sqlGetRow(res);

    /*
     * Currently we do not support moving trailers between warehouses.
     * Instead, the traier must be dispatched from 1 warehouse and checked
     * into the other.
     */

    if(!sqlIsNull(res, row, "yard_loc_wh_id")
        && (strcmp(sqlGetString (res, row, "yard_loc_wh_id"), yard_loc_wh_id))
            != 0)
    {
        return(srvResults(eMOVE_TRLR_BETWEEN_WHS, NULL));
    }

    if (!sqlIsNull(res, row, "yard_loc"))
    {
        misTrimcpy(current_yard_loc,
                   sqlGetString(res, row, "yard_loc"),
                   YARD_LOC_LEN);
    }

    if (!sqlIsNull(res, row, "trlr_stat"))
    {
        misTrimcpy(trlr_stat,
                   sqlGetString(res, row, "trlr_stat"),
                   TRLR_STAT_LEN);
    }
    else
    {
        /* If it's null, we'll assume it's Expected and not arrived yet. */
           misTrimcpy(trlr_stat,
                      TRLSTS_EXPECTED,
                      TRLR_STAT_LEN);
    }

    misTrimcpy(trlr_cod, sqlGetString(res, row, "trlr_cod"),
               TRLR_COD_LEN);
               
    /*
     * Get "stoloc_wh_id" from the "trlr" table and add it to the return 
     * structure for use by triggers that require a wh_id argument.
     */
    if(!sqlIsNull(res, row, "stoloc_wh_id"))
    {
        misTrimcpy(stoloc_wh_id, sqlGetString(res, row, "stoloc_wh_id"),
                   WH_ID_LEN);
    }
    else
    {
        return(srvResults(eMISS_ARG_WH_ID, NULL));
    }

    sqlFreeResults(res);

    /* If they're going to the same location, just */
    /* exit nicely. We may be calling this just to fire */
    /* the reset trailer status trigger.                */

    if (misCiStrncmp(new_yard_loc, current_yard_loc, YARD_LOC_LEN) == 0)
    {
        misTrc(T_FLOW,
               "Current location, '%s', is same as new location, '%s' "
               "exiting... ", current_yard_loc, new_yard_loc);

        return (srvResults(eOK, NULL));
    }

    /* Now, verify that the new location is usable and not in error. */

    misTrc(T_FLOW,
           "Verifying there are no move conflicts... ");

    /* DHK Hopewell PRXXXX Start - Add extra parameter for actual trailer moves. */
    sprintf(buffer,
            "check trailer move ok "
            "where trlr_id = '%s' "
            "  and yard_loc = '%s' "
            "  and yard_loc_wh_id = '%s' "
            "  and trlr_move = '1' ",
            trlr_id,
            new_yard_loc,
            yard_loc_wh_id);
	
	/* DHK Hopewell PRXXXX Stop - Add extra parameter for actual trailer moves. */

    ret_status = srvInitiateCommand (buffer, NULL);

    if (ret_status != eOK)
    {
        return (srvResults(ret_status, NULL));
    }

    /* If we have a receiving trailer that we're turning around for        */
    /* shipping, we'll need to handle that differently.                    */

    if ((misCiStrncmp(trlr_cod, TRLR_COD_RCV, TRLR_COD_LEN) == 0) &&
        (turn_flg == BOOLEAN_FALSE))
    {
        misTrc(T_FLOW,
               "Working with a receiving trailer ... ");

        misTrc(T_FLOW,
               "Determining if it's a valid yard or dock location... ");

        sprintf(buffer,
                " list receiving dock doors  "
                "   where locsts = '%s'      "
                "     and stoloc = '%s'      "
                "     and wh_id = '%s'       ",
                LOCSTS_EMPTY,
                new_yard_loc,
                yard_loc_wh_id);

        ret_status = srvInitiateCommand(buffer, NULL);
        if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
        {
            return (srvResults(ret_status, NULL));
        }
        else if (ret_status == eOK)
        {
            dock_loc = BOOLEAN_TRUE;
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            /* Not a dock door, let's verify it's a valid yard location.*/

            sprintf(buffer,
                    " list yard locations        "
                    "   where stoloc = '%s'      "
                    "     and wh_id = '%s'       ",
                    new_yard_loc,
                    yard_loc_wh_id);

            ret_status = srvInitiateCommand(buffer, NULL);
            if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
            {
                return (srvResults(ret_status, NULL));
            }
            else if (ret_status == eDB_NO_ROWS_AFFECTED)
            {
                /* Not a valid yard or dock location. */
                misTrc(T_FLOW,
                       "This location is not a valid dock or yard"
                       " location for this trailer.");

                return (srvResults(eINT_INVALID_TRAILER_LOCATION, NULL));
            }
        }

        /* Let's get some basic receive truck information. */

        misTrc(T_FLOW,
               "Getting receive truck information ... ");

        sprintf(buffer,
                "select rcvtrk.trknum trknum, "
                "       rcvtrk.devcod         "
                "  from rcvtrk, trlr "
                " where rcvtrk.trlr_id = trlr.trlr_id "
                "   and trlr.trlr_id = '%s' "
                "   and rcvtrk.wh_id = '%s' ",
                trlr_id,
                yard_loc_wh_id);

        ret_status = sqlExecStr (buffer, &res);

        if (ret_status != eOK)
        {
            sqlFreeResults(res);
            return (srvResults(ret_status, NULL));
        }

        row = sqlGetRow(res);

        if (!sqlIsNull(res, row, "trknum"))
        {
            misTrimcpy(rcv_trknum,
                       sqlGetString(res, row, "trknum"),
                       TRKNUM_LEN);
        }

        if (sqlIsNull(res, row, "devcod")) 
        {
            /* We will not update devcod by current device if it 
             * is not a RF device. See PR39768
             */
            sprintf(buffer,
                    "select 'x' "
                    "   from devmst "
                    "  where devcod = '%s' "
                    "    and devcls = 'R' "
                    "    and wh_id = '%s' ",
                    devcod,
                    yard_loc_wh_id);

            ret_status = sqlExecStr (buffer, NULL);

            if (ret_status == eOK)
            {
                update_devcod = BOOLEAN_TRUE;
            }   
        } 

        sqlFreeResults(res);

        /* We're going to do an update here, because change trailer */
        /* no longer accepts the yard_loc as a parameter. */

        sprintf(buffer,
                "update trlr "
                "  set yard_loc = '%s', "
                "      yard_loc_wh_id = '%s' "
                " where trlr_id = '%s' ",
                new_yard_loc,
                yard_loc_wh_id,
                trlr_id);

        ret_status = sqlExecStr (buffer, NULL);

        if (ret_status != eOK)
        {
            return (srvResults(ret_status, NULL));
        }

        if (dock_loc == BOOLEAN_TRUE)
        {

            /* Update the work zone on the expected receipts location to */
            /* match that of the dock door so we will be directing our   */
            /* receiving workers to the right locations.                 */

            misTrc(T_FLOW,
                   "Retreiving wrkzon of new yard location (%s)",
                   new_yard_loc);

            sprintf (buffer, 
                     "select wrkzon "
                     "  from locmst "
                     " where stoloc = '%s' "
                     "   and wh_id = '%s' ",
                     new_yard_loc,
                     yard_loc_wh_id);

            ret_status = sqlExecStr (buffer, &res);
            if (ret_status != eOK)
            {
                sqlFreeResults (res);
                return (srvResults (ret_status, NULL));
            }
           
            row = sqlGetRow(res);

            misTrimcpy (wrkzon, sqlGetString (res, row, "wrkzon"), WRKZON_LEN);

            sqlFreeResults (res);

            if (misTrimLen (wrkzon, WRKZON_LEN))
            {
                misTrc(T_FLOW,
                       "Updating wrkzon on expected receipts loc to match"
                       " the truck. ");
             
                sprintf(buffer,
                        " update locmst              "
                        "    set wrkzon = '%s'       "
                        "  where stoloc = '%s'       "
                        "    and wh_id = '%s'        ",
                        wrkzon,
                        rcv_trknum,
                        yard_loc_wh_id);

                ret_status = sqlExecStr (buffer, NULL);

                if (ret_status != eOK)
                {
                    return (srvResults(ret_status, NULL));
                }
            }

            /* Now, if we're moving to a dock location and there is no */
            /* device assigned to the truck, assign it. */
            /* Use the device that's passed in, otherwise get the env devcod. */

            if (update_devcod == BOOLEAN_TRUE)
            {
                sprintf(buffer,
                        " change receive truck  "
                        "   where trknum = '%s' "
                        "     and devcod = '%s' "
                        "     and wh_id = '%s' ",
                        rcv_trknum,
                        devcod,
                        yard_loc_wh_id);

                ret_status = srvInitiateCommand(buffer, NULL);
                if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
                {
                    return (srvResults(ret_status, NULL));
                }
            }
        }
    }
    else
    {
        misTrc(T_FLOW,
               "Working with a shipping trailer or a receiving trailer "
               "that we've turned around for shipping... ");

        misTrc(T_FLOW,
               "Determining if it's a valid yard or dock location... ");

        sprintf(buffer,
                " list available shipment dock doors  "
                "   where stoloc = '%s'      "
                "     and wh_id = '%s'       ",
                new_yard_loc,
                yard_loc_wh_id);

        ret_status = srvInitiateCommand(buffer, NULL);
        if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
        {
            return (srvResults(ret_status, NULL));
        }
        else if (ret_status == eOK)
        {
            dock_loc = BOOLEAN_TRUE;
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            /* Not a dock door, let's verify it's a valid yard location.*/

            sprintf(buffer,
                    " list yard locations        "
                    "   where stoloc = '%s'      "
                    "     and wh_id = '%s'       ",
                    new_yard_loc,
                    yard_loc_wh_id);

            ret_status = srvInitiateCommand(buffer, NULL);
            if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
            {
                return (srvResults(ret_status, NULL));
            }
            else if (ret_status == eDB_NO_ROWS_AFFECTED)
            {
                /* Not a valid yard or dock location. */
                misTrc(T_FLOW,
                       "This location is not a valid dock or yard"
                       " location for this trailer.");

                return (srvResults(eINT_INVALID_TRAILER_LOCATION, NULL));
            }
        }

        /* We're going to do an update here, because change trailer */
        /* no longer accepts the yard_loc as an parameter. */

        sprintf(buffer,
                "update trlr "
                "  set yard_loc = '%s', "
                "      yard_loc_wh_id = '%s' "
                " where trlr_id = '%s' ",
                new_yard_loc,
                yard_loc_wh_id,
                trlr_id);

        ret_status = sqlExecStr (buffer, NULL);

        if (ret_status != eOK)
        {
            return (srvResults(ret_status, NULL));
        }

    }

    /* Setup the return structure */
    
    /* Both the stoloc_wh_id and yard_loc_wh_id are added to 
     * the return structure because some triggers require a 
     * wh_id while others need a yard_loc_wh_id.
     */

    CurPtr = srvResultsInit(eOK,
                            "trlr_id",           COMTYP_CHAR, TRLR_ID_LEN,
                            "oldloc",            COMTYP_CHAR, STOLOC_LEN,
                            "yard_loc",          COMTYP_CHAR, STOLOC_LEN, 
                            "trlr_stat",         COMTYP_CHAR, TRLR_STAT_LEN,
                            "trlr_cod",          COMTYP_CHAR, TRLR_COD_LEN,
                            "rcv_trknum",        COMTYP_CHAR, TRKNUM_LEN,
                            "yard_loc_wh_id",    COMTYP_CHAR, WH_ID_LEN,
                            "wh_id",             COMTYP_CHAR, WH_ID_LEN,
                            NULL);

    srvResultsAdd(CurPtr,
                  trlr_id,
                  strlen(current_yard_loc)  ? current_yard_loc : "",
                  strlen(new_yard_loc)      ? new_yard_loc     : "",
                  strlen(trlr_stat)         ? trlr_stat        : "",
                  strlen(trlr_cod)          ? trlr_cod         : "",
                  strlen(rcv_trknum)        ? rcv_trknum       : "",
                  strlen(yard_loc_wh_id)    ? yard_loc_wh_id   : "",
                  stoloc_wh_id);


    /* All done... */
    return (CurPtr);
}

