static char *rcsid = "$Id: pckCompleteEmergencyReplenishment.c 162890 2008-07-10 20:21:50Z pflanzer $";
/*#START***********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *
 *      Purpose:  This routine is called to complete any outstanding 
 *                replenishments which now have product in the appropriate
 *                areas for allocation.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <applib.h>
#include <dcserr.h>
#include <dcsgendef.h>
#include <dcscolwid.h>

#include <pckRPLlib.h>

/*
 * HISTORY
 * JJS 06/10/2009 - Creation.  Pass vc_from_rpl = 1 to "allocate inventory".
 *                  This gets written to the pckwrk.vc_from_rpl column if
 *                  and when a pick gets created (which is then used down
 *                  the line for label print routing).
 *
 */

static long FailReplenishment(char *rplref, 
                              char *reason)
{
    long    ret_status;
    char    buffer[1000];

    pckrpl_WriteTrc("Failing replenishment: %s", rplref);

    sprintf(buffer,
        "update rplwrk "
        "    set rplsts = '%s', "
        "        rplcnt = rplcnt + 1, "
        "        alcdte = sysdate, "
        "        rplmsg = '%s' "
        "  where rplref = '%s'",
        RPLSTS_HOPFAIL, reason, rplref);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK)
        pckrpl_WriteTrc("Unable to fail replenishment: %s, (%d)",
            rplref, ret_status);

    return (ret_status);
}

static long AllocForDemand(char *rplref,
                           moca_bool_t bIsWorkOrder)
{
    char pick_type[20];
    char srv_command[2000];
    char buffer[2048];
    char seg_qty[50];
    char srcare[ARECOD_LEN + 1];
    char prtnum[PRTNUM_LEN + 1];
    char prt_client_id[CLIENT_ID_LEN + 1];
    char orgcod[ORGCOD_LEN + 1];
    char revlvl[REVLVL_LEN + 1];
    char lotnum[LOTNUM_LEN + 1];
    char dstare[ARECOD_LEN + 1];
    char lodlvl[LODLVL_LEN + 1];
    char invsts[INVSTS_LEN + 1];
    char invsts_prg[INVSTS_PRG_LEN + 1];
    char ship_line_id[SHIP_LINE_ID_LEN + 1];
    char wkonum[WKONUM_LEN+1];
    char wkorev[WKOREV_LEN+1];
    char wkolin[WKOLIN_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    char concod[CONCOD_LEN + 1];
    char stcust[ADRNUM_LEN + 1];
    char rtcust[ADRNUM_LEN + 1];
    char schbat[SCHBAT_LEN + 1];
    char pcksts[PCKSTS_LEN + 1];
    char dstloc[DSTLOC_LEN + 1];
    char ordnum[ORDNUM_LEN+1];
    char ordlin[ORDLIN_LEN+1];
    char ordsln[ORDSLN_LEN+1];
    char ship_id[SHIP_ID_LEN+1];
    char wh_id[WH_ID_LEN+1];
    char tmp_rplref[RPLREF_LEN + 1];
    char where_clause[400];
    char tmp_buffer[60];
    char supnum[SUPNUM_LEN + 1];

    char *savepoint_p = "rpl_before_alloc_inv";
    
    moca_bool_t     splflg;
    moca_bool_t     frsflg;

    long            pckqty;
    double          pck_catch_qty;
    long            alloc_qty;
    double          alc_catch_qty;
    long            ret_status;
    long            untcas;
    long            untpak;
    long            min_shelf_hrs;
    RETURN_STRUCT  *CurPtr;
    RETURN_STRUCT  *CurPtr1 = NULL;
    mocaDataRes    *res;
    mocaDataRow    *row;

    memset(pick_type, 0, sizeof(pick_type));
    
    strcpy(pick_type, ALLOCATE_PICKNSHIP);
    
    if (bIsWorkOrder)
    {
        sprintf(buffer,
        "select rw.pckqty, rw.pck_catch_qty, rw.prtnum, rw.prt_client_id, "
        "       rw.orgcod, rw.revlvl, rw.supnum, rw.lotnum,"
        "       rw.invsts, rw.invsts_prg, rw.lodlvl, rw.dstare, "
        "       rw.wkonum, rw.wkorev, rw.client_id, "
        "       rw.wkolin, "
        "       rw.stcust, rw.concod, rw.schbat, rw.pcksts,"
                "       rw.rtcust, "
        "       rw.untcas, rw.untpak, rw.splflg, rw.dstloc, rw.wh_id "
        "  from rplwrk rw "
        " where rw.rplref = '%s' "
        " for update ",
        rplref);
        ret_status = sqlExecStr(buffer, &res);
    }
    else
    {
        sprintf(buffer,
        " exec sql with lock "
        "where main_table = \"rplwrk rw\"" 
        "  and main_pk    = \"rw.rplref\" "
            "  and sql_select = \"select rw.pckqty, rw.pck_catch_qty, "
        "                            rw.prtnum, rw.prt_client_id, "
        "                            rw.orgcod, rw.revlvl, rw.supnum, rw.lotnum,"
        "                            rw.invsts, rw.invsts_prg, "
        "                            rw.lodlvl, rw.dstare, "
        "                            rw.ship_line_id,"
        "                            sl.ordnum, sl.ordlin, sl.ordsln, "
        "                            sl.ship_id, rw.stcust, rw.concod, "
        "                            rw.schbat, rw.pcksts,  rw.rtcust, "
        "                            rw.untcas, rw.untpak, rw.splflg, "
        "                            rw.dstloc, rw.wh_id, rw.frsflg, "
        "                            rw.min_shelf_hrs, sl.client_id \" "
           "and sql_from_where = \" from shipment_line sl, rplwrk rw "
        "                      where rw.rplref = '%s' "
        "                        and rw.ship_line_id = sl.ship_line_id \"",
        rplref);
        ret_status = srvInitiateInline(buffer, &CurPtr1);
        res = srvGetResults(CurPtr1);
    }
    
    if (ret_status != eOK)
    {
        pckrpl_WriteTrc("Unable to select rplwrk for update: %d", ret_status);
        if (CurPtr1) 
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr1);
            CurPtr1 = NULL;
        }
        else
        {
            sqlFreeResults(res);            
        }        
        return (eRPL_NORMAL_FAILURE);
    }

    memset(orgcod, 0, sizeof(orgcod));
    memset(lotnum, 0, sizeof(lotnum));
    memset(revlvl, 0, sizeof(revlvl));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(dstare, 0, sizeof(dstare));
    memset(lodlvl, 0, sizeof(lodlvl));
    memset(invsts, 0, sizeof(invsts));
    memset(invsts_prg, 0, sizeof(invsts_prg));
    memset(srcare, 0, sizeof(srcare));
    memset(ship_line_id, 0, sizeof(ship_line_id));
    memset(concod, 0, sizeof(concod));
    memset(stcust, 0, sizeof(stcust));
    memset(rtcust, 0, sizeof(rtcust));
    memset(schbat, 0, sizeof(schbat));
    memset(pcksts, 0, sizeof(pcksts));
    memset(dstloc, 0, sizeof(dstloc));
    memset(ordnum, 0, sizeof(ordnum));
    memset(ordlin, 0, sizeof(ordlin));
    memset(ordsln, 0, sizeof(ordsln));
    memset(client_id, 0, sizeof(client_id));
    memset(ship_id, 0, sizeof(ship_id));
    memset(wkonum, 0, sizeof(wkonum));
    memset(wkorev, 0, sizeof(wkorev));
    memset(wkolin, 0, sizeof(wkolin));
    memset(wh_id,  0, sizeof(wh_id));
    memset(supnum, 0, sizeof(supnum));

    row = sqlGetRow(res);

    strncpy(prtnum, sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
    strncpy(prt_client_id, 
        sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
    if (!sqlIsNull(res, row, "invsts"))
        strncpy(invsts, sqlGetString(res, row, "invsts"), INVSTS_LEN);
    if (!sqlIsNull(res, row, "invsts_prg"))
        strncpy(invsts_prg, 
                sqlGetString(res, row, "invsts_prg"), 
                INVSTS_PRG_LEN);

    strncpy(pcksts, sqlGetString(res, row, "pcksts"), PCKSTS_LEN);
    strncpy(dstloc, sqlGetString(res, row, "dstloc"), DSTLOC_LEN);

    splflg = sqlGetBoolean(res, row, "splflg");
    pckqty = sqlGetLong(res, row, "pckqty");
    if(!sqlIsNull(res, row, "pck_catch_qty"))
    {
        pck_catch_qty = sqlGetFloat(res, row, "pck_catch_qty");
    }
    else
    {
        pck_catch_qty = 0.0;
    }
    untcas = sqlGetLong(res, row, "untcas");
    untpak = sqlGetLong(res, row, "untpak");

    sprintf(seg_qty, "%ld", pckqty);

    if (!sqlIsNull(res, row, "orgcod"))
    strncpy(orgcod,
        sqlGetString(res, row, "orgcod"), ORGCOD_LEN);
    if (!sqlIsNull(res, row, "lotnum"))
    strncpy(lotnum,
        sqlGetString(res, row, "lotnum"), LOTNUM_LEN);
    if (!sqlIsNull(res, row, "revlvl"))
    strncpy(revlvl,
        sqlGetString(res, row, "revlvl"), REVLVL_LEN);
    if (!sqlIsNull(res, row, "supnum"))
        strncpy(supnum,
            sqlGetString(res, row, "supnum"), SUPNUM_LEN);
    if (!sqlIsNull(res, row, "schbat"))
    strncpy(schbat,
        sqlGetString(res, row, "schbat"), SCHBAT_LEN);
    if (!sqlIsNull(res, row, "concod"))
    strncpy(concod,
        sqlGetString(res, row, "concod"), CONCOD_LEN);
    if (!sqlIsNull(res, row, "stcust"))
    strncpy(stcust,
        sqlGetString(res, row, "stcust"), ADRNUM_LEN);
    if (!sqlIsNull(res, row, "rtcust"))
    strncpy(rtcust,
        sqlGetString(res, row, "rtcust"), ADRNUM_LEN);
    if (!sqlIsNull(res, row, "dstare"))
    strncpy(dstare,
        sqlGetString(res, row, "dstare"), ARECOD_LEN);
    if (!sqlIsNull(res, row, "wh_id"))
    strncpy(wh_id,
        sqlGetString(res, row, "wh_id"), WH_ID_LEN);

    if (bIsWorkOrder)
    {
    if (!sqlIsNull(res, row, "wkonum"))
        strncpy(wkonum,
            sqlGetString(res, row, "wkonum"), WKONUM_LEN);
    
    if (!sqlIsNull(res, row, "wkorev"))
        strncpy(wkorev,
            sqlGetString(res, row, "wkorev"), WKOREV_LEN);

    if (!sqlIsNull(res, row, "wkolin"))
        strncpy(wkolin,
            sqlGetString(res, row, "wkolin"), WKOLIN_LEN);
    
    if (!sqlIsNull(res, row, "client_id"))
        strncpy(client_id,
            sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);
    }
    else
    {
    if (!sqlIsNull(res, row, "ship_line_id"))
        strncpy(ship_line_id,
            sqlGetString(res, row, "ship_line_id"), SHIP_LINE_ID_LEN);
    
    if (!sqlIsNull(res, row, "ordnum"))
        strncpy(ordnum,
            sqlGetString(res, row, "ordnum"), ORDNUM_LEN);
    
    if (!sqlIsNull(res, row, "ordlin"))
        strncpy(ordlin,
            sqlGetString(res, row, "ordlin"), ORDLIN_LEN);
    
    if (!sqlIsNull(res, row, "ordsln"))
        strncpy(ordsln,
            sqlGetString(res, row, "ordsln"), ORDSLN_LEN);
    
    if (!sqlIsNull(res, row, "client_id"))
        strncpy(client_id,
            sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);
    
    if (!sqlIsNull(res, row, "ship_id"))
        strncpy(ship_id,
            sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);

    frsflg = sqlGetBoolean(res, row, "frsflg");
    min_shelf_hrs = !sqlIsNull(res, row, "min_shelf_hrs") ?
                    sqlGetLong(res, row, "min_shelf_hrs") :
                    -1;
    }
    if (CurPtr1) 
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr1);
        CurPtr1 = NULL;
    }
    else
    {
        sqlFreeResults(res);            
    }        

    pckrpl_WriteTrc("Processing rplref: %s (for order) ", rplref);
    if (bIsWorkOrder)
        pckrpl_WriteTrc("   Work Order: %s, Rev: %s, Client: %s, Line %s",
            wkonum, wkorev, client_id, wkolin);
    else
        pckrpl_WriteTrc("    Batch: %s, ShipLineid: %s ",
            schbat, ship_line_id);

    pckrpl_WriteTrc("    Part: %s, Quantity: %ld",
            prtnum, pckqty);

    sqlSetSavepoint(savepoint_p);

    /* call allocate */
    if (bIsWorkOrder)
    {
        sprintf(srv_command,
        "allocate inventory "
        "  where wkonum = '%s' and wkorev = '%s' "
        "   and wkolin = '%s' and client_id = '%s' "
        "   and prtnum = '%s' and prt_client_id = '%s' "
        "   and orgcod = '%s' and revlvl = '%s' "
        "   and supnum = '%s' and lotnum = '%s' "
        "   and untpak = %d   and untcas = %d "
        "   and pckqty = %d   and segqty = '%d' "
        "   and invsts = '%s' and invsts_prg = '%s' "
        "   and pcksts = '%s' "
        "   and pcktyp = '%s' and schbat = '%s' "
        "   and pipcod = 'N'  and frsflg = 0 "
        "   and wh_id = '%s'  and pck_catch_qty = %f "
        " and vc_from_rpl = 1 ",
        wkonum, wkorev, wkolin, client_id,
        prtnum, prt_client_id,
        orgcod, revlvl, supnum, lotnum,
        untpak, untcas, pckqty, pckqty,
        invsts, invsts_prg, pcksts, pick_type, schbat, 
        wh_id, pck_catch_qty);
    }
    else
    {
        sprintf(srv_command,
        "allocate inventory where pcktyp = \"%s\" "
        " and prtnum = \"%s\" and prt_client_id = \"%s\" "
        " and quantity = %ld "
        " and orgcod = \"%s\" and revlvl = \"%s\" "
        " and supnum = \"%s\"  "
        " and lotnum = \"%s\" and invsts = \"%s\" "
        " and invsts_prg = \"%s\" "
        " and stcust = \"%s\"  "
        " and rtcust = \"%s\"  "
        " and segqty = \"%s\" "
        " and dstare = \"%s\" and schbat = \"%s\" "
        " and concod = \"%s\"  "
        " and ship_line_id = \"%s\" "
        " and untcas = '%ld'  and untpak = '%ld' "
        " and splflg = '%ld'  and dstloc = \"%s\" "
        " and retmov= \"NO\""
        " and pcksts = \"%s\" and ordnum = \"%s\" "
        " and ordlin = \"%s\" and ordsln = \"%s\" "
        " and client_id = \"%s\" and ship_id = \"%s\""
        " and pipcod = 'N' "
        " and frsflg = '%ld' and min_shelf_hrs = '%ld' "
        " and wh_id = \"%s\" and pck_catch_qty = %f "
        " and vc_from_rpl = 1 ",
        pick_type, misTrim(prtnum), misTrim(prt_client_id),
        pckqty,
        misTrim(orgcod), misTrim(revlvl), misTrim(supnum),
        misTrim(lotnum), misTrim(invsts), 
        misTrim(invsts_prg), 
        misTrim(stcust), 
        misTrim(rtcust), 
        misTrim(seg_qty), misTrim(dstare), misTrim(schbat),
        misTrim(concod), 
        misTrim(ship_line_id), 
        untcas, untpak, splflg, misTrim(dstloc),
        misTrim(pcksts), ordnum, ordlin, ordsln, client_id, ship_id,
        frsflg, min_shelf_hrs, wh_id, pck_catch_qty);
    }
    CurPtr = NULL;

    ret_status = srvInitiateCommand(srv_command, &CurPtr);
    if (ret_status == eDB_DEADLOCK)
    {
        pckrpl_WriteTrc(" --->>>>  DEAD LOCK existing rplref: %s! ",
        rplref);
        return ret_status;
    }
    if (ret_status != eOK)
    {
        pckrpl_WriteTrc(" --->>>>  Unable to allocate rplref: %s! ",
        rplref);

        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        sqlRollbackToSavepoint(savepoint_p);

        memset (tmp_rplref, 0, sizeof (tmp_rplref));
        strncpy (tmp_rplref, rplref, RPLREF_LEN);

        pckrpl_WriteTrc(" --->>>>  Checking pending inventory for "
                        "rplref: %s... ", rplref);

        /* What we're looking for here is the lowest level of */
        /* the replenishment. This would be the child of a single */
        /* hop replen, since children of multi-hops are handled */
        /* under AllocHop.  */

        sprintf(buffer,
            "select r1.rplref,                   "
            "       r1.dstare,                   "
            "       r1.dstloc,                   "
            "       r1.rplsts                    "
            "  from rplwrk r1                    "
            " where r1.parref = '%s'             "
            "   and not exists (select 'x'       "
            "                     from rplwrk r2 "
            "                    where r2.parref = r1.rplref) ",
            tmp_rplref);

        ret_status = sqlExecStr(buffer, &res);
        if (ret_status == eOK)
        { 
            /* If we found one and it's Pending                   */
            /* Deposit, then let's make sure the inventory        */
            /* headed this way hasn't been grabbed out            */
            /* from under us by another allocation due to timing. */

            row = sqlGetRow(res); 

            if (!misCiStrncmp(sqlGetString(res, row, "rplsts"),
                              RPLSTS_PENDDEP,
                              RPLSTS_LEN))
            {
                /* Okay, now we can verify whether or not there */
                /* are any picks headed this way that would     */
                /* satisfy this replen.                         */

                memset(where_clause, 0, sizeof(where_clause));

                if (untcas != 0)
                {
                    sprintf (tmp_buffer, " and untcas = '%ld' ",
                             untcas);
                    strcat (where_clause, tmp_buffer);
                }

                if (untpak != 0)
                {
                    sprintf (tmp_buffer, " and untpak = '%ld' ",
                             untpak);
                    strcat (where_clause, tmp_buffer);
                }

                sprintf(buffer,
                    "select 'x' "
                    "  from invsum "
                    " where arecod = '%s' "
                    "   and stoloc = '%s' "
                    "   and pndqty > 0 "
                    "   and prtnum = '%s' "
                    "   and prt_client_id = '%s' "
                    "   and wh_id = '%s' "
                    "   and invsts in (select invsts "
                    "                    from prgmst "
                    "                   where invsts_prg = '%s') "
                    "   %s                 "
                    "   and rownum < 2 ",
                    sqlGetString(res, row, "dstare"),
                    sqlGetString(res, row, "dstloc"),
                    prtnum,
                    prt_client_id,
                    wh_id,
                    invsts_prg,
                    where_clause ? where_clause : "");

                ret_status = sqlExecStr(buffer, NULL);
                if (ret_status == eOK)
                {
                    pckrpl_WriteTrc(" --->>>>  There's inventory "
                                    "headed into the %s location, we may "
                                    "still be able to satisfy "
                                    " this replenishment. ",
                                    sqlGetString(res, row, "dstloc"));
                }
                else
                {
                    pckrpl_WriteTrc(" --->>>>  There's no inventory "
                                    "to allocate and none on the way, "
                                    "someone must have stolen our  "
                                    "inventory. We're going to reset "
                                    "this replenishment and go again.");

                    sprintf(buffer,
                            "reset emergency replenishment "
                            "  where rplref = '%s' ",
                            tmp_rplref);

                    ret_status = srvInitiateCommand(buffer, NULL);

                    if (ret_status != eOK)
                    {
                        pckrpl_WriteTrc(" --->>>>  Unable to "
                                        "reset replenishment! ");
                    }
                }
            }
        }
        else
        {
            pckrpl_WriteTrc(" --->>>> Waiting on: %s - %s",
                tmp_rplref,
                dstare);
        }
        sqlFreeResults(res);


        return (eRPL_NORMAL_FAILURE);
    }

    alloc_qty = 0;
    alc_catch_qty = 0;
    if (ret_status == eOK)
    {
        res = srvGetResults(CurPtr);

        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            alloc_qty += sqlGetLong(res, row, "pckqty");
            if(!sqlIsNull(res, row, "pck_catch_qty"))
            {
                alc_catch_qty += sqlGetFloat(res, row, "pck_catch_qty");
            }
        }
            
    }

    pckqty -= alloc_qty;
    pck_catch_qty -= alc_catch_qty;
    if (CurPtr)
        srvFreeMemory(SRVRET_STRUCT, CurPtr);

    if (pckqty <= 0 && pck_catch_qty <= 0)
    {
        pckrpl_WriteTrc("   Fully Allocated - request removed ");

        ret_status = pckrpl_CleanupCrossDock(rplref,
                         alloc_qty,
                         1, /* delete */
                         bIsWorkOrder ? NULL : 
                         ship_line_id,
                         wkonum,
                         wkorev,
                         wkolin,
                         wh_id);

        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            pckrpl_WriteTrc("Error (%d) deleting xdkwrk for rplref: %s",
                    ret_status, rplref);
            sqlRollbackToSavepoint(savepoint_p);
            return (ret_status);
        }

        /* Then delete this request and all depending on it... */
        memset(tmp_rplref, 0, sizeof(tmp_rplref));
        strncpy(tmp_rplref, rplref, RPLREF_LEN);
        do
        {
            sprintf(buffer,
                "select rplref "
                "  from rplwrk "
                " where parref = '%s' ",
                tmp_rplref);
            ret_status = sqlExecStr(buffer, &res);
            if ( (ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED) )
            {
                pckrpl_WriteTrc("Unable (%d) to select the rplwrk: %s",
                    ret_status, tmp_rplref);
                sqlFreeResults(res);
                sqlRollbackToSavepoint(savepoint_p);
                return (ret_status);
            }
            sprintf(buffer,
                "delete from rplwrk "
                " where rplref = '%s' ",
                tmp_rplref);
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                pckrpl_WriteTrc("Error (%d) removing completed rplref: %s ",
                    ret_status, rplref);
                sqlRollbackToSavepoint(savepoint_p);
                sqlFreeResults(res);
                return (ret_status);
            }
            row = sqlGetRow(res);
                
            memset (tmp_rplref, 0, sizeof (tmp_rplref));
            if (row && !sqlIsNull(res, row, "rplref"))
            {
                misTrimcpy (tmp_rplref, 
                    sqlGetString(res, row, "rplref"), RPLREF_LEN);
            }
            sqlFreeResults(res);
        } while (misTrimLen(tmp_rplref, RPLREF_LEN));

    }
    else
    {
        /* Otherwise, update the pckqty to be what is left... */
        pckrpl_WriteTrc("   Partially allocated - request reduced by %ld",
            alloc_qty);

        sprintf(buffer,
            "update rplwrk "
            "   set pckqty = pckqty - %ld, "
            "       pck_catch_qty = pck_catch_qty - %f "
            " where rplref = '%s' ",
            alloc_qty, 
            alc_catch_qty,
            rplref);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            pckrpl_WriteTrc("Error (%d) updating completed rplref: %s",
                ret_status, rplref);
            sqlRollbackToSavepoint(savepoint_p);
            return (ret_status);
        }

        ret_status = pckrpl_CleanupCrossDock(rplref,
                         alloc_qty,
                         0, /* update */
                         bIsWorkOrder ? NULL : 
                         ship_line_id,
                         wkonum,
                         wkorev,
                         wkolin,
                         wh_id);
    
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            pckrpl_WriteTrc("Error (%d) updating completed rplref: %s",
                ret_status, rplref);
            sqlRollbackToSavepoint(savepoint_p);
            return (ret_status);
        }
    }

    return (eOK);
}

static long AllocHop(char *rplref)
{
    long            ret_status;
    long            alloc_qty;
    double          alc_catch_qty;
    long            pckqty;
    double          pck_catch_qty;
    long            untcas;
    long            untpak;
    char            tmpbuf[200];
    char            pick_type[100];
    char            srv_command[2000];
    char            seg_qty[10];
    char            fifo_date[50];
    char            dstloc[STOLOC_LEN + 1];
    char            buffer[500];

    char            srcare[ARECOD_LEN + 1];
    char            prtnum[PRTNUM_LEN + 1];
    char            prt_client_id[CLIENT_ID_LEN + 1];
    char            orgcod[ORGCOD_LEN + 1];
    char            revlvl[REVLVL_LEN + 1];
    char            lotnum[LOTNUM_LEN + 1];
    char            dstare[ARECOD_LEN + 1];
    char            lodlvl[LODLVL_LEN + 1];
    char            parref[RPLREF_LEN + 1];
    char            invsts[INVSTS_LEN + 1];
    char            invsts_prg[INVSTS_PRG_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    char            asset_typ[ASSET_TYP_LEN + 1];
    char            tmp_rplref[RPLREF_LEN + 1];
    char            tmp_parref[RPLREF_LEN + 1];
    char            supnum[SUPNUM_LEN + 1];

    char            where_clause[400];
    char            tmp_buffer[40];

    moca_bool_t     splflg;

    RETURN_STRUCT  *CurPtr = NULL;
    RETURN_STRUCT  *LocRes = NULL;
    mocaDataRes    *res = NULL;
    mocaDataRes    *d_res = NULL;
    mocaDataRes    *lres = NULL;
    mocaDataRow    *row, *prv_row, *d_row, *lrow;

    char *savepoint_p = "rpl_multi_hop";

    pckrpl_WriteTrc("Considering rplref: %s for multi-hop allocation",
            rplref);

    memset(orgcod, 0, sizeof(orgcod));
    memset(lotnum, 0, sizeof(lotnum));
    memset(revlvl, 0, sizeof(revlvl));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(dstare, 0, sizeof(dstare));
    memset(lodlvl, 0, sizeof(lodlvl));
    memset(invsts, 0, sizeof(invsts));
    memset(wh_id,  0, sizeof(wh_id));
    memset(invsts_prg, 0, sizeof(invsts_prg));
    memset(srcare, 0, sizeof(srcare));
    memset(dstloc, 0, sizeof(dstloc));
    memset(asset_typ, 0, sizeof(asset_typ));
    memset(supnum, 0, sizeof(supnum));

    sprintf(buffer,
        "select pckqty, pck_catch_qty, prtnum, prt_client_id, wh_id, "
        "       orgcod, revlvl, supnum, lotnum,"
        "       invsts, invsts_prg, lodlvl, dstare, parref, untcas,"
        "       untpak, splflg, dstloc "
        "  from rplwrk "
        " where rplref = '%s' "
        "  for update of pckqty",
        rplref);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        pckrpl_WriteTrc("Couldn't get rplwrk: %s for update(%d)",
            rplref, ret_status);
        sqlFreeResults(res);
        return (eRPL_NORMAL_FAILURE);

        /*    return (ret_status); */
    }
    row = sqlGetRow(res);

    strncpy(prtnum, sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
    strncpy(prt_client_id, 
        sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
    strncpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);

    if (!sqlIsNull(res, row, "invsts"))
        strncpy(invsts, sqlGetString(res, row, "invsts"), INVSTS_LEN);

    if (!sqlIsNull(res, row, "invsts_prg"))
        strncpy(invsts_prg, 
                sqlGetString(res, row, "invsts_prg"), 
                INVSTS_PRG_LEN);

    splflg = sqlGetBoolean(res, row, "splflg");
    pckqty = sqlGetLong(res, row, "pckqty");
    if(!sqlIsNull(res, row, "pck_catch_qty"))
    {
        pck_catch_qty = sqlGetFloat(res, row, "pck_catch_qty");
    }
    else
    {
        pck_catch_qty = 0.0;
    }
    untcas = sqlGetLong(res, row, "untcas");
    untpak = sqlGetLong(res, row, "untpak");

    if (!sqlIsNull(res, row, "orgcod"))
    strncpy(orgcod, sqlGetString(res, row, "orgcod"), ORGCOD_LEN);
    if (!sqlIsNull(res, row, "lotnum"))
    strncpy(lotnum, sqlGetString(res, row, "lotnum"), LOTNUM_LEN);
    if (!sqlIsNull(res, row, "revlvl"))
    strncpy(revlvl, sqlGetString(res, row, "revlvl"), REVLVL_LEN);
    if (!sqlIsNull(res, row, "supnum"))
    strncpy(supnum, sqlGetString(res, row, "supnum"), SUPNUM_LEN);
    if (!sqlIsNull(res, row, "dstare"))
    strncpy(srcare, sqlGetString(res, row, "dstare"), ARECOD_LEN);
    if (!sqlIsNull(res, row, "lodlvl"))
    strncpy(lodlvl, sqlGetString(res, row, "lodlvl"), LODLVL_LEN);
    if (!sqlIsNull(res, row, "dstloc"))
        strncpy(dstloc, sqlGetString(res, row, "dstloc"), STOLOC_LEN);

    /**************************************************************
      1.  To be here, we really need to have a parent replenishment
          in order to be doing anything useful.
      2.  In order for us to need to allocate again, our parent
          must not be for an order. 
      
     Remember that AllocOrder takes care of allocating for a
     shipment line.  Which means that unless we have a second hop, there's
     nothing to be done here.  For example:
         rpl1 -  order information in it (handled by AllocOrder)
          |
     rpl2 - single replen waiting deposit...

     Since rpl2's parent is for an order, then there is no further
     allocation required that won't be picked up by AllocOrder.
     The only case that matters is:

         rpl1 - shipment line information
       |
     rpl2 - final replen (AreaB)
       |
     rpl3 - intermediate replen (AreaA)

     In this example, when the deposit is completed for rpl3, we
     will need to generate a movement from AreaA to AreaB
     **************************************************************/

    if (!sqlIsNull(res, row, "parref"))
    {
        memset(parref, 0, sizeof(parref));
        strncpy(parref,
            sqlGetString(res, row, "parref"), RPLREF_LEN);

        sprintf(buffer,
            "select dstare, lodlvl, ship_line_id"
            "  from rplwrk "
            " where rplref = '%s' "
            " for update of pckqty ",
            parref);
        sqlFreeResults(res);
        res = NULL;

        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            pckrpl_WriteTrc("Unable to get parent rplwrk: %s for update (%d)",
                parref, ret_status);
            sqlFreeResults(res);

            /* If this failed for any other reason than deadlock, 
             * we'll go ahead and delete and return OK.
             */
            if (ret_status == eDB_DEADLOCK)
                return(ret_status);

            sprintf(buffer,
                "delete from rplwrk where rplref = '%s'",
                rplref);

            sqlExecStr(buffer, NULL);
            return (eRPL_NORMAL_FAILURE);
        }

        row = sqlGetRow(res);

        if (!sqlIsNull(res, row, "ship_line_id"))
        {
            pckrpl_WriteTrc("rplref: %s, does not require further allocation "
                    " (parent is shipment line) ", rplref);
            sqlFreeResults(res);
                res = NULL;

            return (eRPL_NORMAL_FAILURE);
        }

        if (!sqlIsNull(res, row, "dstare"))
        {
            memset(dstare, 0, sizeof(dstare));
            strncpy(dstare,
                sqlGetString(res, row, "dstare"), ARECOD_LEN);
        }
        if (!sqlIsNull(res, row, "lodlvl"))
        {
            memset(lodlvl, 0, sizeof(lodlvl));
            strncpy(lodlvl,
                sqlGetString(res, row, "lodlvl"), LODLVL_LEN);
        }
        sqlFreeResults(res);
        res = NULL;
    }
    else
    {
        /* If we don't have a parent, then what exactly are we 
           completing???  (Remember...we're only in this routine
           if this rplref is NOT for an order...) This is a bogey
           replenishment, we'll just go ahead and delete it and go on....  */
        sqlFreeResults(res);
        res = NULL;

        sprintf(buffer,
            "delete from rplwrk "
            " where rplref = '%s' ",
            rplref);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            pckrpl_WriteTrc("Unable to delete orphaned rplwrk: %s, (%d)",
                rplref, ret_status);
            
            return (ret_status);
        }
        return (eRPL_NORMAL_FAILURE);
    }

    /* move the quantity to the segment quantity */
    sprintf(tmpbuf, "%ld", pckqty);
    strcpy(seg_qty, tmpbuf);

    memset(pick_type, 0, sizeof(pick_type));
    strcpy(pick_type, ALLOCATE_REPLENISH);

    pckrpl_WriteTrc(" Processing rplref: %s (for replen) ", rplref);
    pckrpl_WriteTrc("   Part: %s, Qty: %ld, Src Area: %s Dest Area: %s",
            prtnum, pckqty, srcare, dstare);

    sqlSetSavepoint(savepoint_p);
    /* call allocate */
    sprintf(srv_command,
        "allocate inventory where pcktyp = \"%s\" "
        " and prtnum = \"%s\" and prt_client_id = \"%s\" "
        " and quantity = '%ld' and pck_catch_qty = %f "
        " and orgcod = \"%s\" and revlvl = \"%s\" "
        " and supnum = \"%s\" "
        " and lotnum = \"%s\" and invsts = \"%s\" "
        " and invsts_prg = \"%s\" "
        " and segqty = \"%s\" and srcare = \"%s\""
        " and dstare = \"%s\" and pcklvl = \"%s\""
        " and untcas = '%ld'  and untpak = '%ld'"
        " and splflg = \"%ld\" "
        " and retmov= \"NO\""
        " and pipcod = 'N' "
        " and wh_id = \"%s\" "
        " and vc_from_rpl = 1 ",
        pick_type, prtnum, prt_client_id, 
        pckqty, pck_catch_qty,
        orgcod, revlvl, supnum, lotnum,
        invsts, invsts_prg, seg_qty, srcare,
        dstare, lodlvl, untcas, untpak, splflg, wh_id);

    CurPtr = NULL;

    ret_status = eDB_DEADLOCK;

    ret_status = srvInitiateCommand(srv_command, &CurPtr);
    if (ret_status == eDB_DEADLOCK)
    {
        pckrpl_WriteTrc(" --->>>>  DEAD LOCK existing");
        return ret_status;
    }
    if (ret_status != eOK)
    {
    pckrpl_WriteTrc(" --->>>>  Unable to allocate! ");

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;

    sqlRollbackToSavepoint(savepoint_p);

        /* Let's make sure the inventory        */
        /* headed this way hasn't been grabbed out            */
        /* from under us by another allocation due to timing. */

    
        memset (where_clause, 0, sizeof (where_clause));
        if (untcas != 0)
        {
            sprintf (tmp_buffer, " and untcas = '%ld' ", untcas);
            strcat (where_clause, tmp_buffer);
        }

        if (untpak != 0)
        {
            sprintf (tmp_buffer, " and untpak = '%ld' ", untpak);
            strcat (where_clause, tmp_buffer);
        }


        sprintf(buffer,
                "select 'x' "
                "  from invsum "
                " where arecod = '%s' "
                "   and stoloc = '%s' "
                "   and pndqty > 0 "
                "   and prtnum = '%s' "
                "   and prt_client_id = '%s' "
                "   and wh_id = '%s' "
                "   and invsts in (select invsts "
                "                    from prgmst "
                "                   where invsts_prg = '%s') "
                "   %s                 "
                "   and rownum < 2 ",
                srcare,
                dstloc,
                prtnum,
                prt_client_id,
                wh_id,
                invsts_prg,
                where_clause ? where_clause : "");

        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status == eOK)
        {

            pckrpl_WriteTrc(" --->>>>  There's inventory "
                            "headed into the %s area, we may "
                            "still be able to satisfy "
                            " this replenishment. ",
                             srcare);
        }
        else
        {
            pckrpl_WriteTrc(" --->>>>  There's no inventory "
                            "to allocate and none on the way, "
                            "someone must have stolen our  "
                            "inventory or the replenishment pick "
                            "was cancelled. We're going to reset "
                            "this replenishment and let it go again.");

            sprintf(buffer,
                    "reset emergency replenishment "
                    "  where rplref = '%s' ",
                    rplref);

            ret_status = srvInitiateCommand(buffer, NULL);

            if (ret_status != eOK)
            {
                pckrpl_WriteTrc(" --->>>>  Unable to "
                                "reset replenishment! ");
            }
        }
        return(eRPL_NORMAL_FAILURE);
    }

    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);
    prv_row = row;

    /* Spin through the result set and allocate a location
       for each combo code */

    do
    {  /* The inside loop will advance the row for this loop */

    alloc_qty = 0;
    alc_catch_qty = 0;
    memset(orgcod, 0, sizeof(orgcod));
    memset(lotnum, 0, sizeof(lotnum));
    memset(revlvl, 0, sizeof(revlvl));
    memset(supnum, 0, sizeof(supnum));
    strncpy(orgcod,
        sqlGetString(res, row, "orgcod"), ORGCOD_LEN);
    strncpy(lotnum,
        sqlGetString(res, row, "lotnum"), LOTNUM_LEN);
    strncpy(revlvl,
        sqlGetString(res, row, "revlvl"), REVLVL_LEN);
    strncpy(supnum,
        sqlGetString(res, row, "supnum"), SUPNUM_LEN);

    /* Re-get the inventory status */
    strncpy(invsts, sqlGetString(res, row, "invsts"), INVSTS_LEN);
        
    /* Get asset_typ if it was returned */
    if (!sqlIsNull(res, row, "asset_typ"))
        strncpy(asset_typ, sqlGetString(res, row, "asset_typ"), ASSET_TYP_LEN);

    /* Turn the fifo date into something that allocate location
       can use.... */
    sprintf(buffer,
        "select to_date('%s', 'J') fifo_date "
        "  from dual",
        sqlGetString(res, row, "fifdte"));
    ret_status = sqlExecStr(buffer, &d_res);
    if (ret_status != eOK)
    {
        sqlFreeResults(d_res);
        d_res = NULL;
        memset(fifo_date, 0, sizeof(fifo_date));
    }
    else
    {
        d_row = sqlGetRow(d_res);
        sprintf(fifo_date,
            "%s", (char *) sqlGetString(d_res, d_row, "fifo_date"));
        sqlFreeResults(d_res);
        d_res = NULL;
    }

    /* Sum the allocation quantity for a combo code */
    do
    {
        alloc_qty += sqlGetLong(res, row, "pckqty");
        if(!sqlIsNull(res, row, "pck_catch_qty"))
        {
            alc_catch_qty += sqlGetFloat(res, row, "pck_catch_qty");
        }
        prv_row = row;
        row = sqlGetNextRow(row);
    } while (row != NULL &&
         strncmp(sqlGetString(res, prv_row, "cmbcod"),
             sqlGetString(res, row, "cmbcod"), CMBCOD_LEN) == 0);
    
    /* Decrement the quantity remaining to be allocated */
    pckqty -= alloc_qty;
    pck_catch_qty -= alc_catch_qty;

    untcas = sqlGetLong(res, prv_row, "untcas");
    untpak = sqlGetLong(res, prv_row, "untpak");

    /* Allocate a location for the deposit */

    LocRes = NULL;
    sprintf(srv_command,
        "allocate location where arecod = \"%s\" and "
        " prtnum = \"%s\" and prt_client_id = \"%s\" and "
        " lotnum = \"%s\" and revlvl = \"%s\" and supnum = \"%s\" and "
        " orgcod = \"%s\" and invsts = \"%s\" and untcas = \"%d\" and "
        " untqty = \"%d\" and fifdte = \"%s\" and type = \"%s\" and "
        " untpak = \"%d\" and splflg = \"%ld\" and "
        " invmov_typ = \"%s\" and wh_id = \"%s\" "
                " and asset_typ = \"%s\" ",
        dstare, 
        prtnum, prt_client_id,
        lotnum, revlvl, supnum,
        orgcod, invsts, untcas,
        alloc_qty, fifo_date, ALLOCATE_REPLENISH,
        untpak, splflg, INVMOV_TYP_RPLN, wh_id,
                asset_typ);

    ret_status = srvInitiateCommand(srv_command, &LocRes);
    if (ret_status != eOK)
    {
        if (LocRes)
        srvFreeMemory(SRVRET_STRUCT, LocRes);
            LocRes = NULL;
        if (CurPtr)
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;

        pckrpl_WriteTrc("   **** FAILED to allocate "
                "DESTINATION in area %s ****",
                dstare);

        if (ret_status != eDB_DEADLOCK)
        {
        sprintf(buffer, "No storage in area %s", dstare);
        FailReplenishment(rplref, buffer);
        }
        
        sqlRollbackToSavepoint(savepoint_p);
        return(eRPL_NORMAL_FAILURE);
    }

    memset(dstloc, 0, sizeof(dstloc));
    lrow = NULL;
    lres = LocRes->ReturnedData;

    for (lrow = sqlGetRow(lres); 
         lrow && sqlGetNextRow(lrow); lrow = sqlGetNextRow(lrow));

    if (lrow)
        misTrimcpy(dstloc,
               (char *)sqlGetString(lres, lrow, "nxtloc"), STOLOC_LEN);

    srvFreeMemory(SRVRET_STRUCT, LocRes);
    LocRes = NULL;

    /* Update the pick move with the location */
    sprintf(buffer,
        "update pckmov "
        "   set stoloc = '%s' "
        " where cmbcod = '%s'"
        "   and arecod = '%s'",
        dstloc,
        sqlGetString(res, prv_row, "cmbcod"),
        dstare);

    ret_status = sqlExecStr(buffer, NULL);

    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        pckrpl_WriteTrc("Unable to update pckmov with dstloc, "
                " rplwrk: %s, (%d)",
                rplref, ret_status);
        sqlRollbackToSavepoint(savepoint_p);
        return(ret_status);
    }

        /* Update the pick works with the location */
        /* so that, if the pick gets cancelled it */
        /* gets written to the canpck table. Then, if they */
        /* want to have the allocate cancelled picks */
        /* command reallocate it, it will go after the same */
        /* dstloc, but reallocate from a new source location. */

        sprintf(buffer,
                "update pckwrk "
                "   set dstloc = '%s' "
                " where cmbcod = '%s'"
                "   and dstare = '%s'",
                dstloc,
                sqlGetString(res, prv_row, "cmbcod"),
                dstare);

        ret_status = sqlExecStr(buffer, NULL);

        if (ret_status != eOK)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            pckrpl_WriteTrc("Unable to update pckwrk with dstloc, "
                            " rplwrk: %s, (%d)",
                            rplref, ret_status);
            sqlRollbackToSavepoint(savepoint_p);
            return(ret_status);
        }
        pckrpl_WriteTrc(" -> %s set to go to destination: %s",
            rplref, dstloc);

    } while (row != NULL);
    
    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;

    sprintf(buffer,
        "update rplwrk "
        "   set alcqty = '%ld', "
        "       alc_catch_qty = %f, "
        "       dstloc = '%s', "
        "       untcas = %ld, "
        "       untpak = %ld "
        " where rplref = '%s' ",
        alloc_qty, alc_catch_qty, dstloc, untcas, untpak, parref);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK)
    {
        /* This isn't fatal...it just means that no future
           glom-ing can happen... */
        pckrpl_WriteTrc("(non-fatal) Failed to update rplwrk for "
                "allocated qty (%d)",
                ret_status);
    }

    memset(tmp_parref, 0, sizeof(tmp_parref));
    strncpy(tmp_parref, parref, RPLREF_LEN);
    do
    {
       /* Set the parent's allocation date/time to be now... */
        sprintf(buffer,
        "update rplwrk "
        "   set alcdte = sysdate"
        " where rplref = '%s' ",
        tmp_parref);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            pckrpl_WriteTrc("Update of alcdte failed: (%d), rplref: %s",
                ret_status, parref);
            sqlRollbackToSavepoint(savepoint_p);
            return (ret_status);
        }
    
        sprintf(buffer,
        "select parref "
        "  from rplwrk "
        " where rplref = '%s' ",
        tmp_parref);
        ret_status = sqlExecStr(buffer, &res);
        if ( (ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED) )
        {
            pckrpl_WriteTrc("Unable (%d) to select the rplwrk: %s",
                ret_status, tmp_rplref);
            sqlFreeResults(res);
            sqlRollbackToSavepoint(savepoint_p);
            return (ret_status);
        }
        row = sqlGetRow(res);
    
        memset(tmp_parref, 0, sizeof(tmp_parref));
        if (!sqlIsNull(res, row, "parref"))
            misTrimcpy (tmp_parref, 
                sqlGetString(res, row, "parref"), RPLREF_LEN);

    } while (misTrimLen(tmp_parref, RPLREF_LEN));
    
    memset(tmp_rplref, 0, sizeof(tmp_rplref));
    strncpy(tmp_rplref, rplref, RPLREF_LEN);

    do
    {
        /* Now...delete the completed replenishment... */
        sprintf(buffer,
        "delete from rplwrk "
        " where rplref = '%s' ",
        tmp_rplref);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            pckrpl_WriteTrc("Unable to remove completed replenishment: %d",
                ret_status);
            sqlRollbackToSavepoint(savepoint_p);
            return (ret_status);
        }

        sprintf(buffer,
            "select parref "
            "  from rplwrk "
            " where rplref = '%s' ",
            tmp_rplref);
        ret_status = sqlExecStr(buffer, NULL);
        if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
        {
            pckrpl_WriteTrc("Unable (%d) to select the rplwrk: %s",
                ret_status, tmp_rplref);
            sqlFreeResults(res); 
            sqlRollbackToSavepoint(savepoint_p);
            return (ret_status);
        }
        row = sqlGetRow(res);
        
        misTrimcpy (tmp_rplref, sqlGetString(res, row, "parref"), RPLREF_LEN);
    } while (misTrimLen(tmp_rplref, RPLREF_LEN));
    
    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    return (eOK);
}

/*****************************************************************************/
/*****************************************************************************/

LIBEXPORT
RETURN_STRUCT *varCompleteEmergencyReplenishment(char *rplref_i,
                                                 char *wh_id_i,
                         moca_bool_t *comflg_i)
{
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *CmdRes;
    long ret_status;
    
    char buffer[2000];
    char rplref[RPLREF_LEN+1];
    char wh_id[WH_ID_LEN+1];
    char compare_date[30];
    
    moca_bool_t bLookingSpecific;
    moca_bool_t bCommitAsWeGo;

    RPL_POLICY *policy;

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    else
        return(APPMissingArg("wh_id"));

    policy = pckrpl_GetConfig(wh_id);
    if (!policy)
    {
        return(APPError(eRPL_NOT_INSTALLED));
    }

    if (comflg_i && *comflg_i)
        bCommitAsWeGo = 1;
    else
        bCommitAsWeGo = 0;
    
    pckrpl_WriteTrc("\n*** Beginning Replenishment Completion "
            "Processing Cycle ***\n");

    sprintf(buffer,
        "select sysdate - "
        "        /*=moca_util.days(*/ (%d/(60.0 * 24)) /*=)*/ compare_date "
        "  from dual",
        policy->min_rpl_time);
    
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        return (APPError(ret_status));
    }
  
    row = sqlGetRow(res);
    sprintf(compare_date, "%s", sqlGetString(res, row, "compare_date"));
    sqlFreeResults(res);
  
    memset(rplref, 0, sizeof(rplref));
    if (rplref_i && misTrimLen(rplref_i, RPLREF_LEN))
    {
        misTrimcpy(rplref, rplref_i, RPLREF_LEN);
        bLookingSpecific = BOOLEAN_TRUE;
    }

    if (strlen(rplref))
    {
        sprintf(buffer,
        "select rplref, ship_line_id, wkonum, ship_id, prtnum, "
        "       prt_client_id, wkorev, client_id, parref, wh_id"
        "  from rplwrk "
        " where rplref = '%s' " 
        "   and rplsts = '%s' ",
        rplref, RPLSTS_PENDDEP);
    }
    else
    {
        sprintf(buffer,
        "select rplref, ship_line_id, wkonum, ship_id, prtnum, "
        "       prt_client_id, wkorev, client_id, parref, wh_id"
        "  from rplwrk "
        " where wh_id = '%s' " 
        "   and rplsts = '%s' ",
        wh_id, RPLSTS_PENDDEP);   
    }

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        pckrpl_WriteTrc("Status %d returned looking for replenishments "
            "to complete - getting out", ret_status);
        sqlFreeResults(res);
        pckrpl_WriteTrc("\n*** Done with Replenishment Completion "
            "Processing Cycle ***\n");

        if (bLookingSpecific)
            return(APPError(ret_status));
        else if (ret_status == eDB_NO_ROWS_AFFECTED)
            return(srvResults(eOK, NULL));
        else
            return(APPError(ret_status));
    }

    CmdRes = srvResultsInit(eOK,
                "ship_id",      COMTYP_CHAR,  SHIP_ID_LEN,
                "ship_line_id", COMTYP_CHAR,  SHIP_LINE_ID_LEN,
                "wkonum",       COMTYP_CHAR,  WKONUM_LEN,
                "wkorev",       COMTYP_CHAR,  WKOREV_LEN,
                "client_id",    COMTYP_CHAR,  CLIENT_ID_LEN,
                "prtnum",       COMTYP_CHAR,  PRTNUM_LEN,
                "prt_client_id",COMTYP_CHAR,  CLIENT_ID_LEN,
                "wh_id",        COMTYP_CHAR,  WH_ID_LEN,
                NULL);

    /* Loop through and process each replenishment work */
    row = sqlGetRow(res);
    while (row != NULL)
    {
        /* First determine if the "replenishment request" is 
           really an outstanding shipment line which is waiting on
           replenishments, or is a replenishment waiting on 
           another replenishment....  */
        if (sqlIsNull(res, row, "ship_line_id") &&
            sqlIsNull(res, row, "wkonum"))
        {
            /* This is a replenishment request... */
          
            pckrpl_WriteTrc("  Attempting to allocate hop for rplref: %s",
                    sqlGetString(res, row, "rplref"));

            ret_status = AllocHop(sqlGetString(res, row, "rplref"));
        }
        else 
        {
            pckrpl_WriteTrc("  Attempting to allocate to order for rplref: %s",
                    sqlGetString(res, row, "rplref"));

            ret_status = AllocForDemand(sqlGetString(res, row, "rplref"),
                        !sqlIsNull(res, row, "wkonum"));
        }

        if (ret_status == eOK)
        {
            srvResultsAdd(CmdRes,
                  (sqlIsNull(res,row,"ship_id") ? NULL :
                   sqlGetString(res,row, "ship_id")),
                  (sqlIsNull(res,row,"ship_line_id") ? NULL :
                   sqlGetString(res,row, "ship_line_id")),
                  (sqlIsNull(res,row,"wkonum") ? NULL :
                   sqlGetString(res,row, "wkonum")),
                  (sqlIsNull(res,row,"wkorev") ? NULL :
                   sqlGetString(res,row, "wkorev")),
                  (sqlIsNull(res,row,"client_id") ? NULL :
                   sqlGetString(res,row, "client_id")),
                  (sqlIsNull(res,row,"prtnum") ? NULL :
                   sqlGetString(res,row, "prtnum")),
                  (sqlIsNull(res,row,"prt_client_id") ? NULL :
                   sqlGetString(res,row, "prt_client_id")),
                   (sqlIsNull(res,row,"wh_id") ? NULL :
                   sqlGetString(res,row, "wh_id")));
        }
        else if (ret_status != eRPL_NORMAL_FAILURE)
        {
            pckrpl_WriteTrc(" Failed (%d) attempting to process "
                    "completion - getting out", ret_status);
            sqlFreeResults(res);
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
            pckrpl_WriteTrc("\n*** Done with Replenishment Completion "
                    "Processing Cycle ***\n");
            return(APPError(ret_status));
        }

        if (bCommitAsWeGo)
            srvCommit();

        /* Set the results to the next rplwrk row */
        row = sqlGetNextRow(row);
    }
  
    sqlFreeResults(res);

    pckrpl_WriteTrc("\n*** Done with Replenishment Completion "
            "Processing Cycle ***\n");
    
    return(CmdRes);  
}
