static const char *rcsid = "$Id: intProcessUCC128Generation.c 153127 2008-04-17 09:23:31Z kzhao $";
/*#START***********************************************************************
 *
 * Copyright (c) 2004 RedPrairie Corporation.  All rights reserved.
 *  
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "intlib.h"

#define DEFAULT_AI      "00"
#define PACTYP_CASE     "0"
#define PACTYP_PALLET   "1"

/*  !!!!!!!!!!! JJS - just for testing now... may eventually use this !!!!!!!! */

typedef struct ucc_info_s
{
    char            ucclvl[LODLVL_LEN + 1];
    char            uccseq[RTSTR1_LEN + 1];
    char            uccman[RTSTR1_LEN + 1];
} UCCINFO;

typedef struct _WORKLIST
{
    char           *wrkref;
    char           *lodlvl;
    long            pckqty;
    long            untcas;
    long            remqty;
    mocaDataRow    *Row;
    mocaDataRes    *Res;
    struct _WORKLIST *Next;
} WORKLIST;

typedef struct _PARTIALLIST
{
    int             remqty;
    WORKLIST       *WorkPtr;
    struct _PARTIALLIST *Next;
    struct _PARTIALLIST *Last;
} PARTIALLIST;

typedef struct return_placeholder
{
    char            wrkref[WRKREF_LEN + 1];
    char            ucccod[UCCCOD_LEN + 1];
    long            pckqty;

    struct return_placeholder *next;
} RETURN_PLACEHOLDER;

static RETURN_PLACEHOLDER *ReturnPlaceHolderList = NULL;
static PARTIALLIST *PartialList;
static long     sGenerateSeparateCmbcods = -1;

/* The "first time" flag gets set to zero upon entering this
   routine and is bumped to one after having updated the first
   pckwrk with a subucc...it is used in conjunction with the 
   separate cmbcod logic...the first cmbcod we have, we want to
   use...after that we must generate new cmbcods and copy the
   pckmovs... */

static long     sFirstTime = 0;


/* Some internal functions */
static RETURN_STRUCT *sProcessWrkref(mocaDataRes * res, mocaDataRow * row);
static long sCreateReturn(RETURN_STRUCT ** CurPtr,
			  char *wrkref,
			  char *ucccod,
			  long pckqty);

static long sDoSplit(PARTIALLIST * CurrentPartial,
		     PARTIALLIST * NextPartial,
		     long untcas,
		     UCCINFO * uccInfo);

static long sGetNewUCC(UCCINFO * uccInfo, char *ucccod, char *wrkref);
static long sAddToReturnStack(char *wrkref, char *ucccod, long pckqty);
static void sFreeReturnStack();
static long sCreateReturnFromStack(RETURN_STRUCT ** CurPtr);

static long sDuplicatePckmovs(char *oldcmbcod, char *newcmbcod);

LIBEXPORT 
RETURN_STRUCT *varProcessUCC128Generation(char *wrkref_i,
				          char *Identifier_i,
				          char *lodnum_i,
				          char *subnum_i,
				          char *dtlnum_i,
				          char *cmbcod_i,
				          char *wh_id_i)
{
    long            ret_status;

    char            wrkref[WRKREF_LEN + 1];
    char            lodnum[LODNUM_LEN + 1];
    char            subnum[SUBNUM_LEN + 1];
    char            dtlnum[DTLNUM_LEN + 1];
    char            Identifier[LODNUM_LEN + 1];
    char            cmbcod[CMBCOD_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    static long     is_installed = -1;
    char            buffer[500];
    RETURN_STRUCT  *CurPtr;
    mocaDataRow    *row;
    mocaDataRes    *res;

    sFirstTime = 1;

    memset(wh_id, 0, sizeof(wh_id));
		
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        strncpy(wh_id, wh_id_i, misTrimLen(wh_id_i, WH_ID_LEN));
    else
        return APPMissingArg("wh_id"); 
    
    if (is_installed < 0)
    {
        if (appIsInstalledWhID(POLCOD_UCC128_PROC, wh_id))
            is_installed = 1;
        else
            is_installed = 0;
    }

    if (!is_installed)
    {
	misTrc(T_FLOW, "UCC 128 processing not installed - skipped");
	return (srvResults (eOK, NULL));
    }

    sFreeReturnStack();  /* just in case */

    /*  first lets see what we got from the 
       command line and move data into local variabless */

    memset(wrkref, 0, sizeof(wrkref));
    memset(lodnum, 0, sizeof(lodnum));
    memset(subnum, 0, sizeof(subnum));
    memset(dtlnum, 0, sizeof(dtlnum));
    memset(cmbcod, 0, sizeof(cmbcod));
    memset(Identifier, 0, sizeof(Identifier));

    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
	strncpy(wrkref, wrkref_i, misTrimLen(wrkref_i, WRKREF_LEN));

    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
	strncpy(lodnum, lodnum_i, misTrimLen(lodnum_i, LODNUM_LEN));

    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
	strncpy(subnum, subnum_i, misTrimLen(subnum_i, SUBNUM_LEN));

    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
	strncpy(dtlnum, dtlnum_i, misTrimLen(dtlnum_i, DTLNUM_LEN));

    if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
	strncpy(cmbcod, cmbcod_i, misTrimLen(cmbcod_i, CMBCOD_LEN));

    if (Identifier_i && misTrimLen(Identifier_i, DTLNUM_LEN))
	strncpy(Identifier, Identifier_i,
		misTrimLen(Identifier_i, LODNUM_LEN));

    /*
     * sGenerateSeparateCmbcods is a static value, and 
     * it was supposed to be changed at first time loading.
     * Because of consideration on subflg or dtlflg on pick,
     * we may change it to 1, and then we have to reload then
     * policy to get correct value when it is 1.
     */
    if (-1 == sGenerateSeparateCmbcods ||
        1  == sGenerateSeparateCmbcods)
    {
	sprintf(buffer,
		"select rtstr1 "
		"  from poldat_view "
		" where polcod = '%s' "
		"   and polvar = '%s' "
		"   and polval = '%s' "
		"   and wh_id  = '%s' ",
		POLCOD_UCC128_PROC,
		POLVAR_MISC,
		POLVAL_UCC128_CONFIRM_SUBS_SEPARATELY,
		wh_id);

	ret_status = sqlExecStr(buffer, &res);
	if (eOK != ret_status)
	{
	    sGenerateSeparateCmbcods = 0;
	    sqlFreeResults(res);
	}
	else
	{
	    row = sqlGetRow(res);
	    if (0 == misCiStrncmp(sqlGetValue(res, row, "rtstr1"),
				  "Y", 1))
		sGenerateSeparateCmbcods = 1;
	    else
		sGenerateSeparateCmbcods = 0;
	    sqlFreeResults(res);
	}
    }

    if (!strlen(wrkref) && !strlen(lodnum) &&
	!strlen(subnum) && !strlen(dtlnum) &&
	!strlen(cmbcod))
    {
	sFreeReturnStack();  /* just in case */
	return (srvResults(eOK, NULL));
    }

    /* Okay, now we've got some args. */

    if (strlen(wrkref))
    {
	sprintf(buffer,
		"select * from pckwrk where wrkref = '%s'", wrkref);
	ret_status = sqlExecStr(buffer, &res);

	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    sFreeReturnStack();  /* just in case */
	    return (srvResults(ret_status, NULL));
	}

	row = sqlGetRow(res);

	if (sqlGetLong(res, row, "appqty") > 0)
	{
	    sqlFreeResults(res);
	    sFreeReturnStack();
	    return (srvResults(eINT_UCC128_ALREADY_APPLIED, NULL));
	}

	if (strlen(misTrim((char *) sqlGetValue(res, row, "subucc"))) ||
	    strlen(misTrim((char *) sqlGetValue(res, row, "loducc"))))
	{
	    /* We already processed this one... */
	    return (srvResults(eOK, NULL));
	}

        misTrc(T_FLOW, "Checking the pick level flags.");

        if ((sqlGetLong(res, row, "subflg") == 1) ||
            (sqlGetLong(res, row, "dtlflg") == 1))
        {
            /* This is a serialized case pick or case pick of a serialized
             * detail so each pick needs
             * to be processed individually.
             */
            misTrc(T_FLOW, "We're going to generate separate cmbcods here "
                           "because we've got a case pick of a serialized "
                           "sub or detail part and we need to process each "
                           "case individually. ");

            sGenerateSeparateCmbcods = 1;
        }
             
        /* Okay, we have a wrkref which can be processed */
	CurPtr = sProcessWrkref(res, row);

	sqlFreeResults(res);

	sFreeReturnStack();
	return (CurPtr);
    }
    else
    {
	/* We don't currently support anything besides
	   generating by wrkref...just return no rows
	   affected here... */
	sFreeReturnStack();
	return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));
    }

}

static void sFreeWorkList(WORKLIST * wptr)
{
    WORKLIST       *last, *cur;

    for (last = NULL, cur = wptr; cur; last = cur, cur = cur->Next)
	if (last)
	    free(last);
    if (last)
	free(last);
    return;
}

static void sFreePartialList(PARTIALLIST * lptr)
{
    PARTIALLIST    *last, *cur;

    for (last = NULL, cur = lptr; cur; last = cur, cur = cur->Next)
	if (last)
	    free(last);
    if (last)
	free(last);
    return;
}

static RETURN_STRUCT *sProcessWrkref(mocaDataRes * wres, mocaDataRow * wrow)
{
    long            ret_status, i, untcas, FoundPartialSpot;
    long            PartialSum, ProcessedPartial;
	/*
	 * a flag that indicate whether it does free operation on PartialList
	 */
	long            remove_flg = FALSE;

    char            buffer[1000];
    char            FieldList[1000];
    char            ValueList[1000];
    char            new_wrkref[WRKREF_LEN + 1];
    char            ucccod[UCCCOD_LEN + 1];
    char            LoadUCC[UCCCOD_LEN + 1];
    char            wrkref[WRKREF_LEN + 1];
    char            cmbcod[CMBCOD_LEN + 1];

    RETURN_STRUCT  *CurPtr;
    mocaDataRes    *res, *cres;
    mocaDataRow    *row, *crow;
    UCCINFO         uccInfo;

    WORKLIST       *WorkList, *NextWorkList;
    PARTIALLIST    *NextPartial, *LastPartial, *NewPartial;

    WorkList = NULL;

    memset(wrkref, 0, sizeof(wrkref));
    strncpy(wrkref, sqlGetValue(wres, wrow, "wrkref"), WRKREF_LEN);

    /* First, let's check to see if labeling is required */
    sprintf(buffer,
	    "get ucc128 label information where wrkref = '%s' "
        "    and wh_id = '%s' ",
	    wrkref,
        sqlGetString(wres, wrow, "wh_id"));

    CurPtr = NULL;
    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK)
    {
	if (ret_status == eDB_NO_ROWS_AFFECTED)
	{
	    srvFreeMemory(SRVRET_STRUCT, CurPtr);
	    return (srvResults(eOK, NULL));
	}
	else
	    return (CurPtr);
    }

    res = CurPtr->ReturnedData;
    row = sqlGetRow(res);

    /* Okay, now we can check the ucc level and verify we need processing */
    memset(&uccInfo, 0, sizeof(uccInfo));
    strncpy(uccInfo.ucclvl,
	    sqlGetValue(res, row, "ucclvl"), LODLVL_LEN);
    strncpy(uccInfo.uccseq,
	    sqlGetValue(res, row, "uccseq"), RTSTR1_LEN);
    strncpy(uccInfo.uccman,
	    sqlGetValue(res, row, "uccman"), RTSTR1_LEN);

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;
    if (0 == strcmp(uccInfo.ucclvl, NO_STR))
	return (srvResults(eOK, NULL));

    untcas = sqlGetLong(wres, wrow, "untcas");

    /* Okay, now let's gather up all the pckwrks 
       with this cmbcod and process them. */

    /*
     * Somthing changed here, we can not combine the pick
     * works here by combcod, because if the same combcod is 
     * assigned to differnt shimpment lines the other pick works
     * might be processed twice or more. 
     * See PR 50623.
     */ 

    sprintf(buffer,
	    "select * from pckwrk where cmbcod = '%s'",
	    misTrim(sqlGetValue(wres, wrow, "cmbcod")));

    if ((ret_status = sqlExecStr(buffer, &cres)) != eOK)
    {
	sqlFreeResults(cres);
	return (srvResults(ret_status, NULL));
    }


    /* If we're doing load level UCC labeling, then our 
       job is simple...we just buzz through and update all
       the pckwrks for this cmbcod to have a loducc... 
       Note - it doesn't much make sense to attempt to 
       put a loducc on a case pick - (I think) */

    if (0 == strcmp(uccInfo.ucclvl, LODLVL_LOAD) &&
	0 == strcmp(uccInfo.ucclvl, misTrim(sqlGetValue(wres,
							wrow, "lodlvl"))))
    {
	/* It's load level labeling...buzz through,
	   and see if we can get a loducc which is already 
	   on the pckwrk.... */
	memset(LoadUCC, 0, sizeof(LoadUCC));
	for (crow = sqlGetRow(cres); crow; crow = sqlGetNextRow(crow))
	{
	    if (!sqlIsNull(cres, crow, "loducc") &&
		misTrimLen(sqlGetValue(cres, crow, "loducc"), UCCCOD_LEN))
		strncpy(LoadUCC,
			sqlGetValue(cres, crow, "loducc"), UCCCOD_LEN);
	}
	if (0 == strlen(LoadUCC))
	{
	    /* Must generate a new UCC code.. */
        misTrc(T_FLOW, " JJS debug #10");
            ret_status = sGetNewUCC(&uccInfo, ucccod, NULL);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(cres);
		return (srvResults(ret_status, NULL));
	    }
	    strncpy(LoadUCC, ucccod, UCCCOD_LEN);
	}

	sprintf(buffer,
		"update pckwrk set loducc = '%s' where cmbcod = '%s'",
		LoadUCC, (char *) sqlGetValue(wres, wrow, "cmbcod"));
	ret_status = sqlExecStr(buffer, NULL);
	if (ret_status != eOK)
	{
	    sqlFreeResults(cres);
	    return (srvResults(ret_status, NULL));
	}

	/* Now let's build our return data... */
	CurPtr = NULL;
	for (crow = sqlGetRow(cres); crow; crow = sqlGetNextRow(crow))
	{
	    ret_status = sCreateReturn(&CurPtr, wrkref, LoadUCC,
				       sqlGetLong(cres,
							     crow, "pckqty"));
	    if (eOK != ret_status)
	    {
		sqlFreeResults(cres);
		return (srvResults(ret_status, NULL));
	    }
	}
	return (CurPtr);
    }

    /* We're stuck doing cases.... */

    WorkList = NULL;
    /* Load up our Work list */
    for (crow = sqlGetRow(cres); crow != NULL; crow = sqlGetNextRow(crow))
    {
	/* Skip Detail Picks */
	if (strcmp(misTrim(sqlGetValue(cres, crow, "lodlvl")),
		   LODLVL_DETAIL) == 0)
	    continue;

	NextWorkList = calloc(1, sizeof(WORKLIST));
	if (!NextWorkList)
	{
	    sqlFreeResults(cres), cres = NULL;
	    return (srvResults(eNO_MEMORY, NULL));
	}

	NextWorkList->wrkref = misTrim(sqlGetValue(cres, crow, "wrkref"));
	NextWorkList->lodlvl = misTrim(sqlGetValue(cres, crow, "lodlvl"));
	NextWorkList->pckqty = sqlGetLong(cres, crow, "pckqty");
	NextWorkList->untcas = sqlGetLong(cres, crow, "untcas");
	NextWorkList->Row = crow;
	NextWorkList->Res = cres;
	if (strlen(misTrim(sqlGetValue(cres, crow, "subucc"))) ||
	    strlen(misTrim(sqlGetValue(cres, crow, "loducc"))))
	    NextWorkList->remqty = 0;
	else
	    NextWorkList->remqty = NextWorkList->pckqty;

	NextWorkList->Next = WorkList;
	WorkList = NextWorkList;
    }

    if (WorkList == NULL)
    {
	sqlFreeResults(cres), cres = NULL;
	return (srvResults(eOK, NULL));
    }

    NextWorkList = WorkList;
    while (NextWorkList)
    {
	/*  If we've gotten here it means we're going to be splitting up
	   the pckwrks. So, we'll loop while we are still at even case qtys
	   splitting the pckwrk out by untcas. */

	for (i = NextWorkList->pckqty;
	     NextWorkList->remqty && i >= NextWorkList->untcas;
	     i = i - NextWorkList->untcas)
	{
	    if ((ret_status = appNextNum(NUMCOD_WRKREF, new_wrkref)) != eOK)
		return (srvResults(ret_status, NULL));

	    memset(cmbcod, 0, sizeof(cmbcod));
	    strncpy(cmbcod,
		    (char *) sqlGetValue(NextWorkList->Res,
					 NextWorkList->Row, "cmbcod"),
		    CMBCOD_LEN);

	    if (sGenerateSeparateCmbcods && !sFirstTime &&
                misTrimStrncmp(NextWorkList->lodlvl,
                                LODLVL_LOAD, LODLVL_LEN) != 0)
	    {
		memset(cmbcod, 0, sizeof(cmbcod));
		ret_status = appNextNum(NUMCOD_CMBCOD, cmbcod);
		if (ret_status != eOK)
		    return (srvResults(ret_status, NULL));
		ret_status = 
		    sDuplicatePckmovs((char *) sqlGetValue(NextWorkList->Res,
							   NextWorkList->Row,
							   "cmbcod"),
				      cmbcod);
		if (eOK != ret_status)
		    return (srvResults(ret_status, NULL));

	    }

	    sFirstTime = 0;

        misTrc(T_FLOW, " JJS debug #20");
	ret_status = sGetNewUCC(&uccInfo, ucccod, NextWorkList->wrkref);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(cres), cres = NULL;
		sFreeWorkList(WorkList);
		sFreeReturnStack();
		WorkList = NULL;
		return (srvResults(ret_status, NULL));
	    }

	    /* See if we are on the last case */
	    if (i == NextWorkList->untcas)
	    {
		sprintf(buffer,
			"update pckwrk "
			"   set %s = '%s', "
			"       pckqty = '%ld', "
			"       cmbcod = '%s' "
			" where wrkref = '%s'",
			!strcmp(uccInfo.ucclvl,
				LODLVL_SUBLOAD) ? "subucc" : "loducc",
			ucccod, i, cmbcod, NextWorkList->wrkref);

		if ((ret_status = sqlExecStr(buffer, NULL)) != eOK)
		{
		    sqlFreeResults(cres), cres = NULL;
		    sFreeWorkList(WorkList);
		    sFreeReturnStack();
		    WorkList = NULL;
		    return (srvResults(ret_status, NULL));
		}

		sAddToReturnStack(wrkref, ucccod, i);
	    }
	    else
	    {
		/* Setup the defaults for this entry */
		strcpy(FieldList, "wrkref, pckqty, subucc, cmbcod");
		sprintf(ValueList, "'%s', '%ld', '%s', '%s'",
			new_wrkref, NextWorkList->untcas, ucccod, cmbcod);

		ret_status = appGenerateTableEntry("pckwrk",
						   NextWorkList->Res,
						   NextWorkList->Row,
						   FieldList,
						   ValueList);
		if (ret_status != eOK)
		{
		    sqlFreeResults(cres), cres = NULL;
		    sFreeWorkList(WorkList);
		    sFreeReturnStack();
		    WorkList = NULL;
		    return (srvResults(ret_status, NULL));
		}

		sAddToReturnStack(new_wrkref,
				  ucccod,
				  NextWorkList->untcas);
	    }
	    NextWorkList->remqty -= NextWorkList->untcas;
	}
	NextWorkList = NextWorkList->Next;
    }

    /* Now, let's check for any remaining quantities and try and pack
       them up in single cases. First, let's resort the worklist by remqty */

    PartialList = NULL;
    NextWorkList = WorkList;

    do
    {
	if (NextWorkList->remqty == 0)
	    continue;

	FoundPartialSpot = FALSE;
	if (!PartialList)
	    PartialList = NewPartial = calloc(1, sizeof(PARTIALLIST));
	else
	{
	    LastPartial = NULL;
	    for (NextPartial = PartialList;
		 NextPartial && !FoundPartialSpot;
		 LastPartial = NextPartial, NextPartial = NextPartial->Next)
	    {
		if (NextWorkList->remqty < NextPartial->remqty)
		{
		    FoundPartialSpot = TRUE;
		    NewPartial = calloc(1, sizeof(PARTIALLIST));
		    if (NextPartial->Last)
			NextPartial->Last->Next = NewPartial;

		    NewPartial->Last = NextPartial->Last;
		    NextPartial->Last = NewPartial;
		    NewPartial->Next = NextPartial;
		    if (PartialList == NextPartial)
			PartialList = NewPartial;
		}
	    }

	    if (!FoundPartialSpot)
	    {
		NewPartial = calloc(1, sizeof(PARTIALLIST));
		if (LastPartial)
		{
		    LastPartial->Next = NewPartial;
		    NewPartial->Last = LastPartial;
		}
		NewPartial->Next = NULL;
	    }
	}
	NewPartial->remqty = NextWorkList->remqty;
	NewPartial->WorkPtr = NextWorkList;
    } while ((NextWorkList = NextWorkList->Next));

    while (PartialList)
    {
	if (!PartialList->Next)
	{
        misTrc(T_FLOW, " JJS debug #30");
            ret_status = sGetNewUCC(&uccInfo, ucccod, NULL);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(cres), cres = NULL;
		sFreeWorkList(WorkList);
		WorkList = NULL;
		sFreeReturnStack();
		sFreePartialList(PartialList);
		PartialList = NULL;
		return (srvResults(ret_status, NULL));
	    }

	    NextPartial = PartialList;

	    memset(cmbcod, 0, sizeof(cmbcod));
	    strncpy(cmbcod,
		    (char *) sqlGetValue(NextPartial->WorkPtr->Res,
					 NextPartial->WorkPtr->Row, "cmbcod"),
		    CMBCOD_LEN);

	    if (sGenerateSeparateCmbcods && !sFirstTime)
	    {
		memset(cmbcod, 0, sizeof(cmbcod));
		ret_status = appNextNum(NUMCOD_CMBCOD, cmbcod);
		if (ret_status != eOK)
		    return (srvResults(ret_status, NULL));

		ret_status = 
		    sDuplicatePckmovs((char *) 
				      sqlGetValue(NextPartial->WorkPtr->Res,
						  NextPartial->WorkPtr->Row, 
						  "cmbcod"),
				      cmbcod);
		if (eOK != ret_status)
		    return (srvResults(ret_status, NULL));
	    }

	    sFirstTime = 0;
	    sprintf(buffer,
		    "update pckwrk "
		    "   set subucc = '%s', "
		    "       pckqty = '%d', "
		    "       cmbcod = '%s' "
		    " where wrkref = '%s'",
		    ucccod, NextPartial->remqty,
		    cmbcod,
		    NextPartial->WorkPtr->wrkref);

	    if ((ret_status = sqlExecStr(buffer, NULL)) != eOK)
	    {
		sqlFreeResults(cres), cres = NULL;
		sFreeWorkList(WorkList);
		WorkList = NULL;
		sFreeReturnStack();
		sFreePartialList(PartialList);
		PartialList = NULL;

		return (srvResults(ret_status, NULL));
	    }

	    sAddToReturnStack(NextPartial->WorkPtr->wrkref,
			      ucccod,
			      NextPartial->remqty);
	    free(PartialList);
	    PartialList = NULL;
	    continue;
	}

	PartialSum = 0;
	for (NextPartial = PartialList; NextPartial != NULL;)
	{
	    PartialSum += NextPartial->remqty;

	    ProcessedPartial = FALSE;
	    for (NewPartial = NextPartial->Next; NewPartial != NULL;
		 NewPartial = NewPartial->Next)
	    {
		if (NewPartial->remqty + PartialSum >= untcas)
		{
		    ret_status = sDoSplit(NextPartial, NewPartial,
					  untcas, &uccInfo);
		    if (ret_status != eOK)
		    {
			sqlFreeResults(cres), cres = NULL;
			sFreeWorkList(WorkList);
			WorkList = NULL;
			sFreePartialList(PartialList);
			PartialList = NULL;
			sFreeReturnStack();
			return (srvResults(ret_status, NULL));
		    }
		    ProcessedPartial = TRUE;
		    break;
		}
	    }

	    if (ProcessedPartial == TRUE)
		break;

	    /* Otherwise we have some partials 
	       which don't add up to a untcas */

        misTrc(T_FLOW, " JJS debug #40");
            ret_status = sGetNewUCC(&uccInfo, ucccod, NULL);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(cres), cres = NULL;
		sFreeReturnStack();
		sFreeWorkList(WorkList);
		WorkList = NULL;
		sFreePartialList(PartialList);
		PartialList = NULL;
		return (srvResults(ret_status, NULL));
	    }
	    memset(cmbcod, 0, sizeof(cmbcod));
	    strncpy(cmbcod,
		    (char *) sqlGetValue(NextPartial->WorkPtr->Res,
					 NextPartial->WorkPtr->Row, "cmbcod"),
		    CMBCOD_LEN);
	    
	    if (sGenerateSeparateCmbcods && !sFirstTime)
	    {
		memset(cmbcod, 0, sizeof(cmbcod));
		ret_status = appNextNum(NUMCOD_CMBCOD, cmbcod);
		if (ret_status != eOK)
		    return (srvResults(ret_status, NULL));
		ret_status = 
		    sDuplicatePckmovs((char *) 
				      sqlGetValue(NextPartial->WorkPtr->Res,
						  NextPartial->WorkPtr->Row, 
						  "cmbcod"),
				      cmbcod);
		if (eOK != ret_status)
		    return (srvResults(ret_status, NULL));
	    }
	    sFirstTime = 0;
		remove_flg = FALSE;

	    for (NewPartial = NextPartial; NewPartial != NULL;)
	    {
		sprintf(buffer,
			"update pckwrk "
			"    set subucc = '%s', "
			"        pckqty = '%d', "
			"        cmbcod = '%s' "
			"  where wrkref = '%s'", ucccod,
			NewPartial->remqty, cmbcod,
			NewPartial->WorkPtr->wrkref);

		if ((ret_status = sqlExecStr(buffer, NULL)) != eOK)
		{
		    sqlFreeResults(cres), cres = NULL;
		    sFreeReturnStack();
		    sFreeWorkList(WorkList);
		    WorkList = NULL;
		    sFreePartialList(PartialList);
		    PartialList = NULL;
		    return (srvResults(ret_status, NULL));
		}


		sAddToReturnStack(NewPartial->WorkPtr->wrkref,
				  ucccod,
				  NewPartial->remqty);
		if (NewPartial == PartialList)
		{
			NewPartial = NewPartial->Next;
		    free(PartialList);
		    PartialList = NewPartial;
			remove_flg = TRUE;
		}
		else
		{
			NewPartial = NewPartial->Next;
		}
	    }

		/*
		 * if it does free operation on PartialList,
		 * we should use PartialList to replace NextPartial,
		 * otherwise it will cause core dump due to memory violate
		 */
		if(remove_flg == TRUE)
		{
			NextPartial = PartialList;
		}
		else
		{
			NextPartial = NextPartial->Next;
		}
	}
    }

    sFreeWorkList(WorkList);
    WorkList = NULL;
    /* Now we must build a return... */
    CurPtr = NULL;
    ret_status = sCreateReturnFromStack(&CurPtr);
    if (ret_status != eOK)
    {
	sFreeReturnStack();
	return (srvResults(ret_status, NULL));
    }
    return (CurPtr);
}

static long sDoSplit(PARTIALLIST * CurrentPartial,
		     PARTIALLIST * NewPartial,
		     long untcas,
		     UCCINFO * uccInfo_p)
{
    long            untsum, ret_status, Done;

    char            ucccod[UCCCOD_LEN + 1], FieldList[1000], ValueList[1000];
    char            new_wrkref[WRKREF_LEN + 1], buffer[1000];
    char            cmbcod[CMBCOD_LEN + 1];

    PARTIALLIST    *NextPartial, *LastPartial;

        misTrc(T_FLOW, " JJS debug #50");
    ret_status = sGetNewUCC(uccInfo_p, ucccod, NULL);
    if (ret_status != eOK)
	return (ret_status);

    memset(cmbcod, 0, sizeof(cmbcod));
    strncpy(cmbcod,
	    (char *) sqlGetValue(NewPartial->WorkPtr->Res,
				 NewPartial->WorkPtr->Row, "cmbcod"),
	    CMBCOD_LEN);

    if (sGenerateSeparateCmbcods && !sFirstTime)
    {
	memset(cmbcod, 0, sizeof(cmbcod));
	ret_status = appNextNum(NUMCOD_CMBCOD, cmbcod);
	if (ret_status != eOK)
	    return (ret_status);
	ret_status = 
	    sDuplicatePckmovs((char *) sqlGetValue(NewPartial->WorkPtr->Res,
						   NewPartial->WorkPtr->Row, 
						   "cmbcod"),
			      cmbcod);
	if (eOK != ret_status)
	    return (ret_status);

    }
    sFirstTime = 0;

    /* This is the last partial we have */

    if (!NewPartial->Next)
    {
	sprintf(buffer,
		"update pckwrk "
		"   set subucc = '%s', "
		"       pckqty = '%d', "
		"       cmbcod = '%s' "
		" where wrkref = '%s'",
		ucccod,
		NewPartial->remqty,
		cmbcod,
		NewPartial->WorkPtr->wrkref);

	if ((ret_status = sqlExecStr(buffer, NULL)) != eOK)
	    return (ret_status);

	sAddToReturnStack(NewPartial->WorkPtr->wrkref,
			  ucccod,
			  NewPartial->remqty);

       /* If there is a remainder from a previous split, 
        * the record needs to be updated
        */
	if (NewPartial != CurrentPartial)
	{
	    sprintf(buffer,
		"update pckwrk "
		"   set subucc = '%s', "
		"       cmbcod = '%s', "
		"       pckqty = '%d' "
		" where wrkref = '%s'",
		ucccod, cmbcod,
		CurrentPartial->remqty,
		CurrentPartial->WorkPtr->wrkref);

	    if ((ret_status = sqlExecStr(buffer, NULL)) != eOK)
		return (ret_status);

	    sAddToReturnStack(CurrentPartial->WorkPtr->wrkref,
			  ucccod,
			  CurrentPartial->remqty);
	}

	free(PartialList);
	PartialList = NULL;
	return (eOK);
    }

    untsum = 0;
    LastPartial = NULL;
    Done = FALSE;
    for (NextPartial = PartialList; NextPartial && !Done;
	 NextPartial = NextPartial->Next)
    {
	do
	{
	    if (NextPartial == NewPartial)
		Done = TRUE;

	    if (untsum + NextPartial->remqty <= untcas)
	    {

		/* If there is a remainder from a previous split, 
		 * the record needs to be updated
		 */
		if (NewPartial != CurrentPartial)
		{
		    sprintf(buffer,
			"update pckwrk "
			"   set subucc = '%s', "
			"       cmbcod = '%s', "
			"       pckqty = '%d' "
			" where wrkref = '%s'",
			ucccod, cmbcod,
			CurrentPartial->remqty,
			CurrentPartial->WorkPtr->wrkref);

		    if ((ret_status = sqlExecStr(buffer, NULL)) != eOK)
			return (ret_status);

		    sAddToReturnStack(CurrentPartial->WorkPtr->wrkref,
				  ucccod,
				  CurrentPartial->remqty);
		}
		untsum += NextPartial->remqty;
		sprintf(buffer,
			"update pckwrk "
			"   set subucc = '%s', "
			"       cmbcod = '%s', "
			"       pckqty = '%d' "
			" where wrkref = '%s'",
			ucccod, cmbcod,
			NextPartial->remqty,
			NextPartial->WorkPtr->wrkref);

		if ((ret_status = sqlExecStr(buffer, NULL)) != eOK)
		    return (ret_status);

		sAddToReturnStack(NextPartial->WorkPtr->wrkref,
				  ucccod,
				  NextPartial->remqty);

		if (NextPartial->Last)
		{
		    NextPartial->Last->Next = NextPartial->Next;
		    if (NextPartial->Next)
			NextPartial->Next->Last = NextPartial->Last;
		}

		if (NextPartial == PartialList)
		{
		    PartialList = NextPartial->Next;
		    if (PartialList)
			PartialList->Last = NULL;
		}

		free(NextPartial);
	    }
	    else
	    {
		if ((ret_status = appNextNum(NUMCOD_WRKREF,
					     new_wrkref)) != eOK)
		    return (ret_status);

		/* Setup the defaults for this entry */
		strcpy(FieldList, "wrkref, pckqty, subucc, cmbcod");
		sprintf(ValueList, "'%s', '%ld', '%s', '%s'",
			new_wrkref,
			(untcas - untsum), ucccod, cmbcod);

		ret_status = appGenerateTableEntry("pckwrk",
						   NextPartial->WorkPtr->Res,
						   NextPartial->WorkPtr->Row,
						   FieldList,
						   ValueList);

		if (ret_status != eOK)
		    return (ret_status);

		sAddToReturnStack(new_wrkref, ucccod,
				  untcas - untsum);
		NextPartial->remqty = NextPartial->remqty - (untcas - untsum);
		return (eOK);
	    }

	    if (NextPartial == CurrentPartial)
		NextPartial = NewPartial;
	    else
		NextPartial = NextPartial->Next;

	}
	while (!Done);
    }
    return (eOK);
}

/* This is a little function which manages the returning data */
static long sAddToReturnStack(char *wrkref, char *ucccod, long pckqty)
{
    RETURN_PLACEHOLDER *tmp;

    tmp = (RETURN_PLACEHOLDER *) calloc(1, sizeof(RETURN_PLACEHOLDER));
    if (!tmp)
	return (eNO_MEMORY);

    strncpy(tmp->wrkref, wrkref, WRKREF_LEN);
    strncpy(tmp->ucccod, ucccod, UCCCOD_LEN);
    tmp->pckqty = pckqty;

    tmp->next = ReturnPlaceHolderList;
    ReturnPlaceHolderList = tmp;
    return (eOK);
}

static void sFreeReturnStack()
{
    RETURN_PLACEHOLDER *last, *tmp;

    last = NULL;
    for (tmp = ReturnPlaceHolderList; tmp; last = tmp, tmp = tmp->next)
	if (last)
	    free(last);

    if (last)
	free(last);

    ReturnPlaceHolderList = NULL;
}

static long sCreateReturnFromStack(RETURN_STRUCT ** CurPtr)
{
    RETURN_PLACEHOLDER *tmp;
    long            ret_status;

    *CurPtr = NULL;

    for (tmp = ReturnPlaceHolderList; tmp; tmp = tmp->next)
    {
	ret_status = sCreateReturn(CurPtr,
				   tmp->wrkref, tmp->ucccod, tmp->pckqty);
	if (ret_status != eOK)
	    return (ret_status);
    }
    return (eOK);
}

static long sCreateReturn(RETURN_STRUCT ** CurPtr,
			  char *wrkref,
			  char *ucccod,
			  long pckqty)
{
    int             i;
    static char     datatypes[20];
    static mocaDataRow *currow;

    if (!*CurPtr)
    {
	i = 0;
	currow = NULL;
	memset(datatypes, 0, sizeof(datatypes));
	srvSetupColumns(3);
	datatypes[i] =
	    srvSetColName(i + 1, "wrkref", COMTYP_CHAR, WRKREF_LEN);
	i++;
	datatypes[i] =
	    srvSetColName(i + 1, "ucccod", COMTYP_CHAR, UCCCOD_LEN);
	i++;
	datatypes[i] =
	    srvSetColName(i + 1, "pckqty", COMTYP_INT, sizeof(int));
	i++;

	*CurPtr = srvSetupReturn(eOK,
				 datatypes,
				 wrkref,
				 ucccod,
				 pckqty);
	if (NULL == *CurPtr)
	    return (eNO_MEMORY);

	currow = sqlGetRow((*CurPtr)->ReturnedData);
    }
    else
    {
	currow = srvAddToReturn(*CurPtr,
				currow,
				datatypes,
				wrkref,
				ucccod,
				pckqty);
	if (NULL == currow)
	    return (eNO_MEMORY);
    }
    return (eOK);
}

static long sGetNewUCC(UCCINFO * uccInfo_p, char *ucccod, char *wrkref)
{
    char            buffer[1000];

    long            ret_status;

    RETURN_STRUCT  *CurPtr;
    mocaDataRes     *Res;
    mocaDataRow    *Row;

    sprintf(buffer,
	    "generate ucc128 identifier where ucclvl = \"%s\" "
	    "and uccseq = \"%s\" and uccman = \"%s\" "
	    "and sumflg = \"%ld\" and wrkref = \"%s\" ",
	    uccInfo_p->ucclvl,
	    uccInfo_p->uccseq,
	    uccInfo_p->uccman,
	    BOOLEAN_TRUE,
            wrkref);

    CurPtr = NULL;
    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK)
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
	return (ret_status);
    }

    Res = CurPtr->ReturnedData;
    Row = sqlGetRow(Res);
    strcpy(ucccod,
	   !strcmp(uccInfo_p->ucclvl, LODLVL_SUBLOAD) ?
	   misTrim(sqlGetValue(Res, Row, "subucc")) :
	   misTrim(sqlGetValue(Res, Row, "loducc")));

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    return (eOK);
}

static long sDuplicatePckmovs(char *oldcmbcod, char *newcmbcod)
{
    char            buffer[2000];
    mocaDataRes    *res;
    mocaDataRow    *row;
    char            values[60];

    long            ret_status;

    sprintf(buffer,
	    "select * from pckmov where cmbcod = '%s'", oldcmbcod);

    ret_status = sqlExecStr(buffer, &res);
    if (eOK != ret_status)
    {
	sqlFreeResults(res);
	return (ret_status);
    }

    sprintf(values, "'%s'", newcmbcod);
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	ret_status = appGenerateTableEntry("pckmov",
					   res,
					   row,
					   "cmbcod",
					   values);
	if (eOK != ret_status)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}
    }
    return (eOK);
}
