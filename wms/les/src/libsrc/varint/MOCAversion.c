static char RCS_Id[] = "$Id: MOCAversion.c 51215 2000-02-03 16:15:30Z mlange $";
/*#START***********************************************************************
 *
 *  $URL: https://athena.redprairie.com/svn/prod/env/tags/2008.1.2/src/libsrc/varint/MOCAversion.c $
 *  $Revision: 51215 $
 *  $Author: mlange $
 *
 *  Description: Return the release version or build date of this 
 *               component library.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <string.h>
#include <time.h>

#ifndef RELEASE_VERSION
#define RELEASE_VERSION ""
#endif

LIBEXPORT char *MOCAversion(void)
{
    if (RELEASE_VERSION[0] == '\0')
        return "Build Date: " __DATE__;
    else
        return RELEASE_VERSION;
}
