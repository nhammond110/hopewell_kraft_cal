static char    *rcsid = "$Id: intListAvailableShipments.c 165645 2008-08-06 19:42:12Z pflanzer $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2004 RedPrairie Corporation.  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <common.h>
#include "intlib.h"

#define DEF_WORK_ORDER_TYPE "I"
#define DEF_WORK_ORDER_TEMPLATE_TYPE "T"

/*
 * HISTORY
 * JJS 03/11/2009 - Adding address name to output so the user can group by
 *                  and see more easily which shipments are going where.
 *
 */

static mocaDataRes	*shColumns = NULL;
static mocaDataRes	*slColumns = NULL;
static mocaDataRes	*ohColumns = NULL;
static mocaDataRes	*olColumns = NULL;
static mocaDataRes      *stpColumns = NULL;
static mocaDataRes      *cstColumns = NULL;

static long sInitialize()
{
    long ret_status;
    char buffer[1000];

    if (!shColumns)
    {
	sprintf(buffer,
		" select * "
		"   from shipment "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &shColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(shColumns);
	    shColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, shColumns);
    }

    if (!slColumns)
    {
	sprintf(buffer,
		" select * "
		"   from shipment_line "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &slColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
            sqlFreeResults(shColumns);
	    sqlFreeResults(slColumns);
	    slColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, slColumns);
    }

    if (!ohColumns)
    {
	sprintf(buffer,
		" select * "
		"   from ord "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &ohColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
            sqlFreeResults(shColumns);
            sqlFreeResults(slColumns);
	    sqlFreeResults(ohColumns);
	    ohColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, ohColumns);
    }

    if (!olColumns)
    {
	sprintf(buffer,
		" select * "
		"   from ord_line "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &olColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
            sqlFreeResults(shColumns);
            sqlFreeResults(slColumns);
            sqlFreeResults(ohColumns);
	    sqlFreeResults(olColumns);
	    olColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, olColumns);
    }

    if (!stpColumns)
    {
        sprintf(buffer,
                " select * "
                "   from stop "
                "  where 1 = 2 " );

        ret_status = sqlExecStr(buffer, &stpColumns);
        if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(shColumns);
            sqlFreeResults(slColumns);
            sqlFreeResults(ohColumns);
            sqlFreeResults(olColumns);
            sqlFreeResults(stpColumns);
            stpColumns = NULL;
            return (ret_status);
        }
        misFlagCachedMemory((OSFPTR) sqlFreeResults, stpColumns);
    }

    if (!cstColumns)
    {
        sprintf(buffer,
                " select * "
                "   from cstmst "
                "  where 1 = 2 " );

        ret_status = sqlExecStr(buffer, &cstColumns);
        if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(shColumns);
            sqlFreeResults(slColumns);
            sqlFreeResults(ohColumns);
            sqlFreeResults(olColumns);
            sqlFreeResults(stpColumns);
            sqlFreeResults(cstColumns);
            cstColumns = NULL;
            return (ret_status);
        }
        misFlagCachedMemory((OSFPTR) sqlFreeResults, cstColumns);
    }

    return (eOK);
} /* End intialize */

static void sFormatWhere(char *buffer,
			 char *table,
			 char *argname,
			 int  oper,
			 void *argdata,
			 char argtype,
			 char dbType)
{
    char temp1[200];
    char temp2[200];

    memset (temp1, 0, sizeof(temp1));
    memset (temp2, 0, sizeof(temp2));

    if (appDataToString(argdata, argtype, temp1) != eOK)
	return;

    /* Convert to date if we need to */
    if (dbType == COMTYP_DATTIM)
    {
        sprintf(temp2,
	        "to_date('%s')",
		temp1);
        strcpy(temp1, temp2);
	memset(temp2, 0, sizeof(temp2));
    }

    sprintf(buffer, " and %s.%s ", table, argname);
    switch (oper)
    {
        case OPR_NOTNULL:
	    strcpy(temp2, "is not null ");
	    break;
        case OPR_ISNULL:
	    strcpy(temp2, "is null ");
	    break;
        case OPR_EQ:
	    sprintf(temp2, argtype == COMTYP_STRING &&
		    dbType != COMTYP_DATTIM ? " = '%s' " : " = %s ", temp1);
	    break;
        case OPR_NE:
	    sprintf(temp2, argtype == COMTYP_STRING &&
		    dbType != COMTYP_DATTIM ? " != '%s' " : " != %s ", temp1);
	    break;
        case OPR_LT:
	    sprintf(temp2, argtype == COMTYP_STRING &&
		    dbType != COMTYP_DATTIM ? " < '%s' " : " < %s ", temp1);
	    break;
        case OPR_LE:
	    sprintf(temp2, argtype == COMTYP_STRING &&
		    dbType != COMTYP_DATTIM ? " <= '%s' " : " <= %s ", temp1);
	    break;
        case OPR_GT:
	    sprintf(temp2, argtype == COMTYP_STRING &&
		    dbType != COMTYP_DATTIM ? " > '%s' " : " > %s ", temp1);
	    break;
        case OPR_GE:
	    sprintf(temp2, argtype == COMTYP_STRING &&
		    dbType != COMTYP_DATTIM ? " >= '%s' " : " >= %s ", temp1);
	    break;
        case OPR_LIKE:
	    sprintf(temp2, " like '%s%%' ", temp1);
	    break;
        case OPR_NAMECLAUSE:
            sprintf(temp2, " %s ", temp1);
            break;
        default:
            break;
    }
    strcat(buffer, temp2);
} /* Format Where */

static void sSetExclusions(RETURN_STRUCT *CurPtr)
{
    char buffer[500];
    char tmpbuf[100];
    char selClause[100];
    long ret_status;
    mocaDataRes *res, *dres;
    mocaDataRow *row, *drow;
    char colnam[COLNAM_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    char wh_id[WH_ID_LEN+1];
    char *val;
    char *tmpptr;
    int header;
    int oper;

    dres = CurPtr->ReturnedData;

    for (drow = sqlGetRow(dres);drow; drow = sqlGetNextRow(drow))
    {
        sqlSetString(dres,drow,"pckexc"," ");
    }


    sprintf(buffer,
	    "select lower(colnam) colnam, colval, client_id "
	    "  from pckexc "
	    " where untdte >= sysdate or untdte is null "
	    " order by wh_id, colnam ");

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
        return;

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        header = 0;
	memset(colnam, 0, sizeof(colnam));
	misTrimcpy(colnam, sqlGetValue(res, row, "colnam"), COLNAM_LEN);

	memset(client_id, 0, sizeof(client_id));
	if (!sqlIsNull(res, row, "client_id"))
	    strncpy(client_id,
		    sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);

        memset(wh_id, 0, sizeof(wh_id));
        if (!sqlIsNull(res, row, "wh_id"))
            strncpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);

	/* Let's see if our exception is a header or detail exception */
	header = sqlFindColumn(dres, colnam);

	val = sqlGetString(res,row,"colval");
	if (!val)
	    return;

	/* if it's a header, we should have the info in our select */
	if (header > 0)
	{
	    if (!sqlIsNull(dres, dres->Data,colnam))
	    {
	        for (drow = sqlGetRow(dres);drow; drow = sqlGetNextRow(drow))
	        {
		    if ( misTrimLen(sqlGetString(dres, drow, "pckexc"),1) )
                        /* If there is already an exclusion set we don't need
                         * to do anything more here.
                         */
		        continue;

		    tmpptr = sqlGetString(dres, drow, colnam);

		    if ((tmpptr) &&
		        (appCmpStringWithWilds(tmpptr, val) == eOK))
		    {
                         if (strlen(wh_id) && appCmpStringWithWilds(tmpptr, wh_id) == eOK)
                             *(sqlGetStringByPos(dres, drow, 0)) = EXCLUDED;
			else if (strlen(client_id))
			{
			         tmpptr = sqlGetString(dres, drow, "client_id");

			         /* TODO
			          * if row/value matches pattern,
			          * then set first column (PCKEXC) value to EXCLUDED
			          * Should use new MOCA sqllib function to set
			          * value, when it is available
			          */
			         if (appCmpStringWithWilds(tmpptr, client_id) == eOK)
                                     sqlSetString(dres,drow,"pckexc",
                                                          PCKEXC_EXCLUDED);
				 /*
                                  *
                                  *(sqlGetStringByPos(dres,drow,0))= EXCLUDED;
                                  */
			}
			else
			{
			    /* TODO */
                            sqlSetString(dres, drow, "pckexc", PCKEXC_EXCLUDED);
                            /*
                             *
                             *(sqlGetStringByPos(dres,drow,0))= EXCLUDED;
                             */
			}
		    }
		}
	    }
	}

	/* It's a detail exclsuion, we'll need to look it up */
	else
	{
	    /* If we're excluding by detailed exception, the
	       assumption we use is that a client_id specified
	       in the exclusion is associated with the prt_client_id...
	       this may be a problem...and if it is, we probably
	       need to call out the two specific client fields
	       for exclusions...for now, we go with the assumption */

	    for (drow = sqlGetRow(dres); drow; drow = sqlGetNextRow(drow))
	    {
                if ( misTrimLen(sqlGetString(dres, drow, "pckexc"),1) )
                    /* If there is already an exclusion set we don't need
                     * to do anything more here.
                     */
                    continue;

		memset(tmpbuf, 0, sizeof(tmpbuf));
	        if (strchr(val, '%')) /* This means it's a like */
	            oper = OPR_LIKE;
	        else
	            oper = OPR_EQ;

		if (sqlFindColumn(shColumns, colnam) != -1)
		{
		  /* We've found our argument in the shipment table */
		  sFormatWhere(tmpbuf, "h", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(shColumns,
						   sqlFindColumn(shColumns,
								 colnam)));
		  sprintf(selClause, "h.%s", colnam);
		}
		else if (sqlFindColumn(slColumns, colnam) != -1)
		{
		  /* We've found our argument in the shipment_line table */
		  sFormatWhere(tmpbuf, "d", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(slColumns,
						   sqlFindColumn(slColumns,
								 colnam)));
		  sprintf(selClause, "d.%s", colnam);
		}
		else if (sqlFindColumn(ohColumns, colnam) != -1)
		{
		  /* We've found our argument in the ord table */
		  sFormatWhere(tmpbuf, "o", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(ohColumns,
						   sqlFindColumn(ohColumns,
								 colnam)));
		  sprintf(selClause, "o.%s", colnam);
		}
		else if (sqlFindColumn(olColumns, colnam) != -1)
		{
		  /* We've found our argument in the ord_liner table */
		  sFormatWhere(tmpbuf, "ol", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(olColumns,
						   sqlFindColumn(olColumns,
								 colnam)));
		  sprintf(selClause, "ol.%s", colnam);
		}
		else
		{
		    /* Column is not in any of the 4 tables.
		     * Can not use it for selection criteria
		     */
		    sprintf(selClause, "'bad_pckexc' ");
		}


		sprintf(buffer,
		        "select %s "
			" from shipment h, shipment_line d, ord_line ol, ord o"
			" where d.ship_id  = '%s' "
			"   and h.ship_id = d.ship_id "
			"   and d.client_id = o.client_id "
			"   and d.wh_id  = o.wh_id       "
			"   and d.ordnum = o.ordnum       "
			"   and d.client_id = ol.client_id "
			"   and d.wh_id  = ol.wh_id       "
			"   and d.ordnum = ol.ordnum       "
			"   and d.ordlin = ol.ordlin "
			"   and d.ordsln = ol.ordsln ",
			selClause,
			sqlGetString(dres,drow,"ship_id"));

		/* TODO */
	        strcat(buffer, tmpbuf);

	        if (strlen(client_id))
	        {
	            if (strchr(client_id, '%')) sprintf(tmpbuf,
                         " and prt_client_id like '%s' ", client_id);
	            else
		        sprintf(tmpbuf, " and prt_client_id = '%s' ",
				client_id);

	            strcat(buffer, tmpbuf);
	        }

                if (strlen(wh_id))
                {
                    if (strchr(wh_id, '%'))
                        sprintf(tmpbuf, " and wh_id like '%s' ", wh_id);
                    else
                        sprintf(tmpbuf, " and wh_id = '%s' ", wh_id);

                    strcat(buffer, tmpbuf);
                }

  	        ret_status = sqlExecStr(buffer, NULL);
	        if (ret_status == eOK)
	        {
		    /* TODO */
                    sqlSetString(dres, drow, "pckexc", PCKEXC_EXCLUDED_LINE );
                    /*
                     *
	            *(sqlGetStringByPos(dres, drow, 0)) = EXCLUDED_LINE;
                     */
  	        }
            }
	}
    }
    sqlFreeResults(res);
} /* end sFormatExclusions */

/* Function for replacing. */
static void replace(char *strIn, char * oldstr, char * newstr)
{
	int sl = (int)strlen(strIn);
    int ol = (int)strlen(oldstr);
    int nl = (int)strlen(newstr);
    int pl, tl;

	char *p = strstr(strIn, oldstr);
	while(p){
		pl = (int)strlen(p);
		tl = sl - pl;
		memset(strIn+tl, 0x0, ol);
		memmove(strIn+tl+nl, &strIn[tl+ol], pl);
		memcpy(strIn+tl, newstr, nl);
		strIn += tl + nl;
		sl = (int)strlen(strIn);
		p = strstr(strIn, oldstr);
	}
}

/* Function for supporting "||" operation in C file */
static char *sSupprtOrOperation(char *argData)
{
    char argname[100];
    char tmpstr[1000];
    char table[2];
    char *retbuffer = NULL;
    char p[1000];
    int startcopy = 0;
    int i, j = 0;

    memset(tmpstr,0,sizeof(tmpstr));
    memset(table,0,sizeof(table));
    memset(argname,0,sizeof(argname));
    memset(p,0,sizeof(p));

    strncpy(p,argData, strlen(argData));

    /* Get the argument name from argData string first.
     * Because at first the argument name is "where".
     * We should get the true field name.
     *
     * Do a for operation, if find the number or letter,
     * record it, or skip it.
     * If find a non-number or non-letter again, stop it.
     * Then we get the field name
     */
    for (i= 0; i < (int)strlen(p) ; i++ )
    {
        if ((p[i] >= 'a' && p[i] <= 'z') ||
            (p[i] == '_') ||
            (p[i] > 0 && p[i] < 9))
        {
            argname[j] = p[i];
            startcopy = 1;
            j++;
        }
        else
        {
            if (startcopy == 1)
            {
                break;
            }
        }
    }

    /* Validate which table is this argument in.
     * Then rebuild the field name, using table name
     * as a prefix. I.E. "ship_id" should be "h.ship_id".
     * Then replace the "ship_id" with "h.ship_id", and
     * build a new sql buffer.
     * Return it.
     */
    if (argname != NULL)
    {
        if (sqlFindColumn(shColumns, argname) != -1)
            strcpy(table,"h");
        else if (sqlFindColumn(slColumns, argname) != -1)
            strcpy(table,"d");
        else if (sqlFindColumn(ohColumns, argname) != -1)
            strcpy(table,"o");
        else if (sqlFindColumn(olColumns, argname) != -1)
            strcpy(table,"ol");
        else if (sqlFindColumn(stpColumns, argname) != -1)
            strcpy(table,"h");
        else if (sqlFindColumn(cstColumns, argname) != -1)
            strcpy(table,"c");
    }

    sprintf(tmpstr, "%s.%s", table,argname);
    replace(p,argname,tmpstr);

    sprintf(tmpstr, "and %s", p);

    retbuffer = tmpstr;

    return retbuffer;
}

LIBEXPORT
RETURN_STRUCT *varListAvailableShipments(void)
{
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;

    long	  ret_status;
    char	  buffer[10000];
    char	  where_list[15000];
    char	  table_list[200];
    char	  field_list[5000];
    char      client_list[5000];
    char          *arg_ptr;
    char          usr_id[USR_ID_LEN + 1];
    char          wh_id[WH_ID_LEN+1];
    RETURN_STRUCT *CurPtr;

    /* For srvEnumerateArgs */
    char	 argname[ARGNAM_LEN + 1];
    int 	 oper;
    void	 *argdata;
    char	 argtype;
    SRV_ARGSLIST *CTX;

    memset(table_list, 0, sizeof(table_list));
    memset(where_list, 0, sizeof(where_list));
    memset(field_list, 0, sizeof(field_list));
    memset(client_list, 0, sizeof(client_list));
    memset(wh_id, 0, sizeof(wh_id));
    memset(usr_id, 0, sizeof(usr_id));

    if (!shColumns || !slColumns || !ohColumns ||
	!olColumns || !stpColumns || !cstColumns)
    {
	ret_status = sInitialize();
	if (ret_status != eOK)
	    return (srvResults(ret_status, NULL));
    }

    /* We have some required fields to pass back */
    /* TODO - should order header fields be returned? */
    /* JJS - added a.adrnam */
    strcpy(field_list,
           " a.adrnam, o.wh_id, h.ship_id, o.client_id, "
           " h.host_client_id, h.host_ext_id, "
           " h.shpsts, h.rt_adr_id, "
           " h.stop_id, h.car_move_id, "
	   " h.cargrp, h.carcod, "
	   " h.srvlvl, sum(d.pckqty) pckqty, "
	   " h.sddflg, h.doc_num, h.track_num, "
	   " h.frtchg, h.frtcod, h.frtrte, h.adddte, "
	   " h.alcdte, h.stgdte, h.loddte, h.entdte, "
	   " h.early_shpdte, h.late_shpdte, "
	   " h.early_dlvdte, h.late_dlvdte, "
	   " h.rrlflg ");

    /* Now we spin through our where clause... */
    CTX = NULL;
    while(eOK == srvEnumerateArgList(&CTX, argname, &oper, &argdata, &argtype))
    {
        buffer[0] = '\0';
        /* PR 39078
         * When user input ||, like "ship_id=SHP001 || ship_id=SHP002" in GUI
         * the MCS will convert it to
         * "where = ( ship_id=SHP001 OR ship_id=SHP002 )".
         * Local syntax call support it ,but not C files.
         * We need to support the || operation in C files.
         * So I added a function sSupprtOrOperation.
         * It will analyze the special sentence and change it,
         * let it be a normal SQL sentence.
         * Then it will work well.
         */

        misTrc(T_FLOW, "Checking for || arguments");
        if (!misCiStrcmp(argname, "where"))
        {
            strcat(where_list,sSupprtOrOperation(argdata));
        }

        /* get wh_id for validate client id */
        if (!misCiStrcmp(argname, "wh_id"))
        {
            appDataToString(argdata, argtype, wh_id);
        }

	if (sqlFindColumn(shColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment table */
	    sFormatWhere(buffer, "h", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(shColumns,
					     sqlFindColumn(shColumns,
							   argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
        }
	else if (sqlFindColumn(slColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment_line table */
	    sFormatWhere(buffer, "d", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(slColumns,
					     sqlFindColumn(slColumns,
							   argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
	}
	else if (sqlFindColumn(ohColumns, argname) != -1)
	{
	    /* We've found our argument in the ord table */
	    sFormatWhere(buffer, "o", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(ohColumns,
					     sqlFindColumn(ohColumns,
							   argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
	}
	else if (sqlFindColumn(olColumns, argname) != -1)
	{
	    /* We've found our argument in the ord_liner table */
	    sFormatWhere(buffer, "ol", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(olColumns,
					     sqlFindColumn(olColumns,
	    						   argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
        }
        else if (sqlFindColumn(stpColumns, argname) != -1)
        {
            /* We've found our argument in the stop table */
            sFormatWhere(buffer, "h", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(stpColumns,
                                             sqlFindColumn(stpColumns,
                                                           argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
	}
        else if (sqlFindColumn(cstColumns, argname) != -1)
        {
            /* We've found our argument in the cstmst table */
            sFormatWhere(buffer, "c", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(cstColumns,
                                             sqlFindColumn(cstColumns,
                                                           argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
	}
    	else
	{

	    /* See if the column is either a from date
	     * or a to date
     	     */
	    if (strncmp(argname, FROM_DATE_PREFIX, FROM_DATE_PREFIX_LEN) == 0)
	    {
		/* change the argname */
		arg_ptr = argname;
	        strcpy(argname, arg_ptr + FROM_DATE_PREFIX_LEN);
		oper = OPR_GE;
	    }
	    else if (strncmp(argname, TO_DATE_PREFIX, TO_DATE_PREFIX_LEN) == 0)
	    {
		arg_ptr = argname;
	        strcpy(argname, arg_ptr + TO_DATE_PREFIX_LEN);
		oper = OPR_LE;

	    }
	    else
	    {

	   	/* Column is not in either of the 2 tables.
	       	 * Can not use it for selection criteria
	     	 */
		oper = -1;
	    }

	    if (oper > 0)
 	    {
	        if (sqlFindColumn(shColumns, argname) != -1)
	        {
    		    /* We've found our argument in the shipment table */
    		    sFormatWhere(buffer, "h", argname, oper, argdata, argtype,
                 	         sqlGetDataTypeByPos(shColumns,
                                                     sqlFindColumn(shColumns,
                                                                   argname)));
    		    if (strlen(buffer))
        		strcat(where_list, buffer);
  	        }
	        else if (sqlFindColumn(slColumns, argname) != -1)
	        {
    	           /* We've found our argument in the shipment_line table */
    	           sFormatWhere(buffer, "d", argname, oper, argdata, argtype,
                       	        sqlGetDataTypeByPos(slColumns,
                                                    sqlFindColumn(slColumns,
                                                                  argname)));
    	            if (strlen(buffer))
            	        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(ohColumns, argname) != -1)
                {
                    /* We've found our argument in the ord table */
                    sFormatWhere(buffer, "o", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(ohColumns,
                                                     sqlFindColumn(ohColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(olColumns, argname) != -1)
                {
                    /* We've found our argument in the ord_line table */
                    sFormatWhere(buffer, "ol", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(olColumns,
                                                     sqlFindColumn(olColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(stpColumns, argname) != -1)
                {
                    /* We've found our argument in the stop table */
                    sFormatWhere(buffer, "stp", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(stpColumns,
                                                     sqlFindColumn(stpColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(cstColumns, argname) != -1)
                {
                    /* We've found our argument in the cstmst table */
                    sFormatWhere(buffer, "c", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(cstColumns,
                                                     sqlFindColumn(cstColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
            }

        }

    }
    srvFreeArgList(CTX);

    /* now let's build our table list */
    /* JJS - add adrmst */
    sprintf(buffer,
	    "cstmst c, ftpmst, prtmst_view, ord_line ol, ord o, "
	    " shipment_line d, ship_struct_view h, adrmst a ");
    strcat(table_list, buffer);

    /* client id clause */
    misTrimcpy(usr_id, osGetVar(LESENV_USR_ID), USR_ID_LEN);
    sprintf(buffer,
            " get client in clause for user "
            " where table_prefix = 'o' "
            "   and usr_id = '%s' "
            "   and wh_id = '%s' "
            "   and prt_client_id_flg = 0 ",
            usr_id,
            wh_id);
    CurPtr = NULL;
    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return (srvResults(ret_status, NULL));
    }
    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);
    strcpy(client_list, sqlGetString(res, row, "client_in_clause"));
    srvFreeMemory(SRVRET_STRUCT, CurPtr);

    /* AND now the where clause */
    /*
     * Notes...
     * We need to look for a shpsts of Loading, because with fluid loading,
     * once inventory is on a trailer, the shipment goes to loading, even
     * though there may be a lot of work left to do (like allocate the rest
     * of the stops or something...
     *
     * JJS 03/11/2009 - changes for adrmst/adrnam
     */

    sprintf(buffer,
	     "      ftpmst.ftpcod = prtmst_view.ftpcod "
	     "  and prtmst_view.prtnum = ol.prtnum "
	     "  and prtmst_view.prt_client_id = ol.prt_client_id "
	     "  and prtmst_view.wh_id = ol.wh_id "
             "  and d.linsts in ('%s', '%s') "
	     "  and d.pckqty    > 0 "
	     "  and d.ship_id   = h.ship_id "
	     "  and d.client_id = o.client_id"
	     "  and d.ordnum    = o.ordnum "
	     "  and d.wh_id     = o.wh_id "
	     "  and o.wave_flg  = '%ld' "
	     "  and d.client_id = ol.client_id"
	     "  and d.ordnum    = ol.ordnum "
	     "  and d.ordlin    = ol.ordlin "
	     "  and d.ordsln    = ol.ordsln "
             "  and ol.non_alc_flg = %d     "
	     "  and o.client_id = c.client_id "
	     "  and o.stcust = c.cstnum "
             "  and h.super_ship_flg = '%ld' "
             "  and h.super_ship_id is null "
	     "  and h.shpsts in ('%s', '%s', '%s')"
             "  %s "
             "  and not exists "
             "    (select 1 "
             "       from rplwrk r "
             "      where r.ship_line_id = d.ship_line_id) "
             "  and not exists "
             "    (select 'x' "
             "       from xdkwrk x "
             "      where x.ship_line_id = d.ship_line_id) "
             " and %s "
             " and a.adr_id = h.rt_adr_id "
	     "  group by o.wh_id, a.adrnam, h.ship_id, o.client_id, "
             "           h.host_client_id, h.host_ext_id, "
             "           h.shpsts, h.rt_adr_id, "
             "           h.stop_id, h.car_move_id, "
	     "           h.cargrp, h.carcod, "
	     "           h.srvlvl, h.sddflg, h.doc_num, h.track_num, "
	     "           h.frtchg, h.frtcod, h.frtrte, h.adddte, "
	     "           h.alcdte, h.stgdte, h.loddte, h.entdte, "
	     "           h.early_shpdte, h.late_shpdte, "
	     "           h.early_dlvdte, h.late_dlvdte, "
	     "           h.rrlflg ",
	     LINSTS_PENDING, LINSTS_INPROCESS,
	     BOOLEAN_FALSE,
             BOOLEAN_FALSE,
             /* exclude super shipments. */
             BOOLEAN_FALSE,
	     /* include ready and in-process shipments */
	     SHPSTS_READY, SHPSTS_IN_PROCESS, SHPSTS_LOADING,
	     where_list,
         client_list);
    strcpy(where_list, buffer);

    CurPtr = NULL;
    ret_status = srvInitiateCommandFormat(&CurPtr,
	     "[ select distinct ' ' pckexc, %s "
	     " from %s "
	     " where %s "
	     " order by o.wh_id, h.ship_id ] catch(-1403) >> res "
             " | "
             " if(@res) "
             "     append shipment change deferred flag "
             "     where res = @res "
             "       and status = @? ",
	     field_list,
	     table_list,
	     where_list);

    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	 srvFreeMemory(SRVRET_STRUCT, CurPtr);
	 return (srvSetupReturn(ret_status, ""));
    }

    if (ret_status == eOK)
	sSetExclusions(CurPtr);

    return (CurPtr);
}
