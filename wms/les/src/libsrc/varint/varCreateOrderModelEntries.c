static const char *rcsid = "$Id: intCreateOrderModelEntries.c 111245 2006-03-29 03:49:43Z mzais $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "intlib.h"

LIBEXPORT 
RETURN_STRUCT *varCreateOrderModelEntries(char *ship_id_list_i,
					  char *trlr_id_i,
					  char *car_move_id_i,
					  char *stop_id_i,
					  long *stop_seq_i)
{
    long status;
    RETURN_STRUCT *CurPtr;
    mocaDataRow *row;
    mocaDataRes *res;

    char *ship_idPtr;
    char *ship_idNxt;
    char *ship_id_list;
    char ship_id[SHIP_ID_LEN + 1];
    char carcod[CARCOD_LEN + 1];
    char rt_adr_id[ADR_ID_LEN + 1];
    char trlr_id[TRLR_ID_LEN + 1];
    char wh_id[WH_ID_LEN + 1];
    char car_move_id[CAR_MOVE_ID_LEN + 1];
    char stop_id[STOP_ID_LEN + 1];
    char buffer[2000];
    long FirstTime = TRUE;
    long stop_seq;

    memset(ship_id, 0, sizeof(ship_id));
    memset(carcod, 0, sizeof(carcod));
    memset(rt_adr_id, 0, sizeof(rt_adr_id));
    memset(trlr_id, 0, sizeof(trlr_id));
    memset(car_move_id, 0, sizeof(car_move_id));
    memset(stop_id, 0, sizeof(stop_id));
    memset(wh_id, 0, sizeof(wh_id));

    misTrc(T_FLOW,
	   "intCreateOrderModelEntries entering with "
	   "ship_id_list %s ",
	   ship_id_list_i); 

    if (ship_id_list_i && misTrimLen(ship_id_list_i, SHIP_ID_LEN))
    {
	ship_id_list = (char *) calloc(1, strlen(ship_id_list_i) + 1);
	strncpy(ship_id_list, ship_id_list_i, strlen(ship_id_list_i));
    }
    else
        return (APPMissingArg("ship_id_list"));

    if (trlr_id_i && misTrimLen(trlr_id_i, TRLR_ID_LEN))
	misTrimcpy(trlr_id, trlr_id_i, TRLR_ID_LEN);

    if (car_move_id_i && misTrimLen(car_move_id_i, CAR_MOVE_ID_LEN))
	misTrimcpy(car_move_id, car_move_id_i, CAR_MOVE_ID_LEN);

    if (stop_id_i && misTrimLen(stop_id_i, STOP_ID_LEN))
	misTrimcpy(stop_id, stop_id_i, STOP_ID_LEN);

    if (stop_seq_i)
	stop_seq = *stop_seq_i;
    else
        stop_seq = 1;

    /*  Loop through all of the shipments passed in */
    CurPtr = NULL;

    ship_idPtr = ship_id_list;
    ship_idNxt = NULL;
    do
    {
	ship_idNxt = strchr(ship_idPtr, ',');
	if (ship_idNxt)
	    *ship_idNxt++ = '\0';

	memset(ship_id, 0, sizeof(ship_id));
	strncpy(ship_id, ship_idPtr, misTrimLen(ship_idPtr, SHIP_ID_LEN));
	
	if (FirstTime)
	{
	     /* we need to find out what the carrier is */
	    sprintf(buffer,
		    "select carcod, rt_adr_id, "
                    "       wh_id "
		    "  from shipment "
		    " where ship_id = '%s'",
		    ship_id);
	    status = sqlExecStr(buffer, &res);
	    if (status != eOK)
	    {
	        sqlFreeResults(res);
	        free(ship_id_list);
                return (srvResults(status, NULL));
	    }

	    row = sqlGetRow(res);
	    strncpy(carcod, sqlGetString(res, row, "carcod"),CARCOD_LEN);
	    strncpy(rt_adr_id, sqlGetString(res, row, "rt_adr_id"),ADR_ID_LEN);
	    strncpy(wh_id, sqlGetString(res, row, "wh_id"),WH_ID_LEN);
	    sqlFreeResults(res);

	    /*
	     * Ship_id is what parcel manifest is using for the trlr_num. 
	     * I'm going to generate a trlr_id because there isn't an
	     * existing sequence for trlr_num.
	    */
	    
	    if (!strlen(trlr_id))
	    {
	    	/* Create trlr id */
	    	status = appNextNum (NUMCOD_TRLR_ID, trlr_id);
	    	if (status != eOK)
	    	{
	            misTrc(T_FLOW, "Error %ld performing command [%s]", 
		           status, buffer);
	            free(ship_id_list);
	            return (srvResults(status, NULL));
		}
	    }
	    
	    /* Create virtual trailer for shipment */
	    sprintf (buffer,
		     "create trailer "
		     " where carcod    = '%s' "
		     "   and trlr_num  = '%s' "
		     "   and trlr_id   = '%s' "
		     "   and trlr_stat = '%s' "
		     "   and autogen_flg = 1 "
                     "   and stoloc_wh_id = '%s' "
                     "   and trlr_cod    = '%s' ",
		     carcod, 
                     trlr_id, 
                     trlr_id, 
                     TRLSTS_OPEN,
                     wh_id,
                     TRLR_COD_SHIP);

    	    status = srvInitiateCommand (buffer, NULL);
    	    if (status != eOK)
    	    {
	        free(ship_id_list);
	        return (srvResults(status, NULL));
    	    }

	    /*
	     * Ship_id is what parcel manifest is using for the car_move. 
	     * I'm going to generate one.
	     * strncpy(car_move_id, ship_id, misTrimLen(ship_id, SHIP_ID_LEN));
	    */
	    
	    if (!strlen(car_move_id))
	    {
	    	/* Create carrier move id */
	    	status = appNextNum (NUMCOD_CAR_MOVE_ID, car_move_id);
	    	if (status != eOK)
	    	{
	            misTrc(T_FLOW, "Error %ld performing command [%s]", 
		           status, buffer);
	            free(ship_id_list);
	            return (srvResults(status, NULL));
	    	}
	    }
	    
            /* Create carrier move entry for virtual trailer 
             * Note, we send in wh id here, even thought it's not needed
             * on the carrier move table, so that it is on the stack for
             * any LENS wrappers on create carrier move.
             */

            sprintf (buffer,
		     "create carrier move "
		     "  where carcod      = '%s' "
		     "    and car_move_id = '%s' "
                     "    and wh_id       = '%s' "
		     "    and trlr_id     = '%s' ",
		     carcod, car_move_id, 
                     wh_id,
                     trlr_id);

    	    status = srvInitiateCommand (buffer, NULL);
    	    if (status != eOK)
    	    {
	        free(ship_id_list);
	        return (srvResults(status, NULL));
    	    }

	    /* Create stop entry for shipment address */
	    if (!strlen(stop_id))
	    {
	    	status = appNextNum (NUMCOD_STOP_ID, stop_id);
	    	if (status != eOK)
	    	{
	            misTrc(T_FLOW, "Error %ld performing command [%s]", 
		           status, buffer);
	            free(ship_id_list);
	            return (srvResults(status, NULL));
	    	}
	    }

            /*
             * we need to pass wh_id here even though it is not used by the 
             * create stop command because...
             * when get installed configuration is called by triggers, wh needs to be
             * passed, because that command looks at policy data. and 
             * we need wh_id 
             * to write daily transactions
             * We also need wh_id on the stack for create stop and carrier
             * move for lens/supplier commmands that override the
             * create stop and supplier, so we create these in the
             * correct warehouse.
             */

	    sprintf (buffer,
		     "create stop "
		     "  where car_move_id = '%s' "
		     "    and stop_id     = '%s' "
                     "    and stop_nam    = '%s' "
		     "    and adr_id      = '%s' "
		     "    and stop_seq    = %ld  "
                     "    and wh_id       = '%s' ",
		     car_move_id, 
                     stop_id,
                     stop_id,
                     rt_adr_id,
                     stop_seq,
                     wh_id);

	    status = srvInitiateCommand (buffer, NULL);
	    if (status != eOK)
	    {
	        free(ship_id_list);
	        return (srvResults(status, NULL));
	    }

	    FirstTime = FALSE;
	}

	/* Assign shipment to a stop for parcel manifest shipment */
	sprintf (buffer,
		 " assign shipment to stop "
		 " where stop_id = '%s' "
		 "   and ship_id = '%s' "
                 "   and wh_id = '%s' ",
		 stop_id, 
                 ship_id,
                 wh_id);

	status = srvInitiateCommand (buffer, NULL);
	if (status != eOK)
	{
	    free(ship_id_list);
	    return (srvResults(status, NULL));
	}

	/* Assign shipment stop to virtual trailer carrier move entry */

        /*
         * we need to pass wh_id here even though it is not used by the assign
         * stop to carrier move command. Trigger on assign stop to carrier 
         * move command needs wh_id to Write Daily transaction activity. 
         */

	sprintf (buffer,
		 "assign stop to carrier move "
		 " where car_move_id  = '%s' "
		 "   and stop_id_list = '%s' "
                 "   and wh_id = '%s' ",
		 car_move_id,
                 stop_id,
                 wh_id);

	status = srvInitiateCommand (buffer, NULL);
	if (status != eOK)
	{
	    free(ship_id_list);
	    return (srvResults(status, NULL));
	}
	
	ship_idPtr    = ship_idNxt;
    } while (ship_idPtr);

    free(ship_id_list);

    CurPtr = srvResults( eOK, NULL );

    return (CurPtr);
}

