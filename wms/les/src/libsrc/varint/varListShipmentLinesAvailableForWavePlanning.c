static const char *rcsid = "$Id: intListShipmentLinesAvailableForWavePlanning.c 165645 2008-08-06 19:42:12Z pflanzer $";
/*#START**********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *#END***********************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "intlib.h"
#include "intWavLib.h"

/*
 * HISTORY
 * JJS 02/06/2009 - Creation.  Show the vc_host_move_id and vc_host_stop_seq
 *                  on the pre-plan summary (on both Summary and Detail views).
 *                  Also, order the output in a more useful manner by default.
 * JJS 06/26/2009 - Show city, province, zip code (as one column).
 * JJS 07/02/2009 - Show customer name and part family (NOTE: not adding prtfam
 *                  to summary view because there could be multiple on an order).
 * JJS 07/31/2009 - Fix to use wave planning from Summary View.  This was previously
 *                  not supported, but product started supporting it in 2007.2.
 *                  The reason it was not working for us was a big oopsie... our
 *                  VAR version was originally coded from 2006.1.8, so this is
 *                  actually the first version of this file based off of 2008.1.2.
 *
 */


LIBEXPORT
RETURN_STRUCT *varListShipmentLinesAvailableForWavePlanning(char *wave_rule_i,
/* It is necessary to pass the wave set as an argument because MOCA does not
 * pull the wave_set from MCS context automatically.  swavebuildwhere does not
 * add wave_set to the where clause and would not handle a null value the way we
 * want anyway. The enhanced wave planning screen sets the waveset of lines we are
 * interested in processing, so if the wave_set has a value, we want to select only
 * the lines in the wave_set for processing.  If the wave_set is null, then we want to
 * select all lines available, but not the ones already part of a wave set so we must
 * be able to have the wave_set be null in the select statement.
 */			                                   char *wave_set_i,
/* The Summary flag needs to be an input parameter because
 * it is used by the code to determin what SQL statement to run
 * and is not used in the SQL itself.  It could not be picked up
 * by a MOCA function off the stack because it would do us no good from there.
 * The summary flag controls whether the summary SQL statement is run or the detail
 * SQL statement is run.  It is passed in by the VB to control the user view in wave planning.
 */
						            long *sumflg_i,
/*
 * if ship_id_list available and sumflg == 0, that means
 * we wanna retrieve the details
 */
                                    char *ship_id_list_i,
						            char *wh_id_i)
{
    RETURN_STRUCT *CmdRes;
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;
    char dtype;
    char *colval;

    long ret_status;
    char buffer[5000];
    char* dynBuffer = NULL;
    char wave_rule[RULE_NAM_LEN + 1];
    char wave_set[WAVE_SET_LEN + 1];
    char wh_id[WH_ID_LEN + 1];
    char * ship_id_list = NULL;
    char usr_id[USR_ID_LEN + 1];
    char wavesetwhereClause [100];
    char clientswhereClause[5000];
    char * shipidwhereClause = NULL;
    char *whereClause = NULL;
    long totalQty;
    long totalPieces;
    long rowCount;
    long retval = 0;

    memset(buffer, 0, sizeof(buffer));
    memset(wave_rule, 0, sizeof(wave_rule));
    memset(wave_set, 0, sizeof(wave_set));
    memset(wh_id, 0, sizeof(wh_id));
    memset(usr_id, 0, sizeof(usr_id));
    memset(wavesetwhereClause, 0, sizeof(wavesetwhereClause));
    memset(clientswhereClause, 0, sizeof(clientswhereClause));

    if (!wave_rule_i || misTrimLen(wave_rule_i, RULE_NAM_LEN) == 0)
        return (APPMissingArg("wave_rule"));

    misTrimcpy(wave_rule, wave_rule_i, RULE_NAM_LEN);

    if (!wh_id_i || misTrimLen(wh_id_i, WH_ID_LEN) == 0)
        return (APPMissingArg("wh_id"));

    misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

    if (wave_set_i && misTrimLen(wave_set_i, WAVE_SET_LEN) > 0)
        misTrimcpy(wave_set, wave_set_i, WAVE_SET_LEN);

    if (ship_id_list_i && sumflg_i && *sumflg_i == 0)
    {
        ship_id_list = (char *) calloc(1, strlen(ship_id_list_i) + 1);
        strncpy(ship_id_list, ship_id_list_i, strlen(ship_id_list_i));
    }
    /*
     * Build a where clause for the arguments we expect in this function.
     */

    ret_status = swaveBuildWhere(wave_rule, &whereClause, "shipment");
    if (ret_status != eOK)
    {
        if (whereClause)
            free(whereClause);
        if (ship_id_list) free(ship_id_list);
        return (srvResults(ret_status, NULL));
    }

    /* Validate client id for user */
    misTrimcpy(usr_id, osGetVar(LESENV_USR_ID), USR_ID_LEN);
    sprintf(buffer,
            " get client in clause for user "
            " where table_prefix = 'shipment_line' "
            "   and usr_id = '%s' "
            "   and wh_id = '%s' "
            "   and prt_client_id_flg = 0 ",
            usr_id,
            wh_id);
    CmdRes = NULL;
    ret_status = srvInitiateCommand(buffer, &CmdRes);
    if (ret_status != eOK)
    {
        if (ship_id_list) free(ship_id_list);
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        return (srvResults(ret_status, NULL));
    }
    res = srvGetResults(CmdRes);
    row = sqlGetRow(res);
    strcat(clientswhereClause, " and ");
    strcat(clientswhereClause, sqlGetString(res, row, "client_in_clause"));
    srvFreeMemory(SRVRET_STRUCT, CmdRes);

    /*
     * Find all shipments matching our criteria
     */


    CmdRes = NULL;

    if (sumflg_i)
    {
	if (*sumflg_i == 1)
	{
            sprintf(buffer,
            " [select distinct ord.vc_host_move_id, "
            "                  ord.vc_host_stop_seq, "
            "                  ord.ordtyp, "
            "                  adrmst.adrnam, "
            "                  adrmst.adrcty || ', ' || adrmst.adrstc || ' ' || adrmst.adrpsz adrcty, "
            "                  shipment_line.ordnum, "
            "                  shipment.ship_id, "
  	    "                  count(distinct ord.ordnum) numord,"
	    "                  count(ord_line.ordlin) numsls,"
	    "                  shipment.shpsts,"
	    "                  shipment.early_dlvdte,"
	    "                  shipment.late_dlvdte,"
	    "                  shipment.early_shpdte,"
	    "                  shipment.late_dlvdte,"
	    "                  sum(shipment_line.pckqty) pckqty,"
	    "		       shipment_line.prcpri"
            "    from ord_line, "
            "         ord, "
            "         shipment_line, "
            "         shipment,"
            "         adrmst"
            "   where ord.wave_flg         = '%ld'  "
            "     and ord_line.ordnum      = shipment_line.ordnum "
            "     and ord_line.wh_id       = shipment_line.wh_id "
            "     and ord_line.client_id   = shipment_line.client_id "
            "     and ord_line.ordlin      = shipment_line.ordlin "
            "     and ord_line.ordsln      = shipment_line.ordsln "
            "     and shipment.ship_id     = shipment_line.ship_id "
            "     and ord.ordnum           = ord_line.ordnum "
            "     and ord.wh_id            = ord_line.wh_id "
            "     and ord.client_id        = ord_line.client_id "
            "     and ord.wh_id            = '%s' "
            "     and shipment_line.pckqty > 0 "
            "     and shipment.super_ship_flg = '%ld' "
            "     and shipment.super_ship_id is null "
            "     and shipment.shpsts      != '%s' "
            "     and shipment_line.schbat is null "
	    "     and shipment.rt_adr_id = adrmst.adr_id "
            "     %s %s %s "
	    "  group by ord.vc_host_move_id, ord.vc_host_stop_seq, ord.ordtyp, adrmst.adrnam, "
            "           adrmst.adrcty || ', ' || adrmst.adrstc || ' ' || adrmst.adrpsz, "
            "           shipment_line.ordnum, "
            "           shipment_line.prcpri, shipment.ship_id, "
	    "		    shipment.shpsts, shipment.early_dlvdte, "
	    "		    shipment.late_dlvdte, shipment.early_shpdte, "
  	    "	  	    shipment.late_shpdte "
            "  order by shipment_line.prcpri, "
            "           ord.vc_host_move_id, "
            "           ord.vc_host_stop_seq, "
            "           adrcty, "
            "           shipment_line.ordnum ] ",
            BOOLEAN_TRUE,
            wh_id,
            BOOLEAN_FALSE,
            SHPSTS_CANCELLED,
            whereClause ? " and " : "",
            whereClause ? whereClause : "",
            clientswhereClause);

	    if (whereClause)
		free(whereClause);

	    ret_status = srvInitiateCommand(buffer, &CmdRes);
	    if (ret_status != eOK)
	    {
		if (CmdRes)
		    srvFreeMemory(SRVRET_STRUCT, CmdRes);
		CmdRes = NULL;

		if (ret_status == eDB_NO_ROWS_AFFECTED ||
		    ret_status == eSRV_NO_ROWS_AFFECTED)
		{
		    return (srvResults(eNO_SHIPMENT_LINES_MATCHING_CRITERIA, NULL));
		}
                return (srvResults(ret_status, NULL));
	    }

            return (CmdRes);
	}
    }

    if (misTrimLen(wave_set_i, WAVE_SET_LEN) > 0)
	sprintf(wavesetwhereClause, "and shipment.wave_set = '%s'", wave_set);
    else
    sprintf(wavesetwhereClause, "and 1=1");

    if (ship_id_list)
    {
        retval = misDynSprintf(&shipidwhereClause,
                              " and shipment_line.ship_id in (%s) ",
                              ship_id_list);
        if (retval < 0)
        {
            if (shipidwhereClause) free(shipidwhereClause);
            if (ship_id_list) free(ship_id_list);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    retval = misDynSprintf(&dynBuffer,
            " [select distinct ord.vc_host_move_id, "
            "                  ord.vc_host_stop_seq, "
            "                  ord.ordtyp, "
            "                  prtmst_view.prtfam, "
            "                  adrmst.adrnam, "
            "                  adrmst.adrcty || ', ' || adrmst.adrstc || ' ' || adrmst.adrpsz adrcty, "
            "                  shipment_line.*, "
            "                  prtdsc.lngdsc "
            "    from prtmst_view, "
            "         prtdsc, "
            "         ord_line, "
            "         ord, "
            "         shipment_line, "
            "         shipment, "
            "         adrmst "
            "   where ord.wave_flg         = '%ld'  "
            "     and prtmst_view.prtnum        = ord_line.prtnum "
            "     and prtmst_view.prt_client_id = ord_line.prt_client_id "
            "     and prtmst_view.wh_id         = ord_line.wh_id "
            "     and ord_line.ordnum      = shipment_line.ordnum "
            "     and ord_line.wh_id       = shipment_line.wh_id "
            "     and ord_line.client_id   = shipment_line.client_id "
            "     and ord_line.ordlin      = shipment_line.ordlin "
            "     and ord_line.ordsln      = shipment_line.ordsln "
            "     and shipment.ship_id     = shipment_line.ship_id "
            "     and ord.ordnum           = ord_line.ordnum "
            "     and ord.client_id        = ord_line.client_id "
            "     and ord.wh_id            = ord_line.wh_id "
            "     and ord.wh_id            = '%s' "
            "     and shipment_line.pckqty > 0 "
            "     %s "
            "     and shipment.super_ship_flg = '%ld' "
            "     and shipment.super_ship_id is null "
            "     and shipment.shpsts      != '%s' "
	    "     and shipment.rt_adr_id = adrmst.adr_id "
            "     and shipment_line.schbat is null "
            "     and prtdsc.colnam = '%s' "
            "     and prtdsc.colval = /*=varchar(*/prtmst_view.prtnum||'|'||prtmst_view.prt_client_id||'|'||prtmst_view.wh_id_tmpl/*=)*/ "
            "     and prtdsc.locale_id = '%s' "
            "     %s %s %s %s "
            "  order by shipment_line.prcpri, "
            "           ord.vc_host_move_id, "
            "           ord.vc_host_stop_seq, "
            "           ord.ordtyp, "
            "           adrcty, "
            "           shipment_line.ordnum, "
            "           shipment_line.ordlin ] ",
            BOOLEAN_TRUE,
            wh_id,
            ship_id_list ? shipidwhereClause : "",
            BOOLEAN_FALSE,
            SHPSTS_CANCELLED,
            PRTDSC_COLNAM,
            osGetVar(LESENV_LOCALE_ID),
            wavesetwhereClause,
            whereClause ? " and " : "",
            whereClause ? whereClause : "",
            clientswhereClause);

    if (whereClause)
        free(whereClause);

    if (retval < 0)
    {
        if (dynBuffer) free(dynBuffer);
        if (shipidwhereClause) free(shipidwhereClause);
        if (ship_id_list) free(ship_id_list);
        return(srvResults(eNO_MEMORY, NULL));
    }

    ret_status = srvInitiateCommand(dynBuffer, &CmdRes);

    if (shipidwhereClause) free(shipidwhereClause);
    if (ship_id_list) free(ship_id_list);
    if (dynBuffer) free(dynBuffer);

    if (ret_status != eOK)
    {
        if (CmdRes)
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
        CmdRes = NULL;

        if (ret_status == eDB_NO_ROWS_AFFECTED ||
            ret_status == eSRV_NO_ROWS_AFFECTED)
        {
            return (srvResults(eNO_SHIPMENT_LINES_MATCHING_CRITERIA, NULL));
        }
        return (srvResults(ret_status, NULL));
    }

    totalPieces = 0;

    /*
     * If we get a maximum qty in, then we need to make sure we don't
     * violate that.  Otherwise, we can just send back the results
     * we selected.
     */

    ret_status = srvGetNeededElement("totpcs", NULL, &dtype,
                                     (void **) &colval);

    if (ret_status != eOK)
    {
        /*
         * Argument wasn't passed, just get out.
         */

        return (CmdRes);

    }

    /*
     * We'll handle the total pieces coming in as different data types.
     * When called in msql, it would most likely come in as a number,
     * but if called through Wave Operations, it is probably going to
     * come in as a string.
     */

    switch(dtype)
    {
        case COMTYP_CHAR:
            totalPieces = atoi(colval);
            break;

        case COMTYP_LONG:
        case COMTYP_INT:
            totalPieces = *(long *)colval;
            break;
    }

    misTrc(T_FLOW, "Total pieces allowed is %ld", totalPieces);

    if (totalPieces == 0)
    {
        /* If total pieces is 0, just ignore it and return.
         * Some times 0 is passed in by MCSfi.PushContext if the
         * field in GUI is empty. It makes no sense that a user
         * inputs 0 in total pieces field.
         */
        misTrc(T_FLOW, "Ignore total pieces  %ld", totalPieces);
        return (CmdRes);
    }

    totalQty = 0;
    rowCount = 0;
    res = srvGetResults(CmdRes);

    /*
     * What we are going to do here is loop around all the rows returned,
     * and find out which rows will not violate the total number of
     * pieces allowed.  We will simply just grab the first x amount of
     * rows until the total unit qty we have accumulated equals the
     * maximum pieces allowed.
     */

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        if (totalQty + sqlGetLong(res, row, "pckqty") <= totalPieces)
        {
            totalQty = totalQty + sqlGetLong(res, row, "pckqty");
            rowCount++;
        }
        else
        {
            break;
        }
    }

    /*
     * Now, if our rowcount is less than the total number of rows,
     * we need to strip off those last rows, so we don't violate
     * the maximum number of pieces.
     * The reason we do it this way rather than just defining our
     * result set and adding rows to it is because we are doing a
     * select * so we don't have to hard-code the columns we are
     * sending back, allowing us to be more flexible.
     */

    while (rowCount < sqlGetNumRows(res))
    {
        /* Get the last row. */
        row = sqlGetLastRow(res);
        if (!row)
        {
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
            return srvErrorResults(ret_status, "Could not get last row", NULL);
        }

        /* Remove the last row. */
        sqlRemoveRow(res, row);

        /* Descrement the number of rows in the return struct. */
        CmdRes->rows--;
    }

    /* We could run into a situation where they entered a total pieces
     * quantity that's smaller than the quantity on any of the rows.
     * If that's the case, we'll be stripping off all of the rows
     * and will end up with an empty recordset. Rather than send back
     * 0 rows, we're going to provide them with an error.
     */

    if (rowCount == 0)
    {
        misTrc(T_FLOW, "Total pieces is less than the quantity of any"
                       " of the order lines. ");

        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        CmdRes = NULL;

        return (srvResults(eNO_SHIPMENT_LINES_MATCHING_CRITERIA, NULL));
    }


    return (CmdRes);


}
