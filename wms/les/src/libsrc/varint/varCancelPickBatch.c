static char *rcsid = "$Id: intCancelPickBatch.c 144981 2007-12-14 08:53:25Z kzhao $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <common.h>
#include "intlib.h"

LIBEXPORT
RETURN_STRUCT *varCancelPickBatch(char *schbat_i)
{
    RETURN_STRUCT *CurPtr;
    char buffer[1000];
    long ret_status;

    mocaDataRes *res;
    mocaDataRow *row;

    char schbat[SCHBAT_LEN + 1];
    char batsts[BATSTS_LEN + 1];
    char new_batsts[BATSTS_LEN + 1];

    CurPtr = NULL;
    memset(schbat, 0, sizeof(schbat));
    memset(batsts, 0, sizeof(batsts));
    memset(new_batsts, 0, sizeof(new_batsts));

    if (!schbat_i || misTrimLen(schbat_i, SCHBAT_LEN) == 0)
	return APPMissingArg("schbat");
    
    misTrimcpy(schbat, schbat_i, SCHBAT_LEN);
    
    
    sprintf(buffer,
	    "select batsts, "
	    "       wave_prc_flg "
	    "  from pckbat "
	    " where schbat = '%s' ",
	    schbat);
    
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvResults(ret_status, NULL));
    }
    row = sqlGetRow(res);

    /*
     * If this schbat was not planned via wave processing, then get out,
     * we currently don't support canceling a schbat that was planned through
     * shipment allocation.  Instead, we should just be calling 
     * Cancel Pick Groups where schbat = 'x'
     */

    if (sqlGetBoolean(res, row, "wave_prc_flg") == BOOLEAN_FALSE)
    {
	sqlFreeResults(res);
	return (APPError(eINT_CANT_CMPL_PCKBAT_NOT_IN_WAVE));
    }

    strncpy(batsts, sqlGetString(res, row, "batsts"), BATSTS_LEN);
    sqlFreeResults(res);

    /*
     * If this wave is already complete, it doesn't make sense to cancel it...
     */

    if (!strcmp(batsts, BATSTS_CMPL))
    {
	return (APPError(eINT_PCKBAT_ALREADY_CMPL));
    }

    /*
     * If the status is ALOC, then we need to clean up all
     * outstanding picks, replenishments and crossdock works...
     * Note, the following will eventually cause the pckbat record to be
     * deleted (from deallocate inventory) if the last piece of work
     * gets deleted from the schbat.
     * Because we are in a wave-scenario, we do not want to be reallocating the
     * picks, so make sure we call it with no-realloc set.
     */

    if (!strcmp(batsts, BATSTS_ALOC) || 
	!strcmp(batsts, BATSTS_SCH) ||
	!strcmp(batsts, BATSTS_ALLOC_IN_PROC) ||
	!strcmp(batsts, BATSTS_REL))
    {
	if (strcmp(batsts, BATSTS_SCH) == 0)
	{
	    /*
	     * In order to prevent problems with the pckrelmgr grabbing
	     * part of our wave while we are attempting to cancel, go 
	     * ahead and lock all the pckwrk records
	     */
	    misTrc(T_FLOW, "Locking pckwrk records for subsequent cancel");

	    sprintf(buffer,
		    "select 1 from pckwrk where schbat = '%s' for update",
		    schbat);
	    ret_status = sqlExecStr(buffer, NULL);
	    if (ret_status != eOK &&
            	ret_status != eSRV_NO_ROWS_AFFECTED &&
            	ret_status != eDB_NO_ROWS_AFFECTED)
		return(srvResults(ret_status, NULL));
	}
	/*LBS- This is the merge of standard product WMD-49199. Added the canbatflg.*/
	sprintf(buffer,
		"cancel pick groups "
		" where schbat = '%s' "
		"   and cancod = '%s' "
		"   and chgmod = '%s' "
	    "   and canbatflg = 1 ",
		schbat,
		CANCOD_NO_REALLOC,
		CHGMOD_UPDATE);
	ret_status = srvInitiateCommand(buffer, NULL);
	if (ret_status != eOK && 
            ret_status != eSRV_NO_ROWS_AFFECTED &&
            ret_status != eDB_NO_ROWS_AFFECTED)
	    return (APPError(ret_status));


	sprintf(buffer, 
		"[select rplref "
		"   from rplwrk "
		"  where schbat = '%s'] | cancel replenishment ",
		schbat);
	ret_status = srvInitiateInline(buffer, NULL);
	if (ret_status != eOK && 
            ret_status != eSRV_NO_ROWS_AFFECTED &&
            ret_status != eDB_NO_ROWS_AFFECTED)
	    return (APPError(ret_status));


	sprintf(buffer, 
		"[select xdkref "
		"   from xdkwrk "
		"  where schbat = '%s'] | cancel cross dock ",
		schbat);
	ret_status = srvInitiateInline(buffer, NULL);
	if (ret_status != eOK && 
            ret_status != eSRV_NO_ROWS_AFFECTED &&
            ret_status != eDB_NO_ROWS_AFFECTED)
	    return (APPError(ret_status));
    }

    /*
     * We need to remove all the shipment lines from this schbat.
     * Deassign all shipment lines from pick batch is called instead
     * of deassign shipment line from pick batch for every shipment line
     * to improve performance.
     */

    sprintf(buffer,
            " deassign all shipment lines from pick batch "
            "   where schbat = '%s' ",
            schbat);

    ret_status = srvInitiateCommand(buffer, NULL);

    /*
     * Check for NO_ROWS_AFFECTED, in case there aren't any shipment lines
     * that are still planned in the wave.
     */

    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        return (APPError(ret_status));

    /*
     * Now, remove the pckbat record.  If the batsts was ALOC or REL, this
     * should already have occurred.  However, if it was PLANNED, we have to
     * take care of it ourself.
     */

    sprintf(buffer,
	    "delete from pckbat "
	    " where schbat = '%s' ",
	    schbat);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK &&  ret_status != eDB_NO_ROWS_AFFECTED)
	return (APPError(ret_status));


    /* Now loop through and build the result set */

    CurPtr = NULL;

    CurPtr = srvResultsInit(eOK, 
	                    "schbat", COMTYP_CHAR, SCHBAT_LEN,
			    "old_batsts", COMTYP_CHAR, BATSTS_LEN,
			    "new_batsts", COMTYP_CHAR, BATSTS_LEN,
			    NULL);


    strcpy(new_batsts, BATSTS_CMPL);
    srvResultsAdd (CurPtr,
                   schbat,
		   batsts, 
		   new_batsts);

    return (CurPtr);
}
