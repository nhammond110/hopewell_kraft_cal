static const char *rcsid = "$Id: intCancelPickGroups.c 162950 2008-07-11 12:33:23Z pflanzer $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "intlib.h"

#define DETAILED_ROW_LIMIT	100000
/*
 **     intCancelPickGroups will cancel picks and deallocate inventory based
 **             upon a search criteria ... 
 */
LIBEXPORT 
RETURN_STRUCT *varCancelPickGroups(char *chgmod_i, char *pcksts_i, 
				   char *prtnum_i,
				   char *prt_client_id_i, 
				   char *schbat_i, char *wrkref_i,
				   char *wrktyp_i,
				   char *lodlvl_i, char *srcare_i,
				   char *srcloc_i,
				   char *car_move_id_i, char *stop_id_i, 
				   char *ship_id_i,
				   char *ship_line_id_i, 
				   char *client_id_i, char *ordnum_i,
				   char *rt_adr_id_i, char *ctnnum_i,
                                   char *list_id_i,
                                   char *wh_id_i,
                                   moca_bool_t *comflg_i,
								   moca_bool_t *canbatflg_i)
{
    long return_status = eOK;
    RETURN_STRUCT *CurPtr = NULL;
    RETURN_STRUCT *CurPtr1;
    RETURN_STRUCT *CurPtr2 = NULL;
    mocaDataRes *res;
    mocaDataRow *row, *nxtrow, *schbat_row, *tmprow;

    long ret_status;
    char buffer[2000];
    char pckwrk_buffer[2000];
    char ship_buffer[2000];
    char stop_buffer[2000];

    char tmpbuffer[200];
    char chgmod[CHGMOD_LEN + 1];
    char pcksts[PCKSTS_LEN + 1];
    char prtnum[PRTNUM_LEN + 1];
    char prt_client_id[CLIENT_ID_LEN + 1];
    char schbat[SCHBAT_LEN + 1];
    char wrkref[WRKREF_LEN + 1];
    char wrktyp[WRKTYP_LEN + 1];
    char lodlvl[LODLVL_LEN + 1];
    char srcare[ARECOD_LEN + 1];
    char srcloc[STOLOC_LEN + 1];
    char car_move_id[CAR_MOVE_ID_LEN + 1];
    char stop_id[STOP_ID_LEN + 1];
    char ship_id[SHIP_ID_LEN + 1];
    char ship_line_id[SHIP_LINE_ID_LEN + 1];
    char client_id[CLIENT_ID_LEN + 1];
    char ordnum[ORDNUM_LEN + 1];
    char rt_adr_id[ADR_ID_LEN + 1];
    char ctnnum[CTNNUM_LEN + 1];
    char list_id[LIST_ID_LEN + 1];
    char wh_id[WH_ID_LEN + 1];
    long total_appqty, total_pckqty, total_wrkref;
    moca_bool_t comflg = BOOLEAN_NOTSET;
    moca_bool_t canbatflg = BOOLEAN_FALSE;

    char stop_select [100];
    char stop_where [100];
    char stop_from [100];

    memset(chgmod, 0, sizeof(chgmod));
    memset(stop_where, 0, sizeof(stop_where));
    memset(stop_select, 0, sizeof(stop_select));
    memset(stop_from, 0, sizeof(stop_from));

    if (chgmod_i && misTrimLen(chgmod_i, CHGMOD_LEN))
	misTrimcpy(chgmod, chgmod_i, CHGMOD_LEN);

    if (strncmp(chgmod, CHGMOD_INQUIRY, strlen(CHGMOD_INQUIRY)) != 0 &&
	strncmp(chgmod, CHGMOD_UPDATE, strlen(CHGMOD_UPDATE)) != 0)
    {
	return ((RETURN_STRUCT *) srvSetupReturn(eINT_BAD_CHGMODE, ""));
    }

    memset(pcksts, 0, sizeof(pcksts));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(schbat, 0, sizeof(schbat));
    memset(wrkref, 0, sizeof(wrkref));
    memset(wrktyp, 0, sizeof(wrktyp));
    memset(lodlvl, 0, sizeof(lodlvl));
    memset(srcare, 0, sizeof(srcare));
    memset(srcloc, 0, sizeof(srcloc));
    memset(car_move_id, 0, sizeof(car_move_id));
    memset(stop_id, 0, sizeof(stop_id));
    memset(ship_id, 0, sizeof(ship_id));
    memset(ship_line_id, 0, sizeof(ship_line_id));
    memset(client_id, 0, sizeof(client_id));
    memset(ordnum, 0, sizeof(ordnum));
    memset(rt_adr_id, 0, sizeof(rt_adr_id));
    memset(ctnnum, 0, sizeof(ctnnum));
    memset(list_id, 0, sizeof(list_id));
    memset(wh_id, 0, sizeof(wh_id));

	if (canbatflg_i && *canbatflg_i)
    	canbatflg = BOOLEAN_TRUE;

    if (pcksts_i && misTrimLen(pcksts_i, PCKSTS_LEN))
	misTrimcpy(pcksts, pcksts_i, PCKSTS_LEN);

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
	misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);

    if (prt_client_id_i && misTrimLen(prt_client_id_i, CLIENT_ID_LEN))
	misTrimcpy(prt_client_id, prt_client_id_i, CLIENT_ID_LEN);

    if (schbat_i && misTrimLen(schbat_i, SCHBAT_LEN))
	misTrimcpy(schbat, schbat_i, SCHBAT_LEN);

    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
	strncpy(wrkref, wrkref_i, misTrimLen(wrkref_i, WRKREF_LEN));

    if (wrktyp_i && misTrimLen(wrktyp_i, WRKTYP_LEN))
	misTrimcpy(wrktyp, wrktyp_i, WRKTYP_LEN);

    if (lodlvl_i && misTrimLen(lodlvl_i, LODLVL_LEN))
	misTrimcpy(lodlvl, lodlvl_i, LODLVL_LEN);

    if (srcare_i && misTrimLen(srcare_i, ARECOD_LEN))
	misTrimcpy(srcare, srcare_i, ARECOD_LEN);

    if (srcloc_i && misTrimLen(srcloc_i, STOLOC_LEN))
	misTrimcpy(srcloc, srcloc_i, STOLOC_LEN);

    if (car_move_id_i && misTrimLen(car_move_id_i, CAR_MOVE_ID_LEN))
	misTrimcpy(car_move_id, car_move_id_i, CAR_MOVE_ID_LEN);

    if (stop_id_i && misTrimLen(stop_id_i, STOP_ID_LEN))
	misTrimcpy(stop_id, stop_id_i, STOP_ID_LEN);

    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
	misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);

    if (ship_line_id_i && misTrimLen(ship_line_id_i, SHIP_LINE_ID_LEN))
	misTrimcpy(ship_line_id, ship_line_id_i, SHIP_LINE_ID_LEN);

    if (ordnum_i && misTrimLen(ordnum_i, ORDNUM_LEN))
	misTrimcpy(ordnum, ordnum_i, ORDNUM_LEN);

    if (rt_adr_id_i && misTrimLen(rt_adr_id_i, ADR_ID_LEN))
	misTrimcpy(rt_adr_id, rt_adr_id_i, ADR_ID_LEN);

    if (ctnnum_i && misTrimLen(ctnnum_i, CTNNUM_LEN))
	misTrimcpy(ctnnum, ctnnum_i, CTNNUM_LEN);

    if (list_id_i && misTrimLen(list_id_i, LIST_ID_LEN))
	misTrimcpy(list_id, list_id_i, LIST_ID_LEN);

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

   /* 
    * If comflg is passed in, then use the passed value.
    * or get the value from policy.
    * If the comflg is not passed in to this component,
    * we should be getting the policy value. 
    * If the policy doesn't exist,
    * this should be defaulted to False.
    */
    comflg = BOOLEAN_FALSE;    
    if (comflg_i)
    {   
        comflg = *comflg_i; 
    }    
    else
    {
        sprintf(buffer,
                " select rtnum1 "
                "   from poldat_view "
                "  where polcod = '%s' "
                "    and polvar = '%s' "
                "    and polval = '%s' "
                "    and wh_id  = '%s' ",
                POLCOD_CANCEL_PICK,
                POLVAR_MISCELLANEOUS,
                POLVAL_COMMIT_DURING_PROCESSING,
                wh_id);
        ret_status = sqlExecStr(buffer,&res);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
        {
            sqlFreeResults(res);
            return(srvResults(ret_status, NULL));
        }
        else if (ret_status == eOK)
        {
            row = sqlGetRow(res);
            comflg = sqlGetLong(res, row, "rtnum1");
        }
        sqlFreeResults(res);
        res = NULL;
    }  

    /*
     * Now, try to get the part client ID value with the next function.  If
     * 3PL is not installed, it will just return a default value.
     * If 3PL is installed, the function 
     * will return INVALID_ARGS if there is no client ID.  
     */

    ret_status = appGetClient(prt_client_id_i, prt_client_id);
    if (ret_status != eOK) 
    {	
        if ((prtnum_i && 
	     misTrimLen(prtnum_i, PRTNUM_LEN) != 0 && 
	     ret_status == eINVALID_ARGS) || ret_status != eINVALID_ARGS)
	    return (srvSetupReturn(ret_status, ""));
	else
	    memset(prt_client_id, 0, sizeof(prt_client_id));
    }	

    misTrc(T_FLOW, " Part ->(%s) ClientOUT->(%s)",
	    prtnum, prt_client_id);

    /* Look for our ship_id in the stop table.  If it's   */
    /* there, include it in our lookup, otherwise, leave it out. */


    /* There's a few possibilities here:
     * 1) we have move/stop information, so we may care about the
     *    stop table - in this case, we'll want to make sure
     *    the shipment exists on the stop action, and if it does, 
     *    we'll include those tables in our query for picks.
     * 2) assuming we don't have move/stop information, we can 
     *    proceed without caring about the stop action tabe...in 
     *    that case, we need to check if maybe we don't even have
     *    shipment information - in which case, we'd better have a 
     *    wrkref so that we can take care of things like replenishments
     */

    /* First let's build up the pckwrk criteria buffer...we need this
       in the cases where we either have a stop or not... */

    sprintf(pckwrk_buffer, "");
    sprintf(ship_buffer, "");

    if (strlen(pcksts))
    {
        sprintf(tmpbuffer, "  and pckwrk.pcksts like '%s%%' ", pcksts);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(wh_id))
    {
        sprintf(tmpbuffer, "  and pckwrk.wh_id = '%s' ", wh_id);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(prtnum))
    {
        sprintf(tmpbuffer, "  and pckwrk.prtnum like '%s%%' ", prtnum);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(prt_client_id))
    {
        sprintf(tmpbuffer, "  and pckwrk.prt_client_id like '%s%%' ",
		prt_client_id);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(schbat))
    {
        /*
         * Need to disable the index on schbat for performance issue.
         */

        sprintf(tmpbuffer, "  and nvl(pckbat.schbat,rtrim(' ')) || '' like '%s%%' ", schbat);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(wrkref))
    {
        sprintf(tmpbuffer, "  and pckwrk.wrkref like '%s%%' ", wrkref);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(wrktyp))
    {
        sprintf(tmpbuffer, "  and pckwrk.wrktyp like '%s%%' ", wrktyp);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(lodlvl))
    {
        sprintf(tmpbuffer, "  and pckwrk.lodlvl like '%s%%' ", lodlvl);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(srcare))
    {
        sprintf(tmpbuffer, "  and pckwrk.srcare like '%s%%' ", srcare);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(srcloc))
    {
        sprintf(tmpbuffer, "  and pckwrk.srcloc like '%s%%' ", srcloc);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(ship_id))
    {
        sprintf(tmpbuffer, "  and pckwrk.ship_id like '%s%%' ", ship_id);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(ship_line_id))
    {
        sprintf(tmpbuffer, "  and pckwrk.ship_line_id like '%s%%' ", 
		ship_line_id);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(ordnum))
    {
        sprintf(tmpbuffer, "  and pckwrk.ordnum like '%s%%' ", ordnum);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(ctnnum))
    {
        sprintf(tmpbuffer, "  and pckwrk.ctnnum like '%s%%' ", ctnnum);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(list_id))
    {
        sprintf(tmpbuffer, "  and pckwrk.list_id like '%s%%' ", list_id);
        strcat(pckwrk_buffer, tmpbuffer);
    }
    if (strlen(rt_adr_id))
    {
	sprintf(tmpbuffer, "  and shipment.rt_adr_id like '%s%%' ", rt_adr_id);
	strcat(ship_buffer, tmpbuffer);
    }

    sprintf(stop_buffer, "");
    if (strlen(car_move_id))
    {
	sprintf(tmpbuffer, "  and stop.car_move_id like '%s%%' ", car_move_id);
	strcat(stop_buffer, tmpbuffer);
    }
    if (strlen(stop_id))
    {
	sprintf(tmpbuffer, "  and stop.stop_id like '%s%%' ", stop_id);
	strcat(stop_buffer, tmpbuffer);
    }


    /* Now we need to decide if we need to look at the stop tables */
    sprintf(buffer,
	    "select 1 "
	    "  from dual "
	    " where exists "
	    "   (select shipment.ship_id "
	    "      from shipment, pckwrk, pckbat, stop "
	    "     where pckwrk.schbat = pckbat.schbat "
	    "       and pckwrk.appqty < pckwrk.pckqty "
	    "       and pckbat.cmpdte is null "
	    "       and pckwrk.ship_id = shipment.ship_id "
	    "       and shipment.stop_id = stop.stop_id "
	    "       %s %s %s)",
	    pckwrk_buffer, ship_buffer, stop_buffer);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	misTrc(T_FLOW, "Fatal error from DB - getting out");
	return(srvSetupReturn(ret_status, ""));
    }

    if (ret_status == eOK)
    {
	sprintf(stop_select, "stop.stop_id, ");
    	sprintf(stop_from, " stop, ");
    	sprintf(stop_where, " and shipment.stop_id = stop.stop_id ");
    }
    else
    {
	sprintf(stop_select, "");
	sprintf(stop_from, "");
	sprintf(stop_where, "");
	sprintf(stop_buffer, "");
    }
		
    /* Now, select to see what we can find */

    if (strcmp(chgmod, CHGMOD_UPDATE) == 0)
    {
	sprintf(buffer,
		"exec sql with lock "
		"where main_table  = \"pckwrk\"" 
		"  and main_pk     = \"pckwrk.wrkref\"" 
		"  and sql_select  = "
		"     \"select pckbat.schbat, pckbat.pricod, pckbat.adddte, "
	        "	       pckbat.reldte, pckbat.cmpdte, pckwrk.wrkref, "
	        "	       pckwrk.pcksts, pckwrk.lodlvl, pckwrk.appqty, "
	        "	       pckwrk.pckqty, pckwrk.srcare, pckwrk.prtnum, "
                "	       pckwrk.prt_client_id, pckwrk.wh_id, "
	        "	       pckwrk.ship_id, pckwrk.ship_line_id, "
	        "	       pckwrk.client_id, "
	        "	       pckwrk.ordnum, pckwrk.ordlin, pckwrk.ordsln, "
	        "	       %s pckwrk.ctnnum, pckwrk.wrktyp, "
	        "	       pckwrk.rowid  \" "
                "and sql_from_where = "
		"      \" from %s shipment, pckwrk, pckbat "
	        "        where pckwrk.schbat = pckbat.schbat "
	        "          and pckwrk.appqty < pckwrk.pckqty "
	        "          and pckbat.cmpdte is null "
	        "          and pckwrk.ship_id = shipment.ship_id "
	        "          %s  %s  %s %s \"",
	        stop_select, stop_from, stop_where,
	        pckwrk_buffer, ship_buffer, stop_buffer);
	    ret_status = srvInitiateInline(buffer, &CurPtr2);
	    res = srvGetResults(CurPtr2);
    }
    else
    {
	sprintf(buffer,
		"select pckbat.schbat, pckbat.pricod, pckbat.adddte, "
		"       pckbat.reldte, pckbat.cmpdte, pckwrk.wrkref, "
		"       pckwrk.pcksts, pckwrk.lodlvl, pckwrk.appqty, "
		"       pckwrk.pckqty, pckwrk.srcare, pckwrk.prtnum, "
			"       pckwrk.prt_client_id, pckwrk.wh_id, "
		"       pckwrk.ship_id, pckwrk.ship_line_id, "
		"       pckwrk.client_id, "
		"       pckwrk.ordnum, pckwrk.ordlin, pckwrk.ordsln, "
		"       %s pckwrk.ctnnum, pckwrk.wrktyp, "
		"       pckwrk.rowid "
		" from %s shipment, pckwrk, pckbat "
			"where pckwrk.schbat = pckbat.schbat "
			"  and pckwrk.appqty < pckwrk.pckqty "
			"  and pckbat.cmpdte is null "
			"  and pckwrk.ship_id = shipment.ship_id "
			"  %s  %s  %s %s",
			stop_select, stop_from, stop_where,
			pckwrk_buffer, ship_buffer, stop_buffer);

			ret_status = sqlExecStr(buffer, &res);
    }

    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	if (CurPtr2) 
	{
	    srvFreeMemory(SRVRET_STRUCT, CurPtr2);
	    CurPtr2 = NULL;
	}
	else
	{
	    sqlFreeResults(res);			
	}
	CurPtr = (RETURN_STRUCT *) srvSetupReturn(ret_status, "");
	return (CurPtr);
    }
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
	if (CurPtr2) 
	{
	    srvFreeMemory(SRVRET_STRUCT, CurPtr2);
	    CurPtr2 = NULL;
	}
	else
	{
	    sqlFreeResults(res);			
	}

	/* Could be a replenishment...get rid of the join to the 
	   shipment */

	if (strcmp(chgmod, CHGMOD_UPDATE) == 0)
	{

	    sprintf(buffer,
		"exec sql with lock "
		"where main_table  = \"pckwrk\"" 
		"  and main_pk     = \"pckwrk.wrkref\"" 
		"  and sql_select  = "
		"      \"select pckbat.schbat, pckbat.pricod, pckbat.adddte, "
		"	        pckbat.reldte, pckbat.cmpdte, pckwrk.wrkref, "
		"	        pckwrk.pcksts, pckwrk.lodlvl, pckwrk.appqty, "
		"	        pckwrk.pckqty, pckwrk.srcare, pckwrk.prtnum, "
		"	        pckwrk.prt_client_id, pckwrk.wh_id, "
		"	        pckwrk.ship_id, pckwrk.ship_line_id, "
		"	        pckwrk.client_id, "
		"	        pckwrk.ordnum, pckwrk.ordlin, pckwrk.ordsln, "
		"	        pckwrk.ctnnum, pckwrk.wrktyp, "
		"	        pckwrk.rowid \""
		"and sql_from_where = "
		"      \" from pckwrk, pckbat "
		"	 where pckwrk.schbat = pckbat.schbat "
		"	   and pckwrk.appqty < pckwrk.pckqty "
		"	   and pckbat.cmpdte is null "
		"	%s  \"",
			pckwrk_buffer);
	    ret_status = srvInitiateInline(buffer, &CurPtr2);
	    res = srvGetResults(CurPtr2);
	}
	else
	{
	    sprintf(buffer,
		    "select pckbat.schbat, pckbat.pricod, pckbat.adddte, "
		    "       pckbat.reldte, pckbat.cmpdte, pckwrk.wrkref, "
		    "       pckwrk.pcksts, pckwrk.lodlvl, pckwrk.appqty, "
		    "       pckwrk.pckqty, pckwrk.srcare, pckwrk.prtnum, "
		    "       pckwrk.prt_client_id, pckwrk.wh_id, "
		    "       pckwrk.ship_id, pckwrk.ship_line_id, "
		    "       pckwrk.client_id, "
		    "       pckwrk.ordnum, pckwrk.ordlin, pckwrk.ordsln, "
		    "       pckwrk.ctnnum, pckwrk.wrktyp, "
		    "       pckwrk.rowid "
		    " from pckwrk, pckbat "
		    "where pckwrk.schbat = pckbat.schbat "
		    "  and pckwrk.appqty < pckwrk.pckqty "
		    "  and pckbat.cmpdte is null "
		    "  %s  ",
		    pckwrk_buffer);
	    ret_status = sqlExecStr(buffer, &res);
	}

	if (ret_status != eOK)
	{
	    if (CurPtr2) 
	    {
		srvFreeMemory(SRVRET_STRUCT, CurPtr2);
		CurPtr2 = NULL;
	    }
	    else
	    {
		sqlFreeResults(res);			
	    }
	    CurPtr = (RETURN_STRUCT *) srvSetupReturn(ret_status, "");
	    return (CurPtr);
	}
    }

    /* Loop through and create a result set entry for every work */
    /* reference or different scheduled batch based upon the number */
    /* of rows we got back ... if we are in update mode, then perform */
    /* cancel picks also */

    schbat_row = NULL;
    for (row = sqlGetRow(res), nxtrow = sqlGetNextRow(row); row;
             row = nxtrow, nxtrow = sqlGetNextRow(row))
    {
	if (res->NumOfRows < DETAILED_ROW_LIMIT)
	{
	    /* And, add to the result set */
	    if (CurPtr == NULL)
	    {
		CurPtr = srvResultsInit(eOK,
			    "chgmod", COMTYP_CHAR, strlen(CHGMOD_INQUIRY),
			    "schbat",   COMTYP_CHAR, SCHBAT_LEN,
			    "stop_id",   COMTYP_CHAR, STOP_ID_LEN,
			    "ship_id",   COMTYP_CHAR, SHIP_ID_LEN,
			    "ship_line_id",   COMTYP_CHAR, SHIP_LINE_ID_LEN,
			    "client_id",COMTYP_CHAR, CLIENT_ID_LEN,
			    "wh_id",    COMTYP_STRING, WH_ID_LEN,
			    "ordnum",   COMTYP_CHAR, ORDNUM_LEN,
			    "ordlin",   COMTYP_CHAR, ORDLIN_LEN,
			    "ordsln",   COMTYP_CHAR, ORDSLN_LEN,
			    "pricod",   COMTYP_CHAR, PRICOD_LEN,
			    "wrkref",   COMTYP_CHAR, WRKREF_LEN,
			    "pcksts",   COMTYP_CHAR, PCKSTS_LEN,
			    "srcare",   COMTYP_CHAR, ARECOD_LEN,
			    "prtnum",   COMTYP_CHAR, PRTNUM_LEN,
			    "prt_client_id", COMTYP_CHAR, CLIENT_ID_LEN,
			    "appqty",   COMTYP_INT,  sizeof(int),
			    "pckqty",   COMTYP_INT,  sizeof(int),
			    NULL);

	    }

	    srvResultsAdd(CurPtr,
			  chgmod,
			  sqlGetString(res, row, "schbat"),
			  sqlGetString(res, row, "stop_id"),
			  sqlGetString(res, row, "ship_id"),
			  sqlGetString(res, row, "ship_line_id"),
			  sqlGetString(res, row, "client_id"),
			  sqlGetString(res, row, "wh_id"),
			  sqlGetString(res, row, "ordnum"),
			  sqlGetString(res, row, "ordlin"),
			  sqlGetString(res, row, "ordsln"),
			  sqlGetString(res, row, "pricod"),
			  sqlGetString(res, row, "wrkref"),
			  sqlGetString(res, row, "pcksts"),
			  sqlGetString(res, row, "srcare"),
			  sqlGetString(res, row, "prtnum"),
			  sqlGetString(res, row, "prt_client_id"),
			  sqlGetLong(res, row, "appqty"),
			  sqlGetLong(res, row, "pckqty"));
	}
	else
	{
	    if ((schbat_row == NULL) ||
		(strncmp(sqlGetString(res, row, "schbat"),
			 sqlGetString(res, schbat_row, "schbat"), SCHBAT_LEN) != 0))
	    {
		/* Just loop through and get a total of the applied/picked */
		schbat_row = row;
		total_appqty = 0;
		total_pckqty = 0;
		total_wrkref = 0;
		for (tmprow = schbat_row; tmprow; tmprow = sqlGetNextRow(tmprow))
		{
		    if (strncmp((char *) sqlGetValue(res, tmprow, "schbat"),
			    (char *) sqlGetValue(res, schbat_row, "schbat"),
				SCHBAT_LEN) == 0)
		    {
			total_appqty += sqlGetLong(res, tmprow, "appqty");
			total_pckqty += sqlGetLong(res, tmprow, "pckqty");
			total_wrkref++;
		    }
		}

		/* And, add to the result set */

		if (CurPtr == NULL)
		{
		    CurPtr = srvResultsInit(eOK,
			      "chgmod", COMTYP_CHAR, 
			      strlen(CHGMOD_INQUIRY),
			      "schbat",   COMTYP_CHAR, SCHBAT_LEN,
			      "stop_id",   COMTYP_CHAR, STOP_ID_LEN,
			      "ship_id",   COMTYP_CHAR, SHIP_ID_LEN,
			      "ship_line_id",   COMTYP_CHAR, SHIP_LINE_ID_LEN,
			      "client_id",COMTYP_CHAR, CLIENT_ID_LEN,
			      "wh_id",    COMTYP_STRING, WH_ID_LEN,
			      "ordnum",   COMTYP_CHAR, ORDNUM_LEN,
			      "ordlin",   COMTYP_CHAR, ORDLIN_LEN,
			      "ordsln",   COMTYP_CHAR, ORDSLN_LEN,
			      "pricod",   COMTYP_CHAR, PRICOD_LEN,
			      "adddte",   COMTYP_CHAR, 20,
			      "reldte",   COMTYP_CHAR, 20,
			      "appqty",   COMTYP_INT,  sizeof(int),
			      "pckqty",   COMTYP_INT,  sizeof(int),
			      NULL);
		}

		srvResultsAdd(CurPtr,
			    chgmod,
			    sqlGetString(res, schbat_row, "schbat"),
			    sqlGetString(res, schbat_row, "stop_id"),
			    sqlGetString(res, schbat_row, "ship_id"),
			    sqlGetString(res, schbat_row, "ship_line_id"),
			    sqlGetString(res, schbat_row, "client_id"),
			    sqlGetString(res, schbat_row, "wh_id"),
			    sqlGetString(res, schbat_row, "ordnum"),
			    sqlGetString(res, schbat_row, "ordlin"),
			    sqlGetString(res, schbat_row, "ordsln"),
			    sqlGetString(res, schbat_row, "pricod"),
			    sqlGetString(res, schbat_row, "adddte"),
			    sqlGetString(res, schbat_row, "reldte"),
			    total_appqty,
			    total_pckqty);
	    }
	}
	/* If we are in update mode, first get a count of the work */
	/* references under this pick batch */
	/* if we are affecting the entire batch, simply update the */
	/* existing pick batch entry; otherwise ... */
	/* generate the new scheduled batch, insert it, and loop through */
	/* the pick works to perform the updates. */
	if (strcmp(chgmod, CHGMOD_UPDATE) == 0)
	{
	    ret_status = eOK;
	    
	    /* If we're dealing with either a kit or a part of a kit,
	       we need to make sure it is still there - a prior call
	       to cancel pick (in this loop) could have blown away
	       the wrkref.. */
	    if (strncmp(sqlGetString(res, row, "wrktyp"), WRKTYP_KIT,
			WRKTYP_LEN) == 0 || !sqlIsNull(res, row, "ctnnum"))
	    {
		if (sqlGetDataType(res, "rowid") == COMTYP_STRING)
		{
		    sprintf(buffer, "select 1 from pckwrk where rowid = '%s'",
			    sqlGetString(res, row, "rowid"));
		}
		else
		{
		    /* DB2 has a numeric rowid. Binding will handle SQL
		    ** statements, but we need to convert to C datatype here
		    */
		    sprintf(buffer, "select 1 from pckwrk where rowid = %ld",
			    sqlGetLong(res, row, "rowid"));
		}
		ret_status = sqlExecStr(buffer, NULL);
	    }

	    if (ret_status == eOK)
	    {

	    /* LBS- WMD-49199 merge. We publish canbatflg and lstpckflg to 
	     * prevent 'cancel pick' triggers from processing
	     * to improve the performance.
             * lstpckflg indicates if this is the last pick in the group.
             * The triggers on cancel pick command will use this value
             * to determine if other logic should be performed.
	     */
		sprintf(buffer,
	        " publish data where canbatflg = %d  "
		    "                and lstpckflg = %d |"
			" cancel pick where wrkref = \"%s\" ",
		    (canbatflg ? 1 : 0),
		    (!nxtrow ? 1 : 0),
			sqlGetString(res, row, "wrkref"));
		CurPtr1 = NULL;
		ret_status = srvInitiateInline(buffer, &CurPtr1);
		if (ret_status != eOK)
		{
		    srvFreeMemory(SRVRET_STRUCT, CurPtr1);
		    if (CurPtr != NULL)
			srvFreeMemory(SRVRET_STRUCT, CurPtr);
		    sqlFreeResults(res);
		    CurPtr = (RETURN_STRUCT *) srvSetupReturn(ret_status, "");
		    return (CurPtr);
		}
		srvFreeMemory(SRVRET_STRUCT, CurPtr1);

                /* if commite flag is true, we need to 
                 * commit after cancelling each pick
                 */
                if (comflg == BOOLEAN_TRUE)
                    srvCommit();
	    }
	    ret_status = eOK;
	}
    }

    if (CurPtr2) 
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr2);
	CurPtr2 = NULL;
    }
    else
    {
	sqlFreeResults(res);			
    }

    return (CurPtr);
}
