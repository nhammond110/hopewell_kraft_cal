static char    *rcsid = "$Id: trnAllocLoc.c,v 1.6 2011/12/08 07:15:48 jliang Exp $";
/*#START***********************************************************************
 * Copyright (c) 2004 RedPrairie Corporation. All rights reserved.
 *#END************************************************************************/

/***  The main entry point "trnAllocateLocation" is located at the  ***
 ***  bottom of this buffer.  All support functions are listed      ***
 ***  above it.                                                     ***/
#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "trnlib.h"

/*
 *
 * HISTORY
 * JJS 01/15/2010 - Add procedure QTHoldMixOkInLoc for logic for dealing
 *                  with mixing (custom) QT Hold product in locations.
 *
 */

/* 
 * Here's the deal with these "passes".  We set this
 * up such that we can have two varying approaches.
 * In the event that we have a "KEEP ZONE" strategy,
 * we will process on pass # 1, 3, 5, 6, 7.  If we
 * have a "BREAK ZONE" strategy, then the pass sequence
 * becomes: 1, 2, 3, 4, 7
 *
 */
#define PASS_PARTIAL_LOC_IN_SAME_VELZON     1
#define PASS_PARTIAL_LOC_IN_DIFF_VELZON     2
#define PASS_EMPTY_LOC_IN_SAME_VELZON       3
#define PASS_EMPTY_LOC_IN_DIFF_VELZON       4
#define PASS_PARTIAL_LOC_IN_DIFF_VELZON_2ND 5
#define PASS_EMPTY_LOC_IN_DIFF_VELZON_2ND   6
#define PASS_MIX_PRODUCT                    7

#define STRATEGY_TRY_PRTL_KEEP_ZONE  "FIRST-TRY-PRTL-KEEP-ZONE"
#define STRATEGY_TRY_BOTH_KEEP_ZONE  "FIRST-TRY-BOTH-KEEP-ZONE"
#define STRATEGY_TRY_PRTL_BREAK_ZONE "FIRST-TRY-PRTL-BREAK-ZONE"
#define STRATEGY_TRY_BOTH_BREAK_ZONE "FIRST-TRY-BOTH-BREAK-ZONE"
#define STRATEGY_TRY_ALL_LOCATIONS   "FIRST-TRY-ALL-LOCATIONS"
#define STRATEGY_PARTIALS_ONLY       "USE-ONLY-PARTIALS"
#define STRATEGY_EMPTIES_ONLY        "USE-ONLY-EMPTIES"

typedef struct _product_info
{
    char            lodnum[LODNUM_LEN + 1];
    char            lodlvl[LODLVL_LEN + 1];
    char            prtnum[PRTNUM_LEN + 1];
    char            prt_client_id[CLIENT_ID_LEN + 1];
    char            prtfam[PRTFAM_LEN + 1];
    char            orgcod[ORGCOD_LEN + 1];
    char            lotnum[LOTNUM_LEN + 1];
    char            revlvl[REVLVL_LEN + 1];
    char            supnum[SUPNUM_LEN + 1];
    char            invsts[INVSTS_LEN + 1];
    char            ftpcod[FTPCOD_LEN + 1];
    long            untpal;
    long            untcas;
    long            untpak;

    char            fifdte[30];
    char            disfif[FLAG_LEN +1]; /* Disable FIFO check for repls */
    long            fifwin;	/* on the prtmst_view */
    char            timcod[TIMCOD_LEN + 1];
    long            totqty;
    long            mixed;
    moca_bool_t     empty;
    moca_bool_t     asset_track_enabled;

    double          caslen;
    double          caswid;
    double          cashgt;
    long            caslvl;
    double          locqvl;
    double          loclen;

    long            pal_stck_hgt;

    char            velzon[VELZON_LEN + 1];

    char            reqare[ARECOD_LEN + 1];
    char            reqzon[WRKZON_LEN + 1];
    char            stotyp[30];

    char            curare[ARECOD_LEN + 1];
    char            curzon[WRKZON_LEN + 1];
    char            cur_bldg_id[BLDG_ID_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];

    char            asset_typ[ASSET_TYP_LEN + 1];
    double          asset_len; /* asset length. */
    double          asset_wid; /* asset width. */
    double          asset_hgt; /* asset height. */
    double          asset_max_vol; /* maximum volume. */
    /*
     *  The invmov_typ contains the type of the inventory movement
     *  it can be PICK, TRNS, RPLN, RCV which show the reason of the 
     *  movement.
     */    
    char            invmov_typ[INVMOV_TYP_LEN + 1];
    
    char	    xdkref[XDKREF_LEN + 1];
    long	    xdkqty;

    /* WMD-37244
     * If distro_id is not null, the inventory is for a distro,
     * directed putaway will allocate a put-to-store location for it.
     */
    char            distro_id[DISTRO_ID_LEN + 1];

    struct _product_info *next;
}
PRODUCT;

typedef struct _intare_list
{
    char            arecod[ARECOD_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    char            aretyp[RTSTR1_LEN + 1];
    struct _intare_list *next;
}
INTARE_LIST;

typedef struct _aisle_list
{
    char            aisle_id[AISLE_ID_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    long            qvl;
    struct _aisle_list *next;
}
AISLE_LIST;

typedef struct _area_list
{
    char            arecod[ARECOD_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];

    /* all the area flags */
    moca_bool_t     sigflg;
    moca_bool_t     lodflg;
    moca_bool_t     subflg;
    moca_bool_t     dtlflg;
    moca_bool_t     fifflg;
    moca_bool_t     putprox_flg;
    char            pckcod[PCKCOD_LEN + 1];
    char            loccod[LOCCOD_LEN + 1];

    char            strategy[RTSTR2_LEN + 1];
    char           *zone_exclusion;
    long            zones_checked;
    INTARE_LIST    *intare;
    long            pass_count;

    mocaDataRes    *EmptyRes;
    mocaDataRow    *EmptyRowZonesGreater;
    mocaDataRes    *PartialRes;
    mocaDataRow    *PartialRowZonesGreater;

    struct _area_list *next;
    struct _area_list *prev;

    long            minlvl;
    long            maxlvl;
    long            curlvl;
    long            max_loads_aisle;
    moca_bool_t     tmpflg;

    /* the area is configged for part or part family
     * or client or asset_typ or default
     */
    moca_bool_t     conf_prt;
    moca_bool_t     conf_prtfam;
    moca_bool_t     conf_client;
    moca_bool_t     conf_asset_typ;
    moca_bool_t     conf_supnum;
    moca_bool_t     conf_none;
    moca_bool_t     share_loc_flg;
}
AREA_LIST;

typedef struct _pass_stoloc_list
{
    mocaDataRes     *res;
    mocaDataRow     *row;
    /* we need save area information, because if we cannot
     * find the locations in this area, we should go to next 
     * area to try to find other available locations.
     */
    AREA_LIST       *area;
    struct _pass_stoloc_list *next;
}
PASS_STOLOC_LIST;

static long     Trace;
static FILE    *fptr;
static AISLE_LIST  *aisle_list;
static PASS_STOLOC_LIST *pass_stoloc_list;

void trnFreeStolocList(STOLOC_LIST * LocList)
{
    /*   Local Variables   */

    STOLOC_LIST    *ptr;
    STOLOC_LIST    *ptr1;

    ptr = LocList;

    while (ptr != NULL)
    {
	ptr1 = (STOLOC_LIST *) (ptr->next);
	free(ptr);
	ptr = ptr1;
    }

    LocList = NULL;
}

static void FreeAisleList(void)
{
    AISLE_LIST *aisle = NULL;
    
    if (aisle_list == NULL)
        return;

    while(aisle_list)
    {
        aisle = aisle_list;
        aisle_list = aisle_list->next;
        
        free(aisle);
    }
    
    aisle_list = NULL;
}

static void OpenTraceFile(moca_bool_t trcflg, char *lodnum_i)
{
    char varbuf[1024];
    char trcfil[100];

    Trace = 0;
    if (trcflg == BOOLEAN_TRUE)
    {
	Trace = 1;
    }

    if (!Trace)
	return;

    sprintf(trcfil, "%s/AllocLoc.Sts-%s", 
	    misExpandVars(varbuf, LES_LOG, sizeof(varbuf), NULL),
	    lodnum_i);
    fptr = fopen(trcfil, "w");
    if (fptr)
    {
	fprintf(fptr, "Beginning allocate location \n");
	fflush(fptr);
    }
    else
    {
	Trace = 0;
    }
}

static void CloseTraceFile()
{
    if (!Trace)
	return;

    if (fptr)
    {
	fprintf(fptr, "Leaving allocate location \n");
	fflush(fptr);
	fclose(fptr);
	fptr = NULL;
    }

    Trace = 0;
    return;
}
/*
 * For a given lodnum we will check and get the volume for the
 * inventory, if there is no asset_typ of container_flg is
 * set for the asset_typ on invlod and invsub.
 */
static double GetActualVolumeForInventoryIfAssetExists(char *lodnum,
                                                       double locqvl)
{
    double       vol = 0.0;
    char         buffer[2000];
    long         ret_status;
    mocaDataRes *res;
    mocaDataRow *row;

    memset(buffer, 0, sizeof(buffer));

    /*
     * This query contains an inner query."uniqueness_query"
     *  . uniqueness_query will return dtl_is_serialized.
     *    dtl_is_serialized will be 1,
     *    if there is one dtlnum for a subnum else
     *    dtl_is_serialized will be 0.
     * Based on uniqueness_query.dtl_is_serialized the volume
     * will be calculated. 
     * If it is 1 then volume will be
     * volume of ftpcod's case attributes * 
     *           (invdtl.untqty * 1.0 / invdtl.untcas).
     * If uniqueness_query.dtl_is_serialized > 1 then volume 
     * will be sum of 1 and 2
     *  1. volume of ftpcod's case attributes * 
     *           (invdtl.untqty * 1.0 / invdtl.untcas).
     *  2. volume of ftpcod's unit attributes *
     *           (mod(invdtl.untqty,invdtl.untcas)
     * Also there are two more inner queries in the
     * where clause of the main select statement to
     * check the asset_typ on invlod and invsub are
     * not a container if asset_typ exists.
     */
    sprintf(buffer, 
        " select sum(decode(uniqueness_query.dtl_is_serialized, "
        "        1,((ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt) "
        "        * (invdtl.untqty * 1.0 / invdtl.untcas)), "
        "        ((ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt) "
        "        * floor(invdtl.untqty * 1.0 / invdtl.untcas) "
        "        + ((ftpmst.untlen * ftpmst.untwid * ftpmst.unthgt) "
        "        * (mod(invdtl.untqty,invdtl.untcas)))))) vol "
        "   from invlod, "
        "        invsub, "
        "        invdtl, "
        "        ftpmst, "
        "        (select decode (un.invdtl_count,1,0,1) dtl_is_serialized, "
        "                un.subnum "
        "           from (select count (*) invdtl_count, "
        "                        invdtl.subnum "
        "                   from invdtl "
        "               group by invdtl.subnum) un ) uniqueness_query "
        "  where invlod.lodnum = invsub.lodnum "
        "    and invsub.subnum = invdtl.subnum "
        "    and invdtl.ftpcod = ftpmst.ftpcod "
        "    and not exists (select 1 "
        "                      from asset_typ "
        "                     where asset_typ = invlod.asset_typ "
        "                       and container_flg = 1) "
        "    and not exists (select 1 "
        "                      from asset_typ "
        "                     where asset_typ = invsub.asset_typ "
        "                       and container_flg = 1) "
        "    and invlod.lodnum = '%s' "
        "    and uniqueness_query.subnum = invsub.subnum ", 
        lodnum);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        row = sqlGetRow(res);
        vol = sqlGetFloat(res, row, "vol");
    }
    else
    {
        /* If the above query returns not eOK
         * then return the locqvl that already exists
         * on product pointer.
         */
        vol = locqvl;
    }

    sqlFreeResults(res);
    return vol;
}

/*
 * For a given lodnum we will check and get the volume based on the 
 * asset details in invsub.
 * We will do this if we are allocating a load and we want to know
 * how much volume it will occupy including the asset charecteristics.
 * container flg and asset dimension for the asset_typ in invsub.
 * Also take into account that multiple subs can be in a single asset.
 * multiple asset_link entries for a single asst_id with asset_num = subnum.
 */
static double GetSubAssetVolume(char *lodnum)
{
    double       sub_asset_vol = 0.0;
    char         buffer[2000];
    long         ret_status;
    mocaDataRes *res;
    mocaDataRow *row;

    memset(buffer, 0, sizeof(buffer));

    /*
     * Get the volume for all assets on invsub.
     * Join asset_typ with sub_ast
     * sub_ast = Serialized asset UNION Non Serialized Asset on invsub.
     *     1. Serialized assets on invsub
     *        We need to get the unique asset_typ volume, since more than
     *        one subnum can have a single asset_id(serialized asset).
     *     2. Non Serailized asset
     *        Get all the Non Serailized asset on invsub, Since each sub will
     *        be on single Non Serailized asset.
     * Now based on the container flg of the asset_typ get the max_vol or
     * Calculate the volume of the asset (len*hgt*wid).
     */
    sprintf(buffer, 
        " select sum(decode(asset_typ.container_flg, "
        "            0, asset_typ.asset_len * "
        "               asset_typ.asset_wid * "
        "               asset_typ.asset_hgt  ,"
        "            1, asset_typ.max_vol)) asset_vol "
        "   from asset_typ, "
        "        ((select distinct asset_id, "
        "                 invsub.asset_typ "
        "            from invlod, "
        "                 invsub, "
        "                 asset_link"
        "           where invlod.lodnum = invsub.lodnum "
        "             and invsub.subnum = asset_link.asset_num "
        "             and invlod.lodnum = '%s')"
        "          union "
        "         (select '', "
        "                 invsub.asset_typ "
        "            from invlod, "
        "                 invsub, "
        "                 asset_typ "
        "           where invlod.lodnum = invsub.lodnum "
        "             and invsub.asset_typ = asset_typ.asset_typ "
        "             and asset_typ.ser_flg = 0 "
        "             and invlod.lodnum = '%s')) sub_ast "
        "  where sub_ast.asset_typ = asset_typ.asset_typ",
        lodnum, lodnum);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        row = sqlGetRow(res);
        sub_asset_vol = sqlGetFloat(res, row, "asset_vol");
    }
    else
    {
        sub_asset_vol = 0.0;
    }

    sqlFreeResults(res);
    return sub_asset_vol;
}
/*
 * Get the height and length of the subasset.
 */
static void GetSubAssetLenAndHgt(char *lodnum, 
                                 double *sub_asset_hgt_o, 
                                 double *sub_asset_len_o)
{
    char         buffer[2000];
    long         ret_status;
    mocaDataRes *res;
    mocaDataRow *row;

    memset(buffer, 0, sizeof(buffer));

    sprintf(buffer, 
                " select max(asset_typ.asset_hgt) sub_asset_hgt, "
                "        max(asset_typ.asset_len) sub_asset_len "
                "   from invsub, "
                "        asset_typ "
                "  where invsub.asset_typ is not null "
                "    and invsub.asset_typ = asset_typ.asset_typ "
                "    and invsub.lodnum = '%s' ", lodnum);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        row = sqlGetRow(res);
        *sub_asset_hgt_o = sqlGetFloat(res, row, "sub_asset_hgt");
        *sub_asset_len_o = sqlGetFloat(res, row, "sub_asset_len");
    }
    else
    {
        *sub_asset_hgt_o = 0.0;
        *sub_asset_len_o = 0.0;
    }
    sqlFreeResults(res);
}
static void sLog(long action, char *fmt, ...)
{
    va_list Arguments;

    /* disabled */
    
    if (!action)
    {
	/* Write and flush the file... */

	va_start(Arguments, fmt);
	misVTrc(T_FLOW, fmt, Arguments);
	if (fptr)
	{
	    vfprintf(fptr, fmt, Arguments);
	    fprintf(fptr, "\n");
	    fflush(fptr);
	}
	va_end(Arguments);
	fflush(stdout);
    }
    
    return;
}

static void SetupArea(AREA_LIST * area, mocaDataRes * res, mocaDataRow * row)
{

    strncpy(area->arecod, sqlGetValue(res, row, "arecod"), ARECOD_LEN);
    strncpy(area->wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
    area->sigflg = sqlGetBoolean(res, row, "sigflg");
    area->lodflg = sqlGetBoolean(res, row, "lodflg");
    area->subflg = sqlGetBoolean(res, row, "subflg");
    area->dtlflg = sqlGetBoolean(res, row, "dtlflg");
    area->fifflg = sqlGetBoolean(res, row, "fifflg");
    area->putprox_flg = sqlGetBoolean(res, row, "putprox_flg");

    strncpy(area->pckcod,
	    sqlGetValue(res, row, "pckcod"), PCKCOD_LEN);
    strncpy(area->loccod,
	    sqlGetValue(res, row, "loccod"), LOCCOD_LEN);

    if (!sqlIsNull(res, row, "share_loc_flg"))
        area->share_loc_flg = sqlGetLong(res, row, "share_loc_flg");
    else
        area->share_loc_flg = 0;

    if (!sqlIsNull(res, row, "rtstr2"))
	strncpy(area->strategy,
		sqlGetValue(res, row, "rtstr2"), RTSTR2_LEN);
    if (!sqlIsNull(res, row, "rtnum1"))
	area->minlvl = sqlGetLong(res, row, "rtnum1");

    if (!sqlIsNull(res, row, "rtnum2"))
	area->maxlvl = sqlGetLong(res, row, "rtnum2");

    if (!sqlIsNull(res, row, "rtflt2"))
    area->max_loads_aisle = sqlGetLong(res, row, "rtflt2");

    area->curlvl = -1;
}

static void RemoveDuplAreas(AREA_LIST * head)
{
    AREA_LIST      *ap, *rem_ap;
    AREA_LIST      *cur;

    /* So we start at the beginning of the list and look for
       duplicates further down the list.  If we find them,
       then we remove them from the chain and free them.  Because
       of the way the for loop works, the free is delayed until
       after we deference to advance the loop */
    for (cur = head; cur; cur = cur->next)
    {
	rem_ap = NULL;
	for (ap = cur->next; ap; ap = ap->next)
	{
	    if (rem_ap)
	    {
		free(rem_ap);
		rem_ap = NULL;
	    }

	    if (strncmp(ap->arecod, cur->arecod, ARECOD_LEN) == 0 &&
	        strncmp(ap->wh_id, cur->wh_id, WH_ID_LEN) == 0)
	    {
		/* If we don't have a strategy and this one does, then
		   snag the strategy from it.. */
		if (misTrimLen(cur->strategy, RTSTR2_LEN) == 0 &&
			misTrimLen(ap->strategy, RTSTR2_LEN) != 0)
		    strcpy(cur->strategy, ap->strategy);

		/* If min or max is greater than current, reset it */
		if (cur->minlvl < ap->minlvl)
		    cur->minlvl = ap->minlvl;
		if (cur->maxlvl < ap->maxlvl)
		    cur->maxlvl = ap->maxlvl;
                if (cur->max_loads_aisle < ap->max_loads_aisle)
                    cur->max_loads_aisle = ap->max_loads_aisle;

		if (ap->prev)
		    ap->prev->next = ap->next;
		if (ap->next)
		    ap->next->prev = ap->prev;
		rem_ap = ap;
	    }
	}
	if (rem_ap)
	    free(rem_ap);
    }

}

/* This function is to convert the each level quality to load level */
static long ConvertToLoads(char *prtnum,
                           char *prt_client_id,
                           long untqty)
{
    char            buffer[2000];
    mocaDataRow    *row = NULL;
    mocaDataRes    *res = NULL;
    long            ret_status;
    long			lodqty = 0;

    memset(buffer, 0, sizeof(buffer));
    
    /* convert the untqty to load quality */
    if (untqty > 0)
    {
        sprintf(buffer,
                " select ceil(%d/untpal) lodqty"
                "   from prtmst_view "
                "  where prtnum = '%s' "
                "    and prt_client_id = '%s' ",
                untqty,
                prtnum,
                prt_client_id);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status == eOK)
        {
            row = sqlGetRow(res);
            lodqty = sqlGetLong(res, row, "lodqty");
            if (lodqty == 0)
                lodqty = 1;
        }
    }

    return lodqty;	
}

/* This function summarizes the level of inventory for an area and
 * supplier-client. The function is called from GetInventoryLevels and
 * GetAisleQvl in order to process min/max location and aisle load levels for
 * areas specified in preferred area storage policies. Because invsum does not
 * track inventory at a location by distinct suppliers, this special query is
 * required, unlike processing required for policies using other storage policy
 * options. */
static long GetInventoryLevelsBySupplier(AREA_LIST *ap,
    PRODUCT *prd,
    long *lodqty)
{
    char            buffer[3000];
    mocaDataRow    *row = NULL;
    mocaDataRes    *res = NULL;
    RETURN_STRUCT  *CurPtr = NULL;
    long            untqty = 0L;
    long            pndqty = 0L;
    long            ret_status = eOK;

    memset(buffer, 0, sizeof(buffer));

    /* The processing applies only to areas specified by the Supplier Number
     * storage policy option. */
    if (!ap->conf_supnum)
    {
        return eOK;
    }

    /* Invsum does not track inventory by supplier. We will query for all
     * supplier-client inventory in the area to determine the load count.
     * Pending quantity will be obtained from invmov and pckmov. */
    sprintf(buffer,
        "publish data"
        "  where prtnum='%s'"
        "    and prt_client_id='%s'"
        "    and supnum='%s'"
        "    and arecod='%s'"
        "    and wh_id='%s'"
        "    and lodlvl_l='%s'"
        "    and lodlvl_s='%s'"
        "    and lodlvl_d='%s'"
        " |"
        " [select sum(untqty) untqty,"
        "         sum(pndqty) pndqty"
        "    from (select untqty,"
        "                 0 pndqty"
        "            from locmst,"
        "                 invlod,"
        "                 invsub,"
        "                 invdtl"
        "           where locmst.arecod=@arecod"
        "             and locmst.wh_id=@wh_id"
        "             and invlod.wh_id=locmst.wh_id"
        "             and invlod.stoloc=locmst.stoloc"
        "             and invsub.lodnum=invlod.lodnum"
        "             and invdtl.subnum=invsub.subnum"
        "             and invdtl.prtnum=@prtnum"
        "             and invdtl.prt_client_id=@prt_client_id"
        "             and invdtl.supnum=@supnum"
        ""
        "           union"
        "          select 0 untqty,"
        "                 sum(pckqty - appqty) pndqty"
        "            from pckwrk"
        "           where pckqty > appqty"
        "             and prtnum=@prtnum"
        "             and prt_client_id=@prt_client_id"
        "             and exists"
        "                 (select 1"
        "                    from pckmov"
        "                   where pckmov.cmbcod=pckwrk.cmbcod"
        "                     and (pckmov.arecod=pckwrk.dstare"
        "                          or"
        "                          pckmov.stoloc=pckwrk.dstloc))"
        "             and exists"
        "                 (select 1"
        "                    from pckmov"
        "                   where pckmov.cmbcod=pckwrk.cmbcod"
        "                     and stoloc=@stoloc"
        "                     and wh_id=@wh_id"
        "                     and prcqty=0"
        "                     and arrqty=0)"
        ""
        "           union"
        "           select 0 untqty,"
        "                  sum(invdtl.untqty) pndqty"
        "             from invdtl,"
        "                  invsub,"
        "                  invlod,"
        "                  locmst,"
        "                  invmov"
        "            where invdtl.prtnum=@prtnum"
        "              and invdtl.prt_client_id=@prt_client_id"
        "              and invsub.subnum=invdtl.subnum"
        "              and invlod.lodnum=invsub.lodnum"
        "              and locmst.stoloc=invlod.stoloc"
        "              and locmst.wh_id=invlod.wh_id"
        "              and locmst.arecod=@arecod"
        "              and locmst.wh_id=@wh_id"
        "              and invmov.wh_id=locmst.wh_id"
        "              and invmov.stoloc=locmst.stoloc"
        "              and (   (    invmov.lodlvl=@lodlvl_l"
        "                       and invmov.lodnum=invlod.lodnum)"
        "                   or (    invmov.lodlvl=@lodlvl_s"
        "                       and invmov.lodnum=invsub.subnum)"
        "                   or (    invmov.lodlvl=@lodlvl_d"
        "                       and invmov.lodnum=invdtl.dtlnum))) a]",
        prd->prtnum,
        prd->prt_client_id,
        prd->supnum,
        ap->arecod,
        ap->wh_id,
        LODLVL_LOAD,
        LODLVL_SUB,
        LODLVL_DTL);
    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
    {
        /* Log a message but continue processing. This error is
         * ignored for other policy options also; the impact will
         * only be on area reprioritization by BubbleUpReplens or
         * BubbleDownOverage. Normal location allocation will not
         * be affected. */
        sLog(0, "Unable to determine load count for area='%s' and supnum='%s'."
            " Reprioritization of area by min/max location and aisle load"
            " levels will be skipped during location allocation.", ap->arecod,
            prd->supnum);
    }
    else if (eOK == ret_status && CurPtr)
    {
        /* The query returns 0 or 1 row(s) */
        res = srvGetResults(CurPtr);
        if (sqlGetNumRows(res) > 0)
        {
            row = sqlGetRow(res);
            untqty = 0L;
            pndqty = 0L;
            if (!sqlIsNull(res, row, "untqty"))
            {
                untqty = sqlGetLong(res, row, "untqty");
            }
            if (!sqlIsNull(res, row, "pndqty"))
            {
                pndqty = sqlGetLong(res, row, "pndqty");
            }
            if ((untqty + pndqty) > 0L)
            {
                *lodqty = ConvertToLoads(prd->prtnum,
                    prd->prt_client_id,
                    untqty + pndqty);
            }
        }
    }
    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    return eOK;
}

/* This function summarizes the level of inventory for a given set of areas
   for a given part.  This is done only if min/max levels are specified
   on the policies for this part/part family/supplier-client.  */

static long GetInventoryLevels(AREA_LIST * head, PRODUCT * prd)
{
    char            buffer[2000];
    char            sqlbuftbl[500];
    char            sqlbufnone[500];
    char            sqlbufprt[500];
    char            sqlbufprtfam[500];
    char            sqlbufclient[500];
    char            sqlbuf[5000];
    mocaDataRow    *row = NULL;
    mocaDataRes    *res = NULL;

    AREA_LIST      *ap;
    char            area_string[200];
    long            ret_status;
    long            untqty = 0;
    long            pndqty = 0;
    long            lodqty = 0;
    moca_bool_t     no_areas = BOOLEAN_TRUE;

    /* If we have a mixed part pallet load, get out */
    if (prd->mixed == TRUE)
    return (eOK);

    memset(buffer, 0, sizeof(buffer));
    memset(sqlbuf,      0, sizeof(sqlbuf));
    memset(area_string, 0, sizeof(area_string));
    memset(sqlbufnone,  0, sizeof(sqlbufnone));
    memset(sqlbufprt,   0, sizeof(sqlbufprt));
    memset(sqlbufprtfam,0, sizeof(sqlbufprtfam));
    memset(sqlbufclient,0, sizeof(sqlbufclient));
    memset(sqlbuftbl,   0, sizeof(sqlbuftbl));
    
    sLog(0, "Attempting to load inventory levels..");

    for (ap = head; ap; ap = ap->next)
    {
        if (ap->curlvl == -1 && (ap->minlvl || ap->maxlvl))
        {
            ap->curlvl = 0;

            /* 
             * Here we should build the sql clause.
             * This could be configured to check inventory levels by
             * prtnum or prtfam or supplier or client_id or any combination of
             * these.
             *
             * Notice, when looking at these 4 configurations,
             * this is NOT an "AND" relationship, they
             * are "OR" relationship.
             * If any one of these is configured, then join it to the whole
             * sql clause.
             *
             * Look at the second sql buffer, if it is configged by prtfam.
             * we have to join to prtmst_view table, so I add a sqlbuftbl variable
             * to hold the table name.
             */
            memset(sqlbuf, 0, sizeof(sqlbuf));

            if (ap->conf_none)
            {
                sprintf(sqlbufnone,
                        " (im.arecod = '%s' "
                        "     and im.wh_id = '%s') ",
                        ap->arecod,
                        ap->wh_id);
                strcat(sqlbuf, sqlbufnone);
            }

            if (ap->conf_prt)
            {
                if (strlen(sqlbuf) == 0)
                {
                    sprintf(sqlbufprt,
                        " (im.arecod = '%s' "
                        "     and im.wh_id = '%s' "
                        "     and im.prtnum = '%s' "
                        "     and im.prt_client_id = '%s' "
                        "     and im.wh_id = '%s') ",
                        ap->arecod,
                        ap->wh_id,
                        prd->prtnum,
                        prd->prt_client_id,
                        prd->wh_id);
                }
                else
                {
                    sprintf(sqlbufprt,
                            " or (im.arecod = '%s' "
                            "     and im.wh_id = '%s' "
                            "     and im.prtnum = '%s' "
                            "     and im.prt_client_id = '%s' "
                            "     and im.wh_id = '%s') ",
                            ap->arecod,
                            ap->wh_id,
                            prd->prtnum,
                            prd->prt_client_id,
                            prd->wh_id);
                }
                strcat(sqlbuf, sqlbufprt);                
            }

            if (ap->conf_prtfam)
            {
                if (strlen(sqlbuf) == 0)
                {
                    sprintf(sqlbufprtfam,
                            " (im.arecod = '%s' "
                            "     and im.wh_id = '%s' "
                            "     and pm.prtfam = '%s') ",
                            ap->arecod,
                            ap->wh_id,
                            prd->prtfam);
                }
                else
                {
                    sprintf(sqlbufprtfam,
                            " or (im.arecod = '%s' "
                            "     and im.wh_id = '%s' "
                            "     and pm.prtfam = '%s') ",
                            ap->arecod,
                            ap->wh_id,
                            prd->prtfam);
                }
                strcat(sqlbuf, sqlbufprtfam);
                        
                sprintf(sqlbuftbl,
                       " %s ",
                       " right outer join prtmst_view pm "
                       " on pm.prtnum = im.prtnum  "
                       " and pm.wh_id = im.wh_id  "
                       " and pm.prt_client_id = im.prt_client_id ");
            }

            if (ap->conf_supnum)
            {
                /* Ignore ret_status, see the function for details. */
                lodqty = 0L;
                ret_status = GetInventoryLevelsBySupplier(ap, prd, &lodqty);
                if (lodqty > 0L)
                {
                    ap->curlvl += lodqty;
                    no_areas = BOOLEAN_FALSE;
                }
            }

            if (ap->conf_client)
            {
                if (strlen(sqlbuf) == 0)
                {
                    sprintf(sqlbufclient,
                            " (im.arecod = '%s' "
                            "     and im.wh_id = '%s' "
                            "     and im.prt_client_id = '%s') ",
                            ap->arecod,
                            ap->wh_id,
                            prd->prt_client_id);
                }
                else
                {
                    sprintf(sqlbufclient,
                            " or (im.arecod = '%s' "
                            "     and im.wh_id = '%s' "
                            "     and im.prt_client_id = '%s') ",
                            ap->arecod,
                            ap->wh_id,
                            prd->prt_client_id);
                }
                strcat(sqlbuf, sqlbufclient);                
            }

            /* If only ap->conf_supnum is set, sqlbuf would be empty and the
             * following query should be skipped. So check sqlbuf before
             * continuing.*/
            if (strlen(sqlbuf) == 0)
            {
                continue;
            }

            sprintf(buffer,
                    " select im.wh_id, im.arecod, im.untqty, "
                    "        im.pndqty, im.prtnum, im.prt_client_id"
                    "   from invsum im "
                    " %s "
                    " where %s ",
                    sqlbuftbl,
                    sqlbuf);
            ret_status = sqlExecStr(buffer, &res);

            if (ret_status != eOK)
            {
                sqlFreeResults(res);
                break;
            }
            
            for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                if (!sqlIsNull(res, row, "untqty"))
                {
                    /* we need to know how many loads in the aisles
                     * so convert the unit quality to loads by different
                     * part
                     */                    
                    untqty = sqlGetLong(res, row, "untqty");
                    pndqty = sqlGetLong(res, row, "pndqty");
                    ap->curlvl = ap->curlvl + ConvertToLoads(
                                    sqlGetString(res, row, "prtnum"),
                                    sqlGetString(res, row, "prt_client_id"),
                                    untqty+pndqty);
                    no_areas = BOOLEAN_FALSE;
                }
            }

            if (res) sqlFreeResults(res);
        }
    }

    if (no_areas)
        return (eDB_NO_ROWS_AFFECTED);

    return (eOK);
}

/*  "Bubble" areas needing replenishments up to the top of the search
    list.  This function will look at the inventory levels and if a
    minlvl is specified for an area and the area is below that minlvl,
    then it will bet sent to the top of the list.  We start at the 
    bottom of the list and "sort" our way up.  This guarentees that
    we maintain the priority if multiple areas need replenishment... */
static long BubbleUpReplens(AREA_LIST ** head, PRODUCT * prd)
{
    AREA_LIST      *ap, *newtop, *cur_prev;

    /* If we have a mixed part pallet load, get out */
    if (prd->mixed == TRUE)
	return (eOK);

    /* First go to the end of the list...we start at the end and work 
       our way back...this way keep relative priorities if we bubble
       up multiple areas.... */
    for (ap = *head; ap->next; ap = ap->next)
	ap->tmpflg = BOOLEAN_FALSE;

    /* if the head is the end, then there's nothing to do... */
    if (ap == *head)
	return (eOK);

    newtop = *head;

    GetInventoryLevels(newtop, prd);
    do
    {
	/* If we hit an area we've processed before, we must have
	   come full circle...bail out... */
	if (ap->tmpflg)
	    break;

	ap->tmpflg = BOOLEAN_TRUE;

	if (ap->minlvl > 0 && ap->curlvl < ap->minlvl)
	{
	    sLog(0, " --->>>>  Current Level is %ld, minimum "
		    " is %ld - bumping up...",
		    ap->curlvl, ap->minlvl);

	    /* remove from the list */
	    if (ap != newtop)
	    {
		if (ap->next)
		    ap->next->prev = ap->prev;
		if (ap->prev)
		    ap->prev->next = ap->next;

		/* send to the top */
		ap->next = newtop;
		newtop->prev = ap;
		cur_prev = ap->prev;
		ap->prev = NULL;
		newtop = ap;
	    }
	    else
	    {
		cur_prev = ap->prev;
	    }
	    /* Force the strategy to be TRY BOTH */
	    memset(ap->strategy, 0, sizeof(ap->strategy));
	    strcpy(ap->strategy, STRATEGY_TRY_BOTH_KEEP_ZONE);
	    ap = cur_prev;
	}
	else
	{
	    ap = ap->prev;
	}

    }
    while (ap);


    *head = newtop;

    return (eOK);
}
/*  This routine moves areas with overage inventory to the end of the 
    search list.  List "BubbleUpReplens", this only operates if maxlvl
    is specified for this area.  */
static long BubbleDownOverage(AREA_LIST ** head, PRODUCT * prd)
{
    AREA_LIST      *newtop, *ap, *tail, *cur_next;

    /* If we have a mixed part pallet load, get out */
    if (prd->mixed == TRUE)
	return (eOK);

    for (ap = *head; ap; ap->tmpflg = BOOLEAN_FALSE, ap = ap->next)
	tail = ap;

    /*tail = 0; */
    newtop = *head;
    GetInventoryLevels(newtop, prd);

    ap = *head;
    do
    {
	if (ap->tmpflg)
	    break;

	ap->tmpflg = BOOLEAN_TRUE;

	if (ap->maxlvl > 0 && ap->curlvl >= ap->maxlvl)
	{
	    sLog(0, " --->>>>  Current Level is %ld, maximum "
		    " is %ld - bumping down...",
		    ap->curlvl, ap->maxlvl);

	    /* Remove from list - adjust top of list ptr, etc... */

	    if (ap->prev)
		ap->prev->next = ap->next;
	    else
		newtop = ap->next;

	    if (ap->next)
		ap->next->prev = ap->prev;

	    cur_next = ap->next;


	    /* Now tack on the end of the list... */
	    tail->next = ap;
	    ap->prev = tail;
	    ap->next = NULL;
	    tail = ap;

	    ap = cur_next;
	}
	else
	{
	    ap = ap->next;
	}
    }
    while (ap);


    *head = newtop;

    return (eOK);
}

static void FreeAreaList(AREA_LIST * head)
{
    AREA_LIST      *ap, *last_ap;
    INTARE_LIST    *ptr, *ptr1;

    if(head == NULL)
        return;

    last_ap = NULL;
    for (ap = head; ap; ap = ap->next)
    {
	if (last_ap)
	    free(last_ap);

	if (ap->PartialRes)
	    sqlFreeResults(ap->PartialRes);
	if (ap->EmptyRes)
	    sqlFreeResults(ap->EmptyRes);
	if (ap->intare)
	{
	    ptr = ap->intare;
	    while (ptr != NULL)
	    {
		ptr1 = (INTARE_LIST *) (ptr->next);
		free(ptr);
		ptr = ptr1;
	    }
	    ap->intare = NULL;
	}
	if (ap->zone_exclusion)
	    free(ap->zone_exclusion);
	last_ap = ap;
    }
    if (last_ap)
	free(last_ap);

    return;
}

static void FreeProdList(PRODUCT * head)
{
    PRODUCT        *ap, *last_ap;

    last_ap = NULL;
    for (ap = head; ap; ap = ap->next)
    {
	if (last_ap)
	    free(last_ap);
	last_ap = ap;
    }
    if (last_ap)
	free(last_ap);

    return;
}

/* Load the assigned location search list into Ptr_Assign_Loc.
 * If no locations defined, clear up Ptr_Assign_Loc and
 * return eDB_NO_ROWS_AFFECTED.
 * 
 * For VMI inventory, the inventory identifier passed to 'get storage policy
 * suffixes' determines whether the location storage policies using Consigned
 * Inventory suffix are applicable. If so, the Consigned Inventory suffix is
 * returned so that matching polices can be retrieved, see policies ranked 3 in
 * the for-loop below. Once the location preferences are loaded, processing for
 * location allocation proceeds as for any other policy option.
 * 
 * For assigned locations, see policy STORE-ASG-LOC or STORE-ASG-LOC-[suffix]
 */
static long LoadAssignedLocationSearchList(RETURN_STRUCT **Ptr_Assign_Loc,
                                           PRODUCT *prd,
                                           char *bldg_id,
                                           char *wh_id)
{
    RETURN_STRUCT  *CurPtr = NULL;
    mocaDataRow    *RetRow = NULL;
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL;
    
    long            ret_status;
    char            buffer[4000];
    
    char            lodnum[LODNUM_LEN + 1];
    char            subnum[SUBNUM_LEN + 1];
    char            dtlnum[DTLNUM_LEN + 1];
    char            suffix[RTSTR1_LEN + 1];
    char            dash[2];
    char            bldg_clause[200];
    char            temp_buffer[200];

    memset(buffer, '\0', sizeof(buffer));
    memset(lodnum, '\0', sizeof(lodnum));
    memset(subnum, '\0', sizeof(subnum));
    memset(dtlnum, '\0', sizeof(dtlnum));
    memset(suffix, '\0', sizeof(suffix));
    memset(dash, '\0', sizeof(dash));
    memset(bldg_clause, '\0', sizeof(bldg_clause));
    memset(temp_buffer, '\0', sizeof(temp_buffer));

    sLog(0, "\nLoading Assigned locations for building %s/%s", wh_id, bldg_id);
    
    *Ptr_Assign_Loc = NULL;

    if (strcmp(prd->lodlvl, LODLVL_LOAD) == 0)
        strcpy(lodnum, prd->lodnum);
    else if (strcmp(prd->lodlvl, LODLVL_SUBLOAD) == 0)
        strcpy(subnum, prd->lodnum);
    else if (strcmp(prd->lodlvl, LODLVL_DETAIL) == 0)
        strcpy(dtlnum, prd->lodnum);
    
    if (misTrimLen(prd->reqare, ARECOD_LEN) > 0)
        sprintf(temp_buffer, " and aremst.arecod = '%s' ", prd->reqare);   
          
    if (misTrimLen(bldg_id, BLDG_ID_LEN) > 0)
        sprintf(bldg_clause, " and aremst.bldg_id = '%s' ", bldg_id);

    sprintf(buffer,
            " get storage policy suffixes "
            " where lodnum = \"%s\" "
            "   and subnum = \"%s\" "
            "   and dtlnum = \"%s\" "
            "   and arecod = \"%s\" "
            "   and wrkzon = \"%s\" "
            "   and prtnum = \"%s\" "
            "   and prt_client_id = \"%s\" "
            "   and lotnum = \"%s\" "
            "   and revlvl = \"%s\" "
            "   and orgcod = \"%s\" "
            "   and invsts = \"%s\" "
            "   and untcas = %ld "
            "   and untpak = %ld "
            "   and untqty = %ld "
            "   and fifdte = \"%s\" "
            "   and type = \"%s\" "
            "   and wh_id = \"%s\" ",
            lodnum, subnum, dtlnum, prd->reqare, prd->curzon,
            prd->prtnum, prd->prt_client_id, prd->lotnum, prd->revlvl,
            prd->orgcod, prd->invsts, prd->untcas, prd->untpak,
            prd->totqty, prd->fifdte,
            prd->stotyp,
            prd->wh_id);

    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return (ret_status);
    }
    
    *Ptr_Assign_Loc = srvResultsInit(eOK,
                                     "wh_id", COMTYP_CHAR, WH_ID_LEN,
                                     "arecod", COMTYP_CHAR, ARECOD_LEN,
                                     "lower_loc", COMTYP_CHAR, STOLOC_LEN,
                                     "upper_loc", COMTYP_CHAR, STOLOC_LEN,
                                     NULL);

    for (RetRow = sqlGetRow(srvGetResults(CurPtr));
         RetRow; RetRow = sqlGetNextRow(RetRow))
    {
        memset(suffix, '\0', sizeof(suffix));
        memset(dash, '\0', sizeof(dash));
        if (strcmp(RetRow->DataPtr[0], DEFPOL_SUFFIX) != 0)
        {
            strcpy(dash, "-");
            strcpy(suffix, RetRow->DataPtr[0]);
        }

        /* Get assigned locations first */

        sLog(0, "Attempting to get part/family/supplier/client "
                "assignments for suffix('%s')...", suffix);

        /*  TODO:  Policy part issues
        */

        /* PR 49404
         * The following select used to have a nested OR statement to gather
         * from the poldat_view and also a decode order by, however this was 
         * causing a serious performance hit on SQL Server so we broke them up 
         * into several simpler statements and now union the results to 
         * accomplish the same query.
         * 2008.1, Included storage assigned location policy for asset_typ.
         */
        sprintf(buffer,
                "select p.rtstr1, "
                "       p.rtstr2, "
                "       locmst.arecod, "
                "       locmst.wh_id "
                "  from locmst, "
                "       ((select wh_id, rtstr1, rtstr2, srtseq, 1 rank "
                "           from poldat_view "
                "          where polcod = '%s%s%s' "
                "            and polvar = 'prtnum' "
                "            and polval = '%s%s%s') "
                "        union  "
                "        (select wh_id, rtstr1, rtstr2, srtseq, 2 rank "
                "           from poldat_view "
                "          where polcod = '%s%s%s' "
                "            and polvar = 'prtfam' "
                "            and polval = '%s') "
                "        union "
                "        (select wh_id, rtstr1, rtstr2, srtseq, 3 rank "
                "           from poldat_view "
                "          where polcod = '%s%s%s' "
                "            and polvar = 'supnum' "
                "            and polval = '%s%s%s') "
                "        union "
                "        (select wh_id, rtstr1, rtstr2, srtseq, 4 rank "
                "           from poldat_view "
                "          where polcod = '%s%s%s' "
                "            and polvar = 'client_id' "
                "            and polval = '%s') "
                "        union "
                "        (select wh_id, rtstr1, rtstr2, srtseq, 5 rank "
                "           from poldat_view "
                "          where polcod = '%s%s%s' "
                "            and polvar = 'asset_typ' "
                "            and polval = '%s')) p, "
                "       aremst "
                " where locmst.stoloc = p.rtstr1 "
                "   and locmst.wh_id  = p.wh_id "
                "   and locmst.arecod = aremst.arecod "
                "   and locmst.wh_id = aremst.wh_id "
                "   and locmst.wh_id = '%s' "
                "   %s "
                "   %s "
                " order by "
                "   p.rank, "
                "   p.srtseq",
                POLCOD_STORE_ASGLOC, dash, suffix,
                prd->prt_client_id, CONCAT_CHAR, prd->prtnum,
                POLCOD_STORE_ASGLOC, dash, suffix,
                prd->prtfam,
                POLCOD_STORE_ASGLOC, dash, suffix,
                prd->prt_client_id, CONCAT_CHAR, prd->supnum,
                POLCOD_STORE_ASGLOC, dash, suffix,
                prd->prt_client_id,
                POLCOD_STORE_ASGLOC, dash, suffix,
                prd->asset_typ,
                wh_id,
                temp_buffer,
                bldg_clause);
        
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sLog(0, "Failed to load any part/family/supplier/client"
                " assignments...");
            sqlFreeResults(res);
            continue;
        }
        
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            /* 
             * makeup the where clauses for Assigned Locations 
             */
            misTrc(T_FLOW, "Found Assigned Locations: %s - %s",
                           sqlGetValue(res, row, "rtstr1"),
                           sqlGetValue(res, row, "rtstr2"));
            
            /*
             * Add assigned location range to the structure
             */ 
            srvResultsAdd(*Ptr_Assign_Loc,
                          sqlGetString(res, row, "wh_id"),
                          sqlGetString(res, row, "arecod"),
                          sqlGetString(res, row, "rtstr1"),
                          sqlGetString(res, row, "rtstr2"));
        }
        sqlFreeResults(res);
    }

    /* if no Assigned location was found, free the memory */

    if (sqlGetNumRows(srvGetResults(*Ptr_Assign_Loc)) == 0)
    {
        sLog(0, "No Assigned locations");

        srvFreeMemory(SRVRET_STRUCT, *Ptr_Assign_Loc);
        *Ptr_Assign_Loc = NULL;

        return (eDB_NO_ROWS_AFFECTED);
    }
    else
    {
        sLog(0, "Current Assigned locations are:");

        res = srvGetResults(*Ptr_Assign_Loc);
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            sLog(0, "\t%s/%s\t%s - %s",
                    sqlGetString(res, row, "wh_id"),
                    sqlGetString(res, row, "arecod"),
                    sqlGetString(res, row, "lower_loc"),
                    sqlGetString(res, row, "upper_loc"));
        }
    }

    sLog(0, "\n");

    return (eOK);
}

/* Load the area code search list (we also call locations in
   these areas Non-Assigned locations).
   
   There are 4 policy entries that may influence this list:
       1) the warehouse DEFAULT search order,
       2) the search order for the client id
       3) the search order for the supplier-client 
       4) the search order for the part family 
       5) the search order for the part.
   The more specific the policy, the higher its precedence.
   
   Area preferences for VMI inventory are processed in a manner similar to
   location preferences, see comment for the function
   LoadAssignedLocationSearchList.
   
   If an area(prd->reqare) is specified, ignore the policies, 
   just use the specified area code.
*/

static long LoadNonAssignedAreaSearchList(AREA_LIST ** head, PRODUCT * prd,
	                                  char *bldg_id_i, char *wh_id_i)
{
    char            buffer[3000];
    char            suffix[RTSTR1_LEN + 1];
    char            lodnum[LODNUM_LEN + 1];
    char            subnum[SUBNUM_LEN + 1];
    char            dtlnum[DTLNUM_LEN + 1];
    char            dash[2];
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL;
    long            ret_status = eOK;
    RETURN_STRUCT  *CurPtr = NULL;
    mocaDataRow    *RetRow = 0;
    AREA_LIST      *area = NULL;
    AREA_LIST      *def_list = NULL;
    AREA_LIST      *part_list = NULL;
    AREA_LIST      *supnum_list = NULL;
    AREA_LIST      *fam_list = NULL;
    AREA_LIST      *client_list = NULL;
    AREA_LIST      *cur_tail = NULL;
    AREA_LIST      *asset_typ_list = NULL;

    sLog(0, "Loading area search list...");

    if (misTrimLen(prd->reqare, ARECOD_LEN))
    {
	sprintf(buffer,
        "select NULL rtstr2, 0 rtnum1, 0 rtnum2, 0 rtflt2, aremst.* "
		"  from aremst "
		" where arecod = '%s' "
                "   and wh_id = '%s' "
		"  and praflg = '%d'",
		prd->reqare, 
                prd->wh_id,
		BOOLEAN_FALSE);

	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sLog(0, "(%d) FAILED to load area info for: %s/%s",
		    ret_status, prd->wh_id, prd->reqare);

	    sqlFreeResults(res);
	    return (ret_status);
	}
	row = sqlGetRow(res);
	area = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
	if (!area)
	{
	    sqlFreeResults(res);
	    return (eNO_MEMORY);
	}
	SetupArea(area, res, row);
	*head = area;
	sqlFreeResults(res);
	sLog(0, "Area search order is:%s/%s", prd->wh_id, prd->reqare);

	return (eOK);
    }

    memset(lodnum, '\0', sizeof(lodnum));
    memset(subnum, '\0', sizeof(subnum));
    memset(dtlnum, '\0', sizeof(dtlnum));

    if (strcmp(prd->lodlvl, LODLVL_LOAD) == 0)
	strcpy(lodnum, prd->lodnum);
    else if (strcmp(prd->lodlvl, LODLVL_SUBLOAD) == 0)
	strcpy(subnum, prd->lodnum);
    else if (strcmp(prd->lodlvl, LODLVL_DETAIL) == 0)
	strcpy(dtlnum, prd->lodnum);

    /* 
     * Get the storage policy suffixes....
     */

    sprintf(buffer,
	    "get storage policy suffixes where lodnum = \"%s\" "
	    "and subnum = \"%s\" and dtlnum = \"%s\" and arecod = \"%s\" "
	    "and wrkzon = \"%s\" and prtnum = \"%s\" and prt_client_id = \"%s\" "
	    "and lotnum = \"%s\" "
	    "and revlvl = \"%s\" and orgcod = \"%s\" "
	    "and invsts = \"%s\" and untcas = %ld "
	    "and untqty = %ld and fifdte = \"%s\" "
	    "and type = \"%s\" and untpak = %ld "
	    "and wh_id = \"%s\" ",
	    lodnum, subnum, dtlnum, prd->reqare, prd->curzon,
	    prd->prtnum, prd->prt_client_id, prd->lotnum, prd->revlvl,
	    prd->orgcod, prd->invsts, prd->untcas, prd->totqty, prd->fifdte,
	    prd->stotyp, prd->untpak,
	    prd->wh_id);

    CurPtr = NULL;
    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK)
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
	return (ret_status);
    }

    *head = NULL;
    for (RetRow = sqlGetRow(srvGetResults(CurPtr));
	 RetRow; RetRow = sqlGetNextRow(RetRow))
    {
	memset(suffix, '\0', sizeof(suffix));
	memset(dash, '\0', sizeof(dash));
	if (strcmp(RetRow->DataPtr[0], DEFPOL_SUFFIX) != 0)
	{
	    strcpy(dash, "-");
	    strcpy(suffix, RetRow->DataPtr[0]);
	}

	sLog(0, "Loading DEFAULT area list for suffix('%s')...", suffix);

	sprintf(buffer,
		"select poldat_view.rtstr1, poldat_view.rtstr2, "
                "       poldat_view.rtnum1, poldat_view.rtnum2, "
                "       poldat_view.rtflt2, aremst.* "
		"  from aremst, poldat_view "
		" where poldat_view.polcod = '%s%s%s%s%s' "
		"   and poldat_view.polvar = 'NONE' "
		"   and poldat_view.polval = 'NONE' "
		"   and poldat_view.rtstr1 = aremst.arecod "
		"   and poldat_view.wh_id  = aremst.wh_id "
		"   and aremst.praflg = '%d' "
		"   and aremst.wh_id  = '%s' "
		" order by poldat_view.srtseq DESC",
		bldg_id_i, DASH, POLCOD_STORE_DEFARE, dash, suffix, 
		BOOLEAN_FALSE, wh_id_i);

	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sLog(0, "(%d) FAILED to load default area list", ret_status);
	    sqlFreeResults(res);
	}
	else
	{
	    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
            area = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
            if (!area)
            {
                sqlFreeResults(res);
                return (eNO_MEMORY);
            }

            SetupArea(area, res, row);
            area->conf_none = BOOLEAN_TRUE;
            /* Since we load in DESCENDING order, we simply push
               the area on the list...we end up with the head of 
               the list being the first area we want to consider. */

            if (def_list)
            {
                area->next = def_list;
                def_list->prev = area;
            }
            def_list = area;
	    }
	    sqlFreeResults(res);
            res = NULL;
	}



    /*  
     *  Load up all policies specific to part...
     */

	sLog(0, "Loading prtnum area list for suffix('%s')...", suffix);

	sprintf(buffer,
                "select poldat_view.rtstr2, "
                "       poldat_view.rtnum1, "
                "       poldat_view.rtnum2, "
                "       poldat_view.rtflt2, "
		"       aremst.* "
		"  from aremst, poldat_view "
		" where poldat_view.polcod = '%s%s%s%s%s' "
		"   and poldat_view.polvar = 'prtnum' "
		"   and poldat_view.polval = '%s%s%s' "
		"   and aremst.arecod = poldat_view.rtstr1 "
		"   and aremst.wh_id  = poldat_view.wh_id "
		"   and aremst.praflg = '%d' "
		"   and aremst.wh_id = '%s' "
		" order by poldat_view.srtseq DESC",
		bldg_id_i, DASH, POLCOD_STORE_PREFARE, dash, suffix, 
		prd->prt_client_id, CONCAT_CHAR, prd->prtnum, 
		BOOLEAN_FALSE, wh_id_i);


	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sLog(0, "(%d) FAILED to load part area list", ret_status);
	    sqlFreeResults(res);
            res = NULL;
	}
	else
	{
	    /* There are some areas configurated by part number */
		for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
		area = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
		if (!area)
		{
		    sqlFreeResults(res);
		    return (eNO_MEMORY);
		}

		SetupArea(area, res, row);
                area->conf_prt = BOOLEAN_TRUE;
		/* Since we load in DESCENDING order, we simply push
		 * the area on the list...we end up with the head of 
		 * the list being the first area we want to consider. 
		 */

		if (part_list)
		{
		    area->next = part_list;
		    part_list->prev = area;
		}
		part_list = area;
		
	    }
	    sqlFreeResults(res);
            res = NULL;
	}
	

	/* 
	 * Load Policy Areas for Part Family. These override the 
	 * default area search order. 
	 */

	sLog(0, "Loading family area list for suffix('%s')...", suffix);

	sprintf(buffer,
                "select poldat_view.rtstr2, "
                "       poldat_view.rtnum1, "
                "       poldat_view.rtnum2, "
                "       poldat_view.rtflt2, "
		"       aremst.* "
		"  from aremst, poldat_view "
		" where poldat_view.polcod = '%s%s%s%s%s' "
		"   and poldat_view.polvar = 'prtfam' "
		"   and poldat_view.polval = '%s' "
		"   and aremst.arecod = poldat_view.rtstr1 "
		"   and aremst.wh_id  = poldat_view.wh_id "
		"   and aremst.praflg = '%d' "
		"   and aremst.wh_id = '%s' "
		" order by poldat_view.srtseq DESC",
		bldg_id_i, DASH, POLCOD_STORE_PREFARE, dash, suffix, 
		prd->prtfam, 
		BOOLEAN_FALSE, wh_id_i);

	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sLog(0, "(%d) FAILED to load part familiy area list", ret_status);
	    sqlFreeResults(res);
            res = NULL;
	}
	else
	{
	    /* There are some areas configurated by part family */
		for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
		area = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
		if (!area)
		{
		    sqlFreeResults(res);
		    return (eNO_MEMORY);
		}

		SetupArea(area, res, row);
                area->conf_prtfam = BOOLEAN_TRUE;
		/* Since we load in DESCENDING order, we simply push
		 * the area on the list...we end up with the head of 
		 * the list being the first area we want to consider. 
		 */

		if (fam_list)
		{
		    area->next = fam_list;
		    fam_list->prev = area;
		}
		fam_list = area;		
	    }
	    sqlFreeResults(res);
            res = NULL;
	}

    /*
     * Load Policy Areas for Supplier Number - Client ID. These are normally
     * defined for Consignment Inventory suffix but definitions for other
     * suffixes could also be setup in the system.
     */
    sLog(0, "Loading supplier-client area list for suffix('%s')...", suffix);

    sprintf(buffer,
        "select poldat_view.rtstr2, "
        "       poldat_view.rtnum1, "
        "       poldat_view.rtnum2, "
        "       poldat_view.rtflt2, "
        "       aremst.* "
        "  from aremst,"
        "       poldat_view "
        " where poldat_view.polcod = '%s%s%s%s%s' "
        "   and poldat_view.polvar = 'supnum' "
        "   and poldat_view.polval = '%s%s%s' "
        "   and aremst.arecod = poldat_view.rtstr1 "
        "   and aremst.wh_id  = poldat_view.wh_id "
        "   and aremst.praflg = '%d' "
        "   and aremst.wh_id = '%s' "
        " order by poldat_view.srtseq DESC",
        bldg_id_i, DASH, POLCOD_STORE_PREFARE, dash, suffix,
        prd->prt_client_id, CONCAT_CHAR, prd->supnum,
        BOOLEAN_FALSE, wh_id_i);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        sLog(0, "(%d) FAILED to load supplier-client area list", ret_status);
        sqlFreeResults(res);
        res = NULL;
    }
    else
    {
        /* There are some areas configured by supplier-client */
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            area = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
            if (!area)
            {
                sqlFreeResults(res);
                return (eNO_MEMORY);
            }

            SetupArea(area, res, row);
            area->conf_supnum = BOOLEAN_TRUE;

            /* Since we load in DESCENDING order, we simply push the area on the
             * list. We end up with the head of the list being the first area we
             * want to consider.
             */
            if (supnum_list)
            {
                area->next = supnum_list;
                supnum_list->prev = area;
            }
            supnum_list = area;
        }
        sqlFreeResults(res);
        res = NULL;
    }

    /* 
	 * Load Policy Areas for Client. These override the 
	 * default area search order. 
	 */

	sLog(0, "Loading client area list for suffix('%s')...", suffix);

	sprintf(buffer,
                "select poldat_view.rtstr2, "
                "       poldat_view.rtnum1, "
                "       poldat_view.rtnum2, "
                "       poldat_view.rtflt2, "
		"       aremst.* "
		"  from aremst, poldat_view "
		" where poldat_view.polcod = '%s%s%s%s%s' "
		"   and poldat_view.polvar = 'client_id' "
		"   and poldat_view.polval = '%s' "
		"   and aremst.arecod = poldat_view.rtstr1 "
		"   and aremst.wh_id  = poldat_view.wh_id "
		"   and aremst.praflg = '%d' "
		"   and aremst.wh_id  = '%s' "
		" order by poldat_view.srtseq DESC",
		bldg_id_i, DASH, POLCOD_STORE_PREFARE, dash, suffix, 
		prd->prt_client_id, 
		BOOLEAN_FALSE, wh_id_i);

	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sLog(0, "(%d) FAILED to load client area list", ret_status);
	    sqlFreeResults(res);
            res = NULL;
	}
	else
	{
	    /* There are some areas configurated by client Id */
		for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
		area = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
		if (!area)
		{
		    sqlFreeResults(res);
		    return (eNO_MEMORY);
		}

		SetupArea(area, res, row);
                area->conf_client = BOOLEAN_TRUE;
		/* Since we load in DESCENDING order, we simply push
		 * the area on the list...we end up with the head of 
		 * the list being the first area we want to consider. 
		 */

		if (client_list)
		{
		    area->next = client_list;
		    client_list->prev = area;
		}
		client_list = area;
		
	    }
	    sqlFreeResults(res);
            res = NULL;
	}

        sLog(0, "Loading asset_typ area list for suffix('%s')...", suffix);
        sprintf(buffer,
                    "select poldat_view.rtstr2, "
                    "       poldat_view.rtnum1, "
                    "       poldat_view.rtnum2, "
                    "       poldat_view.rtflt2, "
                    "       aremst.* "
                    "  from aremst, poldat_view "
                    " where poldat_view.polcod = '%s%s%s%s%s' "
                    "   and poldat_view.polvar = 'client_id' "
                    "   and poldat_view.polval = '%s' "
                    "   and aremst.arecod = poldat_view.rtstr1 "
                    "   and aremst.wh_id  = poldat_view.wh_id "
                    "   and aremst.praflg = '%d' "
                    "   and aremst.wh_id  = '%s' "
                    " order by poldat_view.srtseq DESC",
                    bldg_id_i, DASH, POLCOD_STORE_PREFARE, dash, suffix, 
                    prd->asset_typ, 
                    BOOLEAN_FALSE, wh_id_i);

        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sLog(0, "(%d) FAILED to load asset_typ area list", ret_status);
            sqlFreeResults(res);
            res = NULL;
        }
        else
        {
            /* There are some areas configurated by asset_typ */
            for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                area = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
                if (!area)
                {
                    sqlFreeResults(res);
                    return (eNO_MEMORY);
                }

                SetupArea(area, res, row);
                area->conf_asset_typ = BOOLEAN_TRUE;
                if (asset_typ_list)
                {
                    area->next = asset_typ_list;
                    asset_typ_list->prev = area;
                }
                asset_typ_list = area;
            }
            sqlFreeResults(res);
            res = NULL;
        }

	/*  First pass ordering...part->family->supplier->client->default.. */


        if (part_list)
        {
            if (!*head)
            {
                *head = part_list;
            }
            else
            {
                cur_tail->next = part_list;
                part_list->prev = cur_tail;
            }
            for (area = part_list; area; area = area->next)
                cur_tail = area;
            part_list = 0;
        }


        /*
         * If we had an areas defined at the part family level, add them to
         * the list.
         */

        if (fam_list)
        {
            if (!*head)
            {
                *head = fam_list;
            }
            else
            {
                cur_tail->next = fam_list;
                fam_list->prev = cur_tail;
            }
            for (area = fam_list; area; area = area->next)
                cur_tail = area;
            fam_list = 0;
        }

        /*
         * If we had an areas defined at the supplier-client level, add them to
         * the list.
         */

        if (supnum_list)
        {
            if (!*head)
            {
                *head = supnum_list;
            }
            else
            {
                cur_tail->next = supnum_list;
                supnum_list->prev = cur_tail;
            }
            for (area = supnum_list; area; area = area->next)
            {
                cur_tail = area;
            }
            supnum_list = NULL;
        }

        /*
         * If we had an areas defined at the client level, add them to the
         * list.
         */


        if (client_list)
        {
            if (!*head)
            {
                *head = client_list;
            }
            else
            {
                cur_tail->next = client_list;
                client_list->prev = cur_tail;
            }
            for (area = client_list; area; area = area->next)
                cur_tail = area;
            client_list = 0;
        }

        /*
         * If we had an areas defined at the asset_typ level, add them to the
         * list.
         */

        if (asset_typ_list)
        {
            if (!*head)
            {
                *head = asset_typ_list;
            }
            else
            {
                cur_tail->next = asset_typ_list;
                asset_typ_list->prev = cur_tail;
            }
            for (area = asset_typ_list; area; area = area->next)
                cur_tail = area;
            asset_typ_list = 0;
        }

        if (def_list)
        {
            if (!*head)
                *head = def_list;
            else
            {
                cur_tail->next = def_list;
                def_list->prev = cur_tail;
            }
            for (area = def_list; area; area = area->next)
                cur_tail = area;
            def_list = 0;
        }

    }

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;

    if (!*head)
    {
	sLog(0, "Unable to determine search order!");
	return (eDB_NO_ROWS_AFFECTED);
    }

    sLog(0, "Current area search order is: ");

    for (area = *head; area; area = area->next)
    {
	sLog(0, "\t%s/%s - %s min - %ld max - %ld - max loads aisle - %ld",
	       area->wh_id, area->arecod, area->strategy,
	       area->minlvl, area->maxlvl,
           area->max_loads_aisle);
    }

    /* Next, we need to remove duplicates... */
	/* JL PR60435 Start - We want to comment out the remove duplicates so that we can use different strategies on the same area.*/
    /*RemoveDuplAreas(*head);*/
    /* JL PR60435 Stop - We want to comment out the remove duplicates so that we can use different strategies on the same area.*/
    
    sLog(0, "Current area search order is: ");

    for (area = *head; area; area = area->next)
    {
	sLog(0, "\t%s/%s - %s min - %ld max - %ld - max loads aisle - %ld",
	       area->wh_id, area->arecod, area->strategy,
	       area->minlvl, area->maxlvl,
           area->max_loads_aisle);
    }

    /* Then bubble up any areas needing replenishment */
    BubbleUpReplens(head, prd);

    sLog(0, "After inventory shortage check, order is: ");

    for (area = *head; area; area = area->next)
    {
	sLog(0, "\t%s/%s - %s min - %ld max - %ld - max loads aisle - %ld",
	       area->wh_id, area->arecod, area->strategy,
	       area->minlvl, area->maxlvl,
           area->max_loads_aisle);
    }

    /* Finally, bubble down any areas that are over */
    BubbleDownOverage(head, prd);
    sLog(0, "After inventory excess check, search order is: ");

    for (area = *head; area; area = area->next)
    {
	sLog(0, "\t%s/%s - %s min - %ld max - %ld - max loads aisle - %ld",
	       area->wh_id, area->arecod, area->strategy,
	       area->minlvl, area->maxlvl,
           area->max_loads_aisle);
    }

    sLog(0, "Changing NULL strategy to default of %s\n",
	   STRATEGY_TRY_BOTH_KEEP_ZONE);

    for (area = *head; area; area = area->next)
	if (misTrimLen(area->strategy, RTSTR1_LEN) == 0)
	    strncpy(area->strategy, STRATEGY_TRY_BOTH_KEEP_ZONE, RTSTR2_LEN);

    return (eOK);
}

/* This routine will check if there is a move path with an intermediate
   hop to a given area code.  If there is, it loads any zones which should
   be excluded due to full intermediate locations.  We don't currently
   build an exclusion based on useflg = 0 which should probably be
   done... */

static long CheckMovePath(AREA_LIST * area, PRODUCT * prd, char **exclusion,
                          long isPipEnabled)
{
    long            ret_status;
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL;
    RETURN_STRUCT   *CurPtr = NULL;
    char            pndare[ARECOD_LEN + 1];
    char            wrkzon_string[50];
    char            polkey[50];
    char            buffer[2000];
    char            maxqvlString[200];
    char            maxqvlString1[200];
    char           *notin_string = NULL;
    char            tmpstr[50];
    char            pndtyp[50 + 1];
    char            FlagString[50];
    short           ii;
    INTARE_LIST    *listptr;
    INTARE_LIST    *arealisttail;

    memset(wrkzon_string, 0, sizeof(wrkzon_string));
    memset(FlagString, 0, sizeof(FlagString));
    arealisttail = 0;

    if (area->zones_checked)
    {
	if (area->zone_exclusion)
	    if (!misDynStrcpy(exclusion, area->zone_exclusion))
                return (eNO_MEMORY);

	return (eOK);
    }

    area->intare = NULL;
    area->zones_checked = 1;
    sprintf(polkey, "%.*s_%.*s_%.*s",
	    (int) misTrimLen(prd->curare, ARECOD_LEN), prd->curare,
	    (int) misTrimLen(area->arecod, ARECOD_LEN), area->arecod,
	    (int) misTrimLen(prd->lodlvl, LODLVL_LEN), prd->lodlvl);

    sprintf(buffer,
            "list all movement paths   "
            " where srcare = '%s' " 
            "   and dstare = '%s' "
            "   and lodlvl = '%s' "
            "   and wh_id  = '%s' ",
            prd->curare,
            area->arecod,
            prd->lodlvl,
            area->wh_id);

    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        if (CurPtr) srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
        sLog(0, "FAILED to load move path (%ld)", ret_status);
        return (ret_status);
    }
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        if (CurPtr) srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
        sLog(0, "Move Path Does NOT exist");
        return (eOK);
    }

    sLog(0, "Move path(s) found - selecting zones to omit");
    
    res = srvGetResults(CurPtr);
    for (row = sqlGetRow(res), ii = 0; row != NULL; 
         row = sqlGetNextRow(row), ii++)
    {
        if (sqlIsNull(res, row, "hopare") ||
            misTrimLen(sqlGetString(res, row, "hopare"), ARECOD_LEN) == 0)
            continue;

        if ((listptr = (INTARE_LIST *) calloc(sizeof(INTARE_LIST), 1)) == NULL)
        {
            if (CurPtr) srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            return (eNO_MEMORY);
        }

        memset(pndare, 0, sizeof(pndare));
        memset(pndtyp, 0, sizeof(pndtyp));

        strncpy(pndare, sqlGetValue(res, row, "hopare"), ARECOD_LEN);
        strncpy(listptr->arecod, pndare, ARECOD_LEN);
        strncpy(listptr->wh_id, area->wh_id, WH_ID_LEN);
        strncpy(pndtyp, sqlGetValue(res, row, "move_method"), CODVAL_LEN);
        
        misTrim(pndtyp);
        strcpy(listptr->aretyp, pndtyp);
        listptr->next = NULL;

	if (area->intare == NULL)
	{
	    area->intare = listptr;
	    arealisttail = listptr;
	}
	else
	{
	    arealisttail->next = (INTARE_LIST *) listptr;
	    arealisttail = listptr;
	}
    }
    if (CurPtr) srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;

    if (strcmp(prd->reqzon, "") != 0)
	sprintf(wrkzon_string, " and wrkzon = '%s' ", prd->reqzon);

    listptr = area->intare;
    while (listptr)
    {
        /*
         * ALLOCATE_INVENTORY_MOVE is used when you want to move inventory
         * out of a location (clean it out).  For example, when you want
         * to clean out a pickface and move that inventory back to storage.
         * In those cases, we want to use the replenishment release rules
         * (since we wouldn't need pick labels or anything), but we need
         * to make sure that we can move this inventory back to a
         * non-replenishable location (like bulk storage).
         * Therefore, this picktyp acts like a replenishment but will ignore
         * the rplflg when allocating a location.
         */

	if (strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0)
	{
            sprintf(FlagString, " and repflg = '%d' ", BOOLEAN_TRUE);
	    sprintf(maxqvlString,
		    " * decode(erfpct,null,100,0,100,erfpct) * 1.0/100 ");
	}
	else if (strcmp(prd->stotyp, ALLOCATE_TOPOFF_REPLEN) == 0)
	{
            sprintf(FlagString, " and repflg = '%d' ", BOOLEAN_TRUE);
	    sprintf(maxqvlString,
		    " * decode(trfpct,null,100,0,100,trfpct) * 1.0/100 ");
	}
	else
	{
            sprintf(FlagString, " and stoflg = '%d' ", BOOLEAN_TRUE);
	    sprintf(maxqvlString, " * 1 ");
	}
    
    if (isPipEnabled == 1 && strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0)
    {
        memset(maxqvlString1, 0, sizeof(maxqvlString1));
    }
    else
    {
        sprintf(maxqvlString1,
            " having sum(maxqvl) %s <= (sum(curqvl) + sum(pndqvl))", 
            maxqvlString);
    }
    
	sprintf(buffer,
		"select wrkzon "
		"  from locmst "
		" where arecod = '%s' "
		"   and wh_id = '%s' "
		"   and useflg = '%d' "
		"   and locsts != '%s'"
		"   and asgflg = '%d' "
		"   and cipflg = '%d' "
		"   %s "
                "   %s "
		" group by wrkzon "
		" %s ",
		listptr->arecod,
		listptr->wh_id,
		BOOLEAN_TRUE,
		LOCSTS_INV_ERROR, BOOLEAN_FALSE,
		BOOLEAN_FALSE,
		wrkzon_string, 
                FlagString,
                maxqvlString1);

	ret_status = sqlExecStr(buffer, &res);

	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}

	if (ret_status == eOK)
	{
	    sLog(0, "Found full intermediate locations(s)!\n");

	    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
		if (notin_string)
		{
		    if (!misDynStrcat(&notin_string, ","))
                    {
                        if (notin_string) free (notin_string);
			return (eNO_MEMORY);
                    }
		}

		sprintf(tmpstr, "'%s'",
			sqlGetString(res, row, "wrkzon"));

		if (!misDynStrcat(&notin_string, tmpstr))
                {
                    if (notin_string) free (notin_string);
		    return (eNO_MEMORY);
                }
	    }
	    sqlFreeResults(res);
            res = NULL;
	    sLog(0, "Zone exclusions are: %s", notin_string);
	}
	else
	{
	    sqlFreeResults(res);
	    sLog(0, "Found no full intermediate locations. ");
	}
	listptr = listptr->next;
    }

    if(!notin_string)
    {
        sLog(0, "No Zone exclusions found");
        return(eOK);
    }
    
    if (!(area->zone_exclusion = calloc(1, strlen(notin_string) + 1)))
    {
        if (notin_string) free (notin_string);
	return (eNO_MEMORY);
    }

    strcpy(area->zone_exclusion, notin_string);
    if (!misDynStrcpy(exclusion, notin_string))
    {
        if (notin_string) free (notin_string);
	return (eNO_MEMORY);
    }

    if (notin_string) free (notin_string);

    return (eOK);
}



static long sGetLocationCount( PRODUCT * prd,
                               mocaDataRes * res,
                               mocaDataRow * row,
                               long * loc_cnt_o )
{
    /*
     * return 1 if the location:
     * - is in area that is significant
     * & has ignore_psh_flg value of false
     * & has a loccod of Volume or Pallet.  
     * otherwise, return 0.
     */

    mocaDataRes * res2 = NULL;
    mocaDataRow * row2 = NULL;

    char buffer[1000];
    long ret_status;

    sLog(0, "  Checking location %s for Pallet Stack Height.", 
        sqlGetString(res, row, "stoloc") );

    sprintf(buffer,
        " select count(1) loc_cnt " 
        "    from aremst, "
        "         locmst "
        "   where aremst.arecod = locmst.arecod "
        "     and aremst.wh_id = locmst.wh_id "
        "     and locmst.stoloc = '%s' "
        "     and locmst.wh_id = '%s' " 
        "     and aremst.loccod in ('%s', '%s') " 
        "     and aremst.sigflg = 1 " 
        "     and locmst.ignore_psh_flg = 0 ", 
        sqlGetString(res, row, "stoloc"),
        sqlGetString(res, row, "wh_id"),
        LOCCOD_PALLET,
        LOCCOD_VOLUME);

    ret_status = sqlExecStr(buffer, &res2);
       
    if ( eOK != ret_status )
    {
        sqlFreeResults(res2);
        return(ret_status);
    }
    
    row2 = sqlGetRow(res2);

    *loc_cnt_o = sqlGetLong(res2, row2, "loc_cnt");

    sqlFreeResults(res2);

    return(eOK);
}

static long sCalculateMaxqvlForPalletStackHeight( PRODUCT * prd,
                                                  mocaDataRes * res,
                                                  mocaDataRow * row,
                                                  double * maxqvl_o )
{
    RETURN_STRUCT *CurPtr = NULL;
    mocaDataRes   *res2 = NULL;
    mocaDataRow   *row2 = NULL;
    long ret_status;
    char buffer[2000];

    /* Get the Pallet Stack Height Status for the load */
    sprintf(buffer, 
        " get maxqvl for pallet stack height location "
        "     where srcnum = '%s'"
        "       and lodlvl = '%s'"
        "       and stoloc = '%s'"
        "       and wh_id  = '%s' ",
        prd->lodnum,
        prd->lodlvl,
        sqlGetString(res, row, "stoloc"),
        sqlGetString(res, row, "wh_id"));

    ret_status = srvInitiateCommand(buffer, &CurPtr);

    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return(ret_status);
    }

    res2 = srvGetResults(CurPtr);
    row2 = sqlGetRow(res2);

    *maxqvl_o  = sqlGetFloat(res2, row2, "maxqvl");

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;

    return (ret_status);
}

static long RoomToStore(AREA_LIST * area,
			PRODUCT * prd,
			mocaDataRes * res,
			mocaDataRow * row,
                        double lodhgt)
{
    long            ret_status;

    long            loc_cnt;
    long            lod_psh_status;
    long            levels, height;
    double          maxqvl, pndqvl, curqvl;
    double          store_amount;
    double          tmp_height;
    long            tmp_count;
    long            case_count;
    long            min_levels;
    double          tmp_store_amount;
    double          tmp_maxqvl;
    double          tmp_pndqvl;
    double          tmp_curqvl;
    double          sub_asset_hgt = 0.0;
    double          sub_asset_len = 0.0;

    PRODUCT        *tmpprd;

    store_amount = 0;
    tmp_height = 0;
    height = 0;
    tmp_count = 0;
    case_count = 0;
    min_levels = 0;
    tmp_store_amount = 0;
    tmp_maxqvl = 0;
    tmp_pndqvl = 0;
    tmp_curqvl = 0;
    lod_psh_status = 0;

    sLog(0, "Calling RoomToStore Now");
    sLog(0, "Location Height : %ld", sqlGetLong(res, row, "lochgt"));

    /* We will check roomtostore for ASSETS, hence the check for EMPTYPART */
    if (misTrimLen(prd->ftpcod, FTPCOD_LEN) == 0 &&
        prd->empty == 0)
    {
        sLog(0, "It's a RETURNPART inventory, No need to check the room");
        return (eOK);
    }

    /*
     *  First check is to consider location height
     *  The following checks are made:
     *     LOCCOD                check
     *     -------               ------
     *     pallet                # levels * cashgt + asset height < lochgt
     *     volume, length, each  cashgt + asset height < lochgt
     */
    if (strncmp(area->loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0 ||
	strncmp(area->loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0 ||
	strncmp(area->loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
    {

	for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
	{
            if (misTrimLen(prd->lodnum, LODNUM_LEN) > 0 &&
                strcmp(prd->lodlvl, LODLVL_LOAD) == 0 &&
                prd->asset_track_enabled == BOOLEAN_TRUE)
            {
                /* Get the subasset height and length only if it is load */
                GetSubAssetLenAndHgt(prd->lodnum, 
                    &sub_asset_hgt, &sub_asset_len);
            }
            /* if we are allocating an empty serialized asset 
             * since we use a virtual EMPTYPART, we will set the 
             * cashgt =0 and get the asset_hgt.
             * After this we will reset it to 1
             */
            if (prd->empty == 1)
            {
                prd->cashgt = 0;
            }

            if (prd->cashgt + prd->asset_hgt + sub_asset_hgt > 
                sqlGetLong(res, row, "lochgt"))
            {
                sLog(0, "Case height: %f + Asset height: %f "
                        "won't fit in location: %ld",
                        prd->cashgt, prd->asset_hgt,
                        sqlGetLong(res, row, "lochgt"));
                return (eINT_PRODUCT_WONT_FIT_IN_LOCATION);
            }
            if (prd->empty == 1)
            {
                prd->cashgt = 1;
            }
            sub_asset_hgt = 0.0;
	}
    }
    else
    {				/* check pallet... */
	if (prd->mixed == TRUE)
	{
	    for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
	    {
                if (tmpprd->untcas > 0.0 && tmpprd->caslvl > 0.0) 
                {
                /* get the tallest case height*/
                    if ((long) ceil ((double) tmpprd->cashgt) > (long)tmp_height)
                        tmp_height = (double) tmpprd->cashgt;

                    /* get the smallest number of cases per tier*/
                    if (tmp_count > 0 ) 
                    {
                        if ((long) ceil((double) tmpprd->caslvl) < min_levels)
                            min_levels = (long) (double) tmpprd->caslvl;
                    }
                    else
                    {
                        min_levels = (long) (double) tmpprd->caslvl;
                        tmp_count++;
                    }
                    /* get the number of cases */
                    case_count += (long) ceil((double) tmpprd->totqty / (double) tmpprd->untcas );
                }
            }
            levels = (long)ceil((double)case_count / (double)min_levels);
            
            /*
             * Instead of calculated height, the captured load height will  
             * taken as the height.
             */
            if (lodhgt > 0)
                height = (long)ceil(lodhgt);
            else
                height = (long)ceil(tmp_height * levels);

            sLog(0, "Mixed Pallet Case Max load Height : %ld", (long)height);            
            sLog(0, "Mixed Pallet Case Max Height : %ld", (long)tmp_height);
            sLog(0, "Mixed Pallet Min Cases Per Tier : %ld",min_levels);
            sLog(0, "Mixed Pallet Number of Cases : %ld", case_count);
            sLog(0, "Mixed Pallet Number of Tiers : %ld", levels);
        }
	else
	{
            if (prd->untcas > 0.0 && prd->caslvl > 0.0) 
            {
	        levels = (long) ceil((double) (prd->totqty / prd->untcas)
				 / prd->caslvl);
	        if (lodhgt > 0)
                    height = (long)ceil(lodhgt);
                else
                    height = (long)ceil(prd->cashgt * levels);
            }
	}

        if (misTrimLen(prd->lodnum, LODNUM_LEN) > 0 &&
            strcmp(prd->lodlvl, LODLVL_LOAD) == 0 &&
            prd->asset_track_enabled == BOOLEAN_TRUE)
        {
            /*Get subasset length and height to check qvl.*/
            GetSubAssetLenAndHgt(prd->lodnum, 
                &sub_asset_hgt, &sub_asset_len);
        }
        if (prd->empty == 0)
        {
            height = 0;
        }

	if (height + prd->asset_hgt + sub_asset_hgt > 
        sqlGetLong(res, row, "lochgt"))
	{
	    sLog(0, "Pallet height: %ld + Asset height: %f "
	            "won't fit in location: %ld",
		 height, prd->asset_hgt, sqlGetLong(res, row, "lochgt"));
	    return (eINT_PRODUCT_WONT_FIT_IN_LOCATION);
	}
    }

    /* Check QVL value */

    maxqvl = sqlGetFloat(res, row, "maxqvl");
    pndqvl = sqlGetFloat(res, row, "pndqvl");
    curqvl = sqlGetFloat(res, row, "curqvl");

    /* maxqvl above will be overriden IF we are 
     * using pallet stack height */

    /* Get the Pallet Stack Height Status to evaluate if 
       the load meets Pallet Stack Height criteria */
    /* Since inputted load isn't moved to location, we should transfer
     * load to verify whether to meet PSH, and NULL location to
     * avoid confusion.
     */
    /* We need if pallet stack heigh status only when lodnum is set.
     * prd->lodnum could be NULL when replenishment.
     */
    if (misTrimLen(prd->lodnum, LODNUM_LEN))
    {
        ret_status = trnCheckPalletStackHeightStatus(NULL, 
                                                     NULL, 
                                                     prd->lodnum, 
                                                     prd->lodlvl,
                                                     &lod_psh_status);
        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "Failed(%d) checking Pallet Stack Height Status", 
                ret_status);
            return(ret_status);
        }
    }

    /* If Pallet Stack Height is used, we will override maxqvl */
    if (lod_psh_status)
    {
        /* 
         * If the location meets our criteria, calculate maxqvl 
         * to override the maxqvl above 
         */
        ret_status = sGetLocationCount( prd, 
                                        res, 
                                        row, 
                                        &loc_cnt);
        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "Failed(%d) evaluating location", ret_status);
            return(ret_status);
        }
        if (loc_cnt == 1)
        {
            ret_status = sCalculateMaxqvlForPalletStackHeight( prd, 
                                                               res, 
                                                               row, 
                                                               &maxqvl);
            if (ret_status != eOK)
            {
                misTrc(T_FLOW, "Failed(%d) calculating maxqvl for "
                    "pallet stack height", ret_status);
                return(ret_status);
            }
        }
    }
    
    store_amount = 0.0;
    if (strncmp(area->loccod, LOCCOD_PALLET, LOCCOD_LEN) == 0)
    {
	store_amount = 1.0;
    }
    else if (strncmp(area->loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
    {
        /* If asset tracking is enabled for inventory, asset_cat = INV,
         * We need to consider the asset length. Since we can have nested 
         * assets, we will check for length in invsub also if the asset_typ 
         * is set.
         */
	for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
        {
            if (tmpprd->loclen < tmpprd->asset_len ||
                tmpprd->loclen < sub_asset_len)
            {
                if (tmpprd->asset_len > tmpprd->loclen &&
                    tmpprd->asset_len > sub_asset_len)
                {
                    store_amount += tmpprd->asset_len;
                }
                else if (sub_asset_len > tmpprd->loclen)
                {
                    store_amount += sub_asset_len;
                }
            }
            else
            {
                store_amount += tmpprd->loclen;
            }
        }
    }
    else if (strncmp(area->loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0)
    {
        /* If asset tracking is enabled for inventory, asset_cat = INV,
         * We need to consider the asset volume.Since we can have nested 
         * assets, we will check for volume of the subassets also. We have the 
         * volume of the asset in invlod. We will get the subasset volume and 
         * add to load volume if required.
         */
	for (tmpprd = prd; tmpprd ; tmpprd = tmpprd->next)
        {
            if (tmpprd->empty == 0)
            {
                store_amount += tmpprd->locqvl;
            }
            /*
             * If the identifier is lodnum and lodlvl = L
             * then check the invsub whether subnum is on
             * asset. If yes then we need to consider that
             * asset also.
             */
            if ((misTrimLen(tmpprd->lodnum, LODNUM_LEN) > 0 &&
                    strcmp(tmpprd->lodlvl, LODLVL_LOAD) == 0) &&
                    tmpprd->asset_track_enabled == BOOLEAN_TRUE)
            {
                /* Get the actual volume of the inventory.
                 * That is get the volume of the inventory which
                 * which are not resided on a container asset.
                 */
                double actinvvol = GetActualVolumeForInventoryIfAssetExists
                                   (tmpprd->lodnum, tmpprd->locqvl);
                /*
                 * If the actinvvol != tmpprd->locqvl then it means some of the
                 * asset is a container. Set the locqvl as actinvvol.
                 */
                if(actinvvol != tmpprd->locqvl)
                {
                    store_amount = actinvvol;
                }
            }
            /*
             * If the asset on the invlod is a container 
             * then actinvvol will be 0. Hence add the
             * max_vol of the asset of invlod.
             */
            if (tmpprd->asset_max_vol > 0)
            {
                store_amount = tmpprd->asset_max_vol;
            }
            else
            {
                /*
                 * Calculate the asset on invlod and add it to the
                 * store_amount.
                 * And get the volume of the assets in invsub using 
                 * GetSubAssetVolume().
                 * This function will return the volume of the
                 * assets in invsub based on the asset_typ.ser_flg 
                 * and asset_typ.container_flg.
                 */
                store_amount += tmpprd->asset_len * 
                                tmpprd->asset_wid * 
                                tmpprd->asset_hgt;

                if ((misTrimLen(tmpprd->lodnum, LODNUM_LEN) > 0 &&
                    strcmp(tmpprd->lodlvl, LODLVL_LOAD) == 0) &&
                    tmpprd->asset_track_enabled == BOOLEAN_TRUE)
                {
                    store_amount += GetSubAssetVolume(tmpprd->lodnum);
                }
            }
        }
    }
    else if (strncmp(area->loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0)
    {
	for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
        {
	    store_amount += tmpprd->totqty;
        }
    }

    /* we multiple the doubles by 10000 to avoid any rounding errors
     * there was an issue where if the qvl being store_amount matched exactly
     * the qvl that was available, it would not think it would fit because of
     * a rounding issue with the doubles
     */
    tmp_maxqvl = maxqvl * 10000;
    tmp_pndqvl = pndqvl * 10000;
    tmp_curqvl = curqvl * 10000;
    tmp_store_amount = store_amount * 10000;

    sLog(0, "Comparison Values (x10000) Product qvl: %g, "
            "location qvl: %g",
         tmp_store_amount, (tmp_maxqvl - tmp_curqvl - tmp_pndqvl),
         store_amount);

    if (tmp_store_amount > (tmp_maxqvl - tmp_curqvl - tmp_pndqvl))
    {
	sLog(0, "Product (and Asset) qvl: %f, location qvl: %f - WON'T FIT",
	       store_amount, (maxqvl - curqvl - pndqvl));
	return (eINT_PRODUCT_WONT_FIT_IN_LOCATION);
    }

    return (eOK);

}
static long sIsLocationAssignedToOther(PRODUCT *prd,
				       char *stoloc,
                                       char *wh_id)
{

    char buffer[2000];
    long ret_status;

    sLog(0, "  Checking location %s for part assignments.", stoloc);

    sprintf(buffer,
	    "select 1 from locmst where stoloc = '%s' and asgflg = 1"
            "   and wh_id = '%s' ",
	    stoloc, wh_id);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK)
    {
        sLog(0, "  Location is not hard assigned to any part.");
    }
    else
    {
        sLog(0, 
             "  Assignments exist - referring to policies to find if it"
             "is for our part.");

        sprintf(buffer,
            "select poldat_view.rtstr1, poldat_view.rtstr2, locmst.arecod "
            "  from locmst, poldat_view "
            " where poldat_view.wh_id = locmst.wh_id "
            "   and poldat_view.polcod like '%s%%' "
            "   and ((poldat_view.polvar = 'prtnum' "
            "         and poldat_view.polval = '%s%s%s') OR "
            "        (poldat_view.polvar = 'prtfam' "
            "          and poldat_view.polval = '%s') OR "
            "        (poldat_view.polvar = 'client_id' "
            "          and poldat_view.polval = '%s')) "
            "   and (poldat_view.rtstr1 = '%s' or "
            "        '%s' between poldat_view.rtstr1 and poldat_view.rtstr2)"
            "   and locmst.stoloc = '%s'"
            "   and locmst.wh_id  = '%s'",
            POLCOD_STORE_ASGLOC,  
            prd->prt_client_id, CONCAT_CHAR, 
            prd->prtnum, 
            prd->prtfam, 
            prd->prt_client_id, stoloc, stoloc, stoloc, wh_id);
    
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            sLog(0, "Location is not assigned to our part - assuming"
                 " it is assigned to another...");
            return(eINT_LOCATION_ALREADY_ASSIGNED_TO_OTHER);
        }
    }

    /* For distro inventory, check if locations can be shared */
    if (misTrimLen(prd->distro_id, DISTRO_ID_LEN) > 0)
    {
        /* If this location is in an area not marked for sharing locations and
         * the location either already has, or is expected to receive, distro
         * inventory for other stores, we will disallow location sharing.
         * 
         * Initially we test whether the location is shareable; if it is not
         * shareable we futher test if location has, or will receive, inventory
         * for other stores. The second test consists of finding all distro
         * inventory destined for stores other than the store for the current
         * distro and which is already at this location or invmov indicates is
         * scheduled to move to this location. The first select omits
         * distro.client_id filter since we want to consider distro inventory
         * for all other stores irrespective of client ID. invmov.lodnum can be
         * lodnum, subnum or dtlnum and must be considered along with lodlvl to
         * determine which ID is in use.
         */
        sprintf(buffer,
            " list locations"
            "     where stoloc='%s'"
            "       and share_loc_flg<>1"
            "       and wh_id='%s'"
            " |"
            " [select 1 allow_alloc"
            "    from distro,"
            "         invdtl"
            "   where distro.wh_id=@wh_id"
            "     and distro.stcust not in"
            "         (select stcust"
            "            from distro"
            "           where distro_id='%s'"
            "             and client_id='%s'"
            "             and wh_id=@wh_id)"
            "     and distro.distro_id=invdtl.distro_id"
            "     and invdtl.dtlnum in    "
            "         (select dtlnum"
            "            from invdtl,"
            "                 invsub,"
            "                 invlod"
            "           where invdtl.subnum=invsub.subnum"
            "             and invsub.lodnum=invlod.lodnum"
            "             and invlod.stoloc=@stoloc"
            " "
            "           union"
            " "
            "          select dtlnum"
            "            from invmov,"
            "                 invlod,"
            "                 invsub,"
            "                 invdtl"
            "           where invmov.stoloc=@stoloc"
            "             and invmov.wh_id=@wh_id"
            "             and (   (    invmov.lodlvl='%s'"
            "                      and invlod.lodnum=invmov.lodnum)"
            "                  or (    invmov.lodlvl='%s'"
            "                      and invsub.subnum=invmov.lodnum)"
            "                  or (    invmov.lodlvl='%s'"
            "                      and invdtl.dtlnum=invmov.lodnum))"
            "             and invdtl.subnum=invsub.subnum"
            "             and invsub.lodnum=invlod.lodnum)]",
            stoloc,
            wh_id,
            prd->distro_id,
            prd->prt_client_id,
            LODLVL_LOAD,
            LODLVL_SUBLOAD,
            LODLVL_DETAIL);

        ret_status = srvInitiateCommand(buffer, NULL);
        if (ret_status == eOK)
        {
            sLog(0, "Location %s is in an area not marked for sharing "
                " locations for distro inventory and the location already "
                " has inventory destined for other stores.",
                stoloc);
            return(eINT_LOCATION_ALREADY_ASSIGNED_TO_OTHER);
        }
        else if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            /* This is a query error */
            return ret_status;
        }
    }
    return(eOK);
}

static long PartOkInLoc(AREA_LIST * area,
			PRODUCT * prd,
			mocaDataRes * res,
			mocaDataRow * row)
{
    char            old_date[MOCA_STD_DATE_LEN + 1];
    char            new_date[MOCA_STD_DATE_LEN + 1];
    PRODUCT        *tmpprd;
    char            buffer[1000];
    long            ret_status;
    double          fif_win;

    RETURN_STRUCT  *CmdPtr = NULL;
    mocaDataRes    *cmpres = NULL;
    mocaDataRow    *cmprow = NULL;


    /* if PCKCOD is SINGLE and lodflg is 1, then we can't mix untpal...
       check that untpal = totqty we're storing... */
    if (strncmp(area->pckcod, PCKCOD_SINGLE, PCKCOD_LEN) == 0)
    {
	sprintf(buffer,
		"check inventory mix at location "
		" where dstloc = '%s'"
                "   and wh_id  = '%s'",
		sqlGetString(res, row, "stoloc"),
                sqlGetString(res, row, "wh_id"));

	ret_status = srvInitiateInline(buffer, NULL);
	if (ret_status != eOK)
	    return(ret_status);
    }

    /* if we are storing a mixed pallet, then we must have a mixed area */
    if (prd->mixed == TRUE &&
	strncmp(area->pckcod, PCKCOD_MIXABLE, PCKCOD_LEN) != 0)
    {
	sLog(0, "Location: %s/%s - in single part area, attempt to store "
	        "mixed load denied",
	        sqlGetString(res, row, "wh_id"),
	        sqlGetString(res, row, "stoloc"));
	return (eINT_CANNOT_STORE_MIXED_PALLET_IN_NON_MIX_AREA);
    }

    if (area->fifflg == BOOLEAN_FALSE)
	return (eOK);


    /* Make sure that FIFO range is not exceeded...
       also flag if we should be updating one of the fifo dates... */

    memset(old_date, 0, sizeof(old_date));
    memset(new_date, 0, sizeof(new_date));
    strncpy(old_date, sqlGetString(res, row, "oldfif"), MOCA_STD_DATE_LEN);
    strncpy(new_date, sqlGetString(res, row, "newfif"), MOCA_STD_DATE_LEN);
    sLog(0, 
         "New Date: %s - Old Date: %s\n",
	 new_date, 
         old_date);

    if (misTrimLen(old_date, MOCA_STD_DATE_LEN) == 0)
	return (eOK);

    /* if fifo range doesn't fall within current end points... */
    for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
    {

	/*  If the Disable Replenishment FIFO Check is Yes...continue and
	**  do not attempt the fifo checks
	*/
	if (strncmp(tmpprd->disfif, YES_STR, strlen(YES_STR)) == 0)
	{
	    sLog(0, 
                 "The Disable Repl-FIFO Check is on, the calculation "
                 "is skipped.");
	    continue;
	}

	/*  If the fifo window is set to 0...do not do the calculation
	*/
	if(tmpprd->fifwin == 0 )
	{
	    sLog(0, "Fifo Window = 0, the calculation is skipped." );
	    continue;
	}

        /* Now we have to determine if the date falls within the range */
        sprintf(buffer,
                "calculate fifo dates for storage "
                "    where olddte = '%s' "
                "      and newdte = '%s' "
                "      and fifdte = '%s' "
                "      and timcod = '%s' ",
                old_date,
                new_date,
                tmpprd->fifdte,
                tmpprd->timcod);

        ret_status = srvInitiateCommand(buffer, &CmdPtr);
        if (eOK != ret_status)
        {
            sLog(0, "Error calculating FIFO Window %ld.",
                 ret_status);
            srvFreeMemory(SRVRET_STRUCT, CmdPtr);
            return(ret_status);
        }
        cmpres = srvGetResults(CmdPtr);
        cmprow = sqlGetRow(cmpres);

        /* See if either of the dates changed */
        if (strncmp(old_date, 
                    sqlGetString(cmpres, cmprow, "olddte"), 
                    MOCA_STD_DATE_LEN))
        {
            misTrimcpy(old_date, 
                       sqlGetString(cmpres, cmprow, "olddte"),
                       MOCA_STD_DATE_LEN);
        }

        if (strncmp(new_date, 
                    sqlGetString(cmpres, cmprow, "newdte"), 
                    MOCA_STD_DATE_LEN))
        {
            misTrimcpy(new_date, 
                       sqlGetString(cmpres, cmprow, "newdte"),
                       MOCA_STD_DATE_LEN);
        }

        fif_win = sqlGetFloat(cmpres, cmprow, "fif_win");

        srvFreeMemory(SRVRET_STRUCT, CmdPtr);
        CmdPtr = NULL;

	sLog(0,
	     "New Date: %s - Old Date: %s "
	     "---> Difference: %f %s "
	     "fifdte: %s   fifwin: %ld ",
	     new_date, 
             old_date, 
             fif_win,
             tmpprd->timcod,
	     tmpprd->fifdte, 
             tmpprd->fifwin);

        if (fif_win > (double) tmpprd->fifwin)
        {
            sLog(0, 
                 "Location: %s/%s - fifo window exceeded - CAN'T USE",
                 sqlGetValue(res, row, "wh_id"),
                 sqlGetValue(res, row, "stoloc"));
            return (eINT_FIFO_WINDOW_EXCEEDED);
        }
    }
    return (eOK);

}

/* JJS 1/15/2010 - A new procedure for the QT Hold mod.  This will see
 * if the lodnum the user is attempting to allocate a location for can
 * be putaway into stoloc according to QT Hold mixing rules.  Basically,
 * they don't want to mix product in a QT Hold status if the product
 * already in the location does not have the same QT Hold status, and
 * it does not have the same mandte.
 */
static long QTHoldMixOkInLoc(PRODUCT * prd,
                             mocaDataRes * res,
                             mocaDataRow * row)
{
    char            buffer[1000];
    long            ret_status;

    misTrc(T_FLOW, "In QTHoldMixOkInLoc");

    if (prd->lodnum && strlen(prd->lodnum) > 0)
    {
        sprintf(buffer,
                "check var qt hold mix at location "
                " where dstloc = '%s'"
                "   and wh_id  = '%s'"
                "   and lodnum  = '%s'",
                sqlGetString(res, row, "stoloc"),
                sqlGetString(res, row, "wh_id"),
                prd->lodnum);

        ret_status = srvInitiateInline(buffer, NULL);
        if (ret_status != eOK)
            return(ret_status);
    }
}

static long AddToLocList(char *location, STOLOC_LIST ** LocList, PRODUCT * prd,
                         char *wh_id)
{
    STOLOC_LIST    *listptr;
    STOLOC_LIST    *loclisttail;

    if ((listptr = (STOLOC_LIST *) calloc(sizeof(STOLOC_LIST), 1)) == NULL)
    {
	return (eNO_MEMORY);
    }

    strcpy(listptr->nxtloc, location);
    strcpy(listptr->wh_id, wh_id);
    strcpy(listptr->lodnum, prd->lodnum);
    strcpy(listptr->lodlvl, prd->lodlvl);
    listptr->next = NULL;

    if (*LocList == NULL)
    {
	*LocList = listptr;
    }
    else
    {
	loclisttail = *LocList;
	while (loclisttail->next != NULL)
	    loclisttail = loclisttail->next;
	loclisttail->next = (STOLOC_LIST *) listptr;
    }

    sLog(0, "Found location - %s/%s - allocated successfully!",
	   wh_id, location);

    return (eOK);
}

/* This routine performs the DB updates necessary to send something to
   a location for storage... */
static long GetSpecificLocation(AREA_LIST * area,
				PRODUCT * prd,
				char *location,
				STOLOC_LIST ** LocList,
				double curqvl,
				double pndqvl,
                char *wh_id,
                long isPipEnabled)
{
    long            ret_status;
    char            buffer[2000];
    static char     last_location[STOLOC_LEN + 1];
    static char     last_wh_id[WH_ID_LEN + 1];
    AREA_LIST       tmparea, *ap;
    PRODUCT        *tmpprd;
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL;
    char            FlagString[50];
    char            maxqvlString[200];
    char            maxqvlString1[200];
    double          locqvl;
    char            fifo_date[100];
    double          sub_asset_hgt = 0.0;
    double          sub_asset_len = 0.0;

    sLog(0, "Setting up location: %s for pending store", location);

    /* First - see if we have the area information we need...  */
    if (!area)
    {
	sprintf(buffer,
		"select aremst.* "
		"  from locmst, aremst "
		" where locmst.arecod = aremst.arecod "
                "   and locmst.wh_id  = aremst.wh_id "
		"   and aremst.praflg = '%d' "
		"   and locmst.stoloc = '%s'"
                "   and locmst.wh_id  = '%s'", 
                BOOLEAN_FALSE, location, wh_id);
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}
	memset(&tmparea, 0, sizeof(tmparea));
	row = sqlGetRow(res);
	SetupArea(&tmparea, res, row);
	ap = &tmparea;
	sqlFreeResults(res);
        res = NULL;
    }
    else
    {
	ap = area;
    }

    /* Ok - if significant, then we need to track the pndqvl
       at the location  */
    if (ap->sigflg == BOOLEAN_TRUE)
    {
	locqvl = 0;
	if (strncmp(ap->loccod, LOCCOD_PALLET, LOCCOD_LEN) == 0)
        {
	    locqvl = 1.0;
        }
	else if (strncmp(ap->loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
        {
            /* If asset tracking is enabled for inventory, asset_cat = INV,
             * We need to consider the asset length. Since we can have nested 
             * assets, we will check for length in invsub also if the asset_typ 
             * is set.
             */
            for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
            {
                if (misTrimLen(tmpprd->lodnum, LODNUM_LEN) > 0 &&
                    strcmp(tmpprd->lodlvl, LODLVL_LOAD) == 0 &&
                    tmpprd->asset_track_enabled == BOOLEAN_TRUE)
                {
                    GetSubAssetLenAndHgt(tmpprd->lodnum, 
                        &sub_asset_hgt, &sub_asset_len);
                }
                if (tmpprd->loclen < tmpprd->asset_len ||
                    tmpprd->loclen < sub_asset_len)
                {
                    if (tmpprd->asset_len > tmpprd->loclen &&
                        tmpprd->asset_len > sub_asset_len)
                    {
                        locqvl += tmpprd->asset_len;
                    }
                    else if (sub_asset_len > tmpprd->loclen)
                    {
                        locqvl += sub_asset_len;
                    }
                }
                else
                {
                    locqvl += tmpprd->loclen;
                }
            }
        }
	else if (strncmp(ap->loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0)
	{
        /* If asset tracking is enabled for inventory, asset_cat = INV,
         * We need to consider the asset volume.Since we can have nested 
         * assets, we will check for volume of the subassets also. We have the 
         * volume of the asset in invlod. We will get the subasset volume and 
         * add to load volume if required. If container_flg is set for the
         * asset_typ of invlod use the max_vol
         */
	    for (tmpprd = prd; tmpprd ; tmpprd = tmpprd->next)
            {
                /*
                 * Get the qvl of inventory from product pointer.
                 * Add the volume of the asset which is stored in
                 * the product pointer.
                 */
                if (tmpprd->empty == 0)
                    locqvl += tmpprd->locqvl;
                if (tmpprd->asset_max_vol > 0)
                {
                    locqvl = tmpprd->asset_max_vol;
                }
                else
                {
                    locqvl += tmpprd->asset_len * 
                              tmpprd->asset_wid * 
                              tmpprd->asset_hgt;
                }
                /*
                 * If the identifier is lodnum and lodlvl = L
                 * then check the invsub whether subnum is on
                 * asset. If yes then we need to consider that
                 * asset also.
                 */
                if ((misTrimLen(tmpprd->lodnum, LODNUM_LEN) > 0 &&
                    strcmp(tmpprd->lodlvl, LODLVL_LOAD) == 0) &&
                    tmpprd->asset_track_enabled == BOOLEAN_TRUE)
                {  
                    /* Get the actual volume of the inventory.
                     * That is get the volume of the inventory which
                     * which are not residing on a container asset.
                     */
                    double actinvvol = GetActualVolumeForInventoryIfAssetExists
                                       (tmpprd->lodnum, tmpprd->locqvl);
                    /*
                     * If the actinvvol != tmpprd->locqvl then it means some of the
                     * asset is a container. Set the locqvl as actinvvol.
                     */
                    if(actinvvol != tmpprd->locqvl &&
                       tmpprd->empty == 0)
                        locqvl = actinvvol;
                    /*
                     * If the asset on the invlod is a container 
                     * then actinvvol will be 0. Hence add the
                     * max_vol of the asset of invlod.
                     */
                    if (tmpprd->asset_max_vol > 0)
                    {
                        locqvl = tmpprd->asset_max_vol;
                    }
                    else
                    {
                        /*
                         * Else get the volume of the assets in invsub.
                         * This function will return the volume of the
                         * assets in invsub based on the asset_typ.ser_flg 
                         * and asset_typ.container_flg.
                         */
                        locqvl += GetSubAssetVolume(tmpprd->lodnum);
                    }
                }
            }
        }
	else if (strncmp(ap->loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0)
        {
	    for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
            {
		locqvl += tmpprd->totqty;
            }
        }

	if (strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0)
	{
	    sprintf(FlagString, " and repflg = %d ", BOOLEAN_TRUE);
	    sprintf(maxqvlString,
		    " * decode(erfpct,null,100,0,100,erfpct) * 1.0/100 ");
	}
	else if (strcmp(prd->stotyp, ALLOCATE_TOPOFF_REPLEN) == 0)
	{
	    sprintf(FlagString, " and repflg = %d ", BOOLEAN_TRUE);
	    sprintf(maxqvlString,
		    " * decode(trfpct,null,100,0,100,trfpct) * 1.0/100 ");
	}
	else
	{
	    sprintf(FlagString, " and stoflg = %d ", BOOLEAN_TRUE);
	    sprintf(maxqvlString, " * 1 ");
	}

        if (isPipEnabled == 1 && strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0)
        {
           sprintf(maxqvlString1," 1 = 1");
        }
        else
        {
            sprintf(maxqvlString1, "maxqvl %s >= curqvl + pndqvl + %f",
                    maxqvlString, 
                    locqvl);
        }

	/* Scottk - PR # 618 */
	sprintf(buffer,
		"update locmst"
		"   set pndqvl = pndqvl + %f "
		" where floor(curqvl) = floor(%f) " /* 12-8-96kjh numeric */
		"   and floor(pndqvl) = floor(%f) " /* rounding...no exact */
		"   and stoloc = '%s' "
                "   and wh_id  = '%s' "
		"   and locsts != '%s' "
		"   and %s"
		"   and cipflg = %d "
		"   %s",
		locqvl, curqvl, pndqvl, location, wh_id,
		LOCSTS_INV_ERROR, maxqvlString1,
		BOOLEAN_FALSE, FlagString);

	ret_status = sqlExecStr(buffer, NULL);
	if (ret_status == eDB_NO_ROWS_AFFECTED)
	{
	    /* Last time we had an error, we saved off the last */
	    /* location and if we come through here again, let's */
	    /* make sure we are not looping out of control */
	    if (strncmp(location, last_location, STOLOC_LEN) == 0 &&
	        strncmp(wh_id, last_wh_id, WH_ID_LEN) == 0)
	    {
		sLog(0, "(%d) Error updating locmst and it was the "
		        "same location as last %s/%s, abort!",
		        ret_status, last_wh_id, last_location);
		memset(last_location, 0, sizeof(last_location));
		memset(last_wh_id, 0, sizeof(last_wh_id));
		return (ret_status);
	    }
	    else
	    {
		memset(last_location, 0, sizeof(last_location));
		memset(last_wh_id, 0, sizeof(last_wh_id));
		strncpy(last_location, location, STOLOC_LEN);
		strncpy(last_wh_id, wh_id, WH_ID_LEN);
		sLog(0, "(%d) Error updating locmst, "
		        "it was changed from underneath us!",
		        ret_status);
		return (eDB_DEADLOCK);
	    }
	}
	else if (ret_status != eOK)
	{
	    sLog(0, "(%d) Error updating for pndqvl!", ret_status);
	    return (ret_status);
	}
	memset(last_location, 0, sizeof(last_location));
	memset(last_wh_id, 0, sizeof(last_wh_id));

	/* We should also put out a qvlwrk entry to mark what we increased
	   our pndqvl by */
	sLog(0, "Attempting insert of QVLWRK");

        /*
         * If ASSET-TRACKING configuration is enabled,
         * need to track asset on qvlwrk table.
         *
         * if ASSET-TRACKING configuration is not enabled,
         * prd->asset_typ is empty.
         */
        if (misTrimLen(prd->asset_typ, ASSET_TYP_LEN) > 0)
        {
            sprintf(buffer,
                    "insert into qvlwrk "
                    "   (wh_id, stoloc, prtnum, prt_client_id,"
                    "    untqty, pndqvl, asset_typ) "
                    "  values ('%s', '%s', '%s', '%s', %ld, %f, '%s') ",
                    wh_id, location, prd->prtnum, prd->prt_client_id,
                    prd->totqty, locqvl, prd->asset_typ);
        }
        else
        {
            sprintf(buffer,
                    "insert into qvlwrk "
                    "   (wh_id, stoloc, prtnum, prt_client_id,"
                    "    untqty, pndqvl) "
                    "  values ('%s', '%s', '%s', '%s', %ld, %f) ",
                    wh_id, location, prd->prtnum, prd->prt_client_id,
                    prd->totqty, locqvl);
        }

	ret_status = sqlExecStr(buffer, NULL);
	if (ret_status != eOK)
	{
	    sLog(0, "(%d) Error inserting for qvlwrk!", ret_status);
	    return (ret_status);
	}
    }

    /* Next...if pickable, then we need to maintain the invsum record... */
    if (strncmp(ap->pckcod, PCKCOD_NOT_PICK, PCKCOD_LEN) != 0)
    {
	/* It's not NOT PICKABLE, so it is either mixed or single...
	   in either case, it's pickable... */
	for (tmpprd = prd, ret_status = eOK;
	     tmpprd && ret_status == eOK; tmpprd = tmpprd->next)
	{
	    sprintf(buffer,
		    "update invsum "
		    "   set pndqty = pndqty + %d "
		    " where arecod = '%s' "
		    "   and stoloc = '%s' "
		    "   and wh_id = '%s' "
		    "   and prtnum = '%s' "
		    "   and prt_client_id = '%s' ",
		    tmpprd->totqty, ap->arecod,
		    location, wh_id, tmpprd->prtnum, tmpprd->prt_client_id);
	    ret_status = sqlExecStr(buffer, NULL);
	    if (ret_status != eOK)
	    {
	
                memset(fifo_date, 0, sizeof(fifo_date));
                if (misTrimLen(prd->fifdte, 30))
                {
		    if (strlen(prd->fifdte) == 8)
			sprintf (fifo_date, 
				 "to_date ('%s', 'YYYYMMDD')", prd->fifdte);
		    else if (strlen(prd->fifdte) == 14)
			sprintf (fifo_date, 
				 "to_date ('%s', 'YYYYMMDDHH24MISS')", 
				 prd->fifdte);

                }
                else
                {
                    sprintf(fifo_date, "sysdate");
                }
		sprintf(buffer,
			"insert into invsum "
			"   (arecod, stoloc, wh_id, prtnum, prt_client_id,"
			"    invsts, untcas, untpak, untpal, "
			"    comqty, pndqty, untqty, olddte, newdte) "
			" values ('%s', '%s', '%s', '%s', '%s', "
			"         '%s', %d, %d, %d, "
			"         0, %d, 0, %s, %s)",
			ap->arecod, location, wh_id,
			tmpprd->prtnum, tmpprd->prt_client_id,
			tmpprd->invsts, tmpprd->untcas, 
			tmpprd->untpak, tmpprd->totqty,
			tmpprd->totqty, fifo_date, fifo_date);
		ret_status = sqlExecStr(buffer, NULL);
		if (ret_status != eOK)
		    return(ret_status);
	    }

	}
    }
    ret_status = AddToLocList(location, LocList, prd, wh_id);
    return (ret_status);

}

static int sCompareMaxLen(char *str1, char *str2, long maxLen)
{
    int             len1, len2;

    len1 = misTrimLen(str1, maxLen);
    len2 = misTrimLen(str2, maxLen);

    return (strncmp(str1, str2, (len1 > len2 ? len1 : len2)));
}

static long GetLocation(AREA_LIST * area,
                        PRODUCT * prd,
                        mocaDataRes * lres,
                        mocaDataRow * lrow,
                        STOLOC_LIST ** LocList,
            long isPipEnabled)
{
    char            buffer[2000];
    char            maxqvlString[200];
    char            maxqvlString1[200];
    char            wrkzon_string[50];
    char            int_loc[STOLOC_LEN + 1];
    char            sav_loc[STOLOC_LEN + 1];
    char            FlagString[50];
    char            strLoadLvl[50];
    char            lstloc[STOLOC_LEN + 1];  
    long            ret_status;
    mocaDataRow    *row = NULL;
    mocaDataRow    *row1 = NULL;
    mocaDataRes    *res = NULL;
    mocaDataRes    *res1 = NULL;
    INTARE_LIST    *intare;
    STOLOC_LIST    *locs;
    long            ii = 0;

    memset(int_loc, 0, sizeof(int_loc));
    memset(sav_loc, 0, sizeof(sav_loc));
    memset(FlagString, 0, sizeof(FlagString));
    memset(lstloc, 0, sizeof(lstloc));

    sqlSetSavepoint("location_selection");

    ret_status = eOK;

    /* First - do we need an intermediate location?  If so,
       then let's choose it and reserve it... */

    intare = area->intare;
    while (intare)
    {
        
        memset(wrkzon_string, 0, sizeof(wrkzon_string));

        if (strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0)
        {
            sprintf(FlagString, " and l.repflg = '%d' ", BOOLEAN_TRUE);
            sprintf(maxqvlString,
                    " * decode(erfpct,null,100,0,100,erfpct) * 1.0/100 ");
        }
        else if (strcmp(prd->stotyp, ALLOCATE_TOPOFF_REPLEN) == 0)
        {
            sprintf(FlagString, " and l.repflg = '%d' ", BOOLEAN_TRUE);
            sprintf(maxqvlString,
                    " * decode(trfpct,null,100,0,100,trfpct) * 1.0/100 ");
        }
        else
        {
            sprintf(FlagString, " and l.stoflg = '%d' ", BOOLEAN_TRUE);
            sprintf(maxqvlString, " * 1 ");
        }
        
        if (isPipEnabled == 1 && strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0)
        {
            sprintf(maxqvlString1, " and 1 = 1 ");
        }
        else
        {
            sprintf(maxqvlString1, " and l.maxqvl %s > l.pndqvl + l.curqvl ", maxqvlString);
        }
        
        if (strcmp(intare->aretyp, "RECEIVE-DOOR-LANE") == 0 )
        {
            if (strcmp(prd->lodlvl, LODLVL_LOAD) == 0) 
                sprintf(strLoadLvl, " i.lodnum "); 
            else if (strcmp(prd->lodlvl, LODLVL_SUBLOAD) == 0) 
                sprintf(strLoadLvl, " i.subnum ");
            else if (strcmp(prd->lodlvl, LODLVL_DETAIL) == 0) 
                sprintf(strLoadLvl, " i.dtlnum ");
            else
                sprintf(strLoadLvl, " i.lodnum "); 
            sprintf(buffer,
                " select l.stoloc, l.wh_id, "
                       " l.curqvl, l.pndqvl "
                  " from dckloc_stgloc d, "
                  "      trlr t, "
                  "      rcvtrk k,"
                  "      rcvlin n, "
                  "      inventory_view i, "
                  "      zonmst z, "
                  "      locmst l"
                  " where d.dckloc = t.yard_loc"
                   " and t.trlr_id = k.trlr_id"
                   " and k.trknum = n.trknum"
                   " and n.rcvkey = i.rcvkey "
                   " and l.wh_id =  z.wh_id     "
                   " and l.wrkzon = z.wrkzon"
                   " and %s = '%s' "
                   " and d.wh_id = '%s' "
                   " and l.stoloc = d.stgloc "
                   " and l.wh_id = d.wh_id "
                   " %s "
                   " and l.locsts != '%s'"
                   " and l.useflg = '%d'"
                   " and l.asgflg = '%d' "
                   " and l.cipflg = '%d' "
                   " and z.oosflg = '%d' ",
                   strLoadLvl,
                   prd->lodnum, 
                   intare->wh_id,
                   maxqvlString1, 
                   LOCSTS_INV_ERROR,
                   BOOLEAN_TRUE,
                   BOOLEAN_FALSE,
                   BOOLEAN_FALSE,
                   BOOLEAN_FALSE
               );
        }
        else 
        {
            if (strcmp(intare->aretyp, "SAME-ZONE-FIRST-HOP") == 0)
            {
                if (sav_loc[0] == '\0')
                    sprintf(wrkzon_string, " and l.wrkzon = '%s' ",
                            prd->curzon);
                else
                {
                    sprintf(buffer,
                            "select wrkzon from locmst "
                            " where stoloc = '%s' "
                                   "   and wh_id  = '%s' ", 
                                   sav_loc, intare->wh_id);
            
                    ret_status = sqlExecStr(buffer, &res);
            
                    row = sqlGetRow(res);
                    sprintf(wrkzon_string, " and l.wrkzon = '%s' ",
                            (char *) sqlGetValue(res, row, "wrkzon"));
                    sqlFreeResults(res);
                }
            }
            else if (strcmp(intare->aretyp, "SAME-ZONE-SECOND-HOP") == 0)
            {
                if (intare->next)
                {
                    sLog(0, "Unable to process SECOND_HOP policy"
                            " when it's not to the final destination!");
            
                    sqlSetSavepoint("location_selection");
                    trnFreeStolocList(*LocList);
                           *LocList = NULL;
                    return (eINT_UNABLE_TO_PROCESS_SECOND_HOP);
                }
            
                sprintf(wrkzon_string, " and l.wrkzon = '%s' ",
                        (char *) sqlGetValue(lres, lrow, "wrkzon"));
            }
            sLog(0, "Attempting to get intermediate location...");

            sprintf(buffer,
                    "select l.stoloc, l.wh_id "
                    "  from zonmst z, locmst l "
                    " where l.arecod = '%s' "
                    "   and l.wh_id  = '%s' "
                    "   and l.wh_id  = z.wh_id "
                    "   and l.wrkzon = z.wrkzon "
                    "   %s "
                    "   and l.locsts != '%s'"
                    "   and l.useflg = '%d'"
                    "   and l.asgflg = '%d' "
                    "   and l.cipflg = '%d' "
                    "   and z.oosflg = '%d' "
                    "   %s "
                           "   %s "
                    " order by l.velzon, l.trvseq, l.lochgt ",
                    intare->arecod, intare->wh_id,
                    maxqvlString1, LOCSTS_INV_ERROR,
                    BOOLEAN_TRUE,
                    BOOLEAN_FALSE,
                    BOOLEAN_FALSE,
                    BOOLEAN_FALSE,
                    wrkzon_string,
                           FlagString);
        }
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sqlFreeResults(res);
            sqlRollbackToSavepoint("location_selection");
            trnFreeStolocList(*LocList);
            *LocList = NULL;
            return (ret_status);
        }
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            sqlSetSavepoint("location_lock");

            sprintf(buffer,
                    "select curqvl, pndqvl"
                    "  from locmst "
                    " where wh_id = '%s' "
                    "   and stoloc = '%s'"
                    "   for update of pndqvl ",
                    sqlGetString(res, row, "wh_id"),
                    sqlGetString(res, row, "stoloc"));
        ret_status = sqlExecStr(buffer, &res1);
        if (ret_status != eOK)
        {
            sqlFreeResults(res1);
            res1 = NULL;
            continue;
        }            
            row1 = sqlGetRow(res1);
            ret_status = GetSpecificLocation(NULL,
                                             prd,
                                             sqlGetString(res, row, "stoloc"),
                                             LocList,
                                             sqlGetFloat(res1, row1, "curqvl"),
                                             sqlGetFloat(res1, row1, "pndqvl"),
                                             sqlGetString(res, row, "wh_id"),
                                             isPipEnabled);
            if (ret_status == eOK)
            {
                memset(int_loc, 0, sizeof(int_loc));
                strncpy(int_loc,
                        sqlGetValue(res, row, "stoloc"), STOLOC_LEN);
            sqlFreeResults(res1);
        res1 = NULL;
                sLog(0, "Found intermediate location %s!", int_loc);
                break;
            }
            else
            {
                sqlFreeResults(res1);
            res1 = NULL;
                /* Free the lock on the locations if it is not used*/
                sqlRollbackToSavepoint("location_lock");
            }
        }
        sqlFreeResults(res);
        res = NULL;
        if (ret_status != eOK)
        {
            sqlRollbackToSavepoint("location_selection");
            trnFreeStolocList(*LocList);
            *LocList = NULL;
            sLog(0, "Unable to reserve intermediate location for storage!");
            return (eINT_UNABLE_TO_RESERVE_INTERMEDIATE_LOC);
        }
        strcpy(sav_loc, int_loc);
        intare = intare->next;
    }

    /* Before we update pndqvl of the storage location, lock it make sure
     * it get updated in sequence. it is suppose to replace below code:
     *   sprintf(sqlbuffer,
     *   "select polcod "
     *   "  from poldat "
     *   " where polcod = 'ALLOC-LOC-LOCK' "
     *   "   and polvar = '%s' "
     *   "   and polval = 'MISCELLANEOUS' "
     *   "   and wh_id_tmpl = '%s' "
     *   "   and srtseq = 0 "
     *   " for update of polcod ",
     *   POLVAR_MISC,
     *   wh_id);
     *   Because above 'for update' executed at the begin, which
     *   tremendously increase the possibility of waiting for a lock
     *   if multiple users call 'allocate location' at the same time.
     */
    sprintf(buffer,
            "select curqvl, pndqvl"
            "  from locmst "
            " where wh_id = '%s' "
            "   and stoloc = '%s'"
            "   for update of pndqvl ",
            sqlGetString(lres, lrow, "wh_id"),
            sqlGetString(lres, lrow, "stoloc"));
    ret_status = sqlExecStr(buffer, &res1);
    row1 = sqlGetRow(res1);
    if (ret_status != eOK)
    {
        sLog(0, "Unable to lock final location for storage!");
        sqlFreeResults(res1);
        return (eINT_UNABLE_TO_RESERVE_FINAL_LOC);
    }
    /*  Now...get the final location... */
    ret_status = GetSpecificLocation(area,
                     prd,
                     sqlGetString(lres, lrow, "stoloc"),
                     LocList,
                                     sqlGetFloat(res1, row1, "curqvl"),
                                     sqlGetFloat(res1, row1, "pndqvl"),
                     sqlGetString(lres, lrow, "wh_id"),
                     isPipEnabled);

    sqlFreeResults(res1);

    if (ret_status != eOK)
    {
        sqlRollbackToSavepoint("location_selection");
        trnFreeStolocList(*LocList);
        *LocList = NULL;
        sLog(0, "Unable to reserve final location for storage!");
        return (eINT_UNABLE_TO_RESERVE_FINAL_LOC);
    }

    /* Finally...if we have a load, insert invmov records to reflect
       where things are going... */

    if (misTrimLen(prd->lodnum, LODNUM_LEN))
    { 
        locs = *LocList;
        while (locs)
        {
            if (strncmp(locs->lodnum, prd->lodnum, LODNUM_LEN) == 0 &&
                strncmp(locs->lodlvl, prd->lodlvl, LODLVL_LEN) == 0)
            {
                sLog(0, "Attempting insert of invmov");

                /*
                 * [12/30/2004] modified by Anders
                 * Added invmov_typ field to the invmov record to
                 * indicate why we're moving the inventory.
                 * Options include PICK, RCV, RPLN, TRNS.
                 */
                sprintf(buffer,
                        "insert into invmov (lodnum, lodlvl, seqnum, stoloc, "
                        "wh_id, xdkref, xdkqty, invmov_typ) "
                        "values ('%s', '%s', '%ld', '%s', '%s', '%s', "
                        "'%ld', '%s') ",
                        prd->lodnum, prd->lodlvl, ++ii, locs->nxtloc,
                        locs->wh_id,
                        prd->xdkref, prd->xdkqty, prd->invmov_typ);
                ret_status = sqlExecStr(buffer, NULL);
                if (ret_status != eOK)
                {
                    sqlRollbackToSavepoint("location_selection");
                    trnFreeStolocList(*LocList);
                    *LocList = NULL;
                    return (ret_status);
                }
            }
            locs = locs->next;
    }
    }

    return (eOK);
}


/* Add the locations that exceed to the max loads per aisle
 * If we cannot find any location to be allocate, we will
 * try to find one of them to be allocated.
 * Also, we will store the area information for using later.
 */
static long AddToPassLocList(mocaDataRes *res,
                             mocaDataRow *row,
                             AREA_LIST   *area)
{
    PASS_STOLOC_LIST    *listptr;
    PASS_STOLOC_LIST    *loclisttail;

    if ((listptr = (PASS_STOLOC_LIST *) calloc(sizeof(PASS_STOLOC_LIST), 1)) 
        == NULL)
    {
        return (eNO_MEMORY);
    }    

    listptr->res = res;
    listptr->row = row;
    listptr->area = area;
    listptr->next = NULL;

    if (pass_stoloc_list == NULL)
    {
        pass_stoloc_list = listptr;
    }
    else
    {
        loclisttail = pass_stoloc_list;
        while (loclisttail->next != NULL)
            loclisttail = loclisttail->next;
        loclisttail->next = (PASS_STOLOC_LIST *) listptr;
    }
    
    return (eOK);
}

static long WithinMaxAllowedForAisle(AREA_LIST *area,
                                     PRODUCT   *prd,
                                     mocaDataRes *res,
                                     mocaDataRow *row)
{
    mocaDataRow     *Arow = NULL;
    mocaDataRes     *Ares = NULL;
    AISLE_LIST      *ai;
    char            aisle_id[AISLE_ID_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    char            buffer[2000];
    long            ret_status;
    long            lodqty = 0;

    memset(aisle_id, 0, sizeof(aisle_id));
    memset(wh_id, 0, sizeof(wh_id));
    memset(buffer, 0, sizeof(buffer));

    /* if it is mixed part, just return */
    if (prd->mixed == TRUE)
    {
        return (eOK);
    }
    
    /* get the aisle for the location */
    sprintf(buffer,
            " select aisle_id, "
            "        wh_id "
            "   from locmst lm "
            "  where lm.stoloc = '%s' "
            "    and lm.wh_id = '%s' ",
            sqlGetString(res, row, "stoloc"),
            sqlGetString(res, row, "wh_id"));
    ret_status = sqlExecStr(buffer, &Ares);
    if (ret_status != eOK)
    {
        sqlFreeResults(Ares);
        return (eOK);
    }
    Arow = sqlGetRow(Ares);
    misTrimcpy(aisle_id, sqlGetString(Ares, Arow, "aisle_id"), AISLE_ID_LEN);
    misTrimcpy(wh_id, sqlGetString(Ares, Arow, "wh_id"), WH_ID_LEN);
    sqlFreeResults(Ares);

    lodqty = ConvertToLoads(prd->prtnum,
                            prd->prt_client_id,
                            prd->totqty);

    /* find lodqty on the the aisle of the location
     * if it exceed the max value, add it to Pass Loc List
     * At the end, if we didn't get any location, we will
     * find one of the PassLocList to do allocate
     */

    for (ai = aisle_list; ai; ai = ai->next)
    {
        if (misTrimStrncmp(ai->aisle_id, aisle_id, AISLE_ID_LEN) == 0 &&
            misTrimStrncmp(ai->wh_id, wh_id, WH_ID_LEN) == 0 &&
            ai->qvl + lodqty > area->max_loads_aisle)
        {
            AddToPassLocList(res, row, area);
            return (!eOK);
        }
    }

    return (eOK);
}

static long FindEmptyLocation(AREA_LIST * area,
			      PRODUCT * prd,
			      STOLOC_LIST ** LocList, 
                              long isPipEnabled,
                              double lodhgt)
{
    char            buffer[3500];
    char           *zone_exclusion = NULL;
    char           *notin_string = NULL;
    char            prdzon[VELZON_LEN + 1];
    char            wrkzon_string[50];
    long            product_height;
    double          tmp_height;
    long            ret_status;
    PRODUCT        *tmpprd = NULL;
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL;
    mocaDataRow    *firstrow = NULL;
    char            FlagString[50];
    RETURN_STRUCT *CmdRes = NULL;
    long            tmp_count;
    long            case_count;
    long            levels;
    long            min_levels;
    tmp_count = 0;
    case_count = 0;
    levels = 0;
    min_levels = 0;


    if (strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0 ||
	strcmp(prd->stotyp, ALLOCATE_TOPOFF_REPLEN) == 0)
	sprintf(FlagString, " and pckflg = '%d' and repflg = '%d' ",
		BOOLEAN_TRUE, BOOLEAN_TRUE);
    else
	sprintf(FlagString, " and stoflg = '%d' ", BOOLEAN_TRUE);

    /* Create sql command */

    if (prd->mixed == TRUE &&
	strncmp(area->pckcod, PCKCOD_MIXABLE, PCKCOD_LEN) != 0)
    {
	sLog(0, "Bypassing non-mixable area %s/%s because of Mixed Load",
	        area->wh_id, area->arecod);

	return (eDB_NO_ROWS_AFFECTED);
    }

    if (prd->mixed == TRUE)
    {
	sLog(0, "Selecting Empty locations in area %s/%s for Mixed Load",
	        area->wh_id, area->arecod);
    }
    else
    {
	sLog(0, "Selecting Empty locations in area %s/%s for "
	        "Part (%s) Client (%s)",
	        area->wh_id, area->arecod, prd->prtnum, prd->prt_client_id);
    }

    firstrow = NULL;

    product_height = 0;
    if (strncmp(area->loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0 ||
	strncmp(area->loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0 ||
	strncmp(area->loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
    {
	for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
	{
	    if (product_height < (long) ceil(tmpprd->cashgt))
		product_height = (long) ceil(tmpprd->cashgt);
	}
    }
    else
    {
	if (prd->mixed == TRUE)
	{
	    tmp_height = 0.0;
	    for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
	    {
              /* get the tallest case height*/
             if ((long) ceil ((double) tmpprd->cashgt) > (long)tmp_height)
                 tmp_height = (double) tmpprd->cashgt;

             /* get the smallest number of cases per tier*/
             if (tmp_count > 0 ) 
             {
                 if ((long) ceil((double) tmpprd->caslvl) < min_levels)
                            min_levels = (long) (double) tmpprd->caslvl;
             }
             else
             {
                 min_levels = (long) (double) tmpprd->caslvl;
                 tmp_count++;
             }
             /* get the number of cases */
             case_count += (long) ceil((double) tmpprd->totqty / (double) tmpprd->untcas );
          }
            levels = (long)ceil((double)case_count / (double)min_levels);
            product_height = (long)ceil(tmp_height * levels);
            sLog(0, "Mixed Pallet Case Max Height : %ld", (long) tmp_height);
            sLog(0, "Mixed Pallet Min Cases Per Tier : %ld",min_levels);
            sLog(0, "Mixed Pallet Number of Cases : %ld", case_count);
            sLog(0, "Mixed Pallet Number of Tiers : %ld", levels);
	}
	else
	{
            if (prd->untcas > 0.0 && prd->caslvl > 0.0)
            {
                product_height = (long)ceil((((double)prd->totqty /
                                              (double) prd->untcas) /
                                              (double) prd->caslvl)
                                              * prd->cashgt);
            }
            /* else product_height = 0 as initialized */
	}
    }

    if (area->pass_count == PASS_EMPTY_LOC_IN_SAME_VELZON)
    {
	sLog(0, "Select move path");

    ret_status = CheckMovePath(area, prd, &zone_exclusion, isPipEnabled);
	if (ret_status != eOK)
	{
            if (zone_exclusion) free (zone_exclusion);
	    return (ret_status);
	}

	if (zone_exclusion)
	{
	    if (!misDynStrcpy(&notin_string, " and exclude_wrkzon = \""))
            {
                if (zone_exclusion) free (zone_exclusion);
                if (notin_string) free (notin_string);
		return (eNO_MEMORY);
            }
	    if (!misDynStrcat(&notin_string, zone_exclusion))
            {
                if (zone_exclusion) free (zone_exclusion);
                if (notin_string) free (notin_string);
		return (eNO_MEMORY);
            }
	    if (!misDynStrcat(&notin_string, "\" "))
            {
                if (zone_exclusion) free (zone_exclusion);
                if (notin_string) free (notin_string);
		return (eNO_MEMORY);
            }
	}

        if (zone_exclusion) free (zone_exclusion);

	memset(wrkzon_string, 0, sizeof(wrkzon_string));

	if (strcmp(prd->reqzon, "") != 0)
        {
	    sprintf(wrkzon_string, " and wrkzon = '%s' ", prd->reqzon);
        }

        /* If the prtnum is not an EmptyPart then add the asset height to the 
         * product height. 
         * If the prtnum is an EmptyPart then product height = asset height.
         * We will use this to check against the location height.
         */
        if (prd->empty == 0)
        {
            product_height +=  (long)ceil(prd->asset_hgt);
        }
        else
        {
            /* we are looking at an asset get the asset hgt */
            product_height = (long)prd->asset_hgt;
        }

	sprintf(buffer,
		"list empty locations for storage "
		" where arecod = '%s' "
		"   and wh_id = '%s' "
                "   and putprox_flg = '%d' "
		"   and stotyp = '%s' "
                "   and prtnum = '%s' "
                "   and prt_client_id = '%s' "
		"   and lochgt = %d "
		"   %s "
		"   %s "
		"   %s ",
		area->arecod, 
		area->wh_id,
                area->putprox_flg,
		prd->stotyp,
		prd->prtnum, 
		prd->prt_client_id,
        product_height,
		wrkzon_string,  
		notin_string ? notin_string : "",
		FlagString);

	ret_status = srvInitiateCommand(buffer, &CmdRes);
	if (ret_status != eOK)
	{
            if (notin_string) free (notin_string);
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);
	    return (ret_status);
	}
	res = srvGetResults(CmdRes);

	CmdRes->ReturnedData = NULL;
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
        CmdRes = NULL;
        if (notin_string) free (notin_string);

	area->EmptyRes = res;
	memset(prdzon, 0, sizeof(prdzon));
	strncpy(prdzon, prd->velzon, VELZON_LEN);

	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
	    if (strncmp(sqlGetValue(res, row, "velzon"),
			prdzon, VELZON_LEN) >= 0)
		break;
	}
	if (row)
	    area->EmptyRowZonesGreater = row;
	firstrow = row;
    }
    else
    {
	if (area->EmptyRes)
	{
	    firstrow = sqlGetRow(area->EmptyRes);
	    res = area->EmptyRes;
	}
    }

    for (row = firstrow; row; row = sqlGetNextRow(row))
    {
	/* If we hit the spot that we started during the 
	   previous pass, then we know we need to go no further... */
	if ((area->pass_count == PASS_EMPTY_LOC_IN_DIFF_VELZON ||
	     area->pass_count == PASS_EMPTY_LOC_IN_DIFF_VELZON_2ND) &&
	    area->EmptyRowZonesGreater == row)
	    break;

	ret_status = RoomToStore(area,
				 prd,
				 res,
				 row,
				 lodhgt);
	if (ret_status != eOK)
	{
	    sLog(0, "(%d) Location: %s/%s - NO ROOM TO STORE",
		    ret_status, 
		    sqlGetValue(res, row, "wh_id"),
		    sqlGetValue(res, row, "stoloc"));

	    continue;
	}

        ret_status = sIsLocationAssignedToOther(prd, 
                                            sqlGetString(res, row, "stoloc"),
                                            sqlGetString(res, row, "wh_id"));
        if (ret_status != eOK)
	{
	    sLog(0, 
		 "Location %s/%s is assigned to another part"
		 " - unable to use!", 
		 sqlGetValue(res, row, "wh_id"),
		 sqlGetString(res, row, "stoloc"));
	    continue;
	}
     
    /* if there is a max loads per aisle policy,
     * check the loads on the aisle is not max than the value.
     * If it is, than save the location recordset to the 
     * PASS_STOLOC_LIST, we will use it later.
     */
    if (area->max_loads_aisle > 0 
        && misTrimLen(sqlGetString(res, row, "aisle_id"), AISLE_ID_LEN))
    {
        ret_status = WithinMaxAllowedForAisle(
                                area,
                                prd,
                                res,
                                row);
        if (ret_status != eOK)
        {
            sLog(0, "(%d) Location: %s/%s - AISLE MAX QUANLITY LIMIT ",
            ret_status, 
            sqlGetValue(res, row, "wh_id"),
            sqlGetValue(res, row, "stoloc"));
            continue;
        }
    }

	/* At this point...it is OK just to grab the location... */
	ret_status = GetLocation(area,
				 prd,
				 res,
				 row,
				 LocList,
                 isPipEnabled);
	if (ret_status == eOK)
	{
        return (eOK);
	}
    }


    return (eDB_NO_ROWS_AFFECTED);
}


static long FindMixedLocation(AREA_LIST * area,
			      PRODUCT * prd,
			      STOLOC_LIST ** LocList,
                              long isPipEnabled,
                              double lodhgt)
{
    char            buffer[3500];
    long            ret_status;
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL;
    char           *zone_exclusion = NULL;
    char           *notin_string = NULL;
    char            FlagString[100];
    long            product_height;
    double          tmp_height;
    PRODUCT        *tmpprd = NULL;
    char            wrkzon_string[100];
    long            tmp_count;
    long            case_count;
    long            levels;
    long            min_levels;

    RETURN_STRUCT *CmdRes = NULL;

    tmp_count = 0;
    case_count = 0;
    levels = 0;
    min_levels = 0;


    if (strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0 ||
	strcmp(prd->stotyp, ALLOCATE_TOPOFF_REPLEN) == 0)
	sprintf(FlagString, "  and pckflg = '%d' and repflg = '%d' ",
		BOOLEAN_TRUE, BOOLEAN_TRUE);
    else
	sprintf(FlagString, " and stoflg = '%d' ", BOOLEAN_TRUE);

    /* Create sql command */

    if (prd->mixed == TRUE)
    {
	sLog(0, "Selecting locations in area %s/%s for Mixed Load",
	        area->wh_id, area->arecod);
    }
    else
    {
	sLog(0, "Selecting Mixed locations in area %s/%s for "
	        "Mixing Part (%s) Client (%s)",
	        area->wh_id, area->arecod, prd->prtnum, prd->prt_client_id);
    }

    if (strncmp(area->pckcod, PCKCOD_MIXABLE, PCKCOD_LEN) != 0)
    {
	sLog(0, "Bypassing non-mixable area");
	return (eDB_NO_ROWS_AFFECTED);
    }

    ret_status = CheckMovePath(area, prd, &zone_exclusion, isPipEnabled);
    if (ret_status != eOK)
    {
        if (zone_exclusion) free (zone_exclusion);
	return (ret_status);
    }

    if (zone_exclusion)
    {
	if (!misDynStrcpy(&notin_string, " and exclude_wrkzon = \""))
        {
            if (zone_exclusion) free (zone_exclusion);
            if (notin_string) free (notin_string);
	    return (eNO_MEMORY);
        }

	if (!misDynStrcat(&notin_string, zone_exclusion))
        {
            if (zone_exclusion) free (zone_exclusion);
            if (notin_string) free (notin_string);
	    return (eNO_MEMORY);
        }

	if (!misDynStrcat(&notin_string, "\" "))
        {
            if (zone_exclusion) free (zone_exclusion);
            if (notin_string) free (notin_string);
	    return (eNO_MEMORY);
        }
    }

    if (zone_exclusion) free (zone_exclusion);

    memset(wrkzon_string, 0, sizeof(wrkzon_string));
    if (strcmp(prd->reqzon, "") != 0)
	sprintf(wrkzon_string, " and wrkzon = '%s' ", prd->reqzon);

    product_height = 0;
    if (strncmp(area->loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0 ||
	strncmp(area->loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0 ||
	strncmp(area->loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
    {
	for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
	{
	    if (product_height < (long) ceil(tmpprd->cashgt))
		product_height = (long) ceil(tmpprd->cashgt);
	}
    }
    else
    {
	if (prd->mixed == TRUE)
	{
	    tmp_height = 0.0;
	    for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
	    {
              /* get the tallest case height*/
             if ((long) ceil ((double) tmpprd->cashgt) > (long)tmp_height)
                 tmp_height = (double) tmpprd->cashgt;

             /* get the smallest number of cases per tier*/
             if (tmp_count > 0 ) 
             {
                 if ((long) ceil((double) tmpprd->caslvl) < min_levels)
                            min_levels = (long) (double) tmpprd->caslvl;
             }
             else
             {
                 min_levels = (long) (double) tmpprd->caslvl;
                 tmp_count++;
             }
             /* get the number of cases */
             case_count += (long) ceil((double) tmpprd->totqty / (double) tmpprd->untcas );
          }
            levels = (long)ceil((double)case_count / (double)min_levels);
            product_height = (long)ceil(tmp_height * levels);
            sLog(0, "Mixed Pallet Case Max Height : %ld", (long)tmp_height);
            sLog(0, "Mixed Pallet Min Cases Per Tier : %ld",min_levels);
            sLog(0, "Mixed Pallet Number of Cases : %ld", case_count);
            sLog(0, "Mixed Pallet Number of Tiers : %ld", levels);
	}
	else
	{
            if (prd->untcas > 0.0 && prd->caslvl > 0.0)
            {
                product_height = (long)ceil((((double)prd->totqty /
                                              (double) prd->untcas) /
                                              (double) prd->caslvl)
                                              * prd->cashgt);
            }
            /* else product_height = 0 as initialized */
	}
    }
    /* If the prtnum is not an EmptyPart then add the asset height to the 
     * product height. 
     * If the prtnum is an EmptyPart then product height = asset height.
     * We will use this to check against the location height.
     */
    if (prd->empty == 0)
    {
        product_height +=  (long)ceil(prd->asset_hgt);
    }
    else
    {
        /* This is an empty part hence consider asset_hgt. */
        product_height = (long)prd->asset_hgt;
    }

    sprintf(buffer,
	    "list mixed locations for storage "
	    "  where arecod = '%s' "
	    "    and wh_id = '%s' "
	    "    and stotyp = '%s' "
	    "    and prtnum = '%s' "
	    "    and prt_client_id = '%s' "
	    "    and lochgt = %d "
	    "   %s "
	    "   %s "
	    "   %s ",
	    area->arecod, 
	    area->wh_id, 
            prd->stotyp,
	    prd->prtnum, prd->prt_client_id, 
            product_height,
	    wrkzon_string, 
	    notin_string ? notin_string : "",
	    FlagString);

    ret_status = srvInitiateCommand(buffer, &CmdRes);
    if (ret_status != eOK)
    {
        if (notin_string) free (notin_string);
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	return (ret_status);
    }
    res = srvGetResults(CmdRes);
    
    CmdRes->ReturnedData = NULL;
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    CmdRes = NULL;
    if (notin_string) free (notin_string);

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	ret_status = RoomToStore(area,
				 prd,
				 res,
				 row, 
				 lodhgt);
	if (ret_status != eOK)
	{
	    sLog(0, "Location: %s/%s - NO ROOM TO STORE",
	            sqlGetValue(res, row, "wh_id"),
		    sqlGetValue(res, row, "stoloc"));
	    continue;
	}

        ret_status = sIsLocationAssignedToOther(prd, 
                                            sqlGetString(res, row, "stoloc"),
                                            sqlGetString(res, row, "wh_id"));
        if (ret_status != eOK)
	{
	    sLog(0, 
		 "Location %s/%s is assigned to another part"
		 " - unable to use!", 
		 sqlGetString(res, row, "wh_id"),
		 sqlGetString(res, row, "stoloc"));
	    continue;
	}
        
    /* if there is a max loads per aisle policy,
     * check the loads on the aisle is not max than the value.
     * If it is, than save the location recordset to the 
     * PASS_STOLOC_LIST, we will use it later.
     */
    if (area->max_loads_aisle > 0 
        && misTrimLen(sqlGetString(res, row, "aisle_id"), 
                                    AISLE_ID_LEN) )
    {
        ret_status = WithinMaxAllowedForAisle(
                                area,
                                prd,
                                res,
                                row);
        if (ret_status != eOK)
        {
            sLog(0, "(%d) Location: %s/%s - AISLE MAX QUANLITY LIMIT ",
            ret_status,
            sqlGetValue(res, row, "wh_id"),
            sqlGetValue(res, row, "stoloc"));
            continue;
        }
    }

	/* At this point...it is OK just to grab the location... */
	ret_status = GetLocation(area,
				 prd,
				 res,
				 row,
				 LocList,
                 isPipEnabled);
	if (ret_status == eOK)
	{
	    sqlFreeResults(res);
	    return (eOK);
	}
    }

    sqlFreeResults(res);
    return (eDB_NO_ROWS_AFFECTED);

}

static long FindPartialLocation(AREA_LIST * area,
				PRODUCT * prd,
				STOLOC_LIST ** LocList,
                                long isPipEnabled,
                                double lodhgt)
{
    char            buffer[3500];
    long            ret_status;
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL;
    mocaDataRow    *firstrow = NULL;
    char            FlagString[100];
    char            wrkzon_string[100];
    char           *notin_string = NULL;
    char           *zone_exclusion = NULL;
    char            prdzon[VELZON_LEN + 1];
    long            product_height;
    double          tmp_height;
    PRODUCT        *tmpprd = NULL;
    RETURN_STRUCT  *CmdRes = NULL;
    long            tmp_count;
    long            case_count;
    long            levels;
    long            min_levels;
    tmp_count = 0;
    case_count = 0;
    levels = 0;
    min_levels = 0;


    if (strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0 ||
	strcmp(prd->stotyp, ALLOCATE_TOPOFF_REPLEN) == 0)
	sprintf(FlagString, " and pckflg = '%d' and repflg = '%d' ",
			    BOOLEAN_TRUE, BOOLEAN_TRUE);
    else
	sprintf(FlagString, " and stoflg = '%d' ", BOOLEAN_TRUE);

    /* Create sql command */

    if (prd->mixed == TRUE &&
	strncmp(area->pckcod, PCKCOD_MIXABLE, PCKCOD_LEN) != 0)
    {
	sLog(0, "Bypassing non-mixable area %s/%s because of Mixed Load",
	        area->wh_id, area->arecod);

	return (eDB_NO_ROWS_AFFECTED);
    }

    if (prd->mixed == TRUE)
    {
	sLog(0, "Selecting Partial locations in area %s/%s for Mixed Load",
	        area->wh_id, area->arecod);
    }
    else
    {
	sLog(0, "Selecting Partial locations in area %s/%s for Part (%s)"
	        " and Client (%s)",
	        area->wh_id, area->arecod, prd->prtnum, prd->prt_client_id);
    }

    firstrow = NULL;

    /* The first time through, we select all the locations and order them
       by velocity zone.  For the first pass, we have to skip over any 
       returned that have a velzon <= our product's velzon.  We save that 
       marker in the result set, and use it for the second pass... */

    product_height = 0;
    if (strncmp(area->loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0 ||
	strncmp(area->loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0 ||
	strncmp(area->loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
    {
	for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
	{
	    if (product_height < (long) ceil(tmpprd->cashgt))
		product_height = (long) ceil(tmpprd->cashgt);
	}
    }
    else
    {
	if (prd->mixed == TRUE)
	{
	    tmp_height = 0.0;
	    for (tmpprd = prd; tmpprd; tmpprd = tmpprd->next)
	    {
              /* get the tallest case height*/
             if ((long) ceil ((double) tmpprd->cashgt) > (long)tmp_height)
                 tmp_height = (double) tmpprd->cashgt;

             /* get the smallest number of cases per tier*/
             if (tmp_count > 0 ) 
             {
                 if ((long) ceil((double) tmpprd->caslvl) < min_levels)
                            min_levels = (long) (double) tmpprd->caslvl;
             }
             else
             {
                 min_levels = (long) (double) tmpprd->caslvl;
                 tmp_count++;
             }
             /* get the number of cases */
             case_count += (long) ceil((double) tmpprd->totqty / (double) tmpprd->untcas );
            }
            levels = (long)ceil((double)case_count / (double)min_levels);
            product_height = (long)ceil(tmp_height * levels);
            sLog(0, "Mixed Pallet Case Max Height : %ld", (long)tmp_height);
            sLog(0, "Mixed Pallet Min Cases Per Tier : %ld",min_levels);
            sLog(0, "Mixed Pallet Number of Cases : %ld", case_count);
            sLog(0, "Mixed Pallet Number of Tiers : %ld", levels);
	}
	else
	{
            if (prd->untcas > 0.0 && prd->caslvl > 0.0)
            {
                product_height = (long)ceil((((double)prd->totqty /
                                              (double) prd->untcas) /
                                              (double) prd->caslvl)
                                              * prd->cashgt);
            }
            /* else product_height = 0 as initialized */
	}
    }

    if (area->pass_count == PASS_PARTIAL_LOC_IN_SAME_VELZON)
    {

	/* Next check move paths to determine if we need an intermediate
	   location... */

    ret_status = CheckMovePath(area, prd, &zone_exclusion, isPipEnabled);
	if (ret_status != eOK)
	{
            if (zone_exclusion) free (zone_exclusion);
	    return (ret_status);
	}

	if (zone_exclusion)
	{
	    if (!misDynStrcpy(&notin_string, " and exclude_wrkzon = \""))
            {
                if (zone_exclusion) free (zone_exclusion);
                if (notin_string) free (notin_string);
		return (eNO_MEMORY);
            }

	    if (!misDynStrcat(&notin_string, zone_exclusion))
            {
                if (zone_exclusion) free (zone_exclusion);
                if (notin_string) free (notin_string);
		return (eNO_MEMORY);
            }

	    if (!misDynStrcat(&notin_string, "\" "))
            {
                if (zone_exclusion) free (zone_exclusion);
                if (notin_string) free (notin_string);
		return (eNO_MEMORY);
            }
	}

        if (zone_exclusion) free (zone_exclusion);

	memset(wrkzon_string, 0, sizeof(wrkzon_string));

	if (strcmp(prd->reqzon, "") != 0)
        {
	    sprintf(wrkzon_string, " and wrkzon = '%s' ", prd->reqzon);
        }

        /* If the prtnum is not an EmptyPart then add the asset height to the 
         * product height. 
         * If the prtnum is an EmptyPart then product height = asset height.
         * We will use this to check against the location height.
         */
        if (prd->empty == 0)
        {
            product_height +=  (long)ceil(prd->asset_hgt);
        }
        else
        {
            /* This is an empty part hence consider asset_hgt. */
            product_height = (long)prd->asset_hgt;
        }

	sprintf(buffer,
		"list partial locations for storage "
		" where arecod = '%s' "
		"   and wh_id = '%s' "
		"   and prtnum = '%s' "
		"   and prt_client_id = '%s' "
		"   and stotyp = '%s' "
		"   and lochgt = %d "
		"   %s "
		"   %s "
		"   %s ",
		area->arecod, 
		area->wh_id, 
		prd->prtnum, 
		prd->prt_client_id,
		prd->stotyp,
                product_height,
		wrkzon_string,  
		notin_string ? notin_string : "",
		FlagString);

	ret_status = srvInitiateCommand(buffer, &CmdRes);
	if (ret_status != eOK)
	{
            if (notin_string) free (notin_string);
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);
	    return (ret_status);
	}
	res = srvGetResults(CmdRes);
	/* The following two lines are done so that we can treat
	 * the res struct as if we had received it from sqllib 
	 */
	CmdRes->ReturnedData = NULL; 
	srvFreeMemory(SRVRET_STRUCT, CmdRes);

        if (notin_string) free (notin_string);

	area->PartialRes = res;
	memset(prdzon, 0, sizeof(prdzon));
	strncpy(prdzon, prd->velzon, VELZON_LEN);

	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
	    if (strncmp(sqlGetValue(res, row, "velzon"),
			prdzon, VELZON_LEN) >= 0)
		break;
	}
	if (row)
	    area->PartialRowZonesGreater = row;
	firstrow = row;
    }
    else
    {
	if (area->PartialRes)
	{
	    firstrow = sqlGetRow(area->PartialRes);
	    res = area->PartialRes;
	}
    }

    for (row = firstrow; row; row = sqlGetNextRow(row))
    {

	/* If we hit the spot that we started during the 
	   previous pass, then we know we need to go no further... */
	if ((area->pass_count == PASS_PARTIAL_LOC_IN_DIFF_VELZON ||
	     area->pass_count == PASS_PARTIAL_LOC_IN_DIFF_VELZON_2ND) &&
	    area->PartialRowZonesGreater == row)
	    break;

	ret_status = RoomToStore(area,
				 prd,
				 res,
				 row, 
				 lodhgt);
	if (ret_status != eOK)
	{
	    sLog(0, "Location: %s/%s - NO ROOM TO STORE",
	            sqlGetString(res, row, "wh_id"),
		    sqlGetValue(res, row, "stoloc"));

	    continue;
	}
	ret_status = PartOkInLoc(area,
				 prd,
				 res,
				 row);
	if (ret_status != eOK)
	{
	    sLog(0, "Location: %s/%s - PART CONFLICT!",
	            sqlGetString(res, row, "wh_id"),
		    sqlGetValue(res, row, "stoloc"));

	    continue;
	}

	/* VAR - Check for any QT Holds in this partial loc.  We can only mix
	 * inventory with the same QT Hold status and the same
	 * manufacture date
	 */
	ret_status = QTHoldMixOkInLoc(prd,
				      res,
				      row);
	if (ret_status != eOK)
	{
	    sLog(0, "Location: %s/%s - QT Hold mix CONFLICT!",
	            sqlGetString(res, row, "wh_id"),
		    sqlGetValue(res, row, "stoloc"));

	    continue;
	}

	ret_status = sIsLocationAssignedToOther(prd, 
                                            sqlGetString(res, row, "stoloc"),
                                            sqlGetString(res, row, "wh_id"));
        if (ret_status != eOK)
	{
	    sLog(0, 
		 "Location %s/%s is assigned to another part"
		 " - unable to use!",
		 sqlGetString(res, row, "wh_id"), 
		 sqlGetString(res, row, "stoloc"));
	    continue;
	}
        
    /* if there is a max loads per aisle policy,
     * check the loads on the aisle is not max than the value.
     * If it is, than save the location recordset to the 
     * PASS_STOLOC_LIST, we will use it later.
     */
    if (area->max_loads_aisle > 0 
        && misTrimLen(sqlGetString(res, row, "aisle_id"), 
                                    AISLE_ID_LEN) )
    {
        ret_status = WithinMaxAllowedForAisle(
                                area,
                                prd,
                                res,
                                row);
        if (ret_status != eOK)
        {
            sLog(0, "(%d) Location: %s/%s - AISLE MAX QUANLITY LIMIT ",
            ret_status,
            sqlGetString(res, row, "wh_id"),
            sqlGetValue(res, row, "stoloc"));
            continue;
        }
    }

	/* At this point...it is OK just to grab the location... */
	ret_status = GetLocation(area,
				 prd,
				 res,
				 row,
				 LocList,
                 isPipEnabled);
	if (ret_status == eOK)
	{
        return (eOK);
	}

    }

    return (eDB_NO_ROWS_AFFECTED);

}

static long CheckPassStrategy(char *strategy, long pass)
{
    /*
     *  The only purpose of this function is to help determine 
     *  if the strategy specified for the area code indicates that
     *  we should a) process this iteration for this area and get out
     *  b) process this iteration for this area and continue on with
     *  the next iteration, or c) do not process this iteration for 
     *  this area...
     *
     *  We return one of three values:
     *     -2 = do not process this iteration
     *     -1 = skip this particular iteration, but continue processing
     *      0 = get out after this iteration
     *      1 = continue on after this iteration
     *
     */

    /* If you look at the passes and the strategies, then for the
     * first iteration, we are attempting to accomplish something
     * like the table below:
     *     Strategy         Processing flow
     *     --------         ----------------------------------
     *     STPKZ            1 ;   ; 3 ;   ; 5 ; 6 ; 7 ;
     *     STBKZ            1       3 ;   ; 5 ; 6 ; 7 ;
     *     STPBZ            1   2 ; 3 ; 4 ;   ;   ; 7 ;
     *     STBBZ            1   2   3   4 ;   ;   ; 7 ;
     *     STAL             1       3       5   6   7 ;
     *     SPO              1 ; 2 ;   ;   ;   ;   ; 7 ;
     *     SEO                ;   ; 3 ; 4 ;   ;   ;   ;
     * In the above table, if, for the strategy for the area,
     * the next item in your processing list is a semi-colon,
     * then we want to exit after this iteration (0).  If this
     * pass is not listed in the list (and the next item is not
     * a semicolon), then we skip this processing (-1).  If the 
     * pass is not listed and the next item is a semi-colon, then 
     * we skip processing and get out (-2). If pass is in the list
     * and the next item is not a semicolon, then we process this
     * iteration and continue processing (1).
     *
     */

    switch (pass)
    {
    case PASS_PARTIAL_LOC_IN_SAME_VELZON:	/* Pass 1 */
	if (strncmp(strategy,
		    STRATEGY_TRY_PRTL_KEEP_ZONE,
		    strlen(STRATEGY_TRY_PRTL_KEEP_ZONE)) == 0 ||
	    strncmp(strategy,
		    STRATEGY_PARTIALS_ONLY,
		    strlen(STRATEGY_PARTIALS_ONLY)) == 0)
	    return (0);
	else if (strncmp(strategy,
			 STRATEGY_EMPTIES_ONLY,
			 strlen(STRATEGY_EMPTIES_ONLY)) == 0)
	    return (-2);
	else
	    return (1);
	/*NOTREACHED */
	break;

    case PASS_PARTIAL_LOC_IN_DIFF_VELZON:	/* Pass 2 */
	if (strncmp(strategy,
		    STRATEGY_TRY_PRTL_KEEP_ZONE,
		    strlen(STRATEGY_TRY_PRTL_KEEP_ZONE)) == 0 ||
	    strncmp(strategy,
		    STRATEGY_EMPTIES_ONLY,
		    strlen(STRATEGY_EMPTIES_ONLY)) == 0)
	    return (-2);
	else if (strncmp(strategy,
			 STRATEGY_TRY_BOTH_KEEP_ZONE,
			 strlen(STRATEGY_TRY_BOTH_KEEP_ZONE)) == 0 ||
		 strncmp(strategy,
			 STRATEGY_TRY_ALL_LOCATIONS,
			 strlen(STRATEGY_TRY_ALL_LOCATIONS)) == 0)
	    return (-1);
	else if (strncmp(strategy,
			 STRATEGY_TRY_BOTH_BREAK_ZONE,
			 strlen(STRATEGY_TRY_BOTH_BREAK_ZONE)) == 0)
	    return (1);
	else
	    return (0);
	/*NOTREACHED */
	break;

    case PASS_EMPTY_LOC_IN_SAME_VELZON:	/* Pass 3 */
	if (strncmp(strategy,
		    STRATEGY_TRY_BOTH_BREAK_ZONE,
		    strlen(STRATEGY_TRY_BOTH_BREAK_ZONE)) == 0 ||
	    strncmp(strategy,
		    STRATEGY_TRY_ALL_LOCATIONS,
		    strlen(STRATEGY_TRY_ALL_LOCATIONS)) == 0)
	    return (1);
	else if (strncmp(strategy,
			 STRATEGY_PARTIALS_ONLY,
			 strlen(STRATEGY_PARTIALS_ONLY)) == 0)
	    return (-2);
	else
	    return (0);
	/*NOTREACHED */
	break;

    case PASS_EMPTY_LOC_IN_DIFF_VELZON:	/* Pass 4 */
	if (strncmp(strategy,
		    STRATEGY_PARTIALS_ONLY,
		    strlen(STRATEGY_PARTIALS_ONLY)) == 0 ||
	    strncmp(strategy,
		    STRATEGY_TRY_BOTH_KEEP_ZONE,
		    strlen(STRATEGY_TRY_BOTH_KEEP_ZONE)) == 0 ||
	    strncmp(strategy,
		    STRATEGY_TRY_PRTL_KEEP_ZONE,
		    strlen(STRATEGY_TRY_PRTL_KEEP_ZONE)) == 0)
	    return (-2);
	else if (strncmp(strategy,
			 STRATEGY_TRY_ALL_LOCATIONS,
			 strlen(STRATEGY_TRY_ALL_LOCATIONS)) == 0)
	    return (-1);
	else
	    return (0);
	/*NOTREACHED */
	break;

    case PASS_PARTIAL_LOC_IN_DIFF_VELZON_2ND:	/* Pass 5 */
    case PASS_EMPTY_LOC_IN_DIFF_VELZON_2ND:	/* Pass 6 */
	if (strncmp(strategy,
		    STRATEGY_TRY_PRTL_KEEP_ZONE,
		    strlen(STRATEGY_TRY_PRTL_KEEP_ZONE)) == 0 ||
	    strncmp(strategy,
		    STRATEGY_TRY_BOTH_KEEP_ZONE,
		    strlen(STRATEGY_TRY_BOTH_KEEP_ZONE)) == 0)
	    return (0);
	else if (strncmp(strategy,
			 STRATEGY_TRY_ALL_LOCATIONS,
			 strlen(STRATEGY_TRY_ALL_LOCATIONS)) == 0)
	    return (1);
	else
	    return (-2);
	/*NOTREACHED */
	break;
    case PASS_MIX_PRODUCT:
	if (strncmp(strategy,
		    STRATEGY_EMPTIES_ONLY,
		    strlen(STRATEGY_EMPTIES_ONLY)) == 0)
	    return (-2);
	else
	    return (0);
	/*NOTREACHED */
	break;
    }
    return (-1);
}

static long GetAisleQvl(AREA_LIST *area,
                        PRODUCT *prd,
                        char *aisle_id,
                        char *wh_id)
{
    long            ret_status;
    char            *buffer = NULL;    
    char            sqlbufasl[500];
    char            sqlbuftbl[500];
    char            tmpstr[50];
    char            *loc_string = NULL;
    char            *sqlbuf = NULL;
    mocaDataRow     *row = NULL;
    mocaDataRes     *res = NULL;
    long            untqty = 0;
    long            lodqty = 0;
    long            pndqty = 0;
    long            num_rows = 0;

    memset(sqlbufasl,   0, sizeof(sqlbufasl));
    memset(sqlbuftbl,   0, sizeof(sqlbuftbl));
    
    /*
     * First, let's get the the location associated
     * to the aisle_id.
     */
    sprintf(sqlbufasl,
            " select distinct lm.stoloc, lm.wh_id "
            "   from locmst lm "
            "  where lm.aisle_id = '%s' "
            "    and lm.wh_id = '%s' ",
            aisle_id,
            wh_id);
    ret_status = sqlExecStr(sqlbufasl, &res);

    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        return (eDB_NO_ROWS_AFFECTED);
    }

    num_rows = sqlGetNumRows(res);

    /* there need to initial variable loc_string */
    if (!misDynStrcat(&loc_string, ""))
    {
        if (loc_string) free (loc_string);
        if (sqlbuf) free (sqlbuf);
        if (buffer) free (buffer);
        return (eNO_MEMORY);
    }

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        if (strlen(loc_string))
        {
            if (!misDynStrcat(&loc_string, ","))
            {
                sqlFreeResults(res);
                if (loc_string) free (loc_string);
                if (sqlbuf) free (sqlbuf);        
                if (buffer) free (buffer);        
                return (eNO_MEMORY);       
            }
        }
        memset(tmpstr, 0, sizeof(tmpstr));
        sprintf(tmpstr, "'%s'", sqlGetString(res, row, "stoloc"));
        if (!misDynStrcat(&loc_string, tmpstr))
        {
            sqlFreeResults(res);
            if (loc_string) free (loc_string);
            if (sqlbuf) free (sqlbuf);        
            if (buffer) free (buffer);        
            return (eNO_MEMORY);       
        }
    }

    sqlFreeResults(res);

    if (strlen(loc_string) == 0)
    {
        if (loc_string) free (loc_string);
        if (sqlbuf) free (sqlbuf);
        if (buffer) free (buffer);
        return (eOK);
    }

    /* 
     * The following is similar to the GetInventoryLevels function
     * we have to build sql clause by which is configed.
     * more explaination, see GetInventoryLevels function.
     */
    
    /* there need to initial variable sqlbuf */
    if (!misDynStrcat(&sqlbuf, ""))
    {
        if (loc_string) free (loc_string);
        if (sqlbuf) free (sqlbuf);        
        if (buffer) free (buffer);        
        return (eNO_MEMORY);       
    }
        
    /* Assign space first */

    if (area->conf_none)
    {
        if (!misDynStrcat(&sqlbuf, " (im.stoloc in (") || 
            !misDynStrcat(&sqlbuf, loc_string) ||
            !misDynStrcat(&sqlbuf, ") and im.wh_id = '") ||
            !misDynStrcat(&sqlbuf, wh_id) ||
            !misDynStrcat(&sqlbuf, "') "))
        {
            if (loc_string) free (loc_string);
            if (sqlbuf) free (sqlbuf);        
            if (buffer) free (buffer);        
            return (eNO_MEMORY);       
        }
    }
    
    if (area->conf_prt)
    {
        if (strlen(sqlbuf) == 0)
        {
            if (!misDynStrcat(&sqlbuf, " (im.stoloc in (") ||
                !misDynStrcat(&sqlbuf, loc_string) ||
                !misDynStrcat(&sqlbuf, ") and im.wh_id = '") ||
                !misDynStrcat(&sqlbuf, wh_id) ||
                !misDynStrcat(&sqlbuf, "' and im.prtnum = '") ||
                !misDynStrcat(&sqlbuf, prd->prtnum) ||
                !misDynStrcat(&sqlbuf, "' and im.prt_client_id = '") ||
                !misDynStrcat(&sqlbuf, prd->prt_client_id) ||
                !misDynStrcat(&sqlbuf, "') "))
            {
                if (loc_string) free (loc_string);
                if (sqlbuf) free (sqlbuf);        
                if (buffer) free (buffer);        
                return (eNO_MEMORY);       
            }
        }
        else
        {
            if (!misDynStrcat(&sqlbuf, " or (im.stoloc in (") ||
                !misDynStrcat(&sqlbuf, loc_string) ||
                !misDynStrcat(&sqlbuf, ") and im.wh_id = '") ||
                !misDynStrcat(&sqlbuf, wh_id) ||
                !misDynStrcat(&sqlbuf, "' and im.prtnum = '") ||
                !misDynStrcat(&sqlbuf, prd->prtnum) ||
                !misDynStrcat(&sqlbuf, "' and im.prt_client_id = '") ||
                !misDynStrcat(&sqlbuf, prd->prt_client_id) ||
                !misDynStrcat(&sqlbuf, "') "))
            {
                if (loc_string) free (loc_string);
                if (sqlbuf) free (sqlbuf);        
                if (buffer) free (buffer);        
                return (eNO_MEMORY);       
            }
        }
    }
    
    if (area->conf_prtfam)
    {
        if (strlen(sqlbuf) == 0)
        {
            if (!misDynStrcat(&sqlbuf, " (im.stoloc in (") ||
                !misDynStrcat(&sqlbuf, loc_string) ||
                !misDynStrcat(&sqlbuf, ") and im.wh_id = '") ||
                !misDynStrcat(&sqlbuf, wh_id) ||
                !misDynStrcat(&sqlbuf, "' and pm.prtfam = '") ||
                !misDynStrcat(&sqlbuf, prd->prtfam) ||
                !misDynStrcat(&sqlbuf, "') "))
            {
                if (loc_string) free (loc_string);
                if (sqlbuf) free (sqlbuf);        
                if (buffer) free (buffer);        
                return (eNO_MEMORY);       
            }  
        }
        else
        {
            if (!misDynStrcat(&sqlbuf, " or (im.stoloc in (") ||
                !misDynStrcat(&sqlbuf, loc_string) ||
                !misDynStrcat(&sqlbuf, ") and im.wh_id = '") ||
                !misDynStrcat(&sqlbuf, wh_id) ||
                !misDynStrcat(&sqlbuf, "' and pm.prtfam = '") ||
                !misDynStrcat(&sqlbuf, prd->prtfam) ||
                !misDynStrcat(&sqlbuf, "') "))
            {
                if (loc_string) free (loc_string);
                if (sqlbuf) free (sqlbuf);        
                if (buffer) free (buffer);        
                return (eNO_MEMORY);       
            } 
        }
             
        sprintf(sqlbuftbl,
               " %s ",
               " right outer join prtmst_view pm "
               " on pm.prtnum = im.prtnum  "
               " and pm.wh_id = im.wh_id  "
               " and pm.prt_client_id = im.prt_client_id ");
    }

    if (area->conf_supnum)
    {
        /* Ignore ret_status, see the function for details. */
        ret_status = GetInventoryLevelsBySupplier(area, prd, &lodqty);
    }
    
    if (area->conf_client)
    {
        if (strlen(sqlbuf) == 0)
        {
            if (!misDynStrcat(&sqlbuf, " (im.stoloc in (") ||
                !misDynStrcat(&sqlbuf, loc_string) ||
                !misDynStrcat(&sqlbuf, ") and im.wh_id = '") ||
                !misDynStrcat(&sqlbuf, wh_id) ||
                !misDynStrcat(&sqlbuf, "' and im.prt_client_id = '") ||
                !misDynStrcat(&sqlbuf, prd->prt_client_id) ||
                !misDynStrcat(&sqlbuf, "') "))
            {
                if (loc_string) free (loc_string);
                if (sqlbuf) free (sqlbuf);        
                if (buffer) free (buffer);        
                return (eNO_MEMORY);       
            }
        }
        else
        {
            if (!misDynStrcat(&sqlbuf, " or (im.stoloc in (")||
                !misDynStrcat(&sqlbuf, loc_string) ||
                !misDynStrcat(&sqlbuf, ") and im.wh_id = '") ||
                !misDynStrcat(&sqlbuf, wh_id) ||
                !misDynStrcat(&sqlbuf, "' and im.prt_client_id = '") ||
                !misDynStrcat(&sqlbuf, prd->prt_client_id) ||
                !misDynStrcat(&sqlbuf, "' "))
            {
                if (loc_string) free (loc_string);
                if (sqlbuf) free (sqlbuf);        
                if (buffer) free (buffer);        
                return (eNO_MEMORY);       
            }
        }
    }

    /* If only ap->conf_supnum is set, sqlbuf would be empty and the following
     * query should be skipped. So check sqlbuf before continuing.*/
    if (strlen(sqlbuf) == 0)
    {
        if (loc_string) free (loc_string);
        if (sqlbuf) free (sqlbuf);        
        if (buffer) free (buffer);        
        return lodqty;
    }

    if (!misDynStrcat(&buffer, "select im.untqty, "
                                     " im.pndqty, "
                                     " im.prtnum, "
                                     " im.prt_client_id "
                                " from invsum im ") ||
        !misDynStrcat(&buffer, sqlbuftbl))
    {
        if (loc_string) free (loc_string);
        if (sqlbuf) free (sqlbuf);        
        if (buffer) free (buffer);        
        return (eNO_MEMORY);       
    }
    else
    {
        if(strlen(sqlbuf))
        {
            if( !misDynStrcat(&buffer, " where ") ||
                !misDynStrcat(&buffer, sqlbuf))
            {
                if (loc_string) free (loc_string);
                if (sqlbuf) free (sqlbuf);        
                if (buffer) free (buffer);        
                return (eNO_MEMORY); 
            }
        }
    }
    
    ret_status = sqlExecStr(buffer, &res);

    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        if (loc_string) free (loc_string);        
        if (sqlbuf) free (sqlbuf);
        if (buffer) free (buffer);
        return (eOK);
    }
    if (loc_string) free (loc_string);
    if (sqlbuf) free (sqlbuf);
    if (buffer) free (buffer);

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        if (!sqlIsNull(res, row, "untqty"))
        {
            /* we need to know how many loads in the aisles
             * so convert the unit quality to loads by different
             * part
             */
            untqty = sqlGetLong(res, row, "untqty");
            pndqty = sqlGetLong(res, row, "pndqty");
            lodqty = lodqty + ConvertToLoads(
                                sqlGetString(res, row, "prtnum"),
                                sqlGetString(res, row, "prt_client_id"),
                                untqty+pndqty);
        }
    }

    return lodqty;

}

static long AddToAisleList(AREA_LIST *area,
                           PRODUCT *prd,
                           char *aisle_id,
                           char *wh_id)
{
    AISLE_LIST    *listptr;
    AISLE_LIST    *aislelisttail;

    if ((listptr = (AISLE_LIST *) calloc(sizeof(AISLE_LIST), 1)) == NULL)
    {
        return (eNO_MEMORY);
    }

    strcpy(listptr->aisle_id, aisle_id);
    strcpy(listptr->wh_id, wh_id);
    listptr->qvl = GetAisleQvl(area,prd,aisle_id, wh_id);
    listptr->next = NULL;

    if (aisle_list == NULL)
    {
        aisle_list = listptr;
    }
    else
    {
        aislelisttail = aisle_list;
        while (aislelisttail->next != NULL)
            aislelisttail = aislelisttail->next;
        aislelisttail->next = (AISLE_LIST *) listptr;
    }

    sLog(0, "Aisle - %s/%s ", wh_id, aisle_id);

    return (eOK);
}

static long GetAisleInventoryLevel(AREA_LIST * area,
                                   PRODUCT * prd)
{
    long            ret_status;
    char            buffer[2000];
    mocaDataRow        *row = NULL;
    mocaDataRes        *res = NULL;

    FreeAisleList();
    memset(buffer, 0, sizeof(buffer));

    sprintf(buffer,
            " select distinct aisle_id, wh_id "
            "   from locmst lm "
            "  where lm.arecod = '%s' "
            "    and lm.wh_id = '%s' "
            "    and lm.aisle_id is not null ",
            area->arecod, area->wh_id);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
        return (ret_status);
    }
    else if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        /*
         * There is no aisle for stolocs in the area.
         */
        misTrc(T_FLOW, "No Aisle for %s/%s", area->wh_id, area->arecod);
    }
    else
    {
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            AddToAisleList(area,
                           prd,
                           sqlGetString(res,row,"aisle_id"),
                           sqlGetString(res,row,"wh_id"));
        }
    }

    sqlFreeResults(res);    

    return (eOK);
}

static long ProcessArea(AREA_LIST * area,
			PRODUCT * prd,
			STOLOC_LIST ** LocList,
			long process_pass, 
                        long isPipEnabled,
                        double lodhgt)
{
    long            ret_status;
    long            strategy_status;
    long            first_pass = 0;
    int             errcode;

    if (area->pass_count == 0)
	first_pass = 1;
    
    FreeAisleList();

    if (area->max_loads_aisle > 0)
    {
        errcode = GetAisleInventoryLevel(area, prd);
    }
    
    if (!first_pass && area->pass_count >= process_pass)
	return (eOK);

    if (area->pass_count < PASS_PARTIAL_LOC_IN_SAME_VELZON)
    {
	/* This is the partial area selection.. */

	area->pass_count = PASS_PARTIAL_LOC_IN_SAME_VELZON;

	strategy_status = CheckPassStrategy(area->strategy,
					    area->pass_count);
	if (strategy_status == -2)
	    return (eOK);
	if (strategy_status >= 0)
	{
	    sLog(0, "PASS: Find partials, loc.velzon >= prd.velzon");

        ret_status = FindPartialLocation(area, prd, LocList, isPipEnabled, lodhgt);

	    if (ret_status == eOK)
		return (eOK);
            else
            {
                misTrc(T_FLOW, "(Partial Same Zone) Error finding location, "
                               "clearing the LocList");
                *LocList = NULL;
            }
	}
 
	if (strategy_status == 0)
	    return (eOK);
    }

    if (area->pass_count < PASS_PARTIAL_LOC_IN_DIFF_VELZON)
    {
	/* This is the partial area selection.. */

	area->pass_count = PASS_PARTIAL_LOC_IN_DIFF_VELZON;

	strategy_status = CheckPassStrategy(area->strategy,
					    area->pass_count);
	if (strategy_status == -2)
	    return (eOK);
	if (strategy_status >= 0)
	{
	    sLog(0, "PASS: Find partials, loc.velzon < prd.velzon");

        ret_status = FindPartialLocation(area, prd, LocList, isPipEnabled, lodhgt);
	    if (ret_status == eOK)
		return (eOK);
            else
            {
                misTrc(T_FLOW, "(Partial Diff Zone) Error finding location, "
                               "clearing the LocList");
                *LocList = NULL;
            }
	}
	if (strategy_status == 0)
	    return (eOK);
    }
    if (area->pass_count < PASS_EMPTY_LOC_IN_SAME_VELZON)
    {
	/* This pass is looking for empties... */
	area->pass_count = PASS_EMPTY_LOC_IN_SAME_VELZON;

	strategy_status = CheckPassStrategy(area->strategy,
					    area->pass_count);
	if (strategy_status == -2)
	    return (eOK);

	if (strategy_status >= 0)
	{
	    sLog(0, "*** PASS:  Find empties, loc.velzon >= prd.velzon ");

        ret_status = FindEmptyLocation(area, prd, LocList, isPipEnabled, lodhgt);
	    if (ret_status == eOK)
		return (eOK);
            else
            {
                misTrc(T_FLOW, "(Empty Same Zone) Error finding the location, "
                               "clearing the LocList");
                *LocList = NULL;
            }
	}
	if (strategy_status == 0)
	    return (eOK);
    }
    if (area->pass_count < PASS_EMPTY_LOC_IN_DIFF_VELZON)
    {
	/* This pass is looking for empties... */
	area->pass_count = PASS_EMPTY_LOC_IN_DIFF_VELZON;

	strategy_status = CheckPassStrategy(area->strategy,
					    area->pass_count);
	if (strategy_status == -2)
	    return (eOK);

	if (strategy_status >= 0)
	{
	    sLog(0, "*** PASS:  Find empties, loc.velzon < prd.velzon");

        ret_status = FindEmptyLocation(area, prd, LocList, isPipEnabled, lodhgt);
	    if (ret_status == eOK)
		return (eOK);
            else
            {
                misTrc(T_FLOW, "(Empty Diff Zone) Error finding the location, "
                               "clearing the LocList");
                *LocList = NULL;
            }
	}
	if (strategy_status == 0)
	    return (eOK);
    }

    if (area->pass_count < PASS_PARTIAL_LOC_IN_DIFF_VELZON_2ND)
    {
	/* This is the partial area selection.. */

	area->pass_count = PASS_PARTIAL_LOC_IN_DIFF_VELZON_2ND;

	strategy_status = CheckPassStrategy(area->strategy,
					    area->pass_count);
	if (strategy_status == -2)
	    return (eOK);
	if (strategy_status >= 0)
	{
	    sLog(0, "PASS(2): Find partials, loc.velzon < prd.velzon");

        ret_status = FindPartialLocation(area, prd, LocList, isPipEnabled, lodhgt);
	    if (ret_status == eOK)
		return (eOK);
            else
            {
                misTrc(T_FLOW, "(Partial Diff 2nd Zone) Error finding the "
                               "location, clearing the LocList");
                *LocList = NULL;
            }
	}
	if (strategy_status == 0)
	    return (eOK);
    }
    if (area->pass_count < PASS_EMPTY_LOC_IN_DIFF_VELZON_2ND)
    {
	/* This pass is looking for empties... */
	area->pass_count = PASS_EMPTY_LOC_IN_DIFF_VELZON_2ND;

	strategy_status = CheckPassStrategy(area->strategy,
					    area->pass_count);
	if (strategy_status == -2)
	    return (eOK);

	if (strategy_status >= 0)
	{
	    sLog(0, "*** PASS(2):  Find empties, loc.velzon < prd.velzon");

        ret_status = FindEmptyLocation(area, prd, LocList, isPipEnabled, lodhgt);
	    if (ret_status == eOK)
		return (eOK);
            else
            {
                misTrc(T_FLOW, "(Empty Diff 2nd Zone) Error finding the "
                               "location, clearing the LocList");
                *LocList = NULL;
            }
	}
	if (strategy_status == 0)
	    return (eOK);
    }
    if (area->pass_count < PASS_MIX_PRODUCT)
    {

	area->pass_count = PASS_MIX_PRODUCT;
	/* This pass is looking to mix */

	strategy_status = CheckPassStrategy(area->strategy,
					    area->pass_count);
	if (strategy_status < 0)
	    return (eOK);

	sLog(0, "*** PASS:  Mix product");

    ret_status = FindMixedLocation(area, prd, LocList, isPipEnabled, lodhgt);

        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "(Mixed) Error finding the location "
                           "clearing the LocList");
            *LocList = NULL;        
        }
	return (eOK);
    }
    return (eOK);

}

static long GetLocationRange(PRODUCT * prd,
			     char *area,
			     char *loc_lower,
			     char *loc_upper,
			     STOLOC_LIST ** LocList,
                 char *wh_id,
                 long isPipEnabled,
                 double lodhgt)
{
    static AREA_LIST tmparea;
    long            ret_status;
    char            buffer[3300];
    char            maxqvlString[200];
    char            maxqvlString1[200];
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL;
    char            FlagString[100];
    char            wrkzon_string[100];
    char           *notin_string = NULL;
    char            locrng_string[600];
    char           *zone_exclusion = NULL;
    static char     save_area[ARECOD_LEN + 1];
    static char     save_wh_id[WH_ID_LEN + 1];
    static char     save_lvl[LODLVL_LEN + 1];

    sLog(0, "Checking location range from %s to %s",
	    loc_lower, loc_upper);

    if (strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0 ||
        strcmp(prd->stotyp, ALLOCATE_TOPOFF_REPLEN) == 0)
    {
        
	sprintf(FlagString, " and l.pckflg = '%d' and l.repflg = '%d' ",
		BOOLEAN_TRUE, BOOLEAN_TRUE);
    }
    else
    {
	sprintf(FlagString, " and l.stoflg = '%d' ", BOOLEAN_TRUE);
    }

    if ((strncmp(area, save_area, ARECOD_LEN) != 0) ||
        (strncmp(wh_id, save_wh_id, WH_ID_LEN) != 0) ||
	(strncmp(prd->lodlvl, save_lvl, LODLVL_LEN) != 0))
    {

	sprintf(buffer,
		"select * from aremst "
                " where arecod = '%s'"
                "   and wh_id = '%s'"
                "   and praflg = '%d'",
		area, wh_id, BOOLEAN_FALSE);
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}
	memset(save_area, 0, sizeof(save_area));
	memset(save_wh_id, 0, sizeof(save_wh_id));
	memset(save_lvl, 0, sizeof(save_lvl));

	strncpy(save_area, area, ARECOD_LEN);
	strncpy(save_wh_id, wh_id, WH_ID_LEN);
	strncpy(save_lvl, prd->lodlvl, LODLVL_LEN);

	memset(&tmparea, 0, sizeof(tmparea));
	row = sqlGetRow(res);
	SetupArea(&tmparea, res, row);
	sqlFreeResults(res);
        res = NULL;
    }

    ret_status = CheckMovePath(&tmparea, prd, &zone_exclusion, isPipEnabled);
    if (ret_status != eOK)
    {
        if (zone_exclusion) free (zone_exclusion);
	return (ret_status);
    }

    if (zone_exclusion)
    {
	if (!misDynStrcpy(&notin_string, " and l.wrkzon not in ( "))
        {
            if (zone_exclusion) free (zone_exclusion);
            if (notin_string) free (notin_string);
	    return (eNO_MEMORY);
        }
	if (!misDynStrcat(&notin_string, zone_exclusion))
        {
            if (zone_exclusion) free (zone_exclusion);
            if (notin_string) free (notin_string);
	    return (eNO_MEMORY);
        }
	if (!misDynStrcat(&notin_string, ")"))
        {
            if (zone_exclusion) free (zone_exclusion);
            if (notin_string) free (notin_string);
	    return (eNO_MEMORY);
        }
    }

    if (zone_exclusion) free (zone_exclusion);

    memset(wrkzon_string, 0, sizeof(wrkzon_string));
    if (strcmp(prd->reqzon, "") != 0)
	sprintf(wrkzon_string, " and l.wrkzon = '%s' ", prd->reqzon);

    memset(locrng_string, 0, sizeof(locrng_string));
    if (!strncmp(loc_lower, loc_upper, STOLOC_LEN))
	sprintf(locrng_string, " and l.stoloc = '%s' ", loc_lower);
    else
	sprintf(locrng_string, " and l.stoloc >= '%s' and l.stoloc <= '%s' ",
		loc_lower, loc_upper);

    if (strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0)
    {
	sprintf(maxqvlString,
		" * decode(l.erfpct,null,100,0,100,l.erfpct) * 1.0/100 ");
    }
    else if (strcmp(prd->stotyp, ALLOCATE_TOPOFF_REPLEN) == 0)
    {
	sprintf(maxqvlString,
		" * decode(l.trfpct,null,100,0,100,l.trfpct) * 1.0/100 ");
    }
    else
    {
	sprintf(maxqvlString, " * 1 ");
    }

    if (isPipEnabled == 1 && strcmp(prd->stotyp, ALLOCATE_REPLENISH) == 0)
    {
        misTrc(T_FLOW, "PIP is enabled, ignoring QVL in trnAllocLoc");
        sprintf(maxqvlString1,"and 1 = 1");
    }
    else
    {
        
        sprintf(maxqvlString1,"and l.maxqvl %s > l.pndqvl + l.curqvl",maxqvlString);
    }

    /* I have changed '%d' to %d in decode statement because DB2 donot 
     * support implicit type conversion.
     */
    sprintf(buffer,
	    "select l.stoloc, l.wh_id, l.maxqvl %s maxqvl, l.curqvl, l.pndqvl, "
	    "       l.locsts,  nvl(ivs.untpal, 0) untpal, "
	    "       nvl(ivs.untqty, 0) untqty, "
	    "       l.velzon, l.wrkzon, l.lochgt,  "
	    "       ivs.olddte oldfif, "
	    "       ivs.newdte newfif "
	    "  from zonmst z join locmst l "
	    "    on (l.wh_id  = z.wh_id) "
	    "   and (l.wrkzon = z.wrkzon) "
	    "  left outer join invsum ivs "
	    "    on (l.arecod = ivs.arecod) "
	    "   and (l.stoloc = ivs.stoloc) "
	    "   and (l.wh_id = ivs.wh_id) "
	    " where l.arecod = '%s' "
	    "   and l.wh_id  = '%s' "
	    "  %s "		/* locrng string */
	    "  %s "		/* wrkzon string */
	    "  %s "		/* notin string */
	    "  %s "		/* stoflg/repflg = 'Y' */ 
	    "   and l.useflg = '%d' "  /* true */
	    "   and l.locsts != '%s' "
	    "   and l.cipflg = '%d' "  /* false */
	    "  %s "     /* maxqvlString1 */
	    "   and z.oosflg = '%d' "  /* false */
	    " order by l.velzon, l.trvseq, "
	    "          decode(l.asgflg, %d, 1, %d, 2, 3) ",
	    maxqvlString, /*Intentionally left as maxqvlString*/
	    tmparea.arecod,
	    tmparea.wh_id,
	    locrng_string,
	    wrkzon_string,
	    notin_string ? notin_string : "",
	    FlagString, 
	    BOOLEAN_TRUE,
	    LOCSTS_INV_ERROR, 
	    BOOLEAN_FALSE,
	    maxqvlString1,
	    BOOLEAN_FALSE,
	    BOOLEAN_TRUE, BOOLEAN_FALSE);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        if (notin_string) free (notin_string);
	sqlFreeResults(res);
	return (ret_status);
    }

    if (notin_string) free (notin_string);

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        
    if (isPipEnabled == 0 || strcmp(prd->stotyp, ALLOCATE_REPLENISH) != 0)
    {
	ret_status = RoomToStore(&tmparea,
				 prd,
				 res,
				 row,
                                 lodhgt);
    }
    else
    {
        /*if PIP is enabled, we can ignore what's the location qvl, move everything there */
         sLog(0, "GetLocationRange was forced to return eOK by PIP");
        ret_status = eOK;
    }
    
	if (ret_status != eOK)
	{
	    sLog(0, "Location: %s/%s - NO ROOM TO STORE",
	            sqlGetString(res, row, "wh_id"),
		    sqlGetValue(res, row, "stoloc"));
	    continue;
	}
	ret_status = PartOkInLoc(&tmparea,
				 prd,
				 res,
				 row);
	if (ret_status != eOK)
	{
	    sLog(0, "Location: %s/%s - PART CONFLICT!",
	            sqlGetString(res, row, "wh_id"),
		    sqlGetValue(res, row, "stoloc"));

	    continue;
	}

	/* VAR - Check for any QT Holds in the loc.  We can only mix
	 * inventory with the same QT Hold status and the same
	 * manufacture date
	 */
	ret_status = QTHoldMixOkInLoc(prd,
				      res,
				      row);
	if (ret_status != eOK)
	{
	    sLog(0, "Location: %s/%s - QT Hold mix CONFLICT!",
	            sqlGetString(res, row, "wh_id"),
		    sqlGetValue(res, row, "stoloc"));

	    continue;
	}

	ret_status = sIsLocationAssignedToOther(prd, 
                                            sqlGetString(res, row, "stoloc"),
                                            sqlGetString(res, row, "wh_id"));
        if (ret_status != eOK)
	{
	    sLog(0, 
		 "Location %s/%s is assigned to another part"
		 " - unable to use!",
		 sqlGetString(res, row, "wh_id"), 
		 sqlGetString(res, row, "stoloc"));
	    continue;
	}

	/* At this point...it is OK just to grab the location... */
	ret_status = GetLocation(&tmparea,
				 prd,
				 res,
				 row,
				 LocList,
                 isPipEnabled);
	if (ret_status == eDB_DEADLOCK)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}
	else if (ret_status == eOK)
	{
	    sqlFreeResults(res);
	    return (eOK);
	}
    }
    sqlFreeResults(res);
    res= NULL;

    /* If we are here then we could not find a */
    /* Location to use.  We will return the status */
    /* This will return the status of the last location */
    /* that was looked at in the range.  This will be OK */
    /* because there are only two places in the code that */
    /* call this function.  If it is called with a range */
    /* of locations then the calling function will handle */ 
    /* the error.  If it is called with a single location */
    /* (both loc_upper and loc_lower contain the same single */
    /* location) then we should return the error for that loc */

    return (ret_status);
}

static long sCheckFIFOWin(char *newdte,
                          char *olddte,
                          PRODUCT * prd)
{
    char            buffer[1000];
    long            ret_status;
    double          fif_win;

    RETURN_STRUCT  *CmdPtr = NULL;
    mocaDataRes    *cmpres = NULL;
    mocaDataRow    *cmprow = NULL;

    /* Now we have to determine if the date falls within the range */
    sprintf(buffer,
            "calculate fifo dates for storage "
            "    where olddte = '%s' "
            "      and newdte = '%s' "
            "      and fifdte = '%s' "
            "      and timcod = '%s' ",
            olddte,
            newdte,
            prd->fifdte,
            prd->timcod);

    ret_status = srvInitiateCommand(buffer, &CmdPtr);
    if (eOK != ret_status)
    {
        srvFreeMemory(SRVRET_STRUCT, CmdPtr);
        return(ret_status);
    }
    cmpres = srvGetResults(CmdPtr);
    cmprow = sqlGetRow(cmpres);

    fif_win = sqlGetFloat(cmpres, cmprow, "fif_win");

    srvFreeMemory(SRVRET_STRUCT, CmdPtr);
    CmdPtr = NULL;

    if (fif_win > (double) prd->fifwin)
        return (eINT_FIFO_WINDOW_EXCEEDED);
    else
        return (eOK);
}


/*
 * PR 56510 & 60278
 * 
 * Generally, Assigned locations have higer precedence than
 * Non-Assigned locations when system allocates a location.
 * 
 * Only in one case, system will search Non-Assigned locations first, 
 * then Assigned locations
 *   1) product is a FIFO part
 *   2) policy ALLOCATE-LOC/RESPECT-FIFO-SKIP-ASSIGN-LOCS is enabled,
 *   3) Older inventories exist in Non-Assigned locations 
 * 
 * The reason is because some customers have perishable items and 
 * they need to pick old items before new items.
 */
 
static long CheckLocationSearchSequence(PRODUCT * prd,
                              char *wh_id,
                              moca_bool_t fifo_skip_asg_loc_flg,
                              AREA_LIST *area_list,
                              moca_bool_t *search_assigned_location_first)
{
    long            ret_status;
    char            buffer[8000];
    char            *sqlbuffer = NULL;
    char            *non_assigned_loc_where_clause = NULL;
    mocaDataRes     *res;
    mocaDataRow     *row;
    moca_bool_t     std_put_flg;
    AREA_LIST       *area = NULL;

    memset(buffer, 0, sizeof(buffer));

    *search_assigned_location_first = BOOLEAN_TRUE;
    
    if (!(prd->fifwin > 0))
    {
        misTrc(T_FLOW,
               "Product is not a FIFO part, "
               "Assinged locations will be searched prior to "
               "Non-Assigned locations.");

        *search_assigned_location_first = BOOLEAN_TRUE;
        return (eOK);
    }

    if (fifo_skip_asg_loc_flg == BOOLEAN_FALSE)
    {
        misTrc(T_FLOW,
               "Policy ALLOCATE-LOC/RESPECT-FIFO-SKIP-LOCS is not enabled, "
               "Assinged locations will be searched prior to "
               "Non-Assigned locations.");

        *search_assigned_location_first = BOOLEAN_TRUE;
        return (eOK);
    }
    else 
    {
        std_put_flg = 0;
        misTrc(T_FLOW,
               "Policy ALLOCATE-LOC/RESPECT-FIFO-SKIP-LOCS is enabled, "
               "Check the std_put_flg for the invoice type to "
               "determine whether we need to search for assigned location first");
        sprintf(buffer,
                "select max(invtypmst.std_put_flg) std_put_flg "
                "  from invdtl, "
                "       invlod, "
                "       invsub, "
                "       rcvlin, "
                "       rcvinv, "
                "       invtypmst "
                " where invlod.lodnum = invsub.lodnum "
                "   and invsub.subnum = invdtl.subnum "
                "   and rcvlin.rcvkey = invdtl.rcvkey "
                "   and rcvlin.invnum = rcvinv.invnum "
                "   and invtypmst.invtyp = rcvinv.invtyp "
                "   and invlod.wh_id = '%s' ",
                wh_id);
        if (strcmp(prd->lodlvl, LODLVL_LOAD) == 0)
        {
            sprintf(buffer,
                    "%s and invlod.lodnum = '%s' ",
                    buffer, prd->lodnum);
        }
        else if (strcmp(prd->lodlvl, LODLVL_SUBLOAD) == 0)
        {
            sprintf(buffer,
                    "%s and invsub.subnum = '%s' ",
                    buffer, prd->lodnum);
        }
        else if (strcmp(prd->lodlvl, LODLVL_DETAIL) == 0)
        {
            sprintf(buffer,
                    "%s and invdtl.dtlnum = '%s' ",
                    buffer, prd->lodnum);
        }

        ret_status = sqlExecStr(buffer, &res);

        if (ret_status != eOK &&
             ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(res);
            return (ret_status);
        }
        else if (ret_status == eOK)
        {
            row = sqlGetRow(res);
            std_put_flg = sqlGetLong(res, row, "std_put_flg");
        }

        sqlFreeResults(res);

        if (std_put_flg == 1)
        {
            misTrc(T_FLOW,
                   "Since the std_put_flg is on for " 
                   "this invoice type, get the assigned location first");
            *search_assigned_location_first = BOOLEAN_TRUE;
            return (eOK);
        }
    }

    if (!area_list) 
    {
        misTrc(T_FLOW,
               "No Non-Assigned location was defined, "
               "Assinged locations will be searched prior to "
               "Non-Assigned locations.");

        *search_assigned_location_first = BOOLEAN_TRUE;
        return (eOK);
    }

    /* build non-assigned areas(locations) where clause */
    
    for (area = area_list; area; area = area->next)
    {
        /* 
         * makeup the where clauses for Non-Assigned Locations 
         */

        if (non_assigned_loc_where_clause)
            sprintf(buffer, ", '%s'", area->arecod);
        else
            sprintf(buffer, " and arecod in ('%s'", area->arecod);

        if (!misDynStrcat (&non_assigned_loc_where_clause, buffer))
        {
            if (non_assigned_loc_where_clause) 
                free (non_assigned_loc_where_clause);
            return (eNO_MEMORY);
        }
    }

    if (!misDynStrcat(&non_assigned_loc_where_clause, ")" ))
    {
        if (non_assigned_loc_where_clause) 
            free (non_assigned_loc_where_clause);
        return (eNO_MEMORY);
    }

    /* search Non-Assgined locations, check to see if there is older inventory
     * in the system.
     */
    
    sprintf(buffer,
            "[select 'X' "
            "   from invsum "
            "  where prtnum = '%s' "
            "    and prt_client_id = '%s' "
            "    and invsts = '%s' "
            /* we consider the location has the inventory if the inventory is
             * heading for it (pndqty>0) */
            "    and ( (pndqty > 0) or (untqty > 0) ) "
            /* the older inventory */
            "    and olddte < to_date('%s', 'YYYYMMDDHH24MISS') "
            "    and wh_id = '%s' ",
            prd->prtnum,
            prd->prt_client_id,
            prd->invsts,
            prd->fifdte,
            wh_id);

    if (!misDynStrcat(&sqlbuffer, buffer) ||
        !misDynStrcat(&sqlbuffer, non_assigned_loc_where_clause) ||
        !misDynStrcat(&sqlbuffer, " ]"))
    {
        if (non_assigned_loc_where_clause) 
            free (non_assigned_loc_where_clause);
        if (sqlbuffer)
            free (sqlbuffer);
        return (eNO_MEMORY);
    }

    ret_status = srvInitiateCommand(sqlbuffer, NULL);
    free (non_assigned_loc_where_clause);
    free (sqlbuffer);
    sqlbuffer = NULL;

    if (ret_status == eOK)
    {
        misTrc(T_FLOW, 
               "Older Inventory was found in Non-Assigned locations, "
               "Non-Assinged locations will be searched prior to "
               "Assigned locations.");

        *search_assigned_location_first = BOOLEAN_FALSE;
        return (eOK);
    }
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        misTrc(T_FLOW,
               "No older inventory exists in Non-Assigned locations, "
               "Assinged locations will be searched prior to "
               "Non-Assigned locations.");

        *search_assigned_location_first = BOOLEAN_TRUE;
        return (eOK);
    }
    else
    {
        return (ret_status);
    }
}

static long ProcessDistroPutToLocation(PRODUCT * prd,
                                       STOLOC_LIST ** LocList,
                                       char *arecod,
                                       char *stoloc,
                                       char *wh_id,
                                       long isPipEnabled,
                                       double lodhgt)
{
    long ret_status;

    /* When trying a specific location, we will more */
    /* likely have someone grab it from underneath us */
    /* ... so, loop while we get DB_DEADLOCK back from */
    /* GetLocation */
    ret_status = eDB_DEADLOCK;
    while (ret_status == eDB_DEADLOCK)
    {
        ret_status = GetLocationRange(prd,
                                      arecod,
                                      stoloc,
                                      stoloc,
                                      LocList,
                                      wh_id,
                                      isPipEnabled,
                                      lodhgt);
    }

    return (ret_status);
}

static long FindLocationWithMatchAttributes(PRODUCT *prd,
                                            STOLOC_LIST **LocList,
                                            AREA_LIST *area,
                                            long isPipEnabled,
                                            double lodhgt,
                                            long srtseq,
                                            char *distro_lodlvl)
{
    char query[8000];
    char prefs[5000];
    char buffer[2000];
    char *colnam=NULL;
    char *colval=NULL;
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;
    long return_status = eOK;
    moca_bool_t lodflg = BOOLEAN_FALSE;
    moca_bool_t subflg = BOOLEAN_FALSE;
    moca_bool_t dtlflg = BOOLEAN_FALSE;
    int pass_cnt = 0;
    int strategy_cnt = 0;
    char strategy_bak[RTSTR2_LEN + 1];
    char *strategy[] = {
        STRATEGY_EMPTIES_ONLY,
        STRATEGY_PARTIALS_ONLY,
        NULL
    };

    /* For the current distro, we match distro.stcust with
       cst_wh_put_to_loc.cstnum and obtain preferences from
       cst_wh_put_to_loc_field. The preferences are of the following form
       which can be used as filter criteria on tables distro and wh_cstmst:
           distro.<some column>=<a value>
           wh_cstmst.<some column>=<a value>
       
       Distro ID is on invdtl; thus inventory attributes can be linked with the
       preferences.

       The preferences are used below to find locations where inventory
       attributes match these preferences and the inventory is either already
       present in, or scheduled to move to, the area passed in as an argument.

       We use an outer join on cst_wh_put_to_loc_field: if column-specific
       preferences are not available, all inventory for the customer would be
       matched since the three tables are joined on customer number.
       */
    memset(prefs, 0, sizeof(prefs));
    memset(buffer, 0, sizeof(buffer));
    memset(query, 0, sizeof(query));
    sprintf(query,
        " select loc.lodflg,"
        "        loc.subflg,"
        "        loc.dtlflg,"
        "     distro.stcust,"
        "        fld.colnam,"
        "        fld.colval"
        "   from cst_wh_put_to_loc loc"
        "  inner join distro"
        "         on loc.wh_id = distro.wh_id"
        "        and loc.client_id = distro.client_id"
        "        and loc.cstnum = distro.stcust"
        "   left outer join cst_wh_put_to_loc_field fld"
        "         on loc.wh_id = fld.wh_id"
        "        and loc.client_id = fld.client_id"
        "        and loc.cstnum = fld.cstnum"
        "        and loc.arecod = fld.arecod"
        "        and loc.srtseq = fld.srtseq"
        "  where loc.arecod='%s'"
        "    and loc.srtseq=%ld"
        "    and distro.wh_id='%s'"
        "    and distro.client_id='%s'"
        "    and distro.distro_id='%s'",
        area->arecod,
        srtseq,
        area->wh_id,
        prd->prt_client_id,
        prd->distro_id);

    /* Apply lodlvl filter based on the lodlvl of current inventory */
    if (misTrimStrncmp(distro_lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0)
        strcat(query, " and loc.lodflg = 1");
    else if (misTrimStrncmp(distro_lodlvl, LODLVL_SUBLOAD, LODLVL_LEN) == 0)
        strcat(query, " and loc.subflg = 1");
    else if (misTrimStrncmp(distro_lodlvl, LODLVL_DETAIL, LODLVL_LEN) == 0)
        strcat(query, " and loc.dtlflg = 1");

    res = NULL;
    return_status = sqlExecStr(query, &res);
    if (eOK != return_status)
    {
        if (res != NULL)
            sqlFreeResults(res);
        res = NULL;

        if (eDB_NO_ROWS_AFFECTED == return_status)
        {
            /* No preferences are attached to the area. Fall through to find an
               empty location. */
        }
        else
        {
            /* This is a query error, return the error code*/
            return return_status;
        }
    }
    else
    {
        /* Add preferences from cst_wh_put_to_loc_field retrieved above */
        for (row=sqlGetRow(res); row; row=sqlGetNextRow(row))
        {
            /* The above query uses full PK of distro table, so stcust will be
             * the same for all returned rows. Note that distro.stcust is a
             * not-null column. We will use this stcust to prefer locations
             * having existing/expected inventory for the same customer.
             */
            if (strlen(prefs) == 0)
                sprintf(prefs, " and distro.stcust='%s'",
                    sqlGetString(res, row, "stcust"));

            memset(buffer, 0, sizeof(buffer));
            colnam = sqlGetString(res, row, "colnam");
            colval = sqlGetString(res, row, "colval");
            if (misTrimLen(colnam, COLNAM_LEN) > 0)
            {
                if (misTrimLen(colval, COLVAL_LEN) > 0)
                    sprintf(buffer, " and %s='%s'", colnam, colval);
                else
                    sprintf(buffer, " and %s is NULL", colnam);
            }
            strcat(prefs, buffer);
        }

        /* We will now generate the query to locate the locations */
        /* Skip the current load when looking for locations */
        memset(buffer, 0, sizeof(buffer));
        if (!strcmp(prd->lodlvl, LODLVL_LOAD))
            sprintf(buffer, " and invlod.lodnum<>'%s'", prd->lodnum);
        else if (!strcmp(prd->lodlvl, LODLVL_SUBLOAD))
            sprintf(buffer, " and invsub.subnum<>'%s'", prd->lodnum);
        else if (!strcmp(prd->lodlvl, LODLVL_DETAIL))
            sprintf(buffer, " and invdtl.dtlnum<>'%s'", prd->lodnum);
        strcat(prefs, buffer);

        /* In the query below, there is an union with two select statements.
           The way some of the tables are joined is common to both the
           statements; so they are also included here. */
        strcat(prefs,
            " and invlod.wh_id=locmst.wh_id"
            " and invsub.lodnum=invlod.lodnum"
            " and invdtl.subnum=invsub.subnum"
            " and prtmst_view.wh_id=invlod.wh_id"
            " and prtmst_view.prt_client_id=invdtl.prt_client_id"
            " and prtmst_view.prtnum=invdtl.prtnum"
            " and distro.distro_id=invdtl.distro_id"
            " and distro.client_id=invdtl.prt_client_id"
            " and distro.wh_id=invlod.wh_id"
            " and wh_cstmst.wh_id=distro.wh_id"
            " and wh_cstmst.client_id=distro.client_id"
            " and wh_cstmst.cstnum=distro.stcust");

        /* Get the load level preferences for this area. If a flag is off,
           inventory of correponding lodlvl will be excluded when fetching
           inventory. We assume at least one flag is on since rows from
           cst_wh_put_to_loc would be selected only if the current distro's
           lodlvl matches its lodflg/subflg/dtlflg. The query above uses the
           cst_wh_put_to_loc PK; more than one row can result if
           cst_wh_put_to_loc_field has more than one row for the srtseq.
           However, these three flags will be identical for all rows.*/
        row = sqlGetRow(res);
        lodflg = sqlGetBoolean(res, row, "lodflg");
        subflg = sqlGetBoolean(res, row, "subflg");
        dtlflg = sqlGetBoolean(res, row, "dtlflg");
        row = NULL;

        /* Fetch existing inventory where load level corresponds to the flags
           set in the preferences for the area. The grouping facilitates load
           level computation. If all flags are on, we do not need to add any
           filter for lodlvl.
           */
        if (lodflg == BOOLEAN_FALSE || subflg == BOOLEAN_FALSE || dtlflg == BOOLEAN_FALSE)
        {
            strcat(prefs,
                " group by locmst.stoloc,"
                "          invlod.lodnum,"
                "          prtmst_view.untpal,"
                "          prtmst_view.untcas"
                );
            if (lodflg == BOOLEAN_FALSE && subflg == BOOLEAN_FALSE && dtlflg == BOOLEAN_TRUE)
                strcat(prefs,
                    " having sum(invdtl.untqty) < prtmst_view.untcas");
            else if (lodflg == BOOLEAN_FALSE && subflg == BOOLEAN_TRUE && dtlflg == BOOLEAN_FALSE)
                strcat(prefs,
                    " having sum(invdtl.untqty) >= prtmst_view.untcas"
                    "    and sum(invdtl.untqty) < prtmst_view.untpal");
            else if (lodflg == BOOLEAN_FALSE && subflg == BOOLEAN_TRUE && dtlflg == BOOLEAN_TRUE)
                strcat(prefs,
                    " having sum(invdtl.untqty) < prtmst_view.untpal");
            else if (lodflg == BOOLEAN_TRUE && subflg == BOOLEAN_FALSE && dtlflg == BOOLEAN_FALSE)
                strcat(prefs,
                    " having sum(invdtl.untqty) >= prtmst_view.untpal");
            else if (lodflg == BOOLEAN_TRUE && subflg == BOOLEAN_FALSE && dtlflg == BOOLEAN_TRUE)
                strcat(prefs,
                    " having sum(invdtl.untqty) < prtmst_view.untcas"
                    "     or sum(invdtl.untqty) >= prtmst_view.untpal");
            else if (lodflg == BOOLEAN_TRUE && subflg == BOOLEAN_TRUE && dtlflg == BOOLEAN_FALSE)
                strcat(prefs,
                    " having sum(invdtl.untqty) >= prtmst_view.untcas");
        }

        sqlFreeResults(res);
        res=NULL;
        row=NULL;

        /* Generate a query to retrieve locations in the area that match either
           of the following:
               1. have distro inventory with attributes that match the
                  preferences
               2. invmov.stoloc where the inventory is distro inventory with
                  attributes that match the preferences

           The two criteria are addressed by the first and second select
           statements, respectively, in the union below.
        */
        memset(query, 0, sizeof(buffer));
        sprintf(query,
            "select locmst.stoloc"
            "  from locmst,"
            "       invlod,"
            "       invsub,"
            "       invdtl,"
            "       prtmst_view,"
            "       distro,"
            "       wh_cstmst"
            " where locmst.arecod='%s'"
            "   and locmst.wh_id='%s'"
            "   and invlod.stoloc=locmst.stoloc"
            "   and invdtl.prt_client_id='%s'"
            "   %s"
            " "
            " union"
            " "
            "select locmst.stoloc"
            "  from locmst,"
            "       invmov,"
            "       invlod,"
            "       invsub,"
            "       invdtl,"
            "       prtmst_view,"
            "       distro,"
            "       wh_cstmst"
            " where locmst.arecod='%s'"
            "   and locmst.wh_id='%s'"
            "   and invmov.wh_id=locmst.wh_id"
            "   and invmov.stoloc=locmst.stoloc"
            "   and (   (invmov.lodlvl='%s' and invlod.lodnum=invmov.lodnum)"
            "        or (invmov.lodlvl='%s' and invsub.subnum=invmov.lodnum)"
            "        or (invmov.lodlvl='%s' and invdtl.dtlnum=invmov.lodnum)"
            "       )"
            "   and invdtl.prt_client_id='%s'"
            "   %s",
            area->arecod,
            area->wh_id,
            prd->prt_client_id,
            prefs,
            area->arecod,
            area->wh_id,
            LODLVL_LOAD,
            LODLVL_SUBLOAD,
            LODLVL_DETAIL,
            prd->prt_client_id,
            prefs);
        res=NULL;
        return_status = sqlExecStr(query, &res);
        if (eOK != return_status)
        {
            if (res != NULL)
                sqlFreeResults(res);

            if (eDB_NO_ROWS_AFFECTED == return_status)
            {
                /* No locations with attributes matching the preferences were
                   found in this area. Fall through to find an empty
                   location. */
            }
            else
            {
                /* This is a query error, return the error code*/
                return return_status;
            }
        }
        else
        {
            /* We found locations that match preferences. Return location
               information in the LocList argument. */
            for (row=sqlGetRow(res); row; row=sqlGetNextRow(row))
            {
                return_status = ProcessDistroPutToLocation(prd,
                    LocList,
                    area->arecod,
                    sqlGetString(res, row, "stoloc"),
                    area->wh_id,
                    isPipEnabled,
                    lodhgt);

                    if (return_status == eOK)
                        break;
            }
            sqlFreeResults(res);
            res=NULL;
            row=NULL;

            /* If a location with matching attributes was found, return */
            if (return_status == eOK)
                return return_status;

            /* If we were unable to find a suitable location, fall through to
               search for an empty location in this preferred area before
               proceding to the next set of preferences */
        }
    }

    /* Now continue search in the area for an empty location followed by a
     * partial location. The area search strtegy is temporarily changed here
     * for the duration of the search. Note that, if we are here, it means this
     * Put-to-Location rule for this area does not specify a stoloc. If a
     * location was specified, ProcessDistroPutToLocation would have run and
     * this search would not occur. */
    memset(strategy_bak, 0, sizeof(strategy_bak));
    strncpy(strategy_bak, area->strategy, RTSTR2_LEN);
    return_status = eOK;
    strategy_cnt = 0;

    while (strategy[strategy_cnt] != NULL)
    {
        strncpy(area->strategy, strategy[strategy_cnt], RTSTR2_LEN);
        area->pass_count = 0L;
        for (pass_cnt = 1; pass_cnt <= PASS_MIX_PRODUCT; ++pass_cnt)
        {
            return_status = ProcessArea(area, prd, LocList, pass_cnt,
                                        isPipEnabled, lodhgt);
            if (return_status != eOK)
            {
                misTrc(T_FLOW, "Errored in process area for distro");
                break;
            }

            if (*LocList && misTrimLen((*LocList)->nxtloc, STOLOC_LEN))
            {
               sLog(0, "LocList is not empty.");
               break;
            }
        }
        
        if (return_status != eOK)
            break;
        if (*LocList && misTrimLen((*LocList)->nxtloc, STOLOC_LEN))
            break;
        ++strategy_cnt;
    }
    strncpy(area->strategy, strategy_bak, RTSTR2_LEN);
    return return_status;
}

static long ProcessDistroPutToArea(PRODUCT *prd,
                                   STOLOC_LIST **LocList,
                                   AREA_LIST area,
                                   long isPipEnabled,
                                   double lodhgt)
{
    PASS_STOLOC_LIST *pass_stoloc;
    STOLOC_LIST      *ptr = NULL;
    long             errcode;
    long             pass;

    pass_stoloc_list = NULL;  

    /*
     * We do three passes through the area ... if the area is
     * "breadth" first, then first pass is partials, second pass
     * is empties, and third pass is mixing...
     */

    for (pass = 1; pass <= PASS_MIX_PRODUCT; pass++)
    {
        errcode = ProcessArea(&area, prd, LocList, pass, isPipEnabled, lodhgt);

        if (errcode != eOK)
        {
            misTrc(T_FLOW, "Errored in Mix, process area");
            return (errcode);
        }

        if (*LocList)
        {
           sLog(0, "LocList is not empty.");
           ptr = *LocList;
        }
        
        if (ptr && misTrimLen(ptr->nxtloc, STOLOC_LEN))
            return (eOK);
    }


    /* If we didn't get any avaliable locations in the area, then
     * try to get the location in reverse locations.
     * These locations are exceeded to the max_loads_aisle.
     */
    if (pass_stoloc_list)
    {
        /* Now we cannot find the location that less than
         * max loads per aisle, so we want to get a location
         * that exceed to the max_loads_aisle.
         */
        for (pass_stoloc = pass_stoloc_list; pass_stoloc; 
             pass_stoloc = pass_stoloc->next)
        {
            errcode = GetLocation(pass_stoloc->area,
                                  prd,
                                  pass_stoloc->res,
                                  pass_stoloc->row,
                                  LocList,
                                  isPipEnabled);
            if (errcode == eOK)
            {
                if (*LocList)
                {
                   sLog(0, "LocList is not empty.");
                   ptr = *LocList;
                }

                if (ptr && misTrimLen(ptr->nxtloc, STOLOC_LEN))
                    return (eOK);
            }
        }
    }

    return (eDB_NO_ROWS_AFFECTED);
}

/* A query for the store configuration for location allocation is generated and
   executed. The results are returned as a result set in distro_res; it must be
   free'd by the caller. */
static long ProcessDistroPutToConfig(PRODUCT *prd,
                                     mocaDataRes *config_res,
                                     mocaDataRes **distro_res)
{
    char buffer[5000];
    char *query = NULL;
    char *stoloc = NULL;
    char *arecod = NULL;
    char *colnam = NULL;
    char *colval = NULL;
    long srtseq = 0L;
    long prev_srtseq = 0L;
    long ret_status = eOK;
    int numrows = 0;
    int index = 0;
    mocaDataRow *config_row = NULL;
    
    numrows = sqlGetNumRows(config_res);
    for (index = 0; index < numrows; ++index)
    {
        if (index == 0)
            config_row = sqlGetRow(config_res);
        else
            config_row = sqlGetNextRow(config_row);

        srtseq = sqlGetLong(config_res, config_row, "srtseq");
        stoloc = sqlGetString(config_res, config_row, "stoloc");
        arecod = sqlGetString(config_res, config_row, "arecod");
        colnam = sqlGetString(config_res, config_row, "colnam");
        colval = sqlGetString(config_res, config_row, "colval");
        
        /* Generate query if one of the following is true:
               - this is the first row
               - this is not the first row and srtseq changed
           Note that this handles the case where prev_srtseq is not yet
           initialized because this is the first row. */
        if (index == 0 || (index > 0 && srtseq != prev_srtseq))
        {
            /* This fragment will be appended to the query. It begins with
               'union' if this is not the first statement in the query. srtseq
               is added so that we can order the results by srtseq, thereby
               ordering allocated locations in the same order as the config.
               stoloc is added to the select clause but does not participate in
               the where clause; it is the preferred stoloc if the desired
               configuration for the store matches any rows in the DB. aremst
               data is selected since the data will be handy if we need to call
               ProcessDistroPutToArea. */
            
            /* WMD-37244
             * Here, I have to use distro.client_id = prd->prt_client_id,
             * this may impact a user who uses a client A and then has 
             * part client B. But it should be very unlikely scenario.
             */

            memset(buffer, 0, sizeof(buffer));
            sprintf(buffer,
                    " %s"
                    " select %ld srtseq,"
                    "        '%s' stoloc,"
                    "        aremst.*"
                    "   from aremst,"
                    "        distro,"
                    "        wh_cstmst"
                    "  where aremst.arecod='%s'"
                    "    and aremst.wh_id='%s'"
                    "    and distro.wh_id=aremst.wh_id"
                    "    and distro.client_id='%s'"
                    "    and distro.distro_id='%s'"
                    "    and wh_cstmst.wh_id=distro.wh_id"
                    "    and wh_cstmst.client_id=distro.client_id"
                    "    and wh_cstmst.cstnum=distro.stcust",
                    ((query == NULL) ? "" : "union"),
                    srtseq,
                    ((stoloc == NULL) ? "" : stoloc),
                    arecod,
                    prd->wh_id,
                    prd->prt_client_id,
                    prd->distro_id);

            /* Add buffer for the srtseq to the query */
            if (!misDynStrcat(&query, buffer))
            {
                if (query)
                    free (query);
                return eNO_MEMORY;
            }
            
            /* Reset for next srtseq */
            prev_srtseq = srtseq;
        }
        
        /* Append colnam, colval filter for this srtseq */
        if (misTrimLen(colnam, COLNAM_LEN))
        {
            memset(buffer, 0, sizeof(buffer));
            if (colval == NULL)
            {
                sprintf(buffer,
                        " and %s is NULL",
                        colnam);
            }
            else
            {
                /* Quotes are handled by SQL for numeric columns also */
                sprintf(buffer,
                        " and %s = '%s'",
                        colnam,
                        colval);
            }

            /* Add buffer for the srtseq to the query */
            if (!misDynStrcat(&query, buffer))
            {
                if (query)
                    free (query);
                return eNO_MEMORY;
            }
        }
    }
    
    /* Add order-by clause to the query */
    if (!misDynStrcat(&query, " order by srtseq"))
    {
        if (query)
            free (query);
        return eNO_MEMORY;
    }
    
    /* Run the query */
    *distro_res = NULL;
    ret_status = sqlExecStr(query, distro_res);
    if (eOK != ret_status && *distro_res)
    {
        sqlFreeResults(*distro_res);
        *distro_res = NULL;
    }
    
    /* Cleanup */
    free(query);
    
    /* Done */
    return ret_status;
}

static long ProcessAssignedLocations(PRODUCT * prd,
				     STOLOC_LIST ** LocList,
                     RETURN_STRUCT *Ptr_Assign_Loc,
                     long isPipEnabled,
                     double lodhgt)
{
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;
    long ret_status;

    res = srvGetResults(Ptr_Assign_Loc);
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        /* When trying a specific location, we will more */
        /* likely have someone grab it from underneath us */
        /* ... so, loop while we get DB_DEADLOCK back from */
        /* GetLocation */
        ret_status = eDB_DEADLOCK;
        while (ret_status == eDB_DEADLOCK)
        {
            ret_status = GetLocationRange(prd,
                                          sqlGetString(res, row, "arecod"),
                                          sqlGetString(res, row, "lower_loc"),
                                          sqlGetString(res, row, "upper_loc"),
                                          LocList,
                                          sqlGetString(res, row, "wh_id"),
                                          isPipEnabled,
                                          lodhgt);
        }
        if (ret_status == eOK)
            return (eOK);
    }

    return (eDB_NO_ROWS_AFFECTED);
}

static long ProcessNonAssignedLocations(PRODUCT *prd,
                                        STOLOC_LIST ** LocList,
                                        AREA_LIST *area_list,
                                        long isPipEnabled,
                                        double lodhgt)
{
    AREA_LIST        *area;
    PASS_STOLOC_LIST *pass_stoloc;
    STOLOC_LIST      *ptr = NULL;
    long             errcode;
    long             pass;

    pass_stoloc_list = NULL;  

    /*
     * We do three passes through the building's area list...if all areas
     * are "breadth" first, then first pass is partials, second
     * pass is empties, and third pass is mixing... 
     */
    if (area_list)
    {
        for (pass = 1; pass <= PASS_MIX_PRODUCT; pass++)
        {
            for (area = area_list; area; area = area->next)
            {
                errcode = ProcessArea(area, prd, LocList, pass, isPipEnabled, lodhgt);
    
                if (errcode != eOK)
                {
                    misTrc(T_FLOW, "Errored in Mix, process area");
                    return (errcode);
                }
    
                if (*LocList)
                {
                   sLog(0, "LocList is not empty.");
                   ptr = *LocList;
                }
                
                if (ptr && misTrimLen(ptr->nxtloc, STOLOC_LEN))
                {
                    return (eOK);
                }
            }
        }
    }

    /* If we didn't get any avaliable locations in the area, then
     * try to get the location in reverse locations.
     * These locations are exceeded to the max_loads_aisle.
     */
    if (pass_stoloc_list)
    {
        /* Now we cannot find the location that less than
         * max loads per aisle, so we want to get a location
         * that exceed to the max_loads_aisle.
         */
        for (pass_stoloc = pass_stoloc_list; pass_stoloc; 
             pass_stoloc = pass_stoloc->next)
        {
            errcode = GetLocation(pass_stoloc->area,
                                  prd,
                                  pass_stoloc->res,
                                  pass_stoloc->row,
                                  LocList,
                                  isPipEnabled);
            if (errcode == eOK)
            {
                if (*LocList)
                {
                   sLog(0, "LocList is not empty.");
                   ptr = *LocList;
                }

                if (ptr && misTrimLen(ptr->nxtloc, STOLOC_LEN))
                    return (eOK);
            }
        }                        
    }
    return (eDB_NO_ROWS_AFFECTED);
}
static long GetFootprint(PRODUCT * prd)
{
    long            ret_status;
    char            buffer[500];
    mocaDataRow    *row = NULL;
    mocaDataRes    *res = NULL;

    if (misTrimLen(prd->ftpcod, FTPCOD_LEN) == 0)
    {
        prd->caslen = 1;
        prd->cashgt = 1;
        prd->caswid = 1;
        prd->caslvl = 1;
        prd->pal_stck_hgt = 0;
        return (eOK);
    }


    sprintf(buffer,
	    "select * from ftpmst where ftpcod = '%s'", prd->ftpcod);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (ret_status);
    }

    row = sqlGetRow(res);
    prd->caslen = sqlGetFloat(res, row, "caslen");
    prd->cashgt = sqlGetFloat(res, row, "cashgt");
    prd->caswid = sqlGetFloat(res, row, "caswid");
    prd->caslvl = sqlGetLong(res, row, "caslvl");
    prd->pal_stck_hgt = sqlGetLong(res, row, "pal_stck_hgt");

    sqlFreeResults(res);

    if (0 == prd->caslen || 0 == prd->cashgt ||
	0 == prd->caswid || 0 == prd->caslvl)
	return(eINT_WOULD_DIVIDE_BY_ZERO);
	
    
    return (eOK);
}

static long isReturnPart(char *lodnum, char * subnum, char * dtlnum)
{
    long ret_status;
    char tmpbuffer[128];
    char buffer[512];

    memset (buffer, 0, sizeof(buffer));
    memset (tmpbuffer, 0, sizeof(tmpbuffer));
    
    if (misTrimLen(dtlnum, DTLNUM_LEN))
        sprintf(tmpbuffer, " and invdtl.dtlnum = '%s' ", dtlnum);
    else if (misTrimLen(subnum, SUBNUM_LEN))
        sprintf(tmpbuffer, " and invdtl.subnum = '%s' ", subnum);
    else 
        sprintf(tmpbuffer, " and invsub.lodnum = '%s' ", lodnum);
        
    sprintf(buffer,
            " select 1 "
            "   from invdtl, "
            "        invsub  "
            "  where invdtl.subnum = invsub.subnum  "
            "    and invdtl.prtnum = '%s' "
            "    and invdtl.prt_client_id = '%s' "
            "    %s  ",
            PRTNUM_RETURN,
            DEFAULT_INV_ATTR,
            tmpbuffer);

    ret_status = sqlExecStr(buffer, NULL);
    
    if (ret_status == eOK)
        ret_status = 1;
    else
        ret_status = 0;
    return ret_status;
}   

/* For a given identifier check the 
 * invdtl for the virtual part EMPTYPART.
 */
static long isEmptyPart(char *lodnum, 
                        char *subnum, 
                        char *dtlnum)
{
    long ret_status;
    char tmpbuffer[128];
    char buffer[512];

    memset (buffer, 0, sizeof(buffer));
    memset (tmpbuffer, 0, sizeof(tmpbuffer));
    
    if (dtlnum && misTrimLen(dtlnum, DTLNUM_LEN))
    {
        sprintf(tmpbuffer, " and invdtl.dtlnum = '%s' ", dtlnum);
    }
    else if (subnum && misTrimLen(subnum, SUBNUM_LEN))
    {
        sprintf(tmpbuffer, " and invdtl.subnum = '%s' ", subnum);
    }
    else if (lodnum && misTrimLen(lodnum, LODNUM_LEN))
    {
        sprintf(tmpbuffer, " and invsub.lodnum = '%s' ", lodnum);
    }
    else
    {
        return 0;
    }
        
    sprintf(buffer,
            " select 1 "
            "   from invdtl, "
            "        invsub  "
            "  where invdtl.subnum = invsub.subnum  "
            "    and invdtl.prtnum = '%s' "
            "    %s  ",
            PRTNUM_EMPTY,
            tmpbuffer);

    ret_status = sqlExecStr(buffer, NULL);
    
    if (ret_status == eOK)
    {
        ret_status = 1;
    }
    else
    {
        ret_status = 0;
    }
    return ret_status;
}

long trnAllocateLocation(char *lodnum_i, char *subnum_i, char *dtlnum_i,
			 char *arecod, char *wrkzon, char *stoloc,
			 moca_bool_t trcflg, char *trcfil, char *prtnum_i,
			 char *prt_client_id_i,
			 char *lotnum_i, char *revlvl_i, char *orgcod_i,
			 char *invsts_i, long untcas_i, long untqty_i,
			 char *fifdte_i, char *type_i, char *ftpcod_i,
			 STOLOC_LIST ** LocList, long untpak_i,
			 char *cur_bldg_id_i, char *xdkref_i, long xdkqty_i,
                         char *invmov_typ_i, char *asset_typ_i,
                         char *wh_id_i, double lodhgt_i)
{

    /*   Local Variables   */

    static mocaDataRes *mixpolRes = NULL;
    static mocaDataRes *FifoSkipAsgLocRes = NULL;

    mocaDataRes  *tres = NULL;
    mocaDataRow  *trow = NULL;

    mocaDataRes    *res = NULL;
    mocaDataRes    *bldgRes = NULL;
    mocaDataRes    *distroRes = NULL;
    mocaDataRow    *distroRow = NULL;
    mocaDataRow    *row = NULL; 
    mocaDataRow    *topRow = NULL;
    mocaDataRow    *polRow = NULL;
    mocaDataRow    *bldgRow = NULL;
    mocaDataRes    *subassetres = NULL;
    mocaDataRow    *subassetrow = NULL;
    RETURN_STRUCT  *CmdRes = NULL;
    int             errcode;
    long            ret_status;
    long            currentBldgOnly;
    long            bldgCnt;
    int             store_mixed_together, store_mixed_individually;
    double          reccnt;
    char            sqlbuffer[8192];
    char            Buffer1[500];
    char            type[50];
    char            lodnum[LODNUM_LEN + 1];
    char            subnum[SUBNUM_LEN + 1];
    char            dtlnum[DTLNUM_LEN + 1];
    char            asset_typ[ASSET_TYP_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    char            tmpbuf[1000];
    char            dst_bldg_id[BLDG_ID_LEN + 1];
    long            ii;
    long            isPipEnabled;
    long            max_untpal=0L;
    long            max_untcas=0L;
    long            load_qty=0L;
    moca_bool_t     fifo_skip_asg_loc_flg = BOOLEAN_FALSE; 
    moca_bool_t     search_assigned_location_first = BOOLEAN_FALSE;
    RETURN_STRUCT  *Ptr_Assign_Loc = NULL;
    moca_bool_t     asset_track_enabled = BOOLEAN_NOTSET;
    double          lodhgt = 0;
    long            isEmptyPrtnum = 0;

    PRODUCT        *prod_list, *prd, *newprd;
    AREA_LIST      *area_list, area;
    char           *distro_lodlvl = NULL;

    memset(lodnum, 0, sizeof(lodnum));
    memset(subnum, 0, sizeof(subnum));
    memset(dtlnum, 0, sizeof(dtlnum));
    memset(asset_typ, 0, sizeof(asset_typ));
    memset(dst_bldg_id, 0, sizeof(dst_bldg_id));
    memset(wh_id, 0, sizeof(wh_id));

    memset(sqlbuffer, 0, sizeof(sqlbuffer));
    memset(Buffer1, 0, sizeof(Buffer1));
    memset(tmpbuf, 0, sizeof(tmpbuf));
    memset(type, 0, sizeof(type));

    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
        misTrimcpy(lodnum, lodnum_i, LODNUM_LEN);
    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
        misTrimcpy(subnum, subnum_i, SUBNUM_LEN);
    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
        misTrimcpy(dtlnum, dtlnum_i, DTLNUM_LEN);

    misTrc(T_FLOW, "In VAR trnAllocateLocation()");

    lodhgt = lodhgt_i;
    /* 
    * wh_id has been processed in intAllocateLocation.c
    * it will never be null or empty
    */
    misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

    /* Check asset tracking configuration is enabled. 
     * if disabled set the assettyp to null.
     */
    sprintf(Buffer1, "check asset category enabled "
                     " configuration where asset_cat = '%s'"
                     "                 and wh_id = '%s' ",
                     ASSET_CAT_INV,
                     wh_id);

    ret_status = srvInitiateCommand(Buffer1, &CmdRes);
    if (ret_status != eOK)
    {
        misTrc(T_FLOW,
            "Error while checking asset category is enabled ");
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        return(ret_status); 
    }
    else
    {
        res = srvGetResults(CmdRes);
        row = sqlGetRow(res);
        asset_track_enabled = sqlGetBoolean(res, row, "ena_flg");
        if (asset_track_enabled == BOOLEAN_FALSE)
        {
            misTrc(T_FLOW,
                   "Asset category is disabled ");
        }
    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    CmdRes = NULL;
    res = NULL;

    errcode=42;
    isPipEnabled=0; /*Default value*/

    if (trcflg == BOOLEAN_NOTSET)
    {
        sprintf(sqlbuffer,
                "select rtnum1 "
                "  from poldat_view "
                " where polcod = '%s' "
                "   and polvar = '%s' "
                "   and polval = '%s' "
                "   and wh_id  = '%s' ",
                POLCOD_ALLOCATE_WAVE,
                POLVAR_MISCELLANEOUS,
                POLVAL_LOG_FILE_ENABLED,
                wh_id);
        
        ret_status = sqlExecStr(sqlbuffer, &res);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(res);
            return (ret_status);
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            /*
             * If the policy is not defined, let everything continue,
             * just don't do a log file.
             */

            trcflg = 0;
            misTrc(T_FLOW, "Policy not defined for %s/%s/%s",
                            POLCOD_ALLOCATE_WAVE,
                            POLVAR_MISCELLANEOUS,
                            POLVAL_LOG_FILE_ENABLED);
            sqlFreeResults(res);
        }
        else
        {
            row = sqlGetRow(res);
            trcflg = sqlGetLong(res, row, "rtnum1");
            misTrc(T_FLOW, "Log file enabled status is %ld", trcflg);
            sqlFreeResults(res);
        }
    }

    OpenTraceFile(trcflg, lodnum_i);

    /*
     * Get the policy ALLOCATE-LOC\RESPECT-FIFO-SKIP-LOCS\ENABLED 
     */

    if (FifoSkipAsgLocRes == NULL)
    {
        sprintf(sqlbuffer,
                "select wh_id, rtnum1 "
                "  from poldat_view "
                " where polcod = '%s' "
                "   and polvar = '%s' "
                "   and polval = '%s' ",
                POLCOD_ALLOCATE_LOC,
                POLVAR_RESPECT_FIFO_SKIP_LOCS,
                POLVAL_ENABLED);
        
        ret_status = sqlExecStr(sqlbuffer, &FifoSkipAsgLocRes);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(FifoSkipAsgLocRes);
            FifoSkipAsgLocRes = NULL;

            CloseTraceFile();
            return (ret_status);
        }

        misFlagCachedMemory((OSFPTR) sqlFreeResults, FifoSkipAsgLocRes);
    }

    fifo_skip_asg_loc_flg = BOOLEAN_FALSE;

    for(row = sqlGetRow(FifoSkipAsgLocRes); row; row = sqlGetNextRow(row))
    {
        if (strncmp(sqlGetString(FifoSkipAsgLocRes, row, "wh_id"), 
                    wh_id, WH_ID_LEN) == 0)
        {
            if (sqlGetLong(FifoSkipAsgLocRes, row, "rtnum1"))
                fifo_skip_asg_loc_flg = BOOLEAN_TRUE;
            else
                fifo_skip_asg_loc_flg = BOOLEAN_FALSE;

            break;
        }
    }

    misTrc(T_FLOW, "Skip assigned locations for FIFO part is %s",
           fifo_skip_asg_loc_flg ? "enabled" : "disabled");


    prod_list = prd = NULL;

    /* First, create the product structure with a single element ...
       we may need to make more elements later ... */

    prod_list = (PRODUCT *) calloc(1, sizeof(PRODUCT));
    if (!prod_list)
    {
	return (eNO_MEMORY);
    }
    prd = prod_list;

    /* set wh_id for prd, wh_id is constant in this command */
    misTrimcpy(prd->wh_id, wh_id, WH_ID_LEN);

    /* 
     * if asset_typ is coming in, this may happen in replenishment
     * process to allocate a replenishable location.
     * We should take asset volume into account if 
     * ASSET-TRACKING configuration is enabled.
     */
    if (asset_track_enabled == BOOLEAN_TRUE &&
        asset_typ_i && misTrimLen(asset_typ_i, ASSET_TYP_LEN))
    {
        misTrimcpy(asset_typ, asset_typ_i, ASSET_TYP_LEN);

        misTrc(T_FLOW, "Asset is coming in, asset_typ = %s",
               asset_typ);

        sprintf(sqlbuffer,
                " select asset_len, "
                "        asset_wid, "
                "        asset_hgt, "
                "        decode(container_flg, 0, 0, 1, max_vol) max_vol "
                "   from asset_typ "
                "  where asset_typ = '%s' ",
                asset_typ);

        ret_status = sqlExecStr(sqlbuffer, &res);

        if (ret_status == eOK)
        {
            row = sqlGetRow(res);

            misTrimcpy(prd->asset_typ, asset_typ, ASSET_TYP_LEN);
            prd->asset_len = sqlGetFloat(res, row, "asset_len");
            prd->asset_wid = sqlGetFloat(res, row, "asset_wid");
            prd->asset_hgt = sqlGetFloat(res, row, "asset_hgt");
            prd->asset_max_vol = sqlGetFloat(res, row, "max_vol");

            sqlFreeResults(res);
            res = NULL;
        }
        else
        {
            sqlFreeResults(res);
            CloseTraceFile();
            return (ret_status);
        }
    }
    /* Check if the PIP is enabled */
    sprintf(sqlbuffer,
         "select 'x'" 
         "   from poldat_view" 
         "  where polcod = '%s'" 
         "    and polvar = '%s'" 
         "    and polval = '%s'" 
         "    and rtnum1 = %ld"
         "    and wh_id  = '%s'",
         POLCOD_PRE_INV_PICKING,
         POLVAR_INSTALLED,
         POLVAL_INSTALLED,
         BOOLEAN_TRUE,
         wh_id);
    ret_status = sqlExecStr(sqlbuffer, NULL);

    if (ret_status == eOK)
    {
        isPipEnabled = 1;
        misTrc(T_FLOW,"PIP is enabled, When allocating replenishments "
                      " the QVLs of the destination location will be ignored"
                      " We do not want to limit allocation of replenishment "
                      " based on size of the dstloc.");
    }
    else
    {
        isPipEnabled = 0;
        misTrc(T_FLOW,"PIP is disabled, we will need to account"
                      " for the volume of destination location,"
                      " during allocation.");
        if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return (ret_status);
        }
    }

    /* Set the type of allocation.  We default to storage unless
       we have an exact match on replenishment.... */

    memset(type, 0, sizeof(type));
    if (type_i && misTrimLen(type_i, 10) > 0)
    {
	if (misCiStrncmp(type_i, ALLOCATE_REPLENISH,
			 strlen(ALLOCATE_REPLENISH)) == 0)
	    strcpy(prd->stotyp, ALLOCATE_REPLENISH);
	else if (misCiStrncmp(type_i, ALLOCATE_TOPOFF_REPLEN,
			      strlen(ALLOCATE_TOPOFF_REPLEN)) == 0)
	    strcpy(prd->stotyp, ALLOCATE_TOPOFF_REPLEN);
	else
	    strcpy(prd->stotyp, ALLOCATE_STORAGE);
    }
    else
    {
	strcpy(prd->stotyp, ALLOCATE_STORAGE);
    }

    if ( (misCiStrncmp(type_i, ALLOCATE_REPLENISH,
		       strlen(ALLOCATE_REPLENISH)) == 0) ||
	 (misCiStrncmp(type_i, ALLOCATE_TOPOFF_REPLEN,
		       strlen(ALLOCATE_TOPOFF_REPLEN)) == 0) )
    {
	sprintf(sqlbuffer,
		"select polcod "
		"  from poldat_view "
		" where polcod = '%s' "
		"   and polvar = '%s' "
		"   and polval = '%s' "
		"   and rtnum1 = '%ld' "
		"   and wh_id  = '%s' ",
		POLCOD_REPLENISHMENT, 
                POLVAR_MISC,
                POLVAL_REPLENISHMENT_FIFO,
                BOOLEAN_TRUE,
                wh_id);

	errcode = sqlExecStr(sqlbuffer, NULL);

	if (errcode != eOK)
	    strncpy(prd->disfif, "N", FLAG_LEN);
	else
	    strncpy(prd->disfif, "Y", FLAG_LEN);

    }
    else
    {
	strncpy(prd->disfif, "N", FLAG_LEN);
    }

    sLog(0, "Allocating location for load %s sub %s dtl %s",
	    lodnum_i, subnum_i, dtlnum_i);

    /*
     * if only part information was received (this may be the case when
     * we're processing a replenishment), select just the items we're
     * missing (we may not be given a fifo date).  Otherwise, get all
     * the information for the load.
     */

    if ((misTrimLen(lodnum, LODNUM_LEN) == 0) &&
	(misTrimLen(subnum, SUBNUM_LEN) == 0) &&
	(misTrimLen(dtlnum, DTLNUM_LEN) == 0))
    {
	memset(Buffer1, 0, sizeof(Buffer1));
	if (fifdte_i && misTrimLen(fifdte_i, 30))
	    sprintf(Buffer1, "'%s' fifdte, ",
		    fifdte_i);

	sprintf(sqlbuffer,
		"select prtfam, fifwin, timcod, "
		"       nvl(velzon, 'B') velzon, "
		"       untpal, "
		"       %s "
		"       ftpcod "
		"  from prtmst_view "
		" where prtnum = '%s' "
		"   and prt_client_id = '%s' "
		"   and wh_id = '%s'",
		Buffer1, prtnum_i, prt_client_id_i,wh_id_i);
    }
    else
    {
        long returnprtnum = isReturnPart(lodnum, subnum, dtlnum);
        isEmptyPrtnum = isEmptyPart(lodnum, subnum, dtlnum);
        prd->empty = isEmptyPrtnum;

        if (returnprtnum == 0 &&
            isEmptyPrtnum == 0)
        {
            if (dtlnum[0] > '\0')
                sprintf(tmpbuf, " idt.dtlnum = '%s' ", dtlnum);
            else if (subnum[0] > '\0')
                sprintf(tmpbuf, " ivs.subnum = '%s' ", subnum);
            else if (lodnum[0] > '\0')
                sprintf(tmpbuf, " ild.lodnum = '%s' ", lodnum);

            /* WMD-37244
             * Here, I have to join invdtl.prt_client_id to
             * distro.client_id, this may impact a user who
             * uses a client A and then has part client B.
             * But it should be very unlikely scenario.
             */
            sprintf(sqlbuffer,
                    "select distinct count(*) reccnt, are.wh_id, idt.prtnum, "
                    "       are.bldg_id, "
                    "       prt.prt_client_id, prt.prtfam, "
                    "       prt.fifwin, prt.timcod, "
                    "       nvl(prt.velzon, 'B') velzon, idt.orgcod, idt.revlvl, "
                    "       idt.supnum,"
                    "       idt.invsts, "
                    "       idt.lotnum,"
                    "       prt.untpal, "
                    "       idt.untcas, "
                    "       idt.untpak, "
                    "       sum(idt.untqty) sumqty, "
                    "       min(idt.fifdte) minfif, "
                    "       lcn.arecod, prt.ftpcod, lcn.wrkzon,"
                    "       idt.ftpcod iftpcod, "
                    "       idt.prtnum||prt.prt_client_id||idt.orgcod||idt.revlvl"
                    "       ||idt.invsts||idt.lotnum||TO_CHAR(idt.untcas, '999')"
                    "       ||TO_CHAR(idt.untpak, '999') mykey, "
                    "       asset_typ.asset_typ, "
                    "       asset_typ.asset_len, "
                    "       asset_typ.asset_wid, "
                    "       asset_typ.asset_hgt, "
                    "       asset_typ.container_flg, "
                    "       decode(asset_typ.container_flg, 0, 0, 1, asset_typ.max_vol) "
                    "       max_vol, " 
                    "       distro.distro_id "
                    "  from prtmst_view prt, "
                    "       aremst are, "
                    "       locmst lcn,"
                    "       invlod ild left outer join asset_typ "
                    "           on ild.asset_typ = asset_typ.asset_typ, "
                    "       invsub ivs, "
                    "       invdtl idt left outer join distro "
                    "           on idt.distro_id = distro.distro_id "
                    "          and idt.prt_client_id = distro.client_id "
                    "          and distro.wh_id = '%s' "
                    " where %s "
                    "   and ild.lodnum = ivs.lodnum "
                    "   and ivs.subnum = idt.subnum "
                    "   and prt.prtnum = idt.prtnum "
                    "   and prt.prt_client_id = idt.prt_client_id "
                    "   and prt.wh_id = ild.wh_id "
                    "   and lcn.stoloc = ild.stoloc "
                    "   and lcn.wh_id  = ild.wh_id "
                    "   and lcn.arecod = are.arecod "
                    "   and lcn.wh_id  = are.wh_id "
                    "   and lcn.wh_id  = '%s' "
                    " group by are.wh_id, are.bldg_id, "
                    "          idt.prtnum, prt.prt_client_id, "
                    "          prt.prtfam, prt.fifwin, prt.timcod, prt.velzon, "
                    "          idt.orgcod, idt.revlvl, idt.supnum, idt.invsts, "
                    "          idt.lotnum, prt.untpal, "
                    "          idt.untcas, idt.untpak, lcn.arecod, prt.ftpcod, "
                    "          lcn.wrkzon, "
                    "          idt.ftpcod, idt.prtnum||prt.prt_client_id "
                    "          ||idt.orgcod||idt.revlvl "
                    "          ||idt.invsts||idt.lotnum||TO_CHAR(idt.untcas,'999')"
                    "          ||TO_CHAR(idt.untpak,'999'), "
                    "          asset_typ.asset_typ, "
                    "          asset_typ.asset_len, "
                    "          asset_typ.asset_wid, "
                    "          asset_typ.asset_hgt, "
                    "          asset_typ.container_flg, "
                    "          max_vol, "
                    "          distro.distro_id ",
                    wh_id, tmpbuf, wh_id);
        }
        else
        {
            if (dtlnum[0] > '\0')
                sprintf(tmpbuf, " idt.dtlnum = '%s' ", dtlnum);
            else if (subnum[0] > '\0')
                sprintf(tmpbuf, " ivs.subnum = '%s' ", subnum);
            else if (lodnum[0] > '\0')
                sprintf(tmpbuf, " ild.lodnum = '%s' ", lodnum);

            /* WMD-37244
             * Here, I have to join invdtl.prt_client_id to
             * distro.client_id, this may impact a user who
             * uses a client A and then has part client B.
             * But it should be very unlikely scenario.
             */
            sprintf(sqlbuffer,
                    "select distinct count(*) reccnt, are.wh_id, idt.prtnum, "
                    "       are.bldg_id, "
                    "       idt.prt_client_id, '' prtfam, "
                    "       cast(NULL as float) fifwin, NULL timcod, "
                    "       'B' velzon, idt.orgcod, idt.revlvl, "
                    "       idt.supnum, "
                    "       idt.invsts, "
                    "       idt.lotnum,"
                    "       idt.untcas, "
                    "       idt.untpak, "
                    "       sum(idt.untqty) sumqty, "
                    "       min(idt.fifdte) minfif, "
                    "       lcn.arecod, '' ftpcod, lcn.wrkzon,"
                    "       idt.ftpcod iftpcod, "
                    "       idt.prtnum||idt.prt_client_id||idt.orgcod||idt.revlvl"
                    "       ||idt.invsts||idt.lotnum||TO_CHAR(idt.untcas, '999')"
                    "       ||TO_CHAR(idt.untpak, '999') mykey, "
                    "       asset_typ.asset_typ,  "
                    "       asset_typ.asset_len, "
                    "       asset_typ.asset_wid, "
                    "       asset_typ.asset_hgt, "
                    "       asset_typ.container_flg, "
                    "       decode(asset_typ.container_flg, 0, 0, "
                    "              1, asset_typ.max_vol) max_vol, "
                    "       distro.distro_id "
                    "  from aremst are, "
                    "       locmst lcn,"
                    "       invlod ild left outer join asset_typ "
                    "           on ild.asset_typ = asset_typ.asset_typ, "
                    "       invsub ivs, "
                    "       invdtl idt left outer join distro "
                    "           on idt.distro_id = distro.distro_id "
                    "          and idt.prt_client_id = distro.client_id "
                    "          and distro.wh_id = '%s' "
                    " where %s "
                    "   and ild.lodnum = ivs.lodnum "
                    "   and ivs.subnum = idt.subnum ", 
                    wh_id, tmpbuf);
            if (isEmptyPrtnum)
            {
                sprintf(sqlbuffer, "%s "
                        "   and idt.prtnum = '%s' ",
                        sqlbuffer, PRTNUM_EMPTY);
            }
            else
            {
                sprintf(sqlbuffer, "%s "
                        "   and idt.prtnum = '%s' "
                        "   and idt.prt_client_id = '%s'",
                        sqlbuffer, PRTNUM_RETURN, DEFAULT_INV_ATTR);
            }
            sprintf(sqlbuffer, "%s "             
                    "   and lcn.stoloc = ild.stoloc "
                    "   and lcn.wh_id  = ild.wh_id "
                    "   and lcn.arecod = are.arecod "
                    "   and lcn.wh_id  = are.wh_id "
                    "   and lcn.wh_id  = '%s' "
                    " group by are.wh_id, are.bldg_id, "
                    "          idt.prtnum, idt.prt_client_id, "
                    "          idt.orgcod, idt.revlvl, idt.supnum, idt.invsts, "
                    "          idt.lotnum, idt.untcas, idt.untpak, lcn.arecod, "
                    "          lcn.wrkzon, "
                    "          idt.ftpcod, idt.prtnum||idt.prt_client_id "
                    "          ||idt.orgcod||idt.revlvl "
                    "          ||idt.invsts||idt.lotnum||TO_CHAR(idt.untcas,'999')"
                    "          ||TO_CHAR(idt.untpak,'999'), "
                    "          asset_typ.asset_typ, "
                    "          asset_typ.asset_len, "
                    "          asset_typ.asset_wid, "
                    "          asset_typ.asset_hgt, "
                    "          asset_typ.container_flg, "
                    "          max_vol, "
                    "          distro.distro_id ",
                    sqlbuffer,
                    wh_id,
                    tmpbuf,
                    wh_id);
        }
    }

    errcode = sqlExecStr(sqlbuffer, &res);

    if (errcode != eOK)
    {
	sqlFreeResults(res);
	CloseTraceFile();
	FreeProdList(prod_list);
	return (errcode);
    }

    /* Now lock the load if we are selecting by load number */
    /* This cannot be done above because Oracle doesn't allow the for update */
    /* clause on that select ... */
    /* Changed 4/18/95 - JMF cannot do a select for update on the load */
    /* as there is no column to update on the load, so we will verify */
    /* that no invmov records already exist ... */
    /* Changed 7/20/95 - JMF check for invmovs for all levels of inventory */

    if (misTrimLen(dtlnum, DTLNUM_LEN))
    {
	for (ii = 0; ii < 3; ii++)
	{
	    if (ii == 0)
		sprintf(sqlbuffer,
			"select 'X' "
			"  from invmov "
			" where lodnum = '%s' ",
			dtlnum);
	    else if (ii == 1)
		sprintf(sqlbuffer,
			"select 'X' "
			"  from invmov "
			" where lodnum = "
			"  (select invdtl.subnum "
			"     from invdtl "
			"    where invdtl.dtlnum = '%s')",
			dtlnum);
	    else
		sprintf(sqlbuffer,
			"select 'X' "
			"  from invmov "
			" where lodnum = "
			"  (select invsub.lodnum "
			"     from invdtl, invsub "
			"    where invdtl.dtlnum = '%s' "
			"      and invsub.subnum = invdtl.subnum)",
			dtlnum);

	    errcode = sqlExecStr(sqlbuffer, NULL);
	    if (errcode == eOK)
	    {
		sqlFreeResults(res);
		CloseTraceFile();
		FreeProdList(prod_list);
		return (eINT_STOLOC_ALREADY_ALLOCATED);
	    }
	    else if (errcode != eDB_NO_ROWS_AFFECTED)
	    {
		sqlFreeResults(res);
		CloseTraceFile();
		FreeProdList(prod_list);
		return (errcode);
	    }
	}
    }
    else if (misTrimLen(subnum, SUBNUM_LEN))
    {
	for (ii = 0; ii < 3; ii++)
	{
	    if (ii == 0)
		sprintf(sqlbuffer,
			"select 'X' from invmov "
			" where lodnum = '%s' ",
			subnum);
	    else if (ii == 1)
		sprintf(sqlbuffer,
			"select 'X' "
			"  from invmov "
			" where lodnum = "
			"  (select invsub.lodnum "
			"     from invsub "
			"    where invsub.subnum = '%s')",
			subnum);
	    else
		sprintf(sqlbuffer,
			"select 'X' "
			"  from invmov "
			" where lodnum in "
			"  (select invdtl.dtlnum "
			"     from invdtl,invsub "
			"    where invsub.subnum='%s' "
			"      and invsub.subnum = invdtl.subnum)",
			subnum);

	    errcode = sqlExecStr(sqlbuffer, NULL);
	    if (errcode == eOK)
	    {
		sqlFreeResults(res);
		CloseTraceFile();
		FreeProdList(prod_list);
		return (eINT_STOLOC_ALREADY_ALLOCATED);
	    }
	    else if (errcode != eDB_NO_ROWS_AFFECTED)
	    {
		sqlFreeResults(res);
		CloseTraceFile();
		FreeProdList(prod_list);
		return (errcode);
	    }
	}
    }
    else if (misTrimLen(lodnum, LODNUM_LEN))
    {
	for (ii = 0; ii < 3; ii++)
	{
	    if (ii == 0)
		sprintf(sqlbuffer,
			"select 'X' "
			"  from invmov "
			" where lodnum = '%s' ",
			lodnum);
	    else if (ii == 1)
		sprintf(sqlbuffer,
			"select 'X' "
			"  from invmov "
			" where lodnum in "
			"  (select invsub.subnum "
			"     from invsub "
			"    where invsub.lodnum = '%s')",
			lodnum);
	    else
		sprintf(sqlbuffer,
			"select 'X' "
			"  from invmov "
			" where lodnum in "
			"  (select invdtl.dtlnum "
			"     from invdtl,invsub "
			"    where invsub.lodnum='%s' "
			"      and invsub.subnum=invdtl.subnum)",
			lodnum);

	    errcode = sqlExecStr(sqlbuffer, NULL);
	    if (errcode == eOK)
	    {
		sqlFreeResults(res);
		CloseTraceFile();
		FreeProdList(prod_list);
		return (eINT_STOLOC_ALREADY_ALLOCATED);
	    }
	    else if (errcode != eDB_NO_ROWS_AFFECTED)
	    {
		sqlFreeResults(res);
		CloseTraceFile();
		FreeProdList(prod_list);
		return (errcode);
	    }
	}
    }

    row = sqlGetRow(res);

    if ((misTrimLen(lodnum, LODNUM_LEN) == 0) &&
	(misTrimLen(subnum, SUBNUM_LEN) == 0) &&
	(misTrimLen(dtlnum, DTLNUM_LEN) == 0))
    {
	strncpy(prd->wh_id, wh_id, WH_ID_LEN);
	strncpy(prd->prtnum, prtnum_i, PRTNUM_LEN);
	strncpy(prd->prt_client_id, prt_client_id_i, CLIENT_ID_LEN);
	strncpy(prd->prtfam,
		(char *) sqlGetValue(res, row, "prtfam"), PRTFAM_LEN);

	/*
	 * A building may have been passed in, so get it if it was.
	 */

        memset(prd->cur_bldg_id, 0, sizeof(prd->cur_bldg_id));
	if(cur_bldg_id_i && misTrimLen(cur_bldg_id_i, BLDG_ID_LEN) > 0)
            misTrimcpy(prd->cur_bldg_id, cur_bldg_id_i, BLDG_ID_LEN); 

	sLog(0, "Selected Part Info: prtnum='%s', prt_client_id='%s',"
		" prtfam='%s'",
	       prd->prtnum, prd->prt_client_id, prd->prtfam);

	/*
	 * Save load info
	 */
	prd->fifwin = sqlGetLong(res, row, "fifwin");
        if (!sqlIsNull(res, row, "timcod"))
            strncpy(prd->timcod, sqlGetString(res, row, "timcod"), TIMCOD_LEN);
        else
            strncpy(prd->timcod, TIMCOD_DAYS, TIMCOD_LEN);

	strncpy(prd->velzon,
		(char *) sqlGetValue(res, row, "velzon"), VELZON_LEN);
	if (orgcod_i && misTrimLen(orgcod_i, ORGCOD_LEN))
	    strncpy(prd->orgcod, orgcod_i, ORGCOD_LEN);
	if (revlvl_i && misTrimLen(revlvl_i, REVLVL_LEN))
	    strncpy(prd->revlvl, revlvl_i, REVLVL_LEN);
	if (invsts_i && misTrimLen(invsts_i, INVSTS_LEN))
	    strncpy(prd->invsts, invsts_i, INVSTS_LEN);
	if (lotnum_i && misTrimLen(lotnum_i, LOTNUM_LEN))
	    strncpy(prd->lotnum, lotnum_i, LOTNUM_LEN);
	if (ftpcod_i && misTrimLen(ftpcod_i, FTPCOD_LEN))
	    strncpy(prd->ftpcod, ftpcod_i, misTrimLen(ftpcod_i, FTPCOD_LEN));
	else
	    strncpy(prd->ftpcod,
		    (char *) sqlGetValue(res, row, "ftpcod"), FTPCOD_LEN);

	if (fifdte_i && misTrimLen(fifdte_i, 30))
	{
	    strncpy(prd->fifdte, fifdte_i, misTrimLen(fifdte_i, 30));
        }
	if (!sqlIsNull(res, row, "untpal"))
            prd->untpal = sqlGetLong(res, row, "untpal");
	prd->untcas = untcas_i;
	prd->untpak = untpak_i;
	prd->totqty = untqty_i;
	if (xdkref_i && misTrimLen(xdkref_i, XDKREF_LEN))
	    misTrimcpy(prd->xdkref, xdkref_i, XDKREF_LEN);
	prd->xdkqty = xdkqty_i;
    }
    else
    {
	/* Check for mixed product */
	memset(tmpbuf, 0, sizeof(tmpbuf));
	strcpy(tmpbuf, sqlGetString(res, row, "mykey"));
	while (row && !strcmp(tmpbuf, sqlGetString(res, row, "mykey")))
	    row = sqlGetNextRow(row);

	if (row)
	    prd->mixed = TRUE;

	/* Now, if it is mixed, see if we are supposed to distribute the
	   load around ... or if we are supposed to store it as single */

	if (prd->mixed == TRUE)
	{
	    store_mixed_together = store_mixed_individually = FALSE;

	    /* If we haven't read any of the mixed policies, get them */
	    if (mixpolRes == NULL)
	    {
		sprintf(sqlbuffer,
			"select wh_id, polval, srtseq, rtstr1 "
			"  from poldat_view "
			" where polcod = '%s' "
			"   and polvar = '%s' ",
			POLCOD_STORE_DEFARE,
			POLVAR_SEL_MIXED_PARMS);
		errcode = sqlExecStr(sqlbuffer, &mixpolRes);
		if (errcode != eOK && errcode != eDB_NO_ROWS_AFFECTED)
		{
		    sqlFreeResults(res);
		    CloseTraceFile();
		    FreeProdList(prod_list);
		    return (errcode);
		}

		misFlagCachedMemory((OSFPTR) sqlFreeResults, mixpolRes);
	    }

	    /* Save the top row of our product information and loop through */
	    /* the policies to determine what we are being instructed to do */
	    topRow = sqlGetRow(res);
	    for (polRow = sqlGetRow(mixpolRes); polRow;
		 polRow = sqlGetNextRow(polRow))
	    {
	        /* only visit the policies for current warehouse. */
	        if (strncmp(sqlGetString(mixpolRes, polRow, "wh_id"), 
	                    wh_id, WH_ID_LEN) != 0)
                {
                    continue;
                }

		if (strncmp((char *) sqlGetValue(mixpolRes, polRow, "polval"),
			    POLVAL_STORE_MIXED_LOADS_INDIVIDUALLY,
			 strlen(POLVAL_STORE_MIXED_LOADS_INDIVIDUALLY)) == 0
		    && misCiStrncmp((char *) sqlGetValue(mixpolRes,
				     polRow, "rtstr1"), "Y", FLAG_LEN) == 0)
		{
		    store_mixed_individually = TRUE;
		}
		else if (strncmp(sqlGetValue(mixpolRes, polRow, "polval"),
				 POLVAL_STORE_MIXED_LOADS_TOGETHER,
			    strlen(POLVAL_STORE_MIXED_LOADS_TOGETHER)) == 0)
		{
		    /* If we have a list of what columns are significant */
		    /* for storing together, loop through the product and */
		    /* make sure that the column does match */
		    store_mixed_together = TRUE;
		    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
		    {
			if (sqlIsNull(res, row,
				      misTrim((char *) sqlGetValue(mixpolRes,
							polRow, "rtstr1"))))
			    store_mixed_together = FALSE;

			switch (sqlGetDataType(res,
				     misTrim((char *) sqlGetValue(mixpolRes,
							polRow, "rtstr1"))))
			{
			case COMTYP_INT:
			    if (*(long *) sqlGetValue(res, row,
				     misTrim((char *) sqlGetValue(mixpolRes,
						      polRow, "rtstr1"))) !=
				*(long *) sqlGetValue(res, topRow,
				     misTrim((char *) sqlGetValue(mixpolRes,
							polRow, "rtstr1"))))
				store_mixed_together = FALSE;
			    break;
			case COMTYP_FLOAT:
			    if (*(double *) sqlGetValue(res, row,
				     misTrim((char *) sqlGetValue(mixpolRes,
						      polRow, "rtstr1"))) !=
				*(double *) sqlGetValue(res, topRow,
				     misTrim((char *) sqlGetValue(mixpolRes,
							polRow, "rtstr1"))))
				store_mixed_together = FALSE;
			    break;
			case COMTYP_CHAR:
			    if (strcmp((char *) sqlGetValue(res, row,
				     misTrim((char *) sqlGetValue(mixpolRes,
							polRow, "rtstr1"))),
				       (char *) sqlGetValue(res, topRow,
				     misTrim((char *) sqlGetValue(mixpolRes,
						       polRow, "rtstr1")))))
				store_mixed_together = FALSE;
			    break;
			}
			if (store_mixed_together == FALSE)
			    break;
		    }
		    if (store_mixed_together == FALSE)
			break;
		}

	    }

	    if (store_mixed_together == FALSE &&
		store_mixed_individually == FALSE)	/* Undefined-Return */
	    {
		prd->mixed = TRUE;
		sLog(0, "Mixed Pallet found in trnAllocLoc - Bypassed");
		sqlFreeResults(res);
		CloseTraceFile();
		FreeProdList(prod_list);
		return (eINT_MIXED_LOAD);
	    }
	    else if (store_mixed_together == TRUE)
	    {
		/* fall through to fill out all of the part_info structure
		   elements ... and then we will process normally */
	    }
	    else
		/* store_mixed_individually is TRUE */
	    {
		if (misTrimLen(subnum, SUBNUM_LEN))
		{
		    sLog(0, "Mixed Sub-Load to be processed "
			    "as individual Details!");
		}
		else
		{
		    sLog(0, "Mixed Load to be processed "
			    "as individual Sub-Loads!");
		}
		sqlFreeResults(res);
		CloseTraceFile();
		FreeProdList(prod_list);

		/* select all details and call allocate location */
		if (misTrimLen(dtlnum, DTLNUM_LEN))
		    return (eINT_MIXED_LOAD);
		else if (misTrimLen(subnum, SUBNUM_LEN))
		    sprintf(sqlbuffer,
			    "select dtlnum from invdtl "
			    " where subnum = '%s' ",
			    subnum);
		else
		    sprintf(sqlbuffer,
			    "select subnum from invsub "
			    " where lodnum = '%s' ",
			    lodnum);
		ret_status = sqlExecStr(sqlbuffer, &res);
		if (ret_status != eOK)
		{
		    sqlFreeResults(res);
		    return (ret_status);
		}
		for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
		{
		    memset(dtlnum, 0, sizeof(dtlnum));
		    memset(subnum, 0, sizeof(subnum));
		    memset(lodnum, 0, sizeof(lodnum));
		    if (!sqlIsNull(res, row, "dtlnum"))
			strncpy(dtlnum,
				(char *) sqlGetValue(res, row, "dtlnum"),
				misTrimLen((char *) sqlGetValue(res,
					       row, "dtlnum"), DTLNUM_LEN));
		    else
			strncpy(subnum,
				(char *) sqlGetValue(res, row, "subnum"),
				misTrimLen((char *) sqlGetValue(res,
					       row, "subnum"), SUBNUM_LEN));

		    sprintf(sqlbuffer,
			    "allocate location "
			    " where lodnum = '%s' "
			    "   and subnum = '%s' "
			    "   and dtlnum = '%s' "
			    "   and wh_id  = '%s' ",
			    lodnum, subnum, dtlnum, wh_id);
		    
		    CmdRes = NULL;
		    ret_status = srvInitiateInline(sqlbuffer, &CmdRes);
		    if (ret_status != eOK)
			return (ret_status);
		    tres = srvGetResults(CmdRes);
		    for (trow = sqlGetRow(tres); 
			 trow; trow = sqlGetNextRow(trow))
		    {
			PRODUCT lprd;
			
			memset(&lprd, 0, sizeof(lprd));

			strcpy(lprd.lodnum, 
			       sqlGetString(tres, trow, "lodnum"));
			strcpy(lprd.lodlvl,
			       sqlGetString(tres, trow, "lodlvl"));
                        strcpy(lprd.wh_id,
			       sqlGetString(tres, trow, "wh_id"));
                        
                        /*
                         * changed for PR45852 on [09/27/2005]
                         * the allocate location command won't return the 
                         * wh_id at this time, so just set the wh_id to
                         * the input wh_id as default now; otherwize its value
                         * is NULL, this will made it running to crash
                         *
                         * PR51910 on [11/28/2005]
                         * the allocate location command returns the
                         * wh_id at this time.
                         */
			AddToLocList(sqlGetString(tres, trow, "nxtloc"),
				     LocList, &lprd,
				     sqlGetString(tres, trow, "wh_id")); 
		    }
		    srvFreeMemory(SRVRET_STRUCT, CmdRes);
		}
		sqlFreeResults(res);
		return (ret_status);
	    }

	}

	/* At this point, we want to load up everything as a mixed part */
	/* multi-structure list, or just a single part single-element */

	topRow = sqlGetRow(res);
	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
	    if (row == topRow || prd->mixed == TRUE)
	    {
		if (row != topRow)
		{
		    newprd = NULL;
		    newprd = (PRODUCT *) calloc(1, sizeof(PRODUCT));
		    if (!newprd)
		    {
			sqlFreeResults(res);
			CloseTraceFile();
			FreeProdList(prod_list);
			return (eNO_MEMORY);
		    }
		    prd->next = newprd;
		    prd = newprd;
		    prd->mixed = TRUE;
		}

                /* set wh_id for prd, wh_id is always constant */
                misTrimcpy(prd->wh_id, wh_id, WH_ID_LEN);

		reccnt = *(double *) sqlGetValue(res, row, "reccnt");
	        if(misTrimLen(cur_bldg_id_i, BLDG_ID_LEN) > 0 )
                    misTrimcpy(prd->cur_bldg_id, cur_bldg_id_i, BLDG_ID_LEN);
                else
                    misTrimcpy(prd->cur_bldg_id, 
                            sqlGetString(res, row, "bldg_id"), BLDG_ID_LEN);

		strncpy(prd->prtnum,
		      (char *) sqlGetValue(res, row, "prtnum"), PRTNUM_LEN);
		strncpy(prd->prt_client_id,
		      (char *) sqlGetValue(res, row, "prt_client_id"),
							      CLIENT_ID_LEN);
		strncpy(prd->prtfam, sqlGetString(res, row, "prtfam"), PRTFAM_LEN);

		sLog(0, "Selected Load Info: count=%ld, prtnum='%s', "
		        "prt_client_id='%s', prtfam='%s'",
		        (long) reccnt, prd->prtnum, prd->prt_client_id,
		        prd->prtfam);

		prd->fifwin = sqlGetLong(res, row, "fifwin");
                if (!sqlIsNull(res, row, "timcod"))
                    strncpy(prd->timcod, 
                            sqlGetString(res, row, "timcod"), 
                            TIMCOD_LEN);
                else
                    strncpy(prd->timcod, TIMCOD_DAYS, TIMCOD_LEN);

		strncpy(prd->velzon,
		      (char *) sqlGetValue(res, row, "velzon"), VELZON_LEN);
		strncpy(prd->orgcod,
		      (char *) sqlGetValue(res, row, "orgcod"), ORGCOD_LEN);
		strncpy(prd->revlvl,
		      (char *) sqlGetValue(res, row, "revlvl"), REVLVL_LEN);
		strncpy(prd->supnum,
		      (char *) sqlGetValue(res, row, "supnum"), SUPNUM_LEN);
		strncpy(prd->invsts,
		      (char *) sqlGetValue(res, row, "invsts"), INVSTS_LEN);
		strncpy(prd->lotnum,
		      (char *) sqlGetValue(res, row, "lotnum"), LOTNUM_LEN);
		if (!sqlIsNull(res, row, "iftpcod"))
		    strncpy(prd->ftpcod,
		     (char *) sqlGetValue(res, row, "iftpcod"), FTPCOD_LEN);
		else
		    strncpy(prd->ftpcod,
		      (char *) sqlGetValue(res, row, "ftpcod"), FTPCOD_LEN);
		if (!sqlIsNull(res, row, "untpal"))
		    prd->untpal = sqlGetLong(res, row, "untpal");
		prd->untcas = sqlGetLong(res, row, "untcas");
		prd->untpak = sqlGetLong(res, row, "untpak");

		strncpy(prd->fifdte,
			sqlGetString(res, row, "minfif"),
			misTrimLen(sqlGetValue(res, row, "minfif"), 30));
		strncpy(prd->curare,
		        (char *) sqlGetValue(res, row, "arecod"), ARECOD_LEN);

		strncpy(prd->curzon, 
			(char *) sqlGetValue(res, row, "wrkzon"), WRKZON_LEN);

                if (!sqlIsNull(res, row, "distro_id"))
                    strncpy(prd->distro_id,
                            sqlGetString(res,row, "distro_id"), DISTRO_ID_LEN);

                prd->asset_track_enabled = asset_track_enabled;

		if (misTrimLen(dtlnum, DTLNUM_LEN))
		{
		    strncpy(prd->lodnum, dtlnum, misTrimLen(dtlnum, DTLNUM_LEN));
		    strcpy(prd->lodlvl, LODLVL_DETAIL);
		}
		else if (misTrimLen(subnum, SUBNUM_LEN))
		{
                    /* If the identifier is a subnum and asset tracking is enabled
                     * then get the asset_typ from invsub and populate the prd pointer
                     * we need another select here to get the asset_typon invsub.
                     * since the select above will get the asset typ from INVLOD.
                     */
		    strncpy(prd->lodnum, subnum, misTrimLen(subnum, SUBNUM_LEN));
		    strcpy(prd->lodlvl, LODLVL_SUBLOAD);

                    if (asset_track_enabled == BOOLEAN_TRUE)
                    {
                        sprintf(sqlbuffer, 
                            " select asset_typ.asset_typ, "
                            "        asset_typ.asset_len, "
                            "        asset_typ.asset_wid, "
                            "        asset_typ.asset_hgt, "
                            "        decode(asset_typ.container_flg, "
                            "               0, 0, 1, asset_typ.max_vol) max_vol "
                            "   from invsub, "
                            "        asset_typ "
                            "  where invsub.asset_typ = asset_typ.asset_typ "
                            "    and invsub.subnum = '%s' ", subnum);
                        ret_status = sqlExecStr(sqlbuffer, &subassetres);
                       if (ret_status == eOK)
                       {
                           subassetrow = sqlGetRow(subassetres);
                           strncpy(prd->asset_typ, 
                               sqlGetString(subassetres, 
                                            subassetrow, 
                                            "asset_typ"), ASSET_TYP_LEN);
                           prd->asset_len = sqlGetFloat(subassetres, 
                                                        subassetrow, 
                                                        "asset_len");
                           prd->asset_wid = sqlGetFloat(subassetres, 
                                                        subassetrow, 
                                                        "asset_wid");
                           prd->asset_hgt = sqlGetFloat(subassetres, 
                                                        subassetrow, 
                                                        "asset_hgt");
                           prd->asset_max_vol = sqlGetFloat(subassetres, 
                                                            subassetrow, 
                                                            "max_vol");
                       }
                       sqlFreeResults(subassetres);
                    }
		}
		else
		{
		    strncpy(prd->lodnum, lodnum, misTrimLen(lodnum, LODNUM_LEN));
		    strcpy(prd->lodlvl, LODLVL_LOAD);

		    /* 
		     * if ASSET-TRACKING  is enabled, 
		     * get asset dimension and store it in prd.
		     *
		     * Note:
		     * 1) For mixed load, we have a prd list to store multiple 
		     * products, we assign asset_typ/asset_len/asset_wid/asset_hgt
		     * to each prd node on the list. 
		     * 
		     * 2) If ASSET-TRACKING is not enabled,
		     * asset_typ is empty.
		     */
                    if (asset_track_enabled == BOOLEAN_TRUE)
                    {
                        if (!sqlIsNull(res, row, "asset_typ"));
                        {
                            strncpy(prd->asset_typ,
                                    sqlGetString(res, row, "asset_typ"),
                                    ASSET_TYP_LEN);

                            prd->asset_len = sqlGetFloat(res, row,
                                                        "asset_len");
                            prd->asset_wid = sqlGetFloat(res, row, 
                                                         "asset_wid");
                            prd->asset_hgt = sqlGetFloat(res, row,
                                                         "asset_hgt");
                            prd->asset_max_vol = sqlGetFloat(res, row, 
                                                             "max_vol");
                        }
                    }

		}
	    }
	    prd->totqty += sqlGetLong(res, row, "sumqty");
	    if (xdkref_i && misTrimLen(xdkref_i, XDKREF_LEN))
		misTrimcpy(prd->xdkref, xdkref_i, XDKREF_LEN);
	    prd->xdkqty = xdkqty_i;

	    /*
	     * Copy the invmov_typ to the prd list, so that the 
             * GetLocation function can get the invmov data from the 
             * prd_list and insert it into the invmov table
	     * The invmov_typ indicates why the inventory is being moved.
	     */
	    strncpy(prd->invmov_typ, invmov_typ_i, 
                                     misTrimLen(invmov_typ_i, INVMOV_TYP_LEN));
	}
	prd = prod_list;
    }
    sqlFreeResults(res);

    /* At this point, we may have a single element product (prd) structure */
    /* for homogeneous pallets (prd->next will be null) ... or ... */
    /* we may have a linked list of all of the different stuff on the load */
    /* These next couple of pieces of data can just go in the first element */
    /* cause that is what we alway start with anyway */
    if (arecod && misTrimLen(arecod, ARECOD_LEN))
        strncpy(prd->reqare, arecod, ARECOD_LEN);

    if (wh_id && misTrimLen(wh_id, WH_ID_LEN))
        strncpy(prd->wh_id, wh_id, WH_ID_LEN);

    if (strcmp(wrkzon, "") != 0)
	strncpy(prd->reqzon, wrkzon, WRKZON_LEN);

    /*
       ** Get the Footprint Information.
     */
    for (prd = prod_list; prd; prd = prd->next)
    {
	if ((errcode = GetFootprint(prd)) != eOK)
	{
	    CloseTraceFile();
	    FreeProdList(prod_list);

	    return (errcode);
	}
        if (prd->untcas > 0.0) 
        {
	    prd->loclen = prd->caslen * ((double) prd->totqty / prd->untcas);
	    prd->locqvl = ((prd->caslen * prd->cashgt * prd->caswid) *
		       ((double) prd->totqty / prd->untcas));
        }
        else 
        {
	    prd->loclen = 0.0;
	    prd->locqvl = 0.0;
        }
    }
    prd = prod_list;

    /*
    ** If a specific location is requested, attempt to get it.
    */
    if (stoloc && misTrimLen(stoloc, STOLOC_LEN))
    {

        sprintf(sqlbuffer,
            "select arecod, wh_id from locmst where stoloc = '%s' "
            " and wh_id = '%s'",
            stoloc, wh_id);
        errcode = sqlExecStr(sqlbuffer, &res);
        if (errcode != eOK)
        {
            sqlFreeResults(res);
            CloseTraceFile();
            FreeProdList(prod_list);

            return (errcode);
        }

        row = sqlGetRow(res);

        /* When trying a specific location, we will more likely have */
        /* someone grab it from underneath us ... so, loop while we */
        /* get DB_DEADLOCK back from GetLocation */
        ret_status = eDB_DEADLOCK;
        while (ret_status == eDB_DEADLOCK)
        {
            ret_status = GetLocationRange(prd,
                          sqlGetValue(res, row, "arecod"),
                          stoloc,
                          stoloc,
                          LocList,
                          sqlGetValue(res, row, "wh_id"),
                          isPipEnabled,
                          lodhgt);
        }
        if (ret_status != eOK)
        {
            sqlFreeResults(res);
            sLog(0, "(%d) Unable to grab requested location: %s/%s",
                ret_status, wh_id, stoloc);
            CloseTraceFile();
            FreeProdList(prod_list);

            /* We want to return the status from GetLocationRange */
            /* because we called it with a single location. This way */
            /* the user will have a better chance of figuring out */
            /* why they could not allocate this location */
            return(ret_status);
        }
        CloseTraceFile();
        FreeProdList(prod_list);

        sqlFreeResults(res);

        return (eOK);
    }


    /*
     * At this point, we know that this was not a request for a
     * specific location.  We should follow the rules as below
     * to allocate a location.
     *   1) if the inventory is NOT for a distro, system should
     *      search assigned/non-assigned locations defined as 
     *      storage policy.
     *   2) if the inventory is for a distro, system should 
     *      search defined put-to-store areas/locations for the
     *      store (customer) where the distro is going. For more
     *      info see WMD-37244
     */

    /* WMD-37244
     *
     * If the inventory is for a distro, we should search put-to-store
     * areas/locations.
     *
     * System currently only supports allocating location for individual
     * details, instead of the whole sub or load. Because details for
     * different stores may go to different put-to-store locations even
     * though they are on the same sub or load.
     * In distro functionality system will call "allocate location" for
     * each detail on the load once. In this situation the prd_list
     * must have single node, just check prd->distro_id to see if it's
     * for a distro.
     *
     * Note:
     * If allocate a location for a whole load which contains details
     * for multiple distros, this command maybe can NOT work correctly.
     */
    if (strlen(prd->distro_id))
    {
        /* if an area is specified, then ignore the put-to-store area/location
         * settings on the store. This may happens when allocate a location
         * in indirect cross dock area for a distro inventory using cross
         * docking functionality.
         *
         * Otherwise, get all put-to-store areas/locations defined for the
         * store(customer), take dept_cod into consideration.
         * For each record, if only area is defined, all locations
         * in this area are candidates, if both area and location
         * are defined,only this location is candidate.
         */

        if (misTrimLen(prd->reqare, ARECOD_LEN) > 0)
        {
            misTrc(T_FLOW, "An area %s was specified, ignore put-to-store "
                   "area/location settings on the store.", prd->reqare);

            sprintf(sqlbuffer,
                    " select NULL rtstr2, 0 rtnum1, 0 rtnum2, 0 rtflt2, "
                    "        NULL stoloc, "
                    "        aremst.* "
                    "   from aremst "
                    "  where arecod = '%s' "
                    "    and wh_id = '%s' "
                    "    and praflg = '%d' ",
                    prd->reqare,
                    wh_id,
                    BOOLEAN_FALSE);

            ret_status = sqlExecStr(sqlbuffer, &distroRes);

            if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
            {
                sqlFreeResults(distroRes);
                FreeProdList(prod_list);
                CloseTraceFile();
                return (ret_status);
            }

            distroRow = sqlGetRow(distroRes);
            memset(&area, 0, sizeof(area));
            SetupArea(&area, distroRes, distroRow);
            errcode = ProcessDistroPutToArea(prd,
                                             LocList,
                                             area,
                                             isPipEnabled,
                                             lodhgt);

            /* if found a location (errcode == eOK) or
             * error occurred, just return.
             */
            if (errcode != eDB_NO_ROWS_AFFECTED)
            {
                FreeProdList(prod_list);
                if (pass_stoloc_list) free (pass_stoloc_list);
                FreeAisleList();
                if (errcode != eOK)
                {
                    trnFreeStolocList(*LocList);
                    *LocList = NULL;
                }
                CloseTraceFile();
                sqlFreeResults(distroRes);
                return (errcode);
            }

            sqlFreeResults(distroRes);
            distroRes = NULL;
            distroRow = NULL;
        }
        else
        {
            misTrc(T_FLOW, "Consider put-to-store area/location "
                           "settings on the store.");

            /* In the query, we should filter rows from cst_wh_put_to_loc based
               on the load level (one of lodflg, subflg and dtlflg). We
               traverse prod_list to determine max untpal, max untcas and total
               load quantity. We set dtlflg, subflg or lodflg depending on
               whether the total load quantity is < untcas, >= untcas but
               < untpal, otherwise, respectively. */
            prd = prod_list;
            max_untpal = prd->untpal;
            max_untcas = prd->untcas;
            load_qty = prd->totqty;
            while (prd->next)
            {
                prd = prd->next;
                if (prd->untpal > max_untpal)
                    max_untpal = prd->untpal;
                if (prd->untcas > max_untcas)
                    max_untcas = prd->untcas;
                load_qty += prd->totqty;
            }
            prd = prod_list;
            memset(tmpbuf, 0, sizeof(tmpbuf));
            if (load_qty < max_untcas)
            {
                strcpy(tmpbuf, " and dtlflg = 1");
                distro_lodlvl = LODLVL_DETAIL;
            }
            else if (load_qty >= max_untcas && load_qty < max_untpal)
            {
                strcpy(tmpbuf, " and subflg = 1");
                distro_lodlvl = LODLVL_SUBLOAD;
            }
            else
            {
                strcpy(tmpbuf, " and lodflg = 1");
                distro_lodlvl = LODLVL_LOAD;
            }

            sprintf(sqlbuffer,
                    "  list put to location criteria for distro "
                    " where distro_id = '%s' "
                    "   and client_id = '%s'"
                    "   and wh_id = '%s' "
                    "   %s " ,
                    prd->distro_id,
                    prd->prt_client_id,
                    wh_id,
                    tmpbuf);
            CmdRes = NULL;
            ret_status = srvInitiateCommand(sqlbuffer, &CmdRes);

            if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
            {
                if (CmdRes != NULL)
                    srvFreeMemory(SRVRET_STRUCT, CmdRes);
                CmdRes = NULL;
                FreeProdList(prod_list);
                CloseTraceFile();

                return (ret_status);
            }
            else if (ret_status == eOK)
            {
                res = srvGetResults(CmdRes);
                distroRes = NULL;
                ret_status = ProcessDistroPutToConfig(prd, res, &distroRes);
                srvFreeMemory(SRVRET_STRUCT, CmdRes);
                CmdRes = NULL;
                res = NULL;
                if (ret_status != eOK)
                {
                    FreeProdList(prod_list);
                    CloseTraceFile();
                    return (ret_status);
                }

                for (distroRow = sqlGetRow(distroRes); distroRow;
                     distroRow = sqlGetNextRow(distroRow))
                {
                    /* If both stoloc and arecod were defined, check to see
                     * if the location is suitable.
                     * If only arecod was defined, check to see if any 
                     * location in the area is suitable.
                     */
                    if(!sqlIsNull(distroRes, distroRow, "stoloc"))
                    {
                        errcode = ProcessDistroPutToLocation(prd,
                            LocList,
                            sqlGetString(distroRes, distroRow, "arecod"),
                            sqlGetString(distroRes, distroRow, "stoloc"),
                            sqlGetString(distroRes, distroRow, "wh_id"),
                            isPipEnabled,
                            lodhgt);
                    }
                    else /* Only area is defined */
                    {
                        memset(&area, 0, sizeof(area));
                        SetupArea(&area, distroRes, distroRow);
                        errcode = FindLocationWithMatchAttributes(prd,
                            LocList,
                            &area,
                            isPipEnabled,
                            lodhgt,
                            sqlGetLong(distroRes, distroRow, "srtseq"),
                            distro_lodlvl);
                    }

                    /* if found a location (errcode == eOK) or
                     * error occurred, just return.
                     */
                    if ((errcode == eOK && *LocList &&
                            misTrimLen((*LocList)->nxtloc, STOLOC_LEN)) ||
                                (errcode != eOK 
                                 && errcode != eDB_NO_ROWS_AFFECTED
                                 && errcode != 
                                    eINT_LOCATION_ALREADY_ASSIGNED_TO_OTHER))
                    {
                        FreeProdList(prod_list);
                        if (pass_stoloc_list) free (pass_stoloc_list);
                        FreeAisleList();
                        if (errcode != eOK)
                        {
                            trnFreeStolocList(*LocList);
                            *LocList = NULL;
                        }
                        CloseTraceFile();
                        sqlFreeResults(distroRes);
                        return (errcode);
                    }
                }
                
                sqlFreeResults(distroRes);
                distroRes = NULL;
                distroRow = NULL;
            }
        }

        /* 
         * if get here, no suitable location was found, 
         * So ignore its distro attribution, use the normal 
         * method to find a storage location
         */
        misTrc(T_FLOW, "No suitable put-to-store location found "
                       "for distro, continuing search for a regular stoloc");
    }

    /*
     * At this point, we know that this was not a request 
     * for a specific location.  And the inventory is not assigned to 
     * a distro inventory We now need to look at assigned locations
     * and then preferred areas.
     * 
     * First we will load the available buildings that we may look for a 
     * location in using Get Storage Building Sequences.  This gets a set of 
     * reachable buildings based on the current building in the sequence
     * order.
     * If no records are defined for the given source building, we assume
     * that a building is only reachable to itself. However, once at least
     * one dest building is defined for that source building, all reachable
     * buildings must be defined for that source building, including the 
     * source building itself.
     * 
     * Now we loop through the buildings in order and ... 
     *    First look for assigned locations
     *    Then use the area-by-area processing logic.
     */
    
    currentBldgOnly = 0;
    CmdRes = NULL;
    sprintf(sqlbuffer,
        "get storage building sequences"
        " where src_bldg_id = '%s' "
        "   and wh_id = '%s' ",
        prd->cur_bldg_id,
        prd->wh_id);
   
    ret_status = srvInitiateInline(sqlbuffer, &CmdRes);
    if (ret_status != eOK)
    {
        if (ret_status == eDB_NO_ROWS_AFFECTED)
        {    
            currentBldgOnly = 1;
            bldgCnt = 1;
        }    
        else
        {    
            CloseTraceFile();
            FreeProdList(prod_list);
            return (ret_status);
        }		    
    }	
    else
    {
    bldgRes = srvGetResults(CmdRes);
    bldgCnt = sqlGetNumRows(bldgRes);
        bldgRow = sqlGetRow(bldgRes);
    }
    
    for (ii = 0; ii < bldgCnt; ii++) 
    {
        if (currentBldgOnly)
        {
            strcpy(dst_bldg_id, prd->cur_bldg_id);
        }
        else
        {    
            misTrimcpy(dst_bldg_id, 
                       sqlGetString(bldgRes, bldgRow, "dst_bldg_id"),
                       BLDG_ID_LEN);
            bldgRow = sqlGetNextRow(bldgRow);
        }		    
   
        pass_stoloc_list = NULL;  
   
        /* Load Assigned location list and Non-Assigned area list */
   
        errcode = LoadAssignedLocationSearchList(&Ptr_Assign_Loc,
                                                 prd,
                                                 dst_bldg_id,
                                                 wh_id);
        if (errcode != eOK && errcode != eDB_NO_ROWS_AFFECTED)
        {
            FreeProdList(prod_list);
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
            FreeAisleList();
            if (Ptr_Assign_Loc) srvFreeMemory(SRVRET_STRUCT, Ptr_Assign_Loc);
            FreeAreaList(area_list);
            CloseTraceFile();
            return (errcode);
        }
   
        errcode = LoadNonAssignedAreaSearchList(&area_list,
                                                prd,
                                                dst_bldg_id,
                                                wh_id);
        if (errcode != eOK && errcode != eDB_NO_ROWS_AFFECTED)
        {
            FreeProdList(prod_list);
   
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
            
            if (Ptr_Assign_Loc) srvFreeMemory(SRVRET_STRUCT, Ptr_Assign_Loc);
            FreeAreaList(area_list);
            FreeAisleList();
            
            CloseTraceFile();
            return (errcode);
        }
   
        /*
         * PR 56510
         *
         * Now we need to know whether we should search
         *    Assigned locations first then Non-Assigned locations
         *  or
         *    Non-Assigned locations first then Assigned locations
         */
        errcode = CheckLocationSearchSequence(prd,
                                              wh_id,
                                              fifo_skip_asg_loc_flg,
                                              area_list,
                                              &search_assigned_location_first);
        if (errcode != eOK)
        {
            FreeProdList(prod_list);
   
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
            
            if (Ptr_Assign_Loc) srvFreeMemory(SRVRET_STRUCT, Ptr_Assign_Loc);
            FreeAreaList(area_list);
            FreeAisleList();
            
            CloseTraceFile();
            return (errcode);
        }
        
   
        /*
         * If we find 'older' inventory exits in Non-Assigned Locations and
         * inventory which falls in fifo-window exists in Assigned Locations, 
         * we use the same logic as before.
         *
         * If we find 'older' inventory only exists in Non-Assigned Locations,
         * and no inventory which falls in fifo-window exits in assigned locations, 
         * we return search the non-assigned location firdt to let system use 
         * the Non-Assigned Location.
         *
         * If we find no 'older' inventory exists in Non-Assigned Locations, 
         * we use the same logic as before.
         */ 
        if (search_assigned_location_first)
        {
            /* Search Assigned locations first */
            errcode = ProcessAssignedLocations(prd, LocList, Ptr_Assign_Loc,
                                               isPipEnabled, lodhgt);
   
            /* If no Assigned location available, 
             * then search non-assigned location.
             */ 
            if (errcode == eDB_NO_ROWS_AFFECTED)
                errcode = ProcessNonAssignedLocations(prd, LocList, area_list,
                                                      isPipEnabled, lodhgt);
        }
        else
        {
            /* Search Non-Assigned location first */ 
            errcode = ProcessNonAssignedLocations(prd, LocList, area_list, 
                                                  isPipEnabled, lodhgt);
   
            /* If no Non-Assigned location available, 
             * then search Assigned location.
             */
            if (errcode == eDB_NO_ROWS_AFFECTED)
                errcode = ProcessAssignedLocations(prd,
                                                   LocList,Ptr_Assign_Loc,
                                                   isPipEnabled, lodhgt);
        }
        
        /*
         * If found a location(eOK) or got an error, release memory and return. 
         */ 
        if (errcode != eDB_NO_ROWS_AFFECTED)
        {
            FreeProdList(prod_list);
   
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
            if (pass_stoloc_list) free (pass_stoloc_list);
            
            if (Ptr_Assign_Loc) srvFreeMemory(SRVRET_STRUCT, Ptr_Assign_Loc);
            FreeAreaList(area_list);
            FreeAisleList();
            
            CloseTraceFile();
            return (errcode);
        }
   
        /* Free the pass_stoloc_list */
        if (pass_stoloc_list) free (pass_stoloc_list);
   
        /* Free Assigned location list and Non-Assigned location list */        
        if (Ptr_Assign_Loc) srvFreeMemory(SRVRET_STRUCT, Ptr_Assign_Loc);
        Ptr_Assign_Loc = NULL;
        FreeAreaList(area_list);
        /* Free the aisle_list */
        FreeAisleList();
    }    
   
    FreeProdList(prod_list);
    
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    CloseTraceFile();
    
    misTrc(T_FLOW, "No suitable location found for inventory, exiting.");
    return (eNO_SUITABLE_LOCATION);
}



