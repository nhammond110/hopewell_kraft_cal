static const char *rcsid = "$Id: intAllocateInventory.c 168672 2008-09-16 08:54:28Z kzhao $";
/*#START***********************************************************************
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "trnlib.h"

#include "intlib.h"

/*
 * HISTORY
 * JJS 02/19/2009 - (or thereabouts) Creation.  Original version just
 *                  here as a stub so we can customize trnAllocInv.c.
 * JJS 06/10/2009 - Changes to set pckwrk.vc_from_rpl flag
 *
 */

LIBEXPORT 
RETURN_STRUCT *varAllocateInventory(char *pcktyp_i, 
                    char *prtnum_i, 
                    char *prt_client_id_i, 
                    long *pckqty_i,
                    double *pck_catch_qty_i,
                    char *orgcod_i, char *revlvl_i, 
                    char *lotnum_i, char *invsts_i,
                    char *invsts_prg_i,
                    char *schbat_i,
                    char *ship_id_i, char *ship_line_id_i, 
                    char *wkonum_i, char *wkorev_i,
                    char *wkolin_i,
                    char *carcod_i, char *srvlvl_i,
                    char *client_id_i, 
                    char *ordnum_i, 
                    char *stcust_i, 
                    char *rtcust_i, 
                    char *ordlin_i, 
                    char *ordsln_i, 
                    char *concod_i, 
                    char *segqty_i,
                    char *stoloc_i, 
                    char *lodnum_i, 
                    char *dstare_i, 
                    char *dstloc_i,
                    char *srcare_i, 
                    char *pcklvl_i, 
                    char *pcksts_i,
                    char *pcksts_uom_i,
                    moca_bool_t *splflg_i, 
                    moca_bool_t *ovrflg_i,
                    long *untcas_i, 
                    long *untpak_i, 
                    long *untpal_i,
                    long *min_shelf_hrs_i,
                    moca_bool_t *frsflg_i,
                    char *frsdte_i,
                    char *trace_suffix_i,
                    char *pipcod_i,
                    long *skip_invlkp_i,
                    long *alloc_loc_flg_i,
                    char *wh_id_i,
                    char *wrktyp_i,
                    moca_bool_t *pipflg_i,
                    char *cmbcod_i,
                    char *alc_search_path_i,
                    char *supnum_i,
                    moca_bool_t *vc_from_rpl_i)
{
    long ret_status;
    PCKWRK_DATA *Picks, *ptr;
    RETURN_STRUCT *CurPtr;
    mocaDataRow *row;
    long seqnum;
    char pcktyp[100];
    moca_bool_t splflg;
    char juldte[50];
    char frsdte[MOCA_STD_DATE_LEN + 1];
    char wh_id[WH_ID_LEN + 1];
    long untcas, untpak, untpal, skip_invlkp, alloc_loc_flg;
    double pck_catch_qty = 0;
    moca_bool_t vc_from_rpl;

    row = 0;

    memset(pcktyp, 0, sizeof(pcktyp));
    memset(wh_id, 0, sizeof(wh_id));

    if (!wh_id_i || !misTrimLen(wh_id_i, WH_ID_LEN))
        return (APPMissingArg("wh_id"));
    else
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

    if (pcktyp_i && misTrimLen(pcktyp_i, sizeof(pcktyp) - 1))
        strncpy(pcktyp, pcktyp_i, misTrimLen(pcktyp_i, sizeof(pcktyp) - 1));

    if (splflg_i)
        splflg = *splflg_i;
    else
        splflg = BOOLEAN_TRUE;

    if (skip_invlkp_i)
        skip_invlkp = *skip_invlkp_i;
    else
        skip_invlkp = BOOLEAN_FALSE;

    if (alloc_loc_flg_i)
        alloc_loc_flg = *alloc_loc_flg_i;
    else
        alloc_loc_flg = BOOLEAN_FALSE;

    /*  
     * Verify that something is passed in, otherwise default to zero.
     */
    if (untcas_i && *untcas_i > -1)
        untcas = *untcas_i;
    else
        untcas = 0;

    if (untpak_i && *untpak_i > -1)
        untpak = *untpak_i;
    else
        untpak = 0;

    if (untpal_i && *untpal_i > -1)
        untpal = *untpal_i;
    else
        untpal = 0;

    if (!pckqty_i || *pckqty_i <= 0)
        return(srvResults(eOK, NULL));
        
    if(pck_catch_qty_i)
    {
        pck_catch_qty = *pck_catch_qty_i;
    }

    /* Setup the freshness date parameter */
    memset(frsdte, 0, sizeof(frsdte));
    if (frsdte_i && misTrimLen(frsdte_i, MOCA_STD_DATE_LEN))
    {
        misTrimcpy(frsdte, frsdte_i, MOCA_STD_DATE_LEN);
    }

    if (vc_from_rpl_i)
        vc_from_rpl = *vc_from_rpl_i;
    else
        vc_from_rpl = BOOLEAN_FALSE;


    Picks = NULL;
    ret_status = trnAllocateInventory(pcktyp_i, 
                    prtnum_i, 
                    prt_client_id_i,
                    *pckqty_i,
                    pck_catch_qty,
                    orgcod_i, revlvl_i, lotnum_i,
                    invsts_i ? invsts_i : "",
                    invsts_prg_i ? invsts_prg_i : "", 
                    schbat_i,
                    ship_id_i, ship_line_id_i, 
                    wkonum_i, wkorev_i, wkolin_i,
                    carcod_i, srvlvl_i,
                    client_id_i, ordnum_i, 
                    stcust_i, rtcust_i,
                    ordlin_i, ordsln_i,
                    concod_i, segqty_i,
                    stoloc_i, lodnum_i, 
                    dstare_i, dstloc_i,
                    &Picks,   
                    srcare_i,
                    pcklvl_i ? pcklvl_i : "",
                    pcksts_i ? pcksts_i : "",
                    pcksts_uom_i ? pcksts_uom_i : "", 
                    splflg, ovrflg_i,
                    untcas, untpak, untpal,
                    min_shelf_hrs_i ? *min_shelf_hrs_i : -1,
                    frsflg_i ? *frsflg_i : 0,
                    frsdte,
                    trace_suffix_i,
                    pipcod_i,
                    skip_invlkp,
                    alloc_loc_flg,
                    wh_id,
                    wrktyp_i,
                    pipflg_i,
                    cmbcod_i,
                    alc_search_path_i,
                    supnum_i,
                    vc_from_rpl);

    if (ret_status != eOK)
    {
        /* Since the replenishment manager takes care of 
           generating replenishments, etc, we return a 
           good status even though we didn't actually
           allocate anything for an order...this is only
           for this mode (prevents rollback of rplwrk)  JRO */

        if ((ret_status == eDB_NO_ROWS_AFFECTED ||
             ret_status == eINT_NO_INVENTORY) &&
            (misCiStrcmp(pcktyp, ALLOCATE_PICKNREPLEN) == 0 ||
             misCiStrcmp(pcktyp, ALLOCATE_PICKNREPLENNSHIP) == 0))
            return (srvResults(eOK, NULL));
        else
            return (APPError(ret_status)); 
    }

    CurPtr = NULL;
    for (ptr = Picks; ptr; ptr = ptr->next)
    {
        seqnum = 0;

        sprintf(juldte, "%ld", ptr->julian_fifdte);

        if (CurPtr == NULL)
        {
            CurPtr = 
            srvResultsInit(eOK,
                       "prtnum", COMTYP_CHAR, PRTNUM_LEN,
                       "prt_client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                       "orgcod", COMTYP_CHAR, ORGCOD_LEN,
                       "revlvl", COMTYP_CHAR, REVLVL_LEN,
                       "supnum", COMTYP_CHAR, SUPNUM_LEN,
                       "lotnum", COMTYP_CHAR, LOTNUM_LEN,
                       "invsts", COMTYP_CHAR, INVSTS_LEN,
                       "invsts_prg", COMTYP_CHAR, INVSTS_PRG_LEN,
                       "untcas", COMTYP_INT, sizeof(long),
                       "untpak", COMTYP_INT, sizeof(long),
                       "lodlvl", COMTYP_CHAR, LODLVL_LEN,
                       "pckqty", COMTYP_INT, sizeof(long),
                       "pck_catch_qty", COMTYP_FLOAT, sizeof(double),
                       "srcloc", COMTYP_CHAR, STOLOC_LEN,
                       "srcare", COMTYP_CHAR, ARECOD_LEN,
                       "wrkref", COMTYP_CHAR, WRKREF_LEN,
                       "schbat", COMTYP_CHAR, SCHBAT_LEN,
                       "cmbcod", COMTYP_CHAR, CMBCOD_LEN,
                       "concod", COMTYP_CHAR, CONCOD_LEN,
                       "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                       "ordnum", COMTYP_CHAR, ORDNUM_LEN,
                       "ordlin", COMTYP_CHAR, ORDLIN_LEN,
                       "ordsln", COMTYP_CHAR, ORDSLN_LEN,
                       "ship_id", COMTYP_CHAR, SHIP_ID_LEN,
                       "ship_line_id", COMTYP_CHAR, SHIP_LINE_ID_LEN,
                       "wkonum", COMTYP_CHAR, WKONUM_LEN,
                       "wkorev", COMTYP_CHAR, WKOREV_LEN,
                       "wkolin", COMTYP_CHAR, WKOLIN_LEN,
                       "fifdte", COMTYP_CHAR, 10,
                       "pipflg", COMTYP_INT, sizeof(long),
                       "frsflg", COMTYP_INT, sizeof(long),
                       "min_shelf_hrs", COMTYP_INT, sizeof(long),
                       "asset_typ", COMTYP_CHAR, ASSET_TYP_LEN,
                       "alc_search_path", COMTYP_CHAR, ALC_SEARCH_PATH_LEN,
                       "wh_id",  COMTYP_CHAR, WH_ID_LEN,
                       NULL);
        }
        
        srvResultsAdd(CurPtr,
                  ptr->prtnum,
                  ptr->prt_client_id,
                  ptr->orgcod,
                  ptr->revlvl,
                  ptr->supnum,
                  ptr->lotnum,
                  ptr->invsts,
                  ptr->invsts_prg,
                  ptr->untcas,
                  ptr->untpak,
                  ptr->lodlvl,
                  ptr->pckqty,
                  ptr->pck_catch_qty,
                  ptr->srcloc,
                  ptr->srcare,
                  ptr->wrkref,
                  ptr->schbat,
                  ptr->cmbcod,
                  ptr->concod,
                  ptr->client_id,
                  ptr->ordnum,
                  ptr->ordlin,
                  ptr->ordsln,
                  ptr->ship_id,
                  ptr->ship_line_id,
                  ptr->wkonum,
                  ptr->wkorev,
                  ptr->wkolin,
                  juldte,
                  ptr->pipflg,
                  ptr->frsflg,
                  ptr->min_shelf_hrs,
                  ptr->asset_typ,
                  ptr->alc_search_path,
                  ptr->wh_id);
    }

    trnFreePickList(Picks);
    if (CurPtr == NULL)
    {
        /* see above comment regarding replenishment manager... */
            /* here we give an error message only when we pick and ship */

        if (misCiStrcmp(pcktyp, ALLOCATE_PICKNREPLEN) == 0 ||
            misCiStrcmp(pcktyp, ALLOCATE_PICKNREPLENNSHIP) == 0)
            CurPtr = srvResults(eOK, NULL);
        else if (misCiStrcmp(pcktyp, ALLOCATE_PICKNSHIP) == 0)
                CurPtr = srvResults(eINT_NO_INVENTORY, NULL);
        else
            CurPtr = srvResults(eDB_NO_ROWS_AFFECTED, NULL);
    }
    return (CurPtr);
}

