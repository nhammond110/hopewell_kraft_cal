static char *rcsid = "$Id: trnDeallocate.c 158326 2008-06-03 14:58:20Z pflanzer $";
/*#START***********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/
#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "trnlib.h"

/*
 * HISTORY
 * JJS 08/17/2009 - Don't do "for update" on locmst for certain areas
 *                  (specifically CONV - the conveyor and PALBLD) since
 *                  this is causing unnecessary database locking.  We
 *                  don't care about managing QVL in areas that are
 *                  essentially of infinite size.  Note that a similar
 *                  change was made in pick release in "get pick release
 *                  locations for area".
 *
 */


long trnDeallocateInventory(char *wrkref_i)
{
    long ret_status;
    mocaDataRes *res, *tres;
    mocaDataRow *trow, *row;
    long inpqty_update;
    long oviqty_update;
    char buffer[2000];
    moca_bool_t xdkflg;
    char pending_linsts[20];

    char seqnum_clause[200];
    char where_clause[1000];
    char dtl_table[30];
    char catch_qty_clause[256];

    /*
     * Do a direct join to pckwrk twice, because it is faster for
     * performance than doing an in clause where we select from 
     * pckwrk.
     */

    sprintf(buffer,
	    "select pw.prtnum, pw.prt_client_id, pw.orgcod, "
	    "       pw.lotnum, pw.revlvl, "
	    "       pw.invsts, pw.untcas, pw.untpak, "
	    "       pw.pckqty-pw.appqty remqty, "
	    "       pw.pck_catch_qty - pw.app_catch_qty rem_catch_qty, "
	    "       pw.wrkref, pw.ship_id, pw.ship_line_id, "
	    "       pw.client_id, pw.ordnum, pw.ordlin, pw.ordsln, "
	    "       pw.srcloc, pw.srcare, pw.cmbcod, pw.prt_client_id, "
	    "       pw.wrktyp, pw.wkonum, pw.wkorev, pw.wkolin, pw.pipflg, "
	    "       pw.wh_id "
	    "  from pckwrk pw, "
            "       pckwrk pw2 "
            " where pw2.cmbcod = pw.cmbcod "
            "   and pw2.wrkref = '%s' "
	    "   and pw.appqty < pw.pckqty ",
	    wrkref_i);

    ret_status = sqlExecStr(buffer, &res);

    if (eOK != ret_status)
    {
	sqlFreeResults(res);
	misTrc(T_FLOW, 
	       "FATAL - Select against pckwrk failed...nothing to do!");
	return(ret_status);
    }
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        /* This check is because pckwrk's that have the pipflg set
         * have not incremented the invsum.comqty or locmst.pndqty yet.  */
        if((sqlIsNull(res, row, "pipflg") ||
            (sqlGetLong(res, row, "pipflg") != 1)) 
           &&
           (sqlIsNull(res, row, "rem_catch_qty") ||
            (sqlGetFloat(res, row, "rem_catch_qty") == 0)))
        {
	    sprintf(buffer,
                    "update invsum "
                    "   set comqty = comqty - %d "
                    " where stoloc = '%s' "
                    "   and arecod = '%s' "
                    "   and prtnum = '%s' "
                    "   and prt_client_id = '%s' "
                    "   and wh_id = '%s' ",
                    sqlGetLong(res, row, "remqty"),
                    sqlGetString(res, row, "srcloc"),
                    sqlGetString(res, row, "srcare"),
                    sqlGetString(res, row, "prtnum"),
                    sqlGetString(res, row, "prt_client_id"),
                    sqlGetString(res, row, "wh_id"));
	    ret_status = sqlExecStr(buffer, NULL);
	    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
	    {
	        sqlFreeResults(res);
	        return(ret_status);
	    }
	    else if (eDB_NO_ROWS_AFFECTED == ret_status)
	    {
		misTrc(T_FLOW,
		       "Failed to update invsum - continuing");
	    }
        }

	if (strncmp(sqlGetString(res, row, "wrktyp"),
		    WRKTYP_PICK, WRKTYP_LEN) != 0  ||
	    (sqlIsNull(res, row, "ship_line_id") && 
	     sqlIsNull(res, row, "wkonum")))
	{
	    continue;
	}

	if (sqlIsNull(res, row, "wkonum"))
	{
	    strcpy(dtl_table, "shipment_line");
	    strcpy(pending_linsts, LINSTS_PENDING);
	    sprintf(where_clause, " ship_line_id = '%s'",
		    sqlGetString(res, row, "ship_line_id"));
	    memset(seqnum_clause, 0, sizeof(seqnum_clause));
	    memset(catch_qty_clause, 0, sizeof(catch_qty_clause));
	}
	else
	{
	    strcpy(dtl_table, "wkodtl");
	    sprintf(catch_qty_clause,
	        ", pck_catch_qty = pck_catch_qty + %f ",
	        sqlGetFloat(res, row, "rem_catch_qty"));
	    strcpy(pending_linsts, WKO_LINSTS_PENDING);
	    sprintf(where_clause,
		    " wkonum = '%s' and wkorev = '%s' and "
		    " wkolin = '%s' and client_id = '%s' and "
		    " wh_id  = '%s' ",
		    sqlGetString(res, row, "wkonum"),
		    sqlGetString(res, row, "wkorev"),
		    sqlGetString(res, row, "wkolin"),
		    sqlGetString(res, row, "client_id"),
		    sqlGetString(res, row, "wh_id"));
	    sprintf(seqnum_clause, " and seqnum = 0");
	}
	
	sprintf(buffer,
		"select oviqty "
		"  from %s "
		" where %s %s "
		" for update",
		dtl_table, where_clause, seqnum_clause);

   	ret_status = sqlExecStr(buffer, &tres);
	if (eOK != ret_status)
	{
	    sqlFreeResults(res);
	    sqlFreeResults(tres);
	    return(ret_status);
	}
	trow = sqlGetRow(tres);

	/* PR 59877
	 * If we have any extra oviqty, we should remove it first.
	 * The oviqty is the extra amount allocated for the shipment line.
	 * Namely to allow us to allocate full case(s) or a full pallet pick
	 * even if order line or work order line doesn't request so much.
	 */
	if (sqlGetLong(tres, trow, "oviqty") >= sqlGetLong(res, row, "remqty"))
	{
	    inpqty_update = 0;
	    oviqty_update = sqlGetLong(res, row, "remqty");
	}
	else
	{
	    oviqty_update = sqlGetLong(tres, trow, "oviqty");
	    inpqty_update = sqlGetLong(res, row, "remqty") - oviqty_update;
	}
	sqlFreeResults(tres);

	/*
	 * update either the shipment_line or the wkodtl with
	 * the updated quantities
	 */
	sprintf(buffer,
		"update %s "
		"   set pckqty = pckqty + %d, "
		"       inpqty = inpqty - %d, "
		"       oviqty = oviqty - %d %s"		
		" where %s %s ",
		dtl_table,
		inpqty_update, 
		inpqty_update, 
		oviqty_update,
		catch_qty_clause,
		where_clause, seqnum_clause);

	ret_status = sqlExecStr(buffer, NULL);
	if (eOK != ret_status)
	{
	    sqlFreeResults(res);
	    return(ret_status);
	}
        
	/* Before we update the shipment_line/wkodtl to 
	 * PENDING check for any outstanding rplwrk records 
	 */

	if (sqlIsNull(res, row, "wkonum"))
        {
            sprintf(buffer,
                    "[select ship_line_id "
                    "   from shipment_line "
                    "  where %s "
                    "    and inpqty = 0 "
                    "    and not exists "
                    "    (select rplref     "
                    "       from rplwrk "
                    "      where %s) ] " 
                    " | change shipment line status "
                    "    where %s "
                    "      and linsts = '%s' ",
                    where_clause,
                    where_clause,
                    where_clause,
                    pending_linsts);
        }
        else
        {
            sprintf(buffer,
                    "[update %s  "
                    "   set linsts = '%s' "
                    " where %s %s "
                    "   and inpqty = 0 "
                    "   and not exists "
                    "    (select rplref     "
                    "       from rplwrk "
                    "      where %s)] ",
                    dtl_table,
                    pending_linsts, 
                    where_clause, seqnum_clause, 
                    where_clause);
        }
        ret_status = srvInitiateCommand(buffer, NULL);

	if (!sqlIsNull(res, row, "ordnum"))
	{
	    mocaDataRes *xres;
	    mocaDataRow *xrow;

	    if (sqlIsNull(res, row, "wkonum"))
	    {
		sprintf(buffer,
			"select xdkflg "
			"  from ord_line"
			" where ordnum = '%s' "
			"   and ordlin = '%s' "
			"   and ordsln = '%s' "
			"   and client_id = '%s' "
			"   and wh_id = '%s' ",
			sqlGetString(res, row, "ordnum"),
			sqlGetString(res, row, "ordlin"),
			sqlGetString(res, row, "ordsln"),
			sqlGetString(res, row, "client_id"),
			sqlGetString(res, row, "wh_id"));
	    }
	    else 
	    {
		sprintf(buffer,
			"select xdkflg "
			"  from wkodtl "
			" where %s %s ",
			where_clause, seqnum_clause);
	    }
	    ret_status = sqlExecStr(buffer, &xres);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(xres);
		misTrc(T_FLOW, "Unable to grab cross dock information");
		sqlFreeResults(res);
		return(ret_status);
	    }
	    xrow = sqlGetRow(xres);
	    /* Attempt to update any cross-dock work */
	    xdkflg = sqlGetBoolean(xres, xrow, "xdkflg");
	    sqlFreeResults(xres);

	    if (xdkflg == BOOLEAN_TRUE)
	    {
		sprintf(buffer,
			"update xdkwrk "
			"   set xdkqty = xdkqty - %ld, "
			"       alcqty = alcqty - %ld "
			" where %s  ",
			sqlGetLong(res, row, "remqty"), 
			sqlGetLong(res, row, "remqty"), 
			where_clause);

		ret_status = sqlExecStr(buffer, NULL);
		if ((ret_status != eOK) && 
		    (ret_status != eDB_NO_ROWS_AFFECTED))
		{
		    sqlFreeResults(res);
		    misTrc(T_FLOW, 
			   "FATAL - update of xdkwrk failed...nothing to do!");
		    return(ret_status);
		}
		
		if (ret_status == eOK)
		{
		    /* Assume that we'll just reallocate this line
		     * if we haven't cross-docked anything */
		    sprintf(buffer,
			    "delete from xdkwrk "
			    " where %s  "
			    "   and exists "
			    "   (select 1 from %s "
			    "   where %s %s and linsts = '%s') ",
			    where_clause,
			    dtl_table, where_clause, seqnum_clause,
			    pending_linsts);
		    ret_status = sqlExecStr(buffer, NULL);
		    if ((ret_status != eOK) && 
			(ret_status != eDB_NO_ROWS_AFFECTED))
		    {
			sqlFreeResults(res);
			misTrc(T_FLOW, 
			       "FATAL - delete of xdkwrk failed..."
			       "nothing to do!");
			return(ret_status);
		    }
		}
	    }
	}	   	
    }	   	

    misTrc(T_FLOW, "Cleaning pckwrk where appqty is zero");

    sprintf(buffer,
	    "delete from pckwrk where cmbcod = '%s' and appqty = 0",
	    sqlGetString(res, sqlGetRow(res), "cmbcod"));
    sqlExecStr(buffer, NULL);

    sprintf(buffer,
	    "update pckwrk set pckqty = appqty where cmbcod = '%s'",
	    sqlGetString(res, sqlGetRow(res), "cmbcod"));

    
    sqlExecStr(buffer, NULL);

    /*
     * Now, let's see if there are any shipements whose status 
     * can get set back to READY.  We have to do this check after
     * we delete the pckwrk, because reset shipment status checks
     * to make sure there are no existing pckwrk records.
     */

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	/* Check if there's no other lines in process
	 * nor replenishments pending for the shipment
	 * then set the shipment to READY
	 */

	if (!sqlIsNull(res, row, "ship_line_id"))
	{
	    sprintf (buffer,
		    "reset shipment status "
		    "where ship_line_id = '%s' ",
		    sqlGetString(res, row, "ship_line_id"));

	    ret_status = srvInitiateCommand(buffer, NULL);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
		return(ret_status);
	    }	
	}
	else if (!sqlIsNull(res, row, "wkonum"))
	{
	    sprintf (buffer,
		     "reset work order status "
		     " where wkonum = '%s' " 
		     "   and wkorev = '%s' "
		     "   and client_id = '%s' "
		     "   and wh_id = '%s' ",
		     sqlGetString(res, row, "wkonum"),
		     sqlGetString(res, row, "wkorev"),
		     sqlGetString(res, row, "client_id"),
		     sqlGetString(res, row, "wh_id"));

	    ret_status = srvInitiateCommand(buffer, NULL);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
		return(ret_status);
	    }	
	}
    }
    sqlFreeResults(res);

    return(eOK);
}

static long sUpdQvlwrk(char *wh_id_i,
		       char *stoloc_i,
		       char *prtnum_i,
		       char *prt_client_id_i,
		       long quantity_i,
		       char *lodlvl_i,
		       char *asset_typ_i)
{
    char buffer[2000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    char for_update_clause[50];

    sprintf(buffer,
            "check var nonqvl area "
            " where stoloc = '%s' "
            "   and wh_id = '%s' ",
            stoloc_i,
            wh_id_i);

    ret_status = srvInitiateCommand(buffer, NULL);
    if (ret_status != eOK)
    {
       /* This should be the normal case */
      strcpy(for_update_clause, "for update of pndqvl");
    }
    else
    {
      strcpy(for_update_clause, " ");
    }
    
    if (asset_typ_i && 
        misTrimLen(asset_typ_i, ASSET_TYP_LEN) &&
        (strncmp(lodlvl_i, LODLVL_LOAD, LODLVL_LEN) == 0 ||
         strncmp(lodlvl_i, LODLVL_SUBLOAD, LODLVL_LEN) == 0))
    {
        sprintf(buffer, 
                "select pndqvl, rowid "
                "  from qvlwrk "
                " where prtnum = '%s' "
                "   and prt_client_id = '%s' "
                "   and stoloc = '%s' "
                "   and wh_id = '%s' "
                "   and asset_typ = '%s' "
                " order by decode(untqty, %d, 0, abs(%d - untqty)) "
                "   %s",
                prtnum_i,
                prt_client_id_i,
                stoloc_i,
                wh_id_i,
                asset_typ_i,
                quantity_i, quantity_i,
                for_update_clause);
    }
    else
    {
        sprintf(buffer, 
                "select pndqvl, rowid "
                "  from qvlwrk "
                " where prtnum         = '%s'"
                "   and prt_client_id  = '%s'"
                "   and stoloc         = '%s'"
                "   and wh_id          = '%s'"
                "   and asset_typ is null "
                " order by decode(untqty, %d, 0, abs(%d - untqty)) "
                "   %s",
                prtnum_i,
                prt_client_id_i,
                stoloc_i,
                wh_id_i,
                quantity_i, quantity_i,
                for_update_clause);    
    }
    ret_status = sqlExecStr(buffer, &res);

    if (eOK == ret_status)
    {
	row = sqlGetRow(res);
	sprintf(buffer,
		"update locmst "
		"   set pndqvl = pndqvl - %f "
		" where stoloc = '%s' "
		"   and wh_id  = '%s' ",
		sqlGetFloat(res, row, "pndqvl"),
		stoloc_i,
		wh_id_i);

	ret_status = sqlExecStr(buffer, NULL);
	if (eOK != ret_status)
	{
	    sqlFreeResults(res);
	    return(ret_status);
	}
	if (sqlGetDataType(res, "rowid") == COMTYP_STRING)
	{
	    sprintf(buffer, "delete from qvlwrk where rowid = '%s'", 
		    sqlGetString(res, row, "rowid"));
	}
	else
	{
	    /* DB2 has a numeric rowid. Binding will handle SQL
	    ** statements, but we need to convert to C datatype here
	    */
	    sprintf(buffer, "delete from qvlwrk where rowid = %ld", 
		    sqlGetLong(res, row, "rowid"));
	}
	sqlFreeResults(res);

	ret_status = sqlExecStr(buffer, NULL);
	return(ret_status);
    }
    else if (eDB_NO_ROWS_AFFECTED == ret_status)
    {
	sqlFreeResults(res);
	return(eOK);
    }

    sqlFreeResults(res);
    return(ret_status);
}
static long sUpdateInvsum(char *wh_id,
			  char *arecod,
			  char *stoloc,
			  char *prtnum,
			  char *prt_client_id,
			  long quantity)
{
    char buffer[2000];
    long ret_status;

    sprintf(buffer,
	    "delete from invsum "
	    " where arecod = '%s' "
	    "   and stoloc = '%s' "
	    "   and wh_id  = '%s' "
	    "   and prtnum = '%s' "
	    "   and prt_client_id = '%s'"
	    "   and untqty = 0 "
	    "   and comqty = 0 "
	    "   and pndqty = %d",
	    arecod, 
            stoloc,
            wh_id,
            prtnum,
            prt_client_id, 
            quantity);

    ret_status = sqlExecStr(buffer, NULL);

    if (ret_status != eOK)
    {
        if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sprintf(buffer,
                    "update invsum "
                    "   set pndqty = pndqty - %d "
                    " where arecod = '%s' "
                    "   and stoloc = '%s' "
                    "   and wh_id  = '%s' "
                    "   and prtnum = '%s' "
                    "   and prt_client_id = '%s'",
                    quantity,
                    arecod,
                    stoloc,
                    wh_id,
                    prtnum,
                    prt_client_id);

            ret_status = sqlExecStr(buffer, NULL);
        }
        else
        {
            /* We're returning no rows affected, but it might
             * be because the quantities don't match up to the delete.
             * Let's see if there's any invsum record out there at all.
             * If there is, we need to update the invsum record.
             * If not, we'll just return eOK.
             * Although this seems like an error we should be capturing,
             * because technically it's not a good thing if the invsum
             * record is missing, we'll let it go, because by the end of this,
             * if this replenishment gets cancelled, and there's no
             * other inventory pending, it would probably have gotten
             * cleaned up anyways.
             */

            sprintf(buffer,
                    "select 'x' "
                    "  from invsum "
                    " where arecod = '%s' "
                    "   and stoloc = '%s' "
                    "   and wh_id  = '%s' "
                    "   and prtnum = '%s' "
                    "   and prt_client_id = '%s'",
                    arecod, stoloc, wh_id, prtnum, prt_client_id);

            ret_status = sqlExecStr(buffer, NULL);

            if (ret_status == eOK)
            {
                sprintf(buffer,
                        "update invsum "
                        "   set pndqty = pndqty - %d "
                        " where arecod = '%s' "
                        "   and stoloc = '%s' "
                        "   and wh_id  = '%s' "
                        "   and prtnum = '%s' "
                        "   and prt_client_id = '%s'",
                        quantity,
                        arecod,
                        stoloc,
                        wh_id,
                        prtnum,
                        prt_client_id);

                ret_status = sqlExecStr(buffer, NULL);
            }
            else if (ret_status == eDB_NO_ROWS_AFFECTED)
            {
                ret_status = eOK;
            }
        }
    }

    return(ret_status);
}

long trnDeallocateLocation(char *lodnum_i, 
			   char *subnum_i, 
			   char *dtlnum_i,
                           char *wrkref_i)
{
    char buffer[2000];
    long ret_status;
    long success = 0;
    long appqty;
    mocaDataRes *res, *qres;
    mocaDataRow *row, *qrow;
    char invmovString[1000];

    res = qres = NULL;

    if ( (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN) ) ||
         (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN) ) ||
         (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN) ) )
    {
	misTrc(T_FLOW, "Determining area code for all pending locations...");
	misTrc(T_FLOW, "lodnum: %s  subnum: %s  dtlnum: %s",
		        lodnum_i, subnum_i, dtlnum_i );

	if ( dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN) )
	    sprintf(invmovString, 
		    "invmov.lodnum = '%s' and lodlvl = '%s'",
		    dtlnum_i, LODLVL_DETAIL);
	else if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN) )
	    sprintf(invmovString, 
		    "invmov.lodnum = '%s' and lodlvl = '%s'",
		    subnum_i, LODLVL_SUBLOAD);
	else
	    sprintf(invmovString, 
		    "invmov.lodnum = '%s' and lodlvl = '%s'",
		    lodnum_i, LODLVL_LOAD);

	sprintf(buffer,
		"select invmov.stoloc, aremst.sigflg, "
		"       aremst.loccod, aremst.pckcod, "
		"       invmov.lodnum invmov_lodnum, "
		"       invmov.seqnum, invmov.lodlvl, "
		"       aremst.arecod, aremst.wh_id "
		"  from locmst, aremst, invmov"
		" where %s "
		"   and invmov.stoloc = locmst.stoloc "
		"   and invmov.wh_id  = locmst.wh_id "
		"   and locmst.arecod = aremst.arecod "
		"   and locmst.wh_id  = aremst.wh_id "
		" order by invmov.seqnum",
		invmovString);

	ret_status = sqlExecStr(buffer, &res);
	if (eOK != ret_status)
	{
	    sqlFreeResults(res);

	    misTrc(T_FLOW, 
		   "Failed to find invmov as specified - attempting with"
		   " children ");

	    memset(buffer, 0, sizeof(buffer));
	    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
	    {
		misTrc(T_FLOW, 
		       "Already specified detail - no children to check");
	    }
	    else if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
	    {
		/* we've already checked for lodlvl = SUB, so all
		 * we need to do is look "down" - which means Detail
		 */
		sprintf(buffer,
			"select piv.pndloc stoloc, aremst.sigflg, "
			"       aremst.loccod, aremst.pckcod, "
			"       piv.dtlnum invmov_lodnum, "
			"       piv.seqnum, piv.lodlvl, "
			"       aremst.arecod, aremst.wh_id "
			"  from locmst, aremst, pndinv_view piv"
			" where piv.subnum = '%s' "
			"   and piv.lodlvl = '%s' "
			"   and piv.pndloc = locmst.stoloc "
			"   and piv.wh_id  = locmst.wh_id "
			"   and locmst.arecod = aremst.arecod "
			"   and locmst.wh_id  = aremst.wh_id "
			" order by piv.seqnum",
			subnum_i, LODLVL_DETAIL);
	    }
	    else
	    {
		/* we've already checked for lodlvl = LOAD and it 
		 * didn't exist.  Rather than doing a "lodlvl in ..."
		 * or "lodlvl != ", we'll leave lodlvl off for performance
		 * sake knowing that the only thing that can come back is 
		 * SUB or DETAIL 
		 */
		sprintf(buffer,
			"select piv.pndloc stoloc, aremst.sigflg, "
			"       aremst.loccod, aremst.pckcod, "
			"       decode(piv.lodlvl, '%s', piv.subnum, "
			"              '%s', piv.dtlnum) invmov_lodnum, "
			"       piv.lodnum, piv.subnum, piv.dtlnum, "
			"       piv.seqnum, piv.lodlvl, "
			"       aremst.arecod, aremst.wh_id "
			"  from locmst, aremst, pndinv_view piv"
			" where piv.lodnum = '%s' "
			"   and piv.pndloc = locmst.stoloc "
			"   and piv.wh_id  = locmst.wh_id "
			"   and locmst.arecod = aremst.arecod "
			"   and locmst.wh_id  = aremst.wh_id "
			" order by piv.seqnum",
			LODLVL_SUBLOAD, LODLVL_DETAIL, lodnum_i);
	    }
	    
	    if (strlen(buffer))
	    {
		ret_status = sqlExecStr(buffer, &res);
		if (ret_status != eOK)
		{
		    free(res);
		    return(ret_status);
		}
	    }
	    else
	    {
		return(ret_status);
	    }
	}

	
	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
	    if (sqlGetBoolean(res, row, "sigflg") == BOOLEAN_TRUE)
	    {
		/* We must handle the case of mixed pallets
                 * If we are looking at dtlnum. we will not have the 
                 * asset_typ in qvlwrk. Hence select asset_typ as null
                 * if dtlnum_i is passed.
                 */
		if ( dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN) )
		    sprintf(buffer,
			    "select ivd.prtnum, "
			    "       ivd.prt_client_id, "
			    "       null asset_typ, "
			    "       sum(ivd.untqty) lodqty "
			    "  from invdtl ivd, "
			    "       invsub ivs, "
			    "       invlod ivl "
			    " where ivd.subnum = ivs.subnum "
			    "   and ivs.lodnum = ivl.lodnum "
			    "   and ivd.dtlnum = '%s' "
			    " group by ivd.prtnum, "
			    "          ivd.prt_client_id, "
			    "          ivl.asset_typ",
			    dtlnum_i);
		else if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN) )
		    sprintf(buffer,
			    "select ivd.prtnum, "
			    "       ivd.prt_client_id, "
			    "       ivs.asset_typ, "
			    "       sum(ivd.untqty) lodqty "
			    "  from invdtl ivd, "
			    "       invsub ivs, "
			    "       invlod ivl "
			    " where ivd.subnum = ivs.subnum "
			    "   and ivs.lodnum = ivl.lodnum "
			    "   and ivs.subnum = '%s' "
			    " group by ivd.prtnum, "
			    "          ivd.prt_client_id, "
			    "          ivs.asset_typ",
			    subnum_i);
		else 
		    sprintf(buffer,
			    "select ivd.prtnum, "
			    "       ivd.prt_client_id, "
			    "       ivl.asset_typ, "
			    "       sum(ivd.untqty) lodqty "
			    "  from invdtl ivd, invsub ivs, invlod ivl"
			    " where ivd.subnum = ivs.subnum "
			    "   and ivs.lodnum = ivl.lodnum "
			    "   and ivl.lodnum = '%s' "
			    " group by ivd.prtnum, "
			    "          ivd.prt_client_id, "
			    "          ivl.asset_typ",
			    lodnum_i);

		if (!success)
		{
		    ret_status = sqlExecStr(buffer, &qres);
		    if (eOK != ret_status)
		    {
			sqlFreeResults(qres);
			sqlFreeResults(res);
			return(ret_status);
		    }
		    success = 1;
		}
		for (qrow = sqlGetRow(qres); qrow; 
		     qrow = sqlGetNextRow(qrow))
		{
		    ret_status = sUpdQvlwrk(
				   sqlGetString(res, row, "wh_id"),
				   sqlGetString(res, row, "stoloc"),
				   sqlGetString(qres, qrow, "prtnum"),
				   sqlGetString(qres, qrow, "prt_client_id"),
				   sqlGetLong(qres, qrow, "lodqty"),
				   sqlGetString(res, row, "lodlvl"),
				   sqlGetString(qres, qrow, "asset_typ"));
		    if (ret_status == eDB_NO_ROWS_AFFECTED)
		    {
			misTrc(T_FLOW,
			       "Failed to update missing qvlwrk - ignoring!");
		    }
		    else if (eOK != ret_status)
		    {
			sqlFreeResults(qres);
			sqlFreeResults(res);
			return(ret_status);
		    }
		    if (strcmp(sqlGetString(res, row, "pckcod"),
				PCKCOD_NOT_PICK) != 0)
		    {
			ret_status = 
			    sUpdateInvsum(sqlGetString(res, row, "wh_id"),
					  sqlGetString(res, row, "arecod"),
					  sqlGetString(res, row, "stoloc"),
					  sqlGetString(qres, qrow, "prtnum"),
					  sqlGetString(qres, qrow,
						       "prt_client_id"),
					  sqlGetLong(qres, qrow, "lodqty"));
			if (ret_status == eDB_NO_ROWS_AFFECTED)
			{
			    misTrc(T_FLOW,
				   "Failed to update missing "
				   "invsum - ignoring.");
			}
			else if (eOK != ret_status)
			{
			    sqlFreeResults(qres);
			    sqlFreeResults(res);
			    return(ret_status);
			}
		    }
		}
	    }

	    
	    sprintf(buffer,
		    "delete from invmov"
		    " where lodnum = '%s' "
		    "   and lodlvl = '%s' "
		    "   and stoloc = '%s' "
		    "   and wh_id  = '%s' "
		    "   and seqnum = '%d' ",
		    sqlGetString(res, row, "invmov_lodnum"),
		    sqlGetString(res, row, "lodlvl"),
		    sqlGetString(res, row, "stoloc"),
		    sqlGetString(res, row, "wh_id"),
		    sqlGetLong(res, row, "seqnum"));
	    ret_status = sqlExecStr(buffer, NULL);
	}
	if (qres)
	    sqlFreeResults(qres);
	sqlFreeResults(res);
    }
    else
    {
	misTrc(T_FLOW, "Deallocate by pckwrk information only");

	sprintf(buffer,
		"select pm.stoloc, ar.sigflg, ar.loccod, ar.pckcod, "
		"       ar.arecod, ar.wh_id "
		"  from pckmov pm, aremst ar "
		"  where pm.cmbcod = (select cmbcod from pckwrk "
		"                      where wrkref = '%s' "
		"                        and pckqty != appqty)"
		"    and pm.arecod = ar.arecod "
		"    and pm.wh_id  = ar.wh_id "
		"  order by pm.seqnum",
		wrkref_i);

	ret_status = sqlExecStr(buffer, &res);
	if (ret_status == eDB_NO_ROWS_AFFECTED)
	{
	    misTrc(T_FLOW, 
		   "Failed to retrieve pckmov/arecod information"
		   " for pick - ignoring.");
	}
	else if (eOK != ret_status)
	{
	    sqlFreeResults(res);
	    return(ret_status);
	}
	
	sprintf(buffer,
		"select pw.prtnum, pw.prt_client_id, "
		"       sum(pw.pckqty - pw.appqty) remqty, "
		"       sum(pw.appqty) appqty, "
		"       pw.asset_typ, "
		"       pw.lodlvl "
		"  from pckwrk pw "
		" where pw.cmbcod in "
		"   (select cmbcod "
		"      from pckwrk "
		"     where pckwrk.wrkref = '%s')"
		" group by pw.prtnum, pw.prt_client_id, pw.asset_typ, pw.lodlvl ",
		wrkref_i);
	ret_status = sqlExecStr(buffer, &qres);
	if (eOK != ret_status)
	{
	    sqlFreeResults(qres);
	    sqlFreeResults(res);
	    return(ret_status);
	}

	/* For each one of the areas we are going through...we must update
	 * quantities.... 
	 *
	 * NOTE:  We ignored the case where this query failed - the 
	 *        theory is that if we don't have any pckmovs, you
	 *        should still be able to cancel a pick.  The updates
	 *        below all just handle the case of data that needs to
	 *        be cleaned up in the event that there ARE pckmovs
	 */
	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
	    if (BOOLEAN_TRUE == sqlGetBoolean(res, row, "sigflg"))
	    {
		/* There really shouldn't be any looping here...if 
		   there were, it would suggest diff prtnum for the
		   same cmbcod... */
		for (qrow = sqlGetRow(qres); qrow; qrow = sqlGetNextRow(qrow))
		{
                    if (misTrimLen
                          (sqlGetString(res, row, "stoloc"), STOLOC_LEN) != 0) 
                    {

		        ret_status = sUpdQvlwrk(
                                    sqlGetString(res, row, "wh_id"),
                                    sqlGetString(res, row, "stoloc"),
                                    sqlGetString(qres, qrow, "prtnum"),
                                    sqlGetString(qres, qrow, "prt_client_id"),
                                    sqlGetLong(qres, qrow, "remqty"),
                                    sqlGetString(qres, qrow, "lodlvl"),
                                    sqlGetString(qres, qrow, "asset_typ"));
		    
		        if (eOK != ret_status)
		        {
			    sqlFreeResults(res);
			    sqlFreeResults(qres);
			    return(ret_status);
		        }
		        if (strcmp(sqlGetString(res, row, "pckcod"),
				PCKCOD_NOT_PICK) != 0)
		        {
		    	    ret_status =  sUpdateInvsum(
                                    sqlGetString(res, row, "wh_id"),
                                    sqlGetString(res, row, "arecod"),
                                    sqlGetString(res, row, "stoloc"),
                                    sqlGetString(qres, qrow, "prtnum"),
                                    sqlGetString(qres, qrow, "prt_client_id"),
                                    sqlGetLong(qres, qrow, "remqty"));
			    if (eOK != ret_status)
			    {
			        sqlFreeResults(qres);
			        sqlFreeResults(res);
			        return(ret_status);
			    }
                        }
		    }
		}
	    }
	}
	sqlFreeResults(res);

	/* If we haven't applied anything, then we can buzz through
	   and blow away the pckmov entries */
	appqty = 0;
	for (qrow = sqlGetRow(qres); qrow; qrow = sqlGetNextRow(qrow))
	    appqty += sqlGetLong(qres, qrow, "appqty");

	sqlFreeResults(qres);

	if (appqty == 0)
	{
	    sprintf(buffer, 
		    "delete from pckmov "
		    " where cmbcod = "
		    "   (select cmbcod from pckwrk "
		    "     where wrkref = '%s') ",
		    wrkref_i);
	    ret_status = sqlExecStr(buffer, NULL);
	    if (ret_status == eDB_NO_ROWS_AFFECTED)
	    {
		misTrc(T_FLOW,
		       "Failed to delete missing pckmov - ignoring.");
	    }
	    else if (eOK != ret_status)
	    {
		return(ret_status);
	    }
	}
    }

    return(eOK);


}
