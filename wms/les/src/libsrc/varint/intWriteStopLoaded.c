static const char *rcsid = "$Id: intWriteStopLoaded.c,v 1.1 2012/03/24 09:44:56 tdai Exp $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "intlib.h"

/*ARR 042297 -- 

   Added policy to govern whether or not pick batches are
   statused to complete within this function.
   This function checks the static policy value 
   and returns
   appropriate value
 */
static int sIsPckbatCompletionEnabled(char *wh_id)
{
    char buffer[200];
    long ret_status;
    static long pckbat_completion_enabled = -1;
    
    /* if first time through ... get policy value */
    if (pckbat_completion_enabled < 0)
    {
	sprintf(buffer,
		"select 'x' "
		"  from poldat_view "
		" where polcod = 'WRITE-STOP-LOADED' "
		"   and polvar = 'MISCELLANEOUS' "
		"   and polval = 'COMPLETE-SCHBAT-DURING-LOAD' "
		"   and wh_id  = '%s' "
		"   and upper(rtstr1) = 'N' ",
		wh_id);
	ret_status = sqlExecStr(buffer, NULL);
	if (eOK == ret_status)
	    pckbat_completion_enabled = 0;
	else
	    pckbat_completion_enabled = 1;
    }
    return (pckbat_completion_enabled);
}

LIBEXPORT 
RETURN_STRUCT *intWriteStopLoaded(char *stop_id_i,
                                  char *stop_seal_i,
                                  char *wh_id_i)
{
    char stop_id[STOP_ID_LEN + 1];
    long ret_status;
    char buffer[1500];
    char stop_seal[STOP_SEAL_LEN + 1];
    char wh_id[WH_ID_LEN + 1];
    char tmpvar[100];
    RETURN_STRUCT *CurPtr;

    mocaDataRes *res;
    mocaDataRow *row;

    memset(stop_id, 0, sizeof(stop_id));
    memset(stop_seal,   0, sizeof(stop_seal));
    memset(wh_id, 0, sizeof(wh_id));

    if (stop_id_i && misTrimLen(stop_id_i, STOP_ID_LEN))
	misTrimcpy(stop_id, stop_id_i, STOP_ID_LEN);
    else
	return (APPMissingArg("stop_id")); 
	
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    else
        return (APPMissingArg("wh_id"));

    /* Check whether the stop is complete,
     * if this stop is complete, then this command
     * will just return an error that stop is already complete.
     */

    sprintf(buffer,
	    "select 'x' "
	    "  from stop "
	    " where stop_id = '%s'  "
	    "   and stop_cmpl_flg = '%d' ",
	    stop_id,
	    BOOLEAN_TRUE);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
    {
        return (srvResults(eSTOP_COMPLETE, NULL));
    }

    /*
     * The user may record the stop seal at this time, so see if
     * a stop seal was passed.
     */

    if (stop_seal_i && misTrimLen(stop_seal_i, STOP_SEAL_LEN))
        misTrimcpy(stop_seal, stop_seal_i, STOP_SEAL_LEN);


    /* Check that the shipment may be loaded, and set it to loaded 
       if so... */
    /*042297 -- ARR modified table driving order 
       and simple select of 'x' where rownum<2 */

    sprintf(buffer,
	    "select 'x' "
	    " from locmst, invlod, invsub, invdtl, shipment_line, shipment "
	    " where locmst.arecod not in (select arecod "
	    "                               from aremst "
	    "                              where shpflg = '%d') "
	    "   and locmst.stoloc          = invlod.stoloc "
            "   and locmst.wh_id           = invlod.wh_id  "
	    "   and invlod.lodnum          = invsub.lodnum "
	    "   and invsub.subnum          = invdtl.subnum "
	    "   and invdtl.ship_line_id    = shipment_line.ship_line_id "
	    "   and shipment_line.ship_id  = shipment.ship_id "
	    "   and shipment.stop_id    = '%s' "
	    "   and rownum           < 2 ",
	    BOOLEAN_TRUE,
	    stop_id);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
    {
	/* Return an error to indicate that the shipment is not
	   yet loaded ... 
         * In an LTL scenario, the shipments must be split before
         * the stops can be complete.
         * Here's how it works for LTL:
         *   - user tries to close trailer
         *   - close trailer tries to complete stops
         *   - if stops aren't complete, user must split the shipments, so
         *     stop can be marked complete.
         */

	return (srvResults(eINT_STOP_NOT_FULLY_LOADED, NULL));
    }

    /*
     * Now, we didn't see any inventory sitting around that needed to
     * be loaded, but let's make sure all of our shipments are staged,
     * because there may not be inventory, but that doesn't mean the
     * shipments are all staged.
     */

    sprintf(buffer,
           "select ship_id "
           "  from shipment "
           " where stop_id = '%s' "
           "   and stgdte  is null ",
           stop_id);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
    {
        return (srvResults(eINT_SHIP_ID_NOT_YET_STAGED, NULL));
    }

    /* At this point, all we do is update the loddte.
     * Prevous to 6.0.0, the following included 'loddte is null' in the
     * where clause.  I'm not sure why, unless it was to preserve the
     * earliest date the product was loaded.  However, with stop sequence
     * loading, it's possible that a stop may be moved back to uncomplete,
     * and have to be re-completed, at which point the loddte may already]
     * be set.
     * So, that line is being removed from the where clause, and we'll 
     * consider the loddte the date the stop went to complete.
     */

    /* We're calling the 'change shipment status' 
     * component here, rather than doing an update, to trigger 
     * the change to Lens.
     */

    sprintf(buffer,
            " [select ship_id           "
            "    from shipment          "
            "   where stop_id = '%s']   "
            " |                         "
            " change shipment status    "    
            "  where ship_id = @ship_id "
            "    and shpsts = '%s'      "
            " |                         "
            " [update shipment          "
            "     set loddte = sysdate  "
            "  where ship_id = @ship_id]",
            stop_id,
	    SHPSTS_LOADED);

    ret_status = srvInitiateCommand(buffer, NULL);
    if (ret_status != eOK)
	return (srvResults(ret_status, NULL));

    /* Let's update all shipment_line shpqty to the proper loaded qty */

    /* We're calling 'change shipment line' here, rather than doing an
     * update, to make sure that the change is triggered to Lens.
     */

    sprintf(buffer,
	    " [select sl.ship_line_id,                   "
            "         sl.stgqty cur_stgqty,              "
            "         sl.shpqty cur_shpqty,              "
            "         (sl.shpqty + sl.stgqty) new_shpqty "
	    "    from shipment_line sl,                  "
            "         shipment sh                        "
	    "   where sh.stop_id = '%s'                  "
	    "     and sl.ship_id = sh.ship_id]           "
            " |                                          "
            " change shipment line                       "
            "  where ship_line_id = @ship_line_id        "
            "    and shpqty = @new_shpqty                "
	    "    and stgqty = 0                          "
	    "    and linsts = '%s'                       "
            "    and ignore_status = %ld                 ",
            stop_id,
	    LINSTS_SHP_CMP,
            BOOLEAN_TRUE);

    ret_status = srvInitiateCommand(buffer, NULL);

    if (ret_status != eOK)
    {
        misLogError("Unable to update shipment_line.shpqty for " 
                    "stop_id %s (%ld)",
		    stop_id, ret_status);

	return (srvResults(ret_status, NULL));
    }

    /* We're calling 'change order line' here, rather than doing a straight
     * update, to trigger that change to Lens.
     *
     * Because SeamLes is not handling part client Id field
     * in it's transactions. If a prt_client_id was not passed, we will
     * default to the client Id, so we should pass prt_client_id   
     */

    sprintf(buffer,
	    " [select sl.ordnum,                      "
	    "         sl.client_id,                   "
            "         sl.ordlin,                      "
            "         sl.ordsln,                      "
            "         ol.prt_client_id,               "
            "         sl.wh_id,                       "
            "         (sum(ol.shpqty) + sum(sl.shpqty)) new_shpqty "
	    "    from ord_line ol,                    "
            "         shipment_line sl,               "
            "         shipment                        "
	    "   where sl.linsts = '%s'                "
	    "     and sl.ordnum = ol.ordnum           "
	    "     and sl.ordlin = ol.ordlin           "
	    "     and sl.ordsln = ol.ordsln           "
	    "     and sl.client_id = ol.client_id     "
            "     and sl.wh_id     = ol.wh_id         "
	    "     and sl.ship_id   = shipment.ship_id "
	    "     and shipment.stop_id = '%s'         "
            "group by sl.wh_id,                       "
            "         sl.ordnum,                      "
            "         sl.client_id,                   "
            "         ol.prt_client_id,               "
            "         sl.ordlin,                      "
            "         sl.ordsln]                      "
            " |                                       "
	    " change order line                       "
	    "  where ordnum = @ordnum                 "
	    "    and ordlin = @ordlin                 "
	    "    and ordsln = @ordsln                 "
	    "    and client_id = @client_id           "
	    "    and prt_client_id = @prt_client_id         "
            "    and wh_id     = @wh_id               "
            "    and shpqty = @new_shpqty             ",
	    LINSTS_SHP_CMP,
	    stop_id);

    ret_status = srvInitiateInline(buffer, NULL);

    if (ret_status != eOK)
    {
	misLogError("Unable to update ord_line.shpqty for stop_id %s (%ld)",
		    stop_id, ret_status);
	return (srvResults(ret_status, NULL));
    }

    /* Next, we're going to go through and mark all of 
       the pckwrks that are part of this stop as 
       complete... */
    misTrc(T_FLOW, "Updating pckwrks to complete for stop_id: %s", stop_id);

    /*ARR 042297 -- simplified/streamlined the update */

    sprintf(buffer,
	    "update pckwrk "
	    "   set pcksts = '%s' "
	    " where pckwrk.ship_id in "
	    "    (select ship_id "
	    "       from shipment sh "
	    "      where sh.stop_id  = '%s' "
	    "        and sh.loddte  is not null) ",
	    PCKSTS_COMPLETED, stop_id);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	misTrc(T_FLOW, "Unable to update - status: %d", ret_status);
	misLogError("Unable to update - status: %d", ret_status);
	return (srvResults(ret_status, NULL));
    }

    /* And, clear the pick move storage locations ... */
    /*ARR 042297 -- simplified/streamlined the update */
    misTrc(T_FLOW, "Clearing pckmovs of storage location for stop_id: %s",
	   stop_id);

    sprintf(buffer,
	    "update pckmov "
	    "   set stoloc='' "
   	    " where cmbcod in "
	    "    (select cmbcod  "
	    "       from pckwrk pw,shipment sh "
	    "      where pw.ship_id      = sh.ship_id "
	    "        and sh.stop_id     = '%s' "
            "        and sh.loddte is not null) ",
	    stop_id);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	misTrc(T_FLOW, "Unable to update - status: %d", ret_status);
	misLogError("Unable to update - status: %d", ret_status);
	return (srvResults(ret_status, NULL));
    }

    /* Now attempt to mark the schbat as complete (if complete) */

    /*ARR 042297 -- added conditional update of pick bathes to complete 
       The status would in all likelihood be set in a timed
       script from from CRON if not done here 
       -- simplified/streamlined the update */
    if (sIsPckbatCompletionEnabled(wh_id))
    {
	misTrc(T_FLOW, "Updating schbat to complete for stop_id: %s", stop_id);

	sprintf(buffer,
		"complete schbat for stop where stop_id = '%s' ",
		stop_id);
	CurPtr = NULL;
	ret_status = srvInitiateCommand(buffer, &CurPtr);
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
	CurPtr = NULL;

	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    misTrc(T_FLOW, "Unable to update - status: %d", ret_status);
	    misLogError("Unable to update - status: %d", ret_status);
	    return (srvResults(ret_status, NULL));
	}
    }

    /*
     * Clean up any directed work for this stop.
     */

    sprintf(buffer,
            "[select reqnum "
            "   from wrkque "
            "  where refloc = '%s' "
            "    and oprcod = '%s'] " 
            " | "
            " complete work "
            "   where reqnum = @reqnum "
            "     and prcmod = 'nomove' "
            "     and ackdevcod = '%s' "
            "     and ack_usr_id = '%s' ",
            stop_id,
            OPRCOD_LOAD,
            osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "",
            osGetVar(LESENV_DEVCOD) ? osGetVar(LESENV_DEVCOD) : "");

    ret_status = srvInitiateCommand(buffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED &&
        ret_status != eSRV_NO_ROWS_AFFECTED)
    {
        return (srvResults(ret_status, NULL));
    }


    /*
     * Now, mark this stop as complete.
     */

    sprintf(buffer,
            "change stop "
            "  where stop_cmpl_flg = '%d' "
            "    and stop_id = '%s' ",
            BOOLEAN_TRUE,
            stop_id);

    if (strlen(stop_seal))
    {
        sprintf(tmpvar,
               " and stop_seal = '%s' ", stop_seal);
        strcat(buffer, tmpvar);
    }

    ret_status = srvInitiateCommand(buffer, NULL);
    if (ret_status != eOK)
    {
        return (srvResults(ret_status, NULL));
    }
   
    sprintf(buffer,
	    "select car_move.car_move_id, trlr.trlr_stat, "
            "       trlr.trlr_id "
	    "  from car_move left outer join trlr "
	    "    on (car_move.trlr_id = trlr.trlr_id) "
	    "  join stop "
	    "    on (stop.car_move_id = car_move.car_move_id) "
	    " where stop.stop_id        = '%s' ",
	    stop_id);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvResults(ret_status, NULL));
    }

    row = sqlGetRow(res);

    CurPtr = srvResultsInit(eOK,
                            "stop_id",     COMTYP_CHAR, STOP_ID_LEN,
                            "car_move_id", COMTYP_CHAR, CAR_MOVE_ID_LEN,
                            "trlr_id",     COMTYP_CHAR, TRLR_ID_LEN,
                            NULL);
    srvResultsAdd(CurPtr,
                  stop_id,
                  sqlGetString(res, row, "car_move_id"),
                  sqlGetString(res, row, "trlr_id"));


    sqlFreeResults(res);
    return (CurPtr); 
}
