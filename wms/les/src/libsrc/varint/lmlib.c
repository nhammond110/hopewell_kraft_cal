static const char *rcsid = "$Id: lmlib.c,v 1.1 2011/07/21 17:45:43 joworkma Exp $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2003 RedPrairie Corporation.  All rights reserved.
 *  
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "lmlib.h"

typedef struct area_info_list 
{
    char arecod[ARECOD_LEN+1];
    char pckcod[PCKCOD_LEN+1];
    char bldg_id[BLDG_ID_LEN+1];
    char loccod[LOCCOD_LEN+1];
    char wh_id[WH_ID_LEN + 1]; 
    
    long rcv_dck_flg;
    long sigflg;
    long adjflg;
    long stgflg;
    long shpflg;
    long wipflg;
    long fwiflg;
    long cntflg;
    long xdaflg;
    long icnflg;
    long expflg;
    long praflg;
    long dtlflg;
    long subflg;
    long lodflg;
    long fifflg;
    long conflg;
    
    struct area_info_list *next;
} AREA_INFO_LIST;

static AREA_INFO_LIST *gAreaList;

static AREA_INFO_LIST *sFindArea(char *arecod,
                                 char *wh_id)
{
    AREA_INFO_LIST *aptr;
    mocaDataRes *res;
    mocaDataRow *row;
    long ret_status;
    char buffer[1000];

    if (!arecod || strlen(arecod) == 0)
	return(NULL);

    for (aptr = gAreaList; aptr; aptr = aptr->next)
    {
	if (strncmp(aptr->arecod, arecod, ARECOD_LEN) == 0)
	    return(aptr);
    }

    sprintf(buffer,
	    "select * "
            "  from aremst "
             " where arecod = '%s' "
             "   and wh_id  = '%s' ",
	    arecod,
            wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return(NULL);
    }
    row = sqlGetRow(res);
    aptr = (AREA_INFO_LIST *)calloc(1, sizeof(AREA_INFO_LIST));

    strncpy(aptr->arecod, sqlGetString(res, row, "arecod"), ARECOD_LEN);
    strncpy(aptr->wh_id,  sqlGetString(res, row, "wh_id"), WH_ID_LEN);
    if (!sqlIsNull(res, row, "pckcod"))
	strncpy(aptr->pckcod, sqlGetString(res, row, "pckcod"), PCKCOD_LEN);
    if (!sqlIsNull(res, row, "bldg_id"))
	strncpy(aptr->bldg_id, sqlGetString(res, row, "bldg_id"), BLDG_ID_LEN);
    if (!sqlIsNull(res, row, "loccod"))
	strncpy(aptr->loccod, sqlGetString(res, row, "loccod"), LOCCOD_LEN);
    
    if (!sqlIsNull(res, row, "rcv_dck_flg"))
	aptr->rcv_dck_flg = sqlGetLong(res, row, "rcv_dck_flg");
    if (!sqlIsNull(res, row, "sigflg"))
	aptr->sigflg = sqlGetLong(res, row, "sigflg");
    if (!sqlIsNull(res, row, "adjflg"))
	aptr->adjflg = sqlGetLong(res, row, "adjflg");
    if (!sqlIsNull(res, row, "stgflg"))
	aptr->stgflg = sqlGetLong(res, row, "stgflg");
    if (!sqlIsNull(res, row, "shpflg"))
	aptr->shpflg = sqlGetLong(res, row, "shpflg");
    if (!sqlIsNull(res, row, "wipflg"))
	aptr->wipflg = sqlGetLong(res, row, "wipflg");
    if (!sqlIsNull(res, row, "fwiflg"))
	aptr->fwiflg = sqlGetLong(res, row, "fwiflg");
    if (!sqlIsNull(res, row, "cntflg"))
	aptr->cntflg = sqlGetLong(res, row, "cntflg");
    if (!sqlIsNull(res, row, "xdaflg"))
	aptr->xdaflg = sqlGetLong(res, row, "xdaflg");
    if (!sqlIsNull(res, row, "icnflg"))
	aptr->icnflg = sqlGetLong(res, row, "icnflg");
    if (!sqlIsNull(res, row, "expflg"))
	aptr->expflg = sqlGetLong(res, row, "expflg");
    if (!sqlIsNull(res, row, "praflg"))
	aptr->praflg = sqlGetLong(res, row, "praflg");
    if (!sqlIsNull(res, row, "dtlflg"))
	aptr->dtlflg = sqlGetLong(res, row, "dtlflg");
    if (!sqlIsNull(res, row, "subflg"))
	aptr->subflg = sqlGetLong(res, row, "subflg");
    if (!sqlIsNull(res, row, "lodflg"))
	aptr->lodflg = sqlGetLong(res, row, "lodflg");
    if (!sqlIsNull(res, row, "fifflg"))
	aptr->fifflg = sqlGetLong(res, row, "fifflg");
    if (!sqlIsNull(res, row, "conflg"))
	aptr->conflg = sqlGetLong(res, row, "conflg");

    /* Push this on to the list */
    if (gAreaList)
    {
	aptr->next = gAreaList;
    }
    gAreaList = aptr;

    sqlFreeResults(res);

    return(aptr);
}

long lm_WriteDeferredTransaction(LMS_TRANSACTION_INFO *lms)
{
    char buffer[4000];
    long ret_status;

    char *collst = NULL;
    char *vallst = NULL;
    char tmpvar[1000];

    /*
     * Build the column and value list for the insert below.  Note,
     * to properly process strings with single quotes in them, i.e. prtdsc,
     * we use the BuildInsertList function.
     */
 
    if (eOK != appBuildInsertListDBKW ("lms_trn_id", "@nxtnum", 0,
                                       &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }


    if (eOK != appBuildInsertList ("adrnum", lms->adrnum, ADRNUM_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("lmscod", lms->lmscod, LMSCOD_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("lmstyp", lms->lmstyp, LMSTYP_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("lmswrk", lms->lmswrk, LMSWRK_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("actcod", lms->actcod, ACTCOD_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("oprcod", lms->oprcod, OPRCOD_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->palqty);
    if (eOK != appBuildInsertListDBKW ("palqty", tmpvar, 0,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->casqty);
    if (eOK != appBuildInsertListDBKW ("casqty", tmpvar, 0,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->inpqty);
    if (eOK != appBuildInsertListDBKW ("inpqty", tmpvar, 0,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->untqty);
    if (eOK != appBuildInsertListDBKW ("untqty", tmpvar, 0,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("prtnum", lms->prtnum, PRTNUM_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("prtdsc", lms->prtdsc, LNGDSC_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("prt_client_id", lms->prt_client_id, 
                                   CLIENT_ID_LEN, &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%8.2f", lms->untvol);
    if (eOK != appBuildInsertListDBKW ("untvol", tmpvar, 0,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%8.2f", lms->untwgt);
    if (eOK != appBuildInsertListDBKW ("untwgt", tmpvar, 0,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("usr_id", lms->usr_id, USR_ID_LEN,
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "nvl(to_date(@begdte), sysdate)");
    if (eOK != appBuildInsertListDBKW ("begdte", tmpvar, 0, &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "to_date(@enddte)");
    if (eOK != appBuildInsertListDBKW ("enddte", tmpvar, 0, &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("lodnum", lms->lodnum, LODNUM_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("subnum", lms->subnum, SUBNUM_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("dtlnum", lms->dtlnum, DTLNUM_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("dstlod", lms->dstlod, LODNUM_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("dstsub", lms->dstsub, SUBNUM_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("wh_id", lms->wh_id, WH_ID_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }
    
    if (eOK != appBuildInsertList ("srcare", lms->srcare, ARECOD_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("srcloc", lms->srcloc, STOLOC_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("dstare", lms->dstare, ARECOD_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("dstloc", lms->dstloc, STOLOC_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    if (eOK != appBuildInsertList ("devcod", lms->devcod, DEVCOD_LEN, 
                                   &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->hldflg);
    if (eOK != appBuildInsertListDBKW ("hldflg", tmpvar, 0, &collst, &vallst))
    {
        if (collst) free (collst);
        if (vallst) free (vallst);
        return (eNO_MEMORY);
    }

    sprintf(buffer,"validate lm work where devcod = '%s' catch(@?) "
                   "| "
                   "if (@? = 0) "
                   "{"
                   "    complete lms activity where usr_id = '%s' "
                   "    | "
                   "    generate next number where numcod = '%s' "
                   "    | "
                   "    [insert into lmstrn "
                   "       (%s) values (%s)] "
                   "}",
                   lms->devcod, lms->usr_id, NUMCOD_LMS_TRN_ID,
                   collst, vallst);

    ret_status = srvInitiateInline(buffer, NULL);
    if (collst) free (collst);
    if (vallst) free (vallst);
    return(ret_status);
}

long lm_WriteLMSActuals(LMS_TRANSACTION_INFO *lms)
{
    char buffer[5000];
    long ret_status;

    char *wherelst = NULL;
    char tmpvar[1000];

    /*
     * Build our where clause.  NOTE:  To properly process strings with
     * quotes in them, i.e. prtdsc, we use the misBuildWhereList function.
     */

    if (eOK != misBuildWhereList ("lmswrk", lms->lmswrk, LMSWRK_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("actcod", lms->actcod, ACTCOD_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("oprcod", lms->oprcod, OPRCOD_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("lmstyp", lms->lmstyp, LMSTYP_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("lmscod", lms->lmscod, LMSCOD_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("lodnum", lms->lodnum, LODNUM_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("subnum", lms->subnum, SUBNUM_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("dtlnum", lms->dtlnum, DTLNUM_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("wh_id", lms->wh_id, WH_ID_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("dstlod", lms->dstlod, LODNUM_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("dstsub", lms->dstsub, SUBNUM_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("srcare", lms->srcare, ARECOD_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("srcloc", lms->srcloc, STOLOC_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("dstare", lms->dstare, ARECOD_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("dstloc", lms->dstloc, STOLOC_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("prtnum", lms->prtnum, PRTNUM_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("prt_client_id", lms->prt_client_id, 
                                  CLIENT_ID_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("prtdsc", lms->prtdsc, LNGDSC_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->palqty);
    if (eOK != misBuildWhereList ("palqty", tmpvar, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->casqty);
    if (eOK != misBuildWhereList ("casqty", tmpvar, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->inpqty);
    if (eOK != misBuildWhereList ("inpqty", tmpvar, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%ld", lms->untqty);
    if (eOK != misBuildWhereList ("untqty", tmpvar, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%8.2f", lms->untwgt);
    if (eOK != misBuildWhereList ("untwgt", tmpvar, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    sprintf (tmpvar, "%8.2f", lms->untvol);
    if (eOK != misBuildWhereList ("untvol", tmpvar, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("adrnum", lms->adrnum, ADRNUM_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("usr_id", lms->usr_id, USR_ID_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("devcod", lms->devcod, DEVCOD_LEN, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("begdte", lms->begdte, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    if (eOK != misBuildWhereList ("enddte", lms->enddte, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }
  
    sprintf (tmpvar, "%ld", lms->accum_flg);
    if (eOK != misBuildWhereList ("accum_flg", tmpvar, 0, &wherelst))
    {
        if (wherelst) free (wherelst);
        return (eNO_MEMORY);
    }

    sprintf(buffer,
	    "write lms actuals event "
	    " where %s ",
	    wherelst);
	    
    ret_status = srvInitiateInline(buffer, NULL);
    if (wherelst) free (wherelst);
    return(ret_status);
}
    

long lm_GetArea(char *arecod_i, char *stoloc_i, char *arecod_o,
                char *wh_id_i)
{
    char buffer[500];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;

    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
    {
	misTrimcpy(arecod_o, arecod_i, ARECOD_LEN);
    }
    else
    {
	sprintf(buffer,
		"select arecod "
                "  from locmst "
                " where stoloc = '%s'"
                "   and wh_id  = '%s' ",
		stoloc_i,
                wh_id_i);

	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return(ret_status);
	}
	else
	{
	    row = sqlGetRow(res);
	    misTrimcpy(arecod_o, sqlGetValue(res, row, "arecod"), ARECOD_LEN);
	}
	sqlFreeResults(res);
    }
    return(eOK);
}

long lm_GetUserId(char *usr_id_i, char *usr_id_o)
{
    char *p;

    if (usr_id_i && misTrimLen(usr_id_i, USR_ID_LEN) > 0)
    {
	misTrimcpy(usr_id_o, usr_id_i, USR_ID_LEN);
    }
    else
    {
        if ((p = osGetVar(LESENV_USR_ID)) != NULL)
	{
	    misTrimcpy(usr_id_o, p, USR_ID_LEN);
	}
    }
    return eOK;
}

/* 
 * Returns TRUE if an RDT or Voice location or in Direct Cross Dock area
 * 
 *  The built in assumption here is that there is an entry
 *  in the devmst where DEVCOD = STOLOC for every RDT device
 *
 */
long lm_IsLocationLMTracked(char *stoloc,
                            char *wh_id)
{
    char buffer[2000];
    long ret_status;

    sprintf(buffer,
	    "select 1 "
	    "  from devmst "
	    " where devcod = '%s' "
	    "   and wh_id  = '%s' "
	    "   and (devcls = '%s' " 
	    "    or  devcls = '%s')",
	    stoloc, 
	    wh_id,
	    DEVCLS_RDTS, 
	    DEVCLS_VOICE);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
		return(1==1);
    else
	return(1==0);
}

/*
 * A dynamic area is one in which we routinely generate new locations
 * ...since LMS doesn't deal well with new locations, we suppress sending
 * the stoloc and instead send the area code for a dynamic location
 */
long lm_IsDynamicArea(char *arecod, char *wh_id)
{
    AREA_INFO_LIST *aptr;

    aptr = sFindArea(arecod, wh_id);
    if (!aptr)
	return(1==0);
    
    return(aptr->expflg || aptr->shpflg);
}

/*
 * Assumption:  we have ONE cycle count adjustment area.  Its value
 * is hard-coded here.
 */
long lm_IsCycleCountMove(char *srcloc, char *dstloc, char *wh_id)
{
    char buffer[2000];
    long ret_status;
    char lostloc [STOLOC_LEN+1];

    mocaDataRes *res;
    mocaDataRow *row;

    /* If they're moving it to a lost loc, it's the same */
    /* as an adjustment and we don't want to send an event. */

    memset(lostloc, 0, sizeof(lostloc));
    
    sprintf(buffer,
            "select lost_loc dstloc "
            "  from aremst, "
            "       locmst "
            " where aremst.arecod = locmst.arecod "
            "   and aremst.wh_id = locmst.wh_id "
            "   and locmst.stoloc = '%s' "
            "   and locmst.wh_id = '%s' "
            "   and aremst.lost_loc is not null ",
            srcloc,
            wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        row = sqlGetRow(res);
        misTrimcpy(lostloc, sqlGetString(res, row, "dstloc"), STOLOC_LEN);
        sqlFreeResults(res);

        if(misCiStrcmp(dstloc, lostloc) == 0)
        {
            return(1==1);
        }
    }
    else
        sqlFreeResults(res);


    sprintf(buffer,
	    "select 'x' "
	    "  from locmst "
	    " where stoloc in ('%.*s','%.*s') "
            "   and wh_id = '%s' "
	    "   and arecod = '%s'",
            STOLOC_LEN, dstloc, 
	    STOLOC_LEN, srcloc,
            wh_id,
	    ARECOD_CNT_ADJUST);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
        return(1==1);
    else
	return(1==0);
}

long lm_IsFromAutomation(char *usr_id_i)
{
    char usr_id[USR_ID_LEN+1];
    long ret_status;
    char buffer[2000];
    char *p;

    memset(usr_id, 0, sizeof(usr_id));
    if (usr_id_i && misTrimLen(usr_id_i, USR_ID_LEN))
	misTrimcpy(usr_id, usr_id_i, USR_ID_LEN);
    else
    {
	if ((p = osGetVar(LESENV_USR_ID)) != NULL)
	    misTrimcpy(usr_id, p, USR_ID_LEN);
	else
	    return(1==1);  /* Return TRUE if we can't get an usr_id */
    }

    /*
    ** ---------------------------------------------------- 
    ** Just check to see if the usr_id is logged on 
    ** (if not then automated).
    ** ---------------------------------------------------- 
    */

    memset(buffer, 0, sizeof(buffer));
    sprintf(buffer,
	    "select 'x' from dual "
	    " where exists " 
	    "   (select 'x' from devmst "
	    "     where lst_usr_id = '%.*s' )",
	    USR_ID_LEN, usr_id);

    ret_status = sqlExecStr(buffer, NULL);
    if (eOK != ret_status)
	return(1==1);  /* TRUE */
    else
	return(1==0); /* FALSE */
}

long lm_IsWorkstation(char *wh_id_i)
{
    char devcod[DEVCOD_LEN+1];
    char wh_id[WH_ID_LEN + 1];
    long ret_status;
    char buffer[2000];
    char *p;
    mocaDataRes *res;
    mocaDataRow *row;

    memset(wh_id, 0, sizeof(wh_id));
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
	misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    /*
    ** -------------------------------------------------- 
    ** Ignore PCE if the src activity originated from a 
    ** workstation. First get the DEVCOD ...
    ** -------------------------------------------------- 
    */

    if ((p = osGetVar(LESENV_DEVCOD)) == NULL)
    {
	misTrc(T_FLOW, 
	       "Failed to get device from environment - assuming workstation");
	return(1==1);  /* if we can't get a device, assume 
			  it is a WORKSTATION */
    }
    else
    {
	misTrimcpy(devcod, p, DEVCOD_LEN);
    }
    
    /*
    ** -------------------------------------------------- 
    ** ...then find out if it is a workstation. 
    ** -------------------------------------------------- 
    */
    memset(buffer, 0, sizeof(buffer));
    sprintf(buffer,
	    " select devcls "
	    "   from devmst"
	    "  where devcod = '%.*s' "
            "    and wh_id  = '%s' ",
	    DEVCOD_LEN, devcod,
            wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	misTrc(T_FLOW, 
	       "Device: %s not found on devmst, assuming it is workstation",
	       devcod);
	return(1 == 1);
    }
    else
    {
	row = sqlGetRow(res);
	if (strncmp(DEVCLS_TERM, 
		    sqlGetString(res, row, "devcls"), DEVCLS_LEN) == 0)
	{
	    sqlFreeResults(res);
	    return(1==1);
	}
	else
	{
	    sqlFreeResults(res);
	    return(1==0);
	}
    }
}

long lm_IsRdt(char *devcod_i,
              char *wh_id_i)
{
    char devcod[DEVCOD_LEN+1];
    char wh_id[WH_ID_LEN + 1];
    char buffer[2000];
    long ret_status;
    char *p;

    memset(devcod, 0, sizeof(devcod));
    memset(wh_id, 0, sizeof(wh_id));

    if (devcod_i && misTrimLen(devcod_i, DEVCOD_LEN))
    {
	misTrimcpy(devcod, devcod_i, DEVCOD_LEN);
    }
    else
    {
	if ((p = osGetVar(LESENV_DEVCOD)) == NULL)
	{
	    misTrc(T_FLOW, "Can't get device, assuming it is not an RDT");
	    return(1==0);
	}
	else
	{
	    misTrimcpy(devcod, p, DEVCOD_LEN);
	}
    }

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    
    /*
    ** -------------------------------------------------- 
    ** ...then find out if it is an RDT. 
    ** -------------------------------------------------- 
    */
    memset(buffer, 0, sizeof(buffer));
    sprintf(buffer,
	    "select 'x' from dual "
	    " where exists " 
	    "  (select 'x' "
	    "     from devmst"
	    "    where devcod = '%.*s'"
            "      and wh_id  = '%s' "
	    "      and devcls = '%s')",
	    DEVCOD_LEN, devcod, 
            wh_id,
            DEVCLS_RDTS);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
	return(1==1);
    else
	return(1==0);
}

long lm_IsRDTEmpty(char *devcod_i,
                   char *wh_id_i)
{
    char devcod[DEVCOD_LEN+1];
    char wh_id[WH_ID_LEN + 1];
    char buffer[2000];
    long ret_status;

    memset(devcod, 0, sizeof(devcod));
    memset(wh_id, 0, sizeof(wh_id));

    misTrimcpy(devcod, devcod_i, DEVCOD_LEN);
    misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

    sprintf(buffer,
	    "select 1 "
	    "  from invdtl, invsub, invlod "
	    " where invdtl.subnum = invsub.subnum "
	    "   and invsub.lodnum = invlod.lodnum "
            "   and invlod.wh_id  = '%s' "
	    "   and invlod.stoloc = '%s'", 
            wh_id,
            devcod);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
	return(0==1);
    else
	return(1==1);
}

long lm_IsDirectCrossDockMove(char *arecod, char *wh_id)
{
    AREA_INFO_LIST *aptr;
    char sqlbuffer[500];
    long ret_status;

    aptr = NULL;    
    aptr = sFindArea(arecod, wh_id);
    if (!aptr)
        return(1==0);

    if (aptr->xdaflg)
    {
       /*
        ** If this is a cross-dock area, find out if it really
        ** exists or if it's just used for direct cross-docking
        */
        sprintf(sqlbuffer,
                "select 1 "
                "  from poldat_view "
                " where polcod = '%s' "
                "   and rtstr1 = '%s' "
                "   and ((polvar = '%s') "
                "        or (polvar = '%s' and polval = '%s')) "
                "   and wh_id  = '%s' ",
                POLCOD_CROSS_DOCKING, aptr->arecod,
                POLVAR_DROPOFF_AREA, POLVAR_MISC, POLVAL_DEFAULT_DROPOFF_AREA,
                wh_id);
        ret_status = sqlExecStr(sqlbuffer, NULL);
        if (ret_status == eOK)
        {
            misTrc(T_FLOW, 
    	       "Skipping move from/to direct crossing area");
            return(1==1);
        }
    }

    return(1==0);
}

long lm_IsIdentificationMove(char *srcare, char *dstare,
                             char *wh_id)
{
    AREA_INFO_LIST *sptr, *dptr;

    sptr = sFindArea(srcare, wh_id);
    if (!sptr)
	return(1==0);

    dptr = sFindArea(dstare, wh_id);
    if (!dptr)
	return(1==0);

    /*
    ** ---------------------------------------------------------------
    ** We skip any moves which occur to/from adjustments and to
    ** expected receipts...this gets covered with the identification
    ** event 
    **
    ** If a move is within expected receipts, we go ahead and let
    ** that through.  The case for needing this was when building
    ** collections prior to receipt (Pella).  
    ** ---------------------------------------------------------------
    */
    if ((sptr->expflg || sptr->adjflg) &&
	(dptr->expflg || dptr->adjflg))
    {
	if (sptr->expflg && dptr->expflg)
	    return(1==0);

	misTrc(T_FLOW, 
	       "Skipping move from/to adjustments to expected receipts");
	return(1==1);
    }

    return(1==0);
}

long lm_IsShipStagingArea(char *arecod,
                          char *wh_id)
{
    AREA_INFO_LIST *aptr;

    aptr = sFindArea(arecod,wh_id);
    if (!aptr)
	return(1==0);

    return(aptr->stgflg);
}

long lm_IsShipmentArea(char *arecod, 
                       char *wh_id)
{
    AREA_INFO_LIST *aptr;

    aptr = sFindArea(arecod, wh_id);
    if (!aptr)
	return(1==0);

    return(aptr->shpflg);
}

long lm_IsShipAdjustmentMove(char *arecod, 
                             char *stoloc,
                             char *wh_id)
{
    AREA_INFO_LIST *aptr;

    aptr = sFindArea(arecod,wh_id);
    if (!aptr)
	return(1==0);

    if (aptr->adjflg)
    {
	if (strncmp(stoloc, STOLOC_PERM_SHPADJ, STOLOC_LEN) == 0)
	    return(1==1);
    }
    
    return(1==0);
}
