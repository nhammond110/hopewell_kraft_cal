static const char *rcsid = "$Id: intAllocatePickGroup.c 167034 2008-08-21 05:59:53Z bli $";
/*#START***********************************************************************
 *  Copyright (c) 2004 RedPrairie Corporation. All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <applib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <trnlib.h>

#include "intlib.h"

/* !!!!!!!!!JJS - testing !!!!!!!!!!*/

/* JJS 06/13/2009 - Added car_move_id to WORKING_STRUCTURE */
typedef struct _WorkingStruct 
{
    char        wh_id[WH_ID_LEN+1];
    char        prtnum[PRTNUM_LEN+1];
    char        prt_client_id[CLIENT_ID_LEN+1];
    char        orgcod[ORGCOD_LEN+1];
    char        revlvl[REVLVL_LEN+1];
    char        lotnum[LOTNUM_LEN+1];
    char        invsts_prg[INVSTS_PRG_LEN+1];
    char        dstare[ARECOD_LEN+1];
    char        dstloc[STOLOC_LEN+1];
    long        pckqty;
    char        client_id[CLIENT_ID_LEN + 1];
    char        ordnum[ORDNUM_LEN+1];
    char        ordlin[ORDLIN_LEN+1];
    char        ordsln[ORDSLN_LEN+1];
    char        stcust[ADRNUM_LEN+1];
    char        rtcust[ADRNUM_LEN+1];
    char        carcod[CARCOD_LEN+1];
    char        srvlvl[SRVLVL_LEN+1];
    char        marcod[MARCOD_LEN+1];
    char        ordtyp[ORDTYP_LEN+1];
    char        distro_id[DISTRO_ID_LEN+1];
    char        alc_search_path[ALC_SEARCH_PATH_LEN+1];
    char        supnum[SUPNUM_LEN+1];
    short       IsDistro;
    moca_bool_t    parflg;
    moca_bool_t    rpqflg;
    moca_bool_t    splflg;
    moca_bool_t    stdflg;
    moca_bool_t    frsflg;
    moca_bool_t    non_alc_flg;
    moca_bool_t    xdkflg;
    moca_bool_t    xdk_inserted;
    char        entdte[DB_STD_DATE_LEN+1];
    char        concod[CONCOD_LEN+1];
    char        ship_id[SHIP_ID_LEN+1];
    char        ship_line_id[SHIP_LINE_ID_LEN+1];
    char        stop_id[STOP_ID_LEN+1];
    char        car_move_id[CAR_MOVE_ID_LEN+1];
    char        prcpri[PRCPRI_LEN+1];
    long        untpal;
    long        untcas;
    double      casvol;
    long    alccas;
    long    alcpak;
    long    alcpal;
    long        min_shelf_hrs;
    char        frsdte[MOCA_STD_DATE_LEN + 1];
    struct      _WorkingStruct *prev;
    struct      _WorkingStruct *next;
} WORKING_STRUCTURE;



/*
 * The following structure holds a list of failed allocation
 * attempts - meaning we could not allocate what was in this
 * list.  This fits in to the PIP allocation method and is
 * used to speed the allocation process.
 */
typedef struct failed_allocs
{
    char wh_id[WH_ID_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char allocate_type[40];
    char revlvl[REVLVL_LEN+1];
    char lotnum[LOTNUM_LEN+1];
    char orgcod[ORGCOD_LEN+1];
    char invsts_prg[INVSTS_PRG_LEN+1];
    char dstloc[STOLOC_LEN+1];
    char dstare[ARECOD_LEN+1];
    char supnum[SUPNUM_LEN+1];
    long splflg;
    long untcas;
    long untpak;
    long untpal;
    long frsflg;
    long non_alc_flg;
    long min_shelf_hrs;
    char frsdte[MOCA_STD_DATE_LEN + 1];

    struct failed_allocs *next;
} FAILED_ALLOCS;

static long sAddToFailureList(FAILED_ALLOCS **FailedAllocHead,
                              char *wh_id,
                              char *prtnum,
                              char *prt_client_id,
                              char *orgcod,
                              char *revlvl,
                              char *supnum,
                              char *lotnum,
                              char *invsts_prg,
                              char *dstare,
                              char *dstloc,
                              long splflg,
                              long untcas,
                              long untpak, 
                              long untpal,
                              long frsflg,
                              long non_alc_flg,
                              long min_shelf_hrs,
                              char *frsdte)
{
    FAILED_ALLOCS *fa_p;

    fa_p = (FAILED_ALLOCS *)calloc(1, sizeof(FAILED_ALLOCS));
    if (fa_p == NULL)
        return(eNO_MEMORY);

    if (wh_id)  strcpy(fa_p->wh_id, wh_id);
    if (prtnum) strcpy(fa_p->prtnum, prtnum);
    if (prt_client_id)  strcpy(fa_p->prt_client_id, prt_client_id);
    if (orgcod) strcpy(fa_p->orgcod, orgcod);
    if (revlvl) strcpy(fa_p->revlvl, revlvl);
    if (lotnum) strcpy(fa_p->lotnum, lotnum);
    if (invsts_prg) strcpy(fa_p->invsts_prg, invsts_prg);
    if (dstare) strcpy(fa_p->dstare, dstare);
    if (dstloc) strcpy(fa_p->dstloc, dstloc);
    if (supnum) strcpy(fa_p->supnum, supnum);
    fa_p->splflg = splflg;
    fa_p->untcas = untcas;
    fa_p->untpak = untpak;
    fa_p->untpal = untpal;
    fa_p->frsflg = frsflg;
    fa_p->non_alc_flg = non_alc_flg;
    fa_p->min_shelf_hrs = min_shelf_hrs;
    if (frsdte) strcpy(fa_p->frsdte, frsdte);

    if (*FailedAllocHead)
        fa_p->next = *FailedAllocHead;

    *FailedAllocHead = fa_p;

    return(eOK);
    
}
/* Returns TRUE if there is a previous allocation like this 
 * one that has failed (meaning this one is likely to fail as well).
 */
static moca_bool_t sWillAllocFail(FAILED_ALLOCS *FHead,
                                  WORKING_STRUCTURE *request)
{
    FAILED_ALLOCS *fa;

    if (FHead == NULL)
        return(BOOLEAN_FALSE);

    misTrc(T_FLOW, "Optimizing against failed allocation queue");

    for (fa = FHead; fa; fa = fa->next)
    {
        if (strcmp(fa->wh_id, request->wh_id) == 0 &&
            strcmp(fa->prtnum, request->prtnum) == 0 &&
            strcmp(fa->prt_client_id, request->prt_client_id) == 0 &&
            strcmp(fa->lotnum, request->lotnum) == 0 &&
            strcmp(fa->revlvl, request->revlvl) == 0 &&
            strcmp(fa->orgcod, request->orgcod) == 0 &&
            strcmp(fa->dstare, request->dstare) == 0 &&
            strcmp(fa->dstloc, request->dstloc) == 0 &&
            strcmp(fa->invsts_prg, request->invsts_prg) == 0 &&
            fa->splflg == request->splflg &&
            fa->untcas == request->alccas &&
            fa->untpak == request->alcpak &&
            fa->untpal == request->alcpal &&
            fa->frsflg == request->frsflg &&
            fa->non_alc_flg == request->non_alc_flg &&
            fa->min_shelf_hrs == request->min_shelf_hrs &&
            strcmp(fa->frsdte, request->frsdte) == 0 &&
            strcmp(fa->supnum, request->supnum) == 0 )
        {
            misTrc(T_FLOW, 
                   "Found previous allocation attempt which already failed - "
                   "SKIPPING inventory lookup!");
            return(BOOLEAN_TRUE);
        }
    }
    
    misTrc(T_FLOW, "Streaming optimization complete - no savings found");
    return(BOOLEAN_FALSE);
}

static void sFreeFailedAllocList(FAILED_ALLOCS *head)
{
    FAILED_ALLOCS *fa, *last;

    last = NULL;
    for (fa = head; fa; last = fa, fa = fa->next)
    {
        if (last)
            free(last);
    }

    if (last)
        free(last);
    return;
}

static void sFreeWorkingList(WORKING_STRUCTURE *head)
{
    WORKING_STRUCTURE *lastptr, *tmpptr;

    lastptr = NULL;
    for (tmpptr = head; tmpptr; lastptr = tmpptr, tmpptr = tmpptr->next) 
    {
        if (lastptr)
            free (lastptr);
    }
    if (lastptr)
        free(lastptr);

    return;
}

LIBEXPORT
RETURN_STRUCT *varAllocatePickGroup(char *pcktyp_i, 
                                    char *pcksts_i,
                                    char *pcksts_uom_i, 
                                    char *ship_id_i,
                                    char *ship_line_id_i,
                                    char *client_id_i, 
                                    char *ordnum_i,
                                    char *pckgr1_i, char *pckgr2_i, 
                                    char *pckgr3_i, char *pckgr4_i,
                                    char *carcod_i, char *srvlvl_i, 
                                    char *pricod_i,
                                    char *consby_i, 
                                    char *restyp_i, 
                                    char *resval_i,
                                    char *arecod_i, 
                                    char *dstare_i, 
                                    char *dstloc_i,
                                    char *schbat_i, 
                                    char *lblseqstrategy,
                                    moca_bool_t *comflg_i, 
                                    char *batcod_i,
                                    moca_bool_t *rrlflg_i,
                                    char *pipcod_i,
                                    char *wh_id_i)
{
    long ret_status=0L;
    RETURN_STRUCT *CurPtr=NULL, *AllPtrs=NULL,*CurPtr1=NULL, *InvPtr=NULL;
    mocaDataRes *res=NULL, *res1=NULL, *tempres=NULL;
    mocaDataRes *rplres;
    mocaDataRow *rplrow;
    mocaDataRes *invres;
    mocaDataRow *invrow;
    mocaDataRow *row=NULL, *row1=NULL, *temprow=NULL;
    char sqlbuffer[3000];
    long errcode;
    char tmpbuf[50];
    char tmpbuf2[200];
    char pricod_buf[100];
    char saved_ship_id[SHIP_ID_LEN+1];
    char pipcod[PIPCOD_LEN+1];
    char lodnum[LODNUM_LEN+1];
    long loop_cnt = 0;

    FAILED_ALLOCS *FailedAllocHead = NULL;

    char where_clause[2500];
    char temp_clause[300];
    char table_name[20];
    char work_schbat[SCHBAT_LEN+1];
    char batcod[BATCOD_LEN+1];
    char work_concod[CONCOD_LEN+1];
    char *all_schbat = NULL;
    char *all_ship_id = NULL;
    char *all_ship_line_id = NULL;
    char *all_stcust = NULL;
    char *all_rtcust = NULL;
    char *all_client_id = NULL;
    char *all_ordnum = NULL;
    char *all_ordlin = NULL;
    char *all_ordsln = NULL;
    char *all_concod = NULL;
    char *all_segqty = NULL;
    char *all_dstare = NULL;
    char *all_dstloc = NULL;
    char *all_alc_search_path = NULL;
    char *sav_all_schbat = NULL;
    char *sav_all_ship_id = NULL;
    char *sav_all_ship_line_id = NULL;
    char *sav_all_stcust = NULL;
    char *sav_all_rtcust = NULL;
    char *sav_all_client_id = NULL;
    char *sav_all_ordnum = NULL;
    char *sav_all_ordlin = NULL;
    char *sav_all_ordsln = NULL;
    char *sav_all_concod = NULL;
    char *sav_all_segqty = NULL;
    char *sav_all_dstare = NULL;
    char *sav_all_dstloc = NULL;
    char *sav_all_alc_search_path = NULL;
  
    char *schbat_Ptr = NULL;
    char *ship_id_Ptr = NULL;
    char *ship_line_id_Ptr = NULL;
    char *stcust_Ptr = NULL;
    char *rtcust_Ptr = NULL;
    char *client_id_Ptr = NULL;
    char *ordnum_Ptr = NULL;
    char *ordlin_Ptr = NULL;
    char *ordsln_Ptr = NULL;
    char *concod_Ptr = NULL;
    char *segqty_Ptr = NULL;
    char *dstare_Ptr = NULL;
    char *dstloc_Ptr = NULL;
    char *alc_search_path_Ptr = NULL;

    long all_pckqty;
    long rpckqty = 0;
    static long pallet_volume;
    long num_segments;
    WORKING_STRUCTURE   *Top, *ptr, *ShipTop;
    WORKING_STRUCTURE   *lastptr, *tmpptr;
    char dstare[ARECOD_LEN+1];
    char dstloc[STOLOC_LEN+1];
    moca_bool_t comflg;
    moca_bool_t rrlflg;
    char pcksts[PCKSTS_LEN+1];
    char xdkref[XDKREF_LEN+1];
    char wh_id[WH_ID_LEN+1];
    char cmbcod_clause[CMBCOD_LEN + 20];
    
    char consby_in[50];
    static short BreakShipids;
    static short PolXDockInstalled = FALSE;
    static short PolPIProcessing = FALSE;
    short WriteXDock;
    short running_pip_in_parallel;
    long xdk_created = FALSE;
    long add_to_failure;
    long setForXDock = 0;
    moca_bool_t shpdstlocExists = BOOLEAN_FALSE;

    ShipTop = 0;
    lastptr = 0;

    memset(lodnum, 0, sizeof(lodnum));
    memset(batcod, 0, sizeof(batcod));
    memset(pcksts, 0, sizeof(pcksts));
    memset(dstare, 0, sizeof(dstare));
    memset(dstloc, 0, sizeof(dstloc));
    memset(consby_in, 0, sizeof(consby_in));
    memset(wh_id, 0, sizeof(wh_id));
    
    memset(where_clause, 0, sizeof(where_clause));
    memset(table_name, 0, sizeof(table_name));

    strcpy(table_name,"sl");
    
    /* Format the incoming variables */
    
    if (batcod_i && misTrimLen(batcod_i, BATCOD_LEN))
        misTrimcpy(batcod, batcod_i, BATCOD_LEN);
    if (pcksts_i && misTrimLen(pcksts_i, PCKSTS_LEN))
        misTrimcpy(pcksts, pcksts_i, PCKSTS_LEN);
    if (dstare_i && misTrimLen(dstare_i, ARECOD_LEN))
        misTrimcpy(dstare, dstare_i,ARECOD_LEN);
    if (dstloc_i && misTrimLen(dstloc_i, STOLOC_LEN))
        misTrimcpy(dstloc, dstloc_i, STOLOC_LEN);
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    /*!!!!!!!!do I have to do anything here?? car_move_id is 11 chars !!!*/
    if (consby_i && misTrimLen(consby_i, 10))
        strncpy(consby_in, consby_i, misTrimLen(consby_i, sizeof(consby_in)-1));
    /*!!!!!!!temporary trace, just to check */
    misTrc(T_FLOW, "tmpJJSDEBUG consby_in = %s",
           consby_in);

    rrlflg = BOOLEAN_NOTSET;
    if (rrlflg_i)
        rrlflg = *rrlflg_i;

    comflg = BOOLEAN_NOTSET;
    if (comflg_i)
        comflg = *comflg_i;
    else
    {
        /* 
         * If comflg is passed in, then use the passed value.
         * or get the value from policy.
         * In the past, many project teams customized the allocate
         * pick group and hard-coded it to pass a 1 into the C function
         * for the comflg.
         * This policy will prevent those types of customizations.
         * The purpose of this flag is to indicate whether or not 
         * commits occur during allocation, and is often turned on to
         * enhance performance.
         */
        sprintf(sqlbuffer,
                "select rtnum1 from poldat_view "
                "where polcod = '%s' "
                "  and polvar = '%s' "
                "  and polval = '%s' "
                "  and wh_id  = '%s' ",
                POLCOD_ALLOCATE_INV,
                POLVAR_MISC,
                POLVAL_COMMIT_DURING_ALLOC,
                wh_id);
        errcode = sqlExecStr(sqlbuffer,&res);
        if (errcode != eOK && errcode != eDB_NO_ROWS_AFFECTED) 
        {
            sqlFreeResults(res);
            res = NULL;
            return(srvResults(errcode, NULL));
        }
        else if (errcode == eOK)
        {
            row = sqlGetRow(res);
            comflg = sqlGetLong(res, row, "rtnum1");
            sqlFreeResults(res);
            res = NULL;
        }
    }

    /* Either a pick group or schedule batch is required. */
    if ((!pckgr1_i || misTrimLen(pckgr1_i, PCKGR1_LEN) == 0) &&
        (!schbat_i || misTrimLen(schbat_i, SCHBAT_LEN) == 0))
    {
        return (APPMissingArg("pckgr1"));
    }
    
    /* 
     * if schbat is passed in, but wh_id is not passed in, 
     * get wh_id by schbat.
     */
    if ((schbat_i && misTrimLen(schbat_i, SCHBAT_LEN)) &&
        misTrimLen(wh_id, WH_ID_LEN) == 0)
    {
        sprintf(sqlbuffer,
                "select wh_id from pckbat where schbat = '%s' ",
                schbat_i);

        ret_status = sqlExecStr(sqlbuffer, &res);
        if (ret_status == eOK)
        {
            row = sqlGetRow(res);
            misTrimcpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);

            if (res) sqlFreeResults(res);
            res = NULL;
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            if (res) sqlFreeResults(res);
            res = NULL;
            return (APPInvalidArg(schbat_i, "schbat"));
        }
        else
        {
            if (res) sqlFreeResults(res);
            res = NULL;
            return (srvResults(ret_status, NULL));
        }
    }

    if ((pckgr1_i) && strlen(pckgr1_i)) 
    {
        /* if pckgr1 is passed in, wh_id is required too. */
        if (misTrimLen(wh_id, WH_ID_LEN) == 0)
            return APPMissingArg("wh_id");

        sprintf(where_clause,
                "%s.pckgr1 = '%s' and %s.wh_id = '%s' ",
                table_name, pckgr1_i, table_name, wh_id);
    }

    misTrc(T_FLOW, "If got here, we must have gotten warehouse id, wh_id = %s",
           wh_id);

    if ((pckgr2_i) && strlen(pckgr2_i)) 
    {
        if (strlen(where_clause)==0)
            sprintf(where_clause,"%s.pckgr2 = '%s' ",table_name,pckgr2_i);
        else 
        {
            sprintf(temp_clause,"and %s.pckgr2 = '%s' ",table_name,pckgr2_i);
            strcat(where_clause, temp_clause);
        }
    }
    if ((pckgr3_i) && strlen(pckgr3_i)) 
    {
        if (strlen(where_clause)==0)
            sprintf(where_clause,"%s.pckgr3 = '%s' ",table_name,pckgr3_i);
        else 
        {
            sprintf(temp_clause,"and %s.pckgr3 = '%s' ",table_name,pckgr3_i);
            strcat(where_clause, temp_clause);
        }
    }
    if ((pckgr4_i) && strlen(pckgr4_i)) 
    {
        if (strlen(where_clause)==0)
            sprintf(where_clause,"%s.pckgr4 = '%s' ",table_name,pckgr4_i);
        else 
        {
            sprintf(temp_clause,"and %s.pckgr4 = '%s' ",table_name,pckgr4_i);
            strcat(where_clause, temp_clause);
        }
    }

    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
    {
        if (strlen(where_clause)==0)
            sprintf(where_clause, "%s.ship_id = '%s' ", table_name, ship_id_i);
        else 
        {
            sprintf(temp_clause, 
                    "and %s.ship_id = '%s' ", 
                    table_name, 
                    ship_id_i);
            strcat(where_clause, temp_clause);
        }
    }

    if (ship_line_id_i && misTrimLen(ship_line_id_i, SHIP_LINE_ID_LEN))
    {
        if (strlen(where_clause)==0)
            sprintf(where_clause,
                    "%s.ship_line_id = '%s' ",
                    table_name,
                    ship_line_id_i);
        else 
        {
            sprintf(temp_clause,
                    "and %s.ship_line_id = '%s' ",
                    table_name,
                    ship_line_id_i);
            strcat(where_clause, temp_clause);
        }
    }

    if (schbat_i && misTrimLen(schbat_i, SCHBAT_LEN))
    {
        if (strlen(where_clause)==0)
            sprintf(where_clause,"%s.schbat = '%s' ",table_name,schbat_i);
        else 
        {
            sprintf(temp_clause,"and %s.schbat = '%s' ",table_name,schbat_i);
            strcat(where_clause, temp_clause);
        }
    }

    if (strlen(where_clause)==0) 
    {
        misTrc(T_FLOW, "Missing data to query shipemnts with");
        return(srvResults(eINVALID_ARGS, NULL));
    }

    /* The strncmp checks to make sure if we got a consby, it's one 
     * of the following values.  However, if we didn't get one at all
     * we get the message 'Invalid value [] for consby.  Therefore,
     * we will check first to see if it is null, so we can give a 
     * better message.  If we ever decide that consby is not needed,
     * we should remove the next if, and fix the if with the strncmp
     * to ignore a null value.
     */

    if (strlen(consby_in) == 0)
        return (APPMissingArg("consby"));

    if (misCiStrncmp(consby_in,"ship_id",7) !=0 &&
        misCiStrncmp(consby_in,"stop_id",7) !=0 &&
        misCiStrncmp(consby_in,"car_move_id",11) != 0 &&
        misCiStrncmp(consby_in,"rtcust",6) !=0 &&
        misCiStrncmp(consby_in,"stcust",6) != 0 &&
        misCiStrncmp(consby_in,"ordnum",6) != 0 &&
        misCiStrncmp(consby_in,"all",3) != 0)
        return (APPInvalidArg(consby_in, "consby"));

    /* Added as a point to trigger off of */
    CurPtr = NULL;
    errcode = srvInitiateCommandFormat (NULL,
            "register pick group allocation "
            " where pckgr1 = '%s' "
            "   and pckgr2 = '%s' "
            "   and pckgr3 = '%s' "
            "   and pckgr4 = '%s' "
            "   and schbat = '%s' "
            "   and wh_id  = '%s' ",
            pckgr1_i ? pckgr1_i : "",
            pckgr2_i ? pckgr2_i : "",
            pckgr3_i ? pckgr3_i : "",
            pckgr4_i ? pckgr4_i : "",
            schbat_i ? schbat_i : "",
            wh_id);

    if (errcode != eOK)
        return (srvResults(errcode, NULL));

    running_pip_in_parallel = FALSE;
    if (appIsInstalledWhID(POLCOD_PRE_INV_PICKING, wh_id))
    {
        PolPIProcessing = TRUE;
        sprintf(sqlbuffer,
                "select 1 from poldat_view "
                "where polcod = '%s' "
                " and polval = '%s' "
                " and polvar = '%s' "
                " and wh_id = '%s' "
                " and rtnum1 = 1",
                POLCOD_PRE_INV_PICKING,
                POLVAL_PARALLEL_ALLOC,
                POLVAR_OPERATING_MODE,
                wh_id);
        errcode = sqlExecStr(sqlbuffer,NULL);
        if (errcode == eOK)
        {
            running_pip_in_parallel = TRUE;
        }
    }

    if (pallet_volume < 1) 
    {
        pallet_volume = 120960;
        sprintf(sqlbuffer,
                "select rtnum1 from poldat_view "
                "where polcod = '%s' "
                " and polval = '%s' "
                " and wh_id = '%s' ",
                POLCOD_PROC_PICK_REL,
                POLVAL_REL_PALLET_VOLUME,
                wh_id);
        errcode = sqlExecStr(sqlbuffer,&res);
        if (errcode == eOK) 
        {
            row = sqlGetRow(res);
            pallet_volume = sqlGetLong(res,row,"rtnum1");
        }
        if (res)    sqlFreeResults(res);
        res = NULL;

        /*
        ** Check if cross docking is installed
        */
        if (appIsInstalledWhID(POLCOD_CROSS_DOCKING, wh_id))
        {
            PolXDockInstalled = TRUE;
        }
    }

    /* If we got a carrier code sent in, then use it to update */
    if ((carcod_i) && strlen(carcod_i)) 
    {
        sprintf(sqlbuffer,
                "update shipment "
                "   set carcod = '%s' ",
                carcod_i);
        /* 
         * Make sure we only update the service level if it also 
         * get passed.
         */

        memset(tmpbuf2, 0, sizeof(tmpbuf2));
        if ((srvlvl_i) && misTrimLen(srvlvl_i, SRVLVL_LEN))
        {
            sprintf(tmpbuf2, ", srvlvl = '%s' ", srvlvl_i);
            strcat(sqlbuffer, tmpbuf2);
        }

        sprintf(tmpbuf2,
                " where (ship_id) in "
                " (select distinct ship_id "
                "    from shipment_line sl where %s ) ",
                where_clause);
        strcat(sqlbuffer, tmpbuf2);

        errcode = sqlExecStr(sqlbuffer,NULL);
        if (errcode != eOK) 
            return(srvResults(errcode, NULL));
    }

    memset (pipcod, 0, sizeof(pipcod));
    if ((pipcod_i) && strlen(pipcod_i)) 
    {
        strncpy (pipcod, pipcod_i, PIPCOD_LEN);
    }

    /* If we got a release remaining flag sent in, then use it to update */
    if (rrlflg != BOOLEAN_NOTSET) 
    {
        sprintf(sqlbuffer,
                "update shipment "
                "   set rrlflg = '%ld' "
                " where (ship_id) in "
                " (select distinct ship_id "
                "    from shipment_line sl where %s ) ",
                rrlflg,
                where_clause);
        errcode = sqlExecStr(sqlbuffer,NULL);
        if (errcode != eOK) 
            return(srvResults(errcode, NULL));
    }

    /* Get any pckqty from rplwrk by the pick group created where clause */
    sprintf(sqlbuffer,
            "select r.ship_line_id, sum(r.pckqty) rpl_pckqty "
            "  from rplwrk r, shipment_line sl "
            " where %s "
            "   and r.ship_line_id = sl.ship_line_id "
            " group by r.ship_line_id",
            where_clause);
    errcode = sqlExecStr(sqlbuffer, &rplres);
    if (errcode != eOK && errcode != eDB_NO_ROWS_AFFECTED) 
    {
        sqlFreeResults(rplres);
        rplres = NULL;
        return(srvResults(errcode, NULL));
    }

    /* Select all of the information from the shipment and order line tables */
    /* by the pick group created where clause */
    /* Add to_number to select below because nvl(sl.pckqty,0) gets stored as */
    /* different datatypes in sqlserver and oracle.  See PR 33318 */
    /* JJS 06/13/2009 - Outer join to stop to get car_move_id */

    sprintf(sqlbuffer,
	    " exec sql with lock "
	    " where main_table   = \"shipment_line sl\" "
            " and   main_pk      = \"sl.ship_line_id\" "
            " and   sql_select   = \"select sl.wh_id, sl.client_id, sl.ordnum, "
            "       sl.prcpri, cast(nvl(sl.pckqty,0) as float)  pckqty, "
            "       sl.ordlin, sl.ordsln, sl.ship_line_id, "
            "       sh.carcod, sh.srvlvl, "
            "       sh.ship_id, sh.doc_num, "
            "       o.stcust, o.rtcust, o.ordtyp, "
            "       ol.prtnum, ol.prt_client_id, "
            "       ol.orgcod, ol.revlvl, ol.supnum, ol.lotnum, "
            "       ol.marcod, ol.invsts_prg, ol.splflg, "
            "       ol.parflg, ol.rpqflg, ol.stdflg, "
            "       ol.xdkflg, ol.entdte, "
            "       ol.frsflg, ol.non_alc_flg, ol.alc_search_path, "
            "       nvl(ol.min_shelf_hrs, -1) min_shelf_hrs, "
            "       nvl(ol.untcas, 0) alccas, "
            "       nvl(ol.untpak, 0) alcpak, " 
            "       nvl(ol.untpal, 0) alcpal, " 
            "       p.untpal, p.untcas, sh.stop_id, "
            "       stop.car_move_id, "
            "       ol.distro_id, "
            "       (f.caslen * f.cashgt * f.caswid) casvol \" "
            " and sql_from_where = \" from ftpmst f, prtmst_view p, shipment sh, shipment_line sl, "
            "            ord o, ord_line ol, stop "
            "       where %s "
            "         and nvl(sl.pckqty,0) > 0 "
            "         and sl.linsts    in ('%s','%s') "
            "         and sh.ship_id          = sl.ship_id "
            "         and sh.stgdte is null "
            "         and sl.client_id        = o.client_id "
            "         and sl.ordnum           = o.ordnum "
            "         and sl.wh_id            = o.wh_id "
            "         and sl.client_id        = ol.client_id "
            "         and sl.ordnum           = ol.ordnum "
            "         and sl.ordlin           = ol.ordlin "
            "         and sl.ordsln           = ol.ordsln "
            "         and sl.wh_id            = ol.wh_id "
            "         and ol.prtnum           = p.prtnum "
            "         and ol.wh_id            = p.wh_id "
            "         and ol.prt_client_id    = p.prt_client_id "
            "         and sh.stop_id = stop.stop_id(+) "
            "         and p.ftpcod            = f.ftpcod \" "
            " and sql_remainder  = \" order by o.wh_id, o.stcust, ol.distro_id, sh.carcod, sh.srvlvl, "
            "                sl.ship_id, "
            "                sl.client_id, "
            "                sl.ordnum, ol.prtnum, "
            "                ol.prt_client_id, "
            "                ol.wh_id \" ",
            where_clause,
            LINSTS_PENDING,
            LINSTS_INPROCESS);

    errcode = srvInitiateInline(sqlbuffer,&CurPtr1);
    if (errcode != eOK) 
    {
 
        srvFreeMemory(SRVRET_STRUCT, CurPtr1);
        CurPtr1 = NULL;
        sqlFreeResults(rplres);
        rplres = NULL;
        return(srvResults(errcode, NULL));
    }
	 
    res = srvGetResults(CurPtr1); 

    if (sqlGetNumRows(rplres) > 0)
    {
        /* We have to take into consideration any outstanding replenishment
         * quantity and decrement against matching ship_line_id's
         */
        misTrc(T_FLOW, 
               "Attempting to adjust pckqty for %d lines "
               "which have outstanding replenishments",
               sqlGetNumRows(rplres));

        for (rplrow = sqlGetRow(rplres); rplrow; 
             rplrow = sqlGetNextRow(rplrow))
        {
            long found;
            char ship_line_id[SHIP_LINE_ID_LEN+1];

            memset(ship_line_id, 0, sizeof(ship_line_id));
            strncpy(ship_line_id, 
                    sqlGetString(rplres, rplrow, "ship_line_id"), 
                    SHIP_LINE_ID_LEN);

            found = 0;
            for (row = sqlGetRow(res); row && !found; row = sqlGetNextRow(row))
            {
                if (strncmp(ship_line_id, 
                            sqlGetString(res, row, "ship_line_id"),
                            SHIP_LINE_ID_LEN) == 0)
                {
                    long pckqty;
                    long r_pckqty;

                    pckqty = sqlGetLong(res, row, "pckqty");
                    r_pckqty = sqlGetLong(rplres, rplrow, "rpl_pckqty");

                    misTrc(T_FLOW,
                           "Decrementing original pckqty of %d "
                           "by %d due to replens",
                           pckqty, r_pckqty);

                    pckqty -= r_pckqty;

                    if (pckqty < 0)
                    {
                        misTrc(T_FLOW,
                               "Warning: adjusted pick qty "
                               "for ship_line_id(%s) is < 0, (%d)",
                               ship_line_id, pckqty);
                        misTrc(T_FLOW, 
                               "Resetting to zero");
                        pckqty = 0;
                    }
                    found = 1;

                    sqlSetFloat(res, row, "pckqty", (double) pckqty);
                    misTrc(T_FLOW,
                           "Reset pick quantity - value is now: %d",
                           sqlGetLong(res, row, "pckqty"));
                }
            }
        }
    }
    sqlFreeResults(rplres);
    rplres = NULL;

    /* Load them up into our structure */
    Top = NULL;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row)) 
    {
        /* Build the linked list */
        if (Top == NULL) 
        {
            Top = (WORKING_STRUCTURE *) calloc(1, sizeof(WORKING_STRUCTURE));
            if (Top == NULL)
                return(srvResults(eNO_MEMORY, NULL));
            ptr = Top;
            memset(ptr,0,sizeof(WORKING_STRUCTURE));
        }
        else 
        {
            ptr = (WORKING_STRUCTURE *) calloc(1, sizeof(WORKING_STRUCTURE));
            if (ptr == NULL)
                return(srvResults(eNO_MEMORY, NULL));
            memset(ptr,0,sizeof(WORKING_STRUCTURE));
            ptr->prev = lastptr;
            lastptr->next = ptr;
        }
        lastptr = ptr;

        /* Now load up the structure */
        strncpy(ptr->wh_id,sqlGetString(res,row,"wh_id"),WH_ID_LEN);
        strncpy(ptr->prtnum,sqlGetString(res,row,"prtnum"),PRTNUM_LEN);
        strncpy(ptr->prt_client_id,sqlGetString(res,row,"prt_client_id"),
                                                               CLIENT_ID_LEN);
        strncpy(ptr->orgcod,sqlGetString(res,row,"orgcod"),ORGCOD_LEN);
        strncpy(ptr->revlvl,sqlGetString(res,row,"revlvl"),REVLVL_LEN);
        strncpy(ptr->supnum,sqlGetString(res,row,"supnum"),SUPNUM_LEN);
        strncpy(ptr->lotnum,sqlGetString(res,row,"lotnum"),LOTNUM_LEN);
        strncpy(ptr->ordtyp,sqlGetString(res,row,"ordtyp"),ORDTYP_LEN);
        strncpy(ptr->invsts_prg,
                sqlGetString(res,row,"invsts_prg"),
                INVSTS_PRG_LEN);
        ptr->pckqty = sqlGetLong (res,row,"pckqty");
        strncpy(ptr->client_id, 
                     sqlGetString(res,row,"client_id"),CLIENT_ID_LEN);
        strncpy(ptr->prcpri,sqlGetString(res,row,"prcpri"),PRCPRI_LEN);
        strncpy(ptr->ordnum,sqlGetString(res,row,"ordnum"),ORDNUM_LEN);
        strncpy(ptr->ordlin,sqlGetString(res,row,"ordlin"),ORDLIN_LEN);
        strncpy(ptr->alc_search_path,
                sqlGetString(res, row, "alc_search_path"), ALC_SEARCH_PATH_LEN);
        strncpy(ptr->ordsln,sqlGetString(res,row,"ordsln"),ORDSLN_LEN);
        strncpy(ptr->stcust,sqlGetString(res,row,"stcust"),STCUST_LEN);
        strncpy(ptr->rtcust,sqlGetString(res,row,"rtcust"),RTCUST_LEN);
        strncpy(ptr->carcod,sqlGetString(res,row,"carcod"),CARCOD_LEN);
        strncpy(ptr->srvlvl,sqlGetString(res,row,"srvlvl"),SRVLVL_LEN);
        strncpy(ptr->marcod,sqlGetString(res,row,"marcod"),MARCOD_LEN);
        strncpy(ptr->stop_id,sqlGetString(res,row,"stop_id"),STOP_ID_LEN);
        strncpy(ptr->car_move_id,sqlGetString(res,row,"car_move_id"),CAR_MOVE_ID_LEN);
        strncpy(ptr->distro_id,sqlGetString(res,row,"distro_id"),DISTRO_ID_LEN);
        if (misTrimLen(ptr->distro_id, DISTRO_ID_LEN) != 0)
            ptr->IsDistro = TRUE;
        else
            ptr->IsDistro = FALSE;
        ptr->parflg = sqlGetBoolean(res,row,"parflg");
        ptr->rpqflg = sqlGetBoolean(res,row,"rpqflg");
        ptr->splflg = sqlGetBoolean(res,row,"splflg");
        ptr->stdflg = sqlGetBoolean(res,row,"stdflg");
        ptr->xdkflg = sqlGetBoolean(res,row,"xdkflg");
        strncpy(ptr->entdte,sqlGetValue(res,row,"entdte"),DB_STD_DATE_LEN);
        ptr->untpal = sqlGetLong(res,row,"untpal");
        if (ptr->untpal == 0)   ptr->untpal = 1;
        ptr->untcas = sqlGetLong(res,row,"untcas");
        if (ptr->untcas == 0)   ptr->untcas = 1;
        ptr->casvol = sqlGetFloat(res,row,"casvol");
        strncpy(ptr->ship_id, sqlGetString(res,row,"ship_id"), SHIP_ID_LEN);
        strncpy(ptr->ship_line_id, sqlGetString(res,row,"ship_line_id"), 
                SHIP_LINE_ID_LEN);
        ptr->alccas = sqlGetLong(res,row,"alccas");
        ptr->alcpak = sqlGetLong(res,row,"alcpak");
        ptr->alcpal = sqlGetLong(res,row,"alcpal");
        ptr->frsflg = sqlGetBoolean(res, row, "frsflg");
        ptr->non_alc_flg = sqlGetBoolean(res, row, "non_alc_flg");
        ptr->min_shelf_hrs = sqlGetLong(res, row, "min_shelf_hrs");
        /* we'll just initialize the xdk_inserted member to be false */
        ptr->xdk_inserted = BOOLEAN_FALSE;

        /*  If the standard case flag is set (Y), and the allocate case value
        **  (untcas on the detail) is zero reset the alccas to the 
        **  untcas value from the prtmst_view.
            TODO:  Does stdflg include the inner pack and pallet also???4-13-98
        */
        if (ptr->stdflg == BOOLEAN_TRUE && (ptr->alccas == 0))
        {
            ptr->alccas = sqlGetLong(res,row,"untcas");
        }

        /* 
         * If we are doing freshness date processing, get the freshness
         * date to use.  It is important for our calculations on the number
         * of segments, etc.
         *
         * This function will set the ptr->frsdte member of the structure.
         */
        if (ptr->frsflg)
        {
            ret_status = trnGetCstFrsDte(ptr->prtnum,
                                         ptr->client_id,
                                         ptr->stcust,
                                         ptr->rtcust,
                                         ptr->frsdte,
                                         ptr->wh_id);
            if (eOK != ret_status)
            {
                /* Exit out of here */

		srvFreeMemory(SRVRET_STRUCT, CurPtr1);
                CurPtr1 = NULL;
                sFreeWorkingList(Top);
                return(srvResults(ret_status, NULL));
            }
        }

        /*
        *  If xdocking and we've assigned a dstloc (stage lane) for 
        *  this shipment, we need to send all the lines to it.
        *  Note this only matters if the lines are set up for cross-docking.
        *  The setForXDock helps keep track of whether we may need to 
        *  eventually update the dstloc on the shipment line, because
        *  event though cross-docking is installed, we will only want
        *  to update the shipment line if the xdkflg on the shipment line
        *  is true.
        */
        /*
         *  If it is a distro-order, we should find empty stagin lane 
         *  for the store.
         */
                
        setForXDock = 0;
        res1 = NULL;
        if (PolXDockInstalled && !ptr->IsDistro )
        {
            /*
             * Need to handle where cross docking flag is set, but
             * we don't yet have the dstloc assigned.
             */

            sprintf(sqlbuffer,
                    "select sdl.wh_id,"
                    "       sdl.dstare,"
                    "       sdl.dstloc "
                    "  from shipment_line sl, "
                    "       ord_line ol, "
                    "       shp_dst_loc sdl "
                    " where sdl.ship_id = '%s' "
                    "   and ol.xdkflg  = '%d' " 
                    "   and sdl.ship_id  = sl.ship_id "
                    "   and sdl.wh_id    = sl.wh_id "
                    "   and sl.client_id = ol.client_id "
                    "   and sl.ordnum    = ol.ordnum "
                    "   and sl.ordlin    = ol.ordlin "
                    "   and sl.ordsln    = ol.ordsln "
                    "   and sl.wh_id     = ol.wh_id ",
                    ptr->ship_id, BOOLEAN_TRUE);

            errcode = sqlExecStr(sqlbuffer, &res1);
            if (errcode == eOK)
            {
                /*
                 * This is a cross-dockable shipment.  If it already has
                 * a dstloc assigned, let's use it.
                 */
                row1 = sqlGetRow(res1);
                if (!sqlIsNull(res1, row1, "dstare"))
                {
                    misTrimcpy(ptr->dstare,
                               sqlGetString(res1,row1,"dstare"), ARECOD_LEN);
                    if (!sqlIsNull(res1, row1, "dstloc"))
                        misTrimcpy(ptr->dstloc,
                                   sqlGetString(res1,row1,"dstloc"), STOLOC_LEN);

                    if (strlen(ptr->dstloc))
                    {
                        misTrc(T_FLOW,
                               "Dest Location assigned to shipment: %s/%s",
                               ptr->wh_id, ptr->dstloc);

                        shpdstlocExists = BOOLEAN_TRUE;
                    }
                }
                setForXDock = 1;
            }
            sqlFreeResults(res1);
            res1 = NULL;
        }
        else if (ptr->IsDistro)
        {
            /*
             * Need to find a empty stagin line for the store.
             */

            sprintf(sqlbuffer,
                    "  select dst_loc.dstare, "
                    "         dst_loc.dstloc "
                    "    from distro "
                    "    left outer join cst_wh_dst_loc dst_loc "
                    "      on dst_loc.cstnum = distro.stcust "
                    "     and dst_loc.wh_id = distro.wh_id "
                    "     and dst_loc.client_id = distro.client_id "
                    "    left outer join locmst "
                    "      on dst_loc.dstloc = locmst.stoloc "
                    "     and dst_loc.wh_id = locmst.wh_id "
                    "   where nvl(locmst.locsts, '%s') = '%s' "
                    "     and distro.distro_id = '%s' "
                    "   order by dst_loc.srtseq ",
                    LOCSTS_EMPTY,
                    LOCSTS_EMPTY,
                    ptr->distro_id);
            errcode = sqlExecStr(sqlbuffer, &res1);
            if (errcode == eOK)
            {
                /*
                 * This store has a dstloc assigned, let's use it.
                 */
                row1 = sqlGetRow(res1);
                if (!sqlIsNull(res1, row1, "dstare"))
                {
                    misTrimcpy(ptr->dstare,
                               sqlGetString(res1,row1,"dstare"), ARECOD_LEN);
                    if (!sqlIsNull(res1, row1, "dstloc"))
                        misTrimcpy(ptr->dstloc,
                                   sqlGetString(res1,row1,"dstloc"), STOLOC_LEN);

                    if (strlen(ptr->dstloc))
                    {
                        misTrc(T_FLOW,
                               "Dest Location assigned to shipment: %s/%s",
                               ptr->wh_id, ptr->dstloc);

                        shpdstlocExists = BOOLEAN_FALSE;
                    }
                }
                else
                {
                    /*
                     * No dest area/loc is set for the store, then we 
                     * check if dest area/loc is set in order lines.
                     */
                    sqlFreeResults(res1);
                    res1 = NULL;
                    sprintf(sqlbuffer,
                        "  select dstare, dstloc "
                        "    from shp_dst_loc    "
                        "   where ship_id = '%s' "
                        "     and wh_id = '%s'   ",
                        ptr->ship_id,
                        ptr->wh_id);
                    errcode = sqlExecStr(sqlbuffer, &res1);
                    if (errcode == eOK)
                    {
                        /*
                         * we get the dstare or dstloc assigned for the shipment
                         */
                        row1 = sqlGetRow(res1);
                        if(!sqlIsNull(res1, row1, "dstare"))
                        {
                            misTrimcpy (ptr->dstare,
                                sqlGetString(res1,row1,"dstare"), ARECOD_LEN);
                            if (!sqlIsNull(res1, row1, "dstloc"))
                                misTrimcpy(ptr->dstloc,
                                    sqlGetString(res1,row1,"dstloc"), 
                                    STOLOC_LEN);
                        }
                    } 
                }
                setForXDock = 1;
            }
            sqlFreeResults(res1);
            res1 = NULL;
        }

        /* If we got a destination area or location ... 
         *       then take the arguments 
         *       else try to use ones already set on the shipment
         * Otherwise, look them up using the GET ORDER DESTINATION command.
         */
        if (strlen(dstare) || strlen(dstloc)) 
        {
            misTrc(T_FLOW,
                   "Destination area %s/%s and location %s/%s were passed",
                   wh_id, strlen(dstare) ? dstare : "",
                   wh_id, strlen(dstloc) ? dstloc : "");
            /*
             * At this point, ptr->dstare is only set if cross docking
             * was installed and there are order lines that are set up
             * for cross docking on the shipment.  If installed, and 
             * this value is set, we have to use it since we may have 
             * other shipment lines
             */

            if (PolXDockInstalled)
            {
                /*
                 * If the user passed something that doesn't match
                 * where we are cross docking to, give them an error.
                 */

                if ((strlen(ptr->dstloc) && strlen(dstloc) &&
                    strncmp(dstloc, ptr->dstloc, STOLOC_LEN)) ||
                    (strlen(ptr->dstare) && strlen(dstare) &&
                     strncmp(dstare, ptr->dstare, ARECOD_LEN)))
                {
                    misTrc(T_FLOW, "Cross docking is set for shipment, and "
                           "dstare/dstloc passed do not match values "
                           "on the shipment. ");
                    srvFreeMemory(SRVRET_STRUCT, CurPtr1);
                    CurPtr1 = NULL;
                    sFreeWorkingList(Top);
                    return(srvResults(eINT_PARM_MISMATCH, NULL));
                }
            }

            /*
             * If we are at this point, if a dstare and dstloc were passed
             * we can use them because either we are not set up for
             * cross docking, or we passed the above check that verified
             * they were equal.
             */

            if (strlen(dstare)) 
            {   
                misTrc(T_FLOW, "Using passed in dstare: %s", dstare);
                strcpy(ptr->dstare,dstare);
            }

            if (strlen(dstloc)) 
            {
                misTrc(T_FLOW, "Using passed in dstloc: %s", dstloc);
                strcpy(ptr->dstloc,dstloc);
            }                         
        }

        /* if we just have the dstloc, get the dstare */
        if ((strlen(ptr->dstloc)) && strlen(ptr->dstare) == 0)
        {
            sprintf(sqlbuffer,
                    "select arecod "
                    "  from locmst "
                    " where stoloc = '%s' "
                    "   and wh_id = '%s' ",
                    ptr->dstloc,
                    ptr->wh_id); 

            errcode = sqlExecStr(sqlbuffer, &res1);
            if (errcode == eOK)
            {
                row1 = sqlGetRow(res1);
                misTrimcpy(ptr->dstare,
                           sqlGetString(res1,row1,"arecod"), ARECOD_LEN);
            }
            sqlFreeResults(res1);
            res1 = NULL;
        }
        
        /*
        * If cross docking is installed, we need to update the shp_dst_loc
        * with the dstare and dstloc specified, because they are used
        * on down the line by cross-docking.  Otherwise, sending the
        * dstare and dstloc at allocation is a 1-time thing, so we won't
        * update the values (in case they were sent by the host or something.
        */

        if ((PolXDockInstalled || ptr->IsDistro) && strlen(ptr->dstloc) 
            && (shpdstlocExists == BOOLEAN_FALSE))
        {
          /* We'll only do this if cross docking is installed.
           * and when the dstloc does not exist shp_dst_loc.
           * There are 2 possbile cases here
           * 1. There is no record in shp_dst_loc for this shipment. We 
           * create for it.
           * 2. There is a record in shp_dst_loc for this shipment, but dstloc
           * on this record is NULL. We should update dstloc. We could not call
           * 'change shipment destination location' because it ONLY works with
           * READY shipment, but shipment is be INPROCESS now.
           * 
           * Notice, it does not work to remove any current records
           * and create the shp_dst_loc record, because 'create shipment 
           * destination location' ONLY works with shipment in READY status, but
           * in 2nd case, shipment will be in INPROCESS status.
           */
                
            sprintf(sqlbuffer,
                "[select dstloc"
                "   from shp_dst_loc "
                "  where ship_id = '%s'] catch(-1403)"
                " | "
                " if(@? = -1403)"
                " { "
                "    create shipment destination location "
                "     where ship_id = '%s' " 
                "       and dstare  = '%s' "
                "       and dstloc  = '%s' "
                "       and wh_id   = '%s' "
                " } "
                " else "
                " { "
                "    if (!@dstloc or @dstloc = '') "
                "    { "
                "       [update shp_dst_loc "
                "           set dstloc = '%s', "
                "               moddte = sysdate, "
                "               mod_usr_id = '%s' "
                "         where ship_id = '%s'] "
                "    } "
                " }",
                ptr->ship_id,
                ptr->ship_id,
                ptr->dstare,
                ptr->dstloc,
                ptr->wh_id,
                ptr->dstloc,
                osGetVar(LESENV_USR_ID)?osGetVar(LESENV_USR_ID):"",
                ptr->ship_id);
            
            errcode = srvInitiateCommand(sqlbuffer,NULL);
    
            if (errcode != eOK)
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr1);
                CurPtr1 = NULL;
                sFreeWorkingList(Top);
                return(srvResults(errcode, NULL));
            }
        }                  

         /*
         * At this point, if we have a dstloc determined, we need
         * to allocate the resource location for it.
         * (since we either have it due to cross-docking, it's on
         * the shipment, or the user specifically specified it.)
         * Otherwise, it won't get allocated until pick release.
         */

        if (strlen(ptr->dstloc) && (PolXDockInstalled || ptr->IsDistro))
        {
            errcode = srvInitiateCommandFormat (NULL,
                    "allocate resource location "
                    " where stoloc  = '%s' "
                    "   and wh_id = '%s' "
                    "   and ship_id = '%s' ",
                    ptr->dstloc,
                    ptr->wh_id,
                    ptr->ship_id);
            if (errcode != eOK) 
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr1);
                CurPtr1 = NULL;
                sFreeWorkingList(Top);
                return(srvResults(errcode, NULL));
            }
        }

         /*
         * It wasn't set on the shipment, and it wasn't passed in,
         * so let's try to determine the order's destination.
         * This will only be done in cross docking situations
         */

        if (strlen(ptr->dstare)==0 
          && (PolXDockInstalled || ptr->IsDistro)
          && setForXDock ==1)
        {
            CurPtr = NULL;
            errcode = srvInitiateCommandFormat (&CurPtr,
                    "get order destination "
                    " where ship_id      = '%s' "
                    "   and ship_line_id = '%s' "
                    "   and client_id    = '%s' "
                    "   and ordnum       = '%s' "
                    "   and ordlin       = '%s' and ordsln = '%s' "
                    "   and carcod       = '%s' and srvlvl = '%s' "
                    "   and marcod       = '%s' and ordtyp = '%s' "
                    "   and wh_id        = '%s' ",
                    ptr->ship_id,
                    ptr->ship_line_id,
                    ptr->client_id,
                    ptr->ordnum,
                    ptr->ordlin,
                    ptr->ordsln,
                    ptr->carcod,
                    ptr->srvlvl,
                    ptr->marcod,
                    ptr->ordtyp,
                    ptr->wh_id);
            if (errcode != eOK) 
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                srvFreeMemory(SRVRET_STRUCT, CurPtr1);
                CurPtr = NULL;
                CurPtr1 = NULL;
                sFreeWorkingList(Top);
                return(srvResults(errcode, NULL));
            }
            tempres = srvGetResults(CurPtr);
            temprow = sqlGetRow(tempres);
            strncpy(ptr->dstare,
                    sqlGetString(tempres, temprow,"dstare"),ARECOD_LEN);
            if (!sqlIsNull(tempres, temprow, "dstloc"))
            {
                strncpy(ptr->dstloc,
                        sqlGetString(tempres, temprow, "dstloc"),STOLOC_LEN);
            }
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
        }
    }
    srvFreeMemory(SRVRET_STRUCT, CurPtr1);
    CurPtr1 = NULL;

   
    memset(work_schbat,0,sizeof(work_schbat));
    if (schbat_i && misTrimLen(schbat_i,SCHBAT_LEN)) 
    {
        misTrimcpy(work_schbat, schbat_i, SCHBAT_LEN);
    }
    else
    {
        /*
         * The following situation can occur when running allocate pick group
         * with comflg = 1
         *
         * 1.  Allocate pick group is started, and a new schbat is created
         *     for the pick group.
         * 2.  The pick batch record is created in a state of IN-PROCESS
         * 3.  allocate inventory runs and fails with a DEADLOCK (-60)
         * 4.  allocate pick group exits with a return status of -60
         * 5.  MOCA detects the deadlock, and reruns the command according
         *     to design
         * 
         * In the above scenario, if comflg is 1, then it is quite possible
         * that part of the pick group is already created and committed.  In 
         * this case, when MOCA reruns the command, we don't want another 
         * schedule batch created.  This would result in a pick group that is 
         * split over multiple pick batches.  
         * We'll try to make use of the static schedule batch set by 
         * trnSetSchbat later on in this code.  When allocate pick group 
         * completes, trnClearSchbat is called to clear the static schedule 
         * batch.  In theory, if this function exists prematurely, then the 
         * static schedule batch variable should never have been cleared.  To 
         * safeguard against using an old schbat, we'll also check to make
         * sure the static schedule batch is still in process. To avoid
         * using schbat created in other warehouse if users plan wave
         * concurrently in different warehouses, we check wh_id too.
         */

        misTrimcpy(work_schbat, trnGetSchbat(), SCHBAT_LEN);

        if (misTrimLen(work_schbat, SCHBAT_LEN) > 0)
        {
            sprintf(sqlbuffer,
                    "select 'x' "
                    "  from pckbat "
                    " where schbat = '%s' "
                    "   and batsts = '%s' "
                    "   and wh_id = '%s' ",
                    work_schbat,
                    BATSTS_ALLOC_IN_PROC,
                    wh_id);

            errcode = sqlExecStr (sqlbuffer, NULL);
            if (eOK != errcode) 
            {
                /* Create the next schedule batch */
                errcode = appNextNum(NUMCOD_SCHBAT, work_schbat);
                if (errcode != eOK) 
                {
                    /* Free up our working list */
                    sFreeWorkingList(Top);
                    return(srvResults(errcode, NULL));
                }
            }
        }
        else
        {
            /* Create the next schedule batch */
            errcode = appNextNum(NUMCOD_SCHBAT, work_schbat);
            if (errcode != eOK) 
            {
                /* Free up our working list */
                sFreeWorkingList(Top);
                return(srvResults(errcode, NULL));
            }
        }
    }
    
    /* And if the batch doesn't already exist, then create it ... */
    sprintf(sqlbuffer,
            "select cmpdte, "
            "       batsts "
            "  from pckbat "
            " where schbat = '%s' ",
            work_schbat);

    errcode = sqlExecStr(sqlbuffer,&res);
    if (errcode != eOK) 
    {
        sqlFreeResults(res);
        res = NULL;

        memset(temp_clause,'\0',sizeof(temp_clause));
        if (arecod_i && restyp_i && resval_i && 
                misTrimLen(arecod_i,strlen(arecod_i)) &&
                misTrimLen(restyp_i, strlen(restyp_i)) &&
                misTrimLen(resval_i, strlen(resval_i)))
            sprintf(temp_clause," and arecod='%s' and restyp='%s' and "
                    " resval='%s' ",arecod_i,restyp_i,resval_i);

        memset(tmpbuf, 0, sizeof(tmpbuf));
        if (strlen(batcod))
        {
          sprintf (tmpbuf, " and batcod = '%s' ", batcod);
        }
        
        /* See if we need to include a priority code in the pckbat record. */
        memset(pricod_buf, 0, sizeof(pricod_buf));
        if ((pricod_i && strlen(pricod_i)))
            sprintf(pricod_buf, "  and pricod = '%s'", pricod_i);

        CurPtr = NULL;
        errcode = srvInitiateCommandFormat (NULL,
                    "create pick batch "
                    " where schbat = '%s'  "
                    "   and batsts = '%s' "
                    " %s "
                    "   and wave_prc_flg = '%ld' "
                    " %s "
                    " and lblseq='%s' %s "
                    " and wh_id='%s' ",
                    work_schbat,
                    BATSTS_ALLOC_IN_PROC,
                    pricod_buf,
                    BOOLEAN_FALSE,
                    temp_clause,
                    lblseqstrategy ? lblseqstrategy : "",
                    tmpbuf,
                    wh_id);
        if ((errcode != eOK && errcode != eDB_UNIQUE_CONS_VIO) ||
            (errcode == eDB_UNIQUE_CONS_VIO &&
            (!schbat_i || misTrimLen(schbat_i,SCHBAT_LEN) == 0))) 
        {
            /* Free up our working list */
            lastptr = NULL;
            sFreeWorkingList(Top);
            return(srvResults(errcode, NULL));
        }
    }
    else 
    {
        row = sqlGetRow(res);
        /*
         * Make sure the batch looks like it's inprocess, and shows that we
         * have allocated something. (may currently be in a PLAN state).
         */

        /* See if we need to include a priority code in the pckbat record. */
        memset(pricod_buf, 0, sizeof(pricod_buf));
        if ((pricod_i && strlen(pricod_i)))
            sprintf(pricod_buf, "      pricod = '%s',", pricod_i);

        if (!sqlIsNull(res,row,"cmpdte") || 
            strcmp(sqlGetString(res, row, "batsts"), BATSTS_ALOC) ||
            strcmp(sqlGetString(res, row, "batsts"), BATSTS_ALLOC_IN_PROC)) 
        {
            sprintf(sqlbuffer,
                    "update pckbat "
                    "   set cmpdte = '', "
                    " %s "
                    "       batsts = '%s' "
                    " where schbat = '%s' ",
                    pricod_buf,
                    BATSTS_ALLOC_IN_PROC,
                    work_schbat);
            errcode = sqlExecStr(sqlbuffer,NULL);
            if (errcode != eOK) 
            {
                sqlFreeResults(res);
                res = NULL;
                /* Free up our working list */
                sFreeWorkingList(Top);
                return(srvResults(errcode, NULL));
            }
        }
        sqlFreeResults(res);
        res = NULL;
    }

    /*
     * And now, let's set the schbat for allocate inventory.
     * By doing this, we will prevent allocate inventory from making
     * repeated calls to create pick batch, which is a performace save.
     * Since all subsequenct calls to allocate inventory will get called for
     * the same schedule batch, and since, from above, we know the schbat is
     * created, we will tell allocate inventory to not even try creating the
     * schbat.
     */

    misTrc(T_FLOW, "Schedule batch for pick group is %s", work_schbat);
    trnSetSchbat(work_schbat);

/******** I don't think the following is needed...the consolidated pick
 ******** logic gets handled below where we build the call for allocate  
    for (ptr = Top; ptr; ptr = ptr->next) 
    {
        if ((misCiStrncmp(consby_in,"ship_id",7)==0) ||
            (misCiStrncmp(consby_in,"stop_id",7)==0) ||
            (misCiStrncmp(consby_in,"rtcust",6)==0) ||
            (misCiStrncmp(consby_in,"stcust",6)==0))
        {
            memset(work_concod,0,sizeof(work_concod));
            errcode = appNextNum(NUMCOD_CONCOD, work_concod);
            if (errcode != eOK) 
            {
                sFreeWorkingList(Top);
                return(srvResults(errcode, NULL));
            }
        }
        strncpy(ptr->concod,work_concod,CONCOD_LEN);
    }
************************************************************************/

    memset(saved_ship_id, 0, sizeof(saved_ship_id));
    AllPtrs=NULL;
    row=NULL;
    for (ptr = Top; ptr; ptr = ptr->next) 
    {
        ++loop_cnt;

        /* First, count up the number of segments we will have for this call */
        num_segments = 0;
        for (tmpptr = ptr; tmpptr; tmpptr = tmpptr->next) 
        {
            if (tmpptr->pckqty > 0 && !tmpptr->xdk_inserted)
            {
                WriteXDock = TRUE;
                if ((PolXDockInstalled || ptr->IsDistro) &&
                    (strcmp(pcktyp_i, ALLOCATE_SHIPMENT_ONLY)) &&
                    (tmpptr->xdkflg == BOOLEAN_TRUE))
                {
                    /*
                    ** A XDock work record should only exist if some 
                    ** of the pckqty couldn't be cross-docked so
                    ** go ahead and allocate it
                    */
                    sprintf(sqlbuffer,
                            "select pckqty "
                            "  from xdkwrk "
                            " where ship_line_id = '%s' ",
                            tmpptr->ship_line_id);

                    errcode = sqlExecStr(sqlbuffer, &res);
                    if (errcode == eOK)
                    {
                        WriteXDock = FALSE;
                        row = sqlGetRow(res);
                        misTrc(T_FLOW, "Cross-dock work already exists for "
                                "pckqty %ld - Wanted to add pckqty %ld. ",
                                sqlGetLong(res, row, "pckqty"), tmpptr->pckqty);

                        /* And zero it out so we don't run across it again */
                        /* 
                         * But we won't zero it if it is a distro-order, 
                         * because we still want to allocate the quantity
                         */ 
                        if (!tmpptr->IsDistro)
                            tmpptr->pckqty = 0;
                        tmpptr->xdk_inserted = BOOLEAN_TRUE;
                    }
                    sqlFreeResults(res);
                    res = NULL;
                }
                else
                {
                    WriteXDock = FALSE;
                }

                if (WriteXDock)
                {
                    memset(xdkref, 0, sizeof(xdkref));
                    if (errcode = appNextNum(NUMCOD_XDKREF, xdkref) != eOK)
                        return (srvResults(errcode, NULL));

                    sprintf(sqlbuffer,
                            "insert into xdkwrk "
                            "(xdkref, xdktyp, wh_id, dstare, prtnum, adddte,"
                            " prt_client_id, invsts_prg, pckqty, xdkqty, "
                            " alcqty, lotnum, orgcod, revlvl, untcas, "
                            " untpak, untpal, ship_id, ship_line_id, "
                            " client_id, ordnum, ordlin, ordsln, "
                            " dstloc, schbat, stcust, rpqflg, splflg, supnum) "
                            " values "
                            " ('%s', '%s', '%s', '%s', '%s', "
                            "  to_date('%s', 'YYYYMMDDHH24MISS'), "
                            "  '%s', '%s', '%ld', '%ld', '%ld', "
                            "  '%s', '%s', '%s', '%ld', '%ld', "
                            "  '%ld', '%s', '%s', '%s', '%s', "
                            "  '%s', '%s', '%s', '%s', '%s', '%ld', '%ld', '%s') ",
                            xdkref, XDKTYP_SHIPMENT, tmpptr->wh_id,
                            tmpptr->dstare, tmpptr->prtnum, tmpptr->entdte,
                            tmpptr->prt_client_id, tmpptr->invsts_prg,
                            tmpptr->pckqty, 0, 0, tmpptr->lotnum,
                            tmpptr->orgcod, tmpptr->revlvl, tmpptr->alccas,
                            tmpptr->alcpak, tmpptr->alcpal, tmpptr->ship_id,
                            tmpptr->ship_line_id, tmpptr->client_id,
                            tmpptr->ordnum, tmpptr->ordlin, tmpptr->ordsln,
                            tmpptr->dstloc, work_schbat, tmpptr->stcust,
                            tmpptr->rpqflg, tmpptr->splflg, tmpptr->supnum);

                    errcode = sqlExecStr(sqlbuffer, NULL);
                    if (errcode != eOK && errcode != eDB_UNIQUE_CONS_VIO)
                        return (srvResults(errcode, NULL));

                    /* And zero it out so we don't run across it again */
                    /* 
                     * But we won't zero it if it is a distro-order, 
                     * because we still want to allocate the quantity
                     */ 
                    if (!tmpptr->IsDistro)
                        tmpptr->pckqty = 0;
                    tmpptr->xdk_inserted = BOOLEAN_TRUE;

                    /*
                    ** Indicate that at least one cross dock entry was created
                    */
                    xdk_created = TRUE;
                }

                if (((misCiStrncmp(consby_in,"ship_id",7)==0 &&
                      strncmp(tmpptr->ship_id,ptr->ship_id, SHIP_ID_LEN)==0) ||
                     (misCiStrncmp(consby_in,"stop_id",7)==0 &&
                      strncmp(tmpptr->stop_id,ptr->stop_id, STOP_ID_LEN)==0) ||
                     (misCiStrncmp(consby_in,"car_move_id",11)==0 &&
                      strncmp(tmpptr->car_move_id,ptr->car_move_id, CAR_MOVE_ID_LEN)==0) ||
                     (misCiStrncmp(consby_in,"stcust",6)==0 &&
                      strncmp(tmpptr->stcust,ptr->stcust,STCUST_LEN)==0) ||
                     (misCiStrncmp(consby_in,"all",3) == 0) ||
                     (misCiStrncmp(consby_in,"rtcust",6)==0 &&
                      strncmp(tmpptr->rtcust,ptr->rtcust,RTCUST_LEN)==0) ||
                     (misCiStrncmp(consby_in,"ordnum",6)==0 && 
                      strncmp(tmpptr->ordnum,ptr->ordnum, ORDNUM_LEN)==0 &&
                      strncmp(tmpptr->client_id,ptr->client_id, 
                                                      CLIENT_ID_LEN)==0)) &&

                    strncmp(tmpptr->prtnum,ptr->prtnum,PRTNUM_LEN)==0 &&
                    strncmp(tmpptr->prt_client_id,ptr->prt_client_id,
                            CLIENT_ID_LEN)==0 &&
                    strncmp(tmpptr->orgcod,ptr->orgcod,ORGCOD_LEN)==0 &&
                    strncmp(tmpptr->revlvl,ptr->revlvl,REVLVL_LEN)==0 &&
                    strncmp(tmpptr->supnum,ptr->supnum,SUPNUM_LEN)==0 &&
                    strncmp(tmpptr->lotnum,ptr->lotnum,LOTNUM_LEN)==0 &&
                    strncmp(tmpptr->invsts_prg, ptr->invsts_prg,
                            INVSTS_PRG_LEN)==0 &&
                    tmpptr->splflg == ptr->splflg &&
                    tmpptr->alccas == ptr->alccas &&
                    tmpptr->alcpak == ptr->alcpak &&
                    tmpptr->alcpal == ptr->alcpal &&
                    tmpptr->frsflg == ptr->frsflg &&
                    tmpptr->non_alc_flg == ptr->non_alc_flg &&
                    tmpptr->min_shelf_hrs == ptr->min_shelf_hrs &&
                    strncmp(tmpptr->frsdte,ptr->frsdte,MOCA_STD_DATE_LEN)==0)
                        num_segments++;
            }

            if (ptr->splflg == BOOLEAN_FALSE)
                break;    /*  If the ptr is "do not split cases", allocate
                          **  that line by it's self.
                          */
            if (ptr->non_alc_flg == BOOLEAN_TRUE)
                break;    /*  If the ptr is for a non-allocation line,
                          **  we're not allocating it.
                          */
        }
        if (num_segments == 0)  continue;
        
        /* Now, make space for everybody going in to trnAllocate */
        all_ship_id = (char *)calloc(1, (SHIP_ID_LEN+2)*num_segments);
        all_ship_line_id = (char *)calloc(1, (SHIP_LINE_ID_LEN+2)*num_segments);
        all_schbat = (char *)calloc(1, (SCHBAT_LEN+2)*num_segments);
        all_stcust = (char *)calloc(1, (ADRNUM_LEN+2)*num_segments);
        all_rtcust = (char *)calloc(1, (ADRNUM_LEN+2)*num_segments);
        all_client_id = (char *)calloc(1, (CLIENT_ID_LEN+2)*num_segments);
        all_ordnum = (char *)calloc(1, (ORDNUM_LEN+2)*num_segments);
        all_ordlin = (char *)calloc(1, (ORDLIN_LEN+2)*num_segments);
        all_ordsln = (char *)calloc(1, (ORDSLN_LEN+2)*num_segments);
        all_concod = (char *)calloc(1, (CONCOD_LEN+2)*num_segments);
        all_segqty = (char *)calloc(1, (6+2)*num_segments);
        all_dstare = (char *)calloc(1, (ARECOD_LEN+2)*num_segments);
        all_dstloc = (char *)calloc(1, (STOLOC_LEN+2)*num_segments);
        all_alc_search_path =
                    (char *)calloc(1, (ALC_SEARCH_PATH_LEN+2)*num_segments);

        if (all_ship_id       == NULL ||
            all_ship_line_id  == NULL ||
            all_schbat        == NULL ||
            all_stcust        == NULL ||
            all_rtcust        == NULL ||
            all_client_id     == NULL ||
            all_ordnum        == NULL ||
            all_ordlin        == NULL ||
            all_ordsln        == NULL ||
            all_concod        == NULL ||
            all_segqty        == NULL ||
            all_dstare        == NULL ||
            all_dstloc        == NULL ||
            all_alc_search_path == NULL)
        {
            /* Free up our working list */
            sFreeWorkingList(Top);
            if (all_ship_id) free (all_ship_id);
            if (all_ship_line_id) free (all_ship_line_id);
            if (all_schbat) free (all_schbat);
            if (all_stcust) free (all_stcust);
            if (all_rtcust) free (all_rtcust);
            if (all_client_id) free (all_client_id);
            if (all_ordnum) free (all_ordnum);
            if (all_ordlin) free (all_ordlin);
            if (all_ordsln) free (all_ordsln);
            if (all_concod) free (all_concod);
            if (all_segqty) free (all_segqty);
            if (all_dstare) free (all_dstare);
            if (all_dstloc) free (all_dstloc);
            if (all_alc_search_path) free (all_alc_search_path);
            return(srvResults(eNO_MEMORY, NULL));
        }
        all_pckqty = 0;
        /* Save the address of these pointers. */
        sav_all_schbat        = all_schbat;
        sav_all_ship_id       = all_ship_id;
        sav_all_ship_line_id  = all_ship_line_id;
        sav_all_stcust        = all_stcust;
        sav_all_rtcust        = all_rtcust;
        sav_all_client_id     = all_client_id;
        sav_all_ordnum        = all_ordnum;
        sav_all_ordlin        = all_ordlin;
        sav_all_ordsln        = all_ordsln;
        sav_all_concod        = all_concod;
        sav_all_segqty        = all_segqty;
        sav_all_dstare        = all_dstare;
        sav_all_dstloc        = all_dstloc;
		sav_all_alc_search_path = all_alc_search_path;

        memset(work_concod, 0, sizeof(work_concod));
        /* Now, loop through and build the paramters with spaces in between */
        for (tmpptr = ptr; tmpptr; tmpptr = tmpptr->next) 
        {
            if (tmpptr->pckqty > 0 &&
                ((misCiStrncmp(consby_in,"ship_id",7)==0 &&
                  strncmp(tmpptr->ship_id, ptr->ship_id, SHIP_ID_LEN)==0) ||
                 (misCiStrncmp(consby_in,"stop_id",7)==0 &&
                  strncmp(tmpptr->stop_id, ptr->stop_id, STOP_ID_LEN)==0) ||
                 (misCiStrncmp(consby_in,"car_move_id",7)==0 &&
                  strncmp(tmpptr->car_move_id, ptr->car_move_id, CAR_MOVE_ID_LEN)==0) ||
                 (misCiStrncmp(consby_in,"all",3)==0) ||
                 (misCiStrncmp(consby_in,"stcust",6)==0 &&
                  strncmp(tmpptr->stcust,ptr->stcust,STCUST_LEN)==0) ||
                 (misCiStrncmp(consby_in,"rtcust",6)==0 &&
                  strncmp(tmpptr->rtcust,ptr->rtcust,RTCUST_LEN)==0) ||
         (misCiStrncmp(consby_in,"ordnum",6)==0 &&
          strncmp(tmpptr->ordnum, ptr->ordnum, ORDNUM_LEN)==0 &&
          strncmp(tmpptr->client_id, ptr->client_id, CLIENT_ID_LEN)==0)) &&
                strncmp(tmpptr->prtnum,ptr->prtnum,PRTNUM_LEN)==0 &&
                strncmp(tmpptr->prt_client_id,ptr->prt_client_id,
                                                CLIENT_ID_LEN)==0 &&
                strncmp(tmpptr->orgcod,ptr->orgcod,ORGCOD_LEN)==0 &&
                strncmp(tmpptr->revlvl,ptr->revlvl,REVLVL_LEN)==0 &&
                strncmp(tmpptr->supnum,ptr->supnum,SUPNUM_LEN)==0 &&
                strncmp(tmpptr->lotnum,ptr->lotnum,LOTNUM_LEN)==0 &&
                strncmp(tmpptr->invsts_prg,
                        ptr->invsts_prg,
                        INVSTS_PRG_LEN)==0 &&
                tmpptr->splflg == ptr->splflg &&
                tmpptr->alccas == ptr->alccas &&
                tmpptr->alcpak == ptr->alcpak &&
                tmpptr->alcpal == ptr->alcpal && 
                tmpptr->non_alc_flg == ptr->non_alc_flg &&
                tmpptr->frsflg == ptr->frsflg &&
                tmpptr->min_shelf_hrs == ptr->min_shelf_hrs &&
                strncmp(tmpptr->frsdte,ptr->frsdte,MOCA_STD_DATE_LEN)==0)

            {
                if (strlen(work_concod) == 0)
                    appNextNum(NUMCOD_CONCOD, work_concod);

                strcat (all_schbat, work_schbat);
                strcat (all_schbat, CONCAT_CHAR);

                strcat (all_ship_id, tmpptr->ship_id);
                strcat (all_ship_id, CONCAT_CHAR);
                strcat (all_ship_line_id, tmpptr->ship_line_id);
                strcat (all_ship_line_id, CONCAT_CHAR);
                if (strlen(tmpptr->stcust))
                {
                    strcat (all_stcust, tmpptr->stcust);
                    strcat (all_stcust, CONCAT_CHAR);
                }
                if (strlen(tmpptr->rtcust))
                {
                    strcat (all_rtcust, tmpptr->rtcust);
                    strcat (all_rtcust, CONCAT_CHAR);
                }
                strcat (all_client_id, tmpptr->client_id);
                strcat (all_client_id, CONCAT_CHAR);
                strcat (all_ordnum, tmpptr->ordnum);
                strcat (all_ordnum, CONCAT_CHAR);
                strcat (all_ordlin, tmpptr->ordlin);
                strcat (all_ordlin, CONCAT_CHAR);
                strcat (all_ordsln, tmpptr->ordsln);
                strcat (all_ordsln, CONCAT_CHAR);
                strcat (all_concod, work_concod);
                strcat (all_concod, CONCAT_CHAR);
                sprintf (temp_clause, "%6ld", tmpptr->pckqty);
                strcat (all_segqty, temp_clause);
                strcat (all_segqty, CONCAT_CHAR);
                if (strlen(tmpptr->dstare) && PolXDockInstalled)
                {
                    strcat (all_dstare, tmpptr->dstare);
                    strcat (all_dstare, CONCAT_CHAR);
                }
                else
                {
                    /*I used a blank space to allow for blank entries here
                      and in dstloc. Cant use the empty string because
                      strtoc will ignore those in trnAllocInv */
                    if (strlen(ptr->dstare)==0)
                    {
                        strcat (all_dstare, " ");
                        strcat (all_dstare, CONCAT_CHAR);
                    }
                    else
                    {
                        strcat (all_dstare, tmpptr->dstare);
                        strcat (all_dstare, CONCAT_CHAR);
                    }
                }
                if (strlen(tmpptr->dstloc) && PolXDockInstalled)
                {
                    strcat (all_dstloc, tmpptr->dstloc);
                    strcat (all_dstloc, CONCAT_CHAR);
                }
                else
                {
                    if (strlen(ptr->dstloc)==0)
                    {
                        strcat (all_dstloc, " ");
                        strcat (all_dstloc, CONCAT_CHAR);
                    }
                    else
                    {
                        strcat (all_dstloc, tmpptr->dstloc);
                        strcat (all_dstloc, CONCAT_CHAR);
                    }
                }
                if (strlen(tmpptr->alc_search_path))
                {
                    strcat (all_alc_search_path, tmpptr->alc_search_path);
                    strcat (all_alc_search_path, CONCAT_CHAR);
                }
                else
                {
                    strcat (all_alc_search_path, " ");
                    strcat (all_alc_search_path, CONCAT_CHAR);
                }

                /* Add to the total pick quantity */
                all_pckqty += tmpptr->pckqty;

                /* And zero it out so we don't run across it again */
                tmpptr->pckqty = 0;

            }

            /*  If the split case flag is set to NO (do not break cases just
            **  to complete a line), allocate that line by it's self.
            */
            if (ptr->splflg == BOOLEAN_FALSE)
                break;

            /*  If the non allocatable flag is set to True,
            **  don't allocate it.
            */
            if (ptr->non_alc_flg == BOOLEAN_TRUE)
                break;
        }

        /* Let's naw off our last comma */
        misTrim(all_segqty);
        misTrim(all_concod);
        misTrim(all_ordsln);
        misTrim(all_ordlin);
        misTrim(all_client_id);
        misTrim(all_ordnum);
        misTrim(all_stcust);
        misTrim(all_rtcust);
        misTrim(all_ship_id);
        misTrim(all_ship_line_id);
        misTrim(all_schbat);
        misTrim(all_alc_search_path);
        if (strlen(all_segqty) > 0)
            all_segqty[strlen(all_segqty)-1]='\0';
        if (strlen(all_concod) > 0)
            all_concod[strlen(all_concod)-1]='\0';
        if (strlen(all_ordsln) > 0)
            all_ordsln[strlen(all_ordsln)-1]='\0';
        if (strlen(all_ordlin) > 0)
            all_ordlin[strlen(all_ordlin)-1]='\0';
        if (strlen(all_client_id) > 0)
            all_client_id[strlen(all_client_id) - 1]='\0';
        if (strlen(all_ordnum) > 0)
            all_ordnum[strlen(all_ordnum)-1]='\0';
        if (strlen(all_stcust) > 0)
            all_stcust[strlen(all_stcust)-1]='\0';
        if (strlen(all_rtcust) > 0)
            all_rtcust[strlen(all_rtcust)-1]='\0';
        if (strlen(all_ship_id) > 0)
            all_ship_id[strlen(all_ship_id)-1]='\0';
        if (strlen(all_ship_line_id) > 0)
            all_ship_line_id[strlen(all_ship_line_id)-1]='\0';
        if (strlen(all_schbat) > 0)
            all_schbat[strlen(all_schbat)-1]='\0';
        if (strlen(all_dstare) > 0)
            all_dstare[strlen(all_dstare)-1]='\0';
        if (strlen(all_dstloc) > 0)
            all_dstloc[strlen(all_dstloc)-1]='\0';
        if (strlen(all_alc_search_path) > 0)
            all_alc_search_path[strlen(all_alc_search_path)-1]='\0';

        /*
         * don't allocate if a xdkwrk was inserted.
         * But if current order is a distro order, we will continue allocate
         */
        if (ptr->xdk_inserted == BOOLEAN_TRUE 
         && !ptr->IsDistro)
            continue;

        /* If we have a non-allocatable line, we don't need to 
         * do anything at this point. 
         */

        if (ptr->non_alc_flg == BOOLEAN_TRUE)
        {
            misTrc(T_FLOW, "Skipping allocation - this is a non-allocatable "
                           "line!");
            ret_status = eOK;
            add_to_failure = 0;

            continue;
        }
        if (strncmp(saved_ship_id, ptr->ship_id, SHIP_ID_LEN) != 0)
        {
            if (comflg == BOOLEAN_TRUE)  srvCommit();
            strcpy(saved_ship_id, ptr->ship_id);
        }

        /*
         * Build up a suffix for the status file name consisting of 
         * the pick group, part number and sequence number, in case
         * the part number gets allocated more than once in the pick
         * group.
         */

        memset(tmpbuf, 0, sizeof(tmpbuf));
        sprintf(tmpbuf, "-%s-%s-%ld", 
                pckgr1_i ? pckgr1_i : "", 
                ptr->prtnum, loop_cnt);

        add_to_failure = 0;
        
        /* Here we will check if the shipment is for a distro. */
        if (ptr->IsDistro)
        {
            /* We should find all the inventories that have not been picked. */

            /* 
             * If We can't find the inventory for this distro, we do nothing,
             * i.e. we won't set add_to_failure=1. Because we already have 
             * cross-dock work created, and we never create replenishment 
             * for a distro order. 
             */

            sprintf(sqlbuffer,
                    " [select distinct invlod.lodnum, aremst.put_to_sto_flg"
                    "    from invdtl, "
                    "         invsub, "
                    "         invlod, "
                    "         invsum, "
                    "         locmst, "
                    "         aremst "
                    "   where invdtl.subnum = invsub.subnum"
                    "     and invsub.lodnum = invlod.lodnum"
                    "     and invlod.stoloc = locmst.stoloc"
                    "     and invlod.wh_id = locmst.wh_id"
                    "     and invlod.stoloc = invsum.stoloc"
                    "     and invlod.wh_id = invsum.wh_id"
                    "     and invdtl.prtnum = invsum.prtnum "
                    "     and invdtl.prt_client_id = invsum.prt_client_id "
                    "     and locmst.arecod = aremst.arecod "
                    "     and locmst.wh_id = aremst.wh_id "
                    "     and (aremst.put_to_sto_flg = '%ld' "
                    "      or locmst.stoflg =  '%ld') "
                    "     and invdtl.distro_id = '%s'"
                    "     and invdtl.prtnum = '%s' "
                    "     and invdtl.prt_client_id = '%s' "
                    "     and invsum.untqty > invsum.comqty "
                    "     and invdtl.wrkref is null"
                    "     order by aremst.put_to_sto_flg desc ]",
                    BOOLEAN_TRUE,
                    BOOLEAN_TRUE,
                    ptr->distro_id,
                    ptr->prtnum,
                    ptr->prt_client_id);

            errcode = srvInitiateCommand(sqlbuffer, &InvPtr);
            if (errcode == eOK)
            {
                invres = srvGetResults (InvPtr);

                for (invrow = sqlGetRow(invres); 
                     invrow; invrow = sqlGetNextRow(invrow))
                {
                    misTrimcpy(lodnum, 
                               sqlGetString(invres, invrow, "lodnum"), 
                               STOLOC_LEN);
                    CurPtr = NULL;



                    /* We add the logic to split paramters and pass them to
                     * allocate inventory. Becase allocate inventory can only 
                     * deal with one shipment_line at a time if the shipment 
                     * is for a distro.
                     */

                    /* Add a CONCAT_CHAR to the end of each string */
                    strcat(all_schbat, CONCAT_CHAR);
                    strcat(all_ship_id, CONCAT_CHAR);
                    strcat(all_ship_line_id, CONCAT_CHAR);
                    strcat(all_stcust, CONCAT_CHAR);
                    strcat(all_rtcust, CONCAT_CHAR);
                    strcat(all_client_id, CONCAT_CHAR);
                    strcat(all_ordnum, CONCAT_CHAR);   
                    strcat(all_ordlin, CONCAT_CHAR);   
                    strcat(all_ordsln, CONCAT_CHAR);   
                    strcat(all_concod, CONCAT_CHAR);   
                    strcat(all_segqty, CONCAT_CHAR);   
                    strcat(all_dstare, CONCAT_CHAR);   
                    strcat(all_dstloc, CONCAT_CHAR);   
                    strcat(all_alc_search_path, CONCAT_CHAR);   

                    /* Find the first position of CONCAT_CHAR in each string */
                    schbat_Ptr    = strstr(all_schbat, CONCAT_CHAR);
                    ship_id_Ptr   = strstr(all_ship_id, CONCAT_CHAR);
                    ship_line_id_Ptr = strstr(all_ship_line_id, CONCAT_CHAR);
                    stcust_Ptr    = strstr(all_stcust, CONCAT_CHAR);
                    rtcust_Ptr    = strstr(all_rtcust, CONCAT_CHAR);
                    client_id_Ptr = strstr(all_client_id, CONCAT_CHAR);
                    ordnum_Ptr    = strstr(all_ordnum, CONCAT_CHAR);
                    ordlin_Ptr    = strstr(all_ordlin, CONCAT_CHAR);
                    ordsln_Ptr    = strstr(all_ordsln, CONCAT_CHAR);
                    concod_Ptr    = strstr(all_concod, CONCAT_CHAR);
                    segqty_Ptr    = strstr(all_segqty, CONCAT_CHAR);
                    dstare_Ptr    = strstr(all_dstare, CONCAT_CHAR);
                    dstloc_Ptr    = strstr(all_dstloc, CONCAT_CHAR);
                    alc_search_path_Ptr = strstr(all_alc_search_path, 
                                              CONCAT_CHAR);

                    while(schbat_Ptr && ship_id_Ptr &&  ship_line_id_Ptr &&
                        stcust_Ptr && rtcust_Ptr && client_id_Ptr &&
                        ordnum_Ptr && ordlin_Ptr && ordsln_Ptr && concod_Ptr &&
                        segqty_Ptr && dstare_Ptr && dstloc_Ptr &&
                        alc_search_path_Ptr)
                    {
                        /* Set the first position to 0 in order to 
                         * get the first sub-string */
                        schbat_Ptr[0] = '\0';
                        ship_id_Ptr[0] = '\0';
                        ship_line_id_Ptr[0] = '\0';
                        stcust_Ptr[0] = '\0';
                        rtcust_Ptr[0] = '\0';
                        client_id_Ptr[0] = '\0';
                        ordnum_Ptr[0] = '\0';
                        ordlin_Ptr[0] = '\0';
                        ordsln_Ptr[0] = '\0';
                        concod_Ptr[0] = '\0';
                        segqty_Ptr[0] = '\0';
                        dstare_Ptr[0] = '\0';
                        dstloc_Ptr[0] = '\0';
                        alc_search_path_Ptr[0] = '\0';

                        /* Pass in cmbcod if available to allocate inventory 
                         * so that pckmov records will not be generated again. 
                         */
                        memset(cmbcod_clause, 0, sizeof(cmbcod_clause));
                        sprintf(sqlbuffer,
                            " select pckwrk.cmbcod from "
                            "        ord_line, "
                            "        invdtl, "
                            "        invsub, "
                            "        pckwrk, "
                            "        invlod, "
                            "        shipment_line "
                            "  where pckwrk.appqty = 0 "
                            "    and pckwrk.pcksts in ('%s', '%s', '%s') "
                            "    and shipment_line.ship_id = pckwrk.ship_id "
                            "    and shipment_line.ordnum = pckwrk.ordnum "
                            "    and shipment_line.ordlin = pckwrk.ordlin "
                            "    and shipment_line.ordsln = pckwrk.ordsln "
                            "    and shipment_line.client_id= pckwrk.client_id "
                            "    and shipment_line.wh_id = pckwrk.wh_id "
                            "    and shipment_line.ship_id = '%s' "
                            "    and ord_line.ordnum = pckwrk.ordnum "
                            "    and ord_line.ordlin = pckwrk.ordlin "
                            "    and ord_line.ordsln = pckwrk.ordsln "
                            "    and ord_line.client_id = pckwrk.client_id "
                            "    and ord_line.wh_id = pckwrk.wh_id "
                            "    and ord_line.distro_id = invdtl.distro_id "
                            "    and invdtl.prt_client_id = ord_line.client_id "
                            "    and invlod.wh_id = ord_line.wh_id "
                            "    and invdtl.subnum = invsub.subnum "
                            "    and invsub.lodnum = invlod.lodnum "
                            "    and invlod.lodnum = '%s' "
                            "    and invlod.stoloc = pckwrk.srcloc "
                            "    and invlod.wh_id = pckwrk.wh_id "
                            "  group by pckwrk.cmbcod ",
                            PCKSTS_HOLD,
                            PCKSTS_PENDING,
                            PCKSTS_RELEASED,
                            ptr->ship_id,
                            lodnum);
                        errcode = sqlExecStr(sqlbuffer, &res);
    
                        if (errcode == eOK) 
                        {
                            row = sqlGetRow(res);
                            strcpy(cmbcod_clause, " and cmbcod = '");
                            strcat(cmbcod_clause, 
                                sqlGetString(res, row, "cmbcod"));
                            strcat(cmbcod_clause, "' ");
                            sqlFreeResults(res);
                            res=NULL;
                        }
    
                        if (CurPtr)
                            srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        CurPtr = NULL;
                        
                        /* Call 'Allocatie inventory' with the current lodnum)*/
                        ret_status = srvInitiateCommandFormat(&CurPtr,
                                "allocate inventory "
                                "   where pcktyp='%s' "
                                "     and prtnum='%s' "
                                "     and prt_client_id='%s' "
                                "     and pckqty='%ld'  and orgcod='%s' "
                                "     and revlvl='%s' and supnum='%s' "
                                "     and lotnum='%s' "
                                "     and invsts_prg='%s' "
                                "     and schbat='%s' and ship_id='%s'  "
                                "     and ship_line_id='%s' "
                                "     and client_id='%s' "
                                "     and ordnum='%s'  "
                                "     and stcust=\"%s\"  "
                                "     and rtcust=\"%s\"  "
                                "     and ordlin='%s' and ordsln='%s' "
                                "     and concod='%s' and segqty='%s' "
                                "     and dstare='%s' "
                                "     and pcksts='%s' "
                                "     and pcksts_uom='%s' "
                                "     and splflg='%ld' "
                                "     and dstloc='%s' and untcas='%ld' "
                                "     and untpak='%ld'  and untpal='%ld' "
                                "     and pipcod = '%s' "
                                "     and min_shelf_hrs = %ld "
                                "     and frsflg = %ld "
                                "     and frsdte = '%s' "
                                "     and trace_suffix = '%s' "
                                "     and skip_invlkp = '%ld'"
                                "     and alc_search_path = '%s' "
                                "     and wh_id = '%s' "
                                "     and lodnum = '%s' %s",
                                pcktyp_i?pcktyp_i:"",
                                ptr->prtnum,
                                ptr->prt_client_id,
                                all_pckqty,ptr->orgcod,ptr->revlvl,
                                ptr->supnum,ptr->lotnum,
                                ptr->invsts_prg,all_schbat,all_ship_id,
                                all_ship_line_id,
                                all_client_id,
                                all_ordnum,
                                all_stcust,all_rtcust,all_ordlin,all_ordsln,
                                all_concod,
                                all_segqty,all_dstare,
                                pcksts,
                                pcksts_uom_i ? pcksts_uom_i : "",
                                ptr->splflg,
                                all_dstloc,
                                ptr->alccas, ptr->alcpak, ptr->alcpal,
                                pipcod,
                                ptr->min_shelf_hrs,
                                ptr->frsflg,
                                ptr->frsdte,
                                tmpbuf,
                                (PolPIProcessing ? 
                                    sWillAllocFail(FailedAllocHead, ptr) :
                                    BOOLEAN_FALSE),
                                all_alc_search_path,
                                ptr->wh_id,
                                lodnum,
                                cmbcod_clause);

                        if (ret_status == eOK)
                        {
                            mocaDataRes *ares;
                            mocaDataRow *arow;
                            long pipFound = 0;
                            long pipPos;
                            int  pckqty;
    
                            ares = srvGetResults(CurPtr);
                            arow = sqlGetRow(ares);
    
                            pipPos = sqlFindColumn(ares, "pipflg");
                            pckqty = sqlGetLong(ares, arow, "pckqty");
                            if (pipPos > 0)
                            {
                                for (arow = sqlGetRow(ares); arow && !pipFound; 
                                     arow = sqlGetNextRow(arow))
                                {
                                    if (!sqlIsNullByPos(ares, arow, pipPos) &&
                                        sqlGetLongByPos(ares, arow, pipPos) > 0)
                                        pipFound = 1;
                                }
                            }
    
                            /* We need to update xdkwrk 
                             * If pckqty <= 0, we should remvoe this recorde 
                             */
                            sprintf(sqlbuffer,
                                    "[update xdkwrk "
                                    "    set pckqty = pckqty - %ld "
                                    "  where ship_line_id = '%s'] catch(-1403)"
                                    " ; "
                                    "[delete xdkwrk "
                                    "  where ship_line_id = '%s' "
                                    "    and pckqty <=0 ] catch(-1403)",
                                    pckqty,
                                    ptr->ship_line_id,
                                    ptr->ship_line_id);
    
                            errcode = srvInitiateCommand(sqlbuffer, NULL);
                            /* reduce the quantity also, and stop allocating 
                             * if we have enough.
                             */
                            all_pckqty = all_pckqty - pckqty;
                            if (all_pckqty <= 0)
                            {
                                ptr->pckqty = 0;
                                break;
                            }
                        }
                        
                        /* Step over the CONCAT_CHAR in each string */
                        all_schbat = schbat_Ptr + strlen(CONCAT_CHAR);
                        all_ship_id = ship_id_Ptr + strlen(CONCAT_CHAR);
                        all_ship_line_id = 
                            ship_line_id_Ptr + strlen(CONCAT_CHAR);
                        all_stcust = stcust_Ptr + strlen(CONCAT_CHAR);
                        all_rtcust = rtcust_Ptr + strlen(CONCAT_CHAR);
                        all_client_id = client_id_Ptr + strlen(CONCAT_CHAR);
                        all_ordnum = ordnum_Ptr + strlen(CONCAT_CHAR);
                        all_ordlin = ordlin_Ptr + strlen(CONCAT_CHAR);
                        all_ordsln = ordsln_Ptr + strlen(CONCAT_CHAR);
                        all_concod = concod_Ptr + strlen(CONCAT_CHAR);
                        all_segqty = segqty_Ptr + strlen(CONCAT_CHAR);
                        all_dstare = dstare_Ptr + strlen(CONCAT_CHAR);
                        all_dstloc = dstloc_Ptr + strlen(CONCAT_CHAR);
                        all_alc_search_path = 
                            alc_search_path_Ptr + strlen(CONCAT_CHAR);
                        
                        /* Find the next position of CONCAT_CHAR 
                         * in each string */
                        schbat_Ptr    = strstr(all_schbat, CONCAT_CHAR);
                        ship_id_Ptr   = strstr(all_ship_id, CONCAT_CHAR);
                        ship_line_id_Ptr = strstr(all_ship_line_id,CONCAT_CHAR);
                        stcust_Ptr    = strstr(all_stcust, CONCAT_CHAR);
                        rtcust_Ptr    = strstr(all_rtcust, CONCAT_CHAR);
                        client_id_Ptr = strstr(all_client_id, CONCAT_CHAR);
                        ordnum_Ptr    = strstr(all_ordnum, CONCAT_CHAR);
                        ordlin_Ptr    = strstr(all_ordlin, CONCAT_CHAR);
                        ordsln_Ptr    = strstr(all_ordsln, CONCAT_CHAR);
                        concod_Ptr    = strstr(all_concod, CONCAT_CHAR);
                        segqty_Ptr    = strstr(all_segqty, CONCAT_CHAR);
                        dstare_Ptr    = strstr(all_dstare, CONCAT_CHAR);
                        dstloc_Ptr    = strstr(all_dstloc, CONCAT_CHAR);
                        alc_search_path_Ptr = strstr(all_alc_search_path, 
                                                  CONCAT_CHAR);
                    }
                }
            }
            if (InvPtr)
                srvFreeMemory(SRVRET_STRUCT, InvPtr);
            InvPtr = NULL;
        }
        /* If it is not, we just keep the crrently logicl. */
        else
        {
            /* Change to srvInitiateCommandFormat to avoid memory leaks. */
            ret_status = srvInitiateCommandFormat(&CurPtr,
                    "allocate inventory "
                    "   where pcktyp='%s' "
                    "     and prtnum='%s' "
                    "     and prt_client_id='%s' "
                    "     and pckqty='%ld'  and orgcod='%s' "
                    "     and revlvl='%s' and supnum = '%s' "
                    "     and lotnum='%s' "
                    "     and invsts_prg='%s' "
                    "     and schbat='%s' and ship_id='%s'  "
                    "     and ship_line_id='%s' "
                    "     and client_id='%s' "
                    "     and ordnum='%s'  "
                    "     and stcust=\"%s\"  "
                    "     and rtcust=\"%s\"  "
                    "     and ordlin='%s' and ordsln='%s' "
                    "     and concod='%s' and segqty='%s' "
                    "     and dstare='%s' "
                    "     and pcksts='%s' "
                    "     and pcksts_uom='%s' "
                    "     and splflg='%ld' "
                    "     and dstloc='%s' and untcas='%ld' "
                    "     and untpak='%ld'  and untpal='%ld' "
                    "     and pipcod = '%s' "
                    "     and min_shelf_hrs = %ld "
                    "     and frsflg = %ld "
                    "     and frsdte = '%s' "
                    "     and trace_suffix = '%s' "
                    "     and skip_invlkp = '%ld'"
                    "     and alc_search_path = '%s' "
                    "     and wh_id = '%s' ",
                    pcktyp_i?pcktyp_i:"",
                    ptr->prtnum,
                    ptr->prt_client_id,
                    all_pckqty,ptr->orgcod,ptr->revlvl,ptr->supnum,ptr->lotnum,
                    ptr->invsts_prg,all_schbat,all_ship_id,
                    all_ship_line_id,
                    all_client_id,
                    all_ordnum,
                    all_stcust,all_rtcust,all_ordlin,all_ordsln,all_concod,
                    all_segqty,all_dstare,
                    pcksts,
                    pcksts_uom_i ? pcksts_uom_i : "",
                    ptr->splflg,
                    all_dstloc,
                    ptr->alccas, ptr->alcpak, ptr->alcpal,
                    pipcod,
                    ptr->min_shelf_hrs,
                    ptr->frsflg,
                    ptr->frsdte,
                    tmpbuf,
                    (PolPIProcessing ? sWillAllocFail(FailedAllocHead, ptr) :
                                     BOOLEAN_FALSE),
                    all_alc_search_path,
                    ptr->wh_id);

            if (ret_status == eDB_NO_ROWS_AFFECTED ||
                ret_status == eINT_NO_INVENTORY)
                add_to_failure = 1;
            
            if (ret_status == eOK)
            {
                mocaDataRes *ares;
                mocaDataRow *arow;
                long pipFound = 0;
                long pipPos;

                ares = srvGetResults(CurPtr);
                arow = sqlGetRow(ares);

                pipPos = sqlFindColumn(ares, "pipflg");

                if (pipPos > 0)
                {
                    for (arow = sqlGetRow(ares); arow && !pipFound; 
                         arow = sqlGetNextRow(arow))
                    {
                        if (!sqlIsNullByPos(ares, arow, pipPos) &&
                            sqlGetLongByPos(ares, arow, pipPos) > 0)
                            pipFound = 1;
                    }
                }
                if (pipFound || sqlGetNumRows(ares) == 0)
                    add_to_failure = 1;
            }
        }

        if (add_to_failure)
        {
            /* We failed to allocate - stash this off in our
             * failed allocation list so that we can more intelligently
             * call allocate inventory
             */
            ret_status = sAddToFailureList(&FailedAllocHead,
                                           ptr->wh_id,
                                           ptr->prtnum,
                                           ptr->prt_client_id,
                                           ptr->orgcod,
                                           ptr->revlvl,
                                           ptr->supnum,
                                           ptr->lotnum,
                                           ptr->invsts_prg,
                                           ptr->dstare,
                                           ptr->dstloc,
                                           ptr->splflg,
                                           ptr->alccas,
                                           ptr->alcpak, 
                                           ptr->alcpal,
                                           ptr->frsflg,
                                           ptr->non_alc_flg,
                                           ptr->min_shelf_hrs,
                                           ptr->frsdte);

        }


        if (all_schbat) free (sav_all_schbat);
        if (all_ship_id) free (sav_all_ship_id);
        if (all_ship_line_id) free (sav_all_ship_line_id);
        if (all_stcust) free (sav_all_stcust);
        if (all_rtcust) free (sav_all_rtcust);
        if (all_client_id) free (sav_all_client_id);
        if (all_ordnum) free (sav_all_ordnum);
        if (all_ordlin) free (sav_all_ordlin);
        if (all_ordsln) free (sav_all_ordsln);
        if (all_concod) free (sav_all_concod);
        if (all_segqty) free (sav_all_segqty);
        if (all_dstare) free (sav_all_dstare);
        if (all_dstloc) free (sav_all_dstloc);
        if (all_alc_search_path) free (sav_all_alc_search_path);

        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
        {
            /* Free up our working list */
            sFreeWorkingList(Top);
            srvFreeMemory(SRVRET_STRUCT,CurPtr);
            CurPtr = NULL;
            if (AllPtrs)
                srvFreeMemory(SRVRET_STRUCT, AllPtrs);
            AllPtrs = NULL;
            return(srvResults(ret_status, NULL));
        }
 
        if (ret_status == eOK) 
        {
            if (CurPtr && CurPtr->rows == 0) 
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
            }
            else if (CurPtr)
            {
                    ret_status = srvCombineResults (&AllPtrs,&CurPtr);
                    if (ret_status != eOK)
                    {
                        srvFreeMemory(SRVRET_STRUCT, AllPtrs);
                        srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        AllPtrs = NULL;
                        CurPtr = NULL;
                        return(srvResults(ret_status, NULL));
                    }
            }
        }
        /* PR 30609. LN - 8/20/2003
         * This next line had been commented out at one time when it was
         * causing issues in Sql Server sites where dirty reads were enabled
         * because the pick release manager would then start processing
         * these lines even though the schedule batch was not finished 
         * allocating.
         * These problems were fixed under PR 29254. As a result,
         * we're going to enable this incremental commit to speed up
         * processing.
         */
        
        if (comflg == BOOLEAN_TRUE)  srvCommit();   
    }

    sFreeFailedAllocList(FailedAllocHead);

    /* At this point, the batch status for this pick batch has progressed from
     * Planned through Allocation-In-Process to Allocated. The batch status
     * is expected to reflect the pick status of picks in the batch. 
     * 
     * The possible batsts and pcksts codes are listed below.
     *     batsts PLAN Planned               | pcksts H Hold
     *     batsts AINP Allocation In Process | pcksts P Pending
     *     batsts ALOC Allocated             | pcksts R Released
     *     batsts SCH  Scheduled for Release | pcksts C Complete
     *     batsts REL  Released              | pcksts U Un-Assigned
     *     batsts CMPL Complete              |
     * 
     * The ALOC batsts is appropriate if none of the picks in the batch have
     * batsts P or R. However it is possible that, depending on lodlvls
     * specified in pcksts_uom, some of the picks have been moved to Pending (or
     * even Released at this point). If so, the batch status should progress to
     * Scheduled-for-Release or Released.
     * 
     * If a pick with pcksts P is present, then batsts should be SCH; else, if a
     * pick with pcksts R is present and, simultaneously, there are no picks
     * with pcksts H, then batsts should be REL. If these two checks fail, the
     * default batsts ALOC should be retained.
     * 
     * In the query below, the rownum clause is used to retrieve a
     * representative row having the desired pick status so that the update
     * will be run only once.
     */
    sprintf(sqlbuffer,
        " publish data"
        "   where schbat='%s'"
        "     and batsts='%s'"
        " |"
        " [select @schbat schbat,"
        "         '%s' batsts"
        "    from pckwrk"
        "   where schbat=@schbat"
        "     and pcksts = '%s'"
        "     and rownum < 2] catch (-1403)"
        " |"
        " if (@? = -1403)"
        " {"
        "     [select @schbat schbat,"
        "             '%s' batsts"
        "        from pckwrk"
        "       where schbat=@schbat"
        "         and pcksts = '%s'"
        "         and not exists"
        "             (select 1 flg"
        "                from pckwrk"
        "               where schbat=@schbat"
        "                 and pcksts = '%s')"
        "         and rownum < 2] catch (-1403)"
        " }"
        " |"
        " [update pckbat"
        "    set batsts = @batsts"
        "  where schbat = @schbat]",
        work_schbat,
        BATSTS_ALOC,
        BATSTS_SCH,
        PCKSTS_PENDING,
        BATSTS_REL,
        PCKSTS_RELEASED,
        PCKSTS_HOLD
        );
    errcode = srvInitiateCommand(sqlbuffer, NULL);
    if (errcode != eOK)
    {
        /* Not the biggest crime in the world, let the error go. */
        misTrc(T_FLOW, "Problem updating the pckbat.  Look into the error!");
    }

    /* Deal with the preinventory picks right here */
    if (work_schbat && misTrimLen(work_schbat, SCHBAT_LEN) &&
        !running_pip_in_parallel)
    {

        /* This is the part that we swing back and select all the pre-inventory
           pick work entries for the schedule batch and create replenishments
           to pull the product forward. */
    
        ret_status = srvInitiateInlineFormat(NULL,
                "process pip for schedule batch"
                " where schbat = '%s' "
                "   and comflg = %ld "
                "   and pcktyp = '%s' "
                "   and pip_pcksts = '%s' ",
                work_schbat,
                comflg_i,
                pcktyp_i?pcktyp_i:"",
                pcksts);

        /* Bomb out if this returns an error */
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
        {
            /* Free up our working list */
            sFreeWorkingList(Top);
            if (AllPtrs)
                srvFreeMemory(SRVRET_STRUCT, AllPtrs);
            AllPtrs = NULL;
            return(srvResults(ret_status, NULL));
        }
    }

    if (AllPtrs == NULL)
    {
        /* see above comment regarding replenishment manager... */
        if (strcmp(pcktyp_i, ALLOCATE_PICKNREPLEN) == 0 ||
            strcmp(pcktyp_i, ALLOCATE_PICKNREPLENNSHIP) == 0 ||
            xdk_created == TRUE)
        {
            CurPtr = srvResults(eOK,
                                "schbat", COMTYP_CHAR, SCHBAT_LEN, work_schbat,
                                NULL);
        }
        else
        {
            CurPtr = srvResults(eDB_NO_ROWS_AFFECTED, NULL);    
        }       
    }
    else 
        CurPtr=AllPtrs;

    /* Free up our working list */
    sFreeWorkingList(Top);

    /*
     * Now, make sure we clear the schbat for the next
     * call to allocate pick group.  This will also prevent garbage from 
     * being in the field in case allocate inventory gets called outside of
     * this program. (Like for move requests)
     */

    trnClearSchbat();


    return(CurPtr);
}
