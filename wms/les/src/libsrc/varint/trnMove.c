static char *rcsid = "$Id: trnMove.c,v 1.1 2011/07/26 17:30:21 joworkma Exp $";
/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 2002
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/
#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "trnlib.h"

long trnMoveLoad(LOC_INFO *srcloc_info, 
                 ARE_INFO *srcare_info,
                 LOD_INFO *load_info,
                 LOC_INFO *dstloc_info,
                 ARE_INFO *dstare_info,
                 char *wh_id_i,
                 char *wrkref_i,
                 char *lstemp_i,
                 char *lstcod_i,
                 moca_bool_t revflg_i,
                 char *xdkref_i,
                 long *xdkqty_i,
                 double catch_qty_i)
{
    long ret_status;
    
    misTrc(T_FLOW, "Beginning MOVE LOAD");

    ret_status = movProcessLoadMove(srcloc_info,
                    srcare_info,
                    load_info,
                    dstloc_info,
                    dstare_info,
                    wrkref_i,
                    lstcod_i,
                    lstemp_i,
                    xdkref_i,
                    xdkqty_i,
                    catch_qty_i);

    if (eOK != ret_status)
    return(ret_status);

    if (misTrimLen(wrkref_i, WRKREF_LEN))
    {
        ret_status = movApplyLoadToWrkref(wrkref_i, 
                      load_info);


        if (eOK != ret_status)
            return(ret_status);

        ret_status = trnUCC128ApplyIdentifier(wh_id_i,
                          wrkref_i, 
                          load_info->lodnum, 
                          NULL, NULL, 
                          NULL, NULL, NULL);
        if (eOK != ret_status)
            return(ret_status);

    }           

    ret_status = movUpdatePckmovQuantities(srcare_info, 
                       dstare_info,
                       srcloc_info,
                       dstloc_info,
                       NULL,
                       NULL,
                       load_info,
                       revflg_i);

    misTrc(T_FLOW, "Done with MOVE LOAD");
    return(ret_status);
    
}

long trnMoveSub(LOC_INFO *srcloc_info,  
                ARE_INFO *srcare_info,
                LOD_INFO *srclod_info,
                SUB_INFO *sub_info,
                LOC_INFO *dstloc_info,
                ARE_INFO *dstare_info,
                LOD_INFO *dstlod_info,
                char *wh_id_i,
                char *wrkref_i,
                char *lstcod_i,
                char *lstemp_i,
                moca_bool_t revflg_i,
                char **sublist_o,
                char **dtllist_o,
                char *xdkref_i,
                long *xdkqty_i,
                double catch_qty_i)
{

    long ret_status;


    misTrc(T_FLOW, "Entering MOVE SUB");

    ret_status = movProcessSubMove(srcloc_info,
                   srcare_info,
                   srclod_info,
                   sub_info,
                   dstloc_info,
                   dstare_info,
                   dstlod_info,
                   wrkref_i,
                   lstcod_i,
                   lstemp_i,
                   xdkref_i,
                   xdkqty_i,
                   catch_qty_i);
    
    if (eOK != ret_status)
    return(ret_status);

    if (misTrimLen(wrkref_i, WRKREF_LEN)) 
    {
        misTrc(T_FLOW, "Processing the apply of inventory to pick");
        ret_status = movApplySubToWrkref(wrkref_i, sub_info);

        if (eOK != ret_status)
            return(ret_status);

        ret_status = trnUCC128ApplyIdentifier(wh_id_i,
                          wrkref_i, 
                          NULL, 
                          sub_info->subnum, 
                          NULL, NULL, sublist_o, dtllist_o);

        if (eOK != ret_status)
            return(ret_status);
    }

    ret_status = movUpdatePckmovQuantities(srcare_info, 
                       dstare_info,
                       srcloc_info,
                       dstloc_info,
                       NULL,
                       sub_info,
                       NULL,
                       revflg_i);

    misTrc(T_FLOW, "Done with MOVE SUB");
    return(ret_status);
}

long trnMoveDetail(LOC_INFO *srcloc_info,       
                   ARE_INFO *srcare_info,
                   LOD_INFO *srclod_info,
                   SUB_INFO *srcsub_info,
                   DTL_INFO *detail_info,
                   LOC_INFO *dstloc_info,
                   ARE_INFO *dstare_info,
                   SUB_INFO *dstsub_info,
                   char *wh_id_i,
                   char *wrkref_i,
                   char *lstcod_i,
                   char *lstemp_i,
                   moca_bool_t revflg_i,
                   char **sublist_o,
                   char **dtllist_o,
                   char *xdkref_i,
                   long *xdkqty_i,
                   double catch_qty_i)
{

    long ret_status;
    char *split_details;
    struct _dtllist *p, *last_p;

    ret_status = movProcessDetailMove(srcloc_info,       
                      srcare_info,
                      srclod_info,
                      srcsub_info,
                      detail_info,
                      dstloc_info,
                      dstare_info,
                      dstsub_info,
                      wrkref_i,
                      lstcod_i,
                      lstemp_i,
                      xdkref_i,
                      xdkqty_i,
                      catch_qty_i);
    
    if (eOK != ret_status)
    return(ret_status);

    if (misTrimLen(wrkref_i, WRKREF_LEN)) 
    {
        misTrc(T_FLOW, "Processing the apply of inventory to pick");
        ret_status = movApplyDetailToWrkref(wrkref_i, detail_info);
        if (eOK != ret_status)
            return(ret_status);
        
        split_details = NULL;
        misDynStrcat(&split_details, "'");
        misDynStrcat(&split_details, detail_info->dtlnum);
        misDynStrcat(&split_details, "'");

        for (p = detail_info->split; p; p = p->next)
        {
            misDynStrcat(&split_details, ",'");
            misDynStrcat(&split_details, p->dtlnum);
            misDynStrcat(&split_details, "'");
        }
        
        for (last_p = NULL, p = detail_info->split; p; last_p = p, p = p->next)
            if (last_p)
            free(last_p);

        if (last_p)
            free(last_p);

        detail_info->split = NULL;
        
        ret_status = trnUCC128ApplyIdentifier(wh_id_i,
                              wrkref_i, 
                              NULL, NULL, 
                              detail_info->dtlnum,
                              split_details, 
                              sublist_o,
                              dtllist_o);

        free(split_details);

        if (eOK != ret_status)
            return(ret_status);
    }
    

    ret_status = movUpdatePckmovQuantities(srcare_info, 
                       dstare_info,
                       srcloc_info,
                       dstloc_info,
                       detail_info,
                       NULL,
                       NULL,
                       revflg_i);
    
    return(ret_status);
}


