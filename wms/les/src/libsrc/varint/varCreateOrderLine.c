/*#START***********************************************************************
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "intlib.h"

#define DEF_CRNCY_CODE "USD"
#define TMPLEN 1000

/*
 * HISTORY
 * JJS 09/04/2008 - When orders are created via Integrator, it will always
 *                  pass min_shelf_hrs = 0 even if min_shelf_hrs is NULL in
 *                  the IFD (since the field uses a Fmt of 9999999999). This
 *                  is a problem because this code will not look Customer
 *                  Requirements Processing when a non-NULL value of min_shelf_hrs
 *                  is passed in (even when zero).  So, for Hopewell, we will
 *                  make it look at Customer Requirements even if 0 is passed in.
 *
 *
 *
 *
 *
 */

LIBEXPORT
RETURN_STRUCT *varCreateOrderLine(char *client_id_i,
                                  char *ordnum_i, 
                                  char *ordlin_i,
                                  char *ordsln_i,
                                  char *wh_id_i,
                                  char *prt_client_id_i,
                                  char *prtnum_i, 
                                  char *orgcod_i,
                                  char *revlvl_i, 
                                  char *lotnum_i,
                                  char *cooinc_i,
                                  char *coolst_i, 
                                  long *host_ordqty_i, 
                                  long *ovramt_i,
                                  char *ovrcod_i, 
                                  char *marcod_i,
                                  moca_bool_t *parflg_i, 
                                  moca_bool_t *ovaflg_i,
                                  moca_bool_t *ovpflg_i, 
                                  moca_bool_t *rpqflg_i,
                                  moca_bool_t *splflg_i, 
                                  moca_bool_t *stdflg_i,
                                  moca_bool_t *bckflg_i,
                                  char *prcpri_i,
                                  char *frtcod_i, 
                                  char *cargrp_i,
                                  char *carcod_i, 
                                  char *srvlvl_i,
                                  moca_bool_t *sddflg_i,
                                  char *entdte_i,
                                  char *sales_ordnum_i,
                                  char *sales_ordlin_i,
                                  char *cstprt_i,
                                  char *manfid_i, 
                                  char *deptno_i,
                                  char *accnum_i, 
                                  char *prjnum_i,
                                  long *untcas_i, 
                                  long *untpak_i,
                                  long *untpal_i,
                                  char *early_shpdte_i, 
                                  char *late_shpdte_i,
                                  char *early_dlvdte_i, 
                                  char *late_dlvdte_i,
                                  char *ordinv_i,
                                  moca_bool_t *edtflg_i,
                                  moca_bool_t *xdkflg_i,
                                  moca_bool_t *atoflg_i,
                                  char *dstare_i, 
                                  char *dstloc_i,
                                  char *rsvpri_i, 
                                  long *ordqty_i,
                                  long *rsvqty_i, 
                                  long *pckqty_i,
                                  char *pckgr1_i, 
                                  char *pckgr2_i,
                                  char *pckgr3_i, 
                                  char *pckgr4_i,
                                  int  *trigger_i, 
                                  char *mode_i,
                                  moca_bool_t *frsflg_i,
                                  char *invsts_prg_i,
                                  long *min_shelf_hrs_i,
                                  char *host_invsts_i,
                                  moca_bool_t *validate_part_i,
                                  moca_bool_t *non_alc_flg_i,
                                  char *tms_ord_stat_i,
                                  moca_bool_t *tms_snt_flg_i,
                                  char *tms_dte_snt_i,
                                  double *unt_price_i,
                                  char *crncy_name_i,
                                  char *wave_set_i,
                                  double *tot_pln_wgt_i,
                                  double *tot_pln_cube_i,
                                  double *tot_pln_misc_qty_i,
                                  double *tot_pln_pal_qty_i,
                                  double *tot_pln_cas_qty_i,
                                  double *tot_pln_misc2_qty_i,
                                  char *pln_stccod_i,
                                  char *avail_pln_dte_i,
                                  char *gl_num_i,
                                  char *pln_ltlcls_i,
                                  char *pln_comcod_i,
                                  char *appt_dte_i,
                                  char *tms_pln_sts_i,
                                  char *crncy_code_i,
                                  char *pool_cod_i,
                                  char *distro_id_i,
                                  moca_bool_t *ctn_flg_i,
                                  long *tm_shpqty_i,
                                  long *tm_plnqty_i,
                                  char *sf_adr_id_i,
                                  char *st_adr_id_i,
                                  char *rt_adr_id_i,
                                  char *reffld_1_i,
                                  char *reffld_2_i,
                                  char *reffld_3_i,
                                  char *reffld_4_i,
                                  char *reffld_5_i,
                                  char *alc_search_path_i,
                                  double *unt_ins_val_i,
                                  double *rel_val_i,
                                  char *rel_val_unt_typ_i,
                                  char *supnum_i,
                                  char *bto_comcod_i,
                                  char *src_sys_i,
                                  char *sched_id_i,
                                  char *recur_sched_typ_i,
                                  long *sched_wnd_i)

{
    static int custreqInstalled = 1234;
    static int pckrsvmgrInstalled = 1234;
    static int crossdockInstalled = 1234;
    static int tmInstalled = 1234;
    static int wmInstalled = 1234;

    RETURN_STRUCT *CurPtr;
    mocaDataRes *res, *tempres, *prtres;
    mocaDataRow *row, *temprow, *prtrow;
  
    char *valuelist;
    char tmpvar[1000];
    char sqlbuffer[3000];
    long errcode;
    long ret_status;
    long untcas, untpak, untpal;
    long *p_untcas;
    long *p_untpak;
    long *p_untpal;
    char rsvpri[RSVPRI_LEN+1];
    char pln_ltlcls[PLN_LTLCLS_LEN+1];
    int CheckCustReqs = 1;
    int ValidPrt = 0;

    char client_id[CLIENT_ID_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    moca_bool_t carflg = BOOLEAN_NOTSET;
    moca_bool_t bckflg = BOOLEAN_NOTSET;
    moca_bool_t splflg = BOOLEAN_NOTSET; 
    moca_bool_t stdflg = BOOLEAN_NOTSET;
    moca_bool_t parflg = BOOLEAN_NOTSET;
    moca_bool_t frsflg = BOOLEAN_NOTSET;
    moca_bool_t validate_part = BOOLEAN_NOTSET;
    moca_bool_t non_alc_flg = BOOLEAN_NOTSET;
    moca_bool_t xdkflg = BOOLEAN_NOTSET;
    moca_bool_t dtcflg;
    moca_bool_t ctn_flg = BOOLEAN_NOTSET;
    char manfid[MANFID_LEN+1]; 
    char deptno[DEPTNO_LEN+1]; 
    char cstprt[CSTPRT_LEN+1];
    char ordinv[ORDINV_LEN+1];
    char invsts_prg[INVSTS_PRG_LEN+1];
    char temp_dte[DB_STD_DATE_LEN+1];
    char wh_id[WH_ID_LEN+1];
    char tms_pln_sts[TMS_PLN_STS_COD_LEN + 1];
    char facility_country[CTRY_NAME_LEN+1];
    char route_to_country[CTRY_NAME_LEN+1];
    char sf_adr_id[ADR_ID_LEN + 1];
    char st_adr_id[ADR_ID_LEN + 1];
    char rt_adr_id[ADR_ID_LEN + 1];
    char reffld_1[REF_FLD_LEN + 1];
    char reffld_2[REF_FLD_LEN + 1];
    char reffld_3[REF_FLD_LEN + 1];
    char reffld_4[REF_FLD_LEN + 1];
    char reffld_5[REF_FLD_LEN + 1];
    char alc_search_path[ALC_SEARCH_PATH_LEN + 1];
    char supnum[SUPNUM_LEN + 1];
    char recur_sched_typ[RECUR_SCHED_TYP_LEN + 1];
    moca_bool_t intl_flg = BOOLEAN_TRUE;
    
    long rsvqty;
    long pckqty;
    long shpqty;
    long ordqty;

    long entdte_quotes;

    moca_bool_t ovaflg = BOOLEAN_NOTSET;
    moca_bool_t ovpflg = BOOLEAN_NOTSET; 
    moca_bool_t rpqflg = BOOLEAN_NOTSET;
    char prcpri[PRCPRI_LEN+1];
    moca_bool_t sddflg = BOOLEAN_NOTSET;
    char entdte[DB_STD_DATE_LEN + 1];
    char early_shpdte[DB_STD_DATE_LEN + 1];
    char late_shpdte[DB_STD_DATE_LEN + 1];
    char early_dlvdte[DB_STD_DATE_LEN + 1];
    char late_dlvdte[DB_STD_DATE_LEN + 1];
    char appt_dte[DB_STD_DATE_LEN + 1];
    char avail_pln_dte[DB_STD_DATE_LEN + 1];
    moca_bool_t tms_snt_flg = BOOLEAN_NOTSET;
    char tms_dte_snt[DB_STD_DATE_LEN + 1];

    moca_bool_t edtflg = BOOLEAN_NOTSET;
    moca_bool_t atoflg = BOOLEAN_NOTSET;
    char rsvqty_tmp[1000];
    char pckqty_tmp[1000];
    char shpqty_tmp[1000];
    char crncy_code[ISO_CRNCY_SYM_LEN+1];
    char rel_val_unt_typ[REL_VAL_UNT_TYP_LEN+1];
    
    long min_shelf_hrs = 0;
    double *p_tot_pln_wgt = NULL;
    double *p_tot_pln_cube = NULL;
    double *p_tot_pln_pal_qty = NULL;
    double *p_tot_pln_cas_qty = NULL;
    double *p_rel_val = NULL;
    double *p_unt_ins_val = NULL;

    char* p_rel_val_unt_typ = NULL;

    double tot_pln_wgt = 0.0;
    double tot_pln_cube = 0.0;
    double tot_pln_pal_qty = 0.0;
    double tot_pln_cas_qty = 0.0;
    
    double unt_ins_val = 0.0;
    double rel_val = 0.0;
    
    untcas = untpak = untpal =0;
    p_untcas = &untcas;
    p_untpak = &untpak;
    p_untpal = &untpal;
    ordqty = 0;
    
    memset(manfid,        0, sizeof(manfid));
    memset(deptno,        0, sizeof(deptno));
    memset(cstprt,        0, sizeof(cstprt));
    memset(client_id,     0, sizeof(client_id));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(ordinv,        0, sizeof(ordinv));
    memset(invsts_prg,    0, sizeof(invsts_prg));
    memset(rsvpri,        0, sizeof(rsvpri));

    memset(facility_country,    0, sizeof(facility_country));
    memset(route_to_country,    0, sizeof(route_to_country));
    memset(wh_id, 0, sizeof(wh_id)); 

    memset(prcpri, 0, sizeof(prcpri));
    memset(entdte, 0, sizeof(entdte));
    memset(early_shpdte, 0, sizeof(early_shpdte));
    memset(late_shpdte, 0, sizeof(late_shpdte)); 
    memset(early_dlvdte, 0, sizeof(early_dlvdte));
    memset(late_dlvdte, 0, sizeof(late_dlvdte));
    memset(avail_pln_dte, 0, sizeof(avail_pln_dte));
    memset(appt_dte, 0, sizeof(appt_dte));
    memset(tms_dte_snt, 0, sizeof(tms_dte_snt));
    memset(pln_ltlcls, 0, sizeof(pln_ltlcls));
    memset(tms_pln_sts, 0, sizeof(tms_pln_sts));
    memset(crncy_code, 0, sizeof(crncy_code));
    memset(sf_adr_id, 0, sizeof(sf_adr_id));
    memset(st_adr_id, 0, sizeof(st_adr_id));
    memset(rt_adr_id, 0, sizeof(rt_adr_id));
    memset(reffld_1, 0, sizeof(reffld_1));
    memset(reffld_2, 0, sizeof(reffld_2));
    memset(reffld_3, 0, sizeof(reffld_3));
    memset(reffld_4, 0, sizeof(reffld_4));
    memset(reffld_5, 0, sizeof(reffld_5));
    memset(alc_search_path, 0, sizeof(alc_search_path));
    memset(rel_val_unt_typ, 0, sizeof(rel_val_unt_typ));
    memset(supnum, 0, sizeof(supnum));
    memset(recur_sched_typ, 0, sizeof(recur_sched_typ));

    shpqty = 0;

    CurPtr = NULL;
    res = NULL;
    prtres = NULL;
    prtrow = NULL;

    /* Check if the customer requirement processing is installed */
    if ( custreqInstalled == 1234 )
    custreqInstalled = appIsInstalled(POLCOD_CUST_REQ_PROC);
   
    /* check if reservation manager is installed */
    if (pckrsvmgrInstalled == 1234)
        pckrsvmgrInstalled = appIsInstalled(POLCOD_PCKRSVMGR);

    if (crossdockInstalled == 1234)
        crossdockInstalled = appIsInstalled(POLCOD_CROSS_DOCKING);
    
    /* Check if wm is installed */
    if (wmInstalled == 1234)
        wmInstalled = appIsInstalled("WM");

    /* Check if tm is installed */
    if (tmInstalled == 1234)
        tmInstalled = appIsInstalled("TM");

    if (!ordnum_i || misTrimLen(ordnum_i, ORDNUM_LEN) == 0)
    return APPMissingArg("ordnum");

    if (!ordlin_i || misTrimLen(ordlin_i, ORDLIN_LEN) == 0)
    return APPMissingArg("ordlin");

    if (!ordsln_i || misTrimLen(ordsln_i, ORDSLN_LEN) == 0)
    return APPMissingArg("ordsln");
  
    /* prtnum is required only if wm is installed */
    if (wmInstalled && (!prtnum_i || misTrimLen(prtnum_i, PRTNUM_LEN) == 0))
    return APPMissingArg("prtnum");

    if (!ordqty_i || *ordqty_i < 0)
        return APPMissingArg("ordqty");

    ordqty = *ordqty_i;
    
    ret_status = appGetClient(client_id_i, client_id);

    misTrc(T_FLOW, "client_id is '%s' ", client_id);

    if (ret_status != eOK)
        return (srvResults(ret_status, NULL));

    if (rsvpri_i && misTrimLen(rsvpri_i, RSVPRI_LEN) > 0)
        misTrimcpy(rsvpri, rsvpri_i, RSVPRI_LEN);

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

    if (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN))
        misTrimcpy(supnum, supnum_i, SUPNUM_LEN);
    /*
     *  Validate the warehouse id. If null, the command
     *  should come back with the default, otherwise we error.
     */
    sprintf(sqlbuffer,
            " get warehouse id "
            " where wh_id = '%s' ",
              wh_id);

    ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);

    if (ret_status != eOK)
    { 
       srvFreeMemory(SRVRET_STRUCT, CurPtr);
       return (srvResults(ret_status, NULL));
    }

   res = srvGetResults(CurPtr);
   row = sqlGetRow(res);
   misTrimcpy(wh_id, sqlGetString(res, row,"wh_id"), WH_ID_LEN);
   srvFreeMemory(SRVRET_STRUCT, CurPtr);
   CurPtr = NULL;
   res = NULL;
   row = NULL;

    /* make sure that there is a valid order for this line */
    sprintf(tmpvar,
            " list orders "
            " where client_id = '%s' "
            " and ordnum = '%s' "
            " and wh_id  = '%s'",
              client_id, ordnum_i, wh_id);

    ret_status = srvInitiateCommand(tmpvar, &CurPtr);

    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
         misTrc(T_FLOW, "Error validating client_id-ordnum-wh_id.");                                                                         
         return (srvResults(ret_status, NULL));
    }
    else if (ret_status == eDB_NO_ROWS_AFFECTED) 
    {
         sprintf(tmpvar, "%s-%s-%s", client_id, ordnum_i, wh_id);
         return APPInvalidArg(tmpvar,"client_id-ordnum-wh_id");
    }
    
    /*validate carrier information */
    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);
    carflg = sqlGetBoolean(res, row, "carflg");
    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;
    res = NULL;
    row = NULL;
 
    if (wmInstalled)
    { 
       if (carflg == BOOLEAN_FALSE && (!carcod_i || !srvlvl_i)) 
            return APPMissingArg(carcod_i?"srvlvl":"carcod");
    }
    /* 
       prt_client_id to the client_id
    */
    if(wmInstalled || (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN)))
    {
    if (!prt_client_id_i || misTrimLen(prt_client_id_i, CLIENT_ID_LEN) == 0)
    {
        strncpy(prt_client_id, client_id, CLIENT_ID_LEN);
    }
    else
    {
        misTrc(T_FLOW, "Validating prt_client_id");
        ret_status = appGetClient(prt_client_id_i, prt_client_id);
    }
    
    if (validate_part_i)
    {
        validate_part = *validate_part_i;
    }
    else
    {
        /*
         * By default, we will always validate the part, they must 
         * specifically send in the value not to.
         */

        validate_part = BOOLEAN_TRUE;
    }
    /*We'll only try to derive dtcflg only if prtnum is provided */
    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
    {  
    /* 
     * we will cache the result of the selected prtmst data
     * in prtres, so that no redundant selection for the same
     * part is needed. It can also avoid second try of getting
     * data for an invalid part.
     */
    sprintf(sqlbuffer,
             " select prtmst_view.* "
             " from prtmst_view "
             " where prtnum = '%s' "
             " and prt_client_id = '%s' "
             " and wh_id = '%s' ",
               prtnum_i, prt_client_id, wh_id);
 
    ret_status = sqlExecStr(sqlbuffer, &prtres);

    if (ret_status != eOK)
    {
        if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            if (validate_part)
            {
                sqlFreeResults(prtres);
                return APPInvalidArg(prtnum_i, "prtnum");
            }
            else
            {
                /*
                 * We'll need to default the dtcflg, since we can't
                 * get it from the part.
                 */
                dtcflg = BOOLEAN_FALSE;
                ValidPrt = 0;
            }
        }
        else 
        {
            sqlFreeResults(prtres);
            return (srvResults(ret_status, NULL));
            ValidPrt = 0;
        }
    }
    else
    {
        /* Get the date controlled flag from the part */
       
        ValidPrt = 1;
        prtrow = sqlGetRow (prtres);
        dtcflg = sqlGetBoolean (prtres, prtrow, "dtcflg");
        if (dtcflg != BOOLEAN_TRUE)
            dtcflg = BOOLEAN_FALSE;
    }
    }
    }
    else
    {
        ValidPrt = 0;
    }
    /* Check to see if the line is being shipped internationally then */
    /* make sure that this carrier/service level is able to ship*/
    /* internationally.  If there is no country for the route to */ 
    /* address or for the facility address assume that */
    /* this validation is occuring elsewhere */
    
    if (!crncy_code_i || misTrimLen(crncy_code_i, ISO_CRNCY_SYM_LEN) == 0)
    {
        /* Set default value to USD*/
        misTrimcpy(crncy_code, DEF_CRNCY_CODE, ISO_CRNCY_SYM_LEN);
    }
    else
    {
        sprintf(sqlbuffer,
             " list currencies "
             " where crncy_code = '%s' ", crncy_code_i);

        ret_status = srvInitiateCommand(sqlbuffer, NULL);        

        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            
            misTrc(T_FLOW, " Error validating crncy_code");
            return(srvResults(ret_status, NULL));
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED) 
        {
            return(APPInvalidArg(crncy_code_i, "crncy_code"));            
        }
        
        misTrimcpy(crncy_code, crncy_code_i, ISO_CRNCY_SYM_LEN);
    }

    /* check that all the ordlines in same order have the same currency code, this is a TM only check */
    if (tmInstalled)
    {
        sprintf(sqlbuffer,
            " [select crncy_code "
            " from ord_line "
            " where ord_line.ordnum = '%s' "
            " and ord_line.client_id = '%s' "
            " and ord_line.wh_id = '%s' "
            " and crncy_code <> '%s' "
            " and (ordlin <> '%s' or ordsln <> '%s')] ",
             ordnum_i, client_id_i, wh_id_i, crncy_code, ordlin_i, ordsln_i);

        ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
    
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        { 
           srvFreeMemory(SRVRET_STRUCT, CurPtr);
           return (srvResults(ret_status, NULL));
        }

        if (ret_status != eDB_NO_ROWS_AFFECTED)
        { 
            /* There are some other order line has different currency */
            return srvResults(eDCS_CRNCY_CODE_CONSISTENT_ERROR, NULL);
        }
    }
    
    if(carcod_i &&  srvlvl_i)
    {
        sprintf(sqlbuffer,
                "[select adr_id from wh where wh_id = '%s' ] "
                "| "
                "[select 'X' "
                "   from adrmst a1, "
                "        adrmst a2, "
                "        ord o "
                "  where (a1.ctry_name=a2.ctry_name or "
                "        a1.ctry_name is null or "
                "        a2.ctry_name is null) "
                "    and a1.adr_id = @adr_id "
                "    and a2.adr_id = o.rt_adr_id "
                "    and o.ordnum='%s' "
                "    and o.client_id='%s'"
                "    and o.wh_id = '%s' ]",
                wh_id,
                ordnum_i,
                client_id_i,
                wh_id);
    
        ret_status = srvInitiateCommand(sqlbuffer, NULL);

        if (ret_status != eOK)
        {
            if (ret_status == eDB_NO_ROWS_AFFECTED) 
            {
                /*The countries are different so..*/
                /*Make sure Carrier can ship international*/
                sprintf(sqlbuffer,
                        "select 'X' "
                        "  from cardtl "
                        " where carcod = '%s' "
                        "   and srvlvl = '%s' "
                        "   and intl_flg = 1",
                    carcod_i,srvlvl_i);
        
                ret_status = sqlExecStr(sqlbuffer, NULL);

                if (ret_status != eOK)
                {
                    /*Either not international or another error*/
                    if (ret_status == eDB_NO_ROWS_AFFECTED)
                    {
                        /*Not international carrier/srvlvl*/
                        if (strlen(carcod_i) > 0)
                        {
                            return srvResults(eINT_INTL_ORD_NON_INTL_CAR,NULL);
                        }
                        else
                        {
                            /*No carrier defined so continue*/
                        }
                    }
                    else
                    {
                        return (srvResults(ret_status, NULL));
                    }
                }
                /*Countries are different but this is an international*/
                /*carrier/srvlvl so continue*/
            }
            else
            {
                /*An error*/
                return (srvResults(ret_status, NULL));
            }
        }
    }

    /*validate carcod */
    if (carcod_i && misTrimLen(carcod_i, CARCOD_LEN))
    {
        sprintf(sqlbuffer,
             " select carhdr.* "
             " from carhdr "
             " where carcod = '%s' ",
               carcod_i);
 
         ret_status = sqlExecStr(sqlbuffer, NULL);
    
         if (ret_status != eOK)
         {
            if (eDB_NO_ROWS_AFFECTED == ret_status)
            {
                sprintf (sqlbuffer, "%s", carcod_i); 
                return APPInvalidArg(sqlbuffer, "carcod");
            }

            else 
            {
                misTrc(T_FLOW, "Error validating carcod.");
                return(srvResults(ret_status, NULL));
            }
         }
    }

    /*validate crncy_name */
    if (crncy_name_i && misTrimLen(crncy_name_i, CRNCY_NAME_LEN))
    {
        sprintf(sqlbuffer,
                " select crncy_mst.* "
                " from crncy_mst "
                " where crncy_code = '%s' ",
                  crncy_name_i);
 
       ret_status = sqlExecStr(sqlbuffer, NULL);

       if (ret_status != eOK)
       {
           if (eDB_NO_ROWS_AFFECTED == ret_status)
		    {
			   sprintf (tmpvar, "%s", crncy_name_i);                    
               return APPInvalidArg(sqlbuffer, "crncy_name");
            }
            else 
            {
               misTrc(T_FLOW, "Error validating crncy_name.");
               return(srvResults(ret_status, NULL));
            }
        }
	}

    if (pool_cod_i && misTrimLen(pool_cod_i, POOL_COD_LEN))
    {
        sprintf(sqlbuffer,
                " select 'x' "
                "   from codmst "
                "   where colnam = 'pool_cod'"
                "     and codval    = '%s' ",
                misStrReplaceAll(pool_cod_i, "'", "''"));

        ret_status = sqlExecStr(sqlbuffer, NULL);
        if (eOK != ret_status)
        {
            if (eDB_NO_ROWS_AFFECTED == ret_status)
            {
                sqlFreeResults(res);
                return(APPInvalidArg(pool_cod_i, "pool_cod"));
            }
            else
            {
                sqlFreeResults(res);
                return(srvResults(ret_status, NULL));
            }
        }
    }

    /*validate over allocation code */
    if (ovrcod_i && misTrimLen(ovrcod_i, OVRCOD_LEN))
    {
        sprintf(tmpvar,
                " select codmst.* "
                " from codmst "
                " where colnam = '%s' "
                " and codval = '%s' ",
                "ovrcod", 
                ovrcod_i);

        ret_status = sqlExecStr(tmpvar, NULL);        

        if (ret_status != eOK && eDB_NO_ROWS_AFFECTED != ret_status)
        {
            
            misTrc(T_FLOW, " Error validating colnam-codval");
            return(srvResults(ret_status, NULL));
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED) 
        {
            sprintf (tmpvar, "%s/%s", "ovrcod", ovrcod_i);
            return APPInvalidArg(tmpvar,"colnam, codval");
            
        }
    }

    /* validate the unt_price */
    if (unt_price_i && *unt_price_i < 0.0)
    {
        sprintf(tmpvar, "%f", *unt_price_i);
        return (APPInvalidArg (tmpvar, "unt_price"));
    }

   
    valuelist = NULL;

    
    if (ovaflg_i && *ovaflg_i == BOOLEAN_TRUE)
    {
       ovaflg = BOOLEAN_TRUE;
    }
    else
    {
        ovaflg = BOOLEAN_FALSE;
    }

    if (ovpflg_i && *ovpflg_i == BOOLEAN_TRUE)
    {
       ovpflg = BOOLEAN_TRUE;
    }
    else
    {
        ovpflg = BOOLEAN_FALSE; 
    }

    if (rpqflg_i && *rpqflg_i == BOOLEAN_TRUE)
    {
        rpqflg = BOOLEAN_TRUE;
    }
    else
    {
        rpqflg = BOOLEAN_FALSE;
    }

    if (prcpri_i && misTrimLen(prcpri_i, PRICOD_LEN) > 0)
    {
        misTrimcpy (prcpri, prcpri_i, PRCPRI_LEN); 
    }
    else
    {
       misTrimcpy (prcpri, PRCPRI_DEFAULT, PRCPRI_LEN); 
    }

    if (sddflg_i && *sddflg_i == BOOLEAN_TRUE)
    {
        sddflg = BOOLEAN_TRUE;
    }
    else
    {
      sddflg = BOOLEAN_FALSE;
    }
       
    if (ctn_flg_i && *ctn_flg_i == BOOLEAN_TRUE)
    {
        ctn_flg = BOOLEAN_TRUE;
    }
    else
    {
        ctn_flg = BOOLEAN_FALSE;
    }
    
    if (sf_adr_id_i && misTrimLen(sf_adr_id_i, ADR_ID_LEN)) 
        misTrimcpy(sf_adr_id, sf_adr_id_i, ADR_ID_LEN);
        
    if (st_adr_id_i && misTrimLen(st_adr_id_i, ADR_ID_LEN)) 
        misTrimcpy(st_adr_id, st_adr_id_i, ADR_ID_LEN);
        
    if (rt_adr_id_i && misTrimLen(rt_adr_id_i, ADR_ID_LEN)) 
        misTrimcpy(rt_adr_id, rt_adr_id_i, ADR_ID_LEN);

    if (reffld_1_i && misTrimLen(reffld_1_i, REF_FLD_LEN)) 
        misTrimcpy(reffld_1, reffld_1_i, REF_FLD_LEN);
        
    if (reffld_2_i && misTrimLen(reffld_2_i, REF_FLD_LEN)) 
        misTrimcpy(reffld_2, reffld_2_i, REF_FLD_LEN);

    if (reffld_3_i && misTrimLen(reffld_3_i, REF_FLD_LEN)) 
        misTrimcpy(reffld_3, reffld_3_i, REF_FLD_LEN);

    if (reffld_4_i && misTrimLen(reffld_4_i, REF_FLD_LEN)) 
        misTrimcpy(reffld_4, reffld_4_i, REF_FLD_LEN);

    if (reffld_5_i && misTrimLen(reffld_5_i, REF_FLD_LEN)) 
        misTrimcpy(reffld_5, reffld_5_i, REF_FLD_LEN);

    if (tmInstalled)
    {
        if (src_sys_i && misTrimLen(src_sys_i, SRC_SYS_LEN))
        {
            /* This source system should be defined in the codmst Table.*/
            sprintf(sqlbuffer,
                    " select 'x' "
                    "   from codmst "
                    "  where colnam = 'src_sys'"
                    "    and codval = '%s'",
                    misStrReplaceAll(src_sys_i, "'", "''"));
    
            ret_status = sqlExecStr(sqlbuffer, NULL);
            if (eOK != ret_status)
            {
                if (eDB_NO_ROWS_AFFECTED == ret_status)
                {
                    sqlFreeResults(res);
                    return(APPInvalidArg(src_sys_i, "src_sys"));
                }
                else
                {
                    sqlFreeResults(res);
                    return(srvResults(ret_status, NULL));
                }
            }
        }
    
        if (sched_id_i && misTrimLen(sched_id_i, COLNAM_LEN))
        {
            /* This schedule id is FK to recur_sched Table.*/
            sprintf(sqlbuffer,
                    " select 'x' "
                    "   from recur_sched "
                    "  where recur_sched_id = '%s'",
                    misStrReplaceAll(sched_id_i, "'", "''"));
    
            ret_status = sqlExecStr(sqlbuffer, NULL);
            if (eOK != ret_status)
            {
                if (eDB_NO_ROWS_AFFECTED == ret_status)
                {
                    sqlFreeResults(res);
                    return(APPInvalidArg(sched_id_i, "sched_id"));
                }
                else
                {
                    sqlFreeResults(res);
                    return(srvResults(ret_status, NULL));
                }
            }
        }
        
        if (recur_sched_typ_i && misTrimLen(recur_sched_typ_i, RECUR_SCHED_TYP_LEN))
        {
            /* This schedule type should be defined in the codmst Table.*/
            sprintf(sqlbuffer,
                    " select 'x' "
                    "   from codmst "
                    "  where colnam = 'recur_sched_typ'"
                    "    and codval = '%s'",
                    misStrReplaceAll(recur_sched_typ_i, "'", "''"));
    
            ret_status = sqlExecStr(sqlbuffer, NULL);
            if (eOK != ret_status)
            {
                if (eDB_NO_ROWS_AFFECTED == ret_status)
                {
                    sqlFreeResults(res);
                    return(APPInvalidArg(recur_sched_typ_i, "recur_sched_typ"));
                }
                else
                {
                    sqlFreeResults(res);
                    return(srvResults(ret_status, NULL));
                }
            }
            
            misTrimcpy(recur_sched_typ, recur_sched_typ_i, RECUR_SCHED_TYP_LEN);
        }
    
        if (strncmp(ordsln_i, "0000",4) == 0)
        {
            /* Ship deliver window is required if schedule type is SHIP.*/
            if (strncmp(recur_sched_typ, RECUR_SCHED_TYP_SHIP, sizeof(RECUR_SCHED_TYP_SHIP)) == 0)
            {
                if (!sched_wnd_i || *sched_wnd_i == 0)
                {
                    return (srvErrorResults(eINT_SHIP_DELIVERY_WINDOW_REQUIRED, \
                        "Ship deliver window is required if schedule type is SHIP.",NULL));
                }
            }

            /* Ship deliver window is not used if schedule type is DELIVERY.*/
            if (strncmp(recur_sched_typ, RECUR_SCHED_TYP_DELIVERY, sizeof(RECUR_SCHED_TYP_DELIVERY)) == 0)
            {
                if (sched_wnd_i && *sched_wnd_i > 0)
                {
                    return (srvErrorResults(eINT_SHIP_DELIVERY_WINDOW_DISABLED, \
                        "Ship deliver window is only used if schedule type is SHIP.",NULL));
                }
            }
        }
    }
    
    if (!misTrimLen(rt_adr_id, ADR_ID_LEN) && 
        misTrimLen(st_adr_id, ADR_ID_LEN))
    {
        strcpy(rt_adr_id, st_adr_id);
    }
    
    /* If TM is installed, route to address is required (ship to address will be used if route to address is not provided.  */
    /* Raise warning only if both order and order line do not have route to(ship to) address defined*/
    if (tmInstalled)
    {
        if (!misTrimLen(rt_adr_id, ADR_ID_LEN))
        {
             sprintf(sqlbuffer,
             " select 1 "
             " from ord "
             " where client_id = '%s' "
             " and ordnum = '%s' "
             " and wh_id  = '%s'"
             " and (rt_adr_id is null and st_adr_id is null) ",
             client_id, ordnum_i, wh_id);
 
             ret_status = sqlExecStr(sqlbuffer, NULL);
        
             if (ret_status == eOK)
             {
                return(APPMissingArg("rt_adr_id"));
             }
        }
    }
    
    if (pln_ltlcls_i && misTrimLen(pln_ltlcls_i, PLN_LTLCLS_LEN) > 0)
    {
        misTrimcpy (pln_ltlcls, pln_ltlcls_i, PLN_LTLCLS_LEN); 
    }
    
    if (!entdte_i || !misTrimLen (entdte_i, STD_DATE_LEN))
    {
        misTrimcpy (entdte, "sysdate", STD_DATE_LEN);
        entdte_quotes = BOOLEAN_FALSE;
    }
    else
    {
        misTrimcpy (entdte, entdte_i, STD_DATE_LEN);
        entdte_quotes = BOOLEAN_TRUE;
    }
        
    if (early_shpdte_i && misTrimLen(early_shpdte_i, STD_DATE_LEN) > 0)
    {
        /* This date doesn't make sense with seconds values,
         * and they cause all sorts of issues when we're building
         * TMS orders, so we're going to 0 out the seconds.
         */
        memset (temp_dte, 0, sizeof(temp_dte));
        misTrimcpy(temp_dte, early_shpdte_i, DB_STD_DATE_LEN);
        temp_dte[12] = '0';
        temp_dte[13] = '0';

        strcpy (early_shpdte, temp_dte); 
    }

    if (late_shpdte_i && misTrimLen(late_shpdte_i, DB_STD_DATE_LEN) > 0)
    {
        /* This date doesn't make sense with seconds values,
         * and they cause all sorts of issues when we're building
         * TMS orders, so we're going to 0 out the seconds.
         */
        memset (temp_dte, 0, sizeof(temp_dte));
        misTrimcpy(temp_dte, late_shpdte_i, DB_STD_DATE_LEN);
        temp_dte[12] = '0';
        temp_dte[13] = '0';

        strcpy (late_shpdte, temp_dte); 
    }

    if (early_dlvdte_i && misTrimLen(early_dlvdte_i, DB_STD_DATE_LEN) > 0)
    {
        /* This date doesn't make sense with seconds values,
         * and they cause all sorts of issues when we're building
         * TMS orders, so we're going to 0 out the seconds.
         */
        memset (temp_dte, 0, sizeof(temp_dte));
        misTrimcpy(temp_dte, early_dlvdte_i, DB_STD_DATE_LEN);
        temp_dte[12] = '0';
        temp_dte[13] = '0';

        strcpy (early_dlvdte, temp_dte); 
    }

    if (late_dlvdte_i && misTrimLen(late_dlvdte_i, DB_STD_DATE_LEN) > 0)
    {
        /* This date doesn't make sense with seconds values,
         * and they cause all sorts of issues when we're building
         * TMS orders, so we're going to 0 out the seconds.
         */
        memset (temp_dte, 0, sizeof(temp_dte));
        misTrimcpy(temp_dte, late_dlvdte_i, DB_STD_DATE_LEN);
        temp_dte[12] = '0';
        temp_dte[13] = '0';

        strcpy (late_dlvdte, temp_dte); 
    }
    
    if (avail_pln_dte_i && misTrimLen(avail_pln_dte_i, STD_DATE_LEN) > 0)
    {
        /* This date doesn't make sense with seconds values,
         * and they cause all sorts of issues when we're building
         * TMS orders, so we're going to 0 out the seconds.
         */
        memset (temp_dte, 0, sizeof(temp_dte));
        misTrimcpy(temp_dte, avail_pln_dte_i, DB_STD_DATE_LEN);
        temp_dte[12] = '0';
        temp_dte[13] = '0';

        strcpy (avail_pln_dte, temp_dte); 
    }
    
    if (appt_dte_i && misTrimLen(appt_dte_i, STD_DATE_LEN) > 0)
    {
        /* This date doesn't make sense with seconds values,
         * and they cause all sorts of issues when we're building
         * TMS orders, so we're going to 0 out the seconds.
         */
        memset (temp_dte, 0, sizeof(temp_dte));
        misTrimcpy(temp_dte, appt_dte_i, DB_STD_DATE_LEN);
        temp_dte[12] = '0';
        temp_dte[13] = '0';

        strcpy (appt_dte, temp_dte); 
    }
    
    if (tms_snt_flg_i && *tms_snt_flg_i == BOOLEAN_TRUE)
    {
        tms_snt_flg = BOOLEAN_TRUE;
    }
    else
    {
        tms_snt_flg = BOOLEAN_FALSE; 
    }

    if (tms_dte_snt_i && misTrimLen(tms_dte_snt_i, DB_STD_DATE_LEN) > 0)
    {
        strcpy (tms_dte_snt, tms_dte_snt);
    }

    /* 
     *  Start Customer Requirement Check
     */
    if (custreqInstalled == 1 )
    {
        sprintf(sqlbuffer, 
                "create special handling instructions "
                " where client_id     = '%s' "
                "   and ordnum        = '%s' "
                "   and prtnum        = '%s' "
                "   and prt_client_id = '%s' "
                "   and ordlin        = '%s' "
                "   and ordsln        = '%s' "
                "   and wh_id         = '%s' ",
                client_id,
                misTrim(ordnum_i),
                misTrim(prtnum_i),
                misTrim(prt_client_id),
                misTrim(ordlin_i),
                misTrim(ordsln_i),
                wh_id);
    
        errcode = srvInitiateCommand (sqlbuffer, &CurPtr);
        if (errcode != eOK) 
        {
            free(valuelist);
            return(srvResults(errcode, NULL));
        }
    }

    /*  Determine if we need to call to get the customer requirement fields.
     *  If all the customer requirement fields are populated, then there is
     *  no reason to collect the customer requirements.
     */
    if (custreqInstalled == 1 )
    {
        if ( (manfid_i  && misTrimLen(manfid_i, MANFID_LEN) > 0) &&
             (deptno_i  && misTrimLen(deptno_i, DEPTNO_LEN) > 0) &&
             (alc_search_path_i &&
              misTrimLen(alc_search_path_i, ALC_SEARCH_PATH_LEN) > 0) &&
             (cstprt_i  && misTrimLen(cstprt_i, CSTPRT_LEN) > 0) &&
             (bckflg_i) &&
             (splflg_i) &&
             (stdflg_i) &&
             (parflg_i) &&
             (untcas_i  && *untcas_i > 0) &&
             (untpak_i  && *untpak_i > 0) &&
             (untpal_i  && *untpal_i > 0) &&
             (ordinv_i  && misTrimLen(ordinv_i, ORDINV_LEN) > 0) &&
             (misTrimLen(rsvpri, RSVPRI_LEN) > 0) &&
             (dtcflg && frsflg_i) &&
             (dtcflg && min_shelf_hrs_i && *min_shelf_hrs_i >= 0)  &&
             (invsts_prg_i && misTrimLen (invsts_prg_i, INVSTS_PRG_LEN)) )
        {
            CheckCustReqs = 0;
        }
    }
    else
    {
        CheckCustReqs = 0;
    }

    /*
     *  If we need to get the customer requirement fields call the command.
     */
    if ( CheckCustReqs == 1 )
    {
        sprintf(sqlbuffer,
                "get customer requirements "
                "where prtnum    = '%s' "
                "  and prt_client_id = '%s' "
                "  and client_id = '%s' "
                "  and ordnum    = '%s' "
                "  and wh_id     = '%s' ",
                prtnum_i,
                prt_client_id_i,
                client_id,
                ordnum_i,
                wh_id);

        CurPtr = NULL;
        errcode = srvInitiateCommand (sqlbuffer, &CurPtr);
        if (errcode != eOK) 
        {
            free(valuelist);
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            return(srvResults(errcode, NULL));
        }

        /*
         *  Populate any CR field that is blank/null.
         */
        tempres = srvGetResults(CurPtr);
        temprow = sqlGetRow(tempres);
    }

    if (manfid_i && misTrimLen(manfid_i, MANFID_LEN) > 0)
    {
        strncpy(manfid, manfid_i, MANFID_LEN);
    }
    else if (CheckCustReqs == 1)
    {
        misTrimcpy(manfid, sqlGetString(tempres, temprow,"manfid"),MANFID_LEN);
    }

    if (deptno_i && misTrimLen(deptno_i, DEPTNO_LEN) > 0)
    {
        misTrimcpy(deptno, deptno_i, DEPTNO_LEN);
    }
    else if (CheckCustReqs == 1)
    {
        strncpy(deptno, sqlGetString(tempres, temprow,"deptno"), DEPTNO_LEN);
    }

    if (alc_search_path_i &&
        misTrimLen(alc_search_path_i, ALC_SEARCH_PATH_LEN) > 0)
    {
        misTrimcpy(alc_search_path, alc_search_path_i, ALC_SEARCH_PATH_LEN);
    }
    else if (CheckCustReqs == 1)
    {
        strncpy(alc_search_path,
                sqlGetString(tempres, temprow, "alc_search_path"),
                ALC_SEARCH_PATH_LEN);
    }

    if (cstprt_i && misTrimLen(cstprt_i, CSTPRT_LEN) > 0)
    {
        misTrimcpy(cstprt, cstprt_i, CSTPRT_LEN);
    }
    else if (CheckCustReqs == 1)
    {
        misTrimcpy(cstprt, sqlGetString(tempres, temprow,"cstprt"),CSTPRT_LEN);
    }

    if (bckflg_i)
    {
        bckflg = *bckflg_i;
    }
    else if (CheckCustReqs == 1)
    {
        bckflg = sqlGetBoolean(tempres, temprow, "bckflg");
    }

    if (splflg_i)
    {
        splflg = *splflg_i;
    }
    else if (CheckCustReqs == 1)
    {
        splflg = sqlGetBoolean(tempres, temprow, "splflg");
    }

    if (stdflg_i)
    {
        stdflg = *stdflg_i;
    }
    else if (CheckCustReqs == 1)
    {
        stdflg = sqlGetBoolean(tempres, temprow, "stdflg");
    }

    if (parflg_i)
    {
        parflg = *parflg_i;
    }
    else if (CheckCustReqs == 1)
    {
        parflg = sqlGetBoolean(tempres, temprow, "parflg");
    }

    if (untcas_i && *untcas_i >= 0)
    {
        *p_untcas = *untcas_i;
    }
    else if (CheckCustReqs == 1)
    {
        *p_untcas = sqlGetLong(tempres, temprow,"untcas");
    }
    else
    {
	p_untcas = NULL;
    }

    if (untpak_i && *untpak_i >= 0)
    {
        *p_untpak = *untpak_i;
    }
    else if (CheckCustReqs == 1)
    {
        *p_untpak = sqlGetLong(tempres, temprow,"untpak");
    }
    else
    {
	p_untpak = NULL;
    }

    if (untpal_i && *untpal_i >= 0)
    {
        *p_untpal = *untpal_i;
    }
    else if (CheckCustReqs == 1)
    {
        *p_untpal = sqlGetLong(tempres, temprow,"untpal");
    }
    else
    {
	p_untpal = NULL;
    }

    if (ordinv_i && misTrimLen(ordinv_i, ORDINV_LEN) >= 0)
    {
        misTrimcpy(ordinv, ordinv_i, ORDINV_LEN);
    }
    else if (CheckCustReqs == 1)
    {
        misTrimcpy(ordinv, sqlGetString(tempres, temprow, "ordinv"), ORDINV_LEN);
    }

    /* if the part is date controlled, then setup the freshness flag and 
     * minimum shelf hours.  If not supplied, then get the defaults
     */

    if (dtcflg == BOOLEAN_TRUE)
    {
        
        /* JJS 09/04/2009 - Look at customer requirements even if 0 was
         * passed in (changed ">= 0" to "> 0")
         */
        if (min_shelf_hrs_i && *min_shelf_hrs_i > 0)
        {
            min_shelf_hrs = *min_shelf_hrs_i;
        }
        else if (CheckCustReqs == 1)
        {
            min_shelf_hrs = sqlGetLong (tempres, temprow, "min_shelf_hrs");
        }
        else
        {
            min_shelf_hrs = 0;
        }
    }

    if (dtcflg == BOOLEAN_TRUE)
    {
        if (frsflg_i)
        {
            frsflg = *frsflg_i;
        }
        else if (CheckCustReqs == 1)
        {
            frsflg = sqlGetBoolean (tempres, temprow, "frscod");
        }
    }
    else
    {
        frsflg = BOOLEAN_FALSE;
    }

    if (invsts_prg_i && misTrimLen(invsts_prg_i, INVSTS_PRG_LEN) >= 0)
    {
        /*
         * Validate the inventory status progression sent down.
         */

        misTrimcpy(invsts_prg, invsts_prg_i, INVSTS_PRG_LEN);

        sprintf(sqlbuffer,
                "select 'x' "
                "  from prgmst "
                " where invsts_prg = '%s' ",
                invsts_prg);

        ret_status = sqlExecStr(sqlbuffer, NULL);

        if (eOK != ret_status)
        {
            if ((ret_status == eDB_NO_ROWS_AFFECTED) ||
                (ret_status == eSRV_NO_ROWS_AFFECTED))
                return (APPInvalidArg(invsts_prg, "invsts_prg"));
            else
                return (srvResults(ret_status, NULL));
        }
    }
    else if (host_invsts_i && misTrimLen(host_invsts_i, INVSTS_LEN))
    {
        /* 
         * If they didn't pass in the invsts_prg, they can pass in an invsts
         * that will map back to the prgmst table that has the same name
         * as the invsts.  That entry can have one and only one inventory
         * status on it, and it must be the same as the one passed in.
         */

        sprintf(sqlbuffer,
                "select 'x' "
                "  from prgmst o "
                " where o.invsts_prg = '%s' "
                "   and rownum < 2 "
                "   and not exists "
                " (select 'x' "
                "    from prgmst i "
                "   where i.invsts_prg = o.invsts_prg "
                "     and i.invsts != o.invsts) ",
                host_invsts_i);

        ret_status = sqlExecStr(sqlbuffer, NULL);
        if (eOK == ret_status)
        {
            /* We have a valid invsts_prg record, see if it's different.
             *
             */
            
            strncpy(invsts_prg, host_invsts_i, INVSTS_LEN);
        }
    }

    if (!strlen(invsts_prg) && CheckCustReqs)
    {
        misTrimcpy(invsts_prg, sqlGetString(tempres, temprow, "invsts_prg"), 
                   INVSTS_PRG_LEN);
    }
    
    if (misTrimLen(rsvpri, RSVPRI_LEN) == 0 &&
        CheckCustReqs == 1 && !sqlIsNull(tempres, temprow, "rsvpri"))
    {
        /* 
         * Use the customer requirements rsvpri if it exists and it
         * wasn't passed directly in. 
         */

        misTrc(T_FLOW, "Using rsvpri from customer requirement");
        misTrimcpy(rsvpri, sqlGetString(tempres,temprow,"rsvpri"), RSVPRI_LEN);
    }

    if ( CheckCustReqs == 1 )
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
    }

    /*
     *  CUSTOMER REQUIREMENT FIELDS
     */

    misTrc(T_FLOW, "Taking care of customer requirement fields");


    if (parflg > 0)
    {
        parflg = BOOLEAN_TRUE; 
    }
    else
    {
        parflg = BOOLEAN_FALSE;
    }

    if (bckflg == BOOLEAN_TRUE || bckflg == BOOLEAN_NOTSET) 
    {
        bckflg = BOOLEAN_TRUE;
    }
    else
    {
        bckflg = BOOLEAN_FALSE;
    }

    if (splflg == BOOLEAN_TRUE || splflg == BOOLEAN_NOTSET)
    {
        splflg = BOOLEAN_TRUE;
    }
    else
    {
       splflg = BOOLEAN_FALSE;
    }

    if (stdflg == BOOLEAN_TRUE )
    {
        stdflg = BOOLEAN_TRUE;
    }
    else
    {
        stdflg = BOOLEAN_FALSE;
    }
    
    if (edtflg_i && *edtflg_i == BOOLEAN_TRUE )
    {
        edtflg = BOOLEAN_TRUE;
    }
    else
    {
        edtflg = BOOLEAN_FALSE;
    }
    
    if (xdkflg_i && *xdkflg_i == BOOLEAN_TRUE )
    {
        xdkflg = BOOLEAN_TRUE;
    }
    else
    {
        xdkflg = BOOLEAN_FALSE;
    }

    if (atoflg_i && *atoflg_i == BOOLEAN_TRUE )
    {
        atoflg = BOOLEAN_TRUE;
    }
    else
    {
        atoflg = BOOLEAN_FALSE;
    }

    if (dstare_i)
    {
        if (misTrimLen(dstare_i, ARECOD_LEN))
        {
            sprintf(sqlbuffer,
                    " select aremst.* "
                    " from aremst "
                    " where arecod = '%s' "
                    "   and wh_id  = '%s' ",
                     dstare_i,
                     wh_id);
            
            ret_status = sqlExecStr(sqlbuffer, NULL);
   
            if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
            {
                misTrc(T_FLOW, "Error validating arecod-wh_id.");
                return (srvResults(ret_status, NULL));
            }
            else if (ret_status == eDB_NO_ROWS_AFFECTED) 
            {
                sprintf (sqlbuffer, "%s-%s", dstare_i, wh_id);
                return APPInvalidArg(sqlbuffer, "dstare-wh_id");
            }
        }
    }

    if (dstloc_i)
    {
        if (misTrimLen(dstloc_i, STOLOC_LEN))
        {
           sprintf(sqlbuffer,
                   " select locmst.* "
                   " from locmst "
                   " where stoloc = '%s' "
                   "   and wh_id  = '%s' ",
                    dstloc_i,
                    wh_id);
           
            ret_status = sqlExecStr(sqlbuffer, NULL);
            
            if (ret_status != eOK  && ret_status != eDB_NO_ROWS_AFFECTED)
            {     
                misTrc(T_FLOW, "Error validating stoloc-wh_id.");
                return (srvResults(ret_status, NULL));
            
            }
            else if (ret_status == eDB_NO_ROWS_AFFECTED) 
            {
                sprintf (sqlbuffer, "%s-%s", dstloc_i, wh_id);
                return APPInvalidArg(sqlbuffer, "dstloc-wh_id");
            }
        }
    }

    misTrc(T_FLOW, "Taking care of the non allocate flag");
    if (non_alc_flg_i && 
        *non_alc_flg_i == BOOLEAN_TRUE  && 
        xdkflg == BOOLEAN_FALSE)
    {
        non_alc_flg = BOOLEAN_TRUE;
    }
    else
    {
        non_alc_flg = BOOLEAN_FALSE;
    }
    
	misTrc(T_FLOW, "Checking if we need to default the rsvpri");
    if (misTrimLen(rsvpri, RSVPRI_LEN) == 0)
    {
        /* 
         * If we still don't have a value for rsvpri, we need to
         * set a default value because it is a required value on the
         * order line.
         */
        misTrimcpy(rsvpri, RSVPRI_DEFAULT, RSVPRI_LEN);
    }
    else 
        misTrimcpy(rsvpri, rsvpri_i, RSVPRI_LEN); 
        


    /* manipulate reservation and pickable quantity.
     *
     */
    if (mode_i && misCiStrcmp(mode_i, "SEAMLES") == 0)
    {
         /* 
          * mode is SEAMLES 
          */

         /*
          * If the rsvqty is downloaded through Seamles as null, 
          * we'll be receiving it here as a 0. So, we only want 
          * to set it to the value being passed if it is > 0. 
          */

        if (rsvqty_i && *rsvqty_i > 0)
        {
            rsvqty = *rsvqty_i;
        }
        else
        {
            if (!pckrsvmgrInstalled)
                rsvqty = *ordqty_i;
            else if ((crossdockInstalled) && (xdkflg == BOOLEAN_TRUE))
                rsvqty = *ordqty_i;
            else if (non_alc_flg == BOOLEAN_TRUE)
                rsvqty = *ordqty_i;
            else
                rsvqty = 0;
        }

        if (pckqty_i && *pckqty_i > -1)
        {
            pckqty = *pckqty_i;
        }
        else
        {
            if (!pckrsvmgrInstalled)
                pckqty = *ordqty_i;
            else if ((crossdockInstalled) && (xdkflg == BOOLEAN_TRUE))
                pckqty = *ordqty_i;
            else if (non_alc_flg == BOOLEAN_TRUE)
                pckqty = *ordqty_i;
            else
                pckqty = 0;
        }
    }
    else
    {
        misTrc(T_FLOW, "Mode is not SeamLes");
        /* mode is GUI */
        if (!pckrsvmgrInstalled)
            rsvqty = *ordqty_i;
        else if ((crossdockInstalled) && (xdkflg == BOOLEAN_TRUE))
            rsvqty = *ordqty_i;
        else if (non_alc_flg == BOOLEAN_TRUE)
        {
            misTrc(T_FLOW, "Is a non-allocatable order line");
            rsvqty = *ordqty_i;
        }
        else
        {
            misTrc(T_FLOW, "Pick Rsv. installed, cross dock not installed");
            rsvqty = 0;
        }

        misTrc(T_FLOW, "Setting the pick qty");
        if (!pckrsvmgrInstalled)
            pckqty = *ordqty_i;
        else if ((crossdockInstalled) && (xdkflg == BOOLEAN_TRUE))
            pckqty = *ordqty_i;
        else if (non_alc_flg == BOOLEAN_TRUE)
            pckqty = *ordqty_i;
        else
            pckqty = 0;
    }
    misTrc(T_FLOW, "Taking care of the rsvqty");
   
    sprintf(rsvqty_tmp, "%ld",  rsvqty);
       
    sprintf(pckqty_tmp, "%ld",  pckqty); 
    
    sprintf(shpqty_tmp, "%ld",  shpqty);
    
  
    if (frsflg != BOOLEAN_TRUE)
    {    
        frsflg = BOOLEAN_FALSE;
    }
    else
    {
        frsflg = BOOLEAN_TRUE;
    }

    /*validate tms_pln_sts code */
    if (tms_pln_sts_i && misTrimLen(tms_pln_sts_i, TMS_PLN_STS_COD_LEN))
    {
         sprintf(sqlbuffer,
             " list code descriptions "
             "    where colnam = 'tms_pln_sts' "
             "      and codval = '%s' ", tms_pln_sts_i);

        ret_status = srvInitiateCommand(sqlbuffer, NULL);        

        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            
            misTrc(T_FLOW, " Error validating colnam-codval");
            return(srvResults(ret_status, NULL));
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED) 
        {
            return(APPInvalidArg(tms_pln_sts_i, "tms_pln_sts"));
            
        }
        
        misTrimcpy(tms_pln_sts, tms_pln_sts_i, TMS_PLN_STS_COD_LEN);
    }
    else
    {
        misTrimcpy(tms_pln_sts, TMS_PLN_STS_READY_TO_AUTOSTAGE, TMS_PLN_STS_COD_LEN);
    }

    if(tmInstalled)
    {
        if ((!pln_ltlcls_i) || !(*pln_ltlcls))
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            sprintf(sqlbuffer,
                    " list policies "
                    " where polcod = 'TM-APPLICATION' "
                    " and polvar = 'ORDER' "
                    " and polval = 'REQUIRED-FIELDS' "
                    " and rtstr1 = 'pln_ltlcls' ");

            ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            if (ret_status == eOK)
            {
                if (misCiStrcmp(tms_pln_sts, TMS_PLN_STS_VENDOR_READY) != 0)
                {
                    return APPMissingArg("pln_ltlcls");
                }
            }
        }

        /* Derive pln_ltlcls if it was not provided. Derivation will be as follows:- 
         * If prtnum is available, check if prtmst.ltlcls is available for the prtnum
         * if prtmst.ltlcls is not null, then update ord_line.pln_ltlcls with this value. 
         * If prtnum or prtmst.ltlcls is null and comcod is available, check if commst.ltlcls 
         * is available for the comcod, if comcod.ltlcls is not null update ord_line.pln_ltlcls 
         * with it's value. If comcod or commst.ltlcls is null and prtnum is available, 
         * check if prtmst.comcod for prtnum is available. If it is the check if commst.ltlcls 
         * for prtmst.comcod is available. If it is then update pln_ltlcls with commst.ltlcls 
         * otherwise return an error.
         */
         
        /*here validate if the comcod is correct*/ 
        if( pln_comcod_i && misTrimLen(pln_comcod_i, PLN_COMCOD_LEN))
         {
            sprintf(sqlbuffer,
                        "[select count(1) is_comcod "
                        " from dscmst "
                        " where colnam='comcod' "
                        " and colval = '%s']"
                        "|"
                        "if (@is_comcod = 0)"
                        "{"
                        "set return status where status  = 11101"
                        "}",
                        pln_comcod_i);
            ret_status = srvInitiateCommand(sqlbuffer, NULL);             
                        
          if(ret_status !=eOK)  
          {
             srvFreeMemory(SRVRET_STRUCT, CurPtr);
            return (srvResults(ret_status, NULL));
          }

         }
         
        if (!pln_ltlcls_i || misTrimLen(pln_ltlcls_i, PLN_LTLCLS_LEN) == 0)
        {   

            /* 
             * We've already tried to select data for this part, if no data is
             * retrieved, no more process for this part is needed.
             */
            if (ValidPrt == 1)
            {
                /* Try to derive pln_ltlcls from prtnum */
                if (!sqlIsNull(prtres, prtrow, "ltlcls"))
                {
                    misTrimcpy(pln_ltlcls, sqlGetString(prtres, prtrow, "ltlcls"), PLN_LTLCLS_LEN);
                }
            }


            /* Check to see if pln_ltlcls was derived, and if not,
             *  check to see if pln_comcod is available */
            if (!(*pln_ltlcls) && (pln_comcod_i || misTrimLen(pln_comcod_i , PLN_COMCOD_LEN) > 0))
            {
                /* Try to derive pln_ltlcls from comcod */
                sprintf(sqlbuffer,
                        "[ select commst.ltlcls "
                        " from commst "
                        " where comcod = '%s' "
                        " and ltlcls IS NOT NULL ]",
                        pln_comcod_i);

                ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
 
                if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
                { 
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    return (srvResults(ret_status, NULL));
                }

                if (ret_status == eOK)
                {
                    res = srvGetResults(CurPtr);
                    row = sqlGetRow(res);
                    misTrimcpy(pln_ltlcls, sqlGetString(res, row, "ltlcls"), PLN_LTLCLS_LEN);
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    CurPtr = NULL;
                    res = NULL;
                    row = NULL;
                }
            }
            
            
            /* Check to see if pln_ltlcls was derived, and if not,
             *  check to see if prtnum is available */
            if (!(*pln_ltlcls) && (ValidPrt == 1))
            {  
                if (!sqlIsNull(prtres, prtrow, "comcod"))
                {
                    /* Check to see if commst.ltlcls is available for prtmst.comcod */
                    sprintf(sqlbuffer,
                        "[ select commst.ltlcls "
                        " from commst "
                        " where comcod = '%s' "
                        "   and ltlcls IS NOT NULL ]",
                    sqlGetString(prtres, prtrow, "comcod"));
 
                    ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
                
                    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
                    { 
                        srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        return (srvResults(ret_status, NULL));
                    }

                    if (ret_status == eOK)
                    {
                        /* Update pln_ltlcls with commst.ltlcls */
                        res = NULL;
                        row = NULL;
                        
                        res = srvGetResults(CurPtr);
                        row = sqlGetRow(res);
                        misTrimcpy(pln_ltlcls, sqlGetString(res, row, "ltlcls"), PLN_LTLCLS_LEN);
                        srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        CurPtr = NULL;
                        res = NULL;
                        row = NULL;
                    }
                }                      
            }
            
            if (!tmInstalled)
            {
                /* Could not derive pln_ltlcls */
                if(!(*pln_ltlcls))
                {
                    return APPMissingArg("pln_ltlcls");
                }
            }
        }
        
        /*
         * If the tot_pln_wgt,  tot_pln_cube, tot_pln_pal_qty, tot_pln_cas_qty,
         * tot_pln_misc_qty and tot_pln_misc2_qty are downloaded through Seamles 
         * as null, we'll be receiving it here as a 0. So, we only want 
         * to set it to the value being passed if it is > 0. 
         */

        /* p_tot_pln_wgt, p_tot_pln_cube, p_tot_pln_pal_qty, p_tot_pln_cas_qty must 
         * point to an allocated address so that the value they point to can be changed
         */
        p_tot_pln_wgt = &tot_pln_wgt;
        p_tot_pln_cube = &tot_pln_cube;
        p_tot_pln_pal_qty = &tot_pln_pal_qty;
        p_tot_pln_cas_qty = &tot_pln_cas_qty;
        
        /* If total plan weight was not given, try to derive its value */

        if (!tot_pln_wgt_i || *tot_pln_wgt_i <= 0 || 
            (mode_i && (misCiStrcmp(mode_i, "SEAMLES") == 0) && !(*tot_pln_wgt_i > 0)))
        {
            if (ValidPrt == 1)
            {				
                if (!sqlIsNull(prtres, prtrow, "grswgt"))
                {
                    *p_tot_pln_wgt = ordqty * (sqlGetFloat(prtres, prtrow, "grswgt")/sqlGetFloat(prtres, prtrow, "untcas"));
                }
                else if (!sqlIsNull(prtres, prtrow, "netwgt"))
                {
                    *p_tot_pln_wgt = ordqty * (sqlGetFloat(prtres, prtrow, "netwgt")/sqlGetFloat(prtres, prtrow, "untcas"));
                }
                else
                {
                    p_tot_pln_wgt = NULL;
                }

                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
                res = NULL;
                row = NULL;
            }
            else
            {
                p_tot_pln_wgt = NULL;
            }
            
            if (!p_tot_pln_wgt)
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
                sprintf(sqlbuffer,
                        " list policies "
                        " where polcod = 'TM-APPLICATION' "
                        " and polvar = 'ORDER' "
                        " and polval = 'REQUIRED-FIELDS' "
                        " and rtstr1 = 'tot_pln_wgt' ");

                ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
                if (ret_status == eOK)
                {
                    if (misCiStrcmp(tms_pln_sts, TMS_PLN_STS_VENDOR_READY) != 0)
                    {
                        return APPMissingArg("tot_pln_wgt");
                    }
                }
            }
        }
        else
        {
            *p_tot_pln_wgt = *tot_pln_wgt_i; 
        }

        /* If total plan cube was not given, try to derive its value */ 
        if (!tot_pln_cube_i || *tot_pln_cube_i <= 0 ||
            (mode_i && (misCiStrcmp(mode_i, "SEAMLES") == 0) && !(*tot_pln_cube_i > 0)))
        {
            if (ValidPrt == 1)
            {
                sprintf(sqlbuffer,
                        "[ select ftp.untlen, ftp.untwid, ftp.unthgt "
                        " from ftpmst ftp, "
                        "      prtmst prt "
                        " where ftp.ftpcod = prt.ftpcod "
                        "   and prtnum = '%s' ]",
                        prtnum_i);

                ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
                
                if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
                { 
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    return (srvResults(ret_status, NULL));
                }
                if (ret_status == eOK)
                {
                    res = srvGetResults(CurPtr);
                    row = sqlGetRow(res);
                    
                    *p_tot_pln_cube = ordqty * 
                                       sqlGetFloat(res, row, "untwid") *
                                       sqlGetFloat(res, row, "untlen") *
                                       sqlGetFloat(res, row, "unthgt");
                     srvFreeMemory(SRVRET_STRUCT, CurPtr);
                     CurPtr = NULL;
                     res = NULL;
                     row = NULL;
                }
                else
                {
                    p_tot_pln_cube = NULL;
                }
            }
            else
            {
                p_tot_pln_cube = NULL;
            }
            
            if (!p_tot_pln_cube)
            {
               sprintf(sqlbuffer,
                        " list policies "
                        " where polcod = 'TM-APPLICATION' "
                        " and polvar = 'ORDER' "
                        " and polval = 'REQUIRED-FIELDS' "
                        " and rtstr1 = 'tot_pln_cube' ");

                ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
				
                if (ret_status == eOK)
                {
                    if (misCiStrcmp(tms_pln_sts, TMS_PLN_STS_VENDOR_READY) != 0)
                    {
                        return APPMissingArg("tot_pln_cube");
                    }
                }
            }
        }
        else
        {
            *p_tot_pln_cube = *tot_pln_cube_i; 
        }
        

        /* If total plan pallets was not given, try to derive its value */
        if (!tot_pln_pal_qty_i || *tot_pln_pal_qty_i <= 0 ||
            (mode_i && (misCiStrcmp(mode_i, "SEAMLES") == 0) && !(*tot_pln_pal_qty_i > 0)))
        {
            if (ValidPrt == 1)
            {
                if (sqlGetFloat(prtres, prtrow, "untpal"))
                {
                    *p_tot_pln_pal_qty = ordqty / sqlGetFloat(prtres, prtrow, "untpal");
                }                    
                else
                {
                    p_tot_pln_pal_qty = NULL;
                }
            }
            else
            {
                p_tot_pln_pal_qty = NULL;
            }
            
            if (!p_tot_pln_pal_qty)
            {
               sprintf(sqlbuffer,
                        " list policies "
                        " where polcod = 'TM-APPLICATION' "
                        " and polvar = 'ORDER' "
                        " and polval = 'REQUIRED-FIELDS' "
                        " and rtstr1 = 'tot_pln_pal_qty' ");

                ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
				
                if (ret_status == eOK)
                {
                    if (misCiStrcmp(tms_pln_sts, TMS_PLN_STS_VENDOR_READY) != 0)
                    {
                        return APPMissingArg("tot_pln_pal_qty");
                    }
                }
            }
        }
        else
        {
            *p_tot_pln_pal_qty = *tot_pln_pal_qty_i;
        }       

        /* If total plan misc quantity was not given,
         *  check to see if its a required field */
        if (!tot_pln_misc_qty_i || *tot_pln_misc_qty_i <= 0 ||
            (mode_i && (misCiStrcmp(mode_i, "SEAMLES") == 0) && !(*tot_pln_misc_qty_i > 0)))
        {
            sprintf(sqlbuffer,
                    " list policies "
                    " where polcod = 'TM-APPLICATION' "
                    " and polvar = 'ORDER' "
                    " and polval = 'REQUIRED-FIELDS' "
                    " and rtstr1 = 'tot_pln_misc_qty' ");
                    
            ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
			CurPtr = NULL;
            if (ret_status == eOK)
            {
                if (misCiStrcmp(tms_pln_sts, TMS_PLN_STS_VENDOR_READY) != 0)
                {
                    return APPMissingArg("tot_pln_misc_qty");
                }
            }         
        }
        
        /* If total plan cases was not given, try to derive its value */
        if (!tot_pln_cas_qty_i || *tot_pln_cas_qty_i <= 0 ||
            (mode_i && (misCiStrcmp(mode_i, "SEAMLES") == 0) && !(*tot_pln_cas_qty_i > 0)))
        {
            if (ValidPrt == 1)
            {
                if (sqlGetFloat(prtres, prtrow, "untcas"))
                {
                    *p_tot_pln_cas_qty = ordqty / sqlGetFloat(prtres, prtrow, "untcas");
                }                    
                else
                {
                    p_tot_pln_cas_qty = NULL;
                }
            }
            else
            {
                misTrc(T_FLOW, "Cannot derive tot_pln_cas_qty");
                p_tot_pln_cas_qty = NULL;
            }
            
            if (!p_tot_pln_cas_qty)
            {
                sprintf(sqlbuffer,
                        " list policies "
                        " where polcod = 'TM-APPLICATION' "
                        " and polvar = 'ORDER' "
                        " and polval = 'REQUIRED-FIELDS' "
                        " and rtstr1 = 'tot_pln_cas_qty' ");

                ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
				
                if (ret_status == eOK)
                {
                    if (misCiStrcmp(tms_pln_sts, TMS_PLN_STS_VENDOR_READY) != 0)
                    {
                        return APPMissingArg("tot_pln_cas_qty");
                    }
                }
            }
		}
		else
        {
            *p_tot_pln_cas_qty = *tot_pln_cas_qty_i;
        }
		
		 if (!tot_pln_misc2_qty_i || *tot_pln_misc2_qty_i <= 0 ||
		     (mode_i && (misCiStrcmp(mode_i, "SEAMLES") == 0) && !(*tot_pln_misc2_qty_i > 0)))
        {
            sprintf(sqlbuffer,
                    " list policies "
                    " where polcod = 'TM-APPLICATION' "
                    " and polvar = 'ORDER' "
                    " and polval = 'REQUIRED-FIELDS' "
                    " and rtstr1 = 'tot_pln_misc2_qty' ");

            ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
			CurPtr = NULL;
            if (ret_status == eOK)
            {
                if (misCiStrcmp(tms_pln_sts, TMS_PLN_STS_VENDOR_READY) != 0)
                {
                    return APPMissingArg("tot_pln_misc2_qty");
                }
            }
        }
              
    }
    
    /* Check insurance coverage relevant fields only for TM */
    if (tmInstalled)
    {
         /* 
         * These pointer will only be initialized when tm is installed 
         * So if tm is not installed, these fields will remain DBNULL in database.
         * Because in migrate script we only migrate those that are DBNULL
         */
        p_rel_val_unt_typ = rel_val_unt_typ_i;
        p_rel_val = &rel_val;
        p_unt_ins_val = &unt_ins_val;

        if (!unt_ins_val_i)
        {
            if (ValidPrt == 1 && !sqlIsNull(prtres, prtrow, "unt_ins_val"))
            {
                /* Try to derive rel_val from prtnum */
                unt_ins_val = sqlGetFloat(prtres, prtrow, "unt_ins_val");
            }
            else
            {
                sprintf(sqlbuffer,
                        " list policies "
                        " where polcod = 'TM-APPLICATION' "
                        " and polvar = 'ORDER' "
                        " and polval = 'REQUIRED-FIELDS' "
                        " and rtstr1 = 'unt_ins_val' ");
                        
                ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
                if (ret_status == eOK)
                {
                    return APPMissingArg("unt_ins_val");
                } 
            }
        }
        else
        {
            /* Validate number */
            sprintf(sqlbuffer,
                " validate number "
                " where colnam = 'unt_ins_val_i' "  
                " and intflg = 0 "
                " and compare = 'GTE' "
                " and value = %f  "
                " and limit = 0 ", *unt_ins_val_i);
                
            ret_status = srvInitiateCommand(sqlbuffer, NULL);
            
            if (ret_status != eOK)
            {            
                misTrc(T_FLOW, " Error validating unt_ins_val");
                return(srvResults(ret_status, NULL));
            }
            unt_ins_val = *unt_ins_val_i;
        }
        
        if (!rel_val_i)
        {
            if (ValidPrt == 1 && !sqlIsNull(prtres, prtrow, "rel_val"))
            {
                /* Try to derive rel_val from prtnum */
                rel_val = sqlGetFloat(prtres, prtrow, "rel_val");
            }
            else
            {
                sprintf(sqlbuffer,
                        " list policies "
                        " where polcod = 'TM-APPLICATION' "
                        " and polvar = 'ORDER' "
                        " and polval = 'REQUIRED-FIELDS' "
                        " and rtstr1 = 'rel_val' ");
                        
                ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
                if (ret_status == eOK)
                {
                    return APPMissingArg("rel_val");
                } 
            }
        }
        else
        {
            /* Validate number */
            sprintf(sqlbuffer,
                " validate number "
                " where colnam = 'rel_val' "  
                " and intflg = 0 "
                " and compare = 'GTE' "
                " and value = %f  "
                " and limit = 0 ", *rel_val_i);
                
            ret_status = srvInitiateCommand(sqlbuffer, NULL);
            
            if (ret_status != eOK)
            {            
                misTrc(T_FLOW, " Error validating rel_val");
                return(srvResults(ret_status, NULL));
            }
            rel_val = *rel_val_i;
        }

        if (rel_val_unt_typ_i && misTrimLen(rel_val_unt_typ_i, REL_VAL_UNT_TYP_LEN))
        {
            /* Validate against codmst */
            sprintf(sqlbuffer,
                    " list code descriptions "
                    " where colnam = 'act_unit_typ' "
                    " and codval = '%s' ", rel_val_unt_typ_i);
        
            ret_status = srvInitiateCommand(sqlbuffer, NULL);
        
            if (ret_status != eOK)
            {            
                misTrc(T_FLOW, " Error validating rel_val_unt_typ");
                return (APPInvalidArg(rel_val_unt_typ_i, "rel_val_unt_typ"));
            }  

            misTrimcpy(rel_val_unt_typ, rel_val_unt_typ_i, REL_VAL_UNT_TYP_LEN);
            
            /* We only support 'WEIGHT' for now */
            if (misTrimStrncmp(rel_val_unt_typ, DEFAULT_REL_VAL_UNT_TYP,REL_VAL_UNT_TYP_LEN)!=0)
            {
                return srvResults(eSAL_REL_VAL_UNT_TYP_ERROR, NULL);
            }
        }
    }

    /* All process pertinent to part has finished, free the result */
    sqlFreeResults (prtres);
    prtrow = NULL;

    misTrc(T_FLOW, "Creating the new record");

    
    /* Note, we are going to publish out the pckqty, because there
     * may be things triggered that want the pckqty (like order 
     * activity) and if the pckqty was not passed in, then this function
     * figures it out, so we need to publish it out for visiblity.
     */
    
    CurPtr = appCreateTableData("ord_line",
                "client_id", client_id, COMTYP_STRING, CLIENT_ID_LEN,
                             BOOLEAN_TRUE,
                "ordnum", ordnum_i, COMTYP_STRING, ORDNUM_LEN, BOOLEAN_TRUE,
                "ordlin", ordlin_i, COMTYP_STRING, ORDLIN_LEN, BOOLEAN_TRUE,
                "ordsln", ordsln_i, COMTYP_STRING, ORDSLN_LEN, BOOLEAN_TRUE,
                "wh_id",  wh_id, COMTYP_STRING, WH_ID_LEN, BOOLEAN_TRUE,
                "prtnum", prtnum_i, COMTYP_STRING, PRTNUM_LEN, BOOLEAN_TRUE,
                "prt_client_id", prt_client_id, COMTYP_STRING, CLIENT_ID_LEN,
                          BOOLEAN_TRUE,
                "orgcod", orgcod_i, COMTYP_STRING, ORGCOD_LEN, BOOLEAN_TRUE,
                "revlvl", revlvl_i, COMTYP_STRING, REVLVL_LEN, BOOLEAN_TRUE,
                "lotnum", lotnum_i, COMTYP_STRING, LOTNUM_LEN, BOOLEAN_TRUE,
                "cooinc", cooinc_i, COMTYP_STRING, COOINC_LEN, BOOLEAN_TRUE,
                "coolst", coolst_i, COMTYP_STRING, COOLST_LEN, BOOLEAN_TRUE,
                "host_ordqty", host_ordqty_i , COMTYP_LONG, sizeof (long),
                        BOOLEAN_FALSE,
                "ovramt", ovramt_i, COMTYP_LONG, sizeof (long), BOOLEAN_FALSE,
                "ovrcod", ovrcod_i, COMTYP_STRING, OVRCOD_LEN, BOOLEAN_TRUE,
                "marcod", marcod_i, COMTYP_STRING, MARCOD_LEN, BOOLEAN_TRUE,
                "ovaflg", &ovaflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "ovpflg", &ovpflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "rpqflg", &rpqflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "prcpri", prcpri, COMTYP_STRING, PRCPRI_LEN, BOOLEAN_TRUE,
                "frtcod", frtcod_i, COMTYP_STRING, FRTCOD_LEN, BOOLEAN_TRUE,
                "cargrp", cargrp_i, COMTYP_STRING, CARGRP_LEN, BOOLEAN_TRUE,
                "carcod", carcod_i, COMTYP_STRING, CARCOD_LEN, BOOLEAN_TRUE,
                "srvlvl", srvlvl_i, COMTYP_STRING, SRVLVL_LEN, BOOLEAN_TRUE,
                "sddflg", &sddflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "entdte", entdte, COMTYP_DATTIM, DB_STD_DATE_LEN,
                          entdte_quotes,
                "sales_ordnum", sales_ordnum_i, COMTYP_STRING, SALES_ORDNUM_LEN,
                          BOOLEAN_TRUE,
                "sales_ordlin", sales_ordlin_i, COMTYP_STRING, SALES_ORDLIN_LEN, 
                          BOOLEAN_TRUE,
                "accnum", accnum_i, COMTYP_STRING, ACCNUM_LEN, BOOLEAN_TRUE,
                "prjnum", prjnum_i, COMTYP_STRING, PRJNUM_LEN, BOOLEAN_TRUE,
                "early_shpdte", early_shpdte, COMTYP_DATTIM, DB_STD_DATE_LEN,
                          BOOLEAN_TRUE,
                "late_shpdte", late_shpdte, COMTYP_DATTIM, DB_STD_DATE_LEN,
                          BOOLEAN_TRUE,
                "early_dlvdte", early_dlvdte, COMTYP_DATTIM, DB_STD_DATE_LEN,
                          BOOLEAN_TRUE,
                "late_dlvdte", late_dlvdte, COMTYP_DATTIM, DB_STD_DATE_LEN,
                          BOOLEAN_TRUE,
                "tms_ord_stat", tms_ord_stat_i, COMTYP_STRING, TMS_ORD_STAT_LEN, 
                          BOOLEAN_TRUE,
                "tms_snt_flg", &tms_snt_flg, COMTYP_INT, sizeof (long), 
                          BOOLEAN_FALSE,
                "tms_dte_snt", &tms_dte_snt, COMTYP_DATTIM, DB_STD_DATE_LEN,
                          BOOLEAN_TRUE,
                "unt_price", unt_price_i, COMTYP_FLOAT, sizeof (double),
                          BOOLEAN_FALSE,
                "crncy_name", crncy_name_i, COMTYP_STRING, CRNCY_NAME_LEN, 
                          BOOLEAN_TRUE,
                "wave_set", wave_set_i, COMTYP_STRING, WAVE_SET_LEN, 
                          BOOLEAN_TRUE,
                "cstprt", cstprt, COMTYP_STRING, CSTPRT_LEN, BOOLEAN_TRUE,
                "manfid", manfid, COMTYP_STRING, MANFID_LEN, BOOLEAN_TRUE,
                "deptno", deptno, COMTYP_STRING, DEPTNO_LEN, BOOLEAN_TRUE,
                "parflg", &parflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "untcas", p_untcas, COMTYP_LONG, sizeof (long), BOOLEAN_FALSE,
                "untpak", p_untpak, COMTYP_LONG, sizeof (long), BOOLEAN_FALSE,
                "untpal", p_untpal, COMTYP_LONG, sizeof (long), BOOLEAN_FALSE,
                "bckflg", &bckflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "splflg", &splflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "stdflg", &stdflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "ordinv", ordinv, COMTYP_STRING, ORDINV_LEN, BOOLEAN_TRUE,
                "edtflg", &edtflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "xdkflg", &xdkflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "atoflg", &atoflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "dstare", dstare_i, COMTYP_STRING, ARECOD_LEN, BOOLEAN_TRUE,
                "dstloc", dstloc_i, COMTYP_STRING, STOLOC_LEN, BOOLEAN_TRUE,
                "non_alc_flg", &non_alc_flg, COMTYP_INT, sizeof (long),
                          BOOLEAN_FALSE,
                "rsvpri", rsvpri, COMTYP_STRING, RSVPRI_LEN, BOOLEAN_TRUE,
                "ordqty", &ordqty, COMTYP_LONG, sizeof (long), BOOLEAN_FALSE,
                "rsvqty", rsvqty_tmp, COMTYP_STRING, TMPLEN, BOOLEAN_TRUE,
                "pckqty", pckqty_tmp, COMTYP_STRING, TMPLEN, BOOLEAN_TRUE,
                "shpqty", shpqty_tmp, COMTYP_STRING, TMPLEN, BOOLEAN_TRUE,
                "pckgr1", pckgr1_i, COMTYP_STRING, PCKGR1_LEN, BOOLEAN_TRUE,
                "pckgr2", pckgr2_i, COMTYP_STRING, PCKGR2_LEN, BOOLEAN_TRUE,
                "pckgr3", pckgr3_i, COMTYP_STRING, PCKGR3_LEN, BOOLEAN_TRUE,
                "pckgr4", pckgr4_i, COMTYP_STRING, PCKGR4_LEN, BOOLEAN_TRUE,
                "mod_usr_id", osGetVar(LESENV_USR_ID)?osGetVar(LESENV_USR_ID):"",
                              COMTYP_STRING,USR_ID_LEN,BOOLEAN_TRUE,
                "moddte", "sysdate", COMTYP_DATTIM, DB_STD_DATE_LEN, 
                          BOOLEAN_FALSE,
                "frsflg", &frsflg, COMTYP_INT, sizeof (long), BOOLEAN_FALSE,
                "invsts_prg", invsts_prg, COMTYP_STRING, INVSTS_PRG_LEN, 
                          BOOLEAN_TRUE,
                "min_shelf_hrs", &min_shelf_hrs, COMTYP_LONG, sizeof (long),
                        BOOLEAN_FALSE,
                "tot_pln_wgt", (p_tot_pln_wgt == NULL ? tot_pln_wgt_i : p_tot_pln_wgt), COMTYP_FLOAT, sizeof (double), BOOLEAN_FALSE,
                "tot_pln_cube", (p_tot_pln_cube == NULL ? tot_pln_cube_i : p_tot_pln_cube), COMTYP_FLOAT, sizeof (double), BOOLEAN_FALSE,
                "tot_pln_misc_qty", tot_pln_misc_qty_i, COMTYP_FLOAT, sizeof (double), BOOLEAN_FALSE,
                "tot_pln_pal_qty", (p_tot_pln_pal_qty == NULL ? tot_pln_pal_qty_i : p_tot_pln_pal_qty), COMTYP_FLOAT, sizeof (double), BOOLEAN_FALSE,
                "tot_pln_cas_qty", (p_tot_pln_cas_qty == NULL ? tot_pln_cas_qty_i : p_tot_pln_cas_qty), COMTYP_FLOAT, sizeof (double), BOOLEAN_FALSE,
                "tot_pln_misc2_qty", tot_pln_misc2_qty_i, COMTYP_FLOAT, sizeof (double), BOOLEAN_FALSE,
                "pln_stccod", pln_stccod_i, COMTYP_STRING, PLN_STCCOD_LEN, BOOLEAN_TRUE,
                "avail_pln_dte", avail_pln_dte, COMTYP_DATTIM, DB_STD_DATE_LEN, BOOLEAN_TRUE,
                "gl_num", gl_num_i, COMTYP_STRING, GL_NUM_LEN, BOOLEAN_TRUE,
                "pln_ltlcls", pln_ltlcls, COMTYP_STRING, PLN_LTLCLS_LEN, BOOLEAN_TRUE,
                "pln_comcod", pln_comcod_i, COMTYP_STRING, PLN_COMCOD_LEN, BOOLEAN_TRUE,
                "appt_dte", appt_dte, COMTYP_DATTIM, DB_STD_DATE_LEN, BOOLEAN_TRUE,
                "tms_pln_sts", tms_pln_sts, COMTYP_STRING, TMS_PLN_STS_COD_LEN, BOOLEAN_TRUE,
                "crncy_code", crncy_code, COMTYP_STRING, ISO_CRNCY_SYM_LEN, BOOLEAN_TRUE,
                "pool_cod", pool_cod_i, COMTYP_STRING, POOL_COD_LEN, BOOLEAN_TRUE,
                "distro_id", distro_id_i, COMTYP_STRING, DISTRO_ID_LEN, BOOLEAN_TRUE,
                "ctn_flg", &ctn_flg, COMTYP_INT, sizeof(long), BOOLEAN_FALSE,
                "tm_shpqty", tm_shpqty_i, COMTYP_INT,
                             sizeof(long), BOOLEAN_FALSE,
                "tm_plnqty", tm_plnqty_i, COMTYP_INT,
                             sizeof(long), BOOLEAN_FALSE,
                "sf_adr_id", sf_adr_id, COMTYP_STRING, ADR_ID_LEN, BOOLEAN_TRUE,
                "st_adr_id", st_adr_id, COMTYP_STRING, ADR_ID_LEN, BOOLEAN_TRUE,
                "rt_adr_id", rt_adr_id, COMTYP_STRING, ADR_ID_LEN, BOOLEAN_TRUE,
                "reffld_1", reffld_1, COMTYP_STRING, REF_FLD_LEN, BOOLEAN_TRUE,
                "reffld_2", reffld_2, COMTYP_STRING, REF_FLD_LEN, BOOLEAN_TRUE,
                "reffld_3", reffld_3, COMTYP_STRING, REF_FLD_LEN, BOOLEAN_TRUE,
                "reffld_4", reffld_4, COMTYP_STRING, REF_FLD_LEN, BOOLEAN_TRUE,
                "reffld_5", reffld_5, COMTYP_STRING, REF_FLD_LEN, BOOLEAN_TRUE,
                "alc_search_path", alc_search_path,
                    COMTYP_STRING, ALC_SEARCH_PATH_LEN, BOOLEAN_TRUE,
                "rel_val_unt_typ", (p_rel_val_unt_typ == NULL ? NULL : rel_val_unt_typ), COMTYP_STRING, REL_VAL_UNT_TYP_LEN, BOOLEAN_TRUE,
                "unt_ins_val", (p_unt_ins_val == NULL ? NULL : unt_ins_val_i), COMTYP_FLOAT, sizeof (double),BOOLEAN_FALSE,
                "rel_val", (p_rel_val == NULL ? NULL : rel_val_i), COMTYP_FLOAT, sizeof (double),BOOLEAN_FALSE,
                "supnum", supnum, COMTYP_STRING, SUPNUM_LEN, BOOLEAN_TRUE,
                "bto_comcod", bto_comcod_i, COMTYP_STRING, BTO_COMCOD_LEN, BOOLEAN_TRUE,
                "src_sys", src_sys_i,
                    COMTYP_STRING, SRC_SYS_LEN, BOOLEAN_TRUE,
                "sched_id", sched_id_i,
                    COMTYP_STRING, COLNAM_LEN, BOOLEAN_TRUE,
                "recur_sched_typ", recur_sched_typ_i,
                    COMTYP_STRING, COLNAM_LEN, BOOLEAN_TRUE,
                "sched_wnd", sched_wnd_i,
                    COMTYP_INT, sizeof (long), BOOLEAN_TRUE,

                NULL);

    free(valuelist);

    return (CurPtr);
}
