static const char *rcsid = "$Id: intDeallocateLocation.c 113482 2006-08-08 12:59:42Z mzais $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <common.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <trnlib.h>

#include "intlib.h"

/*
 **     intDeallocLocation will deallocate a location based on load
 **             or a work reference.
 */

/*
 * HISTORY
 * JJS 08/17/2009 - Don't do "for update" on locmst for certain areas
 *                  (specifically CONV - the conveyor and PALBLD) since
 *                  this is causing unnecessary database locking.  We
 *                  don't care about managing QVL in areas that are
 *                  essentially of infinite size.  Note that a similar
 *                  change was made in pick release in "get pick release
 *                  locations for area".
 *                  NOTE: This version is only needed so that trnDeallocateLocation()
 *                  can be customized (since the architecture doesn't allow trnlib
 *                  functions to easily be customized).  It doesn't actually do
 *                  anything different than intDeallocateLocation.
 *
 */

LIBEXPORT 
RETURN_STRUCT *varDeallocateLocation(char *lodnum_i, 
				     char *subnum_i,
				     char *dtlnum_i,
				     char *wrkref_i)
{
    long ret_status, sql_status;
    RETURN_STRUCT *CurPtr;
    mocaDataRow *row;
    mocaDataRes *res;
    char buffer[1000];
    char wrkref[WRKREF_LEN + 1];
    char lodnum[LODNUM_LEN + 1];
    char subnum[SUBNUM_LEN + 1];
    char dtlnum[DTLNUM_LEN + 1];

    if ((!lodnum_i || misTrimLen(lodnum_i, LODNUM_LEN) == 0) &&
	(!subnum_i || misTrimLen(subnum_i, SUBNUM_LEN) == 0) &&
	(!dtlnum_i || misTrimLen(dtlnum_i, DTLNUM_LEN) == 0) &&
	(!wrkref_i || misTrimLen(wrkref_i, WRKREF_LEN) == 0))
    {
	return(APPError(eINT_REQ_PARAM_MISSING));
    }

    /* Get the source location */
    memset(lodnum, '\0', sizeof(lodnum));
    memset(subnum, '\0', sizeof(subnum));
    memset(dtlnum, '\0', sizeof(dtlnum));
    memset(wrkref, '\0', sizeof(wrkref));

    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
    {
	strncpy(dtlnum, dtlnum_i, misTrimLen(dtlnum_i, DTLNUM_LEN));
	sprintf(buffer, 
		"select im.wh_id, im.stoloc "
		"  from invmov im "
		" where im.lodnum = '%s' "
		"   and im.lodlvl = '%s' ",
		dtlnum, LODLVL_DETAIL);
    }
    else if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
    {
	strncpy(subnum, subnum_i, misTrimLen(subnum_i, SUBNUM_LEN));
	sprintf(buffer,
		"select im.wh_id, im.stoloc "
		"  from invmov im "
		" where im.lodnum = '%s' "
		"   and im.lodlvl = '%s' ",
		subnum, LODLVL_SUBLOAD);
    }
    else if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
    {
	strncpy(lodnum, lodnum_i, misTrimLen(lodnum_i, LODNUM_LEN));
	sprintf(buffer,
		"select im.wh_id, im.stoloc "
		"  from invmov im "
		" where im.lodnum = '%s' "
		"   and im.lodlvl = '%s' ",
		lodnum, LODLVL_LOAD);
    }
    else
    {
	strncpy(wrkref, wrkref_i, misTrimLen(wrkref_i, LODNUM_LEN));
	sprintf(buffer,
		"select pm.wh_id, pm.arecod, pm.stoloc, pw.dstloc "
		"  from pckwrk pw, pckmov pm "
		" where pw.cmbcod in (select cmbcod "
		"                       from pckwrk "
		"                      where wrkref = '%s') "
		"   and pm.cmbcod = pw.cmbcod ",
		wrkref);
    }

    sql_status = sqlExecStr(buffer, &res);
    if (sql_status != eOK && !strlen(dtlnum) &&
	(strlen(subnum) || strlen(lodnum)))
    {
	/* Ok...we don't have an exact match on the invmov...
	 * go ahead and look at the children so that we can
	 * report appropriately 
	 */
	sqlFreeResults(res);
	if (strlen(subnum))
	{
	    sprintf(buffer,
		    "select piv.wh_id wh_id, piv.pndloc stoloc "
		    "  from pndinv_view piv "
		    " where piv.subnum = '%s' "
		    "   and piv.lodlvl = '%s' "
		    " order by piv.seqnum ",
		    subnum, LODLVL_DETAIL);
	}
	else 
	{
	    sprintf(buffer,
		    "select piv.wh_id wh_id, piv.pndloc stoloc "
		    "  from pndinv_view piv "
		    " where piv.lodnum = '%s' "
		    " order by piv.seqnum",
		    lodnum);
	}
	sql_status = sqlExecStr(buffer, &res);
    }

    misTrc(T_FLOW, 
	   "Calling trnDeallocateLocation...lodnum: %s..."
	   "subnum: %s...dtlnum: %s...wrkref: %s",
	   lodnum, subnum, dtlnum, wrkref);
    ret_status = trnDeallocateLocation(lodnum, subnum, dtlnum, wrkref);
    misTrc(T_FLOW, "trnDeallocateLocation returns: %d", ret_status);

    if (ret_status == eOK && sql_status == eOK)
    {
	/* return structure */
	CurPtr = srvResultsInit(eOK,
                                "wh_id", COMTYP_CHAR, WH_ID_LEN,
                                "srcloc", COMTYP_CHAR, STOLOC_LEN,
                                NULL);

	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
            misTrc(T_FLOW, "Lodnum: %s", lodnum);
            misTrc(T_FLOW, "wh_id/stoloc: %s/%s",
                   sqlGetString(res, row, "wh_id"),
                   sqlGetString(res, row, "stoloc"));

	    if ((misTrimLen(lodnum, LODNUM_LEN)) &&
		!sqlIsNull(res, row, "stoloc") &&
		misTrimLen(sqlGetString(res, row, "stoloc"), STOLOC_LEN))
	    {
                srvResultsAdd(CurPtr,
                              sqlGetString(res, row, "wh_id"),
                              sqlGetString(res, row, "stoloc"));
  	    }
	    else
	    {
		if (!sqlIsNull(res, row, "stoloc") &&
		    misTrimLen(sqlGetValue(res, row, "stoloc"), STOLOC_LEN))
		{
                    srvResultsAdd(CurPtr,
                                  sqlGetString(res, row, "wh_id"),
                                  sqlGetString(res, row, "stoloc"));
		}
		
		if (!sqlIsNull(res, row, "dstloc") &&
		    misTrimLen(sqlGetString(res, row, "dstloc"), STOLOC_LEN))
		{
                    misTrc(T_FLOW, 
                           "wh_id/dstloc: %s/%s",
                           sqlGetString(res, row, "wh_id"),
                           sqlGetString(res, row, "dstloc"));

                    srvResultsAdd(CurPtr,
                                  sqlGetString(res, row, "wh_id"),
                                  sqlGetString(res, row, "dstloc"));
		}
	    }
	}
    }
    else
	CurPtr = srvResults(ret_status, NULL);

    sqlFreeResults(res);
    if (!CurPtr)
	CurPtr = srvResults(ret_status, NULL);

    return (CurPtr);
}
