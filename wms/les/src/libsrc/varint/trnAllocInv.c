static char    *rcsid = "$Id: trnAllocInv.c 167341 2008-08-26 04:10:19Z kzhao $";
/*#START***********************************************************************
 *  Copyright (c) 2004 RedPrairie Corporation. All rights reserved.
 *#END************************************************************************/
#include <moca_app.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <applib.h>
#include "trnlib.h"

/*
 * HISTORY
 * JJS 02/04/2009 - Stop detail picks from occurring
 * JJS 02/16/2009 - Add in logic for FEFO replens.
 * JJS 06/08/2009 - Incorporate Product fix for Jira WMD-49699/WMD-44340
 *                  (which is the real fix for "Stop detail picks from
 *                  occurring")
 * JJS 06/08/2009 - Incorporate Product fix for Jira WMD-44408.  Replens
 *                  are being created in Pending status when they should
 *                  be Held (to follow the "Load Levels Marked for
 *                  Immediate Release").
 * JJS 06/10/2009 - Changes to set pckwrk.vc_from_rpl flag.
 * JJS 06/16/2009 - Only allow layer picks in areas designated by VAR
 *                  policy.  Set subflg = 0 for any other area when we
 *                  encounter a layer-picked part.
 * JJS 06/24/2009 - Take out 06/08 change.  They are seeing rplwrk.pcksts
 *                  stuck at H, so take this out.
 * JJS 08/20/2009 - Incorporating Product fix to keep "allocate location"
 *                  from getting called over and over again with the same
 *                  parameters (this was causing replens to take a VERY long
 *                  time to process and contributing to database locking).
 *
 */

/*
 * Quick overview:
 *
 * Allocate inventory is used to reserve inventory at a location
 * level for later picking.  It will write pckwrk, pckmov entries
 * and will also increment the invloc.comqty value after allocating
 * inventory from a location.
 *
 * The basic flow is the following:
 *     1.  Process command line parameters.  We may receive a sequence
 *         of orders to allocate a given prtnum/prt_client_id/qty.
 *           i.e. "ord1, ord2, ..."
 *         The command line parameters are parsed separately if we are
 *         allocating to a load or performing a general allocation.
 *     2.  The command line arguments are placed in a PCKWRK_DATA structure
 *         (stored globally as the PickRequestList)
 *     3.  Processing continues as we determine order destinations (in the
 *         case of allocating for orders/shipments)
 *     4.  We verify that the quantity requested will not cause an illegal
 *         overallocation.
 *     5.  Next, we process the inventory lookup.  This may be done in a
 *         couple of different ways.  First...if we were passed in a 
 *         specific inventory identifier, we try to allocate that (i.e.
 *         lodnum/subnum/stoloc)...otherwise, we try to allocate from
 *         the facility using our PICK policies (which specify an ordered
 *         list of area codes).
 *
 *         With inventory found, we do the following: 
 *         a.  Based on area attributes, we attempt to allocate
 *             a load/case/piece (in that order) from the location
 *         b.  As we find picks, we move the quantity from the
 *             PickRequestList to the PickFoundList.
 *         c.  Once we have processed through all areas possible,
 *             we will typically generate a move path and build
 *             our list of PCKMOVs for each PickFoundList item
 *             (which at this point is really just an in memory
 *             copy of what will be written to the pckwrk)
 *         d.  Based on how we were called, we then write out 
 *             pckmovs and pckwrks and will also write out
 *             rplwrk entries if needed.
 */
 
/***************************************************************
  Here's the deal with the COL_STRUCT and RET_OFFSET macros...
  The COL_STRUCT macro is used to make it easy to define a
  structure (COL_NAMES_ADDRESS) which is used to associate
  column names with structure addresses.  The "addresses" are
  all offsets into the generic pick structure.  The column
  referenced must be a member of the PCKWRK_DATA structure.
  The RET_OFFSET macro simply does pointer arithmetic using the
  static generic structure to determine an offset which may be
  applied to any instance of the same structure to yield an
  appropriate offset...
  ***************************************************************/
#define NOT_SET(s) (!s || misTrimLen(s, 1) == 0)
#define IS_SET(s) (s && misTrimLen(s, 1) > 0)
#define ISNULL(c) ((strlen(c) == 0) ? -1 : 0)
#define BINDSTR(n,l,v) n, COMTYP_CHAR, l, v, ISNULL(v)

#define COL_STRUCT(tmpPck,col,string)  {string,(unsigned long)(tmpPck.col)}
#define RET_OFFSET(offset)  (unsigned long)(offset - (unsigned long)&tmpPckwrk)

static moca_bool_t vc_from_rpl = BOOLEAN_FALSE;

typedef struct _OriginalOrder
{
    char ship_id[SHIP_ID_LEN + 1];
    char ship_line_id[SHIP_LINE_ID_LEN + 1];
    
    char wh_id[WH_ID_LEN+1];
    
    char client_id[CLIENT_ID_LEN + 1];
    char ordnum[ORDNUM_LEN + 1];
    char ordlin[ORDLIN_LEN + 1];
    char ordsln[ORDSLN_LEN + 1];

    char wkonum[WKONUM_LEN+1];
    char wkorev[WKOREV_LEN+1];
    char wkolin[WKOLIN_LEN+1];

    long ordqty;    /* original ordered quantity */
    long pckqty;    /* quantity that was found to pick */

    struct _OriginalOrder *next;

} ORIG_ORDER;
static ORIG_ORDER *OriginalOrderList = NULL;

typedef struct _pckpol_data
{
    char            wh_id[WH_ID_LEN + 1];
    char            arecod[ARECOD_LEN + 1];
    char            pckcod[PCKCOD_LEN + 1];
    moca_bool_t     lodflg;
    moca_bool_t     subflg;
    moca_bool_t     dtlflg;
    moca_bool_t     area_lodflg;
    moca_bool_t     area_subflg;
    moca_bool_t     area_dtlflg;
} PICK_POLICY_DATA;

typedef struct _area_info
{
    moca_bool_t area_lodflg;
    moca_bool_t area_subflg;
    moca_bool_t area_dtlflg;
    char area_loccod[LOCCOD_LEN+1];
} AREA_INFO;

typedef struct _arealist
{
    char    wh_id[WH_ID_LEN + 1];
    char    arecod[ARECOD_LEN + 1];
    char    srcare[ARECOD_LEN + 1];
    char    dstare[ARECOD_LEN + 1];
    char    lodlvl[LODLVL_LEN + 1];
    struct _arealist *nextarea;
}
AREA_LIST;


typedef struct _col_names_address
{
    char           *colName;
    unsigned long   offset;
}
COL_NAMES_ADDRESS;

/* This next section of declares will create an array of structures
   that contain the column name of the corresponding field in the PCKWRK_DATA
   structure and its absolutes offset from the base of a temporary
   structure place holder. If another field is added to the PCKWRK typedef
   it should also be added to this following structure with the provided
   macro */

/* Make sure #define RET_OFFSET has same declare */
static PCKWRK_DATA tmpPckwrk;

static COL_NAMES_ADDRESS _Column_Offsets[] =
{
    COL_STRUCT(tmpPckwrk, carcod, "carcod"),
    COL_STRUCT(tmpPckwrk, srvlvl, "srvlvl"),
    COL_STRUCT(tmpPckwrk, schbat, "schbat"),
    COL_STRUCT(tmpPckwrk, ship_id, "ship_id"),
    COL_STRUCT(tmpPckwrk, wkonum, "wkonum"),
    COL_STRUCT(tmpPckwrk, wkolin, "wkolin"),
    COL_STRUCT(tmpPckwrk, wkorev, "wkorev"),
    COL_STRUCT(tmpPckwrk, client_id, "client_id"),
    COL_STRUCT(tmpPckwrk, ordnum, "ordnum"),
    COL_STRUCT(tmpPckwrk, stcust, "stcust"),
    COL_STRUCT(tmpPckwrk, rtcust, "rtcust"),
    COL_STRUCT(tmpPckwrk, rt_adr_id, "rt_adr_id"),
    COL_STRUCT(tmpPckwrk, ordlin, "ordlin"),
    COL_STRUCT(tmpPckwrk, ordsln, "ordsln"),
    COL_STRUCT(tmpPckwrk, cmbcod, "cmbcod"),
    COL_STRUCT(tmpPckwrk, concod, "concod"),
    COL_STRUCT(tmpPckwrk, wrkref, "wrkref"),
    COL_STRUCT(tmpPckwrk, lodlvl, "lodlvl"),
    COL_STRUCT(tmpPckwrk, prt_lodlvl, "prt_lodlvl"),
    COL_STRUCT(tmpPckwrk, srcare, "srcare"),
    COL_STRUCT(tmpPckwrk, wh_id, "wh_id"),
    {NULL, 0}
};

typedef struct ByInvPolicyStruct
{
    int             matchLine;
    int             matchSubline;
    int             createLine;
    int             createSubline;
    char           *createSublineStr;
    int             loaded;
    char            wh_id[WH_ID_LEN + 1];
} BYINVPOLICY;

typedef struct inventory_attr {
    char orgcod[ORGCOD_LEN+1];
    char lotnum[LOTNUM_LEN+1];
    char revlvl[REVLVL_LEN+1];
    char supnum[SUPNUM_LEN+1];
    char invsts[INVSTS_LEN+1];
    char distro_id[DISTRO_ID_LEN+1];
    moca_bool_t alcflg;
    long untpak;
    long untcas;
    long untqty;
    double catch_qty;
    long comqty;
    double com_catch_qty;
    short potentialAllocation;
    struct inventory_attr *next;

} INVENTORY_ATTR;

typedef struct inventory_struct {
    char lodnum[LODNUM_LEN + 1];
    short mixedPart;

    short mixedOrgcod;
    short mixedRevlvl;
    short mixedLotnum;
    short mixedSupnum;
    short mixedInvsts;
    short mixedAlcflg;
    short mixedUntcas;
    short mixedUntpak;
    short mixedAttribute;  /* If any of the above are set...this is set */

    short palletAllocated;
    short casesAllocated;
    short piecesAllocated;
    short catchAllocated;
    short allocationsExistAtLocation;
    long untpal;
    double catch_pal;
    char asset_typ[ASSET_TYP_LEN + 1];
    INVENTORY_ATTR *attr;
    
    struct inventory_struct *next;
    struct inventory_struct *last;
} INVENTORY;

typedef struct ship_dst_struct{
    PCKWRK_DATA * parent;
    
    char wh_id[WH_ID_LEN + 1];
    char dst_bldg_id[BLDG_ID_LEN + 1];
    char dstare[ARECOD_LEN + 1];
    char dstloc[STOLOC_LEN + 1];

    char src_bldg_id[BLDG_ID_LEN + 1];
    long srtseq;
    
    struct ship_dst_struct *next;
}SHIP_DST;

typedef struct _skipped_location_info
{
    struct _pckpol_data policy;
    struct _area_info area_info;

    char stoloc[STOLOC_LEN+1];
    char devcod[DEVCOD_LEN+1];
    char wh_id[WH_ID_LEN+1];
    char invsts[INVSTS_LEN+1];
    char pcklvl[LODLVL_LEN+1];
    char pcktyp[100];
    char cmbcod[CMBCOD_LEN];
    char juldte[DB_STD_DATE_LEN+1];
    char invinloc_order_by[RTSTR1_LEN+1];
    long alloc_loc_flg;
    moca_bool_t splflg;

    struct _skipped_location_info *next;
    struct _skipped_location_info *first;
} SKIPPED_LOCATION_INFO;

static AREA_LIST *PathHead;
static PCKWRK_DATA *PickRequestList;
static PCKWRK_DATA *PickFoundList;
static PCKWRK_DATA ** PickRequestArray;
static long PickRequestArrayLength;
static PICK_MOVS *PckMovPathHead = NULL;

static RES_ASSOCIATE *resAssociate = NULL;
static SCHBAT_RES *schbatRes = NULL;
static moca_bool_t asset_track_enabled = BOOLEAN_NOTSET;

static FILE   *fptr;
static long   Trace;

static char save_alloc_pick_group_schbat[SCHBAT_LEN+1];

static void trnFreeResAssociate(RES_ASSOCIATE *Top);

static long sInventoryMatchesPick(PCKWRK_DATA *pick,
                  INVENTORY_ATTR *attr,
                                  char *invsts);

static SKIPPED_LOCATION_INFO* sAddLocationToSkipList(PICK_POLICY_DATA* PolPtr,
        char* stoloc,
        char* devcod,
        char* invsts,
        char* pcklvl,
        char* pcktyp,
        char* cmbcod,
        moca_bool_t splflg,
        char* juldte,
        AREA_INFO* area_info,
        long alloc_loc_flg,
        char* wh_id,
        char* invinloc_order_by,
        SKIPPED_LOCATION_INFO* skip_list);

static long sProcessSkippedLocations(SKIPPED_LOCATION_INFO* skipped_locations,
                                     moca_bool_t *isAlloLocFailed);

static void sFreeSkippedLocationList(SKIPPED_LOCATION_INFO* skipped_location);

static char* sBuildCurrentlyAllocatedLocations();

/*New requirement - sCalculateReplenishmentPickStatusForPIP*/
static void sCalculateReplenishmentPickStatusForPIP(PCKWRK_DATA * ptr, char *retstr, long in_totpnd_rplqvl)
{
    RETURN_STRUCT *locPtr = NULL;
    mocaDataRes *locRes = NULL;
    mocaDataRow *locRow = NULL;
    char buffer[2000];
    long ret_status;
    long totpnd_rplqvl;
    totpnd_rplqvl = in_totpnd_rplqvl;
    sprintf (buffer,
             "calculate new replenishment status "
             "    where dstloc = '%s' "
             "      and dstare = '%s' "
             "      and prtnum = '%s' "
             "      and wh_id = '%s' "
             "      and prt_client_id = '%s' "
             "      and ftpcod = '%s' "
             "      and qty = '%ld' "
             "      and srcloc = '%s' "
             "      and untcas = '%ld' "
             "      and asset_typ = '%s' "
             "      and totpnd_rplqvl = %ld",
             ptr->dstloc, 
             ptr->dstare,
             ptr->prtnum,
             ptr->wh_id,
             ptr->prt_client_id,
             ptr->ftpcod,
             ptr->pckqty,
             ptr->srcloc,
             ptr->untcas,
             ptr->asset_typ,
             totpnd_rplqvl);

    ret_status = srvInitiateCommand(buffer, &locPtr);

    if (ret_status == eOK)
    {
        locRes = srvGetResults (locPtr);
        locRow = sqlGetRow(locRes);
        misTrimcpy(retstr, 
                   sqlGetString(locRes, locRow, "pcksts"),
                   PCKSTS_LEN);
        /* Update the Total Pending Replenishment qvl "totpnd_rplqvl"
         * with the value returned by the moca command
         */
        totpnd_rplqvl = sqlGetLong(locRes, locRow, "totpnd_rplqvl");
        misTrc(T_FLOW, "Total Pending Replenishment QVL updated to"
                       "%ld ", totpnd_rplqvl);

                
    }
    if (locPtr) 
        srvFreeMemory (SRVRET_STRUCT, locPtr);
    locRes = NULL;
} 

static void sLog(long action, char *fmt, ...)
{
    va_list Arguments;
    
    if (!action)
    {

    va_start(Arguments, fmt);
    misVTrc(T_FLOW, fmt, Arguments);
        if (Trace)
        {
        /* Write and flush the file... */
        vfprintf(fptr, fmt, Arguments);
        fprintf(fptr, "\n");
        fflush(fptr);
        }
        va_end(Arguments);
    }
    return;
}

/* This funcation used to:
 * 1.If savflg =1, than save the parameters that passed in to record the
 *   failure of 'allocate location'.
 * 2.If savflg = 0, than we check if we can skip 'allocate location'.
 * This will improve the performance for 'allocate inventory' especially when
 * there are many loads but each of them failed because of allocate location.
 */
static long sSaveOrCheckSkipAllocateLocation(char *fifdte,
                                     char *dstare,
                                     char *srcare,
                                     char *prtnum,
                                     char *prt_client_id,
                                     char *lotnum,
                                     char *revlvl,
                                     char *supnum,
                                     char *orgcod,
                                     char *invsts,
                                     long untcas,
                                     long untpak,
                                     long pckqty,
                                     char *dstloc,
                                     char *ftpcod,
                                     char *asset_typ,
                                     char *wh_id,
                                     moca_bool_t *isAlloLocFailed,
                                     moca_bool_t Savflg)
{
        static char fifdte_cp[DB_STD_DATE_LEN+1];
        static char dstare_cp[ARECOD_LEN+1];
        static char srcare_cp[ARECOD_LEN+1];
        static char prtnum_cp[PRTNUM_LEN+1];
        static char prt_client_id_cp[CLIENT_ID_LEN+1];
        static char lotnum_cp[LOTNUM_LEN+1];
        static char revlvl_cp[REVLVL_LEN+1];
        static char supnum_cp[SUPNUM_LEN+1];
        static char orgcod_cp[ORGCOD_LEN+1];
        static char invsts_cp[INVSTS_LEN+1];
        static long untcas_cp;
        static long untpak_cp;
        static long pckqty_cp;
        static char dstloc_cp[STOLOC_LEN+1];
        static char ftpcod_cp[FTPCOD_LEN+1];
        static char asset_typ_cp[ASSET_TYP_LEN+1];
        static char wh_id_cp[WH_ID_LEN+1];

        moca_bool_t skipflg = BOOLEAN_FALSE;

        /*
         * We are saving the allocate location parameters.
         */
        if (Savflg)
        {
                misTrimcpy(fifdte_cp, fifdte, DB_STD_DATE_LEN);
                misTrimcpy(dstare_cp, dstare, ARECOD_LEN);
                misTrimcpy(srcare_cp, srcare, ARECOD_LEN);
                misTrimcpy(prtnum_cp, prtnum, PRTNUM_LEN);
                misTrimcpy(prt_client_id_cp,
                                   prt_client_id, CLIENT_ID_LEN);
                misTrimcpy(lotnum_cp, lotnum, LOTNUM_LEN);
                misTrimcpy(revlvl_cp, revlvl, REVLVL_LEN);
                misTrimcpy(supnum_cp, supnum, SUPNUM_LEN);
                misTrimcpy(orgcod_cp, orgcod, ORGCOD_LEN);
                misTrimcpy(invsts_cp, invsts, INVSTS_LEN);
                misTrimcpy(dstloc_cp, dstloc, STOLOC_LEN);
                misTrimcpy(ftpcod_cp, ftpcod, FTPCOD_LEN);
                misTrimcpy(asset_typ_cp, asset_typ, ASSET_TYP_LEN);
                misTrimcpy(wh_id_cp, wh_id, WH_ID_LEN);
                untcas_cp = untcas;
                untpak_cp = untpak;
                pckqty_cp = pckqty;
                misTrc(T_FLOW, "Alloc loc failed, infor saved!");

                return BOOLEAN_FALSE;
        }
        else
        {

      skipflg= (misCiStrcmp(fifdte,fifdte_cp) == 0 &&
                misCiStrcmp(dstare,dstare_cp) == 0 &&
                misCiStrcmp(srcare,srcare_cp) == 0 &&
                misCiStrcmp(prtnum,prtnum_cp) == 0 &&
                misCiStrcmp(prt_client_id,prt_client_id_cp) == 0 &&
                misCiStrcmp(lotnum,lotnum_cp) == 0 &&
                misCiStrcmp(revlvl,revlvl_cp) == 0 &&
                misCiStrcmp(supnum,supnum_cp) == 0 &&
                misCiStrcmp(orgcod,orgcod_cp) == 0 &&
                misCiStrcmp(invsts,invsts_cp) == 0 &&
                misCiStrcmp(dstloc,dstloc_cp) == 0 &&
                misCiStrcmp(ftpcod,ftpcod_cp) == 0 &&
                misCiStrcmp(asset_typ,asset_typ_cp) == 0 &&
                misCiStrcmp(wh_id,wh_id_cp) == 0 &&
                untcas == untcas_cp &&
                untpak == untpak_cp &&
                pckqty == pckqty_cp &&
                *isAlloLocFailed);

              misTrc(T_FLOW, "fifdte ='%s',fifdte_cp ='%s'", fifdte, fifdte_cp);
              misTrc(T_FLOW, "dstare ='%s',dstare_cp ='%s'", dstare, dstare_cp);
              misTrc(T_FLOW, "srcare ='%s',srcare_cp ='%s'", srcare, srcare_cp);
              misTrc(T_FLOW, "prtnum ='%s',prtnum_cp ='%s'", prtnum, prtnum_cp);
              misTrc(T_FLOW, "prt_client_id ='%s',prt_client_id_cp ='%s'",
                                                prt_client_id,prt_client_id_cp);
              misTrc(T_FLOW, "lotnum ='%s',lotnum_cp ='%s'", lotnum, lotnum_cp);
              misTrc(T_FLOW, "revlvl ='%s',revlvl_cp ='%s'", revlvl, revlvl_cp);
              misTrc(T_FLOW, "supnum ='%s',supnum_cp ='%s'", supnum, supnum_cp);
              misTrc(T_FLOW, "orgcod ='%s',orgcod_cp ='%s'", orgcod, orgcod_cp);
              misTrc(T_FLOW, "invsts ='%s',invsts_cp ='%s'", invsts, invsts_cp);
              misTrc(T_FLOW, "dstloc ='%s',dstloc_cp ='%s'", dstloc, dstloc_cp);
              misTrc(T_FLOW, "ftpcod ='%s',ftpcod_cp ='%s'", ftpcod, ftpcod_cp);
              misTrc(T_FLOW, "asset_typ ='%s',asset_typ_cp = '%s'",
                              asset_typ,asset_typ_cp);
              misTrc(T_FLOW, "wh_id ='%s',wh_id_cp ='%s'", wh_id, wh_id_cp);
              misTrc(T_FLOW, "untcas =%ld,untcas_cp =%ld", untcas, untcas_cp);
              misTrc(T_FLOW, "untpak =%ld,untpak_cp =%ld", untpak, untpak_cp);
              misTrc(T_FLOW, "pckqty =%ld,pckqty_cp =%ld", pckqty, pckqty_cp);

              misTrc(T_FLOW, "skipflg = %ld", skipflg);

             return skipflg;
        }

}

static long sAllocateLocation(char *fifdte,
                              char *dstare,
                              char *srcare,
                              char *prtnum,
                              char *prt_client_id,
                              char *lotnum,
                              char *revlvl,
                              char *supnum,
                              char *orgcod,
                              char *invsts,
                              long untcas,
                              long untpak,
                              long pckqty,
                              char *dstloc,
                              char *ftpcod,
                              char *asset_typ,
                              char *wh_id)
{

    RETURN_STRUCT *locPtr = NULL;
    mocaDataRes *locRes = NULL;
    mocaDataRow *locRow = NULL;
    char buffer[2000];
    char cur_bldg_id[BLDG_ID_LEN + 1];
    char asset_typ_string[200];
    long ret_status;
    
    memset (cur_bldg_id, 0, sizeof(cur_bldg_id));
    memset (buffer, 0, sizeof(buffer));
    memset (asset_typ_string, 0, sizeof(asset_typ_string));

    sLog (0, "Attempting to allocate a location in area %s/%s",
          wh_id, dstare);

    sprintf (buffer,
             "select bldg_id "
             "  from aremst "
             " where arecod = '%s' "
             "   and wh_id = '%s' ",
             srcare,
             wh_id);

    ret_status = sqlExecStr(buffer, &locRes);

    if (ret_status == eOK)
    {
        locRow = sqlGetRow(locRes);
        misTrimcpy(cur_bldg_id, 
                   sqlGetString(locRes, locRow, "bldg_id"),
                   BLDG_ID_LEN);
    }

    sqlFreeResults(locRes);
    locRes = NULL;

    if (asset_typ && misTrimLen(asset_typ, ASSET_TYP_LEN))
    {
        sLog(0, "Asset should be taken into consideration, asset_typ = %s ",
             asset_typ);

        sprintf(asset_typ_string, " and asset_typ = '%s' ", asset_typ);
    }

    sprintf (buffer,
             "[select to_date('%s', 'YYYYMMDDHH24MISS') fifo_date "
             "   from dual] catch(@?) "
             " | "
             "allocate location "
             "   where type = \"%s\" "
             "     and arecod = \"%s\" "
             "     and prtnum = \"%s\" "
             "     and prt_client_id = \"%s\" "
             "     and lotnum = \"%s\" "
             "     and revlvl = \"%s\" "
             "     and supnum = \"%s\" "
             "     and orgcod = \"%s\" "
             "     and invsts = \"%s\" "
             "     and untcas = \"%d\" "
             "     and untpak = \"%d\" "
             "     and untqty = \"%d\" "
             "     and fifdte = @fifo_date "
             "     and invmov_typ = \"%s\" "
             "     and cur_bldg_id = \"%s\" "
             "     and ftpcod = \"%s\" "
             "     %s "
             "     and wh_id =  \"%s\" ",
             fifdte,
             ALLOCATE_REPLENISH,
             dstare,
             prtnum,
             prt_client_id,
             lotnum,
             revlvl,
             supnum,
             orgcod,
             invsts,
             untcas,
             untpak,
             pckqty,
             INVMOV_TYP_PICK,
             cur_bldg_id,
             ftpcod,
             asset_typ_string,
             wh_id);
    
    ret_status = srvInitiateCommand (buffer, &locPtr);
    
    if (eOK != ret_status)
    {
        sLog (0, "Could not allocate location in area %s/%s",
              wh_id, dstare);
    }
    else
    {
    
        /*
         * Allocate Location worked.  get the last row returned.
         */
    
        locRes = srvGetResults (locPtr);
        for (locRow = sqlGetRow (locRes);
             locRow && sqlGetNextRow (locRow);
             locRow = sqlGetNextRow (locRow));
    
        misTrimcpy (dstloc, sqlGetString (locRes, locRow, "nxtloc"),
                    STOLOC_LEN);
    
    }

    if (locPtr) srvFreeMemory (SRVRET_STRUCT, locPtr);
    locPtr = NULL;
    locRes = NULL;
    locRow = NULL;

    return (ret_status);

}

char *trnGetSchbat()
{
    return(save_alloc_pick_group_schbat);
}

void trnSetSchbat(char *schbat)
{
    strcpy(save_alloc_pick_group_schbat, schbat);
}

void trnClearSchbat()
{
    memset(save_alloc_pick_group_schbat, 
           0, 
           sizeof(save_alloc_pick_group_schbat));
}

static long OpenTraceFile(char *wh_id, char *suffix_i)
{
    static mocaDataRes *res = NULL;
    mocaDataRow *row;
    
    long ret_status;
    
    moca_bool_t policyFound = MOCA_FALSE;
    long logFileEnabled = 0L;
    
    char trcfil[100];
    char varbuf[1024 + 1];
    
    memset(trcfil, 0, sizeof(trcfil));
    memset(varbuf, 0, sizeof(varbuf));
    
    if (res == NULL)
    {
        /* load and cache policies of all warehouses */

        sprintf(varbuf,
                "select wh_id, rtnum1 "
                "  from poldat_view "
                " where polcod = '%s' "
                "   and polvar = '%s' "
                "   and polval = '%s' ",
                POLCOD_ALLOCATE_INV,
                POLVAR_MISCELLANEOUS,
                POLVAL_LOG_FILE_ENABLED);

        ret_status = sqlExecStr(varbuf, &res);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            /* Remember to set res = NULL, it's a static variable */
            sqlFreeResults(res);
            res = NULL;
            return (ret_status);
        }

        /* Cach the recordset */
        misFlagCachedMemory((OSFPTR) sqlFreeResults, res);
    }
    
    /* 
     * Cycle the policies to find the one for the specified warehouse. 
     * If the policy is not defined, let everything continue,
     * just don't do a log file.
     */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        if (!misTrimStrncmp(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN))
        {
            logFileEnabled = sqlGetLong(res, row, "rtnum1");
            policyFound = MOCA_TRUE;
        }
    }
    
    if (policyFound == MOCA_FALSE)
        misTrc(T_FLOW, "Policy not defined for LOG-FILE-ENABLED "
                       "for warehouse %s ", wh_id);

    misTrc(T_FLOW, "Log file enabled status for warehouse %s is %ld",
           wh_id, logFileEnabled);

    /* Try to open trace for warehouse */
    Trace = 0;

    if (logFileEnabled == BOOLEAN_TRUE)
        Trace = 1;

    if (!Trace)
        return eOK;

    misTrc(T_FLOW, "creating trace for warehouse %s", wh_id);
    sprintf(trcfil, "%s%c%s_AllocInv.Sts%s",
            misExpandVars(varbuf, LES_LOG, sizeof(varbuf), NULL),
            PATH_SEPARATOR,
            wh_id,
            suffix_i ? suffix_i : "");
    misTrc(T_FLOW, "Creating log file at %s", trcfil);
    fptr = fopen(trcfil, "a");
    if (fptr)
    {
    misTrc(T_FLOW, "Log file successfully opened");
        fprintf(fptr, "Beginning allocate inventory \n");
        fflush(fptr);
    }
    else
    {
        Trace = 0;
    }

    return eOK;
}

static void CloseTraceFile()
{
    misTrc(T_FLOW, "close trace file");
    if (!Trace)
        return;

    if (fptr)
    {
        fprintf(fptr, "Leaving allocate inventory\n");
        fflush(fptr);
        fclose(fptr);
        fptr = NULL;
    }

    Trace = 0;
    return;
}

static void sFreeOriginalOrderList()
{
    ORIG_ORDER     *tmp, *last;

    last = NULL;
    for (tmp = OriginalOrderList; tmp; tmp = tmp->next)
    {
    if (last)
        free(last);
    last = tmp;
    }
    if (last)
    free(last);

    OriginalOrderList = NULL;
    return;

}

static BYINVPOLICY *sGetByInvPolicies(char *wh_id)
{

    char            sqlbuffer[500];
    long            ret_status;
    mocaDataRow    *row;
    mocaDataRes    *res;
    static BYINVPOLICY ByInvPolicies;
    
    if (ByInvPolicies.loaded && strcmp(wh_id, ByInvPolicies.wh_id) == 0)
    return (&ByInvPolicies);

    memset(&ByInvPolicies, 0, sizeof(ByInvPolicies));

    misTrimcpy(ByInvPolicies.wh_id, wh_id, WH_ID_LEN);
    ByInvPolicies.createSublineStr = " ";

    sprintf(sqlbuffer,
        "select polvar,polval,rtstr1,rtstr2 "
        "  from poldat_view "
        " where polcod = '%s' "
        "   and polvar in ('%s','%s') "
        "   and wh_id = '%s' "
        " order by polval,srtseq",
        POLCOD_ALLOCATE_INV,
        POLVAR_MATCH_ORDER_TO_INV, POLVAR_CREATE_ORDER_TO_INV,
        wh_id);
    ret_status = sqlExecStr(sqlbuffer, &res);
    if (ret_status != eOK)
    {
    sqlFreeResults(res);
    }
    else
    {
    ByInvPolicies.loaded = 1;

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        if (misCiStrcmp(misTrim(sqlGetString(res,
                         row, "polvar")),
                POLVAR_MATCH_ORDER_TO_INV) == 0)
        {
        if (misCiStrcmp(misTrim(sqlGetString(res,
                             row, "polval")),
                POLVAL_ORDLIN) == 0)
        {
            if (misCiStrncmp(misTrim(sqlGetString(res,
                              row, "rtstr1")),
                     "Y", 1))
            {
            ByInvPolicies.matchLine = 0;
            }
            else
            {
            ByInvPolicies.matchLine = 1;
            }
        }
        else if (misCiStrcmp(misTrim(sqlGetString(res,
                              row, "polval")),
                     POLVAL_ORDSLN) == 0)
        {
            if (misCiStrncmp(misTrim(sqlGetString(res,
                              row, "rtstr1")),
                     "Y", 1))
            {
            ByInvPolicies.matchSubline = 0;
            }
            else
            {
            ByInvPolicies.matchSubline = 1;
            }
        }
        }
        if (misCiStrcmp(misTrim(sqlGetString(res,
                         row, "polvar")),
                POLVAR_CREATE_ORDER_TO_INV) == 0)
        {
        if (misCiStrcmp(misTrim(sqlGetString(res,
                             row, "polval")),
                POLVAL_ORDLIN) == 0)
        {
            if (misCiStrncmp(misTrim(sqlGetString(res,
                              row, "rtstr1")),
                     "Y", 1))
            {
            ByInvPolicies.createLine = 0;
            }
            else
            {
            ByInvPolicies.createLine = 1;
            }
        }
        else if (misCiStrcmp(misTrim(sqlGetString(res,
                              row, "polval")),
                     POLVAL_ORDSLN) == 0)
        {
            if (misCiStrncmp(misTrim(sqlGetString(res,
                              row, "rtstr1")),
                     "Y", 1))
            {
            ByInvPolicies.createSubline = 0;
            }
            else
            {
            ByInvPolicies.createSubline = 1;
            ByInvPolicies.createSublineStr =
                sqlGetString(res, row, "rtstr2");
            }
        }
        }
    }
    }
    return (&ByInvPolicies);
}

static void sFillPickWork(PCKWRK_DATA * ptr, 
              mocaDataRes * res, 
              mocaDataRow * row)
{
    if (!sqlIsNull(res, row, "wh_id"))
    strncpy(ptr->wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
    if (!sqlIsNull(res, row, "client_id"))
    strncpy(ptr->client_id, 
        sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);
    if (!sqlIsNull(res, row, "ordnum"))
    strncpy(ptr->ordnum, sqlGetString(res, row, "ordnum"), ORDNUM_LEN);
    if (!sqlIsNull(res, row, "ordlin"))
    strncpy(ptr->ordlin, sqlGetString(res, row, "ordlin"), ORDLIN_LEN);
    if (!sqlIsNull(res, row, "ordsln"))
    strncpy(ptr->ordsln, sqlGetString(res, row, "ordsln"), ORDSLN_LEN);
    if (!sqlIsNull(res, row, "ordtyp"))
    strncpy(ptr->ordtyp, sqlGetString(res, row, "ordtyp"), ORDTYP_LEN);
    if (!sqlIsNull(res, row, "marcod"))
    strncpy(ptr->marcod, sqlGetString(res, row, "marcod"), MARCOD_LEN);
    if (!sqlIsNull(res, row, "ship_id"))
    strncpy(ptr->ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
    if (!sqlIsNull(res, row, "ship_line_id"))
    strncpy(ptr->ship_line_id, 
        sqlGetString(res, row, "ship_line_id"), SHIP_LINE_ID_LEN);
    if (!sqlIsNull(res, row, "wkonum"))
    strncpy(ptr->wkonum, sqlGetString(res, row, "wkonum"), WKONUM_LEN);
    if (!sqlIsNull(res, row, "wkorev"))
    strncpy(ptr->wkonum, sqlGetString(res, row, "wkorev"), WKOREV_LEN);
    if (!sqlIsNull(res, row, "wkolin"))
    strncpy(ptr->wkonum, sqlGetString(res, row, "wkolin"), WKOLIN_LEN);

    if (!sqlIsNull(res, row, "stcust"))
    strncpy(ptr->stcust, sqlGetString(res, row, "stcust"), ADRNUM_LEN);
    if (!sqlIsNull(res, row, "rtcust"))
    strncpy(ptr->rtcust, sqlGetString(res, row, "rtcust"), ADRNUM_LEN);
    if (!sqlIsNull(res, row, "carcod"))
    strncpy(ptr->carcod, sqlGetString(res, row, "carcod"), CARCOD_LEN);
    if (!sqlIsNull(res, row, "srvlvl"))
    strncpy(ptr->srvlvl, sqlGetString(res, row, "srvlvl"), SRVLVL_LEN);
    if (!sqlIsNull(res, row, "ordinv"))
    strncpy(ptr->ordinv, sqlGetString(res, row, "ordinv"), ORDINV_LEN);
    if (!sqlIsNull(res, row, "alc_search_path"))
        strncpy(ptr->alc_search_path,
                sqlGetString(res, row, "alc_search_path"), ALC_SEARCH_PATH_LEN);
    if (!sqlIsNull(res, row, "distro_id"))
        strncpy(ptr->distro_id, sqlGetString(res, row, "distro_id"), DISTRO_ID_LEN);
    if (!sqlIsNull(res, row, "bto_seqnum"))
        strncpy(ptr->bto_seqnum,sqlGetString(res, row, "bto_seqnum"),BTO_SEQNUM_LEN);
    if (!sqlIsNull(res, row, "slot"))
        strncpy(ptr->slot, sqlGetString(res, row, "slot"),SLOT_LEN);

}

static void  sSplitPickWork(PCKWRK_DATA * ptr, long qty)
{
    PCKWRK_DATA    *p1;

    p1 = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));

    strncpy(p1->wh_id, ptr->wh_id, WH_ID_LEN);
    strncpy(p1->prtnum, ptr->prtnum, PRTNUM_LEN);
    strncpy(p1->prt_client_id, ptr->prt_client_id, CLIENT_ID_LEN);
    if (IS_SET(ptr->lotnum))
    strncpy(p1->lotnum, ptr->lotnum, LOTNUM_LEN);
    if (IS_SET(ptr->orgcod))
    strncpy(p1->orgcod, ptr->orgcod, ORGCOD_LEN);
    if (IS_SET(ptr->revlvl))
    strncpy(p1->revlvl, ptr->revlvl, REVLVL_LEN);
    if (IS_SET(ptr->supnum))
        strncpy(p1->supnum, ptr->supnum, SUPNUM_LEN);

    strncpy(p1->invsts, ptr->invsts, INVSTS_LEN);
    strncpy(p1->srcare, ptr->srcare, ARECOD_LEN);
    strncpy(p1->srcloc, ptr->srcloc, STOLOC_LEN);
    strncpy(p1->dstare, ptr->dstare, ARECOD_LEN);
    strncpy(p1->dstloc, ptr->dstloc, STOLOC_LEN);
    strncpy(p1->lodnum, ptr->lodnum, LODNUM_LEN);
    strncpy(p1->lodlvl, ptr->lodlvl, LODLVL_LEN);
    strncpy(p1->asset_typ, ptr->asset_typ, ASSET_TYP_LEN);
    strncpy(p1->concod, ptr->concod, CONCOD_LEN);
    strncpy(p1->alc_search_path, ptr->alc_search_path, ALC_SEARCH_PATH_LEN);

    p1->untcas = ptr->untcas;
    p1->untpak = ptr->untpak;
    if (ptr->next)
    ptr->next->prev = p1;

    p1->next = ptr->next;
    ptr->next = p1;
    p1->prev = ptr;
    p1->pckqty = qty;
    ptr->pckqty = ptr->pckqty - qty;
}

/* 
 * Get count(or number) of parameters.
 * Each parameter can be a list of individuals delimited by CONCAT_CHAR,
 * for example :
 *       carcod = "ABFS|FEDXF|SCNN"
 *       srvlvl = "2DAY|LTL|TL"
 *         .
 *         .
 *       ordnum = "ORD01|ORD02|ORD03"
 *         .
 *         .
 *       wh_id  = "WMD1|WMD1|WMD1"
 * in this example, the parameters indicates 3 records (noParams = 3),
 * the return value will be 3.
 * 
 * If any parameter involves different count of individuals, 
 * return value will be -1. for example:
 *       carcod = "ABFS|FEDXF|SCNN"
 *       srvlvl = "2DAY|LTL"
 */
static long sGetNoParams(char *carcod, char *srvlvl,
             char *schbat, char *ship_id,
             char *ship_line_id,
             char *client_id, char *ordnum,
             char *stcust, char *rtcust, char *ordlin,
             char *ordsln, char *concod, char *wh_id,
             char *alc_search_path)
{

    char           *cptr;
    long            tmpqty;
    long            noParams;

    noParams = 0;

    if (misTrimLen(wh_id, WH_ID_LEN))
    {
    for (tmpqty = 0, cptr = wh_id; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(carcod, CARCOD_LEN))
    {
    for (tmpqty = 0, cptr = carcod; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(srvlvl, SRVLVL_LEN))
    {
    for (tmpqty = 0, cptr = srvlvl; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(schbat, SCHBAT_LEN))
    {
    for (tmpqty = 0, cptr = schbat; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(ship_id, SHIP_ID_LEN))
    {
    for (tmpqty = 0, cptr = ship_id; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(ship_line_id, SHIP_LINE_ID_LEN))
    {
    for (tmpqty = 0, cptr = ship_line_id; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(client_id, CLIENT_ID_LEN))
    {
    for (tmpqty = 0, cptr = client_id; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(ordnum, ORDNUM_LEN))
    {
    for (tmpqty = 0, cptr = ordnum; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(stcust, ADRNUM_LEN))
    {
    for (tmpqty = 0, cptr = stcust; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(rtcust, ADRNUM_LEN))
    {
    for (tmpqty = 0, cptr = rtcust; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(ordlin, ORDLIN_LEN))
    {
    for (tmpqty = 0, cptr = ordlin; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(ordsln, ORDSLN_LEN))
    {
    for (tmpqty = 0, cptr = ordsln; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (misTrimLen(concod, CONCOD_LEN))
    {
    for (tmpqty = 0, cptr = concod; 
         cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
    if (noParams != 0 && tmpqty != noParams)
        return (-1);
    else
        noParams = tmpqty;
    }
    if (alc_search_path && misTrimLen(alc_search_path, ALC_SEARCH_PATH_LEN))
    {
        for (tmpqty = 0, cptr = alc_search_path; 
             cptr; cptr = strstr(cptr + 1, CONCAT_CHAR), tmpqty++) ;
        if (noParams != 0 && tmpqty != noParams)
            return (-1);
        else
            noParams = tmpqty;
    }
    return (noParams);
}

static long sMatchOrderToInventory(char *ship_id,
                   char *ship_line_id,
                   char *client_id,
                   char *ordnum,
                   char *ordlin,
                   char *ordsln,
                   char *wh_id,
                   PCKWRK_DATA * pick)
{
    char            preBuffer[8000];
    char            sqlBuffer[10000];
    char            invBuffer[4000];
    char            buildBuffer[500];
    mocaDataRes     *res;
    mocaDataRow    *row;
    long            orderQty;
    long            ret_status;
    BYINVPOLICY    *byInvPtr;

    byInvPtr = sGetByInvPolicies(wh_id);

    sprintf(preBuffer,
        "select sl.wh_id, sl.pckqty, sl.client_id, "
        "       sl.ordnum, sl.ordlin, sl.ordsln,"
        "       sl.ship_id, sl.ship_line_id, "
        "       o.stcust, o.rtcust, o.bto_seqnum, o.slot, "
        "       sh.carcod, sh.srvlvl,"
        "       ol.prtnum, "
        "       ol.distro_id, "
        "       ol.lotnum, ol.orgcod,"
        "       ol.revlvl, ol.supnum, ol.ordinv, "
        "       ol.alc_search_path "
        "  from shipment sh, shipment_line sl, ord o, ord_line ol "
        " where sh.ship_id    = sl.ship_id "
        "   and sl.pckqty     > 0 "
        "   and sl.ship_id    = '%s' "
        "   and sl.client_id  = '%s' "
        "   and sl.ordnum     = '%s' "
        "   and sl.wh_id      = '%s' "
        "   and sl.client_id  = o.client_id "
        "   and sl.ordnum     = o.ordnum "
        "   and sl.wh_id      = o.wh_id "
        "   and sl.client_id  = ol.client_id "
        "   and sl.ordnum     = ol.ordnum "
        "   and sl.ordlin     = ol.ordlin "
        "   and sl.ordsln     = ol.ordsln "
        "   and sl.wh_id      = ol.wh_id ",
        ship_id, client_id, ordnum, wh_id);

    sprintf(invBuffer,
        " and ol.prtnum = '%.*s' "
        " and ol.prt_client_id = '%.*s' "
        " and ol.supnum = '%.*s' "
        " and ol.invsts_prg in "
        "     (select invsts_prg "
            "        from prgmst     "
            "       where invsts  = '%.*s' "
            "         and alcflg = '%d') ",
        (int) strlen(pick->prtnum), pick->prtnum,
        (int) strlen(pick->prt_client_id), pick->prt_client_id,
        (int) strlen(pick->supnum), pick->supnum,
        (int) strlen(pick->invsts), pick->invsts,
            BOOLEAN_TRUE);
    if (strcmp(pick->lotnum, "----"))
    {
    sprintf(buildBuffer, " and ol.lotnum = '%.*s' ",
        (int) strlen(pick->lotnum), pick->lotnum);
    strcat(invBuffer, buildBuffer);
    }
    if (strcmp(pick->orgcod, "----"))
    {
    sprintf(buildBuffer, " and ol.orgcod= '%.*s' ",
        (int) strlen(pick->orgcod), pick->orgcod);
    strcat(invBuffer, buildBuffer);
    }
    if (strcmp(pick->revlvl, "----"))
    {
    sprintf(buildBuffer, " and ol.revlvl= '%.*s' ",
        (int) strlen(pick->revlvl), pick->revlvl);
    strcat(invBuffer, buildBuffer);
    }

    if (byInvPtr->matchSubline)
    {
    sprintf(sqlBuffer,
        "%s "
        " and sl.ordlin = '%s' "
        " and sl.ordsln != '%s' "
        " %s ",
        preBuffer,
        ordlin,
        ordsln,
        invBuffer);
    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
        return (ret_status);
    }
    else if (ret_status == eOK)
    {
        /* Apply this order to the pick */
        row = sqlGetRow(res);
        orderQty = sqlGetLong(res, row, "pckqty");
        sFillPickWork(pick, res, row);
        if (pick->pckqty > orderQty)
        {
        sSplitPickWork(pick, pick->pckqty - orderQty);
        }
        sqlFreeResults(res);
        return (eOK);
    }
    sqlFreeResults(res);
    }
    if (byInvPtr->matchLine)
    {
    sprintf(sqlBuffer,
        "%s "
        " and sl.ordlin != '%s' "
        " %s ",
        preBuffer,
        ordlin,
        invBuffer);
    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
        return (ret_status);
    }
    else if (ret_status == eOK)
    {
        /* Apply this order to the pick */
        row = sqlGetRow(res);
        orderQty = sqlGetLong(res, row, "pckqty");
        sFillPickWork(pick, res, row);
        if (pick->pckqty > orderQty)
        {
        sSplitPickWork(pick, pick->pckqty - orderQty);
        }
        sqlFreeResults(res);
        return (eOK);
    }
    sqlFreeResults(res);
    }
    return (eDB_NO_ROWS_AFFECTED);
}

static long sCreateOrderToInventory(char *ship_id,
                    char *ship_line_id,
                    char *client_id,
                    char *ordnum,
                    char *ordlin,
                    char *ordsln,
                    char *wh_id,
                    PCKWRK_DATA * pick)
{

    char            preBuffer[5000];
    char            sqlBuffer[10000];
    char            buffer[5000];
    char            invBuffer[2000];
    mocaDataRes     *res, *res2, *tres;
    mocaDataRow     *row, *trow;
    long            ret_status;
    char            newordsln[ORDSLN_LEN + 1];
    char            newordlin[ORDLIN_LEN + 1];
    char            newship_line_id[SHIP_LINE_ID_LEN + 1];
    char        colbuf1[1000], colbuf2[1000];
    char        valbuf1[1000], valbuf2[1000];
    char        tblbuf1[10], tblbuf2[10];
    char            invsts_prg[INVSTS_PRG_LEN+1];

    char *adr_id_p, *dstare_p, *dstloc_p, *rsvpri_p;
    char *pckgr1_p, *pckgr2_p, *pckgr3_p, *pckgr4_p;

    long parflg, ovaflg, ovpflg, rpqflg;
    long splflg, stdflg, bckflg, sddflg,ovramt;
    long untcas, untpak, untpal;
    long cons_batch;
    char *cooinc_p, *coolst_p, *ovrcod_p, *marcod_p, *prcpri_p;

    char *frtcod_p, *cargrp_p, *carcod_p, *srvlvl_p, *entdte_p;
    char *sales_ordnum_p, *sales_ordlin_p, *cstprt_p, *manfid_p;
    char *deptno_p, *accnum_p, *prjnum_p, *early_shpdte_p;
    char *late_shpdte_p, *early_dlvdte_p, *late_dlvdte_p;

    BYINVPOLICY *byInvPtr;

    memset(invsts_prg, 0, sizeof(invsts_prg));
    byInvPtr = sGetByInvPolicies(wh_id);

    sprintf(preBuffer,
        "select o.stcust, o.rtcust, sl.*,"
        "       ol.* "
        "  from shipment_line sl, ord o, ord_line ol "
        " where sl.client_id    = o.client_id "
        "   and sl.ordnum       = o.ordnum " 
        "   and sl.wh_id        = o.wh_id " 
        "   and sl.client_id    = ol.client_id "
        "   and sl.ordnum       = ol.ordnum " 
        "   and sl.ordlin       = ol.ordlin "
        "   and sl.ordsln       = ol.ordsln " 
        "   and sl.wh_id        = ol.wh_id " 
        "   and sl.ship_id      = '%s' "
        "   and sl.client_id    = '%s' " 
        "   and sl.ordnum       = '%s' "
        "   and sl.wh_id        = '%s' ",
        ship_id, client_id, ordnum, wh_id);

    memset(invBuffer, 0, sizeof(invBuffer));
    sprintf(buffer, " and ol.prtnum %c= '%.*s' ",
      misCiStrstr(byInvPtr->createSublineStr, "prtnum") ? '!' : ' ',
        (int) strlen(pick->prtnum), pick->prtnum);
    strcat(invBuffer, buffer);

    if (strcmp(pick->lotnum, "----"))
    {
    sprintf(buffer,
        " and ol.lotnum %c= '%.*s' ",
        misCiStrstr(byInvPtr->createSublineStr,
                "lotnum") ? '!' : ' ',
        (int) strlen(pick->lotnum), pick->lotnum);
    strcat(invBuffer, buffer);
    }

    if (strcmp(pick->orgcod, "----"))
    {
    sprintf(buffer, " and ol.orgcod %c= '%.*s' ",
        misCiStrstr(byInvPtr->createSublineStr,
                "orgcod") ? '!' : ' ',
        (int) strlen(pick->orgcod), pick->orgcod);
    strcat(invBuffer, buffer);
    }
    if (strcmp(pick->revlvl, "----"))
    {
    sprintf(buffer, " and ol.revlvl %c= '%.*s' ",
        misCiStrstr(byInvPtr->createSublineStr,
                "revlvl") ? '!' : ' ',
        (int) strlen(pick->revlvl), pick->revlvl);
    strcat(invBuffer, buffer);
    }


    if (byInvPtr->createSubline)
    { 
    sprintf(sqlBuffer,
        "%s "
        " and sl.ordlin = '%s' "
        " %s ",
        preBuffer, ordlin, invBuffer);
    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
        return (ret_status);
    }
    if (ret_status == eOK)
    {
        /* So, we've got our order line */
        /* First, get max sublin number */
        sprintf(sqlBuffer,
            "select max(ordsln) ordsln "
            "  from shipment_line "
            " where ship_id   = '%s' "
            "   and client_id = '%s' "
            "   and ordnum    = '%s' "
            "   and ordlin    = '%s' "
            "   and wh_id     = '%s' ",
            ship_id, client_id, ordnum, ordlin, wh_id);

        ret_status = sqlExecStr(sqlBuffer, &res2);
        if (ret_status != eOK)
        {
        sqlFreeResults(res);
        sqlFreeResults(res2);
        return (ret_status);
        }
        row = sqlGetRow(res2);
        sprintf(sqlBuffer, "%%0%dld", ORDSLN_LEN);
        sprintf(newordsln, sqlBuffer,
            atol(sqlGetString(res2, row, "ordsln")) + 1);
            appNextNum(NUMCOD_SHIP_LINE_ID, newship_line_id);
        sqlFreeResults(res2);

        row = sqlGetRow(res);

        sprintf(tblbuf1,"ord_line");
        sprintf(tblbuf2,"shipment_line");

        sprintf(colbuf1,"wh_id,client_id,ordnum,ordlin,ordsln,"
               "prtnum,prt_client_id,orgcod,revlvl,supnum,lotnum,"
               "ordqty,rsvqty,pckqty");

        sprintf(colbuf2,"ship_id,ship_line_id,wh_id,client_id,"
                   "ordnum,ordlin,ordsln,"
               "pckqty,inpqty,stgqty,shpqty,"
               "oviqty");

        sprintf(valbuf1,"'%s','%s','%s','%s','%s',"
                   "'%s','%s','%s','%s','%s','%s',"
                   "'%ld','%ld','%ld'",
            wh_id, client_id, ordnum, ordlin, newordsln,
            pick->prtnum, pick->prt_client_id,
            pick->orgcod, pick->revlvl, pick->supnum,
            pick->lotnum,
            (long) 0),    /* ordqty */
                pick->pckqty,  /* rsvqty */
            pick->pckqty;  /* pckqty */
            

        sprintf(valbuf2,"'%s','%s','%s','%s',"
                   "'%s','%s','%s',"
                   "'%ld','%ld','%ld','%ld','%ld'",
            ship_id,newship_line_id,wh_id,client_id,
            ordnum, ordlin, newordsln,
            pick->pckqty,    /* pckqty */
            (long) 0,    /* inpqty */
            (long) 0,    /* stgqty */
            (long) 0,    /* shpqty */
            (long) 0);    /* oviqty */

        ret_status = appGenerateTableEntry(tblbuf1, res, row,
                           colbuf1, valbuf1);
        if (ret_status != eOK)
        {
        sqlFreeResults(res);
        return (ret_status);
        }
        else
        {
            ret_status = appGenerateTableEntry(tblbuf2, res, row,
                           colbuf2, valbuf2);
            if (ret_status != eOK)
            {
            sqlFreeResults(res);
            return (ret_status);
            }
            }
        sqlFreeResults(res);
        return (ret_status);
    }
    sqlFreeResults(res);
    }

    if (byInvPtr->createLine)
    {
    /* First, get max ordlin number */
    sprintf(sqlBuffer, "%%0%dld", ORDSLN_LEN);
    sprintf(newordsln, sqlBuffer, (long) 0);

    sprintf(sqlBuffer,
        "select max(ordlin) ordlin "
        "  from shipment_line "
        " where ship_id   = '%s' "
        "   and client_id = '%s' "
        "   and ordnum    = '%s' "
        "   and wh_id     = '%s' ",
        ship_id, client_id, ordnum, wh_id);

    ret_status = sqlExecStr(sqlBuffer, &res2);
    if (ret_status != eOK)
    {
        sqlFreeResults(res2);
        return (ret_status);
    }
    if (ret_status == eOK)
    {
        row = sqlGetRow(res2);
        sprintf(sqlBuffer, "%%0%dld", ORDLIN_LEN);
        sprintf(newordlin, sqlBuffer,
            atol(sqlGetString(res2, row, "ordlin")) + 1);
            appNextNum(NUMCOD_SHIP_LINE_ID, newship_line_id);
        sprintf(sqlBuffer,
            "%s "
            " order by ordlin DESC ", preBuffer);
        ret_status = sqlExecStr(preBuffer, &res);
        if (ret_status != eOK)
        {
        sqlFreeResults(res);
        return (ret_status);
        }
    }
    else
    {
        res = NULL;
    }
    sqlFreeResults(res2);

    if (res)
    {
        row = sqlGetRow(res);
    }
    else
    {
        sprintf(sqlBuffer,
            "select sl.*, "
            "       ol.* "
            "from shipment_line sl, ord_line ol "
            " where sl.ship_id    = '%s' "
            "   and sl.client_id  = '%s' "
            "   and sl.ordnum     = '%s' "
            "   and sl.wh_id      = '%s' "
            "   and sl.client_id  = ol.client_id "
            "   and sl.ordnum     = ol.ordnum "
            "   and sl.ordlin     = ol.ordlin "
            "   and sl.ordsln     = ol.ordsln "
            "   and sl.wh_id      = ol.wh_id ",
            ship_id, client_id, ordnum, wh_id); 

        ret_status = sqlExecStr(sqlBuffer, &res);
        if (ret_status != eOK)
        {
        sqlFreeResults(res);
        return (ret_status);
        }
        row = sqlGetRow(res);
    }

    dstare_p = (res ? sqlGetString(res, row, "dstare") : "");
    dstloc_p = (res ? sqlGetString(res, row, "dstloc") : "");
    rsvpri_p = (res ? sqlGetString(res, row, "rsvpri") : "");
    pckgr1_p = (res ? sqlGetString(res, row, "pckgr1") : "");
    pckgr2_p = (res ? sqlGetString(res, row, "pckgr2") : "");
    pckgr3_p = (res ? sqlGetString(res, row, "pckgr3") : "");
    pckgr4_p = (res ? sqlGetString(res, row, "pckgr4") : "");

    ovramt = (res ? sqlGetLong(res, row, "ovramt") : 0);
    untcas = (res ? sqlGetLong(res, row, "untcas") : 0);
    untpak = (res ? sqlGetLong(res, row, "untpak") : 0);
    untpal = (res ? sqlGetLong(res, row, "untpal") : 0);

    cooinc_p = (res ? sqlGetString(res, row, "cooinc") : "");
    coolst_p = (res ? sqlGetString(res, row, "coolst") : "");
    ovrcod_p = (res ? sqlGetString(res, row, "ovrcod") : "");
    marcod_p = (res ? sqlGetString(res, row, "marcod") : "");
    
    parflg = (res ? sqlGetBoolean(res, row, "parflg") : BOOLEAN_FALSE);
    ovaflg = (res ? sqlGetBoolean(res, row, "ovaflg") : BOOLEAN_FALSE);
    ovpflg = (res ? sqlGetBoolean(res, row, "ovpflg") : BOOLEAN_FALSE);
    rpqflg = (res ? sqlGetBoolean(res, row, "rpqflg") : BOOLEAN_FALSE);
    splflg = (res ? sqlGetBoolean(res, row, "splflg") : BOOLEAN_TRUE);
    stdflg = (res ? sqlGetBoolean(res, row, "stdflg") : BOOLEAN_FALSE);
    bckflg = (res ? sqlGetBoolean(res, row, "bckflg") : BOOLEAN_FALSE);
    prcpri_p = (res ? sqlGetString(res, row, "prcpri") : PRCPRI_DEFAULT);

    sddflg = (res ? sqlGetBoolean(res, row, "sddflg") : BOOLEAN_FALSE);
    frtcod_p = (res ? sqlGetString(res, row, "frtcod") : "");
    cargrp_p = (res ? sqlGetString(res, row, "cargrp") : "");
    carcod_p = (res ? sqlGetString(res, row, "carcod") : "");
    srvlvl_p = (res ? sqlGetString(res, row, "srvlvl") : "");
    entdte_p = (res ? sqlGetString(res, row, "entdte") : "");
    sales_ordnum_p = (res ? sqlGetString(res, row, "sales_ordnum") : "");
    sales_ordlin_p = (res ? sqlGetString(res, row, "sales_ordlin") : "");
    cstprt_p = (res ? sqlGetString(res, row, "cstprt") : "");
    manfid_p = (res ? sqlGetString(res, row, "manfid") : "");
    deptno_p = (res ? sqlGetString(res, row, "deptno") : "");
    accnum_p = (res ? sqlGetString(res, row, "accnum") : "");
    prjnum_p = (res ? sqlGetString(res, row, "prjnum") : "");
    early_shpdte_p = (res ? sqlGetString(res, row, "early_shpdte") : "");
    late_shpdte_p = (res ? sqlGetString(res, row, "late_shpdte") : "");
    early_dlvdte_p = (res ? sqlGetString(res, row, "early_dlvdte") : "");
    late_dlvdte_p = (res ? sqlGetString(res, row, "late_dlvdte") : "");
    
    sprintf(buffer,
        "select invsts_prg "
        "  from prgmst "
        " where invsts = '%s'",
        pick->invsts);
    ret_status = sqlExecStr(buffer, &tres);
    if (ret_status != eOK)
    {
        if (res) sqlFreeResults(res);
        sqlFreeResults(tres);
        return(ret_status);
    }
    trow = sqlGetRow(tres);
    misTrimcpy(invsts_prg, 
           sqlGetString(tres, trow, "invsts_prg"), INVSTS_PRG_LEN);
    sqlFreeResults(tres);


    ret_status = sqlExecBind(
        " insert into ord_line "
        "(wh_id, client_id,ordnum,ordlin,"
        " ordsln,prt_client_id,prtnum,"
        " orgcod,revlvl,supnum,lotnum,"
        " invsts_prg,cooinc,coolst,"
        " ovramt,ovrcod,marcod,parflg,"
        " ovaflg,ovpflg,rpqflg,splflg,"
        " stdflg,bckflg,prcpri,frtcod,"
        " cargrp,carcod,srvlvl,sddflg,"
        " entdte,sales_ordnum,sales_ordlin,"
        " cstprt,manfid,deptno,accnum,"
        " prjnum,untcas,untpak,untpal,"
        " early_shpdte,late_shpdte,"
        " early_dlvdte,late_dlvdte, "
        " dstare,dstloc,"
        " rsvpri,ordqty,rsvqty,pckqty,"
        " shpqty,pckgr1,"
        " pckgr2,pckgr3,pckgr4) "
        "values "
        "(:wh_id, :client_id, :ordnum, :ordlin,"
        " :ordsln, :prt_client_id, :prtnum,"
        " :orgcod, :revlvl, :supnum, :lotnum,"
        " :invsts_prg, :cooinc, :coolst, "
        " :ovramt, :ovrcod, :marcod, :parflg,"
        " :ovaflg, :ovpflg, :rpqflg, :splflg,"
        " :stdflg, :bckflg, :prcpri, :frtcod,"
        " :cargrp, :carcod, :srvlvl, :sddflg,"
        " :entdte, :sales_ordnum, :sales_ordlin,"
        " :cstprt, :manfid, :deptno, :accnum,"
        " :prjnum, :untcas, :untpak, :untpal,"
        " :early_shpdte, :late_shpdte,"
        " :early_dlvdte, :late_dlvdte, "
            " :dstare, :dstloc,"
            " :rsvpri, :ordqty, :rsvqty, :pckqty,"
            " :shpqty, :pckgr1, "
            " :pckgr2, :pckgr3, :pckgr4) ",
        NULL, NULL,
        BINDSTR("wh_id", WH_ID_LEN, wh_id),
        BINDSTR("client_id", CLIENT_ID_LEN, client_id),
        BINDSTR("ordnum", ORDNUM_LEN, ordnum),
        BINDSTR("ordlin", ORDLIN_LEN, newordlin),
        BINDSTR("ordsln", ORDSLN_LEN, newordsln),
        BINDSTR("prt_client_id", CLIENT_ID_LEN, pick->prt_client_id), 
        BINDSTR("prtnum", PRTNUM_LEN, pick->prtnum),
        BINDSTR("orgcod", ORGCOD_LEN, pick->orgcod),
        BINDSTR("revlvl", REVLVL_LEN, pick->revlvl),
        BINDSTR("supnum", SUPNUM_LEN, pick->supnum),
        BINDSTR("lotnum", LOTNUM_LEN, pick->lotnum),
        BINDSTR("invsts_prg", INVSTS_PRG_LEN, invsts_prg),
        BINDSTR("cooinc", COOINC_LEN, cooinc_p),
        BINDSTR("coolst", COOLST_LEN, coolst_p),
        "ovramt", COMTYP_INT, sizeof(long), ovramt, 0,
        BINDSTR("ovrcod", OVRCOD_LEN, ovrcod_p),
        BINDSTR("marcod", MARCOD_LEN, marcod_p),
        "parflg", COMTYP_INT, sizeof(long), parflg, 0,
        "ovaflg", COMTYP_INT, sizeof(long), ovaflg, 0,
        "ovpflg", COMTYP_INT, sizeof(long), ovpflg, 0,
        "rplflg", COMTYP_INT, sizeof(long), rpqflg, 0,
        "splflg", COMTYP_INT, sizeof(long), splflg, 0,
        "stdflg", COMTYP_INT, sizeof(long), stdflg, 0,
        "bckflg", COMTYP_INT, sizeof(long), bckflg, 0,
        BINDSTR("prcpri", PRCPRI_LEN, prcpri_p),
        BINDSTR("frtcod", FRTCOD_LEN, frtcod_p),
        BINDSTR("cargrp", CARGRP_LEN, cargrp_p),
        BINDSTR("carcod", CARCOD_LEN, carcod_p),
        BINDSTR("srvlvl", SRVLVL_LEN, srvlvl_p),
        "sddflg", COMTYP_INT, sizeof(long), sddflg, 0,
        BINDSTR("entdte", DB_STD_DATE_LEN, entdte_p),
        BINDSTR("ordnum", ORDNUM_LEN, sales_ordnum_p),
        BINDSTR("ordlin", ORDLIN_LEN, sales_ordlin_p),
        BINDSTR("cstprt", CSTPRT_LEN, cstprt_p),
        BINDSTR("manfid", MANFID_LEN, manfid_p),
        BINDSTR("deptno", DEPTNO_LEN, deptno_p),
        BINDSTR("accnum", ACCNUM_LEN, accnum_p),
        BINDSTR("prjnum", PRJNUM_LEN, prjnum_p),
        "untcas", COMTYP_INT, sizeof(long), untcas, 0,
        "untpak", COMTYP_INT, sizeof(long), untpak, 0,
        "untpal", COMTYP_INT, sizeof(long), untpal, 0,
        BINDSTR("early_shpdte", DB_STD_DATE_LEN, early_shpdte_p),
        BINDSTR("late_shpdte", DB_STD_DATE_LEN, late_shpdte_p),
        BINDSTR("early_dlvdte", DB_STD_DATE_LEN, early_dlvdte_p),
        BINDSTR("late_dlvdte", DB_STD_DATE_LEN, late_dlvdte_p),
        BINDSTR("dstare", ARECOD_LEN, dstare_p),
        BINDSTR("dstloc", STOLOC_LEN, dstloc_p),
        BINDSTR("rsvpri", RSVPRI_LEN, rsvpri_p),
        "ordqty", COMTYP_INT, sizeof(long), 0, 0,
        "rsvqty", COMTYP_INT, sizeof(long), pick->pckqty, 0,
        "pckqty", COMTYP_INT, sizeof(long), pick->pckqty, 0,
        "shpqty", COMTYP_INT, sizeof(long), 0, 0,
        BINDSTR("pckgr1", PCKGR1_LEN, pckgr1_p),
        BINDSTR("pckgr2", PCKGR2_LEN, pckgr2_p),
        BINDSTR("pckgr3", PCKGR3_LEN, pckgr3_p),
        BINDSTR("pckgr4", PCKGR4_LEN, pckgr4_p));

    ret_status = sqlExecStr(sqlBuffer, NULL);
    if (ret_status != eOK)
    {
        if (res) 
        sqlFreeResults(res);
        return (ret_status);
    }

    
    adr_id_p = (res ? sqlGetString(res, row, "adr_id") : "");
    cons_batch = (res ? sqlGetLong(res, row, "cons_batch") : 0);
    prcpri_p = (res ? sqlGetString(res, row, "prcpri") : PRCPRI_DEFAULT);
    pckgr1_p = (res ? sqlGetString(res, row, "pckgr1") : "");
    pckgr2_p = (res ? sqlGetString(res, row, "pckgr2") : "");
    pckgr3_p = (res ? sqlGetString(res, row, "pckgr3") : "");
    pckgr4_p = (res ? sqlGetString(res, row, "pckgr4") : "");
    ovrcod_p = (res ? sqlGetString(res, row, "ovrcod") : "");
    ovramt = (res ? sqlGetLong(res, row, "ovramt") : 0);

    ret_status = sqlExecBind(
        "insert into shipment_line "
        "(ship_line_id,ship_id,"
        " wh_id,client_id,ordnum,ordlin,"
        " ordsln,adr_id,"
        " cons_batch,linsts,"
        " prcpri,pckgr1,pckgr2,pckgr3,"
        " pckgr4,pckqty,"
        " inpqty,stgqty,shpqty,"
        " oviqty,ovramt,ovrcod) "
        "values "
        " (:ship_line_id, :ship_id, "
        "  :wh_id, :client_id, :ordnum, :ordlin, "
        "  :ordsln, :adr_id, "
        "  :cons_batch, :linsts, "
        "  :prcpri, :pckgr1, :pckgr2, :pckgr3, "
        "  :pckgr4, :pckqty, "
        "  :inpqty, :stgqty, :shpqty, "
        "  :oviqty, :ovramt, :ovrcod) ",
        NULL, NULL,
        BINDSTR("ship_line_id", SHIP_LINE_ID_LEN, newship_line_id),
        BINDSTR("ship_id", SHIP_ID_LEN, ship_id),
        BINDSTR("wh_id", WH_ID_LEN, wh_id),
        BINDSTR("client_id", CLIENT_ID_LEN, client_id),
        BINDSTR("ordnum", ORDNUM_LEN, ordnum),
        BINDSTR("ordlin", ORDLIN_LEN, newordlin),
        BINDSTR("ordsln", ORDSLN_LEN, newordsln),
        BINDSTR("adr_id", ADR_ID_LEN, adr_id_p),
        "cons_batch", COMTYP_INT, sizeof(long), cons_batch, 0,
        BINDSTR("linsts", LINSTS_LEN, LINSTS_PENDING),
        BINDSTR("prcpri", PRCPRI_LEN, prcpri_p),
        BINDSTR("pckgr1", PCKGR1_LEN, pckgr1_p),
        BINDSTR("pckgr2", PCKGR2_LEN, pckgr2_p),
        BINDSTR("pckgr3", PCKGR3_LEN, pckgr3_p),
        BINDSTR("pckgr4", PCKGR4_LEN, pckgr4_p),
        "pckqty", COMTYP_INT, sizeof(long), pick->pckqty, 0,
        "inpqty", COMTYP_INT, sizeof(long), 0, 0,
        "stgqty", COMTYP_INT, sizeof(long), 0, 0,
        "shpqty", COMTYP_INT, sizeof(long), 0, 0,
        "oviqty", COMTYP_INT, sizeof(long), 0, 0,
        "ovramt", COMTYP_INT, sizeof(long), ovramt, 0,
        BINDSTR("ovrcod", OVRCOD_LEN, ovrcod_p));

    ret_status = sqlExecStr(sqlBuffer, NULL);

    if (res)
    {
        sqlFreeResults(res);
    }
    return (ret_status);
    }
    return (eDB_NO_ROWS_AFFECTED);
}

static moca_bool_t sStatusMatchesProgression(char *invsts,
                         char *invsts_prg)
{

    char buffer[1000];
    long ret_status;

    sprintf(buffer,
        "select 1 "
        "  from prgmst "
        " where invsts_prg = '%s'"
        "   and invsts = '%s' "
            "   and alcflg = '%d' ",
        invsts_prg,
        invsts,
            BOOLEAN_TRUE);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
    return(MOCA_TRUE);
    else
    return(MOCA_FALSE);
}

/* This function replaces the ParseParams for load or sub allocations */
static long sParseLoadParams(char *lodnum,
                 char *prtnum, char *prt_client_id,
                 long  pckqty, char *orgcod,
                 char *revlvl, char *supnum, char *lotnum, char *invsts,
                 char *carcod, char *srvlvl,
                 char *schbat, char *ship_id,
                 char *ship_line_id, char *client_id,
                 char *ordnum, 
                 char *stcust, char *rtcust,
                             char *ordlin, char *ordsln,
                 char *concod, char *segqty,
                 char *dstare, char *dstloc,
                 moca_bool_t  splflg, long  untcas,
                 long  untpak,
                 char *wh_id)
{
    PCKWRK_DATA    *Top, *ptr, *ptr2, *tmpptr;
    long            noParams, orderQty, qtyFound;
    long            x, first = 0;
    char            orderbuffer[10000];
    char            shipbuffer[10000];
    char            buildBuffer[5000];
    char            new_concod[CONCOD_LEN + 1];
    char            distro_id_clause[DISTRO_ID_LEN + 50];
    mocaDataRes     *res;
    long            ret_status;
    char            fifdte[DB_STD_DATE_LEN+1];
    mocaDataRow    *row;
    char           *wh_idPtr;
    char           *ordnumPtr, *ordlinPtr, *ordslnPtr;
    char           *client_idPtr;
    char           *ship_idPtr, *ship_line_idPtr;
    char           *stcustPtr, *carcodPtr, *srvlvlPtr;
    char           *rtcustPtr;
    long            allocate_untcas, allocate_untpak;
    char            alloc_casClause[50];
    char            alloc_pakClause[50];

    BYINVPOLICY *byInvPtr;

    /*  Set the allocate_untxxx values from the those passed in.  This 
    **  is to clearly identify the values coming from the shp line
    **  records to be allocated.
    */
    allocate_untcas = untcas;
    allocate_untpak = untpak;

    PickRequestList = (PCKWRK_DATA *) NULL;

    byInvPtr = sGetByInvPolicies(wh_id);

    memset(fifdte, 0, sizeof(fifdte));
    memset(new_concod, 0, sizeof(new_concod));
    appNextNum(NUMCOD_CONCOD, new_concod);
    sprintf(orderbuffer,
        "select * from invlod where lodnum = '%s' for update",
        lodnum);
    ret_status = sqlExecStr(orderbuffer, NULL);
    if (ret_status != eOK)
    {
    return (ret_status);
    }

    /*
     * If order has distro_id, the only rows in invdtl with same distro_id
     * can be used. Let's get distro_id first.
     * But we might not pass ship_line_id in, so we should branch here
     */
    if (ship_line_id && misTrimLen(ship_line_id, SHIP_LINE_ID_LEN))
    {
        sprintf(orderbuffer,
               " select ord_line.distro_id "
               "   from ord_line, shipment_line "
               "  where ord_line.ordnum = shipment_line.ordnum "
               "    and ord_line.client_id = shipment_line.client_id "
               "    and ord_line.wh_id = shipment_line.wh_id "
               "    and ord_line.ordlin = shipment_line.ordlin "
               "    and ord_line.ordsln = shipment_line.ordsln "
               "    and shipment_line.ship_line_id = '%s' "
               "    and ord_line.distro_id is not null ",
               ship_line_id);
    }
    else
    {
        sprintf(orderbuffer,
               " select ord_line.distro_id "
               "   from ord_line, shipment_line "
               "  where ord_line.ordnum = shipment_line.ordnum "
               "    and ord_line.client_id = shipment_line.client_id "
               "    and ord_line.wh_id = shipment_line.wh_id "
               "    and ord_line.ordlin = shipment_line.ordlin "
               "    and ord_line.ordsln = shipment_line.ordsln "
               "    and shipment_line.ship_id = '%s' "
               "    and ord_line.distro_id is not null ",
               ship_id);
    }
    ret_status = sqlExecStr(orderbuffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED )
    {
        sqlFreeResults(res);
        return (ret_status);
    }
    else if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        /* If the order is not a distro order, we don't allow
         * them allocating a distro inventory
         */
        sprintf(distro_id_clause, " and d.distro_id is null");
    }
    else  /* eOK */
    {
        row = sqlGetRow(res);
        if (sqlIsNull(res, row, "distro_id"))
        {
            /* If the order is not a distro order, we don't allow
             * them allocating a distro inventory
             */
            sprintf(distro_id_clause, " and d.distro_id is null");
        }
        else
        {
            sprintf(distro_id_clause,
                    " and d.distro_id = '%s'",
                    sqlGetString(res, row, "distro_id"));
        }
    }
    sqlFreeResults(res);    
    res = NULL;

    /* Grab the oldest fifo date from the inventory - this gets
       returned strictly for consistency... */
    sprintf(orderbuffer,
        "select to_char(invdtl.fifdte, 'YYYYMMDDHH24MISS') fifdte "
        "  from invdtl, invsub "
        " where invsub.subnum = invdtl.subnum "
        "   and invsub.lodnum = '%s'"
        " order by invdtl.fifdte",
        lodnum);
    ret_status = sqlExecStr(orderbuffer, &res);
    if (ret_status != eOK)
    {
    sqlFreeResults(res);
    res = NULL;
    ret_status = sqlExecStr("select to_char(sysdate, "
                            "               'YYYYMMDDHH24MISS') fifdte"
                "  from dual", &res);
    if (ret_status != eOK)
    {
        /* Bad failure...let's get out... */
        sLog(0, "(%d) Unable to get fifo date from DUAL!",
           ret_status);
        sqlFreeResults(res);
        return (ret_status);
    }
    }
    row = sqlGetRow(res);
    strncpy(fifdte, sqlGetString(res, row, "fifdte"), DB_STD_DATE_LEN);
    sqlFreeResults(res);

    /*  Set-up the where clause for allocating by alloc_cas and alloc_pak
    */
    sprintf(alloc_casClause, " and d.untcas = '%ld' ",
        allocate_untcas);

    sprintf(alloc_pakClause, " and d.untpak = '%ld' ",
        allocate_untpak);

    sprintf(orderbuffer,
        "select lm.wh_id, lm.arecod, lm.stoloc, l.lodnum, l.asset_typ,"
        "       d.untcas, d.untpak, "
        "       d.prtnum, d.prt_client_id, "
        "       d.lotnum, d.orgcod, d.revlvl, d.supnum, d.invsts, "
        "       d.ftpcod, p.lodlvl, p.catch_cod, "
        "       sum(d.untqty) untqty, d.distro_id "
        "  from locmst lm, prtmst_view p, invdtl d, "
        "       invsub s, invlod l"
        " where lm.stoloc = l.stoloc "
        "   and lm.wh_id = l.wh_id "
        "   and p.prtnum = d.prtnum "
        "   and p.prt_client_id = d.prt_client_id "
        "   and d.subnum = s.subnum "
        "   and s.lodnum = l.lodnum "
        "   and p.wh_id  = l.wh_id "
        "   and lm.wh_id  = l.wh_id "
        "   and l.lodnum = '%s' "
        "   %s "        /* Allocate Unit Case */
        "   %s "        /* Allocate Unit Pack */
        "   %s "        /* distro_id */
        " group by lm.wh_id, lm.arecod, lm.stoloc, l.lodnum, l.asset_typ, "
        "          d.untcas, d.untpak, "
        "          d.prtnum, d.prt_client_id, "
        "          d.lotnum, d.orgcod, d.revlvl, d.supnum, d.invsts, "
        "          d.ftpcod, p.catch_cod, p.lodlvl, d.distro_id ",
        lodnum,
        ((allocate_untcas > 0) ? alloc_casClause : ""),
        ((allocate_untpak > 0) ? alloc_pakClause : ""),
        distro_id_clause);
    ret_status = sqlExecStr(orderbuffer, &res);
    if (ret_status != eOK)
    {
    sqlFreeResults(res);
    return (ret_status);
    }
    Top = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));
    if (Top == NULL)
    return (eNO_MEMORY);
    ptr = Top;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
    if (schbat && misTrimLen(schbat, SCHBAT_LEN))
    {
        strncpy(ptr->schbat, schbat, misTrimLen(schbat, SCHBAT_LEN));
    }
    else
    {
        /* 
             * Before multiple warehouse, originally the miscellaneous schbat  
             * is a constant string "MISC", now we use warehouse id as  
             * the miscellaneous schbat for a specified warehouse.
             */
        char misc_schbat[SCHBAT_LEN + 1];
        memset(misc_schbat, 0, sizeof(misc_schbat));
        misTrimcpy(misc_schbat, ptr->wh_id, SCHBAT_LEN);
        
        misTrimcpy(ptr->schbat, misc_schbat, SCHBAT_LEN);
    }

    ptr->pckqty = (long) sqlGetFloat(res, row, "untqty");
    if (ptr->pckqty <= 0)
    {
        sqlFreeResults(res);
        trnFreePickList(Top);
        return (eINVALID_ARGS);
    }
    strncpy(ptr->wh_id, misTrimLR(sqlGetString(res, row, "wh_id")),
        WH_ID_LEN);
    strncpy(ptr->srcare, misTrimLR(sqlGetString(res, row, "arecod")),
        ARECOD_LEN);
    strncpy(ptr->srcloc, misTrimLR(sqlGetString(res, row, "stoloc")),
        STOLOC_LEN);
    strncpy(ptr->lodnum, misTrimLR(sqlGetString(res, row, "lodnum")),
        LODNUM_LEN);
    ptr->untcas = sqlGetLong(res, row, "untcas");
    ptr->untpak = sqlGetLong(res, row, "untpak");
    strncpy(ptr->lodlvl, LODLVL_LOAD, LODLVL_LEN);

    /* PR 56507 and WMD-37067
     * If ASSET-TRACKING configuration is enabled, and lodlvl = 'L'(see above),
     * we need to consider asset.
     */
    if (asset_track_enabled && !sqlIsNull(res, row, "asset_typ"))
            strncpy(ptr->asset_typ, misTrimLR(sqlGetString(res, row, "asset_typ")),
                    ASSET_TYP_LEN);

    strncpy(ptr->prtnum, misTrimLR(sqlGetString(res, row, "prtnum")),
        PRTNUM_LEN);
    strncpy(ptr->prt_client_id,
        misTrimLR(sqlGetString(res, row, "prt_client_id")),
        CLIENT_ID_LEN);
    if (strncmp(sqlGetString(res, row, "orgcod"), 
            DEFAULT_ORGCOD, strlen(DEFAULT_ORGCOD)) != 0)
    {
        strncpy(ptr->orgcod, misTrimLR(sqlGetString(res, row, "orgcod")),
            ORGCOD_LEN);
    }
    if (strncmp(sqlGetString(res, row, "lotnum"), 
                 DEFAULT_LOTNUM, strlen(DEFAULT_LOTNUM)) != 0)
    {
        strncpy(ptr->lotnum, misTrimLR(sqlGetString(res, row, "lotnum")),
            LOTNUM_LEN);
    }
    if (strncmp(sqlGetString(res, row, "revlvl"), 
                 DEFAULT_REVLVL, strlen(DEFAULT_REVLVL)) != 0)
    {
        strncpy(ptr->revlvl, misTrimLR(sqlGetString(res, row, "revlvl")),
            REVLVL_LEN);
    }

    if (!sqlIsNull(res, row, "supnum"))
    {
        misTrimcpy(ptr->supnum, 
                   sqlGetString(res, row, "supnum"),
                   SUPNUM_LEN);
    }

    strncpy(ptr->invsts,
        misTrimLR(sqlGetString(res, row, "invsts")),
        INVSTS_LEN);
    if (!sqlIsNull(res, row, "distro_id"))
    {
        misTrimcpy(ptr->distro_id, 
                   sqlGetString(res, row, "distro_id"),
                   DISTRO_ID_LEN);
    }
    if (dstare && misTrimLen(dstare, ARECOD_LEN))
        strncpy(ptr->dstare, misTrimLR(dstare), ARECOD_LEN);
    if (dstloc && misTrimLen(dstloc, STOLOC_LEN))
        strncpy(ptr->dstloc, misTrimLR(dstloc), STOLOC_LEN);
    strncpy(ptr->prt_lodlvl, misTrimLR(sqlGetString(res, row, "lodlvl")),
        LODLVL_LEN);
        if (!sqlIsNull(res, row, "catch_cod"))
        {
            misTrimcpy(ptr->prt_catch_cod, 
                       sqlGetString(res, row, "catch_cod"),
                       CATCH_COD_LEN);
        }
    strncpy(ptr->ftpcod, misTrimLR(sqlGetString(res, row, "ftpcod")),
        FTPCOD_LEN);
    strncpy(ptr->concod, misTrimLR(new_concod), CONCOD_LEN);
    ptr->splflg = splflg;

    strncpy(ptr->fifdte, fifdte, DB_STD_DATE_LEN);

    /*  These values are from the order/shipment details and are customer
    **  requirements.  They identified the need to allocate 
    **  specific packaging units.  A zero is the default and
    **  means that any packaging unit may be allocated.
    */
    if (allocate_untcas < 0)
        ptr->alloc_cas = 0;
    else
        ptr->alloc_cas = allocate_untcas;

    if (allocate_untpak < 0)
        ptr->alloc_pak = 0;
    else
        ptr->alloc_pak = allocate_untpak;

    ptr->next = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));
    if (ptr->next == NULL)
    {
        trnFreePickList(Top);
        return (eNO_MEMORY);
    }
    ptr->next->prev = ptr;
    ptr = ptr->next;
    }
    ptr->prev->next = NULL;
    free(ptr);
    if (ptr == Top)
    Top = NULL;
    ptr = NULL;

    /* At this point, we've established the number of seqments we
       have and that the quantity is OK, now we simply need to
       parse out the rest of the parameters and fill them into their
       appropriate element in the pckwrk structure...  */

    sqlFreeResults(res);

    /* 2007.2 - "Pick Policy by Allocation Search Path"
     *
     * We pass an empty string as the parameter alc_search_path, because
     * it only makes sense for warehouse wide level allocation, it is
     * not necessary in the situation of a load specific allocation.
     */
    noParams = sGetNoParams(carcod, srvlvl, schbat, ship_id,
                ship_line_id, client_id, ordnum, stcust, rtcust,
                ordlin, ordsln, concod, wh_id, "");
    if (noParams < 0)
    {
    trnFreePickList(Top);
    return (noParams);
    }
    sprintf(shipbuffer,
        "select sl.wh_id, sl.pckqty, sl.client_id, "
        "       sl.ordnum, sl.ordlin, sl.ordsln, "
        "       sl.ship_id, sl.ship_line_id, o.stcust, "
            "       o.rtcust, "
        "       sh.carcod, sh.srvlvl, "
        "       ol.prtnum, ol.prt_client_id, "
        "       ol.lotnum, ol.orgcod,"
        "       ol.revlvl, ol.supnum, ol.invsts_prg,"
        "       o.ordtyp, o.bto_seqnum, o.slot, "
        "       ol.marcod, ol.ordinv, "
        "       ol.alc_search_path, "
        "       ol.distro_id "
        "  from shipment sh, shipment_line sl, ord o, ord_line ol "
        " where sh.ship_id  = sl.ship_id "
        "   and sl.client_id = o.client_id "
        "   and sl.ordnum    = o.ordnum "
        "   and sl.wh_id     = o.wh_id "
        "   and sl.client_id = ol.client_id "
        "   and sl.ordnum    = ol.ordnum "
        "   and sl.ordlin    = ol.ordlin "
        "   and sl.ordsln    = ol.ordsln "
        "   and sl.wh_id     = ol.wh_id ");

    wh_idPtr = wh_id - 1;
    client_idPtr = client_id - 1;
    ship_idPtr = ship_id - 1;
    ship_line_idPtr = ship_line_id - 1;
    ordnumPtr = ordnum - 1;
    ordlinPtr = ordlin - 1;
    ordslnPtr = ordsln - 1;
    stcustPtr = stcust - 1;
    rtcustPtr = rtcust - 1;
    carcodPtr = carcod - 1;
    srvlvlPtr = srvlvl - 1;

    for (x = 0; x < noParams; x++)
    {
    sLog(0, "Building order buffer for %d params", noParams);
    first = 0;
    sprintf(buildBuffer, " %s ( ", x == 0 ? " and (" : " or ");
    strcat(shipbuffer, buildBuffer);

        if (misTrimLen(wh_id, WH_ID_LEN))
    {
        wh_idPtr = strtok(wh_idPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sl.wh_id = '%s' ",
            first ? " and " : "", wh_idPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        wh_idPtr = wh_idPtr + strlen(wh_idPtr);
    }
    if (misTrimLen(client_id, CLIENT_ID_LEN))
    {
        client_idPtr = strtok(client_idPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sl.client_id = '%s' ",
            first ? " and " : "", client_idPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        client_idPtr = client_idPtr + strlen(client_idPtr);
    }
    if (misTrimLen(ordnum, ORDNUM_LEN))
    {
        ordnumPtr = strtok(ordnumPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sl.ordnum = '%s' ",
            first ? " and " : "", ordnumPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        ordnumPtr = ordnumPtr + strlen(ordnumPtr);
    }
    if (misTrimLen(ordlin, ORDLIN_LEN))
    {
        ordlinPtr = strtok(ordlinPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sl.ordlin = '%s' ",
            first ? " and " : "", ordlinPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        ordlinPtr += strlen(ordlinPtr);
    }
    if (misTrimLen(ordsln, ORDSLN_LEN))
    {
        ordslnPtr = strtok(ordslnPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sl.ordsln = '%s' ",
            first ? " and " : "", ordslnPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        ordslnPtr += strlen(ordslnPtr);
    }
    if (misTrimLen(carcod, CARCOD_LEN))
    {
        carcodPtr = strtok(carcodPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sh.carcod = '%s' ",
            first ? " and " : "", carcodPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        carcodPtr += strlen(carcodPtr);
    }
    if (misTrimLen(srvlvl, SRVLVL_LEN))
    {
        srvlvlPtr = strtok(srvlvlPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sh.srvlvl = '%s' ",
            first ? " and " : "", srvlvlPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        srvlvlPtr += strlen(srvlvlPtr);
    }
        /* 
         * NOTE: This obviously doesn't work as these columns don't exist
         * on these tables.  They're here to fix someday, I guess.
         */
    if (misTrimLen(stcust, ADRNUM_LEN))
    {
        stcustPtr = strtok(stcustPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s o.stcust = '%s' ",
            first ? " and " : "", stcustPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        stcustPtr += strlen(stcustPtr);
    }
    if (misTrimLen(rtcust, ADRNUM_LEN))
    {
        rtcustPtr = strtok(rtcustPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s o.rtcust = '%s' ",
            first ? " and " : "", rtcustPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        rtcustPtr += strlen(rtcustPtr);
    }
    if (misTrimLen(ship_id, SHIP_ID_LEN))
    {
        ship_idPtr = strtok(ship_idPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sl.ship_id = '%s' ",
            first ? " and " : "", ship_idPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        ship_idPtr += strlen(ship_idPtr);
    }
    if (misTrimLen(ship_line_id, SHIP_LINE_ID_LEN))
    {
        ship_line_idPtr = strtok(ship_line_idPtr + 1, CONCAT_CHAR);
        sprintf(buildBuffer, " %s sl.ship_line_id = '%s' ",
            first ? " and " : "", ship_line_idPtr);
        strcat(shipbuffer, buildBuffer);
        first++;
        ship_line_idPtr += strlen(ship_line_idPtr);
    }

    /* What do we do here??????????
       if (misTrimLen(concod, CONCOD_LEN)) {
       concodPtr = strtok(concodPtr+1, CONCAT_CHAR);
       concodPtr += strlen(concodPtr);
       }
       else{

       memset(new_concod, 0, sizeof(new_concod));
       appNextNum(NUMCOD_CONCOD, new_concod);
       }
     */

    strcat(shipbuffer, ")");
    }
    strcat(shipbuffer, ")");

    ret_status = sqlExecStr(shipbuffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
    sqlFreeResults(res);
    trnFreePickList(Top);
    return (ret_status);
    }
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
    orderQty = sqlGetLong(res, row, "pckqty");
    qtyFound = 0;
    sLog(0, "Attempting to fill order to inventory");
    for (ptr = Top; qtyFound < orderQty && ptr; ptr = ptr->next)
    {
        if (!sqlIsNull(res, row, "invsts_prg") &&
        !sStatusMatchesProgression(ptr->invsts,
                       sqlGetString(res, row,
                            "invsts_prg")))
        {
        continue;
        }


        if (!strcmp(ptr->prtnum, 
            misTrim(sqlGetString(res, row, "prtnum"))) &&
           (!strcmp(ptr->prt_client_id, 
            misTrim(sqlGetString(res, row, "prt_client_id")))) &&
        (misTrimLen(sqlGetString(res, 
                     row, "lotnum"), LOTNUM_LEN) == 0 ||
         sqlIsNull(res, row, "lotnum") ||
         !strcmp(ptr->lotnum,
             misTrim(sqlGetString(res, row, "lotnum")))) &&
        (misTrimLen(sqlGetString(res, 
                     row, "orgcod"), ORGCOD_LEN) == 0 ||
         sqlIsNull(res, row, "orgcod") ||
         !strcmp(ptr->orgcod,
             misTrim(sqlGetString(res, row, "orgcod")))) &&
        (misTrimLen(sqlGetString(res, 
                     row, "revlvl"), REVLVL_LEN) == 0 ||
         sqlIsNull(res, row, "revlvl") ||
         !strcmp(ptr->revlvl,
             misTrim(sqlGetString(res, row, "revlvl")))) &&
        (misTrimLen(sqlGetString(res, 
                     row, "supnum"), SUPNUM_LEN) == 0 ||
         sqlIsNull(res, row, "supnum") ||
         !strcmp(ptr->supnum,
             misTrim(sqlGetString(res, row, "supnum")))) &&
        !misTrimLen(ptr->ordnum, ORDNUM_LEN) &&
        !misTrimLen(ptr->client_id, CLIENT_ID_LEN))
        {
        sLog(0, "Found match - applying %d", qtyFound);
        sFillPickWork(ptr, res, row);
        qtyFound = ptr->pckqty;
        if (ptr->pckqty > orderQty)
        {
            sSplitPickWork(ptr, ptr->pckqty - orderQty);
        }
        ptr->processed = 1;
        }
    }
    if (qtyFound < orderQty)
    {
        /* We've got an order without a pick ref */
        qtyFound = qtyFound;
    }
    }

    sLog(0, "Attempting to match like orders...");
    /* Now, we need to match any like orders */
    if (byInvPtr->matchSubline || byInvPtr->matchLine ||
    byInvPtr->createSubline || byInvPtr->createLine)
    {
    for (ptr = Top; ptr; ptr = ptr->next)
    {
        if (!ptr->processed)
        {
        for (row = sqlGetRow(res);
             !ptr->processed && row; row = sqlGetNextRow(row))
        {
            ret_status =
            sMatchOrderToInventory(sqlGetString(res, row, "ship_id"),
                                   sqlGetString(res, 
                                                row, 
                                                "ship_line_id"),
                                   sqlGetString(res, row, "client_id"),
                                   sqlGetString(res, row, "ordnum"),
                                   sqlGetString(res, row, "ordlin"),
                                   sqlGetString(res, row, "ordsln"),
                                   sqlGetString(res, row, "wh_id"),
                                   ptr);
            if (ret_status != eOK &&
            ret_status != eDB_NO_ROWS_AFFECTED)
            {
            trnFreePickList(Top);
            sqlFreeResults(res);
            return (ret_status);
            }
            if (ret_status == eOK)
            {
            sLog(0, "Matched: %ld", ptr);
            ptr->processed = 1;
            }
        }
        }
    }
    }
    sLog(0, "Done with match...moving on to leftover...");
    /* Now we need to check for picks that don't have order info and see if
       there is another pick that matches that does have an order */
    for (ptr = Top; ptr; ptr = ptr->next)
    {
    if (!ptr->processed)
    {
        sLog(0, "Checking: %ld", ptr);
        for (ptr2 = Top; ptr2; ptr2 = ptr2->next)
        {
        sLog(0, "Against: %ld", ptr2);
        if (strlen(ptr2->ordnum) &&
            strlen(ptr2->client_id) &&
            !strncmp(ptr2->prtnum, ptr->prtnum, PRTNUM_LEN) &&
            !strncmp(ptr2->prt_client_id, ptr->prt_client_id,
                            CLIENT_ID_LEN) &&
            !strncmp(ptr2->lotnum, ptr->lotnum, LOTNUM_LEN) &&
            !strncmp(ptr2->orgcod, ptr->orgcod, ORGCOD_LEN) &&
            !strncmp(ptr2->revlvl, ptr->revlvl, REVLVL_LEN) &&
            !strncmp(ptr2->supnum, ptr->supnum, SUPNUM_LEN) &&
            !strncmp(ptr2->invsts, ptr->invsts, INVSTS_LEN) &&
            !strncmp(ptr2->distro_id, ptr->distro_id, DISTRO_ID_LEN))
        {
                    /* This is a little confusing, so I want to explain
                     * what is happening with the pointers here.
                     * 
                     * ptr needs to be freed, so the link before needs its 
                     * next pointer to skip over the ptr link to the next link.
                     * 
                     * Also we need the reference to the previous links to
                     * skip past ptr.
                     */
            ptr2->pckqty = ptr2->pckqty + ptr->pckqty;
            if (ptr->prev)
            ptr->prev->next = ptr->next;
            if (ptr->next)
            ptr->next->prev = ptr->prev;
                        
                    /* So now we have ptr isolated and ready to free, but there
                     * is one more little piece to consider.  That is the 
                     * outer for loop continuity.  Because the for loop is 
                     * going to advance to the next link, we need to set the 
                     * new current pointer one link back.  
                     *  
                     * Once I have freed the ptr I don't have access to any
                     * of the reference information. So I need to save the
                     * previous link position.
                     *   
                     * I am going to temporarily hold the previous link in
                     * tmpptr so that when the outer for loop advances it, it
                     * will point to the next link that was after this link
                     * that we are about to free.
                     */
                    tmpptr = ptr->prev;
            free(ptr);
            ptr = tmpptr;
            break;
        }
        }
        if (!ptr2)
        {
        sLog(0, "Now checking against filled orders");
        /* At this point we haven't found any order that hasn't been
           fulfilled that will match this order.  Let's go back and
           check orders that have been filled. */
        for (row = sqlGetRow(res);
             !ptr->processed && row; row = sqlGetNextRow(row))
        {
                if (!sqlIsNull(res, row, "invsts_prg") &&
                    !sStatusMatchesProgression(ptr->invsts,
                           sqlGetString(res, row,
                                "invsts_prg")))
                {
                    continue;
                }
                
            if (!strcmp(ptr->prtnum,
                misTrim(sqlGetString(res, row, "prtnum"))) &&
               (!strcmp(ptr->prt_client_id,
                misTrim(sqlGetString(res, 
                                     row, 
                                     "prt_client_id")))) &&
            (misTrimLen(sqlGetString(res, row, "lotnum"),
                    LOTNUM_LEN) == 0 ||
             sqlIsNull(res, row, "lotnum") ||
             !strcmp(ptr->lotnum,
                   misTrim(sqlGetString(res, row, "lotnum")))) &&
            (misTrimLen(sqlGetString(res, row, "orgcod"),
                    ORGCOD_LEN) == 0 ||
             sqlIsNull(res, row, "orgcod") ||
             !strcmp(ptr->orgcod,
                   misTrim(sqlGetString(res, row, "orgcod")))) &&
            (misTrimLen(sqlGetString(res, row, "revlvl"),
                    REVLVL_LEN) == 0 ||
             sqlIsNull(res, row, "revlvl") ||
             !strcmp(ptr->revlvl,
                   misTrim(sqlGetString(res, row, "revlvl")))) &&
            (misTrimLen(sqlGetString(res, row, "supnum"),
                    SUPNUM_LEN) == 0 ||
             sqlIsNull(res, row, "supnum") ||
             !strcmp(ptr->supnum,
                   misTrim(sqlGetString(res, row, "supnum")))) &&
            (sqlIsNull(res, row, "distro_id") ||
            misTrimLen(sqlGetString(res, row, "distro_id"),
                    DISTRO_ID_LEN) == 0 ||
             !strcmp(ptr->distro_id,
                   misTrim(sqlGetString(res, row, "distro_id")))) &&
            !misTrimLen(ptr->ordnum, ORDNUM_LEN) &&
            !misTrimLen(ptr->client_id, CLIENT_ID_LEN))
            {
            sFillPickWork(ptr, res, row);
            qtyFound = ptr->pckqty;
            ptr->processed = 1;
            }
        }

        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            sLog(0, "Now attempting to create order... ");
            ret_status =
            sCreateOrderToInventory(
                                        sqlGetString(res, row, "ship_id"),
                                        sqlGetString(res, row, "ship_line_id"),
                                        sqlGetString(res, row, "client_id"),
                                        sqlGetString(res, row, "ordnum"),
                                        sqlGetString(res, row, "ordlin"),
                                        sqlGetString(res, row, "ordsln"),
                                        sqlGetString(res, row, "wh_id"),
                                        ptr);
            if (ret_status != eOK &&
            ret_status != eDB_NO_ROWS_AFFECTED)
            {
            trnFreePickList(Top);
            sqlFreeResults(res);
            return (ret_status);
            }
            if (ret_status == eOK)
            {
            ret_status =
            sMatchOrderToInventory(
                                       sqlGetString(res, row, "ship_id"),
                                       sqlGetString(res, row, "ship_line_id"),
                                       sqlGetString(res, row, "client_id"),
                                       sqlGetString(res, row, "ordnum"),
                                       sqlGetString(res, row, "ordlin"),
                                       sqlGetString(res, row, "ordsln"),
                                       sqlGetString(res, row, "wh_id"),
                                       ptr);
                if (ret_status != eOK &&
                ret_status != eDB_NO_ROWS_AFFECTED)
                {
                trnFreePickList(Top);
                sqlFreeResults(res);
                return (ret_status);
                }
            }        
        }
        }
    }
    else
        ptr->processed = 0;
    }
    sqlFreeResults(res);
    PickRequestList = Top;
    sLog(0, "Getting out...");
    return (eOK);
}

static long sDetermineVerification(PCKWRK_DATA *pick,
                   moca_bool_t *lodflg,
                   moca_bool_t *subflg,
                   moca_bool_t *dtlflg,
                   moca_bool_t *prtflg,
                   moca_bool_t *locflg,
                   moca_bool_t *qtyflg,
                                   moca_bool_t *catch_qty_flg,
                   moca_bool_t *orgflg,
                   moca_bool_t *revflg,
                   moca_bool_t *supflg,
                   moca_bool_t *lotflg,
                                   char        *wrktyp)
{
    long            ret_status;
    RETURN_STRUCT  *CurPtr = NULL;
    mocaDataRes    *res = NULL;
    mocaDataRow    *row;
    char            srv_command[2000];

    /*
     * NOTE: The rplflg means that this pick is a replenishment pick.  So,
     * if the ship_id or the wkonum is NOT NULL, that means it is a 
     * non-replenishment pick.
     */
    sprintf(srv_command,
        "get pick verification flags "
        " where wh_id = '%s' "
        "   and pick_lodlvl = '%s' "
        "   and pick_arecod = '%s' "
        "   and pick_srcloc = '%s' "
        "   and part_lodlvl = '%s' "
        "   and part_catch_cod = '%s' "
        "   and part_orgflg = %d "
        "   and part_revflg = %d "
        "   and part_lotflg = %d "
        "   and area_lodflg = %d "
        "   and area_subflg = %d "
        "   and area_dtlflg = %d "
        "   and area_loccod = '%s' "
        "   and rplflg = %d "
        "   and wrktyp = '%s' "
        "   and pck_catch_qty = %f ",
        pick->wh_id, pick->lodlvl, pick->srcare, pick->srcloc,
        pick->prt_lodlvl, 
        pick->prt_catch_cod, 
        pick->prt_orgflg, pick->prt_revflg, pick->prt_lotflg,
        pick->area_lodflg, pick->area_subflg, pick->area_dtlflg,
        pick->area_loccod,
            (strlen(pick->ship_id) || strlen(pick->wkonum)) ?
                BOOLEAN_FALSE : BOOLEAN_TRUE,
        wrktyp,
        pick->pck_catch_qty ? pick->pck_catch_qty : 0.0);

    *lodflg        = BOOLEAN_FALSE;
    *subflg        = BOOLEAN_FALSE;
    *dtlflg        = BOOLEAN_FALSE;
    *orgflg        = BOOLEAN_FALSE;
    *revflg        = BOOLEAN_FALSE;
    *supflg        = BOOLEAN_FALSE;
    *lotflg        = BOOLEAN_FALSE;
    *qtyflg        = BOOLEAN_FALSE;
    *catch_qty_flg = BOOLEAN_FALSE;
    *locflg        = BOOLEAN_FALSE;
    *prtflg        = BOOLEAN_FALSE;

    ret_status = srvInitiateCommand(srv_command, &CurPtr);
    if (ret_status == eOK)
    {
    res = srvGetResults(CurPtr);

    if (res && (row = sqlGetRow(res)))
    {
        ret_status = eOK;
        
        *subflg = sqlGetBoolean(res, row, "subflg");
        *dtlflg = sqlGetBoolean(res, row, "dtlflg");
        *lodflg = sqlGetBoolean(res, row, "lodflg");
        *revflg = sqlGetBoolean(res, row, "revflg");
        *supflg = sqlGetBoolean(res, row, "supflg");
        *orgflg = sqlGetBoolean(res, row, "orgflg");
        *lotflg = sqlGetBoolean(res, row, "lotflg");
        *prtflg = sqlGetBoolean(res, row, "prtflg");
        *qtyflg = sqlGetBoolean(res, row, "qtyflg");
        if (!sqlIsNull(res, row, "catch_qty_flg"))
        *catch_qty_flg = sqlGetBoolean(res, row, "catch_qty_flg");

        *locflg = sqlGetBoolean(res, row, "locflg");
    }
    else
    {
        ret_status = eDB_NO_ROWS_AFFECTED;
    }

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    return (ret_status);

    }
    else if (CurPtr)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
        res = NULL;
    }

    return (ret_status);
}

long sGetLoadUntqty(INVENTORY *ip)
{
    long        lodqty;
    INVENTORY_ATTR    *attr;
    
    lodqty = 0;
    for(attr = ip->attr; attr; attr =attr->next)
    {
    lodqty += attr->untqty;
    }
    return(lodqty);
}


/* sVerifyPickQuantity
 * - Checks for over-allocation by unit quantity
 */
static long sVerifyPickQuantity(PCKWRK_DATA *ptr, 
                moca_bool_t *ovralcflg_i, 
                char *pcktyp)
{
    long            ret_status;
    long            remaining_qty;
    mocaDataRow    *row;
    mocaDataRes    *res = NULL;
    char            sqlbuffer[5000];
    char            tempBuf[500];
    moca_bool_t     ovralcflg;
    RETURN_STRUCT  *CurPtr = NULL;
    
    static mocaDataRes *PolRes1 = NULL;
    static mocaDataRes *PolRes2 = NULL;
    mocaDataRow *PolRow1 = NULL;
    mocaDataRow *PolRow2 = NULL;
    
    moca_bool_t ErrorOnOverAllocation = 0, AllowOverAllocation = 0;
    

    /* Return if the pointer is null */
    if (!ptr)
    return (eOK);


    /* If we don't have a ship_id or this isn't a pick type for
     * orders, then no need to check quantities
     */
    if ((strlen(ptr->ship_id) == 0 && strlen(ptr->wkonum) == 0) ||
    (misCiStrcmp(pcktyp, ALLOCATE_PICKNSHIP) != 0 &&
     misCiStrcmp(pcktyp, ALLOCATE_INVENTORY_MOVE) != 0 &&
     misCiStrcmp(pcktyp, ALLOCATE_PICKNREPLENNSHIP) != 0))
    return(eOK);
    /*
     * Get policies controlling behavior (if we haven't already) 
     * There are two:
     *   1)  Do we allow over allocation?
     *   2)  Do we error on overallocation?
     */

    if (PolRes1 == NULL)
    {
        sprintf(sqlbuffer,
        " select wh_id, rtstr1 "
        "   from poldat_view "
        "  where polcod = '%s' "
        "    and polvar = '%s' "
        "    and polval = '%s' "
        "    and upper(rtstr1) = 'N'",
        POLCOD_PRD_OVRALLOC,
        POLVAR_MISC,
        POLVAL_OVRALLOC_ALLOWED);

        ret_status = sqlExecStr(sqlbuffer, &PolRes1);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(PolRes1);
            PolRes1 = NULL;
        }
        
        /* Cache the policies. */
        misFlagCachedMemory((OSFPTR) sqlFreeResults, PolRes1);
    }
    
    AllowOverAllocation = BOOLEAN_TRUE;
    if (PolRes1 != NULL)
    {
        for(PolRow1 = sqlGetRow(PolRes1); PolRow1;
            PolRow1 = sqlGetNextRow(PolRow1))
        {
            if (!strcmp(sqlGetString(PolRes1, PolRow1, "wh_id"), ptr->wh_id))
                AllowOverAllocation = BOOLEAN_FALSE;
        }
    }


    if (PolRes2 == NULL)
    {
        sprintf(sqlbuffer,
        " select wh_id, rtstr1 "
        "   from poldat_view "
        "  where polcod = '%s' "
        "    and polvar = '%s' "
        "    and polval = '%s' "
        "    and upper(rtstr1) = 'N'",
                POLCOD_PRD_OVRALLOC, 
        POLVAR_MISC,
        POLVAL_OVRALLOC_ERROR_RETURN);

        ret_status = sqlExecStr(sqlbuffer, &PolRes2);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(PolRes2);
            PolRes2 = NULL;
        }
        
        /* Cache the policies. */
        misFlagCachedMemory((OSFPTR) sqlFreeResults, PolRes2);
    }

    ErrorOnOverAllocation = BOOLEAN_TRUE;
    if (PolRes2 != NULL)
    {
        for(PolRow2 = sqlGetRow(PolRes2); PolRow2;
            PolRow2 = sqlGetNextRow(PolRow2))
        {
            if (!strcmp(sqlGetString(PolRes2, PolRow2, "wh_id"), ptr->wh_id))
                ErrorOnOverAllocation = BOOLEAN_FALSE;
        }
    }
    

    /* If we have received an overallocate flag parameter, use it 
     * else, use the policy (or defaulted policy value)
     */
    if (ovralcflg_i)
    {
    ovralcflg = *ovralcflg_i;
    }
    else
    {
    ovralcflg = AllowOverAllocation;
    }


    /* Loop through the order lines in the request and check
       the quantity remaining to be picked against the seqment
       quantity */

    for (; ptr; ptr = ptr->next)
    {

    remaining_qty = -1;
    /*
     * select against the shipment_line and 
     * the pckwrk's to find the max pckqty 
     */
    if (strlen(ptr->ship_id) == 0 && strlen(ptr->wkonum) == 0)
        return (eINVALID_ARGS);

    if (strlen(ptr->ship_id))
    {
        sprintf(sqlbuffer,
            "select sl.pckqty, ol.marcod, sl.ovrcod, sl.oviqty, "
            "       sl.inpqty, ol.shpqty, "
            "       ol.ordqty, sl.ovramt, ol.rpqflg, ol.ordinv "
            "  from shipment_line sl, ord_line ol "
            " where sl.ship_line_id = '%s' "
            "   and sl.ordnum = ol.ordnum "
            "   and sl.wh_id  = ol.wh_id "
            "   and sl.ordlin = ol.ordlin "
            "   and sl.ordsln = ol.ordsln "
            "   and sl.client_id = ol.client_id ",
            ptr->ship_line_id);
    }
    else
    {
        sprintf(sqlbuffer,
            "select wd.pckqty, cast(null as float) marcod, wd.ovrcod, wd.oviqty, "
            "       wd.inpqty, wd.linqty ordqty, wd.ovramt, "
            "       wd.rpqflg, cast(null as float) ordinv, 0 shpqty "
            "  from wkodtl wd "
            " where wd.wkonum = '%s' "
            "   and wd.wkorev = '%s' "
            "   and wd.wh_id  = '%s' "
            "   and wd.client_id = '%s' "
            "   and wd.seqnum = 0 "
            "   and wd.wkolin = '%s' ",
            ptr->wkonum, 
            ptr->wkorev,
            ptr->wh_id,
            ptr->client_id,
            ptr->wkolin);
    }
    
    ret_status = sqlExecStr(sqlbuffer, &res);
        
        if (ret_status != eOK)
    {
        sqlFreeResults(res);
        if (ret_status == eDB_NO_ROWS_AFFECTED)
        return (eINVALID_ARGS);
        else
        return (ret_status);
    }
    row = sqlGetRow(res);
    remaining_qty = sqlGetLong(res, row, "pckqty");
    misTrimcpy(ptr->ordinv, sqlGetString(res, row, "ordinv"), ORDINV_LEN);

    if (ovralcflg == BOOLEAN_FALSE)
    {
        if (remaining_qty < ptr->pckqty)
        {
        if (ErrorOnOverAllocation)
        {
            sqlFreeResults(res);
            return (eINT_CANT_OVRALLOC);
        }
        else
            ptr->pckqty = remaining_qty;
        }
        sqlFreeResults(res);
        res = NULL;
    }
    else
    {
        /* Call Get Line Allocation Information
         * This will tell us the amount max we can over allocate   
         */

        if (!sqlIsNull(res, row, "marcod"))
        {
        if (misTrimLen(sqlGetString(res, row, "marcod"), MARCOD_LEN) &&
            remaining_qty < ptr->pckqty)
        {
            sqlFreeResults(res);
            return (eINT_CANT_OVRALLOC_MARRIED_LINE);
        }
        }
        CurPtr = NULL;
        sprintf(sqlbuffer,
            "  get line allocation information "
            " where ship_id = '%s' and ship_line_id = '%s' "
            "   and wkonum = '%s' and wkorev = '%s' "
            "   and wkolin = '%s' "
            "   and client_id = '%s' "
            "   and ordnum = '%s' and ordlin = '%s' "
            "   and ordsln = '%s' and ordqty = %ld "
            "   and shpqty = %ld and pckqty = %ld "
            "   and oviqty = %ld and inpqty = %ld "
            "   and prtnum = '%s' and prt_client_id = '%s'"
            "   and wh_id  = '%s' ",
            ptr->ship_id,
            ptr->ship_line_id,
            ptr->wkonum,
            ptr->wkorev,
            ptr->wkolin,
            ptr->client_id,
            ptr->ordnum,
            ptr->ordlin, ptr->ordsln,
            sqlGetLong(res, row, "ordqty"),
            sqlGetLong(res, row, "shpqty"),
            sqlGetLong(res, row, "pckqty"),
            sqlGetLong(res, row, "oviqty"),
            sqlGetLong(res, row, "inpqty"),
            ptr->prtnum, ptr->prt_client_id,
            ptr->wh_id);
        if (!sqlIsNull(res, row, "marcod"))
        {
        sprintf(tempBuf, " and marcod='%s'  ",
            sqlGetString(res, row, "marcod"));
        strcat(sqlbuffer, tempBuf);
        }
        if (!sqlIsNull(res, row, "ovrcod"))
        {
        sprintf(tempBuf, " and ovrcod='%s' ",
            sqlGetString(res, row, "ovrcod"));
        strcat(sqlbuffer, tempBuf);
        }
        if (!sqlIsNull(res, row, "ovramt"))
        {
        sprintf(tempBuf, " and ovramt=%10.2f ",
            sqlGetFloat(res, row, "ovramt"));
        strcat(sqlbuffer, tempBuf);
        }
        if (!sqlIsNull(res, row, "rpqflg"))
        {
        sprintf(tempBuf, " and rpqflg='%d' ",
            sqlGetBoolean(res, row, "rpqflg"));
        strcat(sqlbuffer, tempBuf);
        }
        sqlFreeResults(res);
        res = NULL;

        ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
        if (ret_status != eOK)
        {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return (ret_status);
        }

        res = srvGetResults(CurPtr);
        row = sqlGetRow(res);

        if (ptr->pckqty > sqlGetLong(res, row, "maxavl"))
        {
        if (ErrorOnOverAllocation)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            return (eINT_CANT_OVRALLOC);
        }
        else
        {
            ptr->pckqty = sqlGetLong(res, row, "maxavl");
            ptr->rpqflg = sqlGetBoolean(res, row, "rpqflg");
            ptr->maxavl = sqlGetLong(res, row, "maxavl");
        }
        }
        else
        {
        /* Regardless of if we're over or not...still need to
           save off what the max avl is as well as whether we
           round up or not... */

        ptr->rpqflg = sqlGetBoolean(res, row, "rpqflg");
        ptr->maxavl = sqlGetLong(res, row, "maxavl");
        }

        srvFreeMemory(SRVRET_STRUCT, CurPtr); 
        CurPtr = NULL;
        res = NULL;
    }
    }
    return (eOK);
}

static long sGetResourceCode(RES_ASSOCIATE * resStruct,
                 char *rescod,
                 char *resstoloc,
                 PCKWRK_DATA * pck)
{
    char            sqlBuffer[2000];
    mocaDataRes    *res = NULL;
    mocaDataRow    *row;
    long            ret_status;
    char            tmpbuf[100];
    COL_NAMES_ADDRESS *cna;
    RETURN_STRUCT  *CurPtr = NULL;


    if (!resStruct)
    {
    strncpy(rescod, RESCOD_DEFAULT, RESCOD_LEN);
    return (eOK);
    }

    sprintf(sqlBuffer,
           "get resource code "
           " where restyp = '%s' "
           "   and resval = '%s' "
           "   and wh_id  = '%s' ",
           resStruct->restyp,
           resStruct->value,
           resStruct->wh_id);

    /*
     * Now, loop around the pick work structure, and send in any
     * values that we might already have.  This will save us from
     * having to do a select to get the values from the database.
     */

    for (cna = _Column_Offsets; cna->colName; cna++)
    {
        memset(tmpbuf, 0, sizeof(tmpbuf));
    sprintf(tmpbuf, 
        " and %s = '%s'",
        cna->colName,
        (char *) ((unsigned long) pck + RET_OFFSET(cna->offset)));
    strcat(sqlBuffer, tmpbuf);
    }

    ret_status = srvInitiateInline(sqlBuffer, &CurPtr);
    if (ret_status != eOK)
    {
        if (CurPtr)
            srvFreeMemory(SRVRET_STRUCT, CurPtr);

        misTrc(T_FLOW, "Error getting resource code ");
        return(ret_status);
    }

    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);

    /* 
     *  If an error comes back, raise a BIG flag 
     *  If the rescod coming back is NULL, raise a BIG flag
     */
    if (sqlIsNull(res, row, "rescod"))
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return (eINT_BAD_RESOURCE_CODE);
    }

    misTrimcpy(rescod, sqlGetString(res, row, "rescod"), RESCOD_LEN );
    if (!sqlIsNull(res, row, "stoloc"))
        misTrimcpy(resstoloc, sqlGetString(res, row, "stoloc"), STOLOC_LEN);

    srvFreeMemory(SRVRET_STRUCT, CurPtr);

    return (eOK);
}

static long sLoadDefaultResAssocPolicies(void)
{
    char            sqlbuffer[500];
    long            ret_status;
    mocaDataRes    *res = NULL;
    mocaDataRow    *row;
    RES_ASSOCIATE  *tmp = 0, *tmp1;
    char            restyp[RESTYP_LEN + 1];
    static          firstTime = TRUE;

    /* Get the Resource to Area Code Association Default Polcies */

    if (resAssociate || !firstTime)
    return (eOK);    /* Assume we have the default policies already */

    sprintf(sqlbuffer,
        "select wh_id, polval, rtstr1, rtstr2 "
        "  from poldat_view "
        " where polcod='%s' "
        "   and polvar='%s' "
        " order by wh_id, polval, rtstr1 ",
        POLCOD_ALLOCATE_INV,
        POLVAR_ALLOCINV_RESCOD_DEF);
    ret_status = sqlExecStr(sqlbuffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
    sqlFreeResults(res);
    return (ret_status);
    }

    if (ret_status == eOK)
    {
    row = sqlGetRow(res);
    tmp1 = NULL;
    while (row)
    {
        if (sqlIsNull(res, row, "rtstr1") ||
        sqlIsNull(res, row, "rtstr2") ||
        !misTrimLen(sqlGetString(res, row, "rtstr1"), RTSTR1_LEN) ||
        !misTrimLen(sqlGetString(res, row, "rtstr2"), RTSTR2_LEN) ||
        misTrimLen(sqlGetString(res,
                        row, "rtstr1"), 
               RTSTR1_LEN) > ARECOD_LEN)
        {
        row = sqlGetNextRow(row);
        continue;
        }
        
        memset(restyp, '\0', sizeof(restyp));
            strncpy(restyp, sqlGetString(res, row, "polval"), RESTYP_LEN);

        if (!resAssociate)
        {
        if (!(tmp = (RES_ASSOCIATE *)
              calloc(1, sizeof(RES_ASSOCIATE))))
            return (eNO_MEMORY);
        resAssociate = tmp;
        }
        else
        {
        /* Check if wh_id/area is already in list */
        tmp1 = resAssociate;
        while (tmp1)
        {
            if (!misCiStrncmp(tmp1->arecod,
                      sqlGetString(res, row, "rtstr1"),
                      ARECOD_LEN) &&
                        !misCiStrncmp(tmp1->wh_id,
                      sqlGetString(res, row, "wh_id"),
                      WH_ID_LEN))
                    {
            break;
                    }
            tmp1 = tmp1->next;
        }

                /* 
                 * If the wh_id/area is not in the list, 
                 * create a new node to append to the end of the list
                 */
        if (!tmp1)  
        {
            if (!(tmp->next = (RES_ASSOCIATE *)
              calloc(1, sizeof(RES_ASSOCIATE))))
            {
                trnFreeResAssociate(resAssociate);
            resAssociate = NULL;
            return (eNO_MEMORY);
            }
            tmp = tmp->next;
        }
        }

        if (!tmp1)  /* Means the wh_id/area is not in the list yet */
        {
            misTrimcpy(tmp->wh_id, sqlGetString(res, row, "wh_id"),
               WH_ID_LEN); 
        misTrimcpy(tmp->arecod, sqlGetString(res, row, "rtstr1"),
               ARECOD_LEN);
        misTrimcpy(tmp->restyp, restyp, RESTYP_LEN);
        misTrimcpy(tmp->value, sqlGetString(res, row, "rtstr2"),
               RESTRN_LEN);
        }
        row = sqlGetNextRow(row);
    }
    }
    sqlFreeResults(res);
    firstTime = FALSE;

    return (eOK);
}

static long sWritePickMoves(void)
{
    long            seqnum;
    PICK_MOVS      *pckmovs;
    PICK_MOV       *mov;
    long            ret_status;
    char            rescod[RESCOD_LEN + 1];
    char            resstoloc[STOLOC_LEN + 1];

    ret_status = eOK;
    pckmovs = PckMovPathHead;

    while (pckmovs)
    {
        seqnum = 0;
        if (pckmovs->pckMovPath)
        {
            mov = pckmovs->pckMovPath;
            while (mov)
            {
                memset(rescod, '\0', sizeof(rescod));
                memset(resstoloc, '\0', sizeof(resstoloc));
                ret_status = sGetResourceCode(mov->resAssc, rescod,
                              resstoloc, pckmovs->pckWrk);
                if (ret_status != eOK)
                    return (ret_status);

                strncpy(mov->rescod, rescod, RESCOD_LEN);
                if (strlen(resstoloc))
                    strncpy(mov->stoloc, resstoloc, STOLOC_LEN);

                ret_status = 
                    sqlExecBind("insert into pckmov (cmbcod, seqnum, arecod, "
                        "                    stoloc, rescod, arrqty, "
                        "                    prcqty, wh_id) "
                        " values (:cmbcod, :seqnum, :arecod, "
                        "         :stoloc, :rescod, 0,  "
                        "         0, :wh_id) ",
                        NULL, NULL,
                        BINDSTR("cmbcod", CMBCOD_LEN, pckmovs->cmbcod),
                        "seqnum", COMTYP_INT, sizeof(long), seqnum, 0,
                        BINDSTR("arecod", ARECOD_LEN, mov->arecod),
                        BINDSTR("stoloc", STOLOC_LEN, mov->stoloc),
                        BINDSTR("rescod", RESCOD_LEN, mov->rescod),
                        BINDSTR("wh_id", WH_ID_LEN, mov->wh_id),
                        NULL);
                        
                if (ret_status != eOK)
                    return (ret_status);
                mov = mov->next;
                seqnum++;
            }
        }
        pckmovs = pckmovs->next;
    }
    return (ret_status);
}

/*
 *  _ParseParams - this functions takes all parameters passed in to
 *                 the allocation inventory routine and parses them
 *                 into a pick work linked list structure.
 *
 *                 The parsing is driven by the segqty parameter...
 *                 which means that if multiple values are specified,
 *                 then the number of values for the rest of the
 *                 parameters must match the number of segqty values.
 *                 (Except for parameters like prtnum, prt_client_id, orgcod,..
 *                  which can only have one value)
 *
 */
static long sParseParams(char *prtnum, char *prt_client_id, 
            long  pckqty, double pck_catch_qty_i,
            char *orgcod, char *revlvl, char *supnum, char *lotnum, 
            char *invsts, char *invsts_prg,
            char *carcod, char *srvlvl,
            char *schbat, char *ship_id, char *ship_line_id,
            char *wkonum, char *wkorev, char *wkolin,
            char *client_id, char *ordnum, 
            char *stcust, char *rtcust,
            char *ordlin, char *ordsln,
            char *concod, char *segqty,
            char *dstare, char *dstloc,
            moca_bool_t  splflg, long  untcas,
            long  untpak, long  untpal,
            long min_shelf_hrs,
            long frsflg,
            char *frsdte,
            char *alc_search_path,
            char *wh_id)
{
    PCKWRK_DATA    *Top, *ptr;
    char           *cptr;
    long            tmpnum;
    char            buffer[1000];
    char            new_concod[CONCOD_LEN + 1];
    mocaDataRes     *res = NULL;
    long            ret_status;
    mocaDataRow     *row;
    char            prt_lodlvl[LODLVL_LEN + 1];
    char            prt_catch_cod[CATCH_COD_LEN + 1];
    long prt_orgflg, prt_lotflg, prt_revflg;
    long            allocate_untcas, allocate_untpak, allocate_untpal;

    /*  Set the allocate_untxxx values from those passed in.  This 
    **  is to clearly identify the values coming from the som/shp detail
    **  records to be allocated instead of the prtmst_view characteristics.
    */
    allocate_untcas = untcas;
    allocate_untpak = untpak;
    allocate_untpal = untpal;

    sprintf(buffer,
        "select catch_cod, lodlvl, "
        "       orgflg, lotflg, revflg "
        "  from prtmst_view where prtnum = '%.*s' "
        "   and prt_client_id = '%.*s'"
        "   and wh_id = '%.*s'",
        PRTNUM_LEN, prtnum, CLIENT_ID_LEN, prt_client_id, WH_ID_LEN, wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        return (eINT_INVALID_PRTNUM);
    }
    row = sqlGetRow(res);
    memset(prt_lodlvl, 0, sizeof(prt_lodlvl));
    strncpy(prt_lodlvl, sqlGetString(res, row, "lodlvl"), LODLVL_LEN);
    memset(prt_catch_cod, 0, sizeof(prt_catch_cod));
    if (!sqlIsNull(res, row, "catch_cod"))
    {
        strncpy(prt_catch_cod, 
                sqlGetString(res, row, "catch_cod"), 
                CATCH_COD_LEN);
    }

    prt_lotflg = sqlGetLong(res, row, "lotflg");
    prt_orgflg = sqlGetLong(res, row, "orgflg");
    prt_revflg = sqlGetLong(res, row, "revflg");
    sqlFreeResults(res);
    res = NULL;

    PickRequestList = (PCKWRK_DATA *) NULL;

    Top = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));
    if (Top == NULL)
    return (eNO_MEMORY);

    cptr = strtok(segqty, CONCAT_CHAR);
    ptr = Top;
    if (cptr == NULL)
    {
        trnFreePickList(Top);
        return (eINVALID_ARGS);
    }
    while (cptr)
    {
        ptr->pckqty = atoi(cptr);
        if (ptr->pckqty <= 0)
        {
            trnFreePickList(Top);
            return (eINVALID_ARGS);
        }
        ptr->pck_catch_qty = pck_catch_qty_i;
        if (wh_id && misTrimLen(wh_id, WH_ID_LEN))
            strncpy(ptr->wh_id, misTrimLR(wh_id), WH_ID_LEN);
        if (prtnum && misTrimLen(prtnum, PRTNUM_LEN))
            strncpy(ptr->prtnum, misTrimLR(prtnum), PRTNUM_LEN);
        if (prt_client_id && misTrimLen(prt_client_id, CLIENT_ID_LEN))
            strncpy(ptr->prt_client_id, misTrimLR(prt_client_id), CLIENT_ID_LEN);
        if (orgcod && misTrimLen(orgcod, ORGCOD_LEN) &&
            strncmp(orgcod, DEFAULT_ORGCOD, strlen(DEFAULT_ORGCOD)) != 0)
            strncpy(ptr->orgcod, misTrimLR(orgcod), ORGCOD_LEN);
        if (lotnum && misTrimLen(lotnum, LOTNUM_LEN) &&
            strncmp(lotnum, DEFAULT_LOTNUM, strlen(DEFAULT_LOTNUM)) != 0)
            strncpy(ptr->lotnum, misTrimLR(lotnum), LOTNUM_LEN);
        if (revlvl && misTrimLen(revlvl, REVLVL_LEN) &&
            strncmp(revlvl, DEFAULT_REVLVL, strlen(DEFAULT_REVLVL)) != 0)
            strncpy(ptr->revlvl, misTrimLR(revlvl), REVLVL_LEN);

        if (supnum && misTrimLen(supnum, SUPNUM_LEN))
            strncpy(ptr->supnum, misTrimLR(supnum), SUPNUM_LEN);

        /* We only want one or the other between the invsts and invsts_prg */
        if (invsts_prg && misTrimLen(invsts_prg, INVSTS_PRG_LEN))
            misTrimcpy(ptr->invsts_prg, invsts_prg, INVSTS_PRG_LEN);
        else if (invsts && misTrimLen(invsts, INVSTS_LEN))
            strncpy(ptr->invsts, misTrimLR(invsts), INVSTS_LEN);

        if (dstare && misTrimLen(dstare, ARECOD_LEN))
            strncpy(ptr->dstare, dstare, ARECOD_LEN);
        if (dstloc && misTrimLen(dstloc, STOLOC_LEN))
            strncpy(ptr->dstloc, dstloc, STOLOC_LEN);
            
        strncpy(ptr->prt_lodlvl, prt_lodlvl, LODLVL_LEN);
        strncpy(ptr->prt_catch_cod, prt_catch_cod, CATCH_COD_LEN);
        ptr->prt_orgflg = prt_orgflg;
        ptr->prt_revflg = prt_revflg;
        ptr->prt_lotflg = prt_lotflg;
        ptr->splflg = splflg;
        ptr->min_shelf_hrs = min_shelf_hrs;
        ptr->frsflg = frsflg;
        if (frsdte && misTrimLen(frsdte, MOCA_STD_DATE_LEN))
            strncpy(ptr->frsdte, frsdte, MOCA_STD_DATE_LEN);
        else
            memset(ptr->frsdte, 0, sizeof(ptr->frsdte));
        
        
        /*  These values are from the order/shipment detail and are customer
        **  requirements.  They identified the need to allocate
        **  specific packaging units instead of the prtmst_view characteristics.
        **  A zero is the default and means that any packaging unit may
        **  be allocated.
        */
        if (allocate_untcas <= 0)
            ptr->alloc_cas = 0;
        else
            ptr->alloc_cas = allocate_untcas;

        if (allocate_untpak <= 0)
            ptr->alloc_pak = 0;
        else
            ptr->alloc_pak = allocate_untpak;

        if (allocate_untpal <= 0)
            ptr->alloc_pal = 0;
        else
            ptr->alloc_pal = allocate_untpal;


        cptr = strtok(NULL, CONCAT_CHAR);
        if (cptr)
        {
            ptr->next = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));
            if (ptr->next == NULL)
            {
                trnFreePickList(Top);
                return (eNO_MEMORY);
            }
            ptr->next->prev = ptr;
            ptr = ptr->next;
        }
    }

    /* Ensure that the sum of all segqty's is equal to the passed in pckqty */
    for (tmpnum = 0, ptr = Top; ptr; tmpnum += ptr->pckqty, ptr = ptr->next) ;

    if (tmpnum != pckqty)
    {
        trnFreePickList(Top);
        return (eINT_QTY_MISMATCH);
    }

    /* At this point, we've established the number of seqments we
       have and that the quantity is OK, now we simply need to
       parse out the rest of the parameters and fill them into their
       appropriate element in the pckwrk structure...  */
       
    if (misTrimLen(schbat, SCHBAT_LEN))
    {
        for (ptr = Top, cptr = strtok(schbat, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
            strncpy(ptr->schbat, misTrimLR(cptr), SCHBAT_LEN);

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }
    else
    {
        sLog(0, 
         "Schedule Batch for pick works will be miscellaneous schbat");
        for (ptr = Top; ptr; ptr = ptr->next)
        {
            /* 
             * Before multiple warehouse, originally the miscellaneous schbat
             * is a constant string "MISC", now we use warehouse id as  
             * the miscellaneous schbat for a specified warehouse.
             */
            char misc_schbat[SCHBAT_LEN + 1];
            memset(misc_schbat, 0, sizeof(misc_schbat));
            misTrimcpy(misc_schbat, ptr->wh_id, SCHBAT_LEN);
            misTrimcpy(ptr->schbat, misc_schbat, SCHBAT_LEN);
        }
    }

    if (misTrimLen(ship_id, SHIP_ID_LEN))
    {
        for (ptr = Top, cptr = strtok(ship_id, CONCAT_CHAR);
             cptr && ptr; 
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            misTrimcpy(ptr->ship_id, cptr, SHIP_ID_LEN);
            if (misTrimLen(ptr->ship_id, SHIP_ID_LEN))
            {
                sprintf(buffer,
                        " select o.bto_seqnum, o.slot "
                        "   from ord o, "
                        "        ord_line ol, "
                        "        shipment_line sl "
                        "  where o.ordnum = ol.ordnum "
                        "    and o.client_id = ol.client_id "
                        "    and o.wh_id = ol.wh_id "
                        "    and sl.ordnum = ol.ordnum "
                        "    and sl.ordlin = ol.ordlin "
                        "    and sl.ordsln = ol.ordsln "
                        "    and sl.client_id = ol.client_id "
                        "    and sl.wh_id = ol.wh_id "
                        "    and sl.ship_id = '%s' ",
                        ptr->ship_id);

                ret_status = sqlExecStr(buffer, &res);
                if(ret_status != eOK)
                {
                    sqlFreeResults(res);
                    trnFreePickList(Top);
                    return ret_status;
                }
                row = sqlGetRow(res);
                if (!sqlIsNull(res, row, "bto_seqnum"))
                    strncpy(ptr->bto_seqnum, sqlGetString(res, row, "bto_seqnum"),BTO_SEQNUM_LEN);
                if (!sqlIsNull(res, row, "slot"))
                    strncpy(ptr->slot, sqlGetString(res, row, "slot"),SLOT_LEN);
                sqlFreeResults(res);
            }
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(ship_line_id, SHIP_LINE_ID_LEN))
    {
        for (ptr = Top, cptr = strtok(ship_line_id, CONCAT_CHAR);
             cptr && ptr; 
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
         {
            misTrimcpy(ptr->ship_line_id, cptr, SHIP_LINE_ID_LEN);

            /* We need to get distro_id by ship_line_id, so we can tell if
             * it is a distro shipment, we won't allow a non-distro shipment
             * allocate distro inventory.
             */
            sprintf(buffer,
                    " select o.bto_seqnum, o.slot, "
                    "        ol.distro_id "
                    "   from ord o, "
                    "        ord_line ol, "
                    "        shipment_line sl "
                    "  where o.ordnum = ol.ordnum "
                    "    and o.client_id = ol.client_id "
                    "    and o.wh_id = ol.wh_id "
                    "    and sl.ordnum = ol.ordnum "
                    "    and sl.ordlin = ol.ordlin "
                    "    and sl.ordsln = ol.ordsln "
                    "    and sl.client_id = ol.client_id "
                    "    and sl.wh_id = ol.wh_id "
                    "    and sl.ship_line_id = '%s' ",
                    ptr->ship_line_id);

            ret_status = sqlExecStr(buffer, &res);
            if(ret_status != eOK)
            {
                sqlFreeResults(res);
                trnFreePickList(Top);
                return ret_status;
            }
            row = sqlGetRow(res);
            if (!sqlIsNull(res, row, "distro_id"))
                misTrimcpy(ptr->distro_id, 
                           sqlGetString(res, row, "distro_id"),
                           DISTRO_ID_LEN);
            if (!sqlIsNull(res, row, "bto_seqnum"))
                strncpy(ptr->bto_seqnum, sqlGetString(res, row, "bto_seqnum"),BTO_SEQNUM_LEN);
            if (!sqlIsNull(res, row, "slot"))
                strncpy(ptr->slot, sqlGetString(res, row, "slot"),SLOT_LEN);
            sqlFreeResults(res);
            res = NULL;
            misTrc(T_FLOW, "shipment_line_id: '%s'; distro_id: '%s' ",
                   ptr->ship_line_id, ptr->distro_id);
         }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(wkonum, WKONUM_LEN))
    {
        for (ptr = Top, cptr = strtok(wkonum, CONCAT_CHAR);
             cptr && ptr; 
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            misTrimcpy(ptr->wkonum, cptr, WKONUM_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(wkorev, WKOREV_LEN))
    {
        for (ptr = Top, cptr = strtok(wkorev, CONCAT_CHAR);
             cptr && ptr; 
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            misTrimcpy(ptr->wkorev, cptr, WKOREV_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(wkolin, WKOLIN_LEN))
    {
        for (ptr = Top, cptr = strtok(wkolin, CONCAT_CHAR);
             cptr && ptr; 
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            misTrimcpy(ptr->wkolin, cptr, WKOLIN_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }


    if (misTrimLen(client_id, CLIENT_ID_LEN))
    {
        for (ptr = Top, cptr = strtok(client_id, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->client_id, misTrimLR(cptr), CLIENT_ID_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(ordnum, ORDNUM_LEN))
    {
        for (ptr = Top, cptr = strtok(ordnum, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->ordnum, misTrimLR(cptr), ORDNUM_LEN);
            if (misTrimLen(ptr->ordnum, ORDNUM_LEN)
                && misTrimLen(ptr->client_id, CLIENT_ID_LEN)
                && misTrimLen(ptr->wh_id, WH_ID_LEN))
            {
                sprintf(buffer,
                        " select bto_seqnum, slot "
                        "   from ord "
                        "  where ordnum = '%s' "
                        "    and client_id = '%s' "
                        "    and wh_id = '%s' ",
                        ptr->ordnum,
                        ptr->client_id,
                        ptr->wh_id);

                ret_status = sqlExecStr(buffer, &res);
                if(ret_status != eOK)
                {
                    sqlFreeResults(res);
                    trnFreePickList(Top);
                    return ret_status;
                }
                row = sqlGetRow(res);
                if (!sqlIsNull(res, row, "bto_seqnum"))
                    strncpy(ptr->bto_seqnum, sqlGetString(res, row, "bto_seqnum"),BTO_SEQNUM_LEN);
                if (!sqlIsNull(res, row, "slot"))
                    strncpy(ptr->slot, sqlGetString(res, row, "slot"),SLOT_LEN);
                sqlFreeResults(res);
                res = NULL;
            }
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(stcust, ADRNUM_LEN))
    {
        for (ptr = Top, cptr = strtok(stcust, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->stcust, misTrimLR(cptr), ADRNUM_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }
    if (misTrimLen(rtcust, ADRNUM_LEN))
    {
        for (ptr = Top, cptr = strtok(rtcust, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->rtcust, misTrimLR(cptr), ADRNUM_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }
    if (misTrimLen(ordlin, ORDLIN_LEN))
    {
        for (ptr = Top, cptr = strtok(ordlin, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->ordlin, misTrimLR(cptr), ORDLIN_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(ordsln, ORDSLN_LEN))
    {
        for (ptr = Top, cptr = strtok(ordsln, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->ordsln, misTrimLR(cptr), ORDSLN_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(concod, CONCOD_LEN))
    {
        for (ptr = Top, cptr = strtok(concod, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->concod, misTrimLR(cptr), CONCOD_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }
    else
    {
        /* Since the user hasn't specified a concod, we'll go ahead
           and generate one and attach it to all requests */

        memset(new_concod, 0, sizeof(new_concod));
        appNextNum(NUMCOD_CONCOD, new_concod);
        for (ptr = Top; ptr; ptr = ptr->next)
        {
            strncpy(ptr->concod, new_concod, CONCOD_LEN);
        }
    }


    if (misTrimLen(carcod, CARCOD_LEN))
    {
        for (ptr = Top, cptr = strtok(carcod, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->carcod, misTrimLR(cptr), CARCOD_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(srvlvl, SRVLVL_LEN))
    {
        for (ptr = Top, cptr = strtok(srvlvl, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->srvlvl, misTrimLR(cptr), SRVLVL_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(dstare, ARECOD_LEN))
    {
        for (ptr = Top, cptr = strtok(dstare, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->dstare, misTrimLR(cptr), ARECOD_LEN);
        }
        
        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (misTrimLen(dstloc, STOLOC_LEN))
    {
        for (ptr = Top, cptr = strtok(dstloc, CONCAT_CHAR);
            cptr && ptr;
            cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->dstloc, misTrimLR(cptr), STOLOC_LEN);
            if(!misTrimLen(ptr->dstare, ARECOD_LEN) &&
                misTrimLen(ptr->dstloc, STOLOC_LEN))
            {
                /* We got a location but not an area so go get the area */
                sprintf(buffer,
                        "select arecod from locmst "
                        " where stoloc = '%s' and wh_id = '%s'",
                        ptr->dstloc, ptr->wh_id);

                ret_status = sqlExecStr(buffer, &res);
                if(ret_status != eOK)
                {
                    sqlFreeResults(res);
                    trnFreePickList(Top);
                    return ret_status;
                }
                row = sqlGetRow(res);
                misTrimcpy(ptr->dstare, sqlGetString(res, row, "arecod"),
                           ARECOD_LEN);
                sqlFreeResults(res);
                res = NULL;
            }
        }
        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    if (alc_search_path && misTrimLen(alc_search_path, ALC_SEARCH_PATH_LEN))
    {
        for (ptr = Top, cptr = strtok(alc_search_path, CONCAT_CHAR);
             cptr && ptr;
             cptr = strtok(NULL, CONCAT_CHAR), ptr = ptr->next)
        {
            strncpy(ptr->alc_search_path, misTrimLR(cptr), ALC_SEARCH_PATH_LEN);
        }

        if (ptr || cptr)
        {
            trnFreePickList(Top);
            return (eINT_PARM_MISMATCH);
        }
    }

    /* Finally, get the frsdte for anything that doesn't already have it */
    for (ptr = Top; ptr; ptr = ptr->next)
    {
        if (ptr->frsflg && !strlen(ptr->frsdte))
        {
            ret_status = trnGetCstFrsDte(ptr->prtnum,
                                         ptr->client_id,
                                         ptr->stcust,
                                         ptr->rtcust,
                                         ptr->frsdte,
                                         ptr->wh_id);
            if (eOK != ret_status)
            {
                return(ret_status);
            }
        }
    }

    PickRequestList = Top;
    return (eOK);
}


/*
 *  Write the unallocated requests to the repleninshment work table
 */
static long sWriteReplenWork(PCKWRK_DATA * List, 
                             char *pcksts, 
                             char *pcksts_uom, 
                             moca_bool_t splflg,
                             moca_bool_t *pipflg_i)
{
    PCKWRK_DATA    *ptr;
    char            sqlcommand[4000];
    char            rplref[RPLREF_LEN + 1];
    long            ret_status;
    char            pick_status[PCKSTS_LEN + 1];
    char orgcod_str[ORGCOD_LEN+10];
    char lotnum_str[LOTNUM_LEN+10];
    char revlvl_str[REVLVL_LEN+10];
    char supnum_str[SUPNUM_LEN+10];

    if (!List)
    {
        sLog(0, "No replenishments need to be created");
        return (eOK);
    }

    memset(pick_status, 0, sizeof(pick_status));
    memset(pick_status, 0, sizeof(pick_status));

    if (pcksts && misTrimLen(pcksts, PCKSTS_LEN))
        strncpy(pick_status, pcksts, PCKSTS_LEN);
    else
        strncpy(pick_status, PCKSTS_PENDING, PCKSTS_LEN);

    if (IS_SET(List->revlvl))
        sprintf(revlvl_str, "'%s'", List->revlvl);
    else
        sprintf(revlvl_str, "NULL");

    if (IS_SET(List->lotnum))
        sprintf(lotnum_str, "'%s'", List->lotnum);
    else
        sprintf(lotnum_str, "NULL");

    if (IS_SET(List->orgcod))
        sprintf(orgcod_str, "'%s'", List->orgcod);
    else
        sprintf(orgcod_str, "NULL");

    if (IS_SET(List->supnum))
        sprintf(supnum_str, "'%s'", List->supnum);
    else
        sprintf(supnum_str, "NULL");

    /* Spin through the Pick Request List because it contains
       all the inventory remaining to be picked.  Anything left
       in the list at this point will not be allocated in this
       round. */
    for (ptr = List; ptr; ptr = ptr->next)
    {
        if (ptr->pckqty > 0)
        {
            memset(rplref, 0, sizeof(rplref));
            appNextNum(NUMCOD_RPLREF, rplref);

            /*
             * Determine pick status for the lodlvl if pcksts specifies load
             * levels
             */ 
            if (pcksts_uom != NULL && misTrimLen(pcksts_uom, LODLVL_LEN) != 0)
            {
                if (strstr(pcksts_uom, ptr->lodlvl) != NULL)
                    strncpy(pick_status, PCKSTS_PENDING, PCKSTS_LEN);
                else
                    strncpy(pick_status, PCKSTS_HOLD, PCKSTS_LEN);
            }

            /* Insert the data into the replenishment work table */
            ret_status =
                sqlExecBind(
                " insert into rplwrk  "
                "    (wh_id, schbat, dstare, dstloc, "
                "     ship_line_id, ship_id, wkonum, wkorev, "
                "     wkolin, client_id,stcust, rtcust, concod, "
                "     pckqty, pck_catch_qty, rplcnt, "
                "     rplsts, prtnum, prt_client_id, "
                "     alcqty, alc_catch_qty, "
                "     untcas, untpak,"
                "     rplref, "
                "     revlvl, supnum, lotnum, orgcod, invsts, "
                "     invsts_prg, "
                "     ordnum, ordlin, ordsln, "
                "     frsflg, min_shelf_hrs, "
                "     adddte, mod_usr_id, pcksts, splflg) "
                "  values(:wh_id, :schbat, :dstare, :dstloc, "
                "         :ship_line_id, :ship_id, "
                "         :wkonum, :wkorev, :wkolin, :client_id, "
                "         :stcust, :rtcust, :concod, "
                "         :pckqty, :pck_catch_qty, :rplcnt, "
                "         :rplsts, :prtnum, :prt_client_id, "
                "         :alcqty, :alc_catch_qty, "
                "         :untcas, :untpak,"
                "         :rplref, "
                "         :revlvl, :supnum, :lotnum, :orgcod, :invsts, "
                "         :invsts_prg, "
                "         :ordnum, :ordlin, :ordsln, "
                "         :frsflg, :min_shelf_hrs, "
                "         sysdate, 'ALLOC', :pcksts, :splflg) ",
                NULL, NULL,
                BINDSTR("wh_id", WH_ID_LEN, ptr->wh_id),
                BINDSTR("schbat", SCHBAT_LEN, ptr->schbat),
                BINDSTR("dstare", ARECOD_LEN, ptr->dstare),
                BINDSTR("dstloc", STOLOC_LEN, ptr->dstloc),
                BINDSTR("ship_line_id",SHIP_LINE_ID_LEN,ptr->ship_line_id),
                BINDSTR("ship_id",SHIP_ID_LEN,ptr->ship_id),
                BINDSTR("wkonum", WKONUM_LEN, ptr->wkonum),
                BINDSTR("wkorev", WKOREV_LEN, ptr->wkorev),
                BINDSTR("wkolin", WKOLIN_LEN, ptr->wkolin),
                BINDSTR("client_id", CLIENT_ID_LEN, ptr->client_id),
                BINDSTR("stcust", STCUST_LEN, ptr->stcust),
                BINDSTR("rtcust", STCUST_LEN, ptr->rtcust),
                BINDSTR("concod", CONCOD_LEN, ptr->concod),
                "pckqty", COMTYP_INT, sizeof(long), ptr->pckqty, 0,
                "pck_catch_qty", COMTYP_FLOAT, 
                    sizeof(double), ptr->pck_catch_qty, 0,
                "rplcnt", COMTYP_INT, sizeof(long), 1, 0,
                BINDSTR("rplsts", RPLSTS_LEN, RPLSTS_ISSUED),
                BINDSTR("schbat", SCHBAT_LEN, ptr->schbat),
                BINDSTR("prtnum", PRTNUM_LEN, ptr->prtnum),
                BINDSTR("prt_client_id",CLIENT_ID_LEN,ptr->prt_client_id),
                "alcqty", COMTYP_INT, sizeof(long), 0, 0,
                "alc_catch_qty", COMTYP_FLOAT, sizeof(double), 0.0, 0,
                "untcas", COMTYP_INT, sizeof(long), ptr->alloc_cas, 0,
                "untpak", COMTYP_INT, sizeof(long), ptr->alloc_pak, 0,
                BINDSTR("rplref", RPLREF_LEN, rplref),
                BINDSTR("revlvl", REVLVL_LEN, ptr->revlvl),
                BINDSTR("supnum", SUPNUM_LEN, ptr->supnum),
                BINDSTR("lotnum", LOTNUM_LEN, ptr->lotnum),
                BINDSTR("orgcod", ORGCOD_LEN, ptr->orgcod),
                BINDSTR("invsts", INVSTS_LEN, ptr->invsts),
                BINDSTR("invsts_prg", INVSTS_PRG_LEN, ptr->invsts_prg),
                BINDSTR("ordnum", ORDNUM_LEN, ptr->ordnum),
                BINDSTR("ordlin", ORDLIN_LEN, ptr->ordlin),
                BINDSTR("ordsln", ORDSLN_LEN, ptr->ordsln),
                "frsflg", COMTYP_INT, sizeof(long), ptr->frsflg, 0,
                "min_shelf_hrs", COMTYP_INT, sizeof(long), 
                    (ptr->min_shelf_hrs > 0 ? ptr->min_shelf_hrs : 0), 0,
                BINDSTR("pcksts", PCKSTS_LEN, pick_status),
                "splflg", COMTYP_INT, sizeof(long), splflg, 0, 0);
            if (ret_status != eOK)
            {
                return (ret_status);
            }

            /* Update the line status to inprocess so the
               release program does not pick up these lines
               again. */
            memset(sqlcommand, '\0', sizeof(sqlcommand));
            /*  Start Here */
            if (strlen(ptr->ship_line_id))
            {
                sprintf(sqlcommand,
                    " change shipment line status  "
                    "  where ship_line_id = '%s' "
                    "    and linsts = '%s' ",
                    ptr->ship_line_id,
                    LINSTS_INPROCESS);
            
                ret_status = srvInitiateCommand(sqlcommand, NULL);
            
                if (ret_status != eOK)
                {
                    return (ret_status);
                }
                sLog(0, "Updated shipment line status to: %s", 
                    LINSTS_INPROCESS);
                    
                /* Update the shipment as well */
                sprintf(sqlcommand, 
                    " change shipment status "
                    "  where ship_id = '%s'  "
                    "    and shpsts = '%s'   ",
                    ptr->ship_id,
                    SHPSTS_IN_PROCESS);

                ret_status = srvInitiateCommand (sqlcommand, NULL);
            
                if (ret_status != eOK)
                {
                    return (ret_status);
                }
                sLog(0, 
                    "Updated shipment status to: %s", 
                    SHPSTS_IN_PROCESS);
            }
            else if (strlen(ptr->wkonum))
            {
                sprintf(sqlcommand,
                    "update wkodtl "
                    "   set linsts = '%s' "
                    " where wkonum = '%s' "
                    "   and client_id = '%s' "
                    "   and wkorev = '%s' "
                    "   and seqnum = 0 "
                    "   and wkolin = '%s' "
                    "   and wh_id  = '%s' ",
                    WKO_LINSTS_INPROCESS,
                    ptr->wkonum, ptr->client_id, 
                    ptr->wkorev, ptr->wkolin, ptr->wh_id);
                
                ret_status = sqlExecStr(sqlcommand, NULL);
                if (ret_status != eOK)
                {
                    return (ret_status);
                }
                sLog(0, "Updated work order line status to: %s", 
                    WKO_LINSTS_INPROCESS);
                sprintf(sqlcommand,
                    "update wkohdr "
                    "   set wkosts = '%s' "
                    " where wkonum = '%s' "
                    "   and client_id = '%s' "
                    "   and wkorev = '%s' "
                    "   and wh_id  = '%s' ",
                    WKOSTS_INPROCESS,
                    ptr->wkonum, 
                    ptr->client_id, 
                    ptr->wkorev,
                    ptr->wh_id);
                
                ret_status = sqlExecStr(sqlcommand, NULL);
                if (ret_status != eOK)
                {
                    return (ret_status);
                }
                sLog(0, "Updated work order status to: %s", 
                     WKOSTS_INPROCESS);
            }
        }
        else
        {
            sLog(0, "Was about to create a zero or negative rplwrk!");
            sLog(0, 
                "SchBat %s, Ship_line_id %s, Wh_Id %s,"
                "ClientId %s, OrdNum %s",
                ptr->schbat, ptr->ship_line_id, ptr->wh_id,
                ptr->client_id, ptr->ordnum);
            sLog(0, 
                "OrdLin %s, OrdSln %s, DstAre %s, DstLoc %s, ",
                ptr->ordlin, ptr->ordsln, ptr->dstare, ptr->dstloc);
            sLog(0,
                "PrtNum %s, OrgCod %s, RevLvl %s, SupNum %s, LotNum %s, "
                "PrtClient_id %s, InvSts %s, PckQty %ld ",
                ptr->prtnum, ptr->orgcod, ptr->revlvl, ptr->supnum, ptr->lotnum,
                ptr->prt_client_id, ptr->invsts, ptr->pckqty);
            sLog(0, 
                "wkonum %s, wkorev %s, wkolin %s, client_id %s, ",
                ptr->wkonum, ptr->wkorev, ptr->wkolin, ptr->client_id);
            return (eERROR);
        }
    }
    sLog(0, "Created replenishments");
    return (eOK);
}

static long sCopyOriginalOrder()
{
    ORIG_ORDER     *tmp;
    PCKWRK_DATA    *pck;

    if (OriginalOrderList)
    sFreeOriginalOrderList();

    OriginalOrderList = NULL;

    for (pck = PickRequestList; pck; pck = pck->next)
    {
    tmp = (ORIG_ORDER *) calloc(1, sizeof(ORIG_ORDER));
    if (!tmp)
    {
        sFreeOriginalOrderList();
        return (eNO_MEMORY);
    }

    strncpy(tmp->wh_id, pck->wh_id, WH_ID_LEN);
    strncpy(tmp->ordnum, pck->ordnum, ORDNUM_LEN);
    strncpy(tmp->client_id, pck->client_id, CLIENT_ID_LEN);
    strncpy(tmp->ordlin, pck->ordlin, ORDLIN_LEN);
    strncpy(tmp->ordsln, pck->ordsln, ORDSLN_LEN);
    strncpy(tmp->ship_id, pck->ship_id, SHIP_ID_LEN);
    strncpy(tmp->ship_line_id, pck->ship_line_id, SHIP_LINE_ID_LEN);

    strncpy(tmp->wkonum, pck->wkonum, WKONUM_LEN);
    strncpy(tmp->wkorev, pck->wkorev, WKOREV_LEN);
    strncpy(tmp->wkolin, pck->wkolin, WKOLIN_LEN);

    tmp->ordqty = pck->pckqty;

    if (OriginalOrderList == NULL)
        OriginalOrderList = tmp;
    else
    {
        tmp->next = OriginalOrderList;
        OriginalOrderList = tmp;
    }
    }
    return (eOK);
}

/* sFindOrigOrder - finds matching order in OrigOrderList */
static ORIG_ORDER *sFindOrigOrder(PCKWRK_DATA * ptr)
{
    ORIG_ORDER     *ord;

    for (ord = OriginalOrderList; ord; ord = ord->next)
    {
    if (strlen(ptr->ship_id))
    {
        if (strncmp(ord->wh_id,     ptr->wh_id,     WH_ID_LEN) == 0     &&
        strncmp(ord->ordnum,    ptr->ordnum,    ORDNUM_LEN) == 0    &&
        strncmp(ord->client_id, ptr->client_id, CLIENT_ID_LEN) == 0 &&
        strncmp(ord->ordlin,    ptr->ordlin,    ORDLIN_LEN) == 0    &&
        strncmp(ord->ordsln,    ptr->ordsln,    ORDSLN_LEN) == 0    &&
        strncmp(ord->ship_id,   ptr->ship_id,   SHIP_ID_LEN) == 0)
        {
        return (ord);
        }
    }
    else if (strlen(ptr->wkonum))
    {
        if (strncmp(ord->wh_id,  ptr->wh_id,  WH_ID_LEN)  == 0 &&
        strncmp(ord->wkonum, ptr->wkonum, WKONUM_LEN) == 0 &&
        strncmp(ord->wkorev, ptr->wkorev, WKOREV_LEN) == 0 &&
        strncmp(ord->wkolin, ptr->wkolin, WKOLIN_LEN) == 0 &&
        strncmp(ord->client_id, ptr->client_id, CLIENT_ID_LEN) == 0)
        {
        return(ord);
        }
    }
    }
    return (NULL);
    
}

/* sIsPickPrunable - this routine evaluates whether a PCKWRK_DATA
 *                   element and all items attached to it via the
 *                   cmbcod is "prunable".  I.e. if getting rid of
 *                   it would still satisfy the order request
 *
 *  This routine is recursive.  On a false condition, we return
 *  FALSE immediately, otherwise, we find the next cmbcod in line
 *  and call ourselves with it.  Once we get to the end of the
 *  cmbcod list, we return TRUE - this means we have evaluated all
 *  items and determined that this "item" is indeed prunable.
 *
 */
static long sIsPickPrunable(PCKWRK_DATA * ptr)
{
    ORIG_ORDER     *ord;
    PCKWRK_DATA    *tmpptr;

    /* First find the the original order information for this
       order.  This will tell us what was originally ordered and
       how much we have picked. */
    ord = sFindOrigOrder(ptr);
    if (!ord)
    return (long)(1 == 0);    /* False */

    if (ord->pckqty - ptr->pckqty >= ord->ordqty)
    {
    /* OK..the passed in ptr looks OK for pruning, next we
       must consider anything attached to it via a cmbcod */
    for (tmpptr = ptr->next; tmpptr; tmpptr = tmpptr->next)
    {
        if (strncmp(ptr->cmbcod, tmpptr->cmbcod, CMBCOD_LEN) == 0)
        break;
    }
    /* When we get out the the for loop above, if we have a tmpptr,
       then we have a matching cmbcod - call ourselves to repeat this
       evaluation for that PCKWRK_DATA item. If we don't have a
       tmpptr, then that means we are done evaluating the cmbcod,
       so we return TRUE  (1==1) */
    if (tmpptr)
        return (sIsPickPrunable(tmpptr));
    else
        return (long)(1 == 1);    /* True */
    }
    else
    {
    /* If the quantity set for picking on the order, less the
       quantity for this pick is not >= the ordered quantity,
       then we cannot prune this pick (it would drop us below
       our limit) */
    return (long)(1 == 0);    /* False */
    }
    /* Pretty tough to imagine getting to here... */
    return (long)(1 == 0);        /* False */

}

static void sRemovePick(PCKWRK_DATA * Pick)
{
    PCKWRK_DATA    *ptr, *tmp;
    ORIG_ORDER     *ord;

    ptr = Pick;
    while (ptr)
    {
    /* Find the order so that we can decrement the found quantities */
    ord = sFindOrigOrder(ptr);
    if (!ord)
        return;        /* we're helpless here... */

    sLog(0,
         "Remove: WH: %s, ORD: %s, Cli: %s, SID: %s, LIN: %s, SLN: %s",
         ord->wh_id, ord->ordnum, ord->client_id, ord->ship_id,
         ord->ordlin, ord->ordlin);
    sLog(0,
         " WKO: %s, LIN: %s, REV: %s",
         ord->wkonum, ord->wkolin, ord->wkorev);
    sLog(0,
         "Line ordered: %d, had %d, now has: %d",
         ord->ordqty, ord->pckqty, (ord->pckqty - ptr->pckqty));
    
    ord->pckqty -= ptr->pckqty;
    /* Next we find the next pick with this cmbcod to process */
    for (tmp = ptr->next; tmp; tmp = tmp->next)
        if (strncmp(tmp->cmbcod, ptr->cmbcod, CMBCOD_LEN) == 0)
        break;
    /* tmp is set to our next removal item. */

    if (PickFoundList == ptr)
    {
        PickFoundList = ptr->next;
        if (PickFoundList)
        PickFoundList->prev = NULL;
    }
    else
    {
        if (ptr->next)
        ptr->next->prev = ptr->prev;
        if (ptr->prev)
        ptr->prev->next = ptr->next;
    }
    free(ptr);
    ptr = tmp;
    }
    return;
}

static void sPrunePicks()
{
    PCKWRK_DATA    *ptr, *tmp;
    ORIG_ORDER     *ord;

    if (!PickFoundList)
    return;

    if (!OriginalOrderList)
    return;

    sLog(0, "Attempting to prune pick list...");

    /* First clear out any data that was already set... */
    for (ord = OriginalOrderList; ord; ord = ord->next)
    ord->pckqty = 0;

    /* Next go through and establish what has been found
       so far... */
    for (ptr = PickFoundList; ptr; ptr = ptr->next)
    {
    ord = sFindOrigOrder(ptr);
    if (ord)
        ord->pckqty += ptr->pckqty;
    /* Clear any prcflg which may be set... */
    ptr->processed = 0;
    }


    /* Now..we can't do the normal for loop type processing
       here as we may be blowing items away as we go...must
       resort to a bit of manual list control.. */
    ptr = PickFoundList;
    while (ptr)
    {
    if (ptr->processed)
    {
        ptr = ptr->next;
        continue;
    }

    /* We mark this as processed regardless of
       whether we prune or not.  The cmbcod is
       evaluated as a unit.  Though it does get
       removed if it is prunable, doing this here
       could firewall us from any problems we encounter
       while removing the pick */
    ptr->processed = 1;
    for (tmp = ptr->next; tmp; tmp = tmp->next)
    {
        if (strncmp(ptr->cmbcod, tmp->cmbcod, CMBCOD_LEN) == 0)
        tmp->processed = 1;
    }

    if (sIsPickPrunable(ptr))
    {
        sLog(0, "Found a pick to prune, cmdcod: %s", ptr->cmbcod);
        /* Before we go pruning, we have to determine what the
           next pick in line to process should be, get that pointer
           and set it prior to any pruning. */
        for (tmp = ptr->next; tmp; tmp = tmp->next)
        if (strncmp(tmp->cmbcod, ptr->cmbcod, CMBCOD_LEN) != 0)
            break;    /* Found next candidate */

        /* tmp is now set to the next candidate for processing...
           we'll use that as soon as we complete our "prune" */
        sRemovePick(ptr);

        ptr = tmp;
    }
    else
    {
        ptr = ptr->next;
    }
    }

}

static void sSetPickQualifiers(PCKWRK_DATA *pick, 
                               mocaDataRes *res)
{
    mocaDataRow *row;
    PCKWRK_DATA *p;
    long i;
    long first_time = 1;
    char *colnam;

    sLog(0, 
         "FORCING INVENTORY ATTRIBUTES FOR REMAINING "
         "DEMAND BASED ON INVENTORY MIXING RULES AT DESTINATION!");

    row = sqlGetRow(res);

    for (p = PickRequestList; p; p = p->next)
    {
        for (i = 0; i < sqlGetNumColumns(res); i++)
        {
            colnam = sqlGetColumnName(res, i);
            
            if (misCiStrncmp(colnam, "lotnum", 6) == 0)
            {
                if (strlen(p->lotnum) == 0 ||
                    strncmp(p->lotnum, DEFAULT_LOTNUM, LOTNUM_LEN) == 0)
                {
                    strncpy(p->lotnum, 
                            sqlGetStringByPos(res, row, i), LOTNUM_LEN);
                }
            }
            else if (misCiStrncmp(colnam, "orgcod", 6) == 0)
            {
                if (strlen(p->orgcod) == 0 ||
                    strncmp(p->orgcod, DEFAULT_ORGCOD, ORGCOD_LEN) == 0)
                {
                    strncpy(p->orgcod, 
                            sqlGetStringByPos(res, row, i), ORGCOD_LEN);
                }
            }
            else if (misCiStrncmp(colnam, "revlvl", 6) == 0)
            {
                if (strlen(p->revlvl) == 0 ||
                    strncmp(p->revlvl, DEFAULT_REVLVL, REVLVL_LEN) == 0)
                {
                    strncpy(p->revlvl, 
                            sqlGetStringByPos(res, row, i), REVLVL_LEN);
                }
            }
            else if (misCiStrncmp(colnam, "supnum", 6) == 0)
            {
                if (strlen(p->supnum) == 0)
                {
                    strncpy(p->supnum, 
                            sqlGetStringByPos(res, row, i), SUPNUM_LEN);
                }
            }
            else if (misCiStrncmp(colnam, "invsts", 6) == 0)
            {
                if (strlen(p->invsts) == 0)
                {
                    strncpy(p->invsts, 
                            sqlGetStringByPos(res, row, i), INVSTS_LEN);
                }
            }
            else if (misCiStrncmp(colnam, "untcas", 6) == 0)
            {
                if (p->untcas == 0)
                    p->untcas = sqlGetLongByPos(res, row, i);
            }
            else if (misCiStrncmp(colnam, "untpak", 6) == 0)
            {
                if (p->untpak == 0)
                    p->untpak = sqlGetLongByPos(res, row, i);
            }
        }
    }

    /* Now fix up the pick */
    for (i = 0; i < sqlGetNumColumns(res); i++)
    {
        colnam = sqlGetColumnName(res, i);
            
        if (misCiStrncmp(colnam, "lotnum", 6) == 0)
        {
            if (strlen(pick->lotnum) == 0 ||
                strncmp(pick->lotnum, DEFAULT_LOTNUM, LOTNUM_LEN) == 0)
            {
                strncpy(pick->lotnum, 
                        sqlGetStringByPos(res, row, i), LOTNUM_LEN);
                sLog(0, " :::Requiring Lotnum: %s", pick->lotnum);
            }
        }
        else if (misCiStrncmp(colnam, "orgcod", 6) == 0)
        {
            if (strlen(pick->orgcod) == 0 ||
                strncmp(pick->orgcod, DEFAULT_ORGCOD, ORGCOD_LEN) == 0)
            {
                strncpy(pick->orgcod, 
                        sqlGetStringByPos(res, row, i), ORGCOD_LEN);
                sLog(0, " :::Requiring Orgcod: %s", pick->orgcod);
            }
        }
        else if (misCiStrncmp(colnam, "revlvl", 6) == 0)
        {
            if (strlen(pick->revlvl) == 0 ||
                strncmp(pick->revlvl, DEFAULT_REVLVL, REVLVL_LEN) == 0)
            {
                strncpy(pick->revlvl, 
                        sqlGetStringByPos(res, row, i), REVLVL_LEN);
                sLog(0, " :::Requiring Revlvl: %s", pick->revlvl);
            }
        }
        else if (misCiStrncmp(colnam, "supnum", 6) == 0)
        {
            if (strlen(pick->supnum) == 0)
            {
                strncpy(pick->supnum, 
                        sqlGetStringByPos(res, row, i), SUPNUM_LEN);
                sLog(0, " :::Requiring supnum: %s", pick->supnum);
            }
        }
        else if (misCiStrncmp(colnam, "invsts", 6) == 0)
        {
            if (strlen(pick->invsts) == 0)
            {
                strncpy(pick->invsts, 
                        sqlGetStringByPos(res, row, i), INVSTS_LEN);
                sLog(0, " :::Requiring Invsts: %s", pick->invsts);
            }
        }
        else if (misCiStrncmp(colnam, "untcas", 6) == 0)
        {
            if (pick->untcas == 0)
            {
                pick->untcas = sqlGetLongByPos(res, row, i);
                sLog(0, " :::Requiring untcas: %d", pick->untcas);
            }
        }
        else if (misCiStrncmp(colnam, "untpak", 6) == 0)
        {
            if (pick->untpak == 0)
            {
                pick->untpak = sqlGetLongByPos(res, row, i);
                sLog(0, " :::Requiring untpak: %d", pick->untpak);
            }
        }
    }

    return;
}

static void sSetRequiredInventoryOptions(char *dstare, 
                                         char *wh_id,
                                         PCKWRK_DATA *pick)
{
    char buffer[4000];
    long ret_status;
    RETURN_STRUCT *CmdRes = NULL;
    mocaDataRes *res = NULL;
    mocaDataRow *row;
    char distinctBuffer[2000];

    /* First get mixing rules at the destination */
    sprintf(buffer,
            "get inventory mixing rule where arecod = '%s' and wh_id = '%s'",
            dstare, wh_id);
    ret_status = srvInitiateInline(buffer, &CmdRes);
    if (ret_status != eOK)
    {
        sLog(0, 
             "Unable to get mixing rules (%d) - ignoring",
             ret_status);
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        return;
    }
    res = srvGetResults(CmdRes);
    row = sqlGetRow(res);
    memset(distinctBuffer, 0, sizeof(distinctBuffer));
    if (sqlIsNull(res, row, "selectlist"))
    {
        sLog(0, "No select list from mixing rule - getting out");
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        return;
    }
    strcpy(distinctBuffer, sqlGetString(res, row, "selectlist"));
        
    
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    CmdRes = NULL;
    res = NULL;

    sprintf(buffer,
            "select distinct %s "
            "  from prtmst_view, invdtl, invsub, invlod "
            " where invlod.stoloc = '%s' "
            "   and invlod.wh_id  = '%s' "
            "   and invlod.lodnum = invsub.lodnum "
            "   and invlod.wh_id  = prtmst_view.wh_id "
            "   and invdtl.prtnum = prtmst_view.prtnum "
            "   and invdtl.prt_client_id = prtmst_view.prt_client_id "
            "   and invsub.subnum = invdtl.subnum "
            "   and invdtl.prtnum = '%s' "
            "   and invdtl.prt_client_id = '%s'",
            distinctBuffer, 
            pick->srcloc,
            pick->wh_id,
            pick->prtnum,
            pick->prt_client_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        sLog(0,
             "  -- error selecting distinct attribute - forced to "
             " skip qualifying pick ");
        sqlFreeResults(res);
        return;
    }
    if (sqlGetNumRows(res) > 1)
    {
        sLog(0,
             "  -- MIXED ATTRIBUTE LOCATION selected - FORCED to "
             " skip qualifying pick ");
        sqlFreeResults(res);
        return;
    }

    sSetPickQualifiers(pick, res);
    sqlFreeResults(res);
    return;
}

static void sMarkFirstPick(char *pcktyp)
{

    if ((strcmp(pcktyp, ALLOCATE_REPLENISH) == 0 ||
         strcmp(pcktyp, ALLOCATE_INVENTORY_MOVE) == 0 ||
         strcmp(pcktyp, ALLOCATE_TOPOFF_REPLEN) == 0) &&
        strlen(PickFoundList->dstare) > 0)
    {
        sSetRequiredInventoryOptions(PickFoundList->dstare,
                                     PickFoundList->wh_id,
                                     PickFoundList);
    }
    return;
}

/*
 * This command is called by IsLocationFitInventory function
 * it's used to get the footprint data by the given pick work
 *  Params:
 *      Ptr     -   Pick work (in)
 *      caslen  -   case length (out)
 *      cashgt  -   case height (out)
 *      caswid  -   case width  (out)
 *      caslvl  -   case levels (out)
 */
static void sGetFootprint(PCKWRK_DATA * ptr,
                          double * caslen,
                          double * cashgt,
                          double * caswid,
                          long * caslvl)
{
    long            ret_status;
    char            buffer[500];
    char ftpcod[FTPCOD_LEN + 1];
    mocaDataRow    *row = NULL;
    mocaDataRes    *res = NULL;

    *caslen = 0.0;
    *cashgt = 0.0;
    *caswid = 0.0;
    *caslvl = 0L;
    memset (ftpcod, 0, sizeof(ftpcod));
    misTrimcpy (ftpcod, ptr->ftpcod, FTPCOD_LEN);
    
    /*
     *  get the ftpcod for the pick if it's empty 
     */
    if (misTrimLen(ftpcod, FTPCOD_LEN) == 0)
    {
        sprintf(buffer,
            " select ftpmst.ftpcod "
            "   from ftpmst, "
            "        invdtl, "
            "        invsub, "
            "        invlod  "
            "  where invdtl.prtnum = '%s' "
            "    and invdtl.prt_client_id = '%s' "
            "    and invdtl.subnum = invsub.subnum "
            "    and invsub.lodnum = invlod.lodnum "
            "    and invlod.stoloc = '%s' "
            "    and invlod.wh_id  = '%s' "
            "    and invdtl.ftpcod = ftpmst.ftpcod "
            "    and rownum < 2 "
            "  order by (ftpmst.untlen*ftpmst.untwid*ftpmst.unthgt) desc",
            ptr->prtnum,
            ptr->prt_client_id,
            ptr->srcloc,
            ptr->wh_id);
        ret_status = sqlExecStr(buffer, &res);

        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(res);
            return;
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            /* If we're doing a pip pick, then there may not be any 
             * inventory in the location, in which case we need to
             * go after the ftpcod of the part.
             */

            sqlFreeResults(res);
            res = NULL;

            sprintf(buffer,
                " select ftpmst.ftpcod "
                "   from ftpmst, "
                "        prtmst_view "
                "  where prtmst_view.prtnum = '%s' "
                "    and prtmst_view.prt_client_id = '%s' "
                "    and prtmst_view.wh_id = '%s' "
                "    and prtmst_view.ftpcod = ftpmst.ftpcod ",
                ptr->prtnum,
                ptr->prt_client_id,
                ptr->wh_id);

            ret_status = sqlExecStr(buffer, &res);
            if (ret_status != eOK)
            {
                sqlFreeResults(res);
                return;
            }
        }

        row = sqlGetRow(res);
        misTrimcpy (ftpcod, sqlGetString(res, row, "ftpcod"), FTPCOD_LEN);
        sqlFreeResults(res);
        res = NULL;
    }

    /*
     * get off the caslen/cashgt/caswid/caslvl for the ftpcod
     */
    sprintf(buffer,
        "select * from ftpmst where ftpcod = '%s'", ftpcod);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        row = sqlGetRow(res);
        *caslen = sqlGetFloat(res, row, "caslen");
        *cashgt = sqlGetFloat(res, row, "cashgt");
        *caswid = sqlGetFloat(res, row, "caswid");
        *caslvl = sqlGetLong(res, row, "caslvl");
    }
    sqlFreeResults(res);
    res = NULL;
}

static void sGetFootprintForPartLocation(char *stoloc,
                                         char *prtnum,
                                         char *prt_client_id,
                                         char *ftpcod)
{
    long        ret_status;
    char        buffer[1000];
    mocaDataRow *row = NULL;
    mocaDataRes *res = NULL;

    sprintf(buffer,
            " select distinct "
            "        invdtl.ftpcod "
            "   from invlod, invsub, invdtl "
            "  where invlod.lodnum = invsub.lodnum "
            "    and invsub.subnum = invdtl.subnum "
            "    and invlod.stoloc = '%s' "
            "    and invdtl.prtnum = '%s' "
            "    and invdtl.prt_client_id = '%s' ",
            stoloc, prtnum, prt_client_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK && sqlGetNumRows(res) == 1)
    {
        /* if only one footprint for part's inventory in this loc use it */
        row = sqlGetRow(res);
        misTrimcpy(ftpcod, sqlGetString(res, row, "ftpcod"), FTPCOD_LEN);
    }
    sqlFreeResults(res);
}

/* PR 56507 and WMD-37067
 * Get asset info such as height and volume
 */
static long sGetAssetDimension(char *asset_typ_i,
                               double *asset_hgt_o,
                               double *asset_vol_o,
                               moca_bool_t *container_flg_o)
{
    static mocaDataRes *assetRes = NULL;
    mocaDataRow *assetRow = NULL;
    
    char buffer[500];
    long ret_status;

    *asset_hgt_o = 0.0;
    *asset_vol_o = 0.0;

    if (!asset_typ_i || !misTrimLen(asset_typ_i, ASSET_TYP_LEN))
        return (eOK);

    if (assetRes == NULL)
    {
        memset(buffer, 0, sizeof(buffer));

        sprintf(buffer,
                " select asset_typ, "
                "        asset_hgt, "
                "        container_flg, " /*Used while calculating total qvl*/
                "        decode(container_flg, 1, max_vol, "
                "               0, (asset_len * asset_wid * asset_hgt)) asset_vol "
                "   from asset_typ");

        ret_status = sqlExecStr(buffer, &assetRes);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(assetRes);
            assetRes = NULL;

            return (ret_status);
        }

        /* Cache the asset types. */
        misFlagCachedMemory((OSFPTR) sqlFreeResults, assetRes);
    }

    for (assetRow=sqlGetRow(assetRes); assetRow; assetRow = sqlGetNextRow(assetRow))
    {
        if (strncmp(asset_typ_i, sqlGetString(assetRes, assetRow, "asset_typ"),
                    ASSET_TYP_LEN) == 0)
        {
            *asset_hgt_o = sqlGetFloat(assetRes, assetRow, "asset_hgt");
            *asset_vol_o = sqlGetFloat(assetRes, assetRow, "asset_vol");
            *container_flg_o = sqlGetBoolean(assetRes, assetRow, "container_flg");

            break;
        }
    }

    return (eOK);
}

/*
 * This function is called by sAssignDstToPick function
 * it will check if the staging location is large enough
 * to contain the inventory.
 *  Params:
 *      arecod  -   the locations' area code
 *      stoloc  -   location code
 *      ptr     -   Pick Work
 *  Return:
 *      eOK     -   if there's enough room to place the inventory
 *      !eOK    -   otherwize.
 */
static long sIsLocationFitInventory(char * arecod,
                                    char * stoloc,
                                    char * wh_id,
                                    PCKWRK_DATA * ptr)
{
    long ret_status;
    
    RETURN_STRUCT * CurPtr = NULL;
    mocaDataRes * res = NULL;
    mocaDataRow * row = NULL;

    char loccod[LOCCOD_LEN + 1];
    long lochgt;
    double maxqvl, pndqvl, curqvl;
    double store_amount;
    double cashgt, caswid, caslen;
    double asset_vol, asset_hgt;
    moca_bool_t container_flg;
    long caslvl;
    char buffer[512];
    PCKWRK_DATA * mptr;

    
    /*
     * if the arecod or the stoloc is empty, then
     * just return true
     *
     * don't check the length of field wh_id, wh_id will never be empty
     */
    if (misTrimLen(arecod, ARECOD_LEN) == 0 ||
        misTrimLen(stoloc, STOLOC_LEN) == 0)
        return eOK;

    memset (loccod, 0, sizeof(loccod));
    memset (buffer, 0, sizeof(buffer));
    lochgt = 0L;

    /* check if PIP is enabled, 
     * if the following query returns eOk 
     * we can return back eOK
     */
    
    sprintf(buffer,
         "select 'x'" 
         "   from poldat_view" 
         "  where polcod = '%s'" 
         "    and polvar = '%s'" 
         "    and polval = '%s'" 
         "    and rtnum1 = %ld"
         "    and wh_id  = '%s'",
         POLCOD_PRE_INV_PICKING,
         POLVAR_INSTALLED,
         POLVAL_INSTALLED,
         BOOLEAN_TRUE,
         wh_id);
    ret_status = sqlExecStr(buffer, NULL);

    if (ret_status == eOK)
    {
        sLog(0, "PIP is enabled, ignoring QVL during allocation");
        misTrc(T_FLOW, "PIP is enabled, ignoring QVL during allocation");
        return ret_status;
    }
    else
    {
        if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return ret_status;
        }
    }

    sprintf(buffer,
            "list areas where arecod = '%s' and wh_id = '%s'", arecod, wh_id);

    ret_status = srvInitiateCommand(buffer, &CurPtr);

    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return ret_status;
    }

    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);

    /*
     *  get the loccod for the area
     */
    misTrimcpy(loccod,
               sqlGetString(res, row, "loccod"),
               LOCCOD_LEN);

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;
    res = NULL;

    /*
     * check the location
     */
    sprintf(buffer, 
            " list locations "
            "where arecod = '%s' "
            "  and stoloc = '%s' "
            "  and wh_id  = '%s' ",
            arecod,
            stoloc,
            wh_id);
    ret_status = srvInitiateCommand(buffer, &CurPtr);

    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return ret_status;
    }

    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);
    
    /* 
     * get the lochgt/maxqvl/pndqvl/curqvl of the location
     */
    lochgt = sqlIsNull(res, row, "lochgt") ? 
                0L: 
                sqlGetLong(res, row, "lochgt");

    maxqvl = sqlGetFloat(res, row, "maxqvl");
    pndqvl = sqlGetFloat(res, row, "pndqvl");
    curqvl = sqlGetFloat(res, row, "curqvl");

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;
    res = NULL;
    
    /*
     * get the inventory's caslen/cashgt/caswid/caslvl
     */
    sGetFootprint(ptr, &caslen, &cashgt, &caswid, &caslvl);

    /* Get the asset height and volume */
    ret_status = sGetAssetDimension(ptr->asset_typ, &asset_hgt, &asset_vol, &container_flg);
    if (ret_status != eOK)
        return (ret_status);
    
    if (lochgt != 0L)
    {
        /*
         * if the loccod is not Pallet, just compare the cashgt and lochgt
         */
        if (strncmp(loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0 ||
            strncmp(loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0 ||
            strncmp(loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
        {
            /*
             * Add container flag also as a parameter since if this is set
             * we will ignore the inventory qvl
             */
            if ((cashgt + asset_hgt > lochgt) && container_flg == BOOLEAN_FALSE)
            {
                sLog(0, "Case height: %f + Asset height: %f "
                        "won't fit in location: %ld",
                        cashgt, asset_hgt, lochgt);
                sLog(0, "Ingore to stage to this location %s/%s",
                        wh_id, stoloc);
                return (eINT_PRODUCT_WONT_FIT_IN_LOCATION);
            }
            else if (asset_hgt > lochgt && container_flg == BOOLEAN_TRUE)
            {
                sLog(0, "Asset height: %f "
                        "won't fit in location: %ld",
                        asset_hgt, lochgt);
                sLog(0, "Ingore to stage to this location %s/%s",
                        wh_id, stoloc);
                return (eINT_PRODUCT_WONT_FIT_IN_LOCATION);
            }
        }
        /*
         * calculate the levels of the inventory and multi to cashgt, compare
         * the result of the lochgt
         */
        else if (caslvl > 0.0 && ptr->untcas > 0.0)
        {
            long levels = 0L, height;
            levels = (long) ceil((double)
                                 (ptr->pckqty / ptr->untcas) / caslvl);
            height = (long) (cashgt * levels);

            /*
             * Add container flag also as a parameter since if this is set
             * we will ignore the inventory qvl
             */
            if ((height + asset_hgt > lochgt) && container_flg == BOOLEAN_FALSE)
            {
                sLog(0, "Pallet height: %ld + Asset height: %f "
                        "won't fit in location: %ld",
                        height, asset_hgt, lochgt);
                return (eINT_PRODUCT_WONT_FIT_IN_LOCATION);
            }
            else if (asset_hgt > lochgt && container_flg == BOOLEAN_TRUE)
            {
                sLog(0, "Asset height: %f "
                        "won't fit in location: %ld",
                        asset_hgt, lochgt);
                return (eINT_PRODUCT_WONT_FIT_IN_LOCATION);
            }
        }
    }

    /* Check QVL value */

    store_amount = 0.0;

    if (strncmp(loccod, LOCCOD_PALLET, LOCCOD_LEN) == 0)
        store_amount = 1;
    else if (strncmp(loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0 && 
             ptr->untcas > 0.0)
    {
        store_amount = (caslen * ((double) ptr->pckqty / ptr->untcas));
    }
    else if (strncmp(loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0 &&
             ptr->untcas > 0.0) 
    {
        store_amount = ((caslen * cashgt * caswid) *
                        ((double) ptr->pckqty / ptr->untcas));

        /* PR 56507 and WMD-37067
         * If ASSET-TRACKING configuration is enabled, and lodlvl is 
         * 'L', and inventory platform is not empty, we need to
         * add this asset volume to the store_amount.
         * WMD-41892
         * As part of substituting asset tracking with platform
         * tracking we have to additionally check for the
         * container_flg of the asset_typ before adding
         * the asset's volume to the store amount.
         */
        if (asset_track_enabled && misTrimLen(ptr->asset_typ, ASSET_TYP_LEN) &&
            strncmp(ptr->lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0 &&
            container_flg == BOOLEAN_FALSE)
        {
            store_amount += asset_vol;
        }
    }
    else if (strncmp(loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0)
    {
        store_amount = ptr->pckqty;
    }

    mptr = PickFoundList;

    while (mptr)
    {
        sGetFootprint(mptr, &caslen, &cashgt, &caswid, &caslvl);

        /* Get the asset height and volume */
        ret_status = sGetAssetDimension(mptr->asset_typ, &asset_hgt, &asset_vol, &container_flg);
        if (ret_status != eOK)
            return (ret_status);

        if (misTrimStrncmp(mptr->dstare, arecod, ARECOD_LEN) == 0 &&
            misTrimStrncmp(mptr->dstloc, stoloc, STOLOC_LEN) == 0 &&
            misTrimStrncmp(mptr->wh_id, wh_id, WH_ID_LEN) == 0)
        {
            if (strncmp(loccod, LOCCOD_PALLET, LOCCOD_LEN) == 0)
                store_amount ++;
            else if (strncmp(loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
            {
                if (mptr->untcas > 0.0)
                {
                    store_amount += (caslen * ((double) 
                                                mptr->pckqty / mptr->untcas));
                }
            }
            else if (strncmp(loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0)
            {
                if (mptr->untcas > 0.0)
                {
                    store_amount += ((caslen * cashgt * caswid) *
                                     ((double) mptr->pckqty / mptr->untcas));

                    /* PR 56507 and WMD-37067
                     * If ASSET-TRACKING configuration is enabled, and lodlvl is 
                     * 'L', and inventory platform is not empty, we need to
                     * add this asset volume to the store_amount.
                     * WMD-41892
                     * As part of substituting asset tracking with platform
                     * tracking we have to additionally check for the
                     * container_flg of the asset_typ before adding
                     * the asset's volume to the store amount.
                     */
                    if (asset_track_enabled && 
                        misTrimLen(mptr->asset_typ, ASSET_TYP_LEN) &&
                        (strncmp(ptr->lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0) &&
                         container_flg == BOOLEAN_FALSE)
                    {
                        store_amount += asset_vol;
                    }
                }
            }
            else if (strncmp(loccod, LOCCOD_EACHES, LOCCOD_LEN) == 0)
            {
                store_amount += mptr->pckqty;
            }
        }
        mptr = mptr->next;
    }

    if (store_amount > (maxqvl - curqvl - pndqvl))
    {
        sLog(0, "Product (and Asset) qvl: %f, location qvl: %f - WON'T FIT",
            store_amount, (maxqvl - curqvl - pndqvl));
        return (eINT_PRODUCT_WONT_FIT_IN_LOCATION);
    }

    return (eOK);

}

/*
 * it's called by trnAllocateInventory function
 * parameters:
 *      ptr     -   PickRequestList
 * if it's not multi-stage line, just set the
 * dstare and dstloc for ptr, and this will be 
 * used while doing replenishment; otherwize
 * the pckAllocateEmergencyReplenishment.c file 
 * handle the multi-staging shipment replenishment.
 */
static void sAssignDstToReplenishPick(PCKWRK_DATA * ptr)
{
    while(ptr)
    {
        SHIP_DST * pdst = (SHIP_DST *)(ptr->reserved);
        if (pdst->next == NULL &&
            misTrimLen(pdst->src_bldg_id, BLDG_ID_LEN) == 0)
        {
            misTrimcpy(ptr->dstare, pdst->dstare, ARECOD_LEN);
            misTrimcpy(ptr->dstloc, pdst->dstloc, STOLOC_LEN);
        }
        ptr = ptr->next;
    }
}

/*
 * it's called by sProcessPalletPick
 *                sProcessCasePick
 *                sProcessPiecePick
 *                sProcessPIPicks
 *                sAllocateLoad
 * this function will assigned the destination location to the pick. 
 * it's called while the pick work is moving to the PickFoundList
 *  Param:
 *      ptr     -   Pick Work
 */
static void sAssignDstToPick(PCKWRK_DATA * ptr)
{
    RETURN_STRUCT * CurPtr = NULL;
    mocaDataRes * res = NULL;
    mocaDataRow * row;
    long ret_status = eOK;
    char buffer[512];
    char src_bldg_id[BLDG_ID_LEN + 1];
    SHIP_DST * dst;

    memset(src_bldg_id, 0, sizeof(src_bldg_id));

    /*
     * get off the dstare by the gaven dstloc
     */
    if (misTrimLen(ptr->dstloc, STOLOC_LEN))
    {
        sprintf(buffer,
                " select arecod "
                "   from locmst "
                "  where stoloc = '%s' "
                "    and wh_id = '%s' ",
                ptr->dstloc,
                ptr->wh_id);

        ret_status = sqlExecStr(buffer, &res);

        if (ret_status == eOK)
        {
            row = sqlGetRow(res);

            misTrimcpy(ptr->dstare,
                       sqlGetString(res, row, "arecod"),
                       ARECOD_LEN);
        }

        sqlFreeResults(res);
        res = NULL;
    }
    else if(misTrimLen(ptr->srcare, ARECOD_LEN) && 
       misTrimLen(ptr->dstare, ARECOD_LEN) == 0 &&
       misTrimLen(ptr->dstloc, STOLOC_LEN) == 0)
    {
        /*
         * dst_bldg_list is the list of the assigned staging locations which
         * has the same source building id with the pick work; nondst_bldg_list
         * is the list of the locations which is not assigned the src_bldg_id
         */
        SHIP_DST    * dst_bldg_list, *nondst_bldg_list;

        dst_bldg_list = NULL;
        nondst_bldg_list = NULL;

        sprintf(buffer,
                "list areas where arecod = '%s' and wh_id = '%s'",
                ptr->srcare,
                ptr->wh_id);
        ret_status = srvInitiateCommand(buffer, &CurPtr);

        if(ret_status == eOK)
        {
            res = srvGetResults(CurPtr);
            row = sqlGetRow(res);
            misTrimcpy(src_bldg_id,
                       sqlGetString(res, row, "bldg_id"),
                       BLDG_ID_LEN);
        }
        if (CurPtr)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            res = NULL;
        }

        if (misTrimLen(src_bldg_id, BLDG_ID_LEN) == 0) return;

        dst = (SHIP_DST *)(ptr->reserved);

        /*
         * first try to find the src_bldg_id equal to the dst->src_bldg_id
         *
         * take care, the dst_bldg_list will contain the list of the 
         * destination area/dstloc peer, it order by the srtseq, so the  
         * first node of it is the place which has small srtseq number, 
         * it means that stage to there is the best choose. if this place 
         * is full, then try to next place.
         */
        while(dst)
        {
            /*
             * get the pick building sequences rows 
             * from the src_bldg_id to bldg_id
             */
            sprintf(buffer,
                    " list pick building sequences "
                    "where src_bldg_id  = '%s' "
                    "  and bldg_id = '%s' "
                    "  and wh_id = '%s' ",
                    src_bldg_id,
                    dst->dst_bldg_id,
                    dst->wh_id);

            ret_status = srvInitiateCommand(buffer, &CurPtr);

            if (ret_status == eOK)
            {
                SHIP_DST * pre_bldg_id, * nxt_bldg_id, *pnew;
                pre_bldg_id = NULL;
                nxt_bldg_id = NULL;
                res = srvGetResults(CurPtr);
                row = sqlGetRow(res);

                pnew = (SHIP_DST *)calloc(1, sizeof(SHIP_DST));
                memcpy(pnew, dst, sizeof(SHIP_DST));
                pnew->next = NULL;
                
                /*
                 * get the srtseq
                 */
                pnew->srtseq = sqlGetLong(res, row, "srtseq");

                /*
                 * if the src_bldg_id is the building of dst->src_bldg_id,
                 * put the row data into the dst_bldg_list, otherwize put it
                 * into the nondst_bldg_list.
                 * at the end,it will concatinate the nondst_bldg_list to the
                 * end of dst_bldg_list, then trying to found a location which 
                 * is large enough to contain the staging inventory.
                 * so this step is to build these two kind of list, and system
                 * will consider the location in dst_bldg_list first, and then 
                 * the nondst_bldg_list.
                 */
                if (misTrimStrncmp(src_bldg_id, 
                                   dst->src_bldg_id, 
                                   BLDG_ID_LEN) == 0)
                {
                    nxt_bldg_id = dst_bldg_list;
                    while(nxt_bldg_id != NULL &&
                          nxt_bldg_id->srtseq <= pnew->srtseq)                        
                    {
                        pre_bldg_id = nxt_bldg_id;
                        nxt_bldg_id = nxt_bldg_id->next;
                    }
                    
                    if (pre_bldg_id == NULL && nxt_bldg_id == NULL)
                        dst_bldg_list = pnew;
                    else if(pre_bldg_id == NULL)
                    {
                        pnew->next = dst_bldg_list;
                        dst_bldg_list = pnew;
                    }
                    else if(nxt_bldg_id == NULL)
                    {
                        pre_bldg_id ->next = pnew;
                    }
                    else
                    {
                        pre_bldg_id->next = pnew;
                        pnew->next = nxt_bldg_id;
                    }

                }
                else if (misTrimLen(dst->src_bldg_id, BLDG_ID_LEN) == 0)
                {
                    nxt_bldg_id = nondst_bldg_list;
                    while(nxt_bldg_id != NULL &&
                          nxt_bldg_id->srtseq <= pnew->srtseq)
                    {
                        pre_bldg_id = nxt_bldg_id;
                        nxt_bldg_id = nxt_bldg_id->next;
                    }

                    if (pre_bldg_id == NULL && nxt_bldg_id == NULL)
                        nondst_bldg_list = pnew;
                    else if(pre_bldg_id == NULL)
                    {
                        pnew->next = nondst_bldg_list;
                        nondst_bldg_list = pnew;
                    }
                    else if(nxt_bldg_id == NULL)
                    {
                        pre_bldg_id->next = pnew;
                    }
                    else
                    {
                        pre_bldg_id->next = pnew;
                        pnew->next = nxt_bldg_id;
                    }
                }

            }
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            res = NULL;
            dst = dst->next;            
        }

        if (dst_bldg_list == NULL)
            dst_bldg_list = nondst_bldg_list;
        else
        {
            dst = dst_bldg_list;
            while(dst->next)
            {
                dst = dst->next;
            }
            dst->next = nondst_bldg_list;
        }

        dst = dst_bldg_list;

        while (dst)
        {
            ret_status = sIsLocationFitInventory(dst->dstare, 
                                                 dst->dstloc,
                                                 dst->wh_id, 
                                                 ptr);

            if (ret_status == eOK)
                break;
            else
            {
                sLog(0, "No Room to stage for the new pick inventory "
                        "at %s/%s staging location", dst->wh_id, dst->dstloc);
            }
            dst = dst->next;
        }

        if (dst == NULL)
        {
            sLog(0, "There is no destination location "
                    "fit the pick inventory!");
            /*
            * if there's no dst_bldg_list fit the pick work
            * we need to call the get order destination to 
            * get the staging area.
            */

            sLog(0, " There's no staging locations defined for this shipment "
                    "from %s/%s source building ",
                    ptr->wh_id, src_bldg_id);

            sprintf (buffer,
                    "  get order destination "
                    "where ship_id = '%s' "
                    "  and ship_line_id = '%s' "
                    "  and wkonum = '%s' "
                    "  and wkolin = '%s' "
                    "  and wkorev = '%s' "
                    "  and client_id  = '%s' "
                    "  and ordnum = '%s' "
                    "  and ordlin = '%s' "
                    "  and ordsln = '%s' "
                    "  and carcod = '%s' "
                    "  and srvlvl = '%s' "
                    "  and marcod = '%s' "
                    "  and ordtyp = '%s' "
                    "  and wh_id  = '%s' ",
                    ptr->ship_id,
                    ptr->ship_line_id,
                    ptr->wkonum,
                    ptr->wkolin,
                    ptr->wkorev,
                    ptr->client_id,
                    ptr->ordnum,
                    ptr->ordlin,
                    ptr->ordsln,
                    ptr->carcod,
                    ptr->srvlvl,
                    ptr->marcod,
                    ptr->ordtyp,
                    ptr->wh_id);
            sLog(0, " We are trying to get the dstare ");

            ret_status = srvInitiateCommand(buffer, &CurPtr);

            if (ret_status == eOK)
            {
                res = srvGetResults(CurPtr);
                row = sqlGetRow(res);

                dst = (SHIP_DST *)calloc(1, sizeof(SHIP_DST));
                memset(dst, 0, sizeof(SHIP_DST));

                dst->next = dst_bldg_list;
                dst_bldg_list = dst;
                dst->parent = ptr;

                misTrimcpy(dst->wh_id, 
                           sqlGetString(res, row, "wh_id"),
                           WH_ID_LEN);

                misTrimcpy(dst->dstare, 
                           sqlGetString(res, row, "dstare"),
                           ARECOD_LEN);

                misTrimcpy(dst->dstloc,
                           sqlGetString(res, row, "dstloc"),
                           STOLOC_LEN);

                misTrimcpy(dst->src_bldg_id,
                           src_bldg_id,
                           BLDG_ID_LEN);

                dst->srtseq = 0L;
            }
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            res = NULL;          

            if (dst == NULL)
            {
                sLog(0, "Really could not get the staging area"
                        "trying to set the romdon location");
                if (dst_bldg_list)
                {
                    sLog(0, "Just tried the staging location "
                            "whose building is very near to source building!");
                    dst = dst_bldg_list;
                }
                else
                {
                    dst = (SHIP_DST *)(ptr->reserved);    
                }
            }
        }

        /* No need to copy dst->wh_id to ptr->wh_id */
        misTrimcpy(ptr->dstare, dst->dstare, ARECOD_LEN);
        misTrimcpy(ptr->dstloc, dst->dstloc, STOLOC_LEN);

        while(dst_bldg_list)
        {
            dst = dst_bldg_list;
            dst_bldg_list = dst->next;
            free(dst);
        }
    }
}

/*
 * this function is called by sProcessPiecePick
 *                            sProcessPalletPick
 *                            sProcessCasePick
 *                            sBuildShpDstList
 *                            trnAllocateInventory
 * it will build a SHIP_DST structure to store the data passed
 * in, and hook this structure to the pick work's destination list.
 *  Params:
 *      ptr         -   Pick work
 *      dstare      -   destination area code
 *      dstloc      -   destination location
 *      src_bldg_id -   source building id
 *      srtseq      -   it's the value in the shp_dst_loc
 */
static void sAttachDstToPick(PCKWRK_DATA *ptr,
                             char *dstare,
                             char *dstloc,
                             char *src_bldg_id,
                             char *wh_id,
                             long srtseq)
{
    char buffer[512];

    RETURN_STRUCT * CurPtr = NULL;
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;
    long ret_status = eOK;


    SHIP_DST * pNode = NULL, *prev = NULL;

    /*
     * create the SHIP_DST structure
     */
    SHIP_DST * shp_dst = (SHIP_DST *)calloc(1, sizeof(SHIP_DST));
    if (shp_dst == NULL)
        return;

    shp_dst->parent = ptr;
    
    misTrimcpy(shp_dst->wh_id, wh_id == NULL ? "" : wh_id, WH_ID_LEN);
    misTrimcpy(shp_dst->dstare, dstare == NULL ? "" : dstare, ARECOD_LEN);
    misTrimcpy(shp_dst->dstloc, dstloc == NULL ? "" : dstloc, STOLOC_LEN);
    misTrimcpy(shp_dst->src_bldg_id, src_bldg_id == NULL ? "" : src_bldg_id,
               BLDG_ID_LEN);
    
    /*
     * get the dst_bldg_id for the dstare
     */
    sprintf(buffer,
            "list areas where arecod = '%s' and wh_id = '%s'",
            shp_dst->dstare, shp_dst->wh_id);

    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status == eOK)
    {
        res = srvGetResults(CurPtr);
        row = sqlGetRow(res);
        misTrimcpy(shp_dst->dst_bldg_id, 
                   sqlGetString(res, row, "bldg_id"),
                   BLDG_ID_LEN);
    }
    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;
    res = NULL;

    shp_dst->srtseq = srtseq;
    shp_dst->next = NULL;

    pNode =(SHIP_DST *)(ptr->reserved);

    prev = NULL;
    /*
     * get the position where the node will be 
     * insert into. this is according by the srtseq
     * value for this record
     */
    while(pNode)
    {
        if (pNode->srtseq > srtseq)
            break;
        prev = pNode;
        pNode = pNode->next;
    }

    shp_dst->next = pNode;

    if (prev == NULL)
    {
        ptr->reserved = shp_dst;
    }
    else
    {
        prev->next = shp_dst;
    }

}

static long sProcessPiecePick(char *stoloc,
                  char *devcod,
                  char *invsts,
                  long avlqty,
                  double avl_catch_qty,
                  long *pickqty,
                  double *pick_catch_qty,
                  long untcas,
                  long untpak,
                  char *arecod,
                  char *pcktyp,
                  char *cmbcod_i,
                  char *fifdte,
                  AREA_INFO *area,
                  long alloc_loc_flg,
                  char *lotnum,
                  char *revlvl,
                  char *supnum,
                  char *orgcod,
                  char *wh_id,
                  moca_bool_t *isAlloLocFailed)
{
    PCKWRK_DATA    *ptr, *new, *CurrentPos;
    char            cmbcod[CMBCOD_LEN + 1];
    long            pckqty;
    double          pck_catch_qty;

    long ret_status; 
    char dstloc[STOLOC_LEN + 1];
    ptr = PickRequestList;
    
    pck_catch_qty = 0;

    /*
     * Starting at the beginning of our PickRequests, process each one for
     * piece picking.
     */

    sLog (0, "Starting processing for piece picks");

    while (ptr)
    {

        /*
         * Determine the quantity we're going to be processing.  If we have
         * more quantity than we need, then we'll just take what we need.
         * Otherwise we take everything we have available.
         */
        if(ptr->pck_catch_qty > 0)
        {
            if(avl_catch_qty > ptr->pck_catch_qty)
            {
                pck_catch_qty = ptr->pck_catch_qty;
                pckqty = 0;
            }
            else if(avl_catch_qty == ptr->pck_catch_qty)
            {
                pck_catch_qty = ptr->pck_catch_qty;
                pckqty = avlqty;
                ptr->pckqty = pckqty;
            }
            else
            {
                pck_catch_qty = avl_catch_qty;
                pckqty = avlqty;
                ptr->pckqty = pckqty + 1;
            }
        }
        else if (avlqty > ptr->pckqty)
        {
            pckqty = ptr->pckqty;
        }
        else
        {
            pckqty = avlqty;
        }

        /*
         * If we're told to allocate a location for this pick, then do so now
         * before adding it to our pick found list.  If we fail for whatever
         * reason, processing continues to the next pick request.  If
         * successful, we'll store the location found in the pick found list.
         */

        memset (dstloc, 0, sizeof(dstloc));

        if (alloc_loc_flg)
        {
                if (*isAlloLocFailed &&
                     sSaveOrCheckSkipAllocateLocation(fifdte,
                                     ((SHIP_DST *)(ptr->reserved))->dstare,
                                            arecod,
                                            ptr->prtnum,
                                            ptr->prt_client_id,
                                            lotnum,
                                            revlvl,
                                            supnum,
                                            orgcod,
                                            invsts,
                                            untcas,
                                            untpak,
                                            pckqty == 0 ? 1: pckqty,
                                            dstloc,
                                            ptr->ftpcod,
                                            NULL,
                                            wh_id,
                                            isAlloLocFailed,
                                            0))
                        {
                                ptr = ptr->next;
                                continue;
                        }

            ret_status = sAllocateLocation (fifdte,
                    ((SHIP_DST *)(ptr->reserved))->dstare,
                                            arecod,
                                            ptr->prtnum,
                                            ptr->prt_client_id,
                                            lotnum,
                                            revlvl,
                                            supnum,
                                            orgcod,
                                            invsts,
                                            untcas,
                                            untpak,
                                            pckqty == 0 ? 1: pckqty,
                                            dstloc,
                                            ptr->ftpcod,
                                            NULL,
                                            wh_id);

            if (eOK != ret_status)
            {
                ptr = ptr->next;
                *isAlloLocFailed = BOOLEAN_TRUE;
                sSaveOrCheckSkipAllocateLocation(fifdte,
                                ((SHIP_DST *)(ptr->reserved))->dstare,
                                            arecod,
                                            ptr->prtnum,
                                            ptr->prt_client_id,
                                            lotnum,
                                            revlvl,
                                            supnum,
                                            orgcod,
                                            invsts,
                                            untcas,
                                            untpak,
                                            pckqty == 0 ? 1: pckqty,
                                            dstloc,
                                            ptr->ftpcod,
                                            NULL,
                                            wh_id,
                                            isAlloLocFailed,
                                            1);
                continue;
            }
        }        
        
        *pickqty += pckqty;
        *pick_catch_qty += pck_catch_qty;
        avlqty -= pckqty;
        avl_catch_qty -= pck_catch_qty;

        memset(cmbcod, 0, sizeof(cmbcod));
        if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
            misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);
        else
            appNextNum(NUMCOD_CMBCOD, cmbcod);

        if (pckqty < ptr->pckqty &&
            (ptr->pck_catch_qty == 0 || pck_catch_qty < ptr->pck_catch_qty))
        {
            /* split off a new pckwrk... */
            new = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));
            if (new == NULL)
                return (eNO_MEMORY);

            memcpy(new, ptr, sizeof(PCKWRK_DATA));
            new->reserved = NULL;
            strncpy(new->wh_id, wh_id, WH_ID_LEN);
            strncpy(new->srcloc, stoloc, STOLOC_LEN);
            strncpy(new->srcare, arecod, ARECOD_LEN);
            strncpy(new->invsts, invsts, INVSTS_LEN);
            strncpy(new->lodlvl, LODLVL_DETAIL, LODLVL_LEN);
            strncpy(new->cmbcod, cmbcod, CMBCOD_LEN);
            strncpy(new->devcod, devcod, DEVCOD_LEN);
            new->untpak = untpak;
            new->untcas = untcas;

            new->area_lodflg = area->area_lodflg;
            new->area_subflg = area->area_subflg;
            new->area_dtlflg = area->area_dtlflg;
            strcpy(new->area_loccod, area->area_loccod);

            new->pckqty = pckqty;
            new->pck_catch_qty = pck_catch_qty;
            ptr->pckqty -= pckqty;
            ptr->pck_catch_qty -= pck_catch_qty;
            ptr->maxavl -= pckqty;
            new->maxavl = pckqty;
            strncpy(ptr->fifdte, fifdte, DB_STD_DATE_LEN);

            if (misTrimLen (dstloc, STOLOC_LEN))
            {
                strncpy(new->dstloc, dstloc, STOLOC_LEN);
            }
            else
            {
                SHIP_DST * dst = (SHIP_DST *)(ptr->reserved);
                while(dst)
                {
                    sAttachDstToPick(new,
                                    dst->dstare,
                                    dst->dstloc,
                                    dst->src_bldg_id,
                                    dst->wh_id,
                                    dst->srtseq);
                    dst = dst->next;
                }
            }
            sAssignDstToPick(new);

            new->next = PickFoundList;
            new->prev = NULL;
            if (PickFoundList)
            PickFoundList->prev = new;

            PickFoundList = new;
                if (!PickFoundList->next)
                    sMarkFirstPick(pcktyp);

            /* Can't pick anything more at this point... */
            return (eOK);
        }
        else
        {
            /* Change this pckwrk into a found item  */
            if (ptr->prev)
                ptr->prev->next = ptr->next;
            if (ptr->next)
                ptr->next->prev = ptr->prev;
            if (ptr == PickRequestList)
                PickRequestList = ptr->next;

            if (PickFoundList)
                PickFoundList->prev = ptr;

            CurrentPos = ptr->next;
            ptr->next = PickFoundList;
            ptr->prev = NULL;
            PickFoundList = ptr;

            strncpy(ptr->wh_id, wh_id, WH_ID_LEN);
            strncpy(ptr->srcloc, stoloc, STOLOC_LEN);
            ptr->maxavl -= ptr->pckqty;
            strncpy(ptr->srcare, arecod, ARECOD_LEN);
            strncpy(ptr->invsts, invsts, INVSTS_LEN);
            strncpy(ptr->lodlvl, LODLVL_DETAIL, LODLVL_LEN);
            strncpy(ptr->cmbcod, cmbcod, CMBCOD_LEN);
            strncpy(ptr->devcod, devcod, DEVCOD_LEN);
            ptr->untpak = untpak;
            ptr->untcas = untcas;

            ptr->area_lodflg = area->area_lodflg;
            ptr->area_subflg = area->area_subflg;
            ptr->area_dtlflg = area->area_dtlflg;
            strcpy(ptr->area_loccod, area->area_loccod);

            strncpy(ptr->fifdte, fifdte, DB_STD_DATE_LEN);

            if (misTrimLen (dstloc, STOLOC_LEN))
                strncpy(ptr->dstloc, dstloc, STOLOC_LEN);
            sAssignDstToPick(ptr);

            ptr = CurrentPos;
            if (!PickFoundList->next)
                sMarkFirstPick(pcktyp);
        }
    }
    return (eOK);

}

/*
 * it's called by sProcessPalletPick function
 *          mixed   -   ip->mixedAttribute
 *          arecod  -   policy->arecod
 *          stoloc  -   load's stoloc
 * return value:
 *          1       -   the load can be round up to pallet
 *          0       -   the load can not be round up to pallet
 *  logic:
 *  if the load is not a mixed load, and the area where the load
 *  in is a single part area, return 1; otherwize return 0
 */
static long sIsInvFitPalletRound(long mixed,
                                 char *arecod,
                                 char *stoloc,
                                 char *wh_id)
{
    long lngRet = 0L;
    char buffer[512];
    long ret_status = eOK;

    memset (buffer, 0, sizeof(buffer));

    /*
    * check the if the area is a single part area
    */
    sprintf(buffer,
            " list areas where pckcod = '%s' "
            "              and arecod = '%s' "
            "              and wh_id  = '%s' ",
            PCKCOD_SINGLE,
            arecod,
            wh_id);

    ret_status = srvInitiateCommand(buffer, NULL);

    if (ret_status != eOK)
    {
        return lngRet;
    }

    if (mixed > 0) 
        return lngRet;

    lngRet = 1L;

    return lngRet;
}

/*
 * It get the ship_id from the Pick Work
 *  Param:
 *      ptr     -   Pick Work   (in)
 *      ship_id -   shipment ID (out)
 */
static void sGetShipID(PCKWRK_DATA *ptr, char *ship_id)
{
    long ret_status = 0;
    char buffer[1024];
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;

    memset(ship_id, 0, SHIP_ID_LEN + 1);
    memset(buffer, 0, sizeof(buffer));

    if (misTrimLen(ptr->ship_line_id, SHIP_LINE_ID_LEN))
    {
        sprintf(buffer,
                " select ship_id "
                "   from shipment_line "
                "  where ship_line_id   = '%s' ",
                ptr->ship_line_id);
    }
    else if (misTrimLen(ptr->ordnum, ORDNUM_LEN))
    {
        sprintf(buffer,        
                "select sl.ship_id "
                "  from ord_line ol, "
                "       shipment_line sl "
                " where ol.ordnum    = '%s' "
                "   and ol.client_id = '%s' "
                "   and ol.wh_id     = '%s' "
                "   and ol.ordlin    = '%s' "
                "   and ol.ordsln    = '%s' "
                "   and ol.ordnum    = sl.ordnum "
                "   and ol.wh_id     = sl.wh_id "
                "   and ol.ordlin    = sl.ordlin "
                "   and ol.ordsln    = sl.ordsln "
                "   and ol.client_id = sl.client_id ",
                ptr->ordnum,
                ptr->client_id,
                ptr->wh_id,
                ptr->ordlin, 
                ptr->ordsln);
    }
    else return;
    
    ret_status = sqlExecStr(buffer, &res);

    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        return;
    }

    row = sqlGetRow(res);

    strncpy(ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
    sqlFreeResults(res);
}

/*
 * it's called by sProcessPalletPick function
 *  arguments:
 *      ptr             -   the request pick work
 *      pal_thresh_pct  -   output value.
 *                          the pallet threshold percentage
 *                          of the request pick work
 *  return:
 *      eOK             -   if pal_thresh_pct is set for the part 
 *                          or customer of the pick work, and the
 *                          pal_thresh_pct value is successfully 
 *                          retrieved, then return eOK
 *      !eOK            -   otherwize, return !eOK
 */
static long sGetOrderInfo(PCKWRK_DATA *ptr,
                          long *pal_thresh_pct)
{
    long ret_status = -1;
    char buffer[1024];

    RETURN_STRUCT *CurPtr = NULL;
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;

    /*
    * get off the order line information
    */
    if (misTrimLen(ptr->ship_line_id, SHIP_LINE_ID_LEN))
    {
        sprintf(buffer,
                " select ol.wh_id, "
                "        ol.client_id, "
                "        ol.ordnum, "
                "        ol.ordlin, "
                "        ol.ordsln  "
                "  from shipment_line sl, "
                "       ord_line ol "
                " where sl.ship_line_id   = '%s' "
                "   and sl.client_id      = ol.client_id "
                "   and sl.ordnum         = ol.ordnum "
                "   and sl.wh_id          = ol.wh_id "
                "   and sl.ordlin         = ol.ordlin "
                "   and sl.ordsln         = ol.ordsln ",
                ptr->ship_line_id);
    }
    else if (misTrimLen(ptr->ordnum, ORDNUM_LEN))
    {
        sprintf(buffer,        
                "select ol.wh_id, "
                "       ol.client_id, " 
                "       ol.ordnum, "
                "       ol.ordlin, "
                "       ol.ordsln  "
                "  from ord_line ol "
                " where ol.ordnum    = '%s' "
                "   and ol.client_id = '%s' "
                "   and ol.wh_id     = '%s' "
                "   and ol.ordlin    = '%s' "
                "   and ol.ordsln    = '%s' ",
                ptr->ordnum,
                ptr->client_id,
                ptr->wh_id,
                ptr->ordlin, 
                ptr->ordsln);
    }
    else return ret_status;

    ret_status = sqlExecStr(buffer, &res);

    if (ret_status != eOK)
    {
        misTrc(T_FLOW, "Error getting order information ");
        sqlFreeResults(res);
        return ret_status;
    }

    row = sqlGetRow(res);

    /*
    * it calls the command 'get pallet threshold percentage' to
    * get the percentage for the order line. it will use this value
    * to determine if it's should be pallet round or not
    */

    sprintf(buffer,
            "  get pallet threshold percentage "
            "where client_id = '%s' "
            "  and ordnum = '%s' "
            "  and wh_id  = '%s' "
            "  and ordlin = '%s' "
            "  and ordsln = '%s' ",
            sqlGetString(res, row, "client_id"),
            sqlGetString(res, row, "ordnum"),
            sqlGetString(res, row, "wh_id"),
            sqlGetString(res, row, "ordlin"),
            sqlGetString(res, row, "ordsln"));

    sqlFreeResults(res);
    res = NULL;

    ret_status = srvInitiateCommand(buffer, &CurPtr);

    if (ret_status != eOK)
    {
        misTrc(T_FLOW, "Error calling get pallet threshold percentage ");
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return ret_status;
    }

    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);

    *pal_thresh_pct = sqlGetLong(res, row, "pal_thresh_pct");
    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;

    return ret_status;
}

/*
 * It's called by sProcessPalletPick
 *      argument:
 *          ptr     -   request pick work
 *      return:
 *          the last request pick work with same
 *          concod value as the argument has
 */
static PCKWRK_DATA * GetPickForRoundUp(PCKWRK_DATA * ptr)
{
    PCKWRK_DATA * pRet;
    char cur_concod[CONCOD_LEN + 1];
    misTrimcpy(cur_concod, ptr->concod, CONCOD_LEN);

    while(ptr)
    {
        if (strncmp(cur_concod, ptr->concod, CONCOD_LEN) == 0)
            pRet = ptr;
        ptr = ptr->next;
    }

    return pRet;
}

/*
 *  sProcessPalletPick - the difference between this routine and 
 *                       sProcessCasePick is that this routine will
 *                       attempt to combine picks (honoring the concod)
 *                       in order to form a pallet pick
 *
 *   Returns eOK if a pick was processed
 *           !eOK otherwise...
 */
static long sProcessPalletPick(char *stoloc,
                   char *devcod,
                   char *invsts,
                   long untpal,
                   double catch_pal,
                   long untcas,
                   long untpak,
                   char *arecod,
                   char *pcktyp,
                   char *cmbcod,
                   char *fifdte,
                   AREA_INFO *area,
                   long alloc_loc_flg,
                   char *lotnum,
                   char *revlvl,
                   char *supnum,
                   char *orgcod,
                   long mixed,
                   char *wh_id,
                   char *asset_typ,
                   INVENTORY *invHead,
                   moca_bool_t *isAlloLocFailed)
{
    PCKWRK_DATA    *ptr, *tptr, *mptr, *new, *NextToProcess;
    char            dstloc[STOLOC_LEN + 1];
    long            left;
    double          catch_left;
    long            total;
    double          total_catch;
    long            alloc_qty;
    /*
     * following variable is used for round up quantity flag
     * which is set up on the order line when creating 
     */
    long            round_up_possible, rounded_up;
    /*
     * pick_pal_round_up flag determine if the request pick
     * could be round up to pallet
     * inv_pal_round_up flag determine if the current selected loads 
     * can be round up to pallet
     */
    long            pick_pal_round_up, inv_pal_round_up;
    long            pal_thresh_pct;
    long            tmp_long;
    char            sav_cmbcod[CMBCOD_LEN + 1];
    char            cur_concod[CONCOD_LEN + 1];
    short           catch_allocation;
    long            replen_mode;

    long            ret_status;

    replen_mode = 0;
    catch_left = 0.0;
    total_catch = 0.0;
    catch_allocation = 0;
    alloc_qty = 0;
    left = 0;
    sLog (0, "Starting processing for pallet picks");

    /* If we're operating strictly in replenishment mode,
       then we want at least this amount..it is OK to go
       over.... */

    /* NOTE: We only set round up for generic replenishments...
       if we are in TOPOFF mode, we don't want to round up
       as we are already asking for the maximum quantity we want */

    if (strcmp(pcktyp, ALLOCATE_REPLENISH) == 0)
        replen_mode = 1;

    memset(sav_cmbcod, 0, sizeof(sav_cmbcod));

    /* Ok...we need to go through the requested pick list...
       ...we are going to search for the first concod that
       can become a full pick...once found, we will need to
       pull the appropriate pick requests out of the
       PickRequestList and put them into the PickFoundList...

       If this is replen_mode, then this check really doesn't
       make sense...since we'll take at least a given quantity,
       then we really just want the first request on the list.. */

    rounded_up = round_up_possible = 0;

    /*
     * firstly, check if the current selected load can be done the 
     * pallet round. if it can not, it's no needed to check if the 
     * request pick can be round up to pallet.
     */
    inv_pal_round_up = sIsInvFitPalletRound(mixed, arecod, stoloc, wh_id);

    if (!replen_mode)
    {
        for (ptr = PickRequestList; ptr; ptr = ptr->next)
        {
            if (ptr->processed)
                continue;

            memset(cur_concod, 0, sizeof(cur_concod));
            strncpy(cur_concod, ptr->concod, CONCOD_LEN);
            catch_allocation = (ptr->pck_catch_qty > 0);

            total = 0;
            total_catch = 0;
            round_up_possible = 0;
            pick_pal_round_up = 0L;
            for (tptr = ptr; tptr; tptr = tptr->next)
            {
                if (strncmp(tptr->concod, cur_concod, CONCOD_LEN) == 0 &&
                    catch_allocation == (tptr->pck_catch_qty > 0))
                {
                    if (tptr->rpqflg == BOOLEAN_TRUE)
                        round_up_possible++;

                    if(catch_allocation)
                    {
                        total_catch += tptr->pck_catch_qty;
                        tptr->processed = 1;
                        
                        if(total_catch >= catch_pal)
                            break;
                    }
                    else
                    {
                        total += tptr->pckqty;
                        tptr->processed = 1;

                        if (total >= untpal)
                            break;
                    }
                }
            }
            /* If we didn't find enough for a
               full, skip to the next concod... */

            if ((total < untpal && !catch_allocation) ||
                (total_catch < catch_pal && catch_allocation))
            {
                /* NOTE: Rounding up is currently only done by unit
                 * quantity.
                 */
                if (round_up_possible && !catch_allocation)
                {
                    rounded_up = 0;
                    for (mptr = ptr; mptr; mptr = mptr->next)
                    {
                        if (strncmp(mptr->concod,
                                cur_concod, CONCOD_LEN) == 0 &&
                            mptr->rpqflg == BOOLEAN_TRUE)
                        {
                            if (mptr->maxavl - mptr->pckqty > 0)
                            {
                                /* OK...so we need to round up
                                 to hit our untpal...let's see
                                 by how much... */
                                tmp_long = untpal - total;

                                /* How much are we taking for round up? */
                                if (tmp_long <= (mptr->maxavl - mptr->pckqty))
                                    rounded_up += tmp_long;
                                else
                                    rounded_up += (mptr->maxavl-mptr->pckqty);

                                total += (mptr->maxavl - mptr->pckqty);
                            }
                        }
                    }
                    if (total >= untpal)
                        break;
                }
                /*
                 * if the pick's quantity is less than the load's quantity
                 * and the round up flag is not set on the order line
                 * and if the load can be round up to pallet, it will check
                 * that the consolidate pick has the same pallet threshold 
                 * percentage value and the total picking quantity is greater 
                 * than the untpal * percentage, if it has, then it means 
                 * that the request pick can be round up to pallet, break out
                 * loop, otherwize, just continue to find the correct request
                 * pick meet the current load
                 */
                else if (inv_pal_round_up && !catch_allocation)
                {
                    if (sGetOrderInfo(ptr, &pal_thresh_pct) == eOK)
                    {
                        pick_pal_round_up = 1L;
                        for (mptr = ptr->next; mptr; mptr  = mptr->next)
                        {
                            if (strncmp(mptr->concod,
                                cur_concod, CONCOD_LEN) == 0)
                            {
                                tmp_long = -1L;
                                pick_pal_round_up = 
                                    (sGetOrderInfo(ptr, &tmp_long) == eOK &&
                                    tmp_long == pal_thresh_pct);
                                 
                                if (pick_pal_round_up == 0L)
                                    break;
                            }
                        }
                    }   
                    /*
                     * check that total pick quantity is greater than the 
                     * untpal
                     */
                    if (pick_pal_round_up &&
                        pal_thresh_pct / 100.0 * untpal <= total)
                    {
                        break;
                    }
                }
                continue;
            }
            else
                break;    /* Break out of the loop - we've found a FULL qty */
        }
        /*
         * if the rpqflg is set to turn on, the pallet round up function
         * should be turn off.
         */
        if (round_up_possible && !catch_allocation)
            pick_pal_round_up = 0L;
    }
    else
    {
        /* Just set the pick to be processed to be the
           head of the list and change it's pick quantity
           (if it's less than the untpal )  */

        ptr = PickRequestList;
        if (ptr->pckqty < untpal)
            ptr->pckqty = untpal;
        if (ptr->pck_catch_qty < catch_pal &&
            ptr->pck_catch_qty > 0)
        {
            ptr->pck_catch_qty = catch_pal;
            ptr->pckqty = untpal;
        }

        memset(cur_concod, 0, sizeof(cur_concod));
        strncpy(cur_concod, ptr->concod, CONCOD_LEN);
        /*
         * if it's a replenishment request pick, just turn off the pallet
         * round up function
         */
        pick_pal_round_up = 0L;
    }

    /* Reset the processed flag for future use... */

    for (tptr = PickRequestList; tptr; tptr = tptr->next)
        tptr->processed = 0;

    if (!ptr)
    {
        sLog(0, 
           "Unable to find full pallet quantity to attempt to allocate");
        return (eDB_NO_ROWS_AFFECTED);
    }

    total = 0;
    total_catch = 0;
    tptr = ptr;
    mptr = GetPickForRoundUp(ptr);

    while (tptr)
    {
        if (strncmp(cur_concod, tptr->concod, CONCOD_LEN) != 0)
        {
            tptr = tptr->next;
            continue;
        }
        if (misTrimLen(sav_cmbcod, CMBCOD_LEN) == 0)
        {
            if (cmbcod && misTrimLen(cmbcod, CMBCOD_LEN))
                misTrimcpy(sav_cmbcod, cmbcod, CMBCOD_LEN);
            else
                appNextNum(NUMCOD_CMBCOD, sav_cmbcod);
        }

        /*
         * if we need to round to pallet
         * the left quantity will be added to
         * the last request pick work, so we need to determine
         * if the tptr is the last pick work with same concod.
         * note: if the pick_pal_round_up = 1L , it means that the
         * total quantity is less than the pallet unit quantity
         */
        if (pick_pal_round_up && !catch_allocation)
        {
            if (tptr == mptr)
            {
                tptr->pckqty = untpal - total;
                tptr->maxavl = tptr->pckqty;
            }
        }

        /* We're on our current concod...process this pick... */

        /* If we're asked to allocate a location, do this first before
           manipulating the lists.  The calculations below are done to
           figure out the correct quantity we have for allocating a
           location. */

        memset (dstloc, 0, sizeof(dstloc));

        if (alloc_loc_flg)
        {
            alloc_qty = 0;

            if(catch_allocation)
            {
                alloc_qty = untpal;
            }
            else if (total + tptr->pckqty > untpal)
            {
                alloc_qty = untpal - total;
            }
            else
            {
                if (rounded_up > 0)
                {
                    if (tptr->rpqflg == BOOLEAN_TRUE &&
                        tptr->pckqty < tptr->maxavl)
                    {
                        tmp_long = tptr->maxavl - tptr->pckqty;

                        if (rounded_up > tmp_long)
                        {
                            alloc_qty = tptr->pckqty + tmp_long;
                        }
                        else
                        {
                            alloc_qty = tptr->pckqty + rounded_up;
                        }
                    }
                }
                else
                {
                    alloc_qty = tptr->pckqty;
                }
            }

            if (replen_mode
                && (!ptr->ftpcod || misTrimLen(ptr->ftpcod, FTPCOD_LEN) == 0))

            {
                /* Try to get a footprint from the inventory */
                sGetFootprintForPartLocation(stoloc,
                                             ptr->prtnum,
                                             ptr->prt_client_id,
                                             ptr->ftpcod);
            }
            if (*isAlloLocFailed &&
                             sSaveOrCheckSkipAllocateLocation(fifdte,
                    ((SHIP_DST *)(ptr->reserved))->dstare,
                                            arecod,
                                            ptr->prtnum,
                                            ptr->prt_client_id,
                                            lotnum,
                                            revlvl,
                                            supnum,
                                            orgcod,
                                            invsts,
                                            untcas,
                                            untpak,
                                            alloc_qty,
                                            dstloc,
                                            ptr->ftpcod,
                                            asset_typ,
                                            wh_id,
                                            isAlloLocFailed,
                                            0))
                        {
                                tptr = tptr->next;
                                continue;
                        }

            ret_status = sAllocateLocation (fifdte,
                    ((SHIP_DST *)(ptr->reserved))->dstare,
                                            arecod,
                                            ptr->prtnum,
                                            ptr->prt_client_id,
                                            lotnum,
                                            revlvl,
                                            supnum,
                                            orgcod,
                                            invsts,
                                            untcas,
                                            untpak,
                                            alloc_qty,
                                            dstloc,
                                            ptr->ftpcod,
                                            asset_typ,
                                            wh_id);
            
            if (eOK != ret_status)
            {
                tptr = tptr->next;
                *isAlloLocFailed = BOOLEAN_TRUE;
                sSaveOrCheckSkipAllocateLocation(fifdte,
                    ((SHIP_DST *)(ptr->reserved))->dstare,
                                            arecod,
                                            ptr->prtnum,
                                            ptr->prt_client_id,
                                            lotnum,
                                            revlvl,
                                            supnum,
                                            orgcod,
                                            invsts,
                                            untcas,
                                            untpak,
                                            alloc_qty,
                                            dstloc,
                                            ptr->ftpcod,
                                            asset_typ,
                                            wh_id,
                                            isAlloLocFailed,
                                            1);
                continue;
            }           
        }
    
        /* If the current request item is requesting more than
           we have, then we need to split off the request... */

        if(catch_allocation)
        {
            if(total_catch + tptr->pck_catch_qty > catch_pal)
            {
                catch_left = catch_pal - total_catch;
                left = untpal - total;
                new = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));
                if(new == NULL)
                {
                    for(tptr = PickRequestList; tptr; tptr = tptr->next)
                    {
                        tptr->processed = 0;
                    }
                    return (eNO_MEMORY);
                }
                memcpy(new, tptr, sizeof(PCKWRK_DATA));
                new->reserved = NULL;
                tptr->pck_catch_qty -= catch_left;
                new->pck_catch_qty = catch_left;
                
                if(tptr->pckqty - left > 0)
                {
                    tptr->pckqty -= left;
                    new->pckqty = left;
                }
                else
                {
                    new->pckqty = 1;
                }

                new->maxavl = new->pckqty;
                strncpy(new->wh_id, wh_id, WH_ID_LEN);
                strncpy(new->srcloc, stoloc, STOLOC_LEN);
                strncpy(new->srcare, arecod, ARECOD_LEN);
                strncpy(new->invsts, invsts, INVSTS_LEN);
                strncpy(new->devcod, devcod, DEVCOD_LEN);
                strncpy(new->concod, cur_concod, CONCOD_LEN);
                strncpy(new->cmbcod, sav_cmbcod, CMBCOD_LEN);
                new->untpak = untpak;
                new->untcas = untcas;
                new->area_lodflg = area->area_lodflg;
                new->area_subflg = area->area_subflg;
                new->area_dtlflg = area->area_dtlflg;
                strcpy(new->area_loccod, area->area_loccod);
                strncpy(new->lodlvl, LODLVL_LOAD, LODLVL_LEN);
                strncpy(new->asset_typ, asset_typ, ASSET_TYP_LEN);
                strncpy(new->fifdte, fifdte, DB_STD_DATE_LEN);
          
                if (misTrimLen (dstloc, STOLOC_LEN)) 
                    strncpy(new->dstloc, dstloc, STOLOC_LEN);
                else
                {
                    SHIP_DST * dst = (SHIP_DST *)(tptr->reserved);
                    while(dst)
                    {
                        sAttachDstToPick(new,
                                    dst->dstare,
                                    dst->dstloc,
                                    dst->src_bldg_id,
                                    dst->wh_id,
                                    dst->srtseq);
                        dst = dst->next;
                    }
                }
                sAssignDstToPick(new);

                total += left;
                total_catch += catch_left;

                /* add to the pick list */
                if (PickFoundList)
                    PickFoundList->prev = new;
                new->next = PickFoundList;
                new->prev = NULL;
                PickFoundList = new;
                if (!PickFoundList->next)
                    sMarkFirstPick(pcktyp);

                /* Since we've got all that we possibly can in
                   this iteration, return eOK  */
                return (eOK);                
            }
            else
            {
                /* If we're here, then we're satisfying the entire
                   request...we need to move it to the Found list
                   and remove it from the Request list.... */

                total_catch += tptr->pck_catch_qty;
                total += untpal;
                tptr->maxavl -= untpal;

                if (tptr->prev)
                    tptr->prev->next = tptr->next;
                if (tptr->next)
                    tptr->next->prev = tptr->prev;
                if (tptr == PickRequestList)
                    PickRequestList = tptr->next;

                /* Before we trash tptr->next, we save it off so
                   we can continue along in our request list... */
                NextToProcess = tptr->next;
                if (PickFoundList)
                    PickFoundList->prev = tptr;
                tptr->next = PickFoundList;
                tptr->prev = NULL;
                strncpy(tptr->wh_id, wh_id, WH_ID_LEN);
                strncpy(tptr->srcloc, stoloc, STOLOC_LEN);
                strncpy(tptr->srcare, arecod, ARECOD_LEN);
                strncpy(tptr->invsts, invsts, INVSTS_LEN);
                strncpy(tptr->lodlvl, LODLVL_LOAD, LODLVL_LEN);
                /* Get the asset_typ which has the minimum asset volume 
                 * in INVENTORY LIST.*/
                strncpy(tptr->asset_typ, asset_typ, ASSET_TYP_LEN);
                strncpy(tptr->concod, cur_concod, CONCOD_LEN);
                strncpy(tptr->cmbcod, sav_cmbcod, CMBCOD_LEN);
                strncpy(tptr->devcod, devcod, DEVCOD_LEN);
                tptr->untpak = untpak;
                tptr->untcas = untcas;

                tptr->area_lodflg = area->area_lodflg;
                tptr->area_subflg = area->area_subflg;
                tptr->area_dtlflg = area->area_dtlflg;
                strcpy(tptr->area_loccod, area->area_loccod);

                strncpy(tptr->fifdte, fifdte, DB_STD_DATE_LEN);

                if (misTrimLen (dstloc, STOLOC_LEN))
                    strncpy(tptr->dstloc, dstloc, STOLOC_LEN);

                sAssignDstToPick(tptr);

                PickFoundList = tptr;
                /* Now reset tptr to our current position 
                 * in the request list.. */
                tptr = NextToProcess;
                if (!PickFoundList->next)
                    sMarkFirstPick(pcktyp);
                /* If we've done all that we can this iter.. */
                if (total_catch >= catch_pal)    
                    return (eOK);
            }        
        }
        else if (total + tptr->pckqty > untpal)
        {
            left = untpal - total;
            new = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));
            if (new == NULL)
            {
                for (tptr = PickRequestList; tptr; tptr = tptr->next)
                    tptr->processed = 0;

                return (eNO_MEMORY);
            }

            memcpy(new, tptr, sizeof(PCKWRK_DATA));
            new->reserved = NULL;
            
            /* update the leftover ptr */
            tptr->pckqty -= left;

            /* Reflect that we just "picked" some... */
            tptr->maxavl -= left;
            /* Just in case (though it shouldn't happen) */
            if (tptr->maxavl < 0)
            tptr->maxavl = 0;

            new->pckqty = left;
            new->maxavl = new->pckqty;
            strncpy(new->wh_id, wh_id, WH_ID_LEN);
            strncpy(new->srcloc, stoloc, STOLOC_LEN);
            strncpy(new->srcare, arecod, ARECOD_LEN);
            strncpy(new->invsts, invsts, INVSTS_LEN);
            strncpy(new->devcod, devcod, DEVCOD_LEN);
            strncpy(new->concod, cur_concod, CONCOD_LEN);
            strncpy(new->cmbcod, sav_cmbcod, CMBCOD_LEN);
            new->untpak = untpak;
            new->untcas = untcas;

            new->area_lodflg = area->area_lodflg;
            new->area_subflg = area->area_subflg;
            new->area_dtlflg = area->area_dtlflg;
            strcpy(new->area_loccod, area->area_loccod);

            strncpy(new->lodlvl, LODLVL_LOAD, LODLVL_LEN);
            strncpy(new->asset_typ, asset_typ, ASSET_TYP_LEN);
            strncpy(new->fifdte, fifdte, DB_STD_DATE_LEN);
          
            if (misTrimLen (dstloc, STOLOC_LEN)) 
                strncpy(new->dstloc, dstloc, STOLOC_LEN);
            else
            {
                SHIP_DST * dst = (SHIP_DST *)(tptr->reserved);
                while(dst)
                {
                    sAttachDstToPick(new,
                                    dst->dstare,
                                    dst->dstloc,
                                    dst->src_bldg_id,
                                    dst->wh_id,
                                    dst->srtseq);
                    dst = dst->next;
                }
            }
            sAssignDstToPick(new);

            total += left;

            /* add to the pick list */
            if (PickFoundList)
                PickFoundList->prev = new;
            new->next = PickFoundList;
            new->prev = NULL;
            PickFoundList = new;
            if (!PickFoundList->next)
                sMarkFirstPick(pcktyp);

            /* Since we've got all that we possibly can in
               this iteration, return eOK  */
            return (eOK);
        }
        else
        {
            /* if we had to round up to hit our untpal, then
               we'll need to be tacking on extra...if we simply
               check for that up front and change our pckqty
               accordingly, everything should flow normally... */
            if (rounded_up > 0)
            {
                if (tptr->rpqflg == BOOLEAN_TRUE &&
                    tptr->pckqty < tptr->maxavl)
                {
                    /* How much extra do we have? */
                    tmp_long = tptr->maxavl - tptr->pckqty;

                    /* If we need more than we have, then
                       go ahead and take everything this line
                       has to offer...and adjust the rounded qty
                       accordingly...NOTE:  the rounded_up quantity
                       reflects how much was calculated in our
                       top loop...we decrement it as we go as it
                       tells us how much is left to find... */
                    if (rounded_up > tmp_long)
                    {
                        rounded_up -= tmp_long;
                        tptr->pckqty += tmp_long;
                    }
                    else
                    {
                        tptr->pckqty += rounded_up;
                        rounded_up = 0;
                    }
                    /* There is no need to adjust the maxavl as
                       that will get adjusted as we split off the
                       new pick, etc... */
                }
            }

            /* If we're here, then we're satisfying the entire
               request...we need to move it to the Found list
               and remove it from the Request list.... */

            total += tptr->pckqty;
            tptr->maxavl -= tptr->pckqty;

            if (tptr->prev)
                tptr->prev->next = tptr->next;
            if (tptr->next)
                tptr->next->prev = tptr->prev;
            if (tptr == PickRequestList)
                PickRequestList = tptr->next;

            /* Before we trash tptr->next, we save it off so
               we can continue along in our request list... */
            NextToProcess = tptr->next;
            if (PickFoundList)
                PickFoundList->prev = tptr;
            tptr->next = PickFoundList;
            tptr->prev = NULL;
            strncpy(tptr->wh_id, wh_id, WH_ID_LEN);
            strncpy(tptr->srcloc, stoloc, STOLOC_LEN);
            strncpy(tptr->srcare, arecod, ARECOD_LEN);
            strncpy(tptr->invsts, invsts, INVSTS_LEN);
            strncpy(tptr->lodlvl, LODLVL_LOAD, LODLVL_LEN);
            /* Get the asset_typ which has the minimum asset volume 
             * in INVENTORY LIST.*/
            strncpy(tptr->asset_typ, asset_typ, ASSET_TYP_LEN);
            strncpy(tptr->concod, cur_concod, CONCOD_LEN);
            strncpy(tptr->cmbcod, sav_cmbcod, CMBCOD_LEN);
            strncpy(tptr->devcod, devcod, DEVCOD_LEN);
            tptr->untpak = untpak;
            tptr->untcas = untcas;

            tptr->area_lodflg = area->area_lodflg;
            tptr->area_subflg = area->area_subflg;
            tptr->area_dtlflg = area->area_dtlflg;
            strcpy(tptr->area_loccod, area->area_loccod);

            strncpy(tptr->fifdte, fifdte, DB_STD_DATE_LEN);

            if (misTrimLen (dstloc, STOLOC_LEN))
                strncpy(tptr->dstloc, dstloc, STOLOC_LEN);

            sAssignDstToPick(tptr);

            PickFoundList = tptr;
            /* Now reset tptr to our current position in the request list.. */
            tptr = NextToProcess;
            if (!PickFoundList->next)
                sMarkFirstPick(pcktyp);

            if (total == untpal)    /* If we've done all that we
                           can this iter.. */
                return (eOK);
        }
    }
    return (eOK);
}

static long sProcessCasePick(char *stoloc,
                 char *arecod,
                 char *devcod,
                 char *invsts,
                 long avlqty,
                 double avl_catch_qty,
                 long untcas,
                 long untpak,
                 long *pickqty,
                 double *pick_catch_qty,
                 char *pcktyp,
                 char *cmbcod_i,
                 char *fifdte,
                 AREA_INFO *area,
                 long alloc_loc_flg,
                 char *lotnum,
                 char *revlvl,
                 char *supnum,
                 char *orgcod,
                 char *wh_id,
                 moca_bool_t *isAlloLocFailed)
{
    PCKWRK_DATA    *ptr, *new;
    long            num_avl, num_pck, num_req;
    long            pckqty;
    char            cmbcod[CMBCOD_LEN + 1];
    long            replen_mode;
  
    char dstloc[STOLOC_LEN + 1];
    long ret_status;
    sLog (0, "Starting processing for case picks");

    replen_mode = 0;
    if (strcmp(pcktyp, ALLOCATE_REPLENISH) == 0)
        replen_mode = 1;

    for (ptr = PickRequestList; ptr; ptr = ptr->next)
    {
        num_avl = (long) avlqty / untcas;
        pckqty = ptr->pckqty;
        if (num_avl <= 0)
        {
            sLog(0, "Requested pick quantity < untcas, %d < %d",
                avlqty, untcas);
            break;  /* Not going to be able to pick anything...get out */
        }

        if (replen_mode && pckqty < untcas)
        {
            pckqty = untcas;
        }

        if (pckqty < untcas)
        {
            /* If we don't have a full case, we have to check
               for round up possibilities... */
            /* Not allowing round ups for catch quantity picks */
            if (ptr->rpqflg == BOOLEAN_TRUE &&
                ptr->maxavl >= untcas &&
                ptr->pck_catch_qty <= 0)
            {
                /* Since the maxavl is >= untcas, we can simply
                   round up to a case... */
                pckqty = untcas;
            }
            else
                continue;
        }

        num_req = (long) pckqty / untcas;

        if (num_avl > num_req)
            num_pck = num_req;
        else
            num_pck = num_avl;

        /* So..we know how much we think we're going to pick...
           we may be able to round up if the pick allows
           overpicking for a better material handling scenario...
           We need to be careful, however, as we don't want to
           inflate the requirement (pckqty) unless we can actually
           grab something...So..before we get too carried away,
           we need to answer the following question: 1) Can we round
           up to make an even case pick?  2)  If we did round up,
           would we have enough available?

           Note: we already caught the easy case of less than a case
           requested.  Now we need to catch the situation where more
           than a case was requested but some change was left over...
           i.e. u/c = 4 and 6 requested...  */

        /* First - do we have an odd quantity? */
        if (num_req * untcas != pckqty)
        {
            /* If it is odd, then let's check if rounding up
               would still be under our limit... */
            /* Disable this for catch quantity allocations */
            if ((num_req + 1) * untcas <= ptr->maxavl &&
                ptr->pck_catch_qty <= 0)
            {
                /* It wouldn't...not make sure we have enough... */
                if ((num_req + 1) <= num_avl)
                {
                    /* We're golden... */
                    pckqty = (num_req + 1) * untcas;
                    num_req += 1;
                    num_pck = num_req;
                }
            }
        }

        /* If this is a catch quantity allocation 
         * modify the quantities involved */
        if(ptr->pck_catch_qty > 0)
        {
            /* If there is more than enough catch quantity */
            if(avl_catch_qty > ptr->pck_catch_qty)
            {
                *pick_catch_qty = ptr->pck_catch_qty;
                num_pck = 0;
            }
            /* If there is not enough catch quantity */
            else if(avl_catch_qty < ptr->pck_catch_qty)
            {
                *pick_catch_qty = avl_catch_qty;
                num_pck = avlqty / untcas;
                pckqty += untcas * 1;
            }
            /* Only option left is that there is exactly the catch 
             * quantity requested that is available
             */
            else
            {
                *pick_catch_qty = avl_catch_qty;
                num_pck = avlqty / untcas;
            }
        }
        else
        {
            *pick_catch_qty = 0;
        }

        /*
         * If we're told to get a location, do it here before we manipulate
         * our picking lists.  We will be sending the actual pckqty that is
         * being allocated (num_pck * untcas) which will handle full and
         * splitted picks instead of the original pckqty (pckqty). 
         */
        memset (dstloc, 0, sizeof(dstloc));
        if (alloc_loc_flg)
        {
            if (replen_mode
                && (!ptr->ftpcod || misTrimLen(ptr->ftpcod, FTPCOD_LEN) == 0))

            {
                /* Try to get a footprint from the inventory */
                sGetFootprintForPartLocation(stoloc,
                                             ptr->prtnum,
                                             ptr->prt_client_id,
                                             ptr->ftpcod);
            }

            if (*isAlloLocFailed &&
                 sSaveOrCheckSkipAllocateLocation(fifdte,
                                 ((SHIP_DST *)(ptr->reserved))->dstare,
                                            arecod,
                                            ptr->prtnum,
                                            ptr->prt_client_id,
                                            lotnum,
                                            revlvl,
                                            supnum,
                                            orgcod,
                                            invsts,
                                            untcas,
                                            untpak,
                                            (num_pck * untcas) == 0 ?
                                             1 * untcas :
                                             (num_pck * untcas),
                                            dstloc,
                                            ptr->ftpcod,
                                            NULL,
                                            wh_id,
                                            isAlloLocFailed,
                                            0))
            {
                    continue;
            }

            ret_status = sAllocateLocation (fifdte,
                    ((SHIP_DST *)(ptr->reserved))->dstare,
                                            arecod,
                                            ptr->prtnum,
                                            ptr->prt_client_id,
                                            lotnum,
                                            revlvl,
                                            supnum,
                                            orgcod,
                                            invsts,
                                            untcas,
                                            untpak,
                                            (num_pck * untcas) == 0 ? 
                                             1 * untcas : 
                                             (num_pck * untcas),
                                            dstloc,
                                            ptr->ftpcod,
                                            NULL,
                                            wh_id);
            
            if (eOK != ret_status)
            {
                *isAlloLocFailed = BOOLEAN_TRUE;
                sSaveOrCheckSkipAllocateLocation(fifdte,
                                       ((SHIP_DST *)(ptr->reserved))->dstare,
                                                 arecod,
                                                 ptr->prtnum,
                                                 ptr->prt_client_id,
                                                 lotnum,
                                                 revlvl,
                                                 supnum,
                                                 orgcod,
                                                 invsts,
                                                 untcas,
                                                 untpak,
                                                 (num_pck * untcas) == 0 ?
                                                  1 * untcas :
                                                  (num_pck * untcas),
                                                 dstloc,
                                                 ptr->ftpcod,
                                                 NULL,
                                                 wh_id,
                                                 isAlloLocFailed,
                                                 1);
                continue;
            }
        }

        ptr->pckqty = pckqty;

        /* If we are not allocating by catch and the pick amount
         * is exactly what was requested or...
         * If we are allocating by catch and the available catch
         * quantity is greater than or equal to the requested catch
         * quantity
         */
        if (num_pck * untcas == pckqty ||
            (*pick_catch_qty > 0 && *pick_catch_qty == ptr->pck_catch_qty))
        {
            /* copy into the PickFoundList.. */
            if (ptr->prev)
                ptr->prev->next = ptr->next;
            if (ptr->next)
                ptr->next->prev = ptr->prev;
            if (ptr == PickRequestList)
                PickRequestList = ptr->next;

            if (PickFoundList)
                PickFoundList->prev = ptr;

            ptr->next = PickFoundList;
            ptr->prev = NULL;
            ptr->maxavl -= ptr->pckqty;
            if (ptr->maxavl < 0)
                ptr->maxavl = 0;
            
            strncpy(ptr->wh_id, wh_id, WH_ID_LEN);
            strncpy(ptr->srcloc, stoloc, STOLOC_LEN);
            strncpy(ptr->srcare, arecod, ARECOD_LEN);
            strncpy(ptr->invsts, invsts, INVSTS_LEN);
            strncpy(ptr->lodlvl, LODLVL_SUB, LODLVL_LEN);

            memset(cmbcod, 0, sizeof(cmbcod));
            if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
                misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);
            else
                appNextNum(NUMCOD_CMBCOD, cmbcod);
            strncpy(ptr->cmbcod, cmbcod, CMBCOD_LEN);
            strncpy(ptr->devcod, devcod, DEVCOD_LEN);

            ptr->untcas = untcas;
            ptr->untpak = untpak;
            strncpy(ptr->fifdte, fifdte, DB_STD_DATE_LEN);
            PickFoundList = ptr;
            
            *pickqty = num_pck * untcas;
            
            ptr->area_lodflg = area->area_lodflg;
            ptr->area_subflg = area->area_subflg;
            ptr->area_dtlflg = area->area_dtlflg;
            strcpy(ptr->area_loccod, area->area_loccod);

            if (misTrimLen (dstloc, STOLOC_LEN))
                strncpy(ptr->dstloc, dstloc, STOLOC_LEN);
            sAssignDstToPick(ptr);

            if (!PickFoundList->next)
                sMarkFirstPick(pcktyp);

            return (eOK);

        }
        else
        {
            /* split off a pick... */
            new = (PCKWRK_DATA *) calloc(1, sizeof(PCKWRK_DATA));
            if (new == NULL)
            {
                return (eNO_MEMORY);
            }

            memcpy(new, ptr, sizeof(PCKWRK_DATA));
            new->reserved = NULL;
            ptr->pckqty -= (num_pck * untcas);
            ptr->maxavl -= (num_pck * untcas);
            ptr->pck_catch_qty -= *pick_catch_qty;
            *pickqty = num_pck * untcas;
            new->maxavl = new->pckqty = num_pck * untcas;
            new->pck_catch_qty = *pick_catch_qty;     
            strncpy(new->wh_id, wh_id, WH_ID_LEN);
            strncpy(new->srcloc, stoloc, STOLOC_LEN);
            strncpy(new->srcare, arecod, ARECOD_LEN);
            strncpy(new->invsts, invsts, INVSTS_LEN);
            strncpy(new->lodlvl, LODLVL_SUB, LODLVL_LEN);
            new->untcas = untcas;
            new->untpak = untpak;

            new->area_lodflg = area->area_lodflg;
            new->area_subflg = area->area_subflg;
            new->area_dtlflg = area->area_dtlflg;
            strcpy(new->area_loccod, area->area_loccod);

            memset(cmbcod, 0, sizeof(cmbcod));
            if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
                misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);
            else
                appNextNum(NUMCOD_CMBCOD, cmbcod);
            strncpy(new->cmbcod, cmbcod, CMBCOD_LEN);
            strncpy(new->devcod, devcod, DEVCOD_LEN);

            if (misTrimLen (dstloc, STOLOC_LEN))
                strncpy(new->dstloc, dstloc, STOLOC_LEN);
            else
            {
                SHIP_DST * dst = (SHIP_DST *)(ptr->reserved);
                while(dst)
                {
                    sAttachDstToPick(new,
                        dst->dstare,
                        dst->dstloc,
                        dst->src_bldg_id,
                        dst->wh_id,
                        dst->srtseq);
                    dst = dst->next;
                }
            }
            sAssignDstToPick(new);

            /* add to the pick list */
            if (PickFoundList)
                PickFoundList->prev = new;
            new->next = PickFoundList;
            new->prev = NULL;
            strncpy(new->fifdte, fifdte, DB_STD_DATE_LEN);
            PickFoundList = new;

            if (!PickFoundList->next)
                sMarkFirstPick(pcktyp);

            return (eOK);
        }
    }
    return (eDB_NO_ROWS_AFFECTED);
}

static RES_ASSOCIATE *sGetResourceTranslation(PCKWRK_DATA * ptr,
                          char *arecod,
                          char *wh_id)
{
    RES_ASSOCIATE  *tmpRes, *retRes = NULL;

    if (ptr->schbatRes && ptr->schbatRes->resAssc)
    {
    /* We have override resource translation */
    tmpRes = ptr->schbatRes->resAssc;
    while (tmpRes)
    {
        if (!misCiStrncmp(arecod, tmpRes->arecod, ARECOD_LEN) &&
            !misCiStrncmp(wh_id, tmpRes->wh_id, WH_ID_LEN))
        return (tmpRes);

        tmpRes = tmpRes->next;
    }
    }
    tmpRes = resAssociate;
    while (tmpRes)
    {
    if (!misCiStrncmp(arecod, tmpRes->arecod, ARECOD_LEN) &&
        !misCiStrncmp(wh_id, tmpRes->wh_id, WH_ID_LEN))
        return (tmpRes);

    tmpRes = tmpRes->next;
    }

    return (retRes);
}

/*
 * The following function adds an inventory structure to the linked
 * list describing the inventory for the location.  The items are
 * organized such that mixed attribute pallets are listed first, ordered
 * by total quantity, followed by the "pure" pallets, also ordered by
 * quantity.  The mixed attribute pallets will not be available for 
 * (pallet) picking by anyone who requests a specific attribute.
 *
 */
static void sAddInvToList(INVENTORY **ihead, 
              INVENTORY *inv)
{
    INVENTORY *ip, *i_last;

    if (*ihead == NULL)
    {
    *ihead = inv;
    return;
    }

    i_last = NULL;
    for (ip = *ihead; ip; i_last = ip, ip = ip->next)
    {
    if ((inv->mixedAttribute && 
         (!ip->mixedAttribute ||
          ip->untpal <= inv->untpal)) ||
        (!inv->mixedAttribute &&
         !ip->mixedAttribute &&
         ip->untpal <= inv->untpal))
    {
        /* insert in to the list BEFORE ip */
        inv->next = ip;
        inv->last = ip->last;

        if (ip->last)
        ip->last->next = inv;
        ip->last = inv;

        if (ip == *ihead)
        *ihead = inv;
        return;
    }

    }
    /* Still here...must add to the end of the list */
    if (i_last)
    {
    i_last->next = inv;
    inv->last = i_last;
    }
    return;
}

/*
 * sLoadInvStructure
 * - The following routine loads the specified inventory structure for a 
 *   particular pallet.  
 * - When complete, the INVENTORY structure will be populated as well
 *   as the INVENTRORY_ATTR structures.
 * - In the event that more than one row of the result set describes
 *   the pallet, the row pointer will be advanced and returned for
 *   processing by the caller.
 */
static mocaDataRow *sLoadInvStructure(mocaDataRes *res, 
                      mocaDataRow *row, 
                      INVENTORY **ip)
{
    INVENTORY_ATTR *attr, *orig;
    mocaDataRow *last_row;
    long has_wrkref_applied = 0;

    *ip = (INVENTORY *)calloc(1, sizeof(INVENTORY));
    
    strcpy((*ip)->lodnum, sqlGetString(res, row, "lodnum"));
    if (!sqlIsNull(res, row, "asset_typ"))
        strcpy((*ip)->asset_typ, sqlGetString(res, row, "asset_typ"));
    
    attr = (INVENTORY_ATTR *)calloc(1, sizeof(INVENTORY_ATTR));

    strcpy(attr->orgcod, sqlGetString(res, row, "orgcod"));
    strcpy(attr->revlvl, sqlGetString(res, row, "revlvl"));
    strcpy(attr->supnum, sqlGetString(res, row, "supnum"));
    strcpy(attr->lotnum, sqlGetString(res, row, "lotnum"));
    strcpy(attr->invsts, sqlGetString(res, row, "invsts"));
    if (!sqlIsNull(res, row, "distro_id"))
        strcpy(attr->distro_id, sqlGetString(res, row, "distro_id"));
    attr->alcflg = sqlGetBoolean(res, row, "alcflg");
    attr->untpak = sqlGetLong(res, row, "untpak");
    attr->untcas = sqlGetLong(res, row, "untcas");
    
    if (!sqlIsNull(res, row, "wrkref"))
    {
        sLog(0,
            "Skipping inventory from load %s, qty: %d, "
            "as it has wrkref %s applied!",
            (*ip)->lodnum, sqlGetLong(res, row, "untqty"),
            sqlGetString(res, row, "wrkref"));
        has_wrkref_applied = 1;
    }
    else
    {
        attr->untqty = sqlGetLong(res, row, "untqty");
        attr->catch_qty = sqlGetFloat(res, row, "catch_qty");
    }

    orig = attr;
    last_row = row;

    (*ip)->untpal += attr->untqty;
    (*ip)->catch_pal += attr->catch_qty;
    (*ip)->attr = attr;

    for (row = sqlGetNextRow(row); row; 
     last_row = row, row = sqlGetNextRow(row))
    {
        if (strcmp((*ip)->lodnum, sqlGetString(res, row, "lodnum")) != 0)
            return(last_row);

        attr->next = (INVENTORY_ATTR *) calloc(1, sizeof(INVENTORY_ATTR));
        attr = attr->next;
        strcpy(attr->orgcod, sqlGetString(res, row, "orgcod"));
        strcpy(attr->revlvl, sqlGetString(res, row, "revlvl"));
        strcpy(attr->supnum, sqlGetString(res, row, "supnum"));
        strcpy(attr->lotnum, sqlGetString(res, row, "lotnum"));
        strcpy(attr->invsts, sqlGetString(res, row, "invsts"));
        if (!sqlIsNull(res, row, "distro_id"))
            strcpy(attr->distro_id, sqlGetString(res, row, "distro_id"));
        attr->alcflg = sqlGetBoolean(res, row, "alcflg");
        attr->untpak = sqlGetLong(res, row, "untpak");
        attr->untcas = sqlGetLong(res, row, "untcas");
        attr->untqty = sqlGetLong(res, row, "untqty");
        attr->catch_qty = sqlGetFloat(res, row, "catch_qty");
        (*ip)->untpal += attr->untqty;

        if (strncmp(orig->revlvl, attr->revlvl, REVLVL_LEN) != 0)
            (*ip)->mixedRevlvl = 1;
        if (strncmp(orig->lotnum, attr->lotnum, LOTNUM_LEN) != 0)
            (*ip)->mixedLotnum = 1;
        if (strncmp(orig->orgcod, attr->orgcod, ORGCOD_LEN) != 0)
            (*ip)->mixedOrgcod = 1;
        if (strncmp(orig->invsts, attr->invsts, INVSTS_LEN) != 0)
            (*ip)->mixedInvsts = 1;
        if (attr->alcflg != orig->alcflg)
            (*ip)->mixedAlcflg = 1;
        if (attr->untcas != orig->untcas)
            (*ip)->mixedUntcas = 1;
        if (attr->untpak != orig->untpak)
            (*ip)->mixedUntpak = 1;
        if (strncmp(orig->supnum, attr->supnum, SUPNUM_LEN) != 0)
            (*ip)->mixedSupnum = 1;

        (*ip)->mixedAttribute = (*ip)->mixedUntcas + (*ip)->mixedUntpak + 
          (*ip)->mixedOrgcod + (*ip)->mixedRevlvl + (*ip)->mixedSupnum +
          (*ip)->mixedLotnum;
    
    }
    if (has_wrkref_applied)
    {
        sLog(0, "   Disabling pallet picking on inventory "
            "with wrkref applied");
        (*ip)->untpal = 0;
    }

    return(last_row);
}

/* sLoadInventoryAtLocation
 * - Load relevant inventory in the specified location into 
 *   a data structure.  The list is ordered by mixing attributes.
 */
static long sLoadInventoryAtLocation(char *stoloc, 
                     char *wh_id,
                     PICK_POLICY_DATA *area,
                     INVENTORY **ihead,
                     char *invinloc_order_by)
{
    INVENTORY *ip;
    mocaDataRes *res = NULL;
    mocaDataRow *row;
    char lodnum[LODNUM_LEN+1];
    long ret_status;

    char buffer[2000];
    sLog(0, "    Loading inventory information at location...");

    /*
     * We grab the wrkref so that we can evaluate whether or not
     * this inventory is any good for our uses.  Inventory with the
     * wrkref applied already will cause issues later when we go to apply
     * the new wrkref to it.
     */
    sprintf(buffer,
        "select il.lodnum, il.asset_typ, id.orgcod,"
        "       id.revlvl, id.supnum, id.lotnum, id.untcas, id.untpak, "
        "       id.invsts, id.wrkref, "
        "       sum(id.untqty) untqty, "
        "       sum(id.catch_qty) catch_qty, "
        "       id.alcflg, "
        "       id.distro_id "
        "  from invdtl id, invsub ivs, invlod il "
        " where id.subnum = ivs.subnum "
        "   and il.lodnum = ivs.lodnum "
        "   and id.prtnum = '%s' "
        "   and id.prt_client_id = '%s' "
        "   and il.stoloc = '%s' "
        "   and il.wh_id = '%s' "
        " group by il.lodnum, il.asset_typ, id.orgcod,"
        "          id.revlvl, id.supnum, id.lotnum, id.untcas, id.untpak,"
        "          id.invsts, id.wrkref, id.alcflg, id.distro_id"
        " order by %s ",
        PickRequestList->prtnum, 
        PickRequestList->prt_client_id, 
        stoloc,
        wh_id,
        invinloc_order_by);

    ret_status = sqlExecStr(buffer, &res);
    if (eOK != ret_status)
    {
        sqlFreeResults(res);
        return(ret_status);
    }
    
    /*
     * Hold on to your hat for a second...here's what we're doing:
     *
     * We're building this internal linked list of pallets that we
     * have in the location.  We want to build this list such that
     * all partial or mixed part pallets are at the end of the list
     * and all mixed attribute pallets are at the beginning of the 
     * list...we dole out allocations of mixed attribute pallets first
     * to those who don't care
     * 
     * Our first query is limited by the part that we are going to
     * allocate...after that, we do another query (if this is a mixed
     * part area) to find any pallets containing mixed product - and 
     * we adjust their position in the list accordingly.
     */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        /* new load */
        row = sLoadInvStructure(res, row, &ip);
        sAddInvToList(ihead, ip);
    }
    sqlFreeResults(res);
    res = NULL;

    if (strcmp(area->pckcod, PCKCOD_MIXABLE) == 0)
    {
        /* Now- since this is a mixed area, we have to be concerned about
           pallets that are mixed...get a list of loads that contain the 
           part we are looking for that are mixed with other parts... */
        /*
         * NOTE:  We explicitly disable the index on the 
         *        invdtl.prtnum using the ||'' syntax.
         */
        sprintf(buffer,
            "select distinct il.lodnum "
            "  from invdtl id, invsub ivs, invlod il "
            " where id.subnum = ivs.subnum "
            "   and il.lodnum = ivs.lodnum "
            "   and il.stoloc = '%s' "
            "   and il.wh_id  = '%s' "
            "   and id.prtnum||'' = '%s' "
            "   and id.prt_client_id||'' = '%s' "
            "   and id.alcflg != '%ld' "
            "   and exists "
            "       (select invsub.lodnum "
            "          from invdtl, invsub "
            "         where invdtl.subnum = invsub.subnum "
            "           and invsub.lodnum = il.lodnum "
            "           and (invdtl.prtnum != id.prtnum or "
            "                invdtl.prt_client_id != id.prt_client_id))",
            stoloc, wh_id,
            PickRequestList->prtnum, PickRequestList->prt_client_id,
            BOOLEAN_FALSE);
        
        ret_status = sqlExecStr(buffer, &res);
        if (eDB_NO_ROWS_AFFECTED == ret_status)
        {
            sqlFreeResults(res);
            return(eOK);
        }
        if (eOK != ret_status)
        {
            sqlFreeResults(res);
            return(ret_status);
        }
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            strcpy(lodnum, sqlGetString(res, row, "lodnum"));
            for (ip = *ihead; ip; ip = ip->next)
            {
            if (strcmp(ip->lodnum, lodnum) == 0)
                ip->mixedPart = 1;
            }
        }
        sqlFreeResults(res);
        res = NULL;
    }
    return(eOK);

}

static long sCheckAllocationOKAtLocation(char *stoloc, char *wh_id)
{

    char buffer[3000];
    long ret_status;
    moca_bool_t bMustHaveOrgcod;
    moca_bool_t bMustHaveRevlvl;
    moca_bool_t bMustHaveLotnum;

    mocaDataRes *res = NULL;
    mocaDataRow *row;

    /* Here is the rule for how things must work:
     *
     * 1.  If we have a mixed attribute location, then we can not
     *     pallet pick for a specific attribute out of that location
     *
     * 2.  We cannot mix allocations in a locations.  I.e. you can not
     *     both care and not care about attributes at the same time.  
     *     We could take an approach that said if we first allocated
     *     something we cared about and then attempted to allocate
     *     for an order that didn't care, then we could convert that
     *     allocate to match what the first allocation cared about.
     *     This seems pretty questionable and we will stick with the
     *     more restrictive case for first pass.
     *
     * 3.  Like rule #1, we cannot allocate a case pick for orders
     *     that care about attributes from a location which has mixed
     *     attributes and also allows pallet picking.
     */


    sLog(0, "    Checking for mix of allocation requests in location");

    /* Before we get carried away with a bunch of checks, let's do the
     * simple one first.  If the pick request doesn't care about anything,
     * then we don't care about what existing picks there are...
     */
    if (!IS_SET(PickRequestList->orgcod) &&
    !IS_SET(PickRequestList->revlvl) && !IS_SET(PickRequestList->lotnum))
    return(eOK);

    /* This query should return no more than 2 rows.  The idea is that
     * we allow completely generic allocations and one style of specific
     * allocations.  That is, if an order doesn't care about lot/org/rev..
     * it is considered "generic".  An order, however, may care about 
     * which lot it receives...which is a specific order.  What we don't 
     * allow is having one order care about a lot and another care about
     * a lot & rev and have both allocate out of the same location... 
     */

    /*
     * PR 33513
     * when there are some replenishment picking from the stoloc,
     * the pw.orgcod/revlvl/lotnum for these kinds of pick will be
     * the default value '----', we have to deal with '----' as NULL
     * value so that we can picking inventory from the stoloc location
     * otherwize it will skip the location as to the error reason : 
     * "Disqualifying location based on conflicting"
     */ 
    sprintf(buffer,
        "select distinct decode(pw.orgcod, NULL, 'XXX', "
        "                                  '%s', 'XXX', 'YYY')|| "
        "                decode(pw.revlvl, NULL, 'XXX', "
        "                                  '%s', 'XXX', 'YYY')|| "
        "                decode(pw.lotnum, NULL, 'XXX', "
        "                                  '%s', 'XXX', 'YYY'), "
        "                decode(pw.orgcod, NULL, NULL, "
        "                                  '%s', NULL, 'YYY') orgcod, "
        "                decode(pw.revlvl, NULL, NULL, "
        "                                  '%s', NULL, 'YYY') revlvl, "
        "                decode(pw.lotnum, NULL, NULL, "
        "                                  '%s', NULL, 'YYY') lotnum " 
        "  from pckwrk pw "
        " where pw.srcloc = '%s' "
        "   and pw.wh_id  = '%s' "
        "   and pw.prtnum = '%s' "
        "   and pw.prt_client_id = '%s' "
        "   and pw.appqty < pw.pckqty "
            "   and pw.pcksts != '%s' ",
            DEFAULT_ORGCOD,
            DEFAULT_REVLVL,
            DEFAULT_LOTNUM,
            DEFAULT_ORGCOD,
            DEFAULT_REVLVL,
            DEFAULT_LOTNUM,
        stoloc, 
        wh_id,
        PickRequestList->prtnum, 
        PickRequestList->prt_client_id,
            PCKSTS_COMPLETED);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
    sqlFreeResults(res);
    return(ret_status);
    }
    
    bMustHaveRevlvl = BOOLEAN_FALSE;
    bMustHaveLotnum = BOOLEAN_FALSE;
    bMustHaveOrgcod = BOOLEAN_FALSE;

    if (eOK == ret_status)
    {
    /*
     * Since we don't allow any mixed allocations to occur,
     * all we need to do is look at the first row that came
     * back to determine what the request scenario at this
     * location is...
     */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        if (!sqlIsNull(res, row, "orgcod"))
        bMustHaveOrgcod = BOOLEAN_TRUE;
        if (!sqlIsNull(res, row, "revlvl"))
        bMustHaveRevlvl = BOOLEAN_TRUE;
        if (!sqlIsNull(res, row, "lotnum"))
        bMustHaveLotnum = BOOLEAN_TRUE;
    }

    /* If the only pick out there is generic where
     * nothing is needed, then whatever is being requested
     * will be OK...
     */
    if (!bMustHaveOrgcod && !bMustHaveRevlvl && !bMustHaveLotnum)
    {
        sqlFreeResults(res);
        return(eOK);
    }

    /*
     * We've eliminated the generic pick request above...the
     * game now just becomes that we have to match what the 
     * existing pckwrks have against what we are requesting...
     * if there is any mismatch, we've got problems...
     */
    
    if ((bMustHaveOrgcod && !IS_SET(PickRequestList->orgcod)) ||
        (!bMustHaveOrgcod && IS_SET(PickRequestList->orgcod)))
    {
        sLog(0, 
         "Disqualifying location based on conflicting "
         "'specificity' on allocation request "
         "orgcod_set_on_pckwrk = %d vs."
         "orgcod_set_on_alloc_request = %d",
         bMustHaveOrgcod, IS_SET(PickRequestList->orgcod));
        sqlFreeResults(res);
        return(eDB_NO_ROWS_AFFECTED);
    }

    if ((bMustHaveLotnum && !IS_SET(PickRequestList->lotnum)) ||
        (!bMustHaveLotnum && IS_SET(PickRequestList->lotnum)))
    {
        sLog(0, 
         "Disqualifying location based on conflicting "
         "'specificity' on allocation request "
         "lotnum_set_on_pckwrk = %d vs."
         "lotnum_set_on_alloc_request = %d",
         bMustHaveLotnum, IS_SET(PickRequestList->lotnum));
        sqlFreeResults(res);
        return(eDB_NO_ROWS_AFFECTED);
    }
    if ((bMustHaveRevlvl && !IS_SET(PickRequestList->revlvl)) ||
        (!bMustHaveRevlvl && IS_SET(PickRequestList->revlvl)))
    {
        sLog(0, 
         "Disqualifying location based on conflicting "
         "'specificity' on allocation request "
         "revlvl_set_on_pckwrk = %d vs."
         "revlvl_set_on_alloc_request = %d",
         bMustHaveRevlvl, IS_SET(PickRequestList->revlvl));
        sqlFreeResults(res);
        return(eDB_NO_ROWS_AFFECTED);
    }
    }
    sqlFreeResults(res);

    sLog(0, "    Location is OK for allocation request");
    return(eOK);

}

static INVENTORY *sMarkInventoryLoadForCases(INVENTORY *head,
                                             char *invsts)
{
    /* Here we are going to find the smallest matching load and put
    **  it at the end of the list. (Our assumption is that there is
    **  only one). 
    */
    /* MITCH */
    long lodqty_min, lodqty_lst, lodqty;
    INVENTORY *ip, *ip_min;
    
    lodqty_min = 0;
    lodqty_lst = 0;
    ip_min = NULL;
    
    for(ip = head; ip; ip = ip->next)
    {
    if ((ip->casesAllocated) &&
        (sInventoryMatchesPick(PickRequestList, ip->attr, invsts))) 
    {
        /* If we find one matching load with cases allocated we
        ** are done.  That is the case pick pallet and it won't
        ** be pallet picked. 
        */
        lodqty_min = 0;
        break;
    }
    lodqty = sGetLoadUntqty(ip);
    if (lodqty < lodqty_min || lodqty_min == 0)
    {
        ip_min = ip;
        lodqty_lst = lodqty_min;
        lodqty_min = lodqty;
    }
    }
    if (lodqty_min != 0 && lodqty_lst != 0 && lodqty_lst > lodqty_min)
    {
    /* We are here if there is a less than a full pallet
    ** on one of the loads. We will mark it for as cases
    ** allocated to prevent load picking from this pallet
    ** (unless it is the last pallet in the location.
    */
    ip_min->casesAllocated = 1;
    
   }
   else
   {
       ip_min = NULL;
   }
    
    
    return(ip_min);
}

/* sFindInventory
 * -
 */
static long sFindInventory(INVENTORY **head_i,
               char *orgcod,
               char *lotnum,
               char *revlvl,
               char *supnum,
               char *invsts,
               long untcas,
               long untpak,
               long *pckqty,
               double *pck_catch_qty,
               char *lodlvl,
               long createPick,
               moca_bool_t mult_lvl_pck_flg)
{

    INVENTORY *head, *ip, *last_ip, *ip_min, *ip_tail;
    INVENTORY_ATTR *ap;
    long qty_to_pick, lodqty, lodqty_min;
    double catch_qty_to_pick;

    head = *head_i;
    if (strncmp(lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0)
    {
        /* Ok here is where we need to be careful. We want to allocate
        ** load picks first from the load that is not a full pallet.
        ** This will allow them to operationally always pick cases from the 
        ** partial pallet first.  So we find the partial and put that
        ** at the end of the list.
        */

        if (head->next != NULL &&  /* Do we have more than 1 load? */
            mult_lvl_pck_flg) /* Do we support muli-level picking here. */
        {
            /* Here we are going to find the smallest matching load and put
            ** it at the end of the list. (Our assumption is that there is
            ** only one). 
            */

            /* This will identify the smallest pallet and mark it as being
            ** allocated for case picking.
            */
            ip_min = sMarkInventoryLoadForCases(head, invsts);
            for(ip = head;ip;ip=ip->next)
            {
                ip_tail = ip;
            }
            if (ip_min != NULL && ip_min != ip_tail)
            {
                /* This move it to the end of the linked list. */
                if (ip_min->next)
                ip_min->next->last = ip_min->last;
                if(ip_min->last != NULL) 
                ip_min->last->next = ip_min->next;
                ip_min->last = ip_tail;
                ip_tail->next = ip_min;
                ip_min->next = NULL;
            }
        }

        /* Pallet pick */
        for (ip = head; ip && *pckqty > 0; ip = ip->next)
        {
            /* We're looking for a load pick...can't use it
               if it's mixed or if we've already grabbed a case pick */
            if (ip->casesAllocated ||
                ip->palletAllocated ||
                ip->piecesAllocated ||
                ip->mixedPart ||
                ip->mixedInvsts ||
                ip->mixedAlcflg)
            {
                sLog(0, 
                   "      Skipping load pick due "
                   "to prv alloc or mix (%d%d%d%d%d%d)",
                   ip->casesAllocated, ip->palletAllocated,
                   ip->piecesAllocated, ip->mixedPart, ip->mixedInvsts,
                           ip->mixedAlcflg);
                continue;
            }
            
            if (ip->mixedAttribute) 
            {
                if (strlen(orgcod) && ip->mixedOrgcod)
                {
                    sLog(0, "      Skipping due to mixed origin code");
                    continue;
                }
                if (strlen(revlvl) && ip->mixedRevlvl)
                {
                    sLog(0, "      Skipping due to mixed revlvl ");
                    continue;
                }
                if (strlen(lotnum) && ip->mixedLotnum)
                {
                    sLog(0, "      Skipping due to mixed lotnum");
                    continue;
                }
                if (strlen(supnum) && ip->mixedSupnum)
                {
                    sLog(0, "      Skipping due to mixed supnum");
                    continue;
                }
            }
            
            /* If we're here, we know that we're not mixed on the
               attribute that we care about...assume from here that
               it is a homogeneous pallet and make sure that the 
               attributes match... 

               NOTE:  We don't have to check about untpak or untcas
               mixing as our product mixing enforcement guarentees
               that we will not allow that type of mixing in a pallet
               picking scenario...  */
               
            if ((strlen(orgcod) && strcmp(orgcod, ip->attr->orgcod) != 0) ||
                (strlen(revlvl) && strcmp(revlvl, ip->attr->revlvl) != 0) ||
                (strlen(supnum) && strcmp(supnum, ip->attr->supnum) != 0) ||
                (strlen(lotnum) && strcmp(lotnum, ip->attr->lotnum) != 0) ||
                strcmp(invsts, ip->attr->invsts) != 0)
            {
                sLog(0, 
                   "      Skipping - homogenous pallet "
                   "but attr mismatch: %s",
                   ip->lodnum);
                continue;
            }
            
            if (!ip->attr->alcflg)
            {
                sLog(0, 
                   "      Skipping - pallet unallocatable: %s",
                   ip->lodnum);
                continue;
            }
            
            /* If a catch quantity allocation exists here and this is not
             * a catch quanity allocation, skip.  We cannot mix the two.
             */
            if(ip->catchAllocated == 1 &&
               *pck_catch_qty <= 0)
            {
                sLog(0,
                   "      Skipping - Catch allocation exists: %s",
                   ip->lodnum);
                continue;
            }
            
            /* If this is a catch quantity allocation check quantity against
             * the inventory catch quantity instead of unit quantity
             */
            /* At this point, we're good to go if the quantity works */
            
            if(*pck_catch_qty > 0 &&
               ip->catch_pal <= *pck_catch_qty)
            {
                sLog(0, "      Matched load: %s as pallet pick by catch", 
                    ip->lodnum);
                ip->palletAllocated = 1;
                ip->catchAllocated = 1;
                *pck_catch_qty -= ip->catch_pal;
                if(*pck_catch_qty <= 0)
                {
                    *pckqty = 0;
                }
                continue;
            }
            else if (ip->untpal <= *pckqty)
            {
                sLog(0, "      Matched load: %s as pallet pick", 
                   ip->lodnum);
                ip->palletAllocated = 1;
                *pckqty -= ip->untpal;
                continue;
            }
        }
    }
    else if (strncmp(lodlvl, LODLVL_SUB, LODLVL_LEN) == 0)
    {
        /* Ok here is where we need to be careful. We want to allocate
        ** case picks first from the load that is not a full pallet.
        ** This will allow them to operationally always pick from the 
        ** partial pallet first.  So we find the partial and put that
        ** at the head of the list.
        */

        if (head->next != NULL) /* Do we have more than 1 load? */
        {
            /* Here we are going to find the smallest matching load and put
            ** it at the front of the list. (Our assumption is that there is
            ** only one). 
            */
            lodqty_min = 0;
            for (ip = head; ip; ip = ip->next)
            {
                if (!(ip->palletAllocated) &&
                    sInventoryMatchesPick(PickRequestList, ip->attr, invsts))
                {
                    lodqty = sGetLoadUntqty(ip);
                    if (lodqty < lodqty_min || 
                        lodqty_min == 0)
                    {
                        ip_min = ip;
                        lodqty_min = lodqty;
                    }
                }
            }
            if (lodqty_min != 0 && ip_min != head)
            {
                /* This move it to the front of the linked list. */
                if (ip_min->last)
                    ip_min->last->next = ip_min->next;
                if(ip_min->next != NULL)
                    ip_min->next->last = ip_min->last;
                ip_min->next = head;
                head->last = ip_min;
                ip_min->last = NULL;
                head = ip_min;
                *head_i = head;
            }
        }
        /* Now we can go through the list. */
        for (ip = head; ip && *pckqty > 0; ip = ip->next)
        {
            /* Case picking */
            if (ip->palletAllocated)
            {
                sLog(0, "      Skipping pallet: %s, already allocated",
                    ip->lodnum);
                continue;
            }

            sLog(0, "       Attempting to match case pick from load: %s",
               ip->lodnum);
            for (ap = ip->attr; ap && *pckqty; ap = ap->next)
            {
                if (ap->untqty == ap->comqty)
                {
                    sLog(0, "      Skipping: qty (%d) completely allocated",
                        ap->untqty);
                    continue;
                }
                if (ap->alcflg == BOOLEAN_FALSE)
                {
                    sLog(0, "      Skipping: qty (%d) unallocatable",
                        ap->untqty);
                    continue;
                }
                if ((strlen(orgcod) && strcmp(orgcod, ap->orgcod) != 0) ||
                    (strlen(revlvl) && strcmp(revlvl, ap->revlvl) != 0) ||
                    (strlen(supnum) && strcmp(supnum, ap->supnum) != 0) ||
                    (strlen(lotnum) && strcmp(lotnum, ap->lotnum) != 0) ||
                    strcmp(invsts, ap->invsts) != 0 ||
                    untcas != ap->untcas)
                {
                    sLog(0,
                        "   Skipping qty: %d, attr mismatch", ap->untqty);
                    continue;
                }
                
                if(ip->catchAllocated == 1 &&
                   *pck_catch_qty <= 0)
                {
                    sLog(0,
                       "      Skipping - Catch allocation exists: %s",
                       ip->lodnum);
                    continue;
                }                
                
                /* If this is a catch quantity allocation */
                if(*pck_catch_qty > 0)
                {
                    /* If the requested quantity exactly equals
                     * the quantity remaining in the location
                     */
                    if(*pck_catch_qty == (ap->catch_qty - ap->com_catch_qty))
                    {
                        ap->comqty = ap->untqty;
                        ap->com_catch_qty = ap->catch_qty;
                        *pckqty = 0;
                        *pck_catch_qty = 0;
                        ip->casesAllocated = 1;
                        ip->catchAllocated = 1;
                        sLog(0,"Matched case catch qty0");                          
                    }
                    /* If the requested quantity is less than 
                     * the quantity remaining in the location
                     */
                    else if(*pck_catch_qty < (ap->catch_qty - 
                                              ap->com_catch_qty))
                    {
                        catch_qty_to_pick = *pck_catch_qty;
                        ap->com_catch_qty += catch_qty_to_pick;
                        *pckqty = 0;
                        *pck_catch_qty -= catch_qty_to_pick;
                        ip->casesAllocated = 1;
                        ip->catchAllocated = 1;
                        sLog(0,"Matched case catch qty1");                         
                    }  
                    /* Else the requested quantity must be more than
                     * the quantity remaining in the location
                     */                  
                    else
                    {
                        catch_qty_to_pick = (ap->catch_qty - 
                                             ap->com_catch_qty);
                        ap->comqty = ap->untqty;
                        ap->com_catch_qty = ap->catch_qty;
                        *pck_catch_qty -= catch_qty_to_pick;
                        ip->casesAllocated = 1;
                        ip->catchAllocated = 1;
                        sLog(0,"Matched case catch qty2");                         
                    }                  
                }
                else
                {
                    catch_qty_to_pick = 0;
                    if (*pckqty <= (ap->untqty - ap->comqty))
                    {
                        qty_to_pick = ((*pckqty)/ap->untcas) * ap->untcas;
                    }
                    else 
                    {
                        qty_to_pick = 
                            ((ap->untqty - ap->comqty)/ap->untcas) * ap->untcas;
                    }
                    (*pckqty) -= qty_to_pick;
                    ap->comqty += qty_to_pick;
                    ip->casesAllocated = 1;
                    sLog(0, 
                        "      Matched case qty, qty = %d\n", 
                        qty_to_pick);                     
                }
            }
        }
    }
    else
    {
        /* For piece picks, we go after the available inventory in 
           reverse order.  The idea here is that we want to piece 
           pick off of all our garbage pallets which are naturally
           sorted at the end... */
        
        /* first get to the end of the list... */
        last_ip = head;
        for (ip = head; ip->next; ip = ip->next)
            last_ip = ip->next;
        
        /* Now, traverse the list from the end forward */
        for (ip = last_ip; ip; ip = ip->last)
        {
            if (ip->palletAllocated)
            {
                sLog(0, "      Skipping load: %s, already allocated",
                   ip->lodnum);
                continue;
            }
            
            sLog(0, "      Attempting to match piece pick from load: %s",
               ip->lodnum);
            /* Piece picking */
            for (ap = ip->attr; ap && *pckqty; ap = ap->next)
            {
                if (ap->untqty == ap->comqty)
                {
                    sLog(0,
                        "      Skipping qty (%d) completely allocated",
                        ap->untqty);
                    continue;
                }

                if (ap->alcflg == BOOLEAN_FALSE)
                {
                    sLog(0,
                        "      Skipping qty (%d), unallocatable",
                        ap->alcflg);
                    continue;
                }
                
                if ((strlen(orgcod) && strcmp(orgcod, ap->orgcod) != 0) ||
                    (strlen(revlvl) && strcmp(revlvl, ap->revlvl) != 0) ||
                    (strlen(supnum) && strcmp(supnum, ap->supnum) != 0) ||
                    (strlen(lotnum) && strcmp(lotnum, ap->lotnum) != 0) ||
                    untcas != ap->untcas ||
                    strcmp(invsts, ap->invsts) != 0 )
                {
                    sLog(0, 
                        "      Skipping qty (%d), attr mismatch",
                        ap->untqty);
                    continue;
                }
                
                if(ip->catchAllocated == 1 &&
                   *pck_catch_qty <= 0)
                {
                    sLog(0,
                       "      Skipping - Catch allocation exists: %s",
                       ip->lodnum);
                    continue;
                }                 
                
                if(*pck_catch_qty > 0)
                {
                    /* If the requested quantity exactly equals
                     * the quantity remaining in the location
                     */
                    if(*pck_catch_qty == (ap->catch_qty - ap->com_catch_qty))
                    {
                        ap->comqty = ap->untqty;
                        ap->com_catch_qty = ap->catch_qty;
                        *pckqty = 0;
                        *pck_catch_qty = 0;
                        ip->piecesAllocated = 1;
                        ip->catchAllocated = 1;
                        sLog(0,"Matched piece catch qty0");
                    }
                    /* If the requested quantity is less than 
                     * the quantity remaining in the location
                     */
                    else if(*pck_catch_qty < (ap->catch_qty - 
                                              ap->com_catch_qty))
                    {
                        catch_qty_to_pick = (ap->catch_qty - 
                                             ap->com_catch_qty);
                        ap->com_catch_qty += catch_qty_to_pick;
                        *pck_catch_qty -= catch_qty_to_pick;
                        ip->piecesAllocated = 1;
                        ip->catchAllocated = 1;
                        sLog(0,"Matched piece catch qty1");                        
                    }  
                    /* Else the requested quantity must be more than
                     * the quantity remaining in the location
                     */                  
                    else
                    {
                        catch_qty_to_pick = (ap->catch_qty - 
                                             ap->com_catch_qty);
                        ap->comqty = ap->untqty;
                        ap->com_catch_qty = ap->catch_qty;
                        *pck_catch_qty -= catch_qty_to_pick;
                        ip->piecesAllocated = 1;
                        ip->catchAllocated = 1;
                        sLog(0,"Matched piece catch qty2");                         
                    }                  
                }
                else
                {
                    if (*pckqty <= (ap->untqty - ap->comqty))
                    {
                        qty_to_pick = *pckqty;
                    }
                    else
                    {
                        qty_to_pick = (ap->untqty - ap->comqty);
                    }
                        
                    (*pckqty) -= qty_to_pick;
                    ap->comqty += qty_to_pick;
                    ip->piecesAllocated = 1;

                    sLog(0, 
                       "matched qty of %d as piece picks",
                       qty_to_pick);                        
                }
            }
        }
    }
    return(eOK);
}

/* sMatchInventoryToPick
 * - Given a pick structure and some inventory attempt
 *   to find inventory that matches the pick.
 */
static long sMatchInventoryToPick(mocaDataRes *pres,
                  mocaDataRow *prow,
                  INVENTORY **head_i,
                  moca_bool_t mult_lvl_pck_flg)
{
    INVENTORY *head;
    char orgcod[ORGCOD_LEN+1];
    char revlvl[REVLVL_LEN+1];
    char lotnum[LOTNUM_LEN+1];
    char invsts[INVSTS_LEN+1];
    char lodlvl[LODLVL_LEN+1];
    char supnum[SUPNUM_LEN+1];
    long untpak;
    long untcas;
    long pckqty;
    double pck_catch_qty;
    long specificity;

    head = *head_i;
    memset(orgcod, 0, sizeof(orgcod));
    memset(revlvl, 0, sizeof(revlvl));
    memset(lotnum, 0, sizeof(lotnum));
    memset(invsts, 0, sizeof(invsts));
    memset(supnum, 0, sizeof(supnum));

    specificity = 0;
    if (!sqlIsNull(pres, prow, "orgcod"))
    {
        strcpy(orgcod, sqlGetString(pres, prow, "orgcod"));
    }
    if (!sqlIsNull(pres, prow, "revlvl"))
    {
        strcpy(revlvl, sqlGetString(pres, prow, "revlvl"));
    }
    if (!sqlIsNull(pres, prow, "lotnum"))
    {
        strcpy(lotnum, sqlGetString(pres, prow, "lotnum"));
    }
    if (!sqlIsNull(pres, prow, "supnum"))
    {
        strcpy(supnum, sqlGetString(pres, prow, "supnum"));
    }
    strcpy(invsts, sqlGetString(pres, prow, "invsts"));
    strcpy(lodlvl, sqlGetString(pres, prow, "lodlvl"));
    untpak = sqlGetLong(pres, prow, "untpak");
    untcas = sqlGetLong(pres, prow, "untcas");
    pckqty = sqlGetLong(pres, prow, "pckqty");
    pck_catch_qty = sqlGetFloat(pres, prow, "pck_catch_qty");

    sFindInventory(&head,
           orgcod, 
           lotnum,
           revlvl,
           supnum,
           invsts,
           untcas,
           untpak,
           &pckqty,
           &pck_catch_qty,
           lodlvl,
           0,
           mult_lvl_pck_flg);
    *head_i = head;
    if (pckqty > 0)
    {
        sLog(0, 
           "Unable to account for all outstanding picks..."
           "location is ineligible for new pick creation");
        return(eDB_NO_ROWS_AFFECTED);
    }
    return(eOK);
}

static long sMatchUncommittedPicks(PCKWRK_DATA **pick,
                   INVENTORY **head_i,
                   moca_bool_t mult_lvl_pck_flg)
{
    PCKWRK_DATA *ptr;
    INVENTORY *head;
    long pckqty;
    double pck_catch_qty;
    long exit_loop;

    head = *head_i;
    exit_loop = 0;
    for (pckqty = 0, pck_catch_qty = 0.0, ptr = *pick; !exit_loop && ptr; ptr = ptr->next)
    {
        if (strcmp((*pick)->cmbcod, ptr->cmbcod) == 0)
        {
            *pick = ptr;
            pckqty += ptr->pckqty;
            pck_catch_qty += ptr->pck_catch_qty;
        }
        else
        {
            exit_loop = 1;
        }
    }

    sFindInventory(&head,
           (*pick)->orgcod, 
           (*pick)->lotnum,
           (*pick)->revlvl,
           (*pick)->supnum,
           (*pick)->invsts,
           (*pick)->untcas,
           (*pick)->untpak,
           &pckqty,
           &pck_catch_qty,
           (*pick)->lodlvl,
           0,
           mult_lvl_pck_flg);

    *head_i = head;
    if (pckqty > 0)
    {
        sLog(0, 
           "Unable to account for all outstanding picks..."
           "location is ineligible for new pick creation");
        return(eDB_NO_ROWS_AFFECTED);
    }

    return(eOK);
}

static long sRemovePreviousAllocations(char *stoloc,
                       char *wh_id,
                       PICK_POLICY_DATA *policy,
                       INVENTORY **invHead_i,
                       char *invsts)
{
    long ret_status;
    moca_bool_t  mult_lvl_pck_flg;
    char buffer[2000];
    PCKWRK_DATA *pick;
    INVENTORY *invHead, *ip;
    short catch_allocations_exist_at_location;

    mocaDataRes *res = NULL;
    mocaDataRow *row;

    ip = NULL;
    invHead = *invHead_i;

    if (policy->area_lodflg && (policy->area_subflg || policy->area_dtlflg))
    {
        mult_lvl_pck_flg = TRUE;
    }
    else
    {
        mult_lvl_pck_flg = FALSE;
    }
    
    sprintf(buffer,
        "select 1 pckwrk_cnt "
        "  from pckwrk "
        " where srcloc = '%s' "
        "   and wh_id  = '%s' "
        "   and prtnum = '%s' "
        "   and prt_client_id = '%s' "
        "   and appqty < pckqty "
        "   and pcksts != '%s' "
        "   and pck_catch_qty > 0",
        stoloc, wh_id,
        PickRequestList->prtnum, 
        PickRequestList->prt_client_id,
        PCKSTS_COMPLETED);
        
    ret_status = sqlExecStr(buffer, NULL);

    if(eOK != ret_status &&
       eDB_NO_ROWS_AFFECTED != ret_status)
    {
        return(ret_status);
    }
    else if(ret_status == eOK)
    {
        catch_allocations_exist_at_location = 1;
    }
    else
    {
        catch_allocations_exist_at_location = 0;
    }
    
    sprintf(buffer, 
        "select decode(orgcod, NULL, 1, 0)+ "
            "  decode(revlvl, NULL, 1, 0)+ "
            "  decode(lotnum, NULL, 1, 0) specificity, "
            "  decode(lodlvl, d.detail, d.piece, cmbcod) cmbcod, "
            "  orgcod, lotnum, revlvl, supnum, lodlvl, "
            "  invsts, untcas, untpak, "
            "  sum(pckqty - appqty) pckqty, "
            "  sum(pck_catch_qty - app_catch_qty) pck_catch_qty "
        " from pckwrk, "
            "  (select 'D' detail, "
                    "  'PIECE' piece " 
                "from dual) d "
        " where srcloc = '%s' "
        "   and wh_id  = '%s' "
        "   and prtnum = '%s' "
        "   and prt_client_id = '%s' "
        "   and appqty < pckqty "
        "   and pcksts != '%s' "
        " group by orgcod, lotnum, revlvl, supnum, lodlvl, "
        "          invsts, untcas, untpak, "
        "          decode(lodlvl, d.detail, d.piece, cmbcod) "
        " order by lodlvl, 1, 2",
        stoloc, wh_id,
        PickRequestList->prtnum, 
        PickRequestList->prt_client_id,
        PCKSTS_COMPLETED);

    ret_status = sqlExecStr(buffer,&res);
    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
    {
        sqlFreeResults(res);
        return(ret_status);
    }
    else if(ret_status == eOK)
    {
        for(ip = invHead; ip; ip = ip->next)
        {
            ip->catchAllocated = catch_allocations_exist_at_location;
            ip->allocationsExistAtLocation = 1;
        }        
        ip = NULL;        
    }
    else
    {
        for(ip = invHead; ip; ip = ip->next)
        {
            ip->allocationsExistAtLocation = 0;
        }        
        ip = NULL;
        if(res) sqlFreeResults(res);
        res = NULL;
    }

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        ret_status = sMatchInventoryToPick(res,
                           row, 
                           &invHead,
                           mult_lvl_pck_flg);
        if (eOK != ret_status)
        {
            sqlFreeResults(res);
            return(ret_status); /* We are over-allocated */
        }
        *invHead_i = invHead;
        ip = invHead;
    }
    sqlFreeResults(res);
    res = NULL;

    /* next check any allocations residing in memory (not yet committed) */
    for (pick = PickFoundList; pick; pick = pick->next)
    {
        if (strcmp(pick->srcloc, stoloc) == 0 &&
            strcmp(pick->wh_id, wh_id) == 0)
        {
            ret_status = sMatchUncommittedPicks(&pick,
                                &invHead,
                                mult_lvl_pck_flg);
            if (eOK != ret_status)
            {
                return(ret_status); /* We are over-allocated */
            }
            *invHead_i = invHead;
            ip = invHead;
        }
    }
    if (ip == NULL && mult_lvl_pck_flg)
    {
        ip = sMarkInventoryLoadForCases(invHead, invsts);
    }

    return(eOK);
}

static long sInventoryMatchesPick(PCKWRK_DATA *pick,
                  INVENTORY_ATTR *attr,
                                  char *invsts)
{
   
    if (attr->alcflg == BOOLEAN_FALSE)
    {
        sLog(0,
             "Inventory not allowed for allocation:  alcflg = %d",
             attr->alcflg);
        return (BOOLEAN_FALSE);
    }

    if ((strncmp(attr->invsts, 
         invsts, INVSTS_LEN) == 0) &&
    (pick->alloc_cas == 0 || 
     attr->untcas == pick->alloc_cas) &&
    (pick->alloc_pak == 0 || 
     attr->untpak == pick->alloc_pak) &&
    (NOT_SET(pick->orgcod) ||
     strncmp(attr->orgcod, 
         pick->orgcod, ORGCOD_LEN) == 0) &&
    (NOT_SET(pick->revlvl) ||
     strncmp(attr->revlvl, 
         pick->revlvl, REVLVL_LEN) == 0) &&
    (NOT_SET(pick->supnum) ||
     strncmp(attr->supnum, 
         pick->supnum, SUPNUM_LEN) == 0) &&
    (NOT_SET(pick->lotnum) ||
     strncmp(attr->lotnum, 
         pick->lotnum, LOTNUM_LEN) == 0) &&
    ((NOT_SET(attr->distro_id) && NOT_SET(pick->distro_id)) ||
     strncmp(attr->distro_id, 
         pick->distro_id, DISTRO_ID_LEN) == 0))
    {
    return(BOOLEAN_TRUE);
    }
    else
    {
    sLog(0, 
           "Inventory does not match pick, pick=attr pairs follow:");
    sLog(0, 
           "sts: %s=%s, u/c: %d=%d, u/p: %d=%d, org: %s=%s, rev: %s=%s"
           "sup: %s=%s,distro_id: %s=%s, lot: %s=%s",
           pick->invsts, attr->invsts,
           pick->alloc_cas, attr->untcas,
           pick->alloc_pak, attr->untpak,
           pick->orgcod, attr->orgcod,
           pick->revlvl, attr->revlvl,
           pick->supnum, attr->supnum,
           pick->distro_id, attr->distro_id,
           pick->lotnum, attr->lotnum);
    return(BOOLEAN_FALSE);
    }
}

static void sClearInventoryFlags(INVENTORY *o_ip, INVENTORY_ATTR *o_ap, 
                 long picked_qty,
                 double picked_catch_qty)
{
    INVENTORY *ptr;
    
    for (ptr = o_ip; ptr; ptr = ptr->next)
    {
        INVENTORY_ATTR *ap;
        long qty;
        double catch_qty;
        for (ap = o_ap; ap; ap = ap->next)
        {
            if (ap->potentialAllocation)
            {
                /* If all the catch quantity was allocated
                 * then all of the unit quantity was also allocated
                 */
                if(picked_catch_qty > (ap->catch_qty - ap->com_catch_qty))
                {
                    catch_qty = (ap->catch_qty - ap->com_catch_qty);
                    qty = (ap->untqty - ap->comqty);
                }
                /* If less than all of the catch quantity was allocated
                 * then no unit quantity was allocated
                 */
                else if(picked_catch_qty > 0)
                {
                    catch_qty = picked_catch_qty;
                    qty = 0;
                }
                /* Not a catch quantity allocation but we may still have
                 * allocated all of the available unit quantity
                 */
                else if (picked_qty > (ap->untqty - ap->comqty))
                {
                    qty = (ap->untqty - ap->comqty);
                }                
                else
                {
                    qty = picked_qty;
                }
                
                ap->comqty += qty;
                ap->com_catch_qty += catch_qty;
                ap->potentialAllocation = 0;
                picked_qty -= qty;
                picked_catch_qty -= catch_qty;
            }
        }
    }
    return;
}

static void sShowInventoryNode(INVENTORY *ip)
{
    INVENTORY_ATTR *ap;

    /* Just loop through and show what we know about the inventory 
       in the location */

    if (!ip)
        return;
    
    if (!(misGetTraceLevel() & T_FLOW))
        return;

    sLog(0, "      Lodnum: %s, mixed part: %d, "
        "mixed invsts: %d, mixed alcflg: %d, mixed attribute: %d",
        ip->lodnum, ip->mixedPart, ip->mixedInvsts, ip->mixedAlcflg,
        ip->mixedAttribute);
    
    if (ip->palletAllocated || ip->casesAllocated || ip->piecesAllocated)
    {
        sLog(0,
            " >Marked as allocated: pal: %d, cas: %d, pce: %d, catch: %d",
            ip->palletAllocated, ip->casesAllocated,
            ip->piecesAllocated, ip->catchAllocated);
    }
    if (ip->mixedAttribute)
    {
        sLog(0, 
            "         >Marked mixed attr: lot:%d org:%d rev:%d sup:%d  "
            "u/c:%d u/pk:%d",
            ip->mixedLotnum, ip->mixedOrgcod, ip->mixedRevlvl,
            ip->mixedSupnum, ip->mixedUntcas, ip->mixedUntpak);
    }
    for (ap = ip->attr; ap; ap = ap->next)
    {
        sLog(0,
            "           - Org:%s, Rev: %s, Sup: %s, Lot: %s, Sts: %s, Alcflg: %d",
            ap->orgcod, ap->revlvl, ap->supnum, ap->lotnum, ap->invsts, ap->alcflg);
        sLog(0, 
            "             u/c: %d u/pk: %d  qty: %d,  comqty: %d",
            ap->untcas, ap->untpak, ap->untqty, ap->comqty);
    }    
    return;
}

static void sFreeInventoryList(INVENTORY *head)
{
    INVENTORY_ATTR *ap, *last_ap;
    INVENTORY *ip, *last_ip;

    last_ip = NULL;
    for (ip = head; ip; last_ip = ip, ip = ip->next)
    {
    last_ap = NULL;
    for (ap = ip->attr; ap; last_ap = ap, ap = ap->next)
    {
        if (last_ap)
        free(last_ap);
    }
    if (last_ap)
        free(last_ap);

    if (last_ip)
        free(last_ip);
    }
    if (last_ip)
    free(last_ip);
    return;
}

static long sForceFullCasesForPalletPick(PCKWRK_DATA *pck)
{
    static mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;

    long force_full = 0;
    long ret_status;
    char buffer[300];

    /* Always force if the splflg is set to false */
    if (pck->splflg == 0)
    return(1==1);

    if (strlen(pck->ship_id))
    {
        if (res == NULL)
        {
            misTrc(T_FLOW, 
                   "Loading policy to determine whether or not we force full"
                   "cases on pallet picks");

            sprintf(buffer,
                    "select wh_id, rtnum1 "
                    "  from poldat_view "
                    " where polcod = '%s' "
                    "   and polvar = '%s' "
                    "   and polval = '%s' "
                    "   and rtnum1 = 1 ",
                    POLCOD_ALLOCATE_INV,
                    POLVAR_MISC,
                    POLVAL_ALLOCINV_FORCE_FULL);

            ret_status = sqlExecStr(buffer, &res);
            if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
            {
                sqlFreeResults(res);
                res = NULL;
            }

            /* Cache the policies. */
            misFlagCachedMemory((OSFPTR) sqlFreeResults, res);
        }
        
        force_full = 0;
        if (res != NULL)
        {
            for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                if (strcmp(sqlGetString(res, row, "wh_id"), pck->wh_id) == 0)
                    force_full = sqlGetLong(res, row, "rtnum1");
            }
        }
    
    return(force_full);
    }
    return(1==0);   /* Return false by default */
}

static long sProcessComplexAllocation(PICK_POLICY_DATA *policy,
                      char *stoloc,
                      char *devcod,
                      char *invsts,
                      char *pcklvl,
                      char *pcktyp,
                      char *cmbcod,
                      moca_bool_t splflg,
                      char *fifdte,
                      AREA_INFO *area,
                      long alloc_loc_flg,
                      char *wh_id,
                      char *invinloc_order_by,
                      moca_bool_t *isAlloLocFailed)
{
    long ret_status;
    long picked_qty;
    double picked_catch_qty;

    moca_bool_t lodpck_ok, subpck_ok, dtlpck_ok;
    long picked_product;

    INVENTORY *invHead, *ip;
    INVENTORY_ATTR *ap;

    char* dyn_buffer = NULL;
    char* currently_allocated_locations = NULL;
    
    sLog(0, "    Evaluating location: %s/%s for picking...", wh_id, stoloc);

    if (strstr(PickRequestList->ordinv, ORDINV_ABSOLUTE_STR)) 
    {
        sLog(0, "    Evaluating location: %s for correct FEFO Date...", stoloc);

        currently_allocated_locations = sBuildCurrentlyAllocatedLocations();
        
        if (currently_allocated_locations == NULL)
            return (eNO_MEMORY);
            
        dyn_buffer = calloc(1, 1024 + 
            sizeof(PickRequestList->prtnum) +
            sizeof(PickRequestList->prt_client_id) +
            sizeof(stoloc) +
            sizeof(policy->arecod) +
            sizeof(invsts) +
            sizeof(PickRequestList->ordinv) +
            sizeof(devcod) +
            sizeof(pcklvl) +
            sizeof(pcktyp) +
            sizeof(wh_id) +
            strlen(currently_allocated_locations));
            

        sprintf(dyn_buffer, "validate location for absolute inventory dates"
                "   where prtnum = '%s' "
                "     and prt_client_id = '%s' "
                "     and stoloc = '%s'"
                "     and arecod = '%s'"
                "     and invsts = '%s'"
                "     and ordinv = '%s'"
                "     and devcod = '%s'"
                "     and pcklvl = '%s'"
                "     and pcktyp = '%s'"
                "     and wh_id  = '%s'"
                "     and min_shelf_hrs = %ld"
                "     and currently_allocated_locations = '%s'",
                PickRequestList->prtnum, 
                PickRequestList->prt_client_id, 
                stoloc,
                policy->arecod, 
                invsts,
                PickRequestList->ordinv,
                devcod,
                pcklvl,
                pcktyp,
                wh_id,
                PickRequestList->min_shelf_hrs,
                currently_allocated_locations);

        ret_status = srvInitiateCommand(dyn_buffer, NULL);
        if(currently_allocated_locations)
        {
            free(currently_allocated_locations);
        }

        if(dyn_buffer)
        {
            free(dyn_buffer);
        }

        if ((ret_status != eOK) 
         && (ret_status != eABSOLUTE_ALLOCATION_VIOLATION))
        {
                return (ret_status);
        }
        /* Indicates that there is another location 
             * with lower FEFO Date and this is no good
         * We will return no data found so the allocation 
             * goes and gets the other locations/areas 
             */
        else if (ret_status == eABSOLUTE_ALLOCATION_VIOLATION)
        {
                sLog(0, "    Location: %s invalid due to newer "
                            "FEFO Date...Than locations in another area", stoloc);
            return(ret_status);  
        }
    }    

    ret_status = sCheckAllocationOKAtLocation(stoloc, wh_id);
    if (eOK != ret_status)
        return(ret_status);

    picked_product = 0;

    /* Location is locked, now load up inventory at location */
    invHead = NULL;
    ret_status = sLoadInventoryAtLocation(stoloc,
                      wh_id,
                      policy,
                      &invHead,
                      invinloc_order_by);
    if (eOK != ret_status)
        return(ret_status);

    /* Next, account for what is already allocated */
    ret_status = sRemovePreviousAllocations(stoloc,
                        wh_id,
                        policy,
                        &invHead,
                        invsts);

    for (ip = invHead; ip; ip = ip->next)
        sShowInventoryNode(ip);

    if (eOK != ret_status)
    {
        sLog(0, "    Exiting due to problems with previous allocations");
        return(ret_status);
    }
    
    /* If this location has catch allocations and this is not a catch
     * allocated pick request the, location is invalid.
     * If this location has unit quanity allocations and this is not a
     * catch allocated pick request, the location is invalid.
     */
    if(invHead->allocationsExistAtLocation == 1 && 
       (PickRequestList->pck_catch_qty > 0 &&
        invHead->catchAllocated == 0) ||
       (PickRequestList->pck_catch_qty <= 0 &&
        invHead->catchAllocated == 1))
    {
        sLog(0, "    Location invalid due to catch allocation conflict.");
        return(eDB_NO_ROWS_AFFECTED);    
    }

    /* Now...we can go through and attempt do some allocation */
    lodpck_ok = policy->lodflg;
    subpck_ok = policy->subflg;
    dtlpck_ok = policy->dtlflg;

    if (pcklvl && strlen(pcklvl))
    {
        if (lodpck_ok && strncmp(pcklvl, LODLVL_LOAD, LODLVL_LEN) == 0)
            lodpck_ok = 1;
        else
            lodpck_ok = 0;
            
        if (subpck_ok && strncmp(pcklvl, LODLVL_SUB, LODLVL_LEN) == 0)
            subpck_ok = 1;
        else
            subpck_ok = 0;
            
        if (dtlpck_ok && strncmp(pcklvl, LODLVL_DTL, LODLVL_LEN) == 0)
            dtlpck_ok = 1;
        else
            dtlpck_ok = 0;
    }

    if (dtlpck_ok && splflg == BOOLEAN_FALSE)
    {
        sLog(0, "    Disallowing piece picking due to split flag being false");
        dtlpck_ok = 0;
    }

    if (lodpck_ok)
    {
        sLog(0, "    Attemping to process pallet picks...");
        /* Process for pallets */
        for (ip = invHead; PickRequestList && ip; ip = ip->next)
        {
            if (ip->mixedPart || ip->mixedInvsts || ip->mixedAlcflg)
            {
                sLog(0,
                    " Skipping pallet(%s) for pallet pick due "
                    "to mixed part, mixed inventory status "
                    "or mixed allocation flag ",
                    ip->lodnum);
                continue;
            }
            if (ip->casesAllocated || 
                ip->palletAllocated || ip->piecesAllocated)
            {
                sLog(0,
                    " Skipping pallet(%s) for pallet pick due "
                    "to previous allocation of inventory on this pallet ",
                    ip->lodnum);
                continue;
            }

            /* If it is a replenishment pick, do not worry
             * if the untpal for the order line differs
             * from the units on this pallet.
             */
            if ((PickRequestList->alloc_pal > 0 &&
                 PickRequestList->alloc_pal != ip->untpal) &&
                (strcmp(pcktyp, ALLOCATE_REPLENISH) != 0))
            {
                sLog(0,
                    " Skipping pallet(%s) for pallet pick due "
                    "to mismatch in untpal on order line ",
                    ip->lodnum);
                continue;
            }

            if (ip->mixedAttribute)
            {
                /* If we have a mixed attribute pallet, then
                   we can not pallet pick it if it is mixed on
                   the attribute that we are looking for...
                   Also, we never allow a mix on untcas or untpak
                   to be load picked. This is because we always store
                   the untcas and untpak on the pckwrk (we don't do
                   this for the other attributes) and all picked
                   inventory needs to match the pckwrk entry. */
                if ((ip->mixedOrgcod &&
                        IS_SET(PickRequestList->orgcod)) ||
                    (ip->mixedRevlvl &&
                        IS_SET(PickRequestList->revlvl)) ||
                    (ip->mixedSupnum &&
                        IS_SET(PickRequestList->supnum)) ||
                    (ip->mixedLotnum &&
                        IS_SET(PickRequestList->lotnum)) ||
                    (ip->mixedUntcas) ||
                    (ip->mixedUntpak))
                {
                    sLog(0,
                     "      Skipping pallet(%s) for pallet pick due "
                     "to mixed attribute ",
                     ip->lodnum);
                    continue;
                }
            }
            
            /* Next, if they requested something special, make sure it
               matches */
            if (!sInventoryMatchesPick(PickRequestList, ip->attr, invsts))
            {
                sLog(0, 
                    "    Skipping pallet(%s) for pick as "
                    "requested attribute does not match ",
                    ip->lodnum);
                continue;
            }
            
            /* If they specified either a specific untpak or untcas, then
               we'd better make sure they all divide out evenly or we 
               can't go pickin the pallet... */
            if (PickRequestList->untcas > 0 &&
                (ip->untpal % PickRequestList->untcas) != 0)
            {
                sLog(0,
                    "    Skipping pallet(%s) for pallet pick as "
                    "qty (%d) is not multiple of requested untcas (%d)", 
                    ip->lodnum, ip->untpal, PickRequestList->untcas);
                continue;
            }
            if (sForceFullCasesForPalletPick(PickRequestList))
            {
                /* If we are forcing full cases for pallet picks,
                 * then you have to make sure the quantities for
                 * each untcas combination divide out evenly
                 */
                for (ap = ip->attr; ap; ap = ap->next)
                {
                    if (ap->untqty % ap->untcas != 0)
                    {
                        sLog(0,
                            "   Skipping pallet(%s) for pallet pick as "
                            " qty(%d) is not multiple of untcas(%d) and ",
                            ip->lodnum, ap->untqty, ap->untcas);
                        sLog(0,
                            "   splflg is false. ");
                        continue;
                    }
                }
            }
            if (PickRequestList->untpak > 0 &&
                (ip->untpal % PickRequestList->untpak) != 0)
            {
                sLog(0,
                    "    Skipping pallet(%s) for pallet pick as "
                    "qty (%d) is not multiple of requested untpak (%d)", 
                    ip->lodnum, ip->untpal, PickRequestList->untpak);
                continue;
            }                 
            
            /* I think we're good to go here...go ahead and formulate a 
               call to sProcessPalletPick - notice that we are taking a 
               fairly non-agressive approach here...we simply looping 
               through here and calling iteratively...in an ideal world,
               we might pile up all the common pallets together and 
               let ProcessPalletPick pluck them all off...it's either a 
               loop here or a loop there and it's a bit of work to put
               the loop in here (not to mention all the checking that has
               to be done to ensure that they are the same...)  Unless
               proven to be too inefficient...we let this loop continue.. */
            ret_status = sProcessPalletPick(stoloc,
                            devcod,
                            invsts,
                            ip->untpal,
                            ip->catch_pal,
                            ip->attr->untcas,
                            ip->attr->untpak,
                            policy->arecod,
                            pcktyp,
                            cmbcod,
                            fifdte,
                            area,
                            alloc_loc_flg,
                            ip->attr->lotnum,
                            ip->attr->revlvl,
                            ip->attr->supnum,
                            ip->attr->orgcod,
                            ip->mixedAttribute,
                            wh_id,
                            ip->asset_typ,
                            invHead,
                            isAlloLocFailed);
            if (ret_status == eOK)
            {
                ip->palletAllocated = 1;
                picked_product = 1;
            }
        }
    }

    if (subpck_ok && PickRequestList)
    {
        long avail_qty;
        double avail_catch_qty;
        INVENTORY *o_ip;
        INVENTORY_ATTR *o_ap;

        sLog(0, "    Attempting to process case picks.");
        /* Process for cases */
        o_ap = NULL;
        o_ip = NULL;
        avail_qty = 0;
        avail_catch_qty = 0;
        picked_qty = 0;
        picked_catch_qty = 0;

        for (ip = invHead; ip && PickRequestList; ip = ip->next)
        {
            INVENTORY_ATTR *ap;

            if (ip->palletAllocated)
            continue;

            for (ap = ip->attr; ap && PickRequestList;ap = ap->next)
            {
                if (!sInventoryMatchesPick(PickRequestList, ap, invsts))
                    continue;
            
                /* Inventory matches the pick (or works for the pick)...
                   now just check if we've accumulated all that we can. */
            
                if (!o_ap)
                {
                    o_ap = ap;
                    o_ip = ip;
                    avail_qty = 
                        ap->untcas * 
                        (long)floor((ap->untqty - ap->comqty)/ap->untcas);
                    avail_catch_qty = ap->catch_qty - ap->com_catch_qty;
                    ap->potentialAllocation = 1;
                }
                else
                {
                    if (ap->untcas != o_ap->untcas)
                    {
                        long saved_pckqty = 0;
                        double saved_pck_catch_qty = 0;
                        /* Ok...we've switched to a new untcas...time
                           to allocate */
                        do
                        {
                            picked_qty = 0;
                            picked_catch_qty = 0;
                            
                            sLog(0,
                             "    Loc: %s, Available: %d, u/c: %d",
                             stoloc, avail_qty, o_ap->untcas);
                            
                            ret_status = sProcessCasePick(stoloc,
                                          policy->arecod,
                                          devcod,
                                          invsts,
                                          avail_qty,
                                          avail_catch_qty,
                                          o_ap->untcas,
                                          o_ap->untpak,
                                          &picked_qty,
                                          &picked_catch_qty,
                                          pcktyp,
                                          cmbcod,
                                          fifdte,
                                          area,
                                          alloc_loc_flg,
                                          o_ap->lotnum,
                                          o_ap->revlvl,
                                          o_ap->supnum,
                                          o_ap->orgcod,
                                          wh_id,
                                          isAlloLocFailed);
                            saved_pckqty += picked_qty;
                            saved_pck_catch_qty += picked_catch_qty;
                            avail_qty -= picked_qty;
                            avail_catch_qty -= picked_catch_qty;
                            sLog(0, "    Resulting picked qty: %d",
                                picked_qty);
                            
                        } while (PickRequestList && 
                                 (picked_qty > 0 || picked_catch_qty > 0) &&
                                 avail_qty > o_ap->untcas);
                    
                        sClearInventoryFlags(o_ip, o_ap, saved_pckqty, saved_pck_catch_qty);
                        if (saved_pckqty > 0 || saved_pck_catch_qty > 0)
                            picked_product = 1;

                        avail_qty = ap->untqty - ap->comqty;
                        avail_catch_qty = ap->catch_qty - ap->com_catch_qty;
                        o_ap = ap;
                        o_ip = ip;
                    }
                    else
                    {
                        /* just accumulate the quantity */
                        avail_qty += (ap->untcas * 
                                  (long)floor((ap->untqty - ap->comqty)/
                                               ap->untcas));
                        avail_catch_qty += ap->catch_qty - ap->com_catch_qty;
                        ap->potentialAllocation = 1;
                    }
                }
            }
        }
        if (o_ap && avail_qty > 0)
        {
            long saved_pckqty = 0;
            double saved_pck_catch_qty = 0;
            /* Time to allocate again... */
            do 
            {
                picked_qty = 0;
                picked_catch_qty = 0;
                sLog(0,
                    "    Loc: %s, Avail: %d, u/c: %d",
                    stoloc, avail_qty, o_ap->untcas);
            
                ret_status = sProcessCasePick(stoloc,
                              policy->arecod,
                              devcod,
                              invsts,
                              avail_qty,
                              avail_catch_qty,
                              o_ap->untcas,
                              o_ap->untpak,
                              &picked_qty,
                              &picked_catch_qty,
                              pcktyp,
                              cmbcod,
                              fifdte,
                              area,
                              alloc_loc_flg,
                              o_ap->lotnum,
                              o_ap->revlvl,
                              o_ap->supnum,
                              o_ap->orgcod,
                              wh_id,
                              isAlloLocFailed);
                saved_pckqty += picked_qty;
                saved_pck_catch_qty += picked_catch_qty;
                avail_qty -= picked_qty;
                avail_catch_qty -= picked_catch_qty;
                sLog(0, "    Resulting Picked qty: %d",
                    picked_qty);
            
            } while (PickRequestList && 
                     (picked_qty > 0 || picked_catch_qty > 0) &&
                     avail_qty > o_ap->untcas);
            
            sClearInventoryFlags(o_ip, o_ap, saved_pckqty, saved_pck_catch_qty);
            if (saved_pckqty > 0 || saved_pck_catch_qty > 0)
                picked_product = 1;
        }
    }


    if (dtlpck_ok && PickRequestList)
    {
        sLog(0, "    Processing detail picks");

        /* Process for pieces */
        for (ip = invHead; ip && PickRequestList; ip = ip->next)
        {
            INVENTORY_ATTR *attr;

            if (ip->palletAllocated)
                continue;
            
            for (attr = ip->attr; attr && PickRequestList; attr = attr->next)
            {
                long saved_pckqty = 0;
                double saved_pck_catch_qty = 0;
                if (!sInventoryMatchesPick(PickRequestList, attr, invsts))
                    continue;

                do 
                {
                    picked_qty = 0;
                    picked_catch_qty = 0;
                    ret_status = sProcessPiecePick(stoloc,
                                   devcod,
                                   invsts,
                                   attr->untqty - attr->comqty,
                                   attr->catch_qty - attr->com_catch_qty,
                                   &picked_qty,
                                   &picked_catch_qty,
                                   attr->untcas,
                                   attr->untpak,
                                   policy->arecod,
                                   pcktyp,
                                   cmbcod,
                                   fifdte,
                                   area,
                                   alloc_loc_flg,
                                   attr->lotnum,
                                   attr->revlvl,
                                   attr->supnum,
                                   attr->orgcod,
                                   wh_id,
                                   isAlloLocFailed);
                    if (picked_qty > 0 || picked_catch_qty > 0)
                    {
                        attr->comqty += picked_qty;
                        attr->com_catch_qty += picked_catch_qty;
                        ip->piecesAllocated = 1;
                        picked_product = 1;
                    }
                } while (PickRequestList && 
                         (picked_qty > 0  || picked_catch_qty > 0) &&
                         attr->untqty > attr->comqty);
            }
        }
    }

    sFreeInventoryList(invHead);
    if (picked_product > 0)
        return(eOK);
    else
        return(eDB_NO_ROWS_AFFECTED);
    
}

static long sProcessPIPicks(char *cmbcod_i)
{
    long           ret_status;
    PCKWRK_DATA    *ptr, *tmpptr;
    RETURN_STRUCT  *CurPtr = NULL;
    char           buffer[2000];

    char           cmbcod[CMBCOD_LEN+1];
    char           fifdte[DB_STD_DATE_LEN+1];

    mocaDataRes    *res = NULL;
    mocaDataRow    *row;

    sLog(0, "    Evaluating for PI Pick");

    ptr = PickRequestList;

    while (ptr)
    {
        /* Save off the next pointer for the while clause */
        tmpptr = ptr->next;

        /* Right here call the command that will find a pckwrk srcloc
         * for the pick.  It passes alot of extra arg's because, like list
         * available pickable inventory, it will be customized by the projects.
         * Why?  Because we are going to make some guesses as to the location
         * and other items that we will never get right for everyone. */

        sprintf(buffer,
                "list pip source locations "
                " where prtnum  = '%s' and prt_client_id = '%s' "
                "   and ship_id = '%s' and ship_line_id = '%s' "
                "   and ordnum  = '%s' and ordlin = '%s' "
                "   and ordsln  = '%s' and orgcod = '%s' "
                "   and revlvl  = '%s' and lotnum = '%s' "
                "   and dstare  = '%s' and rtcust = '%s' "
                "   and invsts  = '%s' and invsts_prg = '%s' "
                "   and wh_id   = '%s' ",
                ptr->prtnum,  ptr->prt_client_id,
                ptr->ship_id, ptr->ship_line_id,
                ptr->ordnum,  ptr->ordlin,
                ptr->ordsln,  ptr->orgcod,
                ptr->revlvl,  ptr->lotnum,
                ptr->dstare,  ptr->rtcust,
                ptr->invsts,  ptr->invsts_prg,
                ptr->wh_id);

        CurPtr = NULL;
        ret_status = srvInitiateCommand(buffer, &CurPtr);

        if (ret_status != eOK )
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);

            /* This is OK, it just means that we could not find
               a source loc.  Later, this pick will be converted to a
               rplwrk... */
            if ((ret_status == eDB_NO_ROWS_AFFECTED) ||
                (ret_status == eSRV_NO_ROWS_AFFECTED))
                return(eOK);
            else
                return(ret_status);
        }
        res = srvGetResults(CurPtr);
        row = sqlGetRow(res);

        /* Normally we would check to make sure that the location 
         * is compatabile with the inventory that we want to store here.
         * Because the information about PI picks is not committed to the
         * database yet, it is not possible with absolute certainity
         * to say that here.  We are counting on the "list forward pick
         * locations" command to validate the location for this command.
         * 
         * If this becomes a problem we may do 
         * some cursory checks in the future.  */

        memset(fifdte, 0, sizeof(fifdte));
        memset(cmbcod, 0, sizeof(cmbcod));
        if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
            misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);
        else
            appNextNum(NUMCOD_CMBCOD, cmbcod);

        /* Change this request into a found item  */
        if (ptr->prev)
            ptr->prev->next = ptr->next;
        if (ptr->next)
            ptr->next->prev = ptr->prev;
        if (ptr == PickRequestList)
            PickRequestList = ptr->next;
        
        if (PickFoundList)
            PickFoundList->prev = ptr;

        ptr->next = PickFoundList;
        ptr->prev = NULL;
        PickFoundList = ptr;

        strncpy(ptr->wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
        strncpy(ptr->srcloc, sqlGetString(res, row, "stoloc"), STOLOC_LEN);
        strncpy(ptr->srcare, sqlGetString(res, row, "arecod"), ARECOD_LEN);
        if (!sqlIsNull(res, row, "devcod"))
            strncpy(ptr->devcod, sqlGetString(res, row, "devcod"), DEVCOD_LEN);
        strncpy(ptr->area_loccod,sqlGetString(res, row, "loccod"),LOCCOD_LEN);
        strncpy(ptr->lodlvl, LODLVL_DETAIL, LODLVL_LEN);
        strncpy(ptr->cmbcod, cmbcod, CMBCOD_LEN);
        ptr->area_subflg = sqlGetBoolean(res, row, "subflg");
        ptr->area_dtlflg = sqlGetBoolean(res, row, "dtlflg");
        ptr->maxavl -= ptr->pckqty;
        ptr->untpak = 0;
        ptr->untcas = 0;
        ptr->area_lodflg = FALSE;
        ptr->pipflg = TRUE;
        strncpy(ptr->fifdte, fifdte, DB_STD_DATE_LEN);
        sAssignDstToPick(ptr);

        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
        res = NULL;

        /* Use the pointer from the top of the loop */
        ptr = tmpptr;
    }
 
    return(eOK);

}

/* sProcessSimpleAllocation
 * - Not implemented
 */
static long sProcessSimpleAllocation(PICK_POLICY_DATA *policy,
                     char *stoloc,
                     char *devcod,
                     char *invsts,
                     char *pcklvl,
                     char *pcktyp,
                     moca_bool_t splflg,
                     char *fifdte,
                     AREA_INFO *area,
                     char *wh_id)
{
    return(eOK);
}

/* sSelectPickInventory
 * - Select pick locations with inventory that 
 *   satisfies entries in the pick request list.
 * - Call allocation commands to attempt allocation of
 *   available inventory.
 * - Maintain a list of skipped locations to be iterated
 *   through more than once due to changes from other allocations.
 */
static long sSelectPickInventory(char *pcklvl,
                 char *srcare,
                 char *pcktyp,
                 char *cmbcod,
                 moca_bool_t splflg, 
                 char *dst_bldg_id,
                 char *invsts,
                 long alloc_loc_flg,
                 char *wh_id)
{
    RETURN_STRUCT   *CmdRes, *PolRes, *LyrPckRes;
    long            ret_status;
    static long     simpleAllocation;
    char buffer[2000];
    char invinloc_order_by[RTSTR1_LEN];
    char juldte[DB_STD_DATE_LEN];
    mocaDataRes    *res, *tres, *pres, *lpckres;
    mocaDataRow    *row, *trow, *prow, *tmprow, *lpckrow;
    PICK_POLICY_DATA Policy, *PolPtr;
    AREA_INFO area_info;
    SKIPPED_LOCATION_INFO *skipped_locations = NULL;
    /* We use isAlloLocFailed to indicate if allocate location failed,
     * if yes, we will check if following 'allocate location' tries to allocate
     * location with the same parameters,if yes, we will skip it to provent
     * useless work which will cause a time issue.
     */
    moca_bool_t isAlloLocFailed = BOOLEAN_FALSE;
    CmdRes = PolRes = NULL;
    res = tres = pres = NULL;


    simpleAllocation = 0;

    memset(invinloc_order_by, 0, sizeof(invinloc_order_by));    

    /* Check the order we should search pallets within a location */

    sprintf(buffer,
        "select rtstr1 "
        "  from poldat_view "
        " where polcod = '%s' "
        "   and polvar = '%s' "
        "   and polval = '%s' "
            "   and wh_id = '%s'  ",
        POLCOD_ALLOCATE_INV,
        POLVAR_ORDER_INV,
        POLVAL_INVINLOC_ORDER_BY,
            wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
        return (ret_status);
    }
    else if (ret_status == eOK)
    {
       row = sqlGetRow(res);

       if (!sqlIsNull(res, row, "rtstr1"))
       {
           strncpy(invinloc_order_by, 
                   sqlGetString(res, row, "rtstr1"), RTSTR1_LEN);
       }
    }
    sqlFreeResults(res);
    res = NULL;

    /* VC-START - JJS 02/16/2009 - borrowed from AWG 03/29/07 */
    /* We need to check to see if this is a replenishment task and if it is
     * we need to see if the part is date controlled and if it is we set
     * the ORDINV to FEFO.  Otherwise, set it to FIFO.
     */
    if (strlen(PickRequestList->ship_id) == 0 && strlen(PickRequestList->wkonum) == 0)
    {
        sLog(0, "      VAR: This pick is for a replenishment");
        sprintf(buffer,
                "select dtcflg "
                "  from prtmst "
                " where prtnum = '%s' "
                "   and prt_client_id = '%s' "
                "   and wh_id_tmpl = '%s' ",
                PickRequestList->prtnum,
                PickRequestList->prt_client_id,
                wh_id);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(res);
            res = NULL;
        }
        row = sqlGetRow(res);
        if (sqlGetBoolean(res, row, "dtcflg") == BOOLEAN_FALSE)
        {
            strncpy(PickRequestList->ordinv, "FIFO-ORDER-BY-ABSOLUTE", ORDINV_LEN);
            sLog(0, "      VAR: Part %s is not Date Controlled - Using FIFO", PickRequestList->prtnum);
            misTrc(T_FLOW, "Will do FIFO Absolute");
        }
        else
        {
            strncpy(PickRequestList->ordinv, "FEFO-ORDER-BY-ABSOLUTE", ORDINV_LEN);
            sLog(0, "      VAR: Part %s is Date Controlled - Using FEFO", PickRequestList->prtnum);
            misTrc(T_FLOW, "Will do FEFO Absolute dude");
        }
        sqlFreeResults(res);
        res = NULL;
    }
    /* VC-STOP - JJS 02/16/2009 - borrowed from AWG 03/29/07 */

    /* Get the customer we should use in a static structure */

    /* Moca's "instr(@var, 'something')" raises error 3:"Could not 
     * place arguments on the stack" if the @var is null or an empty 
     * string so I added a check to catch these values 
     * before calling "instr".
     */

    sprintf(buffer,
        "publish data "
        " where prtnum = '%s' and prt_client_id = '%s' "
        "   and dstare = '%s' and arecod = '%s' "
        "   and pcktyp = '%s' and bldg_id = '%s'"
        "   and client_id = '%s' and pcklvl = '%s' "
        "   and wh_id = '%s' and ordinv = '%s' "
        "   and alc_search_path = '%s' "
        "| "
        "get picking area list "
        "  where @+prtnum "
        "    and @+prt_client_id "
        "    and @+dstare "
        "    and @+arecod "
        "    and @+pcktyp "
        "    and @+bldg_id "
        "    and @+client_id "
        "    and @+pcklvl "
        "    and @+wh_id "
        "    and @+alc_search_path "
        "    and @+ordinv >> picking_area_list "
        "| "
        "if(@ordinv != '') "
        "{ "
        "   if(instr(@ordinv,'%s') > 0) "
        "   { "
        "       get absolute picking area list "
        "   } "
        "   else "
        "   { "
        "       publish data combination where "
        "         rs1 = @picking_area_list "
        "   } "
        "} "
        "else "
        "{ "
        "    publish data combination where "
        "      rs1 = @picking_area_list "
        "} ",
        PickRequestList->prtnum,
        PickRequestList->prt_client_id,
        ((SHIP_DST *)(PickRequestList->reserved))->dstare,
        srcare ? srcare : "", pcktyp, 
        dst_bldg_id ? dst_bldg_id : "",
        PickRequestList->client_id, 
        pcklvl ? pcklvl : "",
        wh_id,
        misTrim(PickRequestList->ordinv),
        PickRequestList->alc_search_path,
        ORDINV_ABSOLUTE_STR);

    ret_status = srvInitiateInline(buffer, &PolRes);
    if (ret_status != eOK)
    {
        if (PolRes)
            srvFreeMemory(SRVRET_STRUCT, PolRes);
        return(ret_status);
    }

    pres = srvGetResults(PolRes);
    for (prow = sqlGetRow(pres); prow; prow = sqlGetNextRow(prow))
    {
        memset(&Policy, 0, sizeof(Policy));
        tmprow = NULL;
        misTrimcpy(Policy.wh_id, 
        sqlGetString(pres, prow, "wh_id"), WH_ID_LEN);
        misTrimcpy(Policy.arecod, 
        sqlGetString(pres, prow, "arecod"), ARECOD_LEN);
        misTrimcpy(Policy.pckcod,
        sqlGetString(pres, prow, "pckcod"), PCKCOD_LEN);
        Policy.lodflg = sqlGetBoolean(pres, prow, "lodflg");
        Policy.subflg = sqlGetBoolean(pres, prow, "subflg");
        Policy.dtlflg = sqlGetBoolean(pres, prow, "dtlflg");
        Policy.area_lodflg = sqlGetBoolean(pres, prow, "area_lodflg");
        Policy.area_subflg = sqlGetBoolean(pres, prow, "area_subflg");
        Policy.area_dtlflg = sqlGetBoolean(pres, prow, "area_dtlflg");

        /* If absolute FEFO or FIFO is used, we should set lodflg, subflg 
         * dtlflg into the first row. Then system will allocate all possible
         * picks following the sorted location list.
         */
        if (strstr(PickRequestList->ordinv, ORDINV_ABSOLUTE_STR))
        {
            for (tmprow = sqlGetRow(pres); tmprow; 
                 tmprow = sqlGetNextRow(tmprow))
            {
                if (!misTrimStrncmp(sqlGetString(pres, tmprow, "wh_id"),
                                    Policy.wh_id, WH_ID_LEN) &&
                    !misTrimStrncmp(sqlGetString(pres, tmprow, "arecod"),
                                    Policy.arecod, ARECOD_LEN))
                {
                    Policy.lodflg = Policy.lodflg || 
                                    sqlGetBoolean(pres, tmprow, "lodflg");
                    Policy.subflg = Policy.subflg || 
                                    sqlGetBoolean(pres, tmprow, "subflg");
                    /* JJS 06/08/2009 - incorporating product fix for the
                     * following line - was originally "...Policy.lodflg ||..."
                     * Jira WMD-49699/WMD-44340
                     */
                    Policy.dtlflg = Policy.dtlflg || 
                                    sqlGetBoolean(pres, tmprow, "dtlflg");
                } 
            }
        }

        /*
	 * JJS 06/16/2009 - A modification to turn off the subflg if this is
         * a layer picked part but not a layer picking area.
         */
        sprintf(buffer, "get var layer pick subflg "
                "where prtnum = '%s' "
                "  and prt_client_id = '%s' "
                "  and arecod = '%s' "
                "  and wh_id  = '%s' ",
                PickRequestList->prtnum,
                PickRequestList->prt_client_id,
                Policy.arecod,
                Policy.wh_id);
        ret_status = srvInitiateInline(buffer, &LyrPckRes);
        if (ret_status == eOK)
        {
           lpckres = srvGetResults(LyrPckRes);
           lpckrow = sqlGetRow(lpckres);
           if (sqlGetBoolean(lpckres, lpckrow, "turn_off_subflg") == 1)
               Policy.subflg = 0;
           if (LyrPckRes)
              srvFreeMemory(SRVRET_STRUCT, LyrPckRes);
        }
        
        PolPtr = &Policy;

        sLog(0, "  Considering area code: %s/%s\n",
        PolPtr->wh_id, PolPtr->arecod);
    
        if (Policy.lodflg == BOOLEAN_FALSE &&
            Policy.subflg == BOOLEAN_FALSE &&
            Policy.dtlflg == BOOLEAN_FALSE)
        {
            sLog(0, 
            "  Skipping area - all flags "
            "(lod,sub,dtl) are turned off\n");
            continue;
        }

        /* Check to see if a srcare was sent into this routine
        * then we only care about the one arecod, so we will check
        * if the srcare matches the policy arecod.
        * If the srcare was not sent in the we check all of the
        * arecod's listed in the policies.
        */
        if ((!srcare || misTrimLen(srcare, 1) == 0) ||
            (misTrimLen(PolPtr->arecod, 1) == misTrimLen(srcare, 1) &&
            (strncmp(srcare, PolPtr->arecod, misTrimLen(srcare, 1)) == 0)))
        {

            /* NOTE:  The command only cares about arecod, prtnum,
            * prt_client_id and invsts...we pass the rest in case someone
            * is replacing this command and needs the allocation data 
            * Also, we now send in the lodflg, subflg, and dtlflg to
            * allow project teams more information should they choose
            * to customize this command.  For example, at Bosch, they
            * overwrote this command and put in a way to skip picks 
            * based on the information in the additional 3 fields. 
            * The pcktyp parameter passed into "list available pickable 
            * inventory" component just for user customization. It will 
            * has no other meaning on the operation, just published on the
            * MOCA context.
            */
        
            sprintf(buffer,
            "list available pickable inventory "
                "where prtnum = '%s' "
                "  and prt_client_id = '%s' "
                "  and invsts = '%s' "
                "  and orgcod = '%s' "
                "  and revlvl = '%s' "
                "  and lotnum = '%s' "
                "  and untcas = '%ld' "
                "  and untpak = '%ld' "
                "  and untpal = '%ld' "
                "  and arecod = '%s'  "
                "  and ship_id = '%s' "
                "  and client_id = '%s' "
                "  and ordnum = '%s' "
                "  and ordlin = '%s' "
                "  and ordsln = '%s' "
                "  and ordinv = '%s' "
                "  and wkonum = '%s' "
                "  and wkorev = '%s' "
                "  and wkolin = '%s' "
                "  and frsdte = '%s' "
                "  and min_shelf_hrs = '%ld' "
                "  and lodflg = '%ld' "
                "  and subflg = '%ld' "
                "  and dtlflg = '%ld' "
                "  and pcktyp = '%s' "
                "  and wh_id  = '%s' ",
                PickRequestList->prtnum,
                PickRequestList->prt_client_id,
                invsts,
                misTrim(PickRequestList->orgcod),
                misTrim(PickRequestList->revlvl),
                misTrim(PickRequestList->lotnum),
                PickRequestList->alloc_cas,
                PickRequestList->alloc_pak,
                PickRequestList->alloc_pal,
                PolPtr->arecod,
                PickRequestList->ship_id,
                misTrim(PickRequestList->client_id),
                misTrim(PickRequestList->ordnum),
                misTrim(PickRequestList->ordlin),
                misTrim(PickRequestList->ordsln),
                misTrim(PickRequestList->ordinv),
                misTrim(PickRequestList->wkonum), 
                misTrim(PickRequestList->wkorev),
                misTrim(PickRequestList->wkolin),
                misTrim(PickRequestList->frsdte),
                PickRequestList->min_shelf_hrs,
                PolPtr->lodflg,
                PolPtr->subflg,
                PolPtr->dtlflg,
                pcktyp,
                PolPtr->wh_id);

            ret_status = srvInitiateCommand(buffer, &CmdRes);
        }
        else
        {
            /* If we are here, the srcare was sent in and
            it did not match the policy arecod.  */
            sLog(0, "  Specific area %s/%s requested - ignoring other areas",
            wh_id, srcare);
            ret_status = eDB_NO_ROWS_AFFECTED;
        }
        if (ret_status != eOK)
        {
            sLog(0, "  No inventory found in area: %s/%s",
            PolPtr->wh_id, PolPtr->arecod);

            if(CmdRes) 
            {
                srvFreeMemory(SRVRET_STRUCT, CmdRes);
                CmdRes = NULL;
            }
                
            continue;
        }

        res = srvGetResults(CmdRes);

        memset(&area_info, 0, sizeof(area_info));
        memset(juldte, 0, sizeof(juldte));

        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {

            if (misTrimLen(area_info.area_loccod, LOCCOD_LEN) == 0)
            {
                area_info.area_lodflg = sqlGetBoolean(res, row, "lodflg");
                area_info.area_subflg = sqlGetBoolean(res, row, "subflg");
                area_info.area_dtlflg = sqlGetBoolean(res, row, "dtlflg");
                strncpy(area_info.area_loccod, 
                sqlGetString(res, row, "loccod"), LOCCOD_LEN);
            }
        
            sqlSetSavepoint("location_summary_pick");
        
            if (sqlGetDataType(res, "rowid") == COMTYP_STRING)
            {
                sprintf(buffer,
                "select to_char(/*=moca_util.days(*/ "
                "               moca_util.date_diff_days(invsum.olddte, "
                "                                     invsum.newdte)/2 /*=)*/ "
                "              + invsum.olddte, 'YYYYMMDDHH24MISS') juldte"
                "  from invsum "
                " where invsum.rowid = '%s' "
                " for update",
                sqlGetString(res, row, "rowid"));
            }
            else
            {
                /* DB2 has a numeric rowid. Binding will handle SQL
                ** statements, but we need to convert to C datatype here
                */
                sprintf(buffer,
                "select invsum.olddte + "
                "      ((invsum.newdte - invsum.olddte)/2) seconds juldte "
                "  from invsum "
                " where invsum.rowid = %ld "
                " for update",
                sqlGetLong(res, row, "rowid"));
            }
            ret_status = sqlExecStr(buffer, &tres);
            if (eOK != ret_status)
            {
                sqlRollbackToSavepoint("location_summary_pick");
                sqlFreeResults(tres);
                srvFreeMemory(SRVRET_STRUCT, CmdRes);
                srvFreeMemory(SRVRET_STRUCT, PolRes);
                return(ret_status);
            }
            trow = sqlGetRow(tres);
            strncpy(juldte, sqlGetString(tres, trow, "juldte"),
            DB_STD_DATE_LEN);
            sLog(0, "    Locking location %s/%s for pick", 
            sqlGetString(res, row, "wh_id"),
            sqlGetString(res, row, "stoloc"));
            sqlFreeResults(tres);
            tres = NULL;

            if (simpleAllocation)
            {
                ret_status = 
                sProcessSimpleAllocation(PolPtr,
                    sqlGetString(res,row,"stoloc"),
                    sqlGetString(res,row,"devcod"),
                    invsts,
                    pcklvl,
                    pcktyp,
                    splflg,
                    juldte,
                    &area_info,
                    sqlGetString(res,row,"wh_id"));
            }
            else
            {
                ret_status = 
                sProcessComplexAllocation(PolPtr,
                    sqlGetString(res, row, "stoloc"),
                    sqlGetString(res, row, "devcod"),
                    invsts,
                    pcklvl,
                    pcktyp,
                    cmbcod,
                    splflg,
                    juldte,
                    &area_info,
                    alloc_loc_flg,
                    sqlGetString(res,row,"wh_id"),
                    invinloc_order_by,
                    &isAlloLocFailed);
            }
            if(ret_status == eABSOLUTE_ALLOCATION_VIOLATION)
            {
                /* 
                 * If the location was not used because it might violate 
                 * an absolute allocation constraint then we should save it
                 * in case we partially allocate some picks in which case we
                 * could possibly go back and use the skipped locations again
                 */
                skipped_locations = 
                    sAddLocationToSkipList(PolPtr,
                    sqlGetString(res, row, "stoloc"),
                    sqlGetString(res, row, "devcod"),
                    invsts,
                    pcklvl,
                    pcktyp,
                    cmbcod,
                    splflg,
                    juldte,
                    &area_info,
                    alloc_loc_flg,
                    sqlGetString(res, row, "wh_id"),
                    invinloc_order_by,
                    skipped_locations);
            }
            else if (ret_status != eOK 
                && ret_status != eDB_NO_ROWS_AFFECTED)
            {
                srvFreeMemory(SRVRET_STRUCT, CmdRes);
                srvFreeMemory(SRVRET_STRUCT, PolRes);
                return(ret_status);
            }
            else if (ret_status == eOK
                && !PickRequestList)
            {
                srvFreeMemory(SRVRET_STRUCT, CmdRes);
                srvFreeMemory(SRVRET_STRUCT, PolRes);
                return(eOK);
            }
            if (eOK != ret_status)
            {
                sqlRollbackToSavepoint("location_summary_pick");
            }
        }
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        CmdRes = NULL;
        res = NULL;
    }
    /* 
     * If allocation still exist we can process for any locations
     * which were skipped by absolute allocation checks
     */
    /*
     * Note: If in the future we see many locationsin this list we could
     * run a loop to process skipped locations an indefinite number of times
     * here.  The alternative is letting replenishment figure out that there
     * really is inventory.
     */
    if(PickRequestList && skipped_locations)
    {
        skipped_locations = skipped_locations->first;
        ret_status = sProcessSkippedLocations(skipped_locations,&isAlloLocFailed);
    }
    sFreeSkippedLocationList(skipped_locations);
    srvFreeMemory(SRVRET_STRUCT, PolRes);

    return(eOK);
}

static void sFreeMovePath()
{
    AREA_LIST   *ptrarea, *last;

    if (PathHead == NULL)
    return;

    last = NULL;
    for (ptrarea = PathHead; ptrarea; ptrarea = ptrarea->nextarea)
    {
        if (last)
            free(last);

        last = ptrarea;    
    }
    if (last)
        free(last);

    PathHead = NULL;
}

static long sLoadMovePaths()
{
    AREA_LIST      *areptr, *topare;
    char            sqlbuffer[400];
    mocaDataRes     *res = NULL;
    mocaDataRow    *row;
    long            ret_status;
    RETURN_STRUCT  *CurPtr = NULL;

    if (PathHead)
        return (eOK);

    sprintf(sqlbuffer,
            "  list all movement paths ");

    ret_status = srvInitiateCommand(sqlbuffer, &CurPtr);
    
    if (ret_status != eOK)
    {
        if (CurPtr) srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
        misTrc(T_FLOW, "No movement paths found");
        return (ret_status);
    }

    topare = NULL;
    areptr = NULL;
    res = srvGetResults(CurPtr);
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        if (!sqlIsNull(res, row, "hopare") &&
            misTrimLen(sqlGetString(res, row, "hopare"), ARECOD_LEN) &&
            misTrimLen(sqlGetString(res, row, "srcare"), ARECOD_LEN) &&
            misTrimLen(sqlGetString(res, row, "dstare"), ARECOD_LEN) &&
            misTrimLen(sqlGetString(res, row, "lodlvl"), LODLVL_LEN))
        {
            if (topare == NULL)
            {
                topare = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
                if (topare == NULL)
                {
                    if (CurPtr) srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    CurPtr = NULL;
                    return (eNO_MEMORY);
                }
                areptr = topare;
            }
            else
            {
                areptr->nextarea = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
                if (areptr->nextarea == NULL)
                {
                    if (CurPtr) srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    CurPtr = NULL;
                    return (eNO_MEMORY);
                }
                areptr = areptr->nextarea;
            }

            if (!sqlIsNull(res, row, "wh_id"))
            {
                misTrimcpy(areptr->wh_id, 
                            sqlGetString(res, row, "wh_id"), WH_ID_LEN);
            }

            if (!sqlIsNull(res, row, "srcare"))
            {
                misTrimcpy(areptr->srcare, 
                            sqlGetString(res, row, "srcare"), ARECOD_LEN);
            }

            if (!sqlIsNull(res, row, "dstare"))
            {
                 misTrimcpy(areptr->dstare, 
                            sqlGetString(res, row, "dstare"), ARECOD_LEN);
            }

            if (!sqlIsNull(res, row, "hopare"))
            {
                misTrimcpy(areptr->arecod, 
                            sqlGetString(res, row, "hopare"), ARECOD_LEN);
            }

            if (!sqlIsNull(res, row, "lodlvl"))
            {
                 misTrimcpy(areptr->lodlvl, 
                            sqlGetString(res, row, "lodlvl"), LODLVL_LEN);
            }
        }
    }

    PathHead = topare;
    misFlagCachedMemory((OSFPTR) sFreeMovePath, NULL);
        
    if (CurPtr) 
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
        res = NULL;
    }
    return (eOK);
}

static long sFillMovePath(PCKWRK_DATA * ptr)
{
    AREA_LIST      *pth;
    PICK_MOVS      *pckmovs, *pckmovsLst;
    PICK_MOV       *mov;

    long            movepathFound;

    pckmovs = PckMovPathHead;
    pckmovsLst = NULL;
    movepathFound = 0;

    while (pckmovs)
    {
    if (!strncmp(ptr->cmbcod, pckmovs->cmbcod, CMBCOD_LEN))
        break;
    pckmovsLst = pckmovs;
    pckmovs = pckmovs->next;
    }

    if (!pckmovs)
    {
    if (!(pckmovs = (PICK_MOVS *) calloc(1, sizeof(PICK_MOVS))))
        return (eNO_MEMORY);

    strncpy(pckmovs->cmbcod, ptr->cmbcod, CMBCOD_LEN);
    pckmovs->pckWrk = ptr;

    if (!pckmovsLst)
        PckMovPathHead = pckmovs;
    else
    {
        pckmovs->prev = pckmovsLst;
        pckmovsLst->next = pckmovs;
    }
    }
    else
    {
        ptr->pckMovs = pckmovs;
        return (eOK);
    }

    ptr->pckMovs = pckmovs;

    mov = NULL;
    /* This for-loop will check if there is a hop-area 
     * specified for the move. If it does not find any
     * hop area it will not add the move-path.
     */
    for (pth = PathHead; pth; pth = pth->nextarea)
    {
        if (strncmp(pth->srcare, ptr->srcare, ARECOD_LEN) == 0 &&
            strncmp(pth->dstare, ptr->dstare, ARECOD_LEN) == 0 &&
            strncmp(pth->lodlvl, ptr->lodlvl, LODLVL_LEN) == 0 &&
            strncmp(pth->wh_id, ptr->wh_id, WH_ID_LEN) == 0)
        {
            if (!misTrimLen(pth->arecod, ARECOD_LEN))
                continue;

            movepathFound = 1;

            if (mov == NULL)
            {
                if (!(pckmovs->pckMovPath = 
                     (PICK_MOV *) calloc(1, sizeof(PICK_MOV))))
                    return (eNO_MEMORY);

                mov = pckmovs->pckMovPath;
            }
            else
            {
                if (!(mov->next = (PICK_MOV *) calloc(1, sizeof(PICK_MOV))))
                    return (eNO_MEMORY);
                
                mov = mov->next;
            }

            strncpy(mov->arecod, pth->arecod, ARECOD_LEN);
            strncpy(mov->wh_id, pth->wh_id, WH_ID_LEN);

            if (strncmp(mov->arecod, ptr->dstare, ARECOD_LEN) == 0 &&
                strncmp(mov->wh_id, ptr->wh_id, WH_ID_LEN) == 0)
            strncpy(mov->stoloc, ptr->dstloc, STOLOC_LEN);

            mov->resAssc = sGetResourceTranslation(ptr, mov->arecod, 
                                                   mov->wh_id);
        }
    }

    /* At this point if move paths found with hop-areas 
     * then pointer will have only hop-area moves. So we need
     * to set the actual destination at the tail.
     * We can make out the hop areas with the mov->arecod.
     */
    if (movepathFound &&
        strncmp(mov->arecod, ptr->dstare, ARECOD_LEN) != 0)
    {
        if (!(mov->next = (PICK_MOV *) calloc(1, sizeof(PICK_MOV))))
            return (eNO_MEMORY);
        mov = mov->next;
        strncpy(mov->arecod, ptr->dstare, ARECOD_LEN);
        strncpy(mov->stoloc, ptr->dstloc, STOLOC_LEN);
        strncpy(mov->wh_id, ptr->wh_id, WH_ID_LEN);

        mov->resAssc = sGetResourceTranslation(ptr, mov->arecod, 
                                                   mov->wh_id);
    }

    /* Things really get all mucked up if we put out pckwrks without
       a move path (i.e. corresponding pckmovs).  We have a choice here,
       we could either return some kind of a fatal error which would
       draw attention to the issue, or we could simply put out a simple
       move path from the source to the dest.  We opt for the latter as
       that seems like a bit better than just puking... */
    if (!movepathFound)
    {
    if (!pckmovs)
    {
        if (!(pckmovs = (PICK_MOVS *)calloc(1, sizeof(PICK_MOVS))))
                return (eNO_MEMORY);
        PckMovPathHead = pckmovs;
    }
    if (!(pckmovs->pckMovPath =
         (PICK_MOV *) calloc(1, sizeof(PICK_MOV))))
        return (eNO_MEMORY);

        mov = pckmovs->pckMovPath;
    strncpy(mov->arecod, ptr->dstare, ARECOD_LEN);
    strncpy(mov->wh_id, ptr->wh_id, WH_ID_LEN);
    if (misTrimLen(ptr->dstloc, STOLOC_LEN))
        strncpy(mov->stoloc, ptr->dstloc, STOLOC_LEN);

    mov->resAssc = sGetResourceTranslation(ptr, mov->arecod, mov->wh_id);
    }

    return (eOK);
}

static long sWritePickWorks(PCKWRK_DATA * ptr,
                char *wrktyp,
                char *pcksts,
                char *pcksts_uom,
                moca_bool_t *pipflg_i)
{
    long ret_status;
    long in_totpnd_rplqvl;
    char wrkref[WRKREF_LEN + 1];
    char ftpcod[FTPCOD_LEN + 1];
    char pick_status[PCKSTS_LEN + 1];
    char orgcod_str[200];
    char lotnum_str[200];
    char revlvl_str[200];
    char supnum_str[200];
    char buffer[2000];
    char rpcksts[PCKSTS_LEN + 1];
    mocaDataRes     *res = NULL;
    mocaDataRow     *row;
    moca_bool_t     lodflg, subflg, dtlflg;
    moca_bool_t     orgflg, revflg, lotflg;
    moca_bool_t     qtyflg, locflg, prtflg;
    moca_bool_t     catch_qty_flg, supflg;

    lodflg        = BOOLEAN_NOTSET;
    subflg        = BOOLEAN_NOTSET;
    dtlflg        = BOOLEAN_NOTSET;
    orgflg        = BOOLEAN_NOTSET;
    revflg        = BOOLEAN_NOTSET;
    supflg        = BOOLEAN_NOTSET;
    lotflg        = BOOLEAN_NOTSET;
    qtyflg        = BOOLEAN_NOTSET;
    catch_qty_flg = BOOLEAN_NOTSET;
    locflg        = BOOLEAN_NOTSET;
    prtflg        = BOOLEAN_NOTSET;
    
    in_totpnd_rplqvl = 0; /*reset in coming total pending replenishment qvl*/
    ret_status = sLoadMovePaths();

    memset(wrkref, 0, sizeof(wrkref));
    appNextNum(NUMCOD_WRKREF, wrkref);

    memset(rpcksts, 0, sizeof(rpcksts));
    memset(pick_status, 0, sizeof(pick_status));
    
    memset(ftpcod, 0, sizeof(ftpcod));
    strncpy(ftpcod, ptr->ftpcod, FTPCOD_LEN);

    if ((ret_status = sFillMovePath(ptr)) != eOK)
        return (ret_status);

    strncpy(ptr->wrkref, wrkref, WRKREF_LEN);

    /* This should never happen....but if it does, we'll bail out here... */
    if (ptr->pckqty <= 0)
        return (eOK);

    if (IS_SET(ptr->lotnum))
        sprintf(lotnum_str, ptr->lotnum);
    else
        sprintf(lotnum_str, "NULL");

    if (IS_SET(ptr->orgcod))
        sprintf(orgcod_str, ptr->orgcod);
    else
        sprintf(orgcod_str, "NULL");

    if (IS_SET(ptr->revlvl))
        sprintf(revlvl_str, ptr->revlvl);
    else
        sprintf(revlvl_str, "NULL");

    if (IS_SET(ptr->supnum))
        sprintf(supnum_str, ptr->supnum);
    else
        sprintf(supnum_str, "NULL");

    sDetermineVerification(ptr,
               &lodflg,
               &subflg,
               &dtlflg,
               &prtflg,
               &locflg,
               &qtyflg,
               &catch_qty_flg,
               &orgflg,
               &revflg,
               &supflg,
               &lotflg,
               wrktyp);

    /* PR 46800
     * We will check the ftpcod first, if it is not exist,
     * then get it from invdtl by the prtnum, prt_client_id and
     * also in the storage location.
     * If there are several ftpcod for the prtnum, prt_client_id
     * in the storage location, get the max one.
     */
    if (!ftpcod || misTrimLen(ftpcod, FTPCOD_LEN) == 0)
    {
        sprintf(buffer,
               " select ftpmst.ftpcod "
               "   from ftpmst, "
               "        invdtl, "
               "        invsub, "
               "        invlod  "
               "  where invdtl.prtnum = '%s' "
               "    and invdtl.prt_client_id = '%s' "
               "    and invdtl.subnum = invsub.subnum "
               "    and invsub.lodnum = invlod.lodnum "
               "    and invlod.stoloc = '%s' "
               "    and invlod.wh_id  = '%s' "
               "    and invdtl.ftpcod = ftpmst.ftpcod "
               "    and rownum < 2 "
               "  order by (ftpmst.untlen*ftpmst.untwid*ftpmst.unthgt) desc",
               ptr->prtnum,
               ptr->prt_client_id,
               ptr->srcloc,
               ptr->wh_id);
        ret_status = sqlExecStr(buffer, &res);

        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(res);
            return(ret_status);
        }
        else if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            /* If we're doing a pip pick, then there may not be any
             * inventory in the location, in which case we need to
             * go after the ftpcod of the part.
             */

            sqlFreeResults(res);
            res = NULL;

            sprintf(buffer,
                " select ftpmst.ftpcod "
                "   from ftpmst, "
                "        prtmst_view "
                "  where prtmst_view.prtnum = '%s' "
                "    and prtmst_view.prt_client_id = '%s' "
                "    and prtmst_view.wh_id = '%s' "
                "    and prtmst_view.ftpcod = ftpmst.ftpcod ",
                ptr->prtnum,
                ptr->prt_client_id,
                ptr->wh_id);

            ret_status = sqlExecStr(buffer, &res);
            if (ret_status != eOK)
            {
                sqlFreeResults(res);
                return (eINT_INVALID_PRTNUM);
            }
        }

        row = sqlGetRow(res);
        misTrimcpy (ftpcod, sqlGetString(res, row, "ftpcod"), 
                    FTPCOD_LEN);
        strncpy(ptr->ftpcod, ftpcod, FTPCOD_LEN);

        sqlFreeResults(res);
        res = NULL;
    }    
    if (pipflg_i && (*pipflg_i == BOOLEAN_TRUE))
    {
       sCalculateReplenishmentPickStatusForPIP(ptr, rpcksts,in_totpnd_rplqvl);   
       strncpy(pick_status, rpcksts, PCKSTS_LEN);
    }
    else if (pcksts_uom != NULL && misTrimLen(pcksts_uom, LODLVL_LEN) > 0)
    {
        /* If this allocation call is from Shipment Allocation Operations or
         * Wave Operations, pcksts_uom (format 'L,S,D') will specify lodlvls
         * that should allocate to a pending status. If the lodlvl is not
         * specified, it should allocate to a hold status.
         */
        if (strstr(pcksts_uom, ptr->lodlvl) != NULL)
            strncpy(pick_status, PCKSTS_PENDING, PCKSTS_LEN);
        else
            strncpy(pick_status, PCKSTS_HOLD, PCKSTS_LEN);
    }
    else if (pcksts && misTrimLen(pcksts, PCKSTS_LEN))
        strncpy(pick_status, pcksts, PCKSTS_LEN);
    else
        strncpy(pick_status, PCKSTS_PENDING, PCKSTS_LEN);

    /* PR 56507 and WMD-37067
     * if ASSET-TRACKING is enabled, and lodlvl is 'L',
     * we need to track asset to consider it's volume.
     *
     * But when user pick up the product, we won't force 
     * user to pick load with the specified asset.
     */
    if (asset_track_enabled && misTrimLen(ptr->asset_typ, ASSET_TYP_LEN) > 0 &&
        strncmp(ptr->lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0)
    {
        ret_status =
            sqlExecBind(" insert into pckwrk "
            " (wrkref, wh_id, wrktyp, schbat, srcloc, dstloc, "
            "  srcare, dstare, ship_id, ship_line_id, "
            "  wkonum, wkorev, wkolin, "
            "  client_id, "
            "  ordnum, ordlin, ordsln, "
            "  stcust, rtcust, concod, cmbcod, "
            "  devcod, pckqty, appqty, "
            "  pck_catch_qty, app_catch_qty, "
            "  pcksts, "
            "  prt_client_id, "
            "  prtnum, orgcod, revlvl, supnum, lotnum, invsts, "
                    "  invsts_prg, "
            "  lodlvl, untcas, "
            "  untpak, ftpcod, visflg, pipflg, "
            "  splflg, locflg, lodflg, subflg, dtlflg, "
            "  prtflg, orgflg, revflg, supflg, lotflg, qtyflg, "
                    "  catch_qty_flg, "
                    "  frsflg, min_shelf_hrs, "
            "  adddte, asset_typ, bto_seqnum, slot, vc_from_rpl) "
            "values "
            "(:wrkref, :wh_id, :wrktyp, :schbat, :srcloc, :dstloc,"
            " :srcare, :dstare, :ship_id, :ship_line_id, "
            " :wkonum, :wkorev, :wkolin, "
            " :client_id, "
            " :ordnum, :ordlin, :ordsln, "
            " :stcust, :rtcust, :concod, :cmbcod,  "
            " :devcod, :pckqty, 0, "
            " :pck_catch_qty, 0.0, "
            " :pcksts, "
            " :prt_client_id, "
            " :prtnum, :orgcod, :revlvl, :supnum, :lotnum, :invsts, "
                    " :invsts_prg, "
            " :lodlvl, :untcas, "
            " :untpak, :ftpcod, :visflg, :pipflg, "
            " :splflg, :locflg, :lodflg, :subflg, :dtlflg, "
            " :prtflg, :orgflg, :revflg, :supflg, :lotflg, :qtyflg, "
                    " :catch_qty_flg, "
                    " :frsflg, :min_shelf_hrs, "
            " sysdate, :asset_typ, :bto_seqnum, :slot, :vc_from_rpl) ",
            NULL, NULL,
            BINDSTR("wrkref", WRKREF_LEN, wrkref),
            BINDSTR("wh_id", WH_ID_LEN, ptr->wh_id),
            BINDSTR("wrktyp",  WRKTYP_LEN, wrktyp),
            BINDSTR("schbat",  SCHBAT_LEN, ptr->schbat),
            BINDSTR("srcloc",  STOLOC_LEN, ptr->srcloc),
            BINDSTR("dstloc",  STOLOC_LEN, ptr->dstloc),
            BINDSTR("srcare",  ARECOD_LEN, ptr->srcare),
            BINDSTR("dstare",  ARECOD_LEN, ptr->dstare),
            BINDSTR("ship_id",  SHIP_ID_LEN, ptr->ship_id),
            BINDSTR("ship_line_id",SHIP_LINE_ID_LEN,ptr->ship_line_id),
            BINDSTR("wkonum", WKONUM_LEN, ptr->wkonum),
            BINDSTR("wkorev", WKOREV_LEN, ptr->wkorev),
            BINDSTR("wkolin", WKOLIN_LEN, ptr->wkolin),
            BINDSTR("client_id",  CLIENT_ID_LEN, ptr->client_id),
            BINDSTR("ordnum",  ORDNUM_LEN, ptr->ordnum),
            BINDSTR("ordlin",  ORDLIN_LEN, ptr->ordlin),
            BINDSTR("ordsln",  ORDSLN_LEN, ptr->ordsln),
            BINDSTR("stcust",  STCUST_LEN, ptr->stcust),
            BINDSTR("rtcust",  STCUST_LEN, ptr->rtcust),
            BINDSTR("concod",  CONCOD_LEN, ptr->concod),
            BINDSTR("cmbcod",  CMBCOD_LEN, ptr->cmbcod),
            BINDSTR("devcod",  DEVCOD_LEN, ptr->devcod),
            "pckqty", COMTYP_INT, 
                sizeof(long), ptr->pckqty, 0,
            "pck_catch_qty", COMTYP_FLOAT, 
                sizeof(double), ptr->pck_catch_qty, 0,
            BINDSTR("pcksts",  PCKSTS_LEN, pick_status),
            BINDSTR("prt_client_id", CLIENT_ID_LEN,ptr->prt_client_id),
            BINDSTR("prtnum",  PRTNUM_LEN, ptr->prtnum),
            BINDSTR("orgcod",  ORGCOD_LEN, ptr->orgcod),
            BINDSTR("revlvl",  REVLVL_LEN, ptr->revlvl),
            BINDSTR("supnum",  SUPNUM_LEN, ptr->supnum),
            BINDSTR("lotnum",  LOTNUM_LEN, ptr->lotnum),
            BINDSTR("invsts",  INVSTS_LEN, ptr->invsts),
            BINDSTR("invsts_prg",  INVSTS_PRG_LEN, ptr->invsts_prg),
            BINDSTR("lodlvl",  LODLVL_LEN, ptr->lodlvl),
            "untcas", COMTYP_INT, sizeof(long), ptr->untcas, 0,
            "untpak", COMTYP_INT, sizeof(long), ptr->untpak, 0,
            BINDSTR("ftpcod",  FTPCOD_LEN, ptr->ftpcod),
            "visflg", COMTYP_INT, sizeof(long), BOOLEAN_TRUE, 0,
            "pipflg", COMTYP_INT, sizeof(long), ptr->pipflg, 0,
            "splflg", COMTYP_INT, sizeof(long), ptr->splflg, 0,
            "locflg", COMTYP_INT, sizeof(long), locflg, 0,
            "lodflg", COMTYP_INT, sizeof(long), lodflg, 0,
            "subflg", COMTYP_INT, sizeof(long), subflg, 0,
            "dtlflg", COMTYP_INT, sizeof(long), dtlflg, 0,
            "prtflg", COMTYP_INT, sizeof(long), prtflg, 0,
            "orgflg", COMTYP_INT, sizeof(long), orgflg, 0,
            "revflg", COMTYP_INT, sizeof(long), revflg, 0,
            "supflg", COMTYP_INT, sizeof(long), supflg, 0,
            "lotflg", COMTYP_INT, sizeof(long), lotflg, 0,
            "qtyflg", COMTYP_INT, sizeof(long), qtyflg, 0, 
            "catch_qty_flg", COMTYP_INT, sizeof(long), catch_qty_flg, 0,
            "frsflg", COMTYP_INT, sizeof(long), ptr->frsflg, 0,
            "min_shelf_hrs", COMTYP_INT, sizeof(long), 
                (ptr->min_shelf_hrs > 0 ? ptr->min_shelf_hrs : 0), 0,
            BINDSTR("asset_typ",  ASSET_TYP_LEN, ptr->asset_typ), 
            BINDSTR("bto_seqnum",BTO_SEQNUM_LEN, ptr->bto_seqnum),
            BINDSTR("slot",SLOT_LEN, ptr->slot),
            "vc_from_rpl", COMTYP_INT, sizeof(long), vc_from_rpl, 0, 
                    0);
    }
    else
    {
        ret_status =
            sqlExecBind(" insert into pckwrk "
            " (wrkref, wh_id, wrktyp, schbat, srcloc, dstloc, "
            "  srcare, dstare, ship_id, ship_line_id, "
            "  wkonum, wkorev, wkolin, "
            "  client_id, "
            "  ordnum, ordlin, ordsln, "
            "  stcust, rtcust, concod, cmbcod, "
            "  devcod, pckqty, appqty, "
            "  pck_catch_qty, app_catch_qty, "
            "  pcksts, "
            "  prt_client_id, "
            "  prtnum, orgcod, revlvl, supnum, lotnum, invsts, "
            "  invsts_prg, "
            "  lodlvl, untcas, "
            "  untpak, ftpcod, visflg, pipflg, "
            "  splflg, locflg, lodflg, subflg, dtlflg, "
            "  prtflg, orgflg, revflg, supflg, lotflg, qtyflg, "
            "  catch_qty_flg, "
            "  frsflg, min_shelf_hrs, "
            "  bto_seqnum, slot, vc_from_rpl, "
            "  adddte ) "
            "values "
            "(:wrkref, :wh_id, :wrktyp, :schbat, :srcloc, :dstloc,"
            " :srcare, :dstare, :ship_id, :ship_line_id, "
            " :wkonum, :wkorev, :wkolin, "
            " :client_id, "
            " :ordnum, :ordlin, :ordsln, "
            " :stcust, :rtcust, :concod, :cmbcod,  "
            " :devcod, :pckqty, 0, "
            " :pck_catch_qty, 0.0, "
            " :pcksts, "
            " :prt_client_id, "
            " :prtnum, :orgcod, :revlvl, :supnum, :lotnum, :invsts, "
            " :invsts_prg, "
            " :lodlvl, :untcas, "
            " :untpak, :ftpcod, :visflg, :pipflg, "
            " :splflg, :locflg, :lodflg, :subflg, :dtlflg, "
            " :prtflg, :orgflg, :revflg, :supflg, :lotflg, :qtyflg, "
            " :catch_qty_flg, "
            " :frsflg, :min_shelf_hrs, "
            " :bto_seqnum, :slot, :vc_from_rpl, "
            " sysdate) ",
            NULL, NULL,
            BINDSTR("wrkref", WRKREF_LEN, wrkref),
            BINDSTR("wh_id", WH_ID_LEN, ptr->wh_id),
            BINDSTR("wrktyp",  WRKTYP_LEN, wrktyp),
            BINDSTR("schbat",  SCHBAT_LEN, ptr->schbat),
            BINDSTR("srcloc",  STOLOC_LEN, ptr->srcloc),
            BINDSTR("dstloc",  STOLOC_LEN, ptr->dstloc),
            BINDSTR("srcare",  ARECOD_LEN, ptr->srcare),
            BINDSTR("dstare",  ARECOD_LEN, ptr->dstare),
            BINDSTR("ship_id",  SHIP_ID_LEN, ptr->ship_id),
            BINDSTR("ship_line_id",SHIP_LINE_ID_LEN,ptr->ship_line_id),
            BINDSTR("wkonum", WKONUM_LEN, ptr->wkonum),
            BINDSTR("wkorev", WKOREV_LEN, ptr->wkorev),
            BINDSTR("wkolin", WKOLIN_LEN, ptr->wkolin),
            BINDSTR("client_id",  CLIENT_ID_LEN, ptr->client_id),
            BINDSTR("ordnum",  ORDNUM_LEN, ptr->ordnum),
            BINDSTR("ordlin",  ORDLIN_LEN, ptr->ordlin),
            BINDSTR("ordsln",  ORDSLN_LEN, ptr->ordsln),
            BINDSTR("stcust",  STCUST_LEN, ptr->stcust),
            BINDSTR("rtcust",  STCUST_LEN, ptr->rtcust),
            BINDSTR("concod",  CONCOD_LEN, ptr->concod),
            BINDSTR("cmbcod",  CMBCOD_LEN, ptr->cmbcod),
            BINDSTR("devcod",  DEVCOD_LEN, ptr->devcod),
            "pckqty", COMTYP_INT, sizeof(long), ptr->pckqty, 0,
            "pck_catch_qty", COMTYP_FLOAT,
                sizeof(double), ptr->pck_catch_qty, 0,
            BINDSTR("pcksts",  PCKSTS_LEN, pick_status),
            BINDSTR("prt_client_id", CLIENT_ID_LEN,ptr->prt_client_id),
            BINDSTR("prtnum",  PRTNUM_LEN, ptr->prtnum),
            BINDSTR("orgcod",  ORGCOD_LEN, ptr->orgcod),
            BINDSTR("revlvl",  REVLVL_LEN, ptr->revlvl),
            BINDSTR("supnum",  SUPNUM_LEN, ptr->supnum),
            BINDSTR("lotnum",  LOTNUM_LEN, ptr->lotnum),
            BINDSTR("invsts",  INVSTS_LEN, ptr->invsts),
            BINDSTR("invsts_prg",  INVSTS_PRG_LEN, ptr->invsts_prg),
            BINDSTR("lodlvl",  LODLVL_LEN, ptr->lodlvl),
            "untcas", COMTYP_INT, sizeof(long), ptr->untcas, 0,
            "untpak", COMTYP_INT, sizeof(long), ptr->untpak, 0,
            BINDSTR("ftpcod",  FTPCOD_LEN, ptr->ftpcod),
            "visflg", COMTYP_INT, sizeof(long), BOOLEAN_TRUE, 0,
            "pipflg", COMTYP_INT, sizeof(long), ptr->pipflg, 0,
            "splflg", COMTYP_INT, sizeof(long), ptr->splflg, 0,
            "locflg", COMTYP_INT, sizeof(long), locflg, 0,
            "lodflg", COMTYP_INT, sizeof(long), lodflg, 0,
            "subflg", COMTYP_INT, sizeof(long), subflg, 0,
            "dtlflg", COMTYP_INT, sizeof(long), dtlflg, 0,
            "prtflg", COMTYP_INT, sizeof(long), prtflg, 0,
            "orgflg", COMTYP_INT, sizeof(long), orgflg, 0,
            "revflg", COMTYP_INT, sizeof(long), revflg, 0,
            "supflg", COMTYP_INT, sizeof(long), supflg, 0,
            "lotflg", COMTYP_INT, sizeof(long), lotflg, 0,
            "qtyflg", COMTYP_INT, sizeof(long), qtyflg, 0, 
            "catch_qty_flg", COMTYP_INT, sizeof(long), catch_qty_flg, 0,
            "frsflg", COMTYP_INT, sizeof(long), ptr->frsflg, 0,
            "min_shelf_hrs", COMTYP_INT, sizeof(long), 
                (ptr->min_shelf_hrs > 0 ? ptr->min_shelf_hrs : 0), 0, 
            BINDSTR("bto_seqnum",BTO_SEQNUM_LEN, ptr->bto_seqnum),
            BINDSTR("slot",SLOT_LEN, ptr->slot),
            "vc_from_rpl", COMTYP_INT, sizeof(long), vc_from_rpl, 0, 
                    0);
    }

    return(ret_status);
}

static long sWritePickBatch(PCKWRK_DATA * ptr)
{

    char         priority[PRCPRI_LEN + 1];
    char         batchcode[BATCOD_LEN + 1];
    char         save_schbat[SCHBAT_LEN + 1];
    char         misc_schbat[SCHBAT_LEN + 1];
    long         ret_status;
    char         *savepoint_p = "alloc_inv_create_batch";
    char         buffer[500];

    if (!ptr || !ptr->schbat || misTrimLen(ptr->schbat, SCHBAT_LEN) == 0)
    {
    sLog(0, "No schedule batch to write out");
    return (eOK);
    }

    /*
     * If the schbat is the same as the last call, then we can just get
     * out.  We will assume that if it is the same for the last call, the
     * pckbat record is already created.  
     * This will save performance in situations
     * where we have a lot of parts in a single pick group, so that we don't
     * keep trying to create a pick batch.  We used to just compare to the
     * very last schbat run, rather that having allocate pick group set the
     * saved schbat.  However, the following situation could occur:
     * 1. Allocate picks for a schedule batch (like move inventory requests 
     *    for MISC)
     * 2. Cancel all picks - which causes the schbat to get deleted.
     * 3. Immediately call allocate inventory to reallocate stuff for that
     *    schbat.  The picks will get created, but we wouldn't create a new
     *    pckbat record, since the schbat was the same on the last call.
     * Therefore, we're only going to optimize this piece of code if we
     * got called through allocate pick group, because that function takes
     * care of writing the pick batch record itself.
     */

    if (strncmp(save_alloc_pick_group_schbat, ptr->schbat, SCHBAT_LEN) == 0)
    {
    sLog(0, "Schedule batch same as saveed schedule batch %s", 
        save_alloc_pick_group_schbat);
    return (eOK);
    }

    memset(save_schbat, 0, sizeof(save_schbat));
    strncpy(save_schbat, ptr->schbat, SCHBAT_LEN);
    
    /* 
     * Before multiple warehouse, originally the miscellaneous schbat is a 
     * constant string "MISC", now we use warehouse id as the miscellaneous 
     * schbat for a specified warehouse.
     */
    memset(misc_schbat, 0, sizeof(misc_schbat));
    misTrimcpy(misc_schbat, ptr->wh_id, SCHBAT_LEN);

    memset(priority, 0, sizeof(priority));
    memset(batchcode, 0, sizeof(batchcode));
    if (misTrimStrncmp(save_schbat, misc_schbat, SCHBAT_LEN) == 0)
    {
    strcpy(batchcode, "Miscellaneous");
    }

    /* Insert the pick batch row.
     * Note, the pick batch for that schbat may already exist, so, 
     * we're going to set a savepoint, and roll back if an error
     * occured (for a constraint violation) to that savepoint, because we don't 
     * know what might be triggering off the create pick batch command, and, 
     * if the create pick batch failed, we want to ignore anything generated by
     * triggers, but, we want allocate inventory to keep on going.
     */

    sLog(0, "Creating a pick batch record for %s", save_schbat); 
    sqlSetSavepoint(savepoint_p);
    sprintf(buffer,
        "create pick batch "
        " where schbat = '%s' "
        "   and batsts = '%s' "
        "   and pricod = '%s' "
        "   and wave_prc_flg = '%ld' "
        "   and batcod       = '%s' "
        "   and wh_id        = '%s' ",
        save_schbat,
        BATSTS_ALOC,
        priority,
        BOOLEAN_FALSE,
        batchcode,
        ptr->wh_id); 

    ret_status = srvInitiateCommand(buffer, NULL);
    if (ret_status != eOK)
    {
    if (ret_status == eDB_UNIQUE_CONS_VIO || 
        ret_status == eSRV_DUPLICATE) 
            sqlRollbackToSavepoint(savepoint_p);    
        else
        return (ret_status);
    }
    
    return (eOK);
}

static long sUpdateShipmentorWorkOrder(char *ship_line_id, 
                       char *wkonum,
                       char *wkorev,
                       char *wkolin,
                       char *client_id,
                       long alcqty,
                       double alc_catch_qty,
                       char *wh_id)
{
    RETURN_STRUCT *  CurPtr = NULL;
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;

    char buffer[2000];
    long ret_status;
    long pckqty, inpqty, oviqty;
    long alcdte_change, change_status;

    long inpqty_update, pckqty_update, oviqty_update;
    double pck_catch_qty;

    char ship_id[SHIP_ID_LEN+1];

    if (ship_line_id && strlen(ship_line_id))
    {
        sprintf(buffer,
            " exec sql with lock "
            " where main_table = 'shipment_line sl' "
            "   and main_pk    = 'sl.ship_line_id' "
            "   and sql_select = 'select sl.pckqty, sl.inpqty, sl.oviqty, sl.ship_id, "
            "                             sl.ship_line_id, s.shpsts, s.alcdte ' "
            "   and sql_from_where = '  from shipment s, shipment_line sl "
            "                       where sl.ship_line_id = ''%s'' "
            "                         and s.ship_id = sl.ship_id ' ",
            ship_line_id);
        ret_status = srvInitiateInline(buffer, &CurPtr);
       
    }
    else 
    {
        sprintf(buffer,
            "select wd.pckqty, wd.pck_catch_qty, wd.inpqty, wd.oviqty, "
            "       wo.wkosts, wo.alcdte, null ship_id "
            "  from wkohdr wo, wkodtl wd "
            " where wo.wkonum = wd.wkonum "
            "   and wo.wkorev = wd.wkorev "
            "   and wo.wh_id  = wd.wh_id "
            "   and wo.client_id = wd.client_id "
            "   and wd.wkonum = '%s' "
            "   and wd.wkorev = '%s' "
            "   and wd.wh_id  = '%s' "
            "   and wd.wkolin = '%s' "
            "   and wd.seqnum = 0 "
            "   and wd.client_id = '%s' ",
            wkonum, wkorev, wh_id, wkolin, client_id);
        ret_status = sqlExecStr(buffer, &res);
    }
    if (ret_status != eOK)
    {
        if (CurPtr) 
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
        }
        else
        {
            sqlFreeResults(res);            
        }        
        return(ret_status);
    }
    else
    {
        if (CurPtr) 
        {
            res = srvGetResults(CurPtr);
        }
    }
    
    alcdte_change = change_status = 0;

    row = sqlGetRow(res);
    if (!sqlIsNull(res, row, "ship_id"))
    {
        strcpy(ship_id, sqlGetString(res, row, "ship_id"));
        if (strcmp(SHPSTS_IN_PROCESS, sqlGetString(res, row, "shpsts")) != 0)
            change_status = 1;
    }
    else
    {
        if (strcmp(WKOSTS_INPROCESS, sqlGetString(res, row, "wkosts")) != 0)
            change_status = 1;
    }
    
    inpqty = sqlGetLong(res, row, "inpqty");
    pckqty = sqlGetLong(res, row, "pckqty");
    oviqty = sqlGetLong(res, row, "oviqty");
    if(!sqlIsNull(res, row, "pck_catch_qty"))
    {
        pck_catch_qty = sqlGetFloat(res, row, "pck_catch_qty");
    }
    else
    {
        pck_catch_qty = 0;
    }
    
    if (!sqlIsNull(res, row, "alcdte"))
        alcdte_change = 1;
    
    if (CurPtr) 
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
    }
    else
    {
        sqlFreeResults(res);            
    }        
    res = NULL;
    
    /* 
     * we need to know whether or not we need to consider oviqty
     */
    /* If this was a catch quantity allocation check for overallocation
     * by catch quantity as opposed to by unit quantity
     */
    if(pck_catch_qty > 0)
    {
        if(alc_catch_qty >= pck_catch_qty)
        {
            /* Allocated fully by catch quantity */
            inpqty_update = inpqty + pckqty;
            oviqty_update = alcqty - pckqty;
            pckqty_update = 0;
        }
        else
        {
            /* Allocated less than the full catch quanity */
            oviqty_update = oviqty + alcqty;
            inpqty_update = inpqty + alcqty;
            pckqty_update = ((pckqty - alcqty) > 0) ? pckqty - alcqty: 1;
        }
    }
    else if (alcqty >= pckqty)
    {
        /* Allocated Fully */
        inpqty_update = inpqty + pckqty;
        oviqty_update = alcqty - pckqty;
        pckqty_update = 0;
    }
    else
    {
        /*since the amount we allocated is not greater */
        /*than the pckqty then we didnt allocate over */
        oviqty_update = 0;
        inpqty_update = inpqty + alcqty;
        pckqty_update = pckqty - alcqty;
    }

    if (ship_line_id && strlen(ship_line_id))
    {
        sprintf(buffer,
            "[update shipment_line "
            "   set inpqty = %d, "
            "       oviqty = %d, "
            "       pckqty = %d "
            " where ship_line_id = '%s' ] "
            "| change shipment line status "
            " where ship_line_id = '%s' "
            "   and linsts = '%s' ",
            inpqty_update, 
            oviqty_update,
            pckqty_update,
            ship_line_id,
            ship_line_id,
            LINSTS_INPROCESS);
    }
    else
    {
        sprintf(buffer,
            "[update wkodtl "
            "   set inpqty = %d, "
            "       oviqty = %d, "
            "       pckqty = %d, "
            "       pck_catch_qty = pck_catch_qty - %f, "
            "       linsts = '%s' "
            " where wkonum = '%s' "
            "   and wkorev = '%s' "
            "   and wh_id  = '%s' "
            "   and wkolin = '%s' "
            "   and seqnum = 0 "
            "   and client_id = '%s' ]",
            inpqty_update, 
            oviqty_update,
            pckqty_update,
            alc_catch_qty,
            LINSTS_INPROCESS,
            wkonum, wkorev, wh_id, wkolin, client_id);
    }
    
    ret_status = srvInitiateCommand(buffer, NULL);
    if (ret_status != eOK)
        return(ret_status);
    
    if (change_status || alcdte_change)
    {
        if (ship_line_id && strlen(ship_line_id))
        {
            sprintf(buffer,
                " [update shipment        "
                "     set alcdte = NULL   "
                "   where ship_id = '%s'] "
                " |                       "
                " change shipment status  "
                "   where ship_id = '%s'  "
                "    and shpsts = '%s'    ",
                ship_id,
                ship_id,
                SHPSTS_IN_PROCESS);

            ret_status = srvInitiateCommand(buffer, NULL);           

        }
        else
        {
            sprintf(buffer,
                "update wkohdr "
                "   set alcdte = NULL, "
                "       wkosts = '%s' "
                " where wkonum = '%s' "
                "   and wkorev = '%s' "
                "   and wh_id  = '%s' "
                "   and client_id = '%s' ",
                WKOSTS_INPROCESS,
                wkonum, wkorev, wh_id, client_id);

             ret_status = sqlExecStr(buffer, NULL);

            }

        if (ret_status != eOK)
            return(ret_status);
    }
    return(eOK);
}

static long sCommitPicks(char *pcktyp, 
                         char *pcksts, 
                         char *pcksts_uom, 
                         char *wrktyp_i, 
                         moca_bool_t *pipflg_i)
{
    long ret_status;
    char buffer[2000];
    char wrktyp[WRKTYP_LEN + 1];
    long total_qty;
    double total_catch_qty;
    char ship_line_id[SHIP_LINE_ID_LEN+1];

    char wkonum[WKONUM_LEN+1];
    char wkolin[WKOLIN_LEN+1];
    char wkorev[WKOREV_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    char wh_id[WH_ID_LEN+1];

    PCKWRK_DATA    *ptr, *tmpptr;

    memset(wrktyp, 0, sizeof(wrktyp));
    if (strcmp(pcktyp, ALLOCATE_PICK) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNSHIP) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNREPLEN) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNREPLENNSHIP) == 0)
    {
        strcpy(wrktyp, WRKTYP_PICK);
    }
    else if (strcmp(pcktyp, ALLOCATE_REPLENISH) == 0 ||
             strcmp(pcktyp, ALLOCATE_INVENTORY_MOVE) == 0 ||
             strcmp(pcktyp, ALLOCATE_TOPOFF_REPLEN) == 0)

        /*
         * ALLOCATE_INVENTORY_MOVE is used when you want to move inventory
         * out of a location (clean it out).  For example, when you want
         * to clean out a pickface and move that inventory back to storage.
         * In those cases, we want to use the replenishment release rules
         * (since we wouldn't need pick labels or anything), but we need
         * to make sure that we can move this inventory back to a 
         * non-replenishable location (like bulk storage).
         * Therefore, this picktyp acts like a replenishment but will ignore
         * the rplflg when allocating a location.
         */

        misTrimcpy(wrktyp, wrktyp_i, sizeof(wrktyp) + 1);
    else
        strcpy(wrktyp, WRKTYP_VOID);

    /* Before we do any processing, just graze over the list and
     * toast any zero qty items...these are bogus... 
     */
    ptr = PickFoundList;
    while (ptr)
    {
        /* set the processed flag to zero for later... */
        ptr->processed = 0;

        if (ptr->pckqty <= 0 && 
            ptr->pck_catch_qty <=0)
        {
            sLog(0, 
                "wh_id: %s, Srcloc: %s, lodlvl: %s, pckqty:%ld, "
                "prtnum:%s, prt_client_id:%s - DELETED",
                ptr->wh_id, ptr->srcloc, ptr->lodlvl,
                ptr->pckqty, ptr->prtnum,ptr->prt_client_id);
        
            sLog(0, "DUE TO BAD PCKQTY");

            if (ptr->prev)
                ptr->prev->next = ptr->next;
            if (ptr->next)
                ptr->next->prev = ptr->prev;

            tmpptr = ptr->next;
            if (ptr == PickFoundList)
                PickFoundList = ptr->next;

            free(ptr);
            ptr = tmpptr;
        }
        else if(ptr->pckqty <= 0 &&
                ptr->pck_catch_qty > 0)
        {
            ptr->pckqty = 1;
            ptr = ptr->next;
        }
        else
        {
            ptr = ptr->next;
        }
    }

    for (ptr = PickFoundList; ptr; ptr = ptr->next)
    {
        ret_status = sWritePickWorks(ptr, wrktyp, pcksts, pcksts_uom, pipflg_i);
        if (ret_status != eOK)
            return (ret_status);
        
        /* Write out a Pick Batch Record Here */
        ret_status = sWritePickBatch(ptr);
        if (ret_status != eOK)
            return (ret_status);
        
        if ((ptr->pipflg != 1) && 
            (ptr->pck_catch_qty == 0))
        {
            sprintf(buffer,
                "update invsum "
                "   set comqty = comqty + %ld "
                " where stoloc = '%s' "
                "   and arecod = '%s' "
                "   and wh_id  = '%s' "
                "   and prtnum = '%s' "
                "   and prt_client_id = '%s' ",
                ptr->pckqty,
                ptr->srcloc, ptr->srcare, ptr->wh_id,
                ptr->prtnum, ptr->prt_client_id);
            
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                sLog(0, "Failed to update invsum to reflect commit");
                return (ret_status);
            }
        }
        else
        {
            sLog(0, "Skipping update of invsum due to PIP pckwrk");
        }
    }

    if (strcmp(pcktyp, ALLOCATE_REPLENISH) == 0 ||
        strcmp(pcktyp, ALLOCATE_INVENTORY_MOVE) == 0 ||
        strcmp(pcktyp, ALLOCATE_TOPOFF_REPLEN) == 0)
    {
        return(eOK);
    }

    memset(ship_line_id, 0, sizeof(ship_line_id));
    memset(wkonum, 0, sizeof(wkonum));
    memset(wkorev, 0, sizeof(wkorev));
    memset(wkolin, 0, sizeof(wkolin));
    memset(client_id, 0, sizeof(client_id));
    memset(wh_id, 0, sizeof(wh_id));

    for (ptr = PickFoundList; ptr; ptr = ptr->next)
    {
        PCKWRK_DATA *lp;

        if (ptr->processed)
            continue;

        if (strlen(ptr->ship_line_id))
            strcpy(ship_line_id, ptr->ship_line_id);
        else
        {
            strcpy(wkonum, ptr->wkonum);
            strcpy(wkorev, ptr->wkorev);
            strcpy(wkolin, ptr->wkolin);
            strcpy(client_id, ptr->client_id);
            strcpy(wh_id, ptr->wh_id);
        }

        total_qty = 0;
        total_catch_qty = 0;
        for (lp = ptr; lp; lp = lp->next)
        {
            /* If we have a shipment (and that matches)
             * or we have a work order (and it matches)
             * then accumulate our quantity for calls 
             * to update shipment
             */
            if ((strlen(ship_line_id) &&
                 strncmp(ship_line_id, 
                    lp->ship_line_id, SHIP_LINE_ID_LEN) == 0) ||
                (strcmp(wkonum, lp->wkonum) == 0 &&
                 strcmp(wkorev, lp->wkorev) == 0 &&
                 strcmp(wkolin, lp->wkolin) == 0 &&
                 strcmp(client_id, lp->client_id) == 0 &&
                 strcmp(wh_id, lp->wh_id) == 0))
            {
                lp->processed = 1;
                total_qty += lp->pckqty;
                total_catch_qty += lp->pck_catch_qty;
            }
        }
        ret_status = sUpdateShipmentorWorkOrder(ship_line_id, 
                            wkonum,
                            wkorev,
                            wkolin,
                            client_id,
                            total_qty,
                            total_catch_qty,
                            wh_id);
        if (ret_status != eOK)
            return(ret_status);
    }
    return (eOK);
}

static long sAllocateLoad(char *lodnum, char *cmbcod_i)
{

    PCKWRK_DATA    *ptr;
    char            cmbcod[CMBCOD_LEN + 1];

    /* Since we've already done all the checks necessary
       to ensure that we have enough quantity to perform
       a full allocation, all we have to do is to buzz
       through the list of Pick requests and fill in the
       appropriate information...in the end, we then make
       the PickFoundList the PickRequestList.... */
    memset(cmbcod, 0, sizeof(cmbcod));
    if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
        misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);
    else
        appNextNum(NUMCOD_CMBCOD, cmbcod);

    for (ptr = PickRequestList; ptr; ptr = ptr->next)
    {
        sAssignDstToPick(ptr);
        strncpy(ptr->cmbcod, cmbcod, CMBCOD_LEN);
    }
    PickFoundList = PickRequestList;
    PickRequestList = NULL;

    return (eOK);
}

/* Attempt to process this allocation using a specific
 * storage location
 */
static long sAllocateFromStoloc(char *stoloc,
                char *pcklvl,
                char *pcktyp,
                char *cmbcod,
                moca_bool_t splflg,
                long alloc_loc_flg,
                char *wh_id)
{
    PICK_POLICY_DATA Policy;
    char  buffer[2000];
    AREA_INFO area_info;
    RETURN_STRUCT *CurPtr = NULL;
    
    mocaDataRow     *row;
    mocaDataRes     *res = NULL;
    char  devcod[DEVCOD_LEN+1];
    char  fifdte[DB_STD_DATE_LEN+1];
    char  invinloc_order_by[RTSTR1_LEN];
    long            ret_status;
    /* We use isAlloLocFailed to indicate if allocate location failed,
     * if yes, we will check if following 'allocate location' tries to allocate
     * location with the same parameters,if yes, we will skip it to provent
     * useless work which will cause a time issue.
     */
    moca_bool_t isAlloLocFailed = BOOLEAN_FALSE;

    /* Check the order we should search pallets within a location */
    memset(invinloc_order_by, 0, sizeof(invinloc_order_by));

    sprintf(buffer,
        "select rtstr1 "
        "  from poldat_view "
        " where polcod = '%s' "
        "   and polvar = '%s' "
        "   and polval = '%s' "
            "   and wh_id = '%s' ",
        POLCOD_ALLOCATE_INV,
        POLVAR_ORDER_INV,
        POLVAL_INVINLOC_ORDER_BY,
            wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
        return (ret_status);
    }
    else if (ret_status == eOK)
    {
       row = sqlGetRow(res);
       if (!sqlIsNull(res, row, "rtstr1"))
       {
           strncpy(invinloc_order_by,
                   sqlGetString(res, row, "rtstr1"), RTSTR1_LEN);
       }
    }
    sqlFreeResults(res);
    res = NULL;

    sprintf(buffer,
    "select aremst.wh_id, aremst.arecod, aremst.lodflg, locmst.devcod,"
    "       aremst.subflg, aremst.dtlflg, aremst.pckcod, "
    "       aremst.loccod, "
    "       to_char(/*=moca_util.days(*/ "
    "                    moca_util.date_diff_days(invsum.olddte, "
    "                                        invsum.newdte) /2 /*=)*/ + "
    "                          invsum.olddte, 'YYYYMMDDHH24MISS') fifdte "
    "  from aremst,invsum, locmst "
    " where locmst.stoloc = '%s' "
    "   and locmst.wh_id  = '%s' "
    "   and locmst.stoloc = invsum.stoloc "
    "   and locmst.arecod = invsum.arecod "
    "   and locmst.wh_id  = invsum.wh_id "
    "   and invsum.prtnum = '%s' "
    "   and invsum.prt_client_id = '%s' "
    "   and locmst.arecod = aremst.arecod "
    "   and locmst.wh_id = aremst.wh_id ",
    stoloc, wh_id, 
    PickRequestList->prtnum, PickRequestList->prt_client_id);
    ret_status = sqlExecStr(buffer, &res);
    if (eOK != ret_status)
    {
        sqlFreeResults(res);
        return(ret_status);
    }
    memset(fifdte, 0, sizeof(fifdte));
    row = sqlGetRow(res);
    strncpy(Policy.wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
    strncpy(Policy.arecod, sqlGetString(res, row, "arecod"), ARECOD_LEN);
    Policy.lodflg = sqlGetBoolean(res, row, "lodflg");
    Policy.subflg = sqlGetBoolean(res, row, "subflg");
    Policy.dtlflg = sqlGetBoolean(res, row, "dtlflg");
    strncpy(Policy.pckcod, sqlGetString(res,row,"pckcod"), PCKCOD_LEN);
    strncpy(devcod, sqlGetString(res, row, "devcod"), DEVCOD_LEN);
    strncpy(fifdte, sqlGetString(res, row, "fifdte"), DB_STD_DATE_LEN);

    memset(&area_info, 0, sizeof(area_info));
    area_info.area_lodflg = sqlGetBoolean(res, row, "lodflg");
    area_info.area_subflg = sqlGetBoolean(res, row, "subflg");
    area_info.area_dtlflg = sqlGetBoolean(res, row, "dtlflg");
    strncpy(area_info.area_loccod, 
        sqlGetString(res, row, "loccod"), LOCCOD_LEN);
    
    sqlFreeResults(res);
    res = NULL;

    if (strlen(PickRequestList->invsts_prg))
    {
        sprintf(buffer,
            " list inventory status progression details "
            "where invsts_prg = '%s' "
            "  and alcflg = 1 ",
            PickRequestList->invsts_prg);
    }
    else
    {
        /* We'll fool it into getting the inventory status */
        sprintf(buffer,
            " publish data "
            "where invsts = '%s' ",
            PickRequestList->invsts);
    }
    
    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (eOK != ret_status)
    {
        sLog(0, "Error getting inventory status(es)");
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CloseTraceFile();
        return(ret_status);
    }
    res = srvGetResults(CurPtr);
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {

        ret_status = sProcessComplexAllocation(&Policy, 
                               stoloc,
                               devcod,
                               sqlGetString(res, row, "invsts"),
                               pcklvl,
                               pcktyp,
                               cmbcod,
                               splflg,
                               fifdte,
                               &area_info,
                               alloc_loc_flg,
                               wh_id,
                               invinloc_order_by,
                               &isAlloLocFailed);
        if (ret_status == eOK)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            return(ret_status);
        }
    }
    srvFreeMemory(SRVRET_STRUCT, CurPtr); 
    return(ret_status);
}

static void sFreeSchBatRes(SCHBAT_RES * ptr)
{
    SCHBAT_RES     *tmpSchbat;
    RES_ASSOCIATE  *tmpRes;

    while (ptr)
    {
    tmpSchbat = ptr;
    if (tmpSchbat->resAssc)
    {
        while (tmpSchbat->resAssc)
        {
        tmpRes = tmpSchbat->resAssc;
        tmpSchbat->resAssc = tmpRes->next;
        free(tmpRes);
        }
    }
    ptr = tmpSchbat->next;
    free(tmpSchbat);
    }

    return;
}

/* Free all memory associated with the parameter list passed in... */
static void sFreePckMovPath(PICK_MOVS * PckMovPaths)
{
    PICK_MOVS      *pckmovs;
    PICK_MOV       *mov;

    if (!PckMovPaths)
    return;

    /* Go To Top of List */
    while (PckMovPaths->prev)
    PckMovPaths = PckMovPaths->prev;

    while (PckMovPaths)
    {
    pckmovs = PckMovPaths;
    if (pckmovs->pckMovPath)
    {
        while (pckmovs->pckMovPath)
        {
        mov = pckmovs->pckMovPath;
        pckmovs->pckMovPath = mov->next;
        free(mov);
        }
    }
    PckMovPaths = pckmovs->next;
    free(pckmovs);
    }

    return;
}

static void trnFreeDstAreList(PCKWRK_DATA *ptr)
{
    SHIP_DST * prev = NULL;

    while(ptr->reserved)
    {
        prev = (SHIP_DST *)(ptr->reserved);
        ptr->reserved = prev->next;
        free(prev);
    }
}

void trnFreePickList(PCKWRK_DATA *List)
{
    PCKWRK_DATA    *ptr;
    
    if (!List)
    return;
    
    sFreePckMovPath(List->pckMovs);
    for (ptr = List; ptr && ptr->next; ptr = ptr->next)
    {
    if (ptr->prev)
        {
            trnFreeDstAreList(ptr->prev);
            free(ptr->prev);
        }
    }

    if (ptr)
    {
    if (ptr->prev)
        {
            trnFreeDstAreList(ptr->prev);
            free(ptr->prev);
        }
        
        trnFreeDstAreList(ptr);
    free(ptr);
    }

}

static void trnFreeResAssociate(RES_ASSOCIATE *Top)
{
    RES_ASSOCIATE *tmp = NULL;

    while (Top)
    {
        tmp = Top->next;
        free(Top);
        Top = tmp;
    }

    return;

}

static void sCleanupPickRequestList()
{
    PCKWRK_DATA *ptr = NULL;
    PCKWRK_DATA *next = NULL;
    PCKWRK_DATA *prev = NULL;
    PCKWRK_DATA *last = NULL;

    for (ptr = PickRequestList; ptr; ptr = ptr->next)
    {
        if (ptr->pckqty == 0)
        {
            /* 
             * No point to this element - copy off its 
             * forward and backward pointers
             */
            next = ptr->next;
            last = ptr->prev;

            if (next)
                next->prev = last;
            if (prev)
                prev->next = next;
            
            if (ptr == PickRequestList)
                PickRequestList = next;
                
            free(ptr);
        }
    }
    return;
}

/*
 * it's called by trnAllocateInventory
 * it will building the shipment destination location list for the pick work
 * according the data store the shp_dst_loc, and set the pick->reserved field 
 * point to the list of staging locations
 *  params:
 *      ptr     -   pick work 
 */
static void sBuildShpDstList(PCKWRK_DATA * ptr)
{
    char ship_id[SHIP_ID_LEN + 1];
    char buffer[512];

    long ret_status = 0;
    RETURN_STRUCT  *CurPtr = NULL;
    mocaDataRes * res = NULL;
    mocaDataRow * row;
    SHIP_DST * head = NULL;
    SHIP_DST * previous = NULL;

    memset (ship_id, 0, sizeof(ship_id));
    memset (buffer, 0, sizeof(buffer));

    
    /*
     * get the ship_id for this pick work
     */
    sGetShipID(ptr, ship_id);

    /*
     * taking account into the shp_dst_loc table
     * by the ship_id
     */
    sprintf(buffer, 
            " list shipment destination locations "
            "where ship_id = '%s'",
            ship_id);

    ret_status = srvInitiateCommand(buffer, &CurPtr);

    if (ret_status == eOK)
    {
        res = srvGetResults(CurPtr);

        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            /*
             * add a node for the staging location, and add it into the
             * right position of the staging location list for the pick work
             *
             * WMD-37363 
             * replace NULL with "" as parameters to be passed down, the reason 
             * is that C compiler will take NULL parameter as string "(null)".
             */
            sAttachDstToPick(ptr, 
                            sqlGetString(res, row, "dstare"),
                            sqlIsNull(res, row, "dstloc") ? 
                                "": 
                                sqlGetString(res, row, "dstloc"),
                            sqlIsNull(res, row, "src_bldg_id")?
                                "":   
                                sqlGetString(res, row, "src_bldg_id"),
                            sqlGetString(res, row, "wh_id"),
                            sqlIsNull(res, row, "srtseq") ? 
                                0L : 
                                sqlGetLong(res, row, "srtseq"));
        }
    }
    else ptr->reserved = NULL;

    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;
    res = NULL;
    
    /*
     * clear the pick work's dstare/dstloc
     */
    memset(ptr->dstare, 0, ARECOD_LEN + 1);
    memset(ptr->dstloc, 0, STOLOC_LEN + 1);
}

/*
 * it's called by sBuildPickRequestArray
 *  it's used to determine if these pick works have same destination
 *  area data. if they have same data, we will merge them into one pick 
 *  work list and the PickRequestArray point to the array of the pick 
 *  work LIST, and every pick work in each pick work LIST has same 
 *  destination area data. if the pPick1 equal to pPick2, it will return 1;
 *  otherwize it will return 0;
 *  Params:
 *      pPick1  -   Pick work one
 *      pPick2  -   Pick work two
 */
static long sPickShpDstAreEqualTo(PCKWRK_DATA * pPick1, PCKWRK_DATA * pPick2)
{
    SHIP_DST *pdstare1, *pdstare2;
    long ret = 0L;

    /*
     * if two picks have not destination location list pointed
     * by the reserved field of pick and they has the same destination area
     * just consider they are equal
     */
    if (pPick1->reserved == NULL &&
        pPick2->reserved == NULL &&
        strncmp(pPick1->dstare, pPick2->dstare, ARECOD_LEN) == 0 &&
        strncmp(pPick1->wh_id, pPick2->wh_id, WH_ID_LEN) == 0)
        return 1L;

    pdstare1 = (SHIP_DST *) (pPick1->reserved);
    pdstare2 = (SHIP_DST *) (pPick2->reserved);

    /*
     * we have to go through all the locations in the destination location list
     * for these two picks, and to determine they all have same destination 
     * area code. if they have, means they are equal
     */
    while(pdstare1 && pdstare2)
    {
        if (strncmp(pdstare1->wh_id, pdstare2->wh_id, WH_ID_LEN) ||
            strncmp(pdstare1->dstare, pdstare2->dstare, ARECOD_LEN) ||
            strncmp(pdstare1->src_bldg_id, pdstare2->src_bldg_id, BLDG_ID_LEN))
            return ret;

        pdstare1 = pdstare1->next;
        pdstare2 = pdstare2->next;
    }

    if (pdstare1 == NULL && pdstare2 == NULL)
        ret = 1L;
    return ret;
}

/* free the pick list array */
static void sFreePickListArray()
{
    long index;
    PCKWRK_DATA * ptr;
    for(index = 0; index < PickRequestArrayLength; index++)
    {
        ptr = PickRequestArray[index];
        if(ptr)
            trnFreePickList(ptr);
    }
    free(PickRequestArray);
    PickRequestArray = NULL;
    PickRequestArrayLength = 0L;
    PickRequestList = NULL;
}

/* join the Pick list of pArray[nxtindex] to pArray[index] */
static void sMergePickListArray(PCKWRK_DATA ** pArray, 
                                long index, 
                                long nxtindex)
{
    PCKWRK_DATA * ptr, *nptr;
    ptr = pArray[index];
    nptr = pArray[nxtindex];

    while(ptr->next)
    {
        ptr = ptr->next;
    }
    
    ptr->next = nptr;
    nptr->prev = ptr;

    pArray[nxtindex] = NULL;
}

/*
 * there might be some null value in the pArray. so
 * purge the cell which is null, and generate a new array
 * to contain the values.
 */
static PCKWRK_DATA ** sPurgePickListArray(PCKWRK_DATA ** pArray, 
                                          long len, 
                                          long newlen)
{
    int index, newindex;
    PCKWRK_DATA ** pReturn;
    pReturn =(PCKWRK_DATA **) calloc(newlen, sizeof(PCKWRK_DATA *));

    if (pReturn == NULL)    
    {
        free(pArray);
        return NULL;
    }
    
    newindex = 0;

    for (index = 0; index < len; index ++)
    {
        if (pArray[index] != NULL && newindex < newlen)
        {
            pReturn[newindex++] = pArray[index];
        }
    }

    free(pArray);
    return pReturn;
}

/*
 *  It's called by trnAllocateInventory
 *  it will divided the pick working into several pick work list array by 
 *  the destination location list.
 *      Params:
 *          pPick (in)      -   Pick List 
 *                              which has the destination location data
 *          length(out)     -   The pick work list array length
 *      Return:
 *          the Pick Work List Array head address
 */
static PCKWRK_DATA ** sBuildPickRequestArray(PCKWRK_DATA * pPick, long *length)
{
    /*
     * firstly, we will group by the concod, if concod are same
     * we will put the request pick work into the same list
     * and we don't care the dstare if they has the same concod.
     */
    PCKWRK_DATA ** pArray = NULL;
    long len = 0, index, mergetime;
    PCKWRK_DATA * ptr = pPick, *mptr, *nxt;
    while (ptr)
    {
        nxt = ptr->next;

        ptr->next = NULL;
        ptr->prev = NULL;
        mptr = NULL;
        for (index = 0; index < len; index ++)
        {
            if (strncmp(pArray[index]->concod, ptr->concod, CONCOD_LEN) == 0)
            {
                mptr = pArray[index];
                break;
            }
        }

        if (mptr == NULL)
        {
            pArray =(PCKWRK_DATA **) realloc(pArray, sizeof(PCKWRK_DATA *) * 
                                                     (len + 1));
            pArray[len] = ptr;
            len ++;
        }
        else
        {
            while(mptr->next)
            {
                mptr = mptr->next;
            }
            mptr->next = ptr;
            ptr->prev = mptr;
        }
        ptr = nxt;
    }

    /*
     * now we have several list of the picking work
     * we need to merge them by the same dstare
     */
    mergetime = 0;
    for(index = 0; index < len; index++)
    {
        int nxtindex;
        ptr = pArray[index];

        if (ptr == NULL) continue;

        for(nxtindex = 0; nxtindex < len;  nxtindex++)
        {
            if (index == nxtindex) continue;

            mptr = pArray[nxtindex];

            if(mptr == NULL) continue;

            if (sPickShpDstAreEqualTo(ptr, mptr) > 0)
            {
                sMergePickListArray(pArray, index, nxtindex);
                mergetime++;
            }
        }
    }

    *length = len - mergetime;
    pArray = sPurgePickListArray(pArray, len, *length);

    return pArray;
}

/*
 *  key function
 *  this function is called by trnAllocateInventory
 *  is used to build the pick building sequence for the destination staging
 *  locations data.
 *      params:
 *          pdst        -       destination staging location list
 *          size (out)  -       the number of building codes containning 
 *                              in the return building array
 *      Return:
 *          the head of the pick building
 */
static char * sBuildPickBuildingSequence(SHIP_DST * pdst, long * size)
{
    char * ret = NULL;
    
    long ret_status = 0;
    RETURN_STRUCT ** pCmdRes = NULL;
    mocaDataRes * res = NULL;
    mocaDataRow * row = NULL;

    long count = 0;
    long maxlen = 0;
    long index = 0;
    long resindex = 0;

    char buffer[512];
    char *psrc_bldg_id = NULL;

    *size = 0L;

    while(pdst)
    {
        RETURN_STRUCT * CurPtr = NULL;

        /* Next, get the building in the correct sequences for checking */
        sprintf (buffer,
                 " list pick building sequences "
                 " where bldg_id = '%s' "
                 "   and wh_id = '%s' ",
                pdst->dst_bldg_id,
                pdst->wh_id);

        ret_status = srvInitiateInline (buffer, &CurPtr);

        if (ret_status != eOK)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            sprintf (buffer,
                     "[select "
                     "   '%s' bldg_id, "
                     "   '%s' dst_bldg_id, "
                     "   '%s' src_bldg_id, "
                     "     0  srtseq, "
                     "   '%s' wh_id "
                     "   from dual]",
                     pdst->dst_bldg_id,
                     pdst->dst_bldg_id,
                     pdst->dst_bldg_id,
                     pdst->wh_id);
            
            ret_status = srvInitiateInline (buffer, &CurPtr);
            if (ret_status != eOK)
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr= NULL;
            }
        }
        if (CurPtr)
        {
            pCmdRes = (RETURN_STRUCT **)realloc(pCmdRes,sizeof(RETURN_STRUCT *)
                                                        * (count + 1));
            pCmdRes[count++] = CurPtr;

            res = srvGetResults(CurPtr);
            if (sqlGetNumRows(res) > maxlen) maxlen = sqlGetNumRows(res);
        }
        pdst = pdst->next;
    }

    psrc_bldg_id = (char *)calloc(count,maxlen * 
                                        (BLDG_ID_LEN + 1) * 
                                        sizeof(char));

    /*
     * here, we need to know that we don't know where
     * the inventory come from as to we did not allocate
     * the inventory, but the 'list pick building sequence'
     * want the dst_bldg_id passed in. how to get the pick building
     * sequence by the rows in the shp_dst_loc table for the pick work
     * here is the solution:
     *  for a sample:
     *      the Pick work has a list of the shipment destination location
     *      they are:    B1 - B1L1      B2  -  B2L2     B3  -  B3L3
     *      and the pick building sequence in the pck_bldg_seq are
     *                  src_bldg_id     dst_bldg_id    srtseq
     *                  B1              B1              0
     *                  B1              B3              1
     *                  B1              B2              2
     *
     *                  B2              B2              0
     *                  B2              B3              1
     *                  B2              B1              2
     *
     *                  B3              B3              0
     *                  B3              B1              1
     *                  B3              B2              2
     *  the first process step is to get the srtseq = 0 rows' dst_bldg_id, they
     *  are                   B1  B2  B3
     *  then the setseq = 1,  B3  B3  B1
     *  then the srtseq = 2,  B2  B1  B2
     *  merge them, the the current checking buidling id is exist in the front 
     *  of the list of building, skip it.
     *  so the pick builing sequenc is B1, B2, B3
     */
    for(resindex = 0; resindex < count; resindex ++)
    {
        res = srvGetResults(pCmdRes[resindex]);
        index = 0;
        for(row = sqlGetRow(res) ; 
            row; 
            row = sqlGetNextRow(row))
        {
            misTrimcpy((char *)&(psrc_bldg_id[(BLDG_ID_LEN + 1) *
                                                   sizeof(char) * 
                                                        (count  *
                                                        index + 
                                                     resindex)]),
                       sqlGetString(res, row, "src_bldg_id"),
                       BLDG_ID_LEN);
            index ++;
        }
        srvFreeMemory(SRVRET_STRUCT, pCmdRes[resindex]);
    }

    if (pCmdRes)
        free(pCmdRes);
    pCmdRes = NULL;
    res = NULL;


    for(index = 0; index < maxlen * count; index ++)
    {
        char * ptmp_bldg_id = (char *)&(psrc_bldg_id[(BLDG_ID_LEN + 1) * 
                                                      sizeof(char) * 
                                                      index]);

        if (misTrimLen(ptmp_bldg_id, BLDG_ID_LEN))
        {
            int rindex = 0;
            for (rindex = 0; rindex < index; rindex ++)
            {
                char * rtmp_bldg_id = (char *)&(psrc_bldg_id[
                                                (BLDG_ID_LEN + 1) * 
                                                sizeof(char) * 
                                                rindex]);
                if (misTrimLen(rtmp_bldg_id, BLDG_ID_LEN) &&
                    misTrimStrncmp (ptmp_bldg_id, rtmp_bldg_id, BLDG_ID_LEN) 
                                    == 0)
                {
                    memset(ptmp_bldg_id, 0, BLDG_ID_LEN);
                    break;
                }
            }
        }
    }

    /*
     * building the return structure
     */

    *size = 0L;
    for (index = 0; index < maxlen * count; index++)
    {
        char * ptmp_bldg_id = (char *)&(psrc_bldg_id[(BLDG_ID_LEN + 1) * 
                                                      sizeof(char) * 
                                                      index]);

        if (misTrimLen(ptmp_bldg_id, BLDG_ID_LEN))
        {
            ret = (char *)realloc(ret, (BLDG_ID_LEN + 1) * 
                                        sizeof(char) * 
                                        ((*size) + 1) );
            misTrimcpy((char *)&(ret[((*size)++) * 
                                     (BLDG_ID_LEN + 1) * 
                                     sizeof(char)]),
                       ptmp_bldg_id,
                       BLDG_ID_LEN);
        }
    }

    if(psrc_bldg_id != NULL)
        free(psrc_bldg_id);

    return ret;
}

/* construct the PickRequestArray to one list of pick work */
static void sLoadtoPickRequestList()
{
    long index;

    PickRequestList = NULL;

    for(index = 0; index < PickRequestArrayLength; index++)
    {
        if (PickRequestArray[index])
        {
            if (PickRequestList == NULL)
            {
                PickRequestList = PickRequestArray[index];
                PickRequestArray[index] = NULL;
            }
            else
            {
                PCKWRK_DATA * ptr;
                ptr = PickRequestList;
                while(ptr->next)
                {
                    ptr = ptr->next;
                }
                PickRequestArray[index]->prev = ptr;
                ptr->next = PickRequestArray[index];
                PickRequestArray[index] = NULL;
            }
        }
    }
}

/*
 * check if the PickRequestArray is empty
 * if the PickRequestArray is empty, it means
 * all the request pick has been allocated
 */
static long IsPickRequestArrayEmpty()
{
    long ret = 1;

    long index;

    for (index = 0; index < PickRequestArrayLength; index++)
    {
        if (PickRequestArray[index] != NULL)
        {
            ret = 0;
            break;
        }
    }

    return ret;
}

long trnAllocateInventory(char *pcktyp_i, 
                char *prtnum_i, char *prt_client_id_i, 
                long pckqty_i, 
                double pck_catch_qty_i,
                char *orgcod_i, char *revlvl_i, char *lotnum_i,
                char *invsts_i, 
                char *invsts_prg_i,
                char *schbat_i,
                char *ship_id_i, char *ship_line_id_i, 
                char *wkonum_i, char *wkorev_i, char *wkolin_i,
                char *carcod_i, char *srvlvl_i,
                char *client_id_i,
                char *ordnum_i, char *stcust_i, char *rtcust_i,
                char *ordlin_i, char *ordsln_i,
                char *concod_i, char *segqty_i,
                char *stoloc_i, char *lodnum_i,
                char *dstare_i, char *dstloc_i,
                PCKWRK_DATA ** Picks,
                char *srcare_i, char *pcklvl_i,
                char *pcksts_i,
                char *pcksts_uom_i,
                moca_bool_t splflg_i, 
                moca_bool_t *ovralcflg_i,
                long  untcas_i, long  untpak_i, long  untpal_i,
                long min_shelf_hrs_i,
                moca_bool_t frsflg_i,
                char *frsdte_i,
                char *trace_suffix_i,
                char *pipcod_i,
                long skip_invlkp_i,
                long alloc_loc_flg_i,
                char *wh_id_i,
                char *wrktyp_i,
                moca_bool_t *pipflg_i,
                char *cmbcod_i,
                char *alc_search_path_i,
                char *supnum_i,
                moca_bool_t vc_from_rpl_i)
{
    long        ret_status;
    long        ii;
    long        current_time;
    
    
    RETURN_STRUCT  *CurPtr = NULL;
    PCKWRK_DATA    *ptr;
    char            buffer[1000];
    long            untcas, untpak, untpal;
    long            pip_enabled;
    char            src_bldg_id[BLDG_ID_LEN +1];
    char            pcktyp[100];
    char            cur_invsts[INVSTS_LEN + 1];
    char            wrktyp[WRKTYP_LEN + 1];
    char            cmbcod[CMBCOD_LEN + 1];

    mocaDataRes    *res = NULL;
    mocaDataRow    *row, *tmprow, *PolRow;
    static mocaDataRes *PolRes = NULL;

    static int firstTime = TRUE;

    untcas = untcas_i;
    untpak = untpak_i;
    untpal = untpal_i;

    /* JJS - Will be TRUE if "allocate inventory" was called
     * from "complete emergency replenishment" - our purpose
     * here during allocation is just to set pckwrk.vc_from_rpl
     * when a pick is created in table pckwrk, but this bool is
     * later used to determine where labels get printed to
     */
    vc_from_rpl = vc_from_rpl_i;

    memset(wrktyp, 0, sizeof(wrktyp));
    memset(cmbcod, 0, sizeof(cmbcod));

    if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
        misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);

    /*If it is a replenishment type pick, it will be changed to its specific */
    /*type*/
    if (strcmp(pcktyp_i, ALLOCATE_REPLENISH) == 0 ||
             strcmp(pcktyp_i, ALLOCATE_INVENTORY_MOVE) == 0 ||
         strcmp(pcktyp_i, ALLOCATE_TOPOFF_REPLEN) == 0)
    {
        if (wrktyp_i && misTrimLen(wrktyp_i, WRKTYP_LEN))
        {             
            misTrimcpy(wrktyp, wrktyp_i, WRKTYP_LEN);
        }
        else
        {
            misTrimcpy(wrktyp, WRKTYP_MANUAL, WRKTYP_LEN);
        }
     }


    /* Load ASSET-TRACKING configuration for later use. */
    sprintf(buffer, "check asset category enabled "
                     " configuration where asset_cat = '%s'"
                     "                 and wh_id = '%s' ",
                    ASSET_CAT_INV,
                    wh_id_i);

    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK)
    {
        misTrc(T_FLOW,
            "Error while checking asset category configuration ");
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return(ret_status);
    }
    else
    {
        res = srvGetResults(CurPtr);
        row = sqlGetRow(res);
        asset_track_enabled = sqlGetLong(res, row, "ena_flg");
        if (asset_track_enabled == BOOLEAN_FALSE)
        {
            misTrc(T_FLOW,
                   "Asset category is disabled ");
        }
    }
    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;
    res = NULL;

    /*
     * Get the policy that determines whether log file tracing is
     * enabled.
     */

    misTrc(T_FLOW, "Entering trnAllocInv... VAR version 8");
    
    ret_status = OpenTraceFile(wh_id_i, trace_suffix_i);
    if (ret_status != eOK)
        return (ret_status);
    
    current_time = time(0);
    sLog(0, 
        "Allocating inventory - %s"
        "   Shipment      %s\n"
        "   Shipment Line %s\n"
        "   Order Number  %s\n"
        "   Order Line    %s\n"
        "   Order Subline %s\n"
        "   Part          %s\n"
        "   Part Client   %s\n"
        "   Lot Number    %s\n"
        "   Origin Code   %s\n"
        "   Revision Lvl  %s\n"
	"   Supplier Number %s\n"
        "   Inventory Sts %s\n"
        "   InvSts Prg    %s\n"
        "   Quantity:     %d\n"
        "   Catch Qty:    %f\n"
        "   Work Order    %s\n"
        "   WKO Line      %s\n"
        "   Load Number   %s\n"
        "   Skip Flag     %ld\n"
        "   Alloc Loc Flg %ld\n"
        "   Dest Area     %s\n"
        "   Dest Location %s\n"
        "   Allocation Search Path %s\n"
        "   Warehouse     %s\n",
        ctime(&current_time),
        (ship_id_i ? ship_id_i : ""),
        (ship_line_id_i ? ship_line_id_i : ""),
        (ordnum_i ? ordnum_i : ""),
        (ordlin_i ? ordlin_i : ""),
        (ordsln_i ? ordsln_i : ""),
        (prtnum_i ? prtnum_i : ""),
        (prt_client_id_i ? prt_client_id_i : ""),
        (lotnum_i ? lotnum_i : ""),
        (orgcod_i ? orgcod_i : ""),
        (revlvl_i ? revlvl_i : ""),
        (supnum_i ? supnum_i : ""),
        (invsts_i ? invsts_i : ""),
        (invsts_prg_i ? invsts_prg_i : ""),
        pckqty_i,
        pck_catch_qty_i,
        (wkonum_i ? wkonum_i : ""),
        (wkolin_i ? wkolin_i : ""),
        (lodnum_i ? lodnum_i : ""),
         skip_invlkp_i,
         alloc_loc_flg_i,
        (dstare_i ? dstare_i : ""),
        (dstloc_i ? dstloc_i : ""),
        (alc_search_path_i ? alc_search_path_i : ""),
        wh_id_i);

    /* Check to see if the pre-inventory pick code was passed in.  If it
       was passed in then use that value.  'Y' means enabled,
       everything else means disabled.  If the code was not passed to this
       function, then use the policy to determine if the pre-inventory
       pick processing should be enabled.  If no policy exists, then the 
       default behavior is disabled. */

    pip_enabled = FALSE; /* Default behavior */

    if (pipcod_i && misTrimLen(pipcod_i, PIPCOD_LEN)) 
    {
        if (strncmp(pipcod_i, "Y", PIPCOD_LEN) == 0)
            pip_enabled = TRUE;
    }
    else 
    {
        if (PolRes == NULL)
        {
            misTrc(T_FLOW, "Loading pre-inventory picking policy");
            
            /* Get the policy */
            sprintf(buffer,
                    "select wh_id, rtnum1 "
                    "  from poldat_view "
                    " where polcod = '%s' "
                    "   and polvar = '%s' "
                    "   and polval = '%s' ",
                    POLCOD_PRE_INV_PICKING,
                    POLVAR_INSTALLED,
                    POLVAL_INSTALLED);
                
            ret_status = sqlExecStr(buffer, &PolRes);
            if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
            {
                sqlFreeResults(PolRes);
                PolRes = NULL;
            }
            
            /* Cache the policies. */
            misFlagCachedMemory((OSFPTR) sqlFreeResults, PolRes);
        }

        pip_enabled = FALSE;
        if (PolRes != NULL)
        {
            for(PolRow = sqlGetRow(PolRes); PolRow; 
                PolRow = sqlGetNextRow(PolRow))
            {
                if (!strcmp(sqlGetString(PolRes, PolRow, "wh_id"), wh_id_i))
                    pip_enabled = sqlGetLong(PolRes, PolRow, "rtnum1");
            }
        }
    }
 
    if (PickFoundList)
    {
        trnFreePickList(PickFoundList);
        PickFoundList = NULL;
    }
    if (PickRequestList)
    {
        trnFreePickList(PickRequestList);
        PickRequestList = NULL;
    }

    if (PickRequestArray)
    {
        free(PickRequestArray);
        PickRequestArray = NULL;
    }
    PickRequestArrayLength = 0L;

    if (PckMovPathHead)
    {
        sFreePckMovPath(PckMovPathHead);
        PckMovPathHead = NULL;
    }
    if (schbatRes)
    {
        sFreeSchBatRes(schbatRes);
        schbatRes = NULL;
    }

    if (Picks)
        *Picks = NULL;


    /* 9/28/95 STE
     * If we are passed in a load, we don't 
     * need the pckqty, segqty, or prtnum.
     * Otherwise we do 
     */
    if (((!prtnum_i || !misTrimLen(prtnum_i, PRTNUM_LEN) ||
          !prt_client_id_i || !misTrimLen(prt_client_id_i, CLIENT_ID_LEN)) ||
          pckqty_i < 0 || !segqty_i || strlen(segqty_i) == 0) &&
        !(lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN)))
    {
        CloseTraceFile();
        return (eINVALID_ARGS);
    }

    memset(pcktyp, 0, sizeof(pcktyp));
    if (strcmp(pcktyp_i, ALLOCATE_PICK) != 0 &&
        strcmp(pcktyp_i, ALLOCATE_PICKNSHIP) != 0 &&
        strcmp(pcktyp_i, ALLOCATE_PICKNREPLEN) != 0 &&
        strcmp(pcktyp_i, ALLOCATE_PICKNREPLENNSHIP) != 0 &&
        strcmp(pcktyp_i, ALLOCATE_REPLENISH) != 0 &&
        strcmp(pcktyp_i, ALLOCATE_INVENTORY_MOVE) != 0 &&
        strcmp(pcktyp_i, ALLOCATE_TOPOFF_REPLEN) != 0)
    {
        sLog(0, "ERROR: Invalid pick type: %s", pcktyp_i);
        CloseTraceFile();
        return (eINVALID_ARGS);
    }
    else
    {
        misTrimcpy(pcktyp, pcktyp_i, sizeof(pcktyp)-1);
    }
  
    /* 
     * If the allocation request is for an order pick type, but no
     * order entity is passed in (i.e. ship_id, wkonum), then
     * convert the request type to a replenishment
     */
    if ((strcmp(pcktyp, ALLOCATE_PICK) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNSHIP) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNREPLEN) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNREPLENNSHIP) == 0) &&
        ((!ship_id_i || misTrimLen(ship_id_i, SHIP_ID_LEN) == 0) &&
        (!wkonum_i || misTrimLen(wkonum_i, WKONUM_LEN) == 0)))
    {
        sLog(0, 
         "Converting pcktyp to %s from %s - no ship_id, "
         "  wkonum specified",
         ALLOCATE_REPLENISH, pcktyp);

        strcpy(pcktyp, ALLOCATE_REPLENISH);
    }

    /*
     * For now, we're only going to allow the allocation of locations during
     * inventory allocation if we're doing it for replenishments.
     */
 
    if (alloc_loc_flg_i && strcmp(pcktyp, ALLOCATE_REPLENISH))
    {
        sLog(0, "ERROR:  Attempting to allocation location for pcktyp other"
                " than ALLOCATE_REPLEN (REPLEN).  This is not allowed. ");
        CloseTraceFile();
        return (eINVALID_ARGS);
    }

    /* 
     * We're require one of the two, but default back to invsts if neither is
     * passed.
     */
    if (!misTrimLen(invsts_i, INVSTS_LEN) &&
        !misTrimLen(invsts_prg_i, INVSTS_PRG_LEN))
    {
        sLog(0, "ERROR: Missing inventory status");
        CloseTraceFile();
        return(eINVALID_ARGS);
    }
    
    /* if we were given a dstloc, use that to figure the arecod */

    PickRequestList = NULL;
    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
    {
        /* NOTE:  We purposely left out adding wkonum, wkorev, wkolin
         * to the call to parse load params
         */
        ret_status = sParseLoadParams(lodnum_i, prtnum_i, 
                          prt_client_id_i, pckqty_i,
                          orgcod_i, revlvl_i, supnum_i, lotnum_i,
                          invsts_i, carcod_i, srvlvl_i,
                          schbat_i, ship_id_i, 
                          ship_line_id_i, 
                          client_id_i,
                          ordnum_i, 
                          stcust_i, rtcust_i, ordlin_i, ordsln_i,
                          concod_i, segqty_i, 
                          dstare_i,   dstloc_i,   splflg_i,
                          untcas, untpak,
                          wh_id_i);
    }
    else
    {
        ret_status = sParseParams(prtnum_i, prt_client_id_i,
                        pckqty_i, pck_catch_qty_i,
                        orgcod_i, revlvl_i, supnum_i, lotnum_i,
                        invsts_i, invsts_prg_i,
                        carcod_i, srvlvl_i,
                        schbat_i, ship_id_i,
                        ship_line_id_i,
                        wkonum_i, wkorev_i, wkolin_i,
                        client_id_i,
                        ordnum_i, 
                        stcust_i, rtcust_i, ordlin_i, ordsln_i,
                        concod_i, segqty_i, 
                        dstare_i, dstloc_i, splflg_i,
                        untcas, untpak, untpal,
                        min_shelf_hrs_i,
                        frsflg_i,
                        frsdte_i,
                        alc_search_path_i, /* allocation search path */
                        wh_id_i);
    }
    
    if (ret_status != eOK || PickRequestList == NULL)
    {
        PickRequestList = NULL;
        CloseTraceFile();
        return (ret_status);
    }

    /* Loop through to see if we need to pickup any of the destination */
    /* areas because they were not sent in.  The string that signifies   */
    /* an empty dstare is one space character.  Four question marks  */
    /* was left in to handle old cases which should not but still might   */
    /* occur.  In every case that I tried,  a dstare is already defined   */
    /* this point so this code will not get run but I have left it here to   */
    /* handle those rare cases */
    ptr = PickRequestList;
    while (ptr)
    {
        if ((strncmp(ptr->dstare, " ", ARECOD_LEN) == 0) || 
            (strncmp(ptr->dstare, "????", ARECOD_LEN) == 0) || 
            (strncmp(ptr->dstare, "", ARECOD_LEN) == 0))
        {
        /*
         * here, we will take account into the shp_dst_loc table to see if 
         * the ship_id against the ptr has rows in the shp_dst_loc table
         * if there are some row for this ship_id, the sBuildShpDstList 
         * function will building a list of this rows, and make the
         * pick work's reserved field point to it; otherwize, there's
         * no rows for the pick work. it will call get order destination
         * command to get the destination data.
         */
            sBuildShpDstList(ptr);

            if (ptr->reserved == NULL)
            /*
             * no rows defined in the shp_dst_loc table for pick work
             */
            {
                sprintf(buffer,
                        "get order destination "
                        " where ship_id = '%s' and ship_line_id = '%s' "
                        "   and wkonum = '%s' and wkolin = '%s' "
                        "   and wkorev = '%s' "
                        "   and client_id = '%s' and ordnum = '%s' "
                        "   and ordlin    = '%s' and ordsln = '%s' "
                        "   and carcod    = '%s' and srvlvl = '%s' "
                        "   and marcod    = '%s' and ordtyp = '%s' "
                        "   and wh_id     = '%s' ",
                        ptr->ship_id,
                        ptr->ship_line_id,
                        ptr->wkonum,
                        ptr->wkolin,
                        ptr->wkorev,
                        ptr->client_id,
                        ptr->ordnum,
                        ptr->ordlin,
                        ptr->ordsln,
                        ptr->carcod,
                        ptr->srvlvl,
                        ptr->marcod,
                        ptr->ordtyp,
                        ptr->wh_id);

                sLog(0, "Attempting to get order destination");
                CurPtr = NULL;
                ret_status = srvInitiateCommand(buffer, &CurPtr);

                if (ret_status != eOK)
                {
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    trnFreePickList(PickRequestList);
                    PickRequestList = NULL;
                    sLog(0,
                         "\nERROR: Could not determine order destination: %d",
                        ret_status);
                    CloseTraceFile();
                    return (ret_status);
                }
                res = srvGetResults(CurPtr);
                tmprow = sqlGetRow(res);

                sAttachDstToPick(ptr,
                                sqlGetString(res, tmprow, "dstare"),
                                sqlGetString(res, tmprow, "dstloc"),
                                NULL,
                                sqlGetString(res, tmprow, "wh_id"),
                                0L);

                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;
                res = NULL;
                tmprow = NULL;
            }        
        }
        else
        /*
         * if the dstare/dstloc is passed in, then
         * just 
         */
        {
            sAttachDstToPick(ptr,
                            ptr->dstare,
                            ptr->dstloc,
                            NULL,
                            ptr->wh_id,
                            0L);

        }

        memset(ptr->dstare, 0, ARECOD_LEN);
        memset(ptr->dstloc, 0, STOLOC_LEN);
        ptr = ptr->next;
    }

    /* Verify Get Order Dest worked */
    ptr = PickRequestList;
    while (ptr)
    {

        if (ptr->reserved == NULL)
        {
            trnFreePickList(PickRequestList);
            PickRequestList = NULL;
            CloseTraceFile();
            return (eINVALID_ARGS);
        }
        ptr = ptr->next;
    }
    ret_status = sLoadDefaultResAssocPolicies();
    if (ret_status != eOK)
    {
        trnFreePickList(PickRequestList);
        trnFreeResAssociate(resAssociate);
        PickRequestList = NULL;
        CloseTraceFile();
        return (ret_status);
    }

    if (firstTime)
    {
        misFlagCachedMemory ((OSFPTR) trnFreeResAssociate, resAssociate);
        firstTime = FALSE;
    }

    /* If we are not skipping the inventory lookup, call out to this... */
    if (!skip_invlkp_i) 
    {
        /* Check the quantities won't cause an over allocation */
        ret_status = sVerifyPickQuantity(PickRequestList, ovralcflg_i,
                     pcktyp);
    }

    /* if we are at zero, return */
    if (ret_status != eOK)
    {
        /* Failure...make sure all memory is free  */
        trnFreePickList(PickRequestList);
        PickRequestList = NULL;
        CloseTraceFile();
        return (ret_status);
    }
    /* Let's go make sure we still have work to do */
    sCleanupPickRequestList();
    if (!PickRequestList)
    {
        sLog(0, "After verify pick quantity - pick request list empty");
        sLog(0, "Getting out.");
        CloseTraceFile();
        return(eOK);
    }

    if (strcmp(pcktyp, ALLOCATE_REPLENISH) != 0 &&
        strcmp(pcktyp, ALLOCATE_INVENTORY_MOVE) != 0 &&
        strcmp(pcktyp, ALLOCATE_TOPOFF_REPLEN) != 0)
    {
        ret_status = sCopyOriginalOrder();

        if (ret_status != eOK)
        {
            trnFreePickList(PickRequestList);
            PickRequestList = NULL;
            CloseTraceFile();
            return (ret_status);
        }
    }

    /* If we're given the lodnum/stoloc then we are
       performing a specific allocation...otherwise, we're
       doing a warehouse wide allocation...  */
    
    if (!skip_invlkp_i && lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
        ret_status = sAllocateLoad(lodnum_i, cmbcod);
    else if (!skip_invlkp_i && stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
        ret_status = sAllocateFromStoloc(stoloc_i,
                         pcklvl_i,
                         pcktyp,
                         cmbcod,
                         splflg_i,
                         alloc_loc_flg_i,
                         wh_id_i);
    else if (!skip_invlkp_i)
    {        
        long PickArrayIndex;

        PickRequestArray = sBuildPickRequestArray(PickRequestList, 
                                                  &PickRequestArrayLength);

        if (PickRequestArray == NULL)
        {
            sLog(0, "After building the Pick Request Array - Array is emtpy");
            sLog(0, "Getting out.");
            CloseTraceFile();
            return (-1);
        }

        /* 
        * If the inventory status progression was passed in, loop around
        * it and pass the inventory status into the inventory selection
        * routine.  If it wasn't passed in, pass the invsts in directly.
        *
        * When looping around the inventory status progression sequence,
        * we want to check all of the buildings for the inventory status
        * before going to a secondary or tertiary inventory status.
        */

        if (strlen(PickRequestArray[0]->invsts_prg))
        {
            sprintf(buffer,
                " list inventory status progression details "
                "where invsts_prg = '%s' "
                "  and alcflg = 1 ",
                PickRequestArray[0]->invsts_prg);
        }
        else
        {
            /* We'll fool it into getting the inventory status */
            sprintf(buffer,
                " publish data "
                "where invsts = '%s' ",
                PickRequestArray[0]->invsts);
        }

        ret_status = srvInitiateCommand(buffer, &CurPtr);
        if (eOK != ret_status)
        {
            sLog(0, "Error getting inventory status(es)");
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CloseTraceFile();
            return(ret_status);
        }
        res = srvGetResults(CurPtr);


        for (row = sqlGetRow(res); 
             row && !IsPickRequestArrayEmpty(); 
             row = sqlGetNextRow(row))
        {
            memset(cur_invsts, 0, sizeof(cur_invsts));
            misTrimcpy(cur_invsts, 
                       sqlGetString(res, row, "invsts"), 
                        INVSTS_LEN);

            for (PickArrayIndex = 0; 
                 PickArrayIndex < PickRequestArrayLength; 
                 PickArrayIndex ++)
            {
                PickRequestList = PickRequestArray[PickArrayIndex];

                if (PickRequestList != NULL)
                {
                    char * psrc_bldg_id = NULL;
                    long src_bldg_len = 0L;

                    /*
                    * GetPickingBuildingSequence gets a set of buildings from
                    * the destination location.  If no records are defined for
                    * the given destination building, we assume that a building
                    * is only reachable to itself.  However, once at least one
                    * source building is defined for that source building, all 
                    * reachable buildings must be defined for that destination 
                    * building, including the destination building itself.
                    */
                    psrc_bldg_id = sBuildPickBuildingSequence(
                                    PickRequestList->reserved, 
                                    &src_bldg_len);

                    for (ii = 0; ii < src_bldg_len && PickRequestList; ii++)
                    {
                        misTrimcpy (src_bldg_id,
                                    (char *)&(psrc_bldg_id[ii * 
                                                           (BLDG_ID_LEN + 1) * 
                                                           sizeof(char)]),
                                    BLDG_ID_LEN);
                        
                        sLog(0, 
                            "Looking for inventory in building: "
                            "%s/%s of status %s",
                            wh_id_i,
                            src_bldg_id,
                            cur_invsts);

                        ret_status = sSelectPickInventory(pcklvl_i,
                                                          srcare_i,
                                                          pcktyp,
                                                          cmbcod,
                                                          splflg_i,
                                                          src_bldg_id,
                                                          cur_invsts,
                                                          alloc_loc_flg_i,
                                                          wh_id_i);

                        if (ret_status != eOK)
                        {
                            srvFreeMemory(SRVRET_STRUCT, CurPtr);
                            free(psrc_bldg_id);
                            sFreePickListArray();
                            CloseTraceFile();
                            if (PickFoundList == NULL && 
                                ret_status != eDB_DEADLOCK)
                                return(eINT_NO_INVENTORY);
                            else
                                return (ret_status);
                        }
                    }

                    if (psrc_bldg_id) free(psrc_bldg_id);
                }

                if (PickRequestArray[PickArrayIndex] != PickRequestList)
                    PickRequestArray[PickArrayIndex] = PickRequestList;
            }            
        }
        if (CurPtr)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;
            res = NULL;
        }

        /*
         * here we need to gather the PickRequestArray into
         * PickRequestList and free PickRequestArray
         */
        sLoadtoPickRequestList();
        
        if(PickRequestArray)
        {
            free(PickRequestArray);
            PickRequestArray = NULL;
        }
    }

    if (OriginalOrderList)
        sFreeOriginalOrderList();

    /* Don't need to process for all pick types, just the picking ones */
    if (pip_enabled &&
        (strcmp(pcktyp, ALLOCATE_PICK) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNSHIP) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNREPLENNSHIP) == 0))
    {
        /* Move from the pick request list to the pick found
           list if a srcloc can work. */
        ret_status = sProcessPIPicks(cmbcod);
    }

    if (PickFoundList)
    {
        if (Picks)
            *Picks = PickFoundList;

        if (strcmp(pcktyp, ALLOCATE_PICK) == 0 ||
            strcmp(pcktyp, ALLOCATE_PICKNREPLEN) == 0 ||
            strcmp(pcktyp, ALLOCATE_TOPOFF_REPLEN) == 0 ||
            strcmp(pcktyp, ALLOCATE_INVENTORY_MOVE) == 0 ||
            strcmp(pcktyp, ALLOCATE_REPLENISH) == 0 ||
            strcmp(pcktyp, ALLOCATE_PICKNSHIP) == 0 ||
            strcmp(pcktyp, ALLOCATE_PICKNREPLENNSHIP) == 0)
        {
            ret_status = sCommitPicks(pcktyp, pcksts_i, pcksts_uom_i, wrktyp, pipflg_i);
            if (Picks)
                *Picks = PickFoundList;
        }
        
        /* If cmbcod is passed in, this means that pick moves for the cmbcod
           have already been generated, then the pick moves will not be
           generated again. */
        if (ret_status == eOK && strlen(cmbcod) == 0)
        {
            ret_status = sWritePickMoves();
            if (ret_status != eOK)
            {
                /* Failure...make sure all memory is free  */
                if (Picks)
                    *Picks = NULL;

                sFreeSchBatRes(schbatRes);
                schbatRes = NULL;
                trnFreePickList(PickRequestList);
                trnFreePickList(PickFoundList);
                PickRequestList = NULL;
                PickFoundList = NULL;
                PckMovPathHead = NULL;
                CloseTraceFile();
                return (ret_status);
            }
        }

        sFreeSchBatRes(schbatRes);
        schbatRes = NULL;
    } 

 
    /* At this point...ret_status is either from the SelectPickInventory
       or related call, or it is from the commit/order prep operation...
       it it is not eOK, then we have to free everything...   */
    /* If we're in replenishment mode, and we don't have any picks to publish,
       then make sure we also error. */
    
    if ((ret_status != eOK) ||
        (!PickFoundList && 
             ((strcmp(pcktyp, ALLOCATE_REPLENISH) ==0 ) ||
               strcmp(pcktyp, ALLOCATE_INVENTORY_MOVE) == 0)))
    {
        /* Failure...make sure all memory is free  */
        if (Picks)
            *Picks = NULL;
        trnFreePickList(PickRequestList);
        trnFreePickList(PickFoundList);
        PickRequestList = NULL;
        PickFoundList = NULL;
        PckMovPathHead = NULL;
        CloseTraceFile();
        return (ret_status);
    }
    
    /* If this is a pick and replenish allocation, write the     */
    /* remaining picks to the replenishment work table for later */
    /* allocation.                                               */
    if (strcmp(pcktyp, ALLOCATE_PICKNREPLEN) == 0 ||
        strcmp(pcktyp, ALLOCATE_PICKNREPLENNSHIP) == 0)
    {
        /*
         * we need to set the destination area info for the 
         * replenish pick if this pick is not multi-stage
         */
        sAssignDstToReplenishPick(PickRequestList);
        
        ret_status = sWriteReplenWork(PickRequestList, pcksts_i, pcksts_uom_i, splflg_i, pipflg_i);
        if (ret_status != eOK)
        {
            /* Failure...make sure all memory is free  */
            if (Picks)
                *Picks = NULL;
            trnFreePickList(PickRequestList);
            trnFreePickList(PickFoundList);
            PickRequestList = NULL;
            PickFoundList = NULL;
            PckMovPathHead = NULL;
            CloseTraceFile();
            return (ret_status);
        }
    }
    
    /* Free Leftover requests... */
    trnFreePickList(PickRequestList);
    if (!Picks)
        trnFreePickList(PickFoundList);
    
    /* Reset the module specific pointers... */
    PickRequestList = NULL;
    PickFoundList = NULL;
    PckMovPathHead = NULL;
    PickRequestArray = NULL;
    PickRequestArrayLength = 0L;
    
    CloseTraceFile();
    
    return (eOK);
}

static SKIPPED_LOCATION_INFO* sAddLocationToSkipList(PICK_POLICY_DATA* PolPtr,
        char* stoloc,
        char* devcod,
        char* invsts,
        char* pcklvl,
        char* pcktyp,
        char* cmbcod,
        moca_bool_t splflg,
        char* juldte,
        AREA_INFO* area_info,
        long alloc_loc_flg,
        char* wh_id,
        char* invinloc_order_by,
        SKIPPED_LOCATION_INFO* skip_list)
{
    if(skip_list)
    {
        while(skip_list->next)
        {
            skip_list = skip_list->next;
        }

        if((skip_list->next = 
            (SKIPPED_LOCATION_INFO *) calloc(sizeof(SKIPPED_LOCATION_INFO), 1))
            == NULL)
        {
            return(NULL);   
        }
        skip_list->next->first = skip_list->first;
        skip_list = skip_list->next;
    }
    else
    {
        if((skip_list = 
            (SKIPPED_LOCATION_INFO *) calloc(sizeof(SKIPPED_LOCATION_INFO), 1))
            == NULL)
        {
            return(NULL);
        }

        skip_list->first = skip_list;
    }

    misTrc(T_FLOW, "Adding location (%s) to skipped locations list.", 
        stoloc);

    misTrimcpy(skip_list->stoloc,
        stoloc,
        sizeof(skip_list->stoloc));
    misTrimcpy(skip_list->devcod,
        devcod,
        sizeof(skip_list->devcod));
    misTrimcpy(skip_list->invsts,
        invsts,
        sizeof(skip_list->invsts));
    misTrimcpy(skip_list->pcklvl,
        pcklvl,
        sizeof(skip_list->pcklvl));
    misTrimcpy(skip_list->pcktyp,
        pcktyp,
        sizeof(skip_list->pcktyp));
    misTrimcpy(skip_list->juldte,
        juldte,
        sizeof(skip_list->juldte));
    misTrimcpy(skip_list->invinloc_order_by,
        invinloc_order_by,
        sizeof(skip_list->invinloc_order_by));
    misTrimcpy(skip_list->wh_id,
        wh_id,
        sizeof(skip_list->wh_id));
    skip_list->splflg = splflg;
    skip_list->alloc_loc_flg = alloc_loc_flg;

    /* AREA_INFO fields */
    /*skip_list->area_info.area_lodflg = area_info->area_lodflg;
    skip_list->area_info.area_subflg = area_info->area_subflg;
    skip_list->area_info.area_dtlflg = area_info->area_dtlflg;
    misTrimcpy(skip_list->area_info.area_loccod,
        area_info->area_loccod,
        LOCCOD_LEN);*/

    memcpy(&(skip_list->area_info), area_info, sizeof(AREA_INFO));
    memcpy(&(skip_list->policy), PolPtr, sizeof(PICK_POLICY_DATA));

    return(skip_list);
}

static long sProcessSkippedLocations(SKIPPED_LOCATION_INFO* skipped_location,
                moca_bool_t *isAlloLocFailed)
{
    long ret_status;
    /*
    SKIPPED_LOCATION_INFO* current_location;    
    */

    /* 
     * Loop through and process each location AFTER the currently 
     * selected one. To process the entire list, reset to the first 
     * element before calling this 
     */
    misTrc(T_FLOW, "Processing skipped locations.");

    while(skipped_location)
    {
        ret_status = 
        sProcessComplexAllocation(&(skipped_location->policy),
            skipped_location->stoloc,
            skipped_location->devcod,
            skipped_location->invsts,
            skipped_location->pcklvl,
            skipped_location->pcktyp,
            skipped_location->cmbcod,
            skipped_location->splflg,
            skipped_location->juldte,
            &(skipped_location->area_info),
            skipped_location->alloc_loc_flg,
            skipped_location->wh_id,
            skipped_location->invinloc_order_by,
            isAlloLocFailed);

        if(ret_status == eABSOLUTE_ALLOCATION_VIOLATION)
        {
            /* 
             * We will only re-try these one time for now.
             * We could attempt them an indefinite number
             * of times as below but that risks an infinite
             * loop so we won't introduce that complexity 
             * unless it is needed.
             */
            /*current_location = skipped_location;

            skipped_location = 
                sAddLocationToSkipList(&(skipped_location->policy),
            skipped_location->stoloc,
            skipped_location->devcod,
            skipped_location->invsts,
            skipped_location->pcklvl,
            skipped_location->pcktyp,
            skipped_location->cmbcod,
            skipped_location->splflg,
            skipped_location->juldte,
            &skipped_location->area_info,
            skipped_location->alloc_loc_flg,
            skipped_location->wh_id,
            skipped_location->invinloc_order_by,
            skipped_location);

            skipped_location = current_location;*/
        }
        else if (ret_status != eOK 
            && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return(ret_status);
        }
        else if (ret_status == eOK
            && !PickRequestList)
        {
            return(eOK);
        }

        skipped_location = skipped_location->next;
    }
    return(ret_status);
}

static void sFreeSkippedLocationList(SKIPPED_LOCATION_INFO* skipped_location)
{
    SKIPPED_LOCATION_INFO* next_location;

    /* Reset to the beginning of the list */
    if(skipped_location && skipped_location->first)
    {
        skipped_location = skipped_location->first;
    }

    /* Loop through and free all of the memory that was allocated for the list */
    while(skipped_location)
    {
        /* Assume the data at skipped_location->policy will be freed elsewhere */
        next_location = skipped_location->next;
        free(skipped_location);
        skipped_location = next_location;
    }
}

static char* sBuildCurrentlyAllocatedLocations()
{
    PCKWRK_DATA* current_found_pick;
    PCKWRK_DATA* first_found_pick;
    char* location_string = NULL;
    int i;
    long number_of_locations; 

    current_found_pick = PickFoundList;
    number_of_locations = 0;

    /* Rewind the list if its not already */
    while(current_found_pick && current_found_pick->prev)
    {
        current_found_pick = current_found_pick->prev;
    }

    /* Save the first pick in the list */
    first_found_pick = current_found_pick;

    /* Count the number of locations so we can allocate a string long enough */
    while(current_found_pick)
    {
        number_of_locations++;
        current_found_pick = current_found_pick->next;
    }

    /* Rewind to the first pick again */
    current_found_pick = first_found_pick;

    /* Attempt to allocate enough memory for a list of locations */
    location_string = (char*) calloc(number_of_locations + 1, STOLOC_LEN+5);

    misTrc(T_FLOW, "Building allocated location list.  Location Count (%ld)", 
        number_of_locations);

    /* Loop through and build our string */
    for(i = 0; 
        current_found_pick && location_string; 
        i++, current_found_pick = current_found_pick->next)
    {
        if(i == 0)
        {
            strcat(location_string, "''");
            strcat(location_string, current_found_pick->srcloc);
            strcat(location_string, "''");
        }
        else
        {
            strcat(location_string, ",''");
            strcat(location_string, current_found_pick->srcloc);
            strcat(location_string, "''");
        }        
    }

    return location_string;
}

