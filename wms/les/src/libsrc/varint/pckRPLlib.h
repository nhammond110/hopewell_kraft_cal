
typedef struct error_control
{
    char            rplsts[RPLSTS_LEN + 1];
    long            RetryCount;
    long            RetryDelayMinutes;
    char            CancelCommand[500];
    char            RetryStatus[RPLSTS_LEN + 1];
    struct error_control *next;
} ERROR_CONTROL;

typedef struct rpl_area_list
{
    char         wh_id[WH_ID_LEN + 1];

    /* The ultimate area that drives this replen - i.e. SSTG, etc.. */
    char         final_arecod[ARECOD_LEN+1];

    /* The area we attempt to pull product to - and what config */
    char         replen_arecod[ARECOD_LEN+1];
    char         replen_lodlvl[LODLVL_LEN+1];

    /* The area that "feeds" the replen area (if pulls to it fail, 2 hop) */
    char         feeder_arecod[ARECOD_LEN+1];
    char         feeder_lodlvl[LODLVL_LEN+1];

    /* The allocation configration. */
    char         cfgtyp[CFGTYP_LEN+1];
    char         cfgval[CFGVAL_LEN+1];

    struct rpl_area_list *next;
} RPL_AREA_LIST;

typedef struct rpl_policy
{
    char  wh_id[WH_ID_LEN + 1];

    long  isInstalled;
    long  min_rpl_time;
    long  max_time_to_wait;
    long  timer_length;

    char  StatusFile[500];
    char  AlertFile[500];

    ERROR_CONTROL  *ErrorControl;
    ERROR_CONTROL   DefaultError;

    RPL_AREA_LIST  *PiecePath;
    RPL_AREA_LIST  *CasePath; 

    long skip_held_rplwrk;

    long skip_held_pcksts;
} RPL_POLICY;


void pckrpl_CloseTrc();
void pckrpl_WriteTrc(char *format,...);
void pckrpl_OpenTrc(long commit_as_we_go, char *wh_id);
RPL_POLICY *pckrpl_GetConfig(char *wh_id);
void pckrpl_FreeConfig(void);
long pckrpl_CleanupCrossDock(char *rplref,
			     long alloc_qty,
			     long delete_flag,
			     char *ship_line_id,
			     char *wkonum,
			     char *wkolin,
			     char *wkorev,
                             char *wh_id
			     );
static char *sGetDateTime(void);
