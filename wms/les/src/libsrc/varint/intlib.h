/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  Purpose:   Structure defs for INTLIB
 *
 *#END************************************************************************/

#ifndef INTLIB_H
#define INTLIB_H

#include <applib.h>

RETURN_STRUCT *int_UpdateReceiveLine(char *srcloc_i,
                                     char *dstloc_i, 
                                     char *client_id_i,
                                     char *supnum_i,
                                     char *invnum_i,
                                     char *lodnum_i,
                                     char *subnum_i,
                                     char *dtlnum_i,
                                     char *updmod_i,
                                     char *wh_id_i,
                                     moca_bool_t *ovrrcptconf_i);

#endif

