/*#START***********************************************************************
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <vargendef.h>

#include "rflib.h"


LIBEXPORT
RETURN_STRUCT *rfGetRFWorkPickupInformation (long *reqnum_i)
{
    RETURN_STRUCT *returnData;

    mocaDataRes   *res;
    mocaDataRow   *row;
    long reqnum;
    char oprcod[OPRCOD_LEN + 1];
    char refloc[REFLOC_LEN + 1];
    char wrkref [WRKREF_LEN + 1];
    char lblbat [LBLBAT_LEN + 1];
    char lodnum [LODNUM_LEN + 1];
    char lodlvl [LODLVL_LEN + 1];
    char srcloc [STOLOC_LEN + 1];
    char dstloc [STOLOC_LEN + 1];
    char batnum [BATNUM_LEN + 1];
    char wh_id  [WH_ID_LEN + 1];
    char list_id[LIST_ID_LEN + 1];
    char tmp_where [100];
    long cmpflg = BOOLEAN_FALSE;
    
    char          buffer [5000];
    long          status;

    /* Initialize internal structures */

    memset(oprcod, 0, sizeof(oprcod));
    memset(refloc, 0, sizeof(refloc));
    memset(wrkref, 0, sizeof(wrkref));
    memset(lblbat, 0, sizeof(lblbat));
    memset(lodlvl, 0, sizeof(lodlvl));
    memset(lodnum, 0, sizeof(lodnum));
    memset(srcloc, 0, sizeof(srcloc));
    memset(dstloc, 0, sizeof(dstloc));
    memset(batnum, 0, sizeof(batnum));
    memset(wh_id, 0, sizeof(wh_id));
    memset(list_id, 0, sizeof(list_id));
    memset(tmp_where, 0, sizeof(tmp_where));

    /* Verify required arguments are present */

    if (!reqnum_i /* || !*reqnum_i */)  /* Zero values are allowed */
    {
        return (APPMissingArg("reqnum"));
    }
    reqnum = *reqnum_i;
   
    /* Verify if any optional arguments are present */

    /* SQL Query to get work request information */

    sprintf (buffer,
             "select wrkref, "
             "       lblbat, "
             "       lodnum, "
             "       lodlvl, "
             "       srcloc, "
             "       dstloc, "
             "       oprcod, "
             "       refloc, "
             "       batnum, "
             "       list_id, "
	     "       wh_id  "
             "  from wrkque "
             " where reqnum = '%d' ", 
             reqnum);

    status = sqlExecStr (buffer, &res);
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
        sqlFreeResults (res);
        return (srvResults(status, NULL));
    }
    if (status == eOK)
    {
        row = sqlGetRow (res);
        misTrimcpy(wrkref, sqlGetString(res, row, "wrkref"), WRKREF_LEN);
        misTrimcpy(lblbat, sqlGetString(res, row, "lblbat"), LBLBAT_LEN);
        misTrimcpy(lodnum, sqlGetString(res, row, "lodnum"), LODNUM_LEN);
        misTrimcpy(lodlvl, sqlGetString(res, row, "lodlvl"), LODLVL_LEN);
        misTrimcpy(srcloc, sqlGetString(res, row, "srcloc"), STOLOC_LEN);
        misTrimcpy(oprcod, sqlGetString(res, row, "oprcod"), OPRCOD_LEN);
        misTrimcpy(refloc, sqlGetString(res, row, "refloc"), REFLOC_LEN);
        misTrimcpy(batnum, sqlGetString(res, row, "batnum"), BATNUM_LEN);
        misTrimcpy(dstloc, sqlGetString(res, row, "dstloc"), DSTLOC_LEN);
        misTrimcpy(list_id, sqlGetString(res, row, "list_id"), LIST_ID_LEN);
        misTrimcpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
    }
    sqlFreeResults (res);

    /* If work request does not exist, then set work request completed flag */

    if ((status == eDB_NO_ROWS_AFFECTED) && (reqnum > 0))
    {
        cmpflg = TRUE;
    }
    
    /*
     * If the work request is assigned to a pick list, 
     * if all the picks in the list are completed, 
     * we think that this work request is completed. 
     */
    if (misTrimLen(list_id, LIST_ID_LEN) > 0)
    {
        sprintf(buffer,
                " select * "
                "   from pckwrk "
                "  where pcksts = '%s' "
                "    and appqty < pckqty "
                "    and list_id = '%s' ",
                PCKSTS_RELEASED,
                list_id);
              
        status = sqlExecStr(buffer, NULL);
        if (eOK == status)
            cmpflg = BOOLEAN_FALSE;
        else if (eDB_NO_ROWS_AFFECTED == status)
            cmpflg = BOOLEAN_TRUE;
        else
            return (srvResults(status, NULL));
    }
    
    /* If work request label batch value is defined */
    /* (i.e. we're batch picking),                  */
    /* then get work reference number and source location */
    /* from pick work table  */

    else if (misTrimLen(lblbat, LBLBAT_LEN) > 0)
    {

        /* SQL Query to get work reference number and source location */

        sprintf (buffer,
            "select pw.wrkref, "
            "       pw.srcloc "
            "  from pckwrk pw, "
            "       locmst lm "
            " where pw.srcloc = lm.stoloc and "
            "       pw.appqty < pw.pckqty and "
            "       pw.lblbat = '%s' and "
	    "       pw.wh_id  = lm.wh_id "
            "order by lm.trvseq ",
            lblbat);
        status = sqlExecStr (buffer, &res);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults (res);
            return (srvResults(status, NULL));
        }
        if (status == eOK)
        {
            row = sqlGetRow (res);
            strcpy (wrkref, sqlGetString (res, row, "wrkref"));
            strcpy (srcloc, sqlGetString (res, row, "srcloc"));
        }
        sqlFreeResults (res);

        /* If matching pick work entry does not exist */
        /* (i.e. done picking), then                  */
        /* set work request completed flag            */ 

        if (status == eDB_NO_ROWS_AFFECTED)
        {
            cmpflg = TRUE;
        }
    }

    /* Else if work request work reference number is defined */
    /* (i.e. directed pick),                                 */
    /* then get work reference number and source location    */
    /* from pick work table                                  */

    else if (misTrimLen(wrkref, WRKREF_LEN) > 0)
    {

        /* SQL Query to get work reference number and source location */
        /* via label batch value (for handling unmarked batch picks)  */

        sprintf (buffer,
            "select pw.wrkref, "
            "       pw.srcloc "
            "  from pckwrk pw, "
            "       locmst lm "
            " where pw.lblbat is not null and "
            "       pw.srcloc = lm.stoloc and "
            "       pw.appqty < pw.pckqty and "
            "       pw.wh_id  = lm.wh_id  and "
            "       pw.lblbat = (select lblbat from pckwrk "
            "                     where wrkref = '%s') "
            "order by lm.trvseq ",
            wrkref);

        status = sqlExecStr (buffer, &res);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults (res);
            return (srvResults(status, NULL));
        }
        if (status == eOK)
        {
            row = sqlGetRow (res);
            strcpy (wrkref, sqlGetString (res, row, "wrkref"));
            strcpy (srcloc, sqlGetString (res, row, "srcloc"));
        }
        sqlFreeResults (res);

        /* If work request is not an unmarked batch pick, */
        /* then get source location */
        /* from pick work table     */          

        if (status == eDB_NO_ROWS_AFFECTED)
        {

            /* SQL Query to get source location via work reference value */

            sprintf (buffer,
                "select srcloc "
                "  from pckwrk "
                " where appqty < pckqty "
                "   and wrkref = '%s' ",
                wrkref);

            status = sqlExecStr (buffer, &res);
            if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
            {
                sqlFreeResults (res);
                return (srvResults(status, NULL));
            }
            if (status == eOK)
            {
                row = sqlGetRow (res);
                strcpy (srcloc, sqlGetString (res, row, "srcloc"));
            }
            sqlFreeResults (res);
        }

        /* If matching pick work entry does not exist (i.e. done picking), */
        /* and this is not some other type of work (such as move trlr)     */
        /* that is using the wrkref field then                             */
        /* set work request completed flag                                 */ 

        if ((status == eDB_NO_ROWS_AFFECTED) && 
            (misTrimLen(refloc, REFLOC_LEN) == 0))
        {
            cmpflg = TRUE;
        }
    }

    /* Else if work request load number is defined, */
    /* then verify load is still at                 */
    /* expected location                            */                             
    else if (strlen (lodnum) > 0)
    {

        /* SQL Query to verify load is still at expected location */

        if (strcmp (lodlvl, LODLVL_DETAIL) == 0)
        {
            sprintf (buffer,
                "select lod.lodnum "
                "  from invlod lod, "
                "       invsub sub, "
                "       invdtl dtl "
                " where lod.lodnum = sub.lodnum "
                "   and sub.subnum = dtl.subnum "
                "   and lod.stoloc = '%s'       "
                "   and dtl.dtlnum = '%s' "
                "   and lod.wh_id = '%s'",
                srcloc, 
                lodnum,
                wh_id);
        }
        else if (strcmp (lodlvl, LODLVL_SUB) == 0)
        {
            sprintf(buffer,
                    "select lod.lodnum "
                    "  from invlod lod, "
                    "       invsub sub "
                    " where lod.lodnum = sub.lodnum "
                    "   and lod.stoloc = '%s'       "
                    "   and sub.subnum = '%s' "
                    "   and lod.wh_id = '%s' ",
                    srcloc, 
                    lodnum,
                    wh_id);
        }
        else
        {
            sprintf (buffer,
                "select lodnum "
                "  from invlod "
                " where stoloc = '%s' "
                "   and lodnum = '%s' "
                "   and  wh_id = '%s' ",
                srcloc, 
                lodnum,
                wh_id);
        }
        status = sqlExecStr (buffer, &res);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults (res);
            return (srvResults(status, NULL));
        }
        sqlFreeResults (res);

        /* If load is not at expected location, */
        /* then set work request completed flag */

        if (status == eDB_NO_ROWS_AFFECTED)
        {
            cmpflg = TRUE;
        }
    }

    /*
     * A change has been made to the way we're creating
     * work when a load is dropped off in a P&D location.
     * It necessitated not writing out the load number
     * on the work request so that users won't have to
     * scan through pallets until the right one is found.
     * This meant that the above check to set the work
     * request completed flag doesn't get ran at all
     * because of the absence of a load number on the
     * request and thus the work doesn't get completed.  
     * To get around this, we will be making a BIG 
     * assumption here.  The basic gist is that we will
     * be matching up the number of work requests with
     * the number of loads in the P&D location.  For
     * example, if we dropped off four loads then four work
     * requests would get created, keeping in mind that
     * these four would be virtually identical.  As soon as 
     * we acknowledged one of the works then one of the 
     * work requests would be set to an ACK status. 
     * After we move the load out of the P&D location,
     * we'll make a check on how many loads are left
     * behind in the location.  If there are only three left
     * and we still have four requests then we'll set
     * the completed flag to TRUE on the acknowledged work.
     */

    else
    {
		/*
		 * If wrkref is null and lodnum is null and oprcod is PCK or TRN,
		 * check if any inventory exists in the location, if no, set
		 * completed flag to TRUE.
		 */
		if (!strcmp(oprcod, OPRCOD_PICK) ||
			  !strcmp(oprcod, OPRCOD_TRANSFER))
		{
			sprintf (buffer,
				     " select 1 "
					 "   from invlod "
			         "  where stoloc = '%s' "
					 "    and wh_id = '%s' ",
					 srcloc,
					 wh_id);

			status = sqlExecStr (buffer, NULL);

			if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
			{
				return (srvResults(status, NULL));
			}

			if(status == eDB_NO_ROWS_AFFECTED)
			{
				cmpflg = TRUE;
			}
		}
        /* WMD-55930
         * If oprcod is any of our predefined count operations
         * codes we do not want to allow the work to be 
         * completed if there is a mismatch between the 
         * number of loads and the number of work requests.
         */
        if ((strcmp(oprcod, OPRCOD_COUNT_BACK) != 0) 
                 && (strcmp(oprcod, OPRCOD_COUNT) != 0)
                 && (strcmp(oprcod, OPRCOD_AUDIT_COUNT) != 0)
                 && (strcmp(oprcod, OPRCOD_DIR_AUDIT_COUNT) != 0)
                 && (strcmp(oprcod, OPRCOD_PHYSICAL) != 0)
                 && (strcmp(oprcod, OPRCOD_COUNT_DETAIL) != 0))              
        {
            sprintf (buffer,
                     "select 1 "
                     "  from aremst a, locmst l "
                     " where a.arecod = l.arecod "
                     "   and a.pdflg  = '%d' "
                     "   and l.stoloc = '%s' "
                     "   and l.wh_id  = a.wh_id "
                     "   and l.wh_id = '%s'",
                     BOOLEAN_TRUE,
                     srcloc,
                     wh_id); 

        status = sqlExecStr (buffer, NULL);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            return (srvResults(status, NULL));
        }
        
        /* PR60435 if not a P&D location, check whether orpcod */
        /* is for re-warehouse consolidation move */
        if( status == eDB_NO_ROWS_AFFECTED )
        {
        	sprintf(buffer,
                	"check var consolidation move"
                	" where oprcod = '%s' "
                	"   and wh_id  = '%s' ",
                	oprcod,
                	wh_id);
    
        	status = srvInitiateCommand(buffer, NULL);
        }
        
		if (status == eOK)
		{
	            /*
		     * Find out how many loads are left in the location
		     * as well as how many work requests exist for
		     * the given P&D location.
		     */
	
	            sprintf (buffer,
	                     " [select count(lodnum) lodcnt"
	                     "    from nxtloc_view "
	                     "   where stoloc = '%s'  "
			     "     and wh_id  = '%s'] "
			     " | "
			     " [select count(reqnum) reqcnt "
			     "    from wrkque "
			     "   where srcloc = '%s'  "
			     "     and wh_id  = '%s'] "
			     " | "
			     " publish data "
			     "   where lodcnt = @lodcnt "
			     "     and reqcnt = @reqcnt ",
	                     srcloc,
			     wh_id,
			     srcloc,
			     wh_id);
	
		    returnData = NULL;
	            status = srvInitiateCommand (buffer, &returnData);
	            if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	            {
			if (returnData) srvFreeMemory (SRVRET_STRUCT, returnData);
	            	return (srvResults(status, NULL));
	            }
		    else if (status == eOK)
		    {
			/*
			 * Set the work completed flag to TRUE
			 * if the number of loads is less than
			 * the number of work requests.
			 */
	
			res = srvGetResults (returnData);
			row = sqlGetRow(res);
			if (sqlGetLong(res, row, "lodcnt") < 
			    sqlGetLong(res, row, "reqcnt"))
	            	    cmpflg = TRUE;
		    }
		    if (returnData) srvFreeMemory (SRVRET_STRUCT, returnData);
            }
		}
    }

    /*
     * If this is a work request to load a stop, then let's make sure that
     * stop isn't completely loaded already (may have been direct loaded
     * or something without using the work request.
     */

    if (!strcmp(oprcod, OPRCOD_LOAD))
    {
        sprintf(buffer,
                "check stop loaded"
                " where stop_id = '%s' ",
                refloc);
    
        status = srvInitiateCommand(buffer, NULL);
        if (status == eOK)
            cmpflg = BOOLEAN_TRUE;

        /* 
         * Make sure there's actually inventory in the staging locations
         * for this stop, in order to load the trailer.  If there isn't,
         * mark the work as complete.
         */

        sprintf(buffer,
                "check inventory exists for trailer load "
                "where stop_id = '%s' "
                "  and wh_id = '%s'",
                refloc,
                wh_id);

        status = srvInitiateCommand(buffer, NULL);
        if ((eOK != status) && 
            (eDB_NO_ROWS_AFFECTED != status) &&
            (eSRV_NO_ROWS_AFFECTED != status))
        {
            return (srvResults(status, NULL));
        }
        if (eOK != status)
            cmpflg = BOOLEAN_TRUE;
    }

    /* If this is a trailer move, let's make sure it hasn't already */
    /* been moved to the destination location. */

    if (!strcmp(oprcod, OPRCOD_TRAILER_MOVE))
    {

        if (wrkref && misTrimLen(wrkref, WRKREF_LEN))
            sprintf(tmp_where, " and trlr.carcod = '%s' ", wrkref);
   
        /*If we get the oprcod, we got the refloc*/
        /*We need to verify in current warehouse*/
        sprintf (buffer,
                 "select yard_loc "
                 "  from wrkque, trlr "
                 " where trlr_num = '%s' "
                 "   and trlr_stat <> '%s' "
                 "   and wrkque.wh_id = '%s' "
                 "   and wrkque.refloc = trlr.trlr_num "
                 "   %s ",
                 refloc,
                 TRLSTS_DISPATCHED,
                 wh_id,
                 tmp_where);
        
        status = sqlExecStr (buffer, &res);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults (res);
            return (srvResults(status, NULL));
        }

        if (eOK == status)
        {
            row = sqlGetRow (res);

            if (misCiStrncmp(sqlGetString(res, row, "yard_loc"), 
                             dstloc, YARD_LOC_LEN) == 0)
            {
                 cmpflg = BOOLEAN_TRUE;
            }
        }
        else
        {
            cmpflg = BOOLEAN_TRUE;
        }

        sqlFreeResults (res);
    }

    /*
     * If this is a work request to inbound service, then verify
     * there is any service on this truck.
     * If there is none, complete work.
     */

    if (!strcmp(oprcod, OPRCOD_INBOUND_SERVICE))
    {
            sprintf(buffer,
                    "list incomplete services for receive truck "
                    "where trknum = '%s' "
                    "  and wh_id = '%s' ",
                    refloc,
                    wh_id);
        
            status = srvInitiateCommand(buffer, NULL);
            if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED) &&
                (status != eSRV_NO_ROWS_AFFECTED))
            {
                sqlFreeResults (res);
                return (srvResults(status, NULL));
            }
            else if (eOK != status)
            {
                cmpflg = BOOLEAN_TRUE;
            }        
                
    }


    /* Setup return information */

    returnData = srvResultsInit (eOK,
                        "wrkref",       COMTYP_CHAR,    WRKREF_LEN,
                        "lblbat",       COMTYP_CHAR,    LBLBAT_LEN,
                        "lodnum",       COMTYP_CHAR,    LODNUM_LEN,
                        "lodlvl",       COMTYP_CHAR,    LODLVL_LEN,
                        "srcloc",       COMTYP_CHAR,    STOLOC_LEN,
                        "cmpflg",       COMTYP_INT,     sizeof (long),
                        NULL);

    status = srvResultsAdd(returnData,
                           wrkref,
                           lblbat,
                           lodnum,
                           lodlvl,
                           srcloc,
                           cmpflg,
                           NULL);


    return (returnData);
}
