static char *rcsid = "$Id: intRemoveStoragePolicy.c 159174 2008-06-09 15:37:58Z mzais $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "trnlib.h"

/*
 * HISTORY
 * NML 06/4/2009 - Fix standard product bug that prevented remvoing storage policies
 *
 */

LIBEXPORT 
RETURN_STRUCT *varRemoveStoragePolicy(char *bldg_id_i, 
                                      char *client_id_i,
                                      char *prtnum_i,
                                      char *prt_client_id_i,
                                      char *prtfam_i, 
                                      char *supnum_i,
                                      char *asset_typ_i,
                                      char *suffix_i,
                                      long *srtseq_i,
                                      char *wh_id_i)
{
    char            buffer[2000];
    char            wh_id[WH_ID_LEN + 1];
    char            polcod[POLCOD_LEN + 1];
    char            polvar[POLVAR_LEN + 1];
    char            polval[POLVAL_LEN + 1];
    long            srtseq = 0;
    int             i = 0;
    long            ret_status = eOK;

    memset(buffer, 0, sizeof(buffer));
    memset(wh_id, 0, sizeof(wh_id));
    memset(polcod, 0, sizeof(polcod));
    memset(polvar, 0, sizeof(polvar));
    memset(polval, 0, sizeof(polval));

    if (wh_id_i != NULL && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    else
        return APPMissingArg ("wh_id");

    if (srtseq_i)
    srtseq = *srtseq_i;
    else
        return APPInvalidArg ("", "srtseq");

    ret_status = trnGetStoragePolicyColumns(
        bldg_id_i,
        suffix_i,
        prtnum_i,
        prt_client_id_i,
        prtfam_i,
        supnum_i,
        client_id_i,
        NULL, /* invsts - used for location policies by some GUI apps */
        asset_typ_i,
        BOOLEAN_TRUE,
        polcod,
        polvar,
        polval);
    if (ret_status != eOK)
    {
        return srvResults(ret_status, NULL);
    }

    /* Remove the policy */
    sprintf(buffer,
        " remove policy"
        "  where polcod = '%s'"
        "    and polvar = '%s'"
        "    and polval = '%s'"
        "    and srtseq = %ld"
        "    and wh_id  = '%s'",
        polcod,
        polvar,
        polval,
        srtseq, 
        wh_id);

    /* After the policy above is deleted, a gap in the srtseq numbers could
     * result for these area preference policies. Here we renumber the sort
     * sequences of policies after the deleted policy to remove the gap. If this
     * update results in an eDB_NO_ROWS_AFFECTED status, ignore it as there were
     * no rows to update. */
    ret_status = srvInitiateInline(buffer, NULL);
    if (ret_status == eOK)
    {
        sprintf(buffer,
            " update poldat"
            "    set srtseq = srtseq - 1"
            "  where polcod = '%s'"  //removed extra %. This was causing parse errors
            "    and polvar = '%s'"
            "    and polval = '%s'"
            "    and wh_id_tmpl = '%s'"
            "    and srtseq > %ld",
            polcod,
            polvar,
            polval,
            wh_id,
            srtseq);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            ret_status = eOK;
        }
    }
    return (srvResults(ret_status, NULL));
}
