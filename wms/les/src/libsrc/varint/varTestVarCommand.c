static char RCS_Id[] = "$Id: varTestVarCommand.c 51219 2000-02-03 20:14:25Z mlange $";
/*#START***********************************************************************
 *
 *  $URL: https://athena.redprairie.com/svn/prod/env/tags/2008.1.2/src/libsrc/varint/varTestVarCommand.c $
 *  $Revision: 51219 $
 *  $Author: mlange $
 *
 *  Description: Simple component to test a VAR-level command.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <string.h>

#include <mocaerr.h>
#include <srvlib.h>

LIBEXPORT 
RETURN_STRUCT *varTestVarCommand(void)
{
    return srvResults(eOK, 
		      "Test VARint", COMTYP_CHAR, 10, "It works!",
		      NULL);
}
