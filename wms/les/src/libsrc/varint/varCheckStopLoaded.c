static const char *rcsid = "$Id: intCheckStopLoaded.c 140759 2007-10-25 19:12:52Z pflanzer $";
/*#START***********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "intlib.h"

/*
 * HISTORY
 * JJS 03/03/2009 - Comment out the line of code that keeps LTL trailers
 *                  from having their stops completed automatically.
 *
 */

LIBEXPORT 
RETURN_STRUCT *varCheckStopLoaded(char *stop_id_i)
{
    char stop_id[STOP_ID_LEN + 1];
    char wh_id[WH_ID_LEN + 1];
    char car_move_id[CAR_MOVE_ID_LEN + 1];
    char bldg_id[BLDG_ID_LEN + 1];
    long load_by_bldg;
    long ret_status;
    char buffer[2500];

    RETURN_STRUCT *CmdRes;
    mocaDataRes *res;
    mocaDataRow *row;
    
    CmdRes = NULL;
    res    = NULL;
    row    = NULL;

    memset(stop_id, 0, sizeof(stop_id));
    memset(wh_id, 0, sizeof(wh_id));
    memset(car_move_id, 0, sizeof(car_move_id));
    memset(bldg_id, 0, sizeof(bldg_id));

    load_by_bldg = 0;

    if (!stop_id_i || misTrimLen(stop_id_i, STOP_ID_LEN) == 0)
        return (APPMissingArg("stop_id"));

    misTrimcpy(stop_id, stop_id_i, STOP_ID_LEN);

    /*
     * First, we'll look at the carrier type of the trailer that this stop
     * id is associated with.  If the carrier type is defined as LTL then
     * we will not be able to determine the stop is loaded, because at any
     * point in time inventory can be loaded onto the trailer until the 
     * trailer is full. If the carrier is defined with multiple carrier types,
     * possible because carrier type is defined at the carrier detail level,
     * then we can't know if this trailer is being used as an LTL trailer, so
     * we can't determine if the stop is full, so we will also return false in
     * that scenario.
     */

    misTrc(T_FLOW, "Check if the carrier type is defined as LTL ...");

    sprintf(buffer,
            " select 'x' "
            "   from cardtl, "
            "        trlr, "
            "        car_move, "
            "        stop "
            "  where cardtl.cartyp    = '%s' "
            "    and cardtl.carcod    = trlr.carcod "
            "    and trlr.trlr_id     = car_move.trlr_id "
            "    and car_move.car_move_id = stop.car_move_id "
            "    and stop.stop_id     = '%s' ",
            CARTYP_LTL,
            stop_id);

    ret_status = sqlExecStr(buffer, NULL);

    if (ret_status == eOK)
    {
       /*****************JJS - comment this out - it keeps LTL trailers
        *****************from having their stops completed automatically,
        *****************and whatever the intent of that may have been,
        *****************we have absolutely no interest in it.  If the
        *****************freaking stop is completely loaded, complete the
        *****************stupid thing.

        return(srvResults(eINT_STOP_NOT_FULLY_LOADED, NULL));

       ******************/
    }
    else
    {
        /*
         * If we didn't find any matching rows, that's OK, it just means
         * our carrier type is not LTL, so we're good to keep going to 
         * check if the stop is loaded.
         */

        if (ret_status != eDB_NO_ROWS_AFFECTED &&
            ret_status != eSRV_NO_ROWS_AFFECTED)
        {
            return (srvResults(ret_status, NULL));
        }
    }


    /* Get the current bldg_id, car_move_id and wh_id */
    
    misTrc(T_FLOW, "Get the current bldg_id, car_move_id and wh_id");
    sprintf(buffer,
        " select car_move.car_move_id, "
        "        aremst.bldg_id, "
        "        locmst.wh_id "
        "   from aremst, "
        "        locmst, "
        "        trlr, "
        "        car_move, "
        "        stop "
        "  where aremst.arecod        = locmst.arecod "
        "    and aremst.wh_id         = locmst.wh_id "
        "    and locmst.stoloc        = trlr.yard_loc "
        "    and locmst.wh_id         = trlr.yard_loc_wh_id "
        "    and trlr.trlr_id         = car_move.trlr_id "
        "    and car_move.car_move_id = stop.car_move_id "
        "    and stop.stop_id         = '%s' ",
        stop_id);
    
    ret_status = sqlExecStr(buffer, &res);
    
    if (eOK == ret_status)
    {
        row = sqlGetRow (res);
        misTrimcpy(car_move_id, sqlGetString(res, row, "car_move_id"),
            CAR_MOVE_ID_LEN);
        misTrimcpy(bldg_id, sqlGetString(res, row, "bldg_id"),BLDG_ID_LEN);
        misTrimcpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
        sqlFreeResults(res);
    }
    else
    {
        sqlFreeResults(res);
        return(srvResults(ret_status, NULL));
    }
  
    /* Get the policy value load_stop_by_building */    

    misTrc(T_FLOW, "Get policy value of load_stop_by_building");
    sprintf(buffer,
        " list policies "
        " where polcod = '%s' "
        "   and polvar = '%s' "
        "   and polval = '%s' "
        "   and wh_id  = '%s' ",
        POLCOD_SHIPPING,
        POLVAR_MISC,
        POLVAL_SHIPPING_LOAD_STOP_BY_BUILDING,
        wh_id);  
    
    ret_status = srvInitiateCommand(buffer, &CmdRes);

    if (eOK == ret_status)
    {
        res = srvGetResults (CmdRes);
        row = sqlGetRow(res);
        load_by_bldg = sqlGetLong(res, row, "rtnum1");
    }

    /* If ret_status is not eOK, then assume that the policy 
     * is turned off i.e. load_by_bldg = 0
     */
    
    srvFreeMemory (SRVRET_STRUCT, CmdRes);
    CmdRes = NULL;  

    /* Check that all the shipments on this stop are loaded by making sure 
     * that all inventory for the stop is in the trailer location.
     * If load_by_bldg = 1, then check if all inventory in the 
     * current building is in the trailer location.
     */

    if(load_by_bldg == 1)
    {
        /* Check if all inventory for the stop has been loaded from the 
         * current building. If it returns empty, then all inventory has been
         * loaded from the building. 
         */

        misTrc(T_FLOW, "Check if all inventory for stop has been loaded from "
            "current bldg ");
        sprintf(buffer,
            "select 'x' "
            "  from aremst, "
            "       locmst, " 
            "       shipment, "
            "       shipment_line, "
            "       invlod,  "
            "       invsub,  "
            "       invdtl, "
            "       stop "
            " where stop.stop_id               = '%s' "
            "   and aremst.bldg_id             = '%s' "
            "   and invlod.lodnum              = invsub.lodnum "
            "   and invsub.subnum              = invdtl.subnum "
            "   and shipment_line.ship_line_id = invdtl.ship_line_id "
            "   and shipment.ship_id           = shipment_line.ship_id "
            "   and stop.stop_id               = shipment.stop_id "
            "   and locmst.stoloc              = invlod.stoloc "
            "   and locmst.wh_id               = invlod.wh_id  "
            "   and aremst.arecod              = locmst.arecod "
            "   and aremst.wh_id               = locmst.wh_id "
            "   and aremst.shpflg              = '%ld' ",
            stop_id,
            bldg_id,
            BOOLEAN_FALSE);

        ret_status = sqlExecStr (buffer, NULL);    

        if (eOK == ret_status)
        {
            return(srvResults(eINT_STOP_NOT_FULLY_LOADED, NULL));
        }
        else if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return (srvResults(ret_status, NULL));
        }
        
        /* Check if there are outstanding picks with a dstloc of the current
         * bldg_id 
         */

        misTrc(T_FLOW, "Check for outstanding picks with dstloc of "
               " current bldg ");
        sprintf(buffer, 
            " select 'x' "
            "   from aremst, "
            "        pckwrk, "
            "        shipment "
            "  where pckwrk.ship_id   = shipment.ship_id "
            "    and pckwrk.srcare    = aremst.arecod "
            "    and pckwrk.wh_id     = aremst.wh_id "
            "    and aremst.wh_id     = '%s' "
            "    and aremst.bldg_id   = '%s' "
            "    and shipment.stop_id = '%s' "
            "    and pckwrk.appqty    < pckwrk.pckqty "
            "    and pckwrk.dstloc is not null ",
            wh_id,
            bldg_id,
            stop_id);
        
        ret_status = sqlExecStr(buffer, NULL);

        if (ret_status == eOK)
        {
            return (srvResults(eINT_ADD_INV_EXISTS_NOT_READY_TO_LOAD, NULL));
        }    
        else if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return (srvResults(ret_status, NULL));
        }

        /* Check for cross docks with respect to the bldg_id */

        misTrc(T_FLOW, "Check for cross docks for the current building");
        sprintf(buffer, 
            " select 'x' "
            "   from aremst, "
            "        pckwrk, "
            "        shipment, " 
            "        cross_dock_view "
            "  where aremst.arecod           = pckwrk.srcare "
            "    and aremst.wh_id            = pckwrk.wh_id "
            "    and pckwrk.ship_id          = shipment.ship_id "
            "    and cross_dock_view.ship_id = shipment.ship_id "
            "    and aremst.bldg_id          = '%s' "
            "    and aremst.wh_id            = '%s' " 
            "    and shipment.stop_id        = '%s' ", 
            bldg_id,
            wh_id,
            stop_id);
        
        ret_status = sqlExecStr(buffer, NULL);

        if (ret_status == eOK)
        {
            return (srvResults(eINT_ADD_INV_EXISTS_NOT_READY_TO_LOAD, NULL));
        }
        else if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return (srvResults(ret_status, NULL));
        }

        /* Make sure all the shipments are staged */

        misTrc(T_FLOW, "Check if all shipments are staged");
        sprintf(buffer,
            " select 'x' "
            "   from aremst, "
            "        locmst, "
            "        invlod, "
            "        invsub, "
            "        pckwrk, "
            "        shipment "
            "  where aremst.arecod  = locmst.arecod "
            "    and aremst.wh_id   = locmst.wh_id " 
            "    and locmst.stoloc  = invlod.stoloc " 
            "    and locmst.wh_id   = invlod.wh_id "
            "    and invlod.lodnum  = invsub.lodnum " 
            "    and invsub.wrkref  = pckwrk.wrkref "
            "    and pckwrk.ship_id = shipment.ship_id "
            "    and aremst.bldg_id = '%s' "
            "    and stop_id        = '%s' "
            "    and stgdte is null ",
            bldg_id,
            stop_id);
        
        ret_status = sqlExecStr(buffer, NULL);

        if (ret_status == eOK)
        {
            /* At least one shipment isn't staged yet */
            return (srvResults(eINT_SHIP_ID_NOT_YET_STAGED, NULL));
        }
        else if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return (srvResults(ret_status, NULL));
        }
    }

    /* Check if all inventory has been loaded irrespective of the bldg_id */
    
    misTrc(T_FLOW, "Check if all inventory has been loaded irrespective "
        "of bldg ");
    sprintf(buffer,
        "select 'x' "
        "  from aremst, "
        "       locmst, " 
        "       shipment, "
        "       shipment_line, "
        "       invlod,  "
        "       invsub,  "
        "       invdtl, "
        "       stop "
        " where stop.stop_id               = '%s' "
        "   and invlod.lodnum              = invsub.lodnum "
        "   and invsub.subnum              = invdtl.subnum "
        "   and shipment_line.ship_line_id = invdtl.ship_line_id "
        "   and shipment.ship_id           = shipment_line.ship_id "
        "   and stop.stop_id               = shipment.stop_id "
        "   and locmst.stoloc              = invlod.stoloc "
        "   and locmst.wh_id               = invlod.wh_id  "
        "   and aremst.arecod              = locmst.arecod "
        "   and aremst.wh_id               = locmst.wh_id "
        "   and aremst.shpflg              = '%ld' ",
        stop_id,
        BOOLEAN_FALSE);
    
    ret_status = sqlExecStr (buffer, NULL);  
    
    if (ret_status == eOK)
    {
        if(load_by_bldg == 0)
            return(srvResults(eINT_STOP_NOT_FULLY_LOADED, NULL));
        else
            return(srvResults(eINT_ADD_INV_EXISTS_NOT_READY_TO_LOAD, NULL));
    }        
    else
    {
        if (ret_status != eDB_NO_ROWS_AFFECTED)
            return (srvResults(ret_status, NULL));
    }
    
    /* Make sure that there aren't any outstanding picks or 
     * replenishments irrespective of bldg_id. We need to do this, 
     * because in fluid loading, all picked inventory may be on the 
     * trailer (not in a staging lane), but there still might be 
     * more work to do.
     */
    
    misTrc(T_FLOW,"Check if there are outstanding picks ");
    sprintf(buffer,
        " select 'x' "
        "   from pckwrk, "
        "        shipment "
        "  where pckwrk.ship_id   = shipment.ship_id "
        "    and appqty < pckqty "
        "    and shipment.stop_id = '%s' ",
        stop_id);
    
    ret_status = sqlExecStr(buffer, NULL);
    
    if (ret_status == eOK)
    {
        if(load_by_bldg == 0)
            return(srvResults(eINT_STOP_NOT_FULLY_LOADED, NULL));
        else
            return(srvResults(eINT_ADD_INV_EXISTS_NOT_READY_TO_LOAD, NULL));
    }
    else
    {
        if (ret_status != eDB_NO_ROWS_AFFECTED)
            return (srvResults(ret_status, NULL));
    }    
    
    misTrc(T_FLOW,"Check if there are replenishments ");
    sprintf(buffer,
        " select 'x' "
        "   from rplwrk, "
        "        shipment "
        "  where rplwrk.ship_id   = shipment.ship_id "
        "    and shipment.stop_id = '%s' ",
        stop_id);
    
    ret_status = sqlExecStr(buffer, NULL);
    
    if (ret_status == eOK)
    {
        if(load_by_bldg == 0)
            return(srvResults(eINT_STOP_NOT_FULLY_LOADED, NULL));
        else
            return(srvResults(eINT_ADD_INV_EXISTS_NOT_READY_TO_LOAD, NULL));
    }
    else
    {
        if (ret_status != eDB_NO_ROWS_AFFECTED)
            return (srvResults(ret_status, NULL));
    }
    
    misTrc(T_FLOW,"Check if there are cross docks ");
    sprintf(buffer,
        " select 'x' "
        "   from cross_dock_view, "
        "        shipment "
        "  where cross_dock_view.ship_id = shipment.ship_id "
        "    and shipment.stop_id        = '%s' ",
        stop_id);
    
    ret_status = sqlExecStr(buffer, NULL);
    
    if (ret_status == eOK)
    {
        if(load_by_bldg == 0)
            return(srvResults(eINT_STOP_NOT_FULLY_LOADED, NULL));
        else
            return(srvResults(eINT_ADD_INV_EXISTS_NOT_READY_TO_LOAD, NULL));
    }
    else
    {
        if (ret_status != eDB_NO_ROWS_AFFECTED)
            return (srvResults(ret_status, NULL));
    }

    /* Make sure all the shipments are staged, just because there is 
     * no inventory left to pick doesn't mean the shipment actaully 
     * got staged.
     */
    
    sprintf(buffer,
        "select 'x' "
        "  from shipment "
        " where stop_id = '%s' "
        "   and stgdte is null ",
        stop_id);
    
    ret_status = sqlExecStr(buffer, NULL);
    
    if (ret_status == eOK)
    {
        /* At least one shipment isn't staged yet */
        return (srvResults(eINT_SHIP_ID_NOT_YET_STAGED, NULL));
    }
    else if (ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return (srvResults(ret_status, NULL));
    }

    /* Now, we need to make sure that all orders that require being
     * shipped complete or shipped together are fully picked.  
     * If not, then we can't say the stop is fully loaded.
     */
    
    sprintf(buffer,
            "check stop quantities "
            " where stop_id = '%s' "
            "   and autflg  = 1 ",
            stop_id);
    
    ret_status = srvInitiateCommand(buffer, NULL);
    
    if (ret_status != eOK)
    {
        return (srvResults(ret_status, NULL));
    }
    else
    {
        return (srvResults(eOK, NULL));
    }
}
