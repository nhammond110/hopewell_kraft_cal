static const char *rcsid = "$Id: intReceiveInventory.c 137572 2007-09-24 03:21:57Z bli $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <trnlib.h>

#include "intlib.h"

#define METHOD_INHIBIT_MOVE 0
#define METHOD_UNDO_RECEIPT 1
#define METHOD_IGNORE_RECEIPT 2

#define AUTO_CLOSE_DISABLED "DISABLED"	
#define AUTO_CLOSE_THIS_TRUCK_ONLY  "THIS-TRUCK-ONLY" 
#define AUTO_CLOSE_INTERLEAVED "INTERLEAVED"  

#define POLVAR_RECEIVE_AUTO_CLOSE "AUTO-CLOSE-TRUCKS"
#define POLVAL_RCV_AUTO_CLOSE_STRATEGY "CLOSE-STRATEGY"
#define POLVAL_RCV_AUTO_CLOSE_RETURN_CLOSE_ERR	"RETURN-CLOSE-TRUCK-ERROR"

#define RTSTR1_RCV_DISABLED "DISABLED"
#define RTSTR1_RCV_THIS_TRUCK_ONLY "THIS-TRUCK-ONLY"
#define RTSTR1_RCV_INTERLEAVED "INTERLEAVED"


static struct receivePolicyStruct
{
    short undoMethod;
    char  autoCloseStrategy[30];
    short autoCloseReturnCloseError;
}
ReceivePolicies;

static long _intRcvInv(char *trknum_i, char *lodnum_i, char *subnum_i,
                       char *dtlnum_i, char *prtnum_i, char *prt_client_id_i,
                       char *orgcod_i,
                       char *revlvl_i, char *lotnum_i, char *invsts_i,
                       long *untqty_i, int rcvmod, char *wh_id_i,
                       moca_bool_t *ovrrcptconf_i,
                       RETURN_STRUCT *CurPtr);

static long _intRcvInvCloseTruck(char *trknum_i, char *wh_id);
static long _intRcvInvCheckTruckClosed(char *trknum_i, char *wh_id, int rcvmod,
                                       moca_bool_t wko_truck_flg);

static long _intRcvInvCheckTruckStatus(char *trknum_i, char *wh_id,
				       short *foundOnMasterTables,
				       short *linesRemaining);
static long _intRcvInvCheckWkoTruck(char *trknum_i, char *wh_id);


LIBEXPORT 
RETURN_STRUCT *varReceiveInventory(char *srcare_i, char *srcloc_i,
                                   char *dstare_i, char *dstloc_i,
                                   char *trknum_i, char *lodnum_i,
                                   char *subnum_i, char *dtlnum_i,
                                   char *prtnum_i, char *prt_client_id_i,
                                   char *orgcod_i,
                                   char *revlvl_i, char *lotnum_i,
                                   char *invsts_i, long *untqty_i,
                                   moca_bool_t *bypass_trigger_i,
                                   char *wh_id_i,
                                   moca_bool_t *ovrrcptconf_i)
{
    static mocaDataRes *AreaRes = NULL;
    mocaDataRow *AreaRow;
    mocaDataRes *res = NULL;
    mocaDataRow *row; 
    RETURN_STRUCT *CurPtr = (RETURN_STRUCT *)NULL;
    char buffer[100];
    long ret_status;
    int gotSrc;
    int gotDst;
    int prcmod;
    moca_bool_t bypass_trigger = BOOLEAN_FALSE;
    moca_bool_t src_wip_area = BOOLEAN_FALSE;
    char wh_id[WH_ID_LEN + 1];
    
    memset(buffer, 0, sizeof(buffer));
    memset(wh_id, 0, sizeof(wh_id));
    
    /* Validate the warehouse id. */
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    else
        return (APPMissingArg("wh_id"));

    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:2");

    if (bypass_trigger_i)
    {
        bypass_trigger = *bypass_trigger_i;
    }

    /* 
     * Since AreaRes is a static variable, I won't select areas against 
     * a specified warehouse, otherwise, select all areas in all warehouses.
     */
    if (AreaRes == NULL)
    {
	sprintf(buffer,
		"select wh_id, arecod from aremst where expflg = '%d'",
		BOOLEAN_TRUE);
	ret_status = sqlExecStr(buffer, &AreaRes);
	if (ret_status != eOK &&
	    ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    return (srvResults(ret_status, NULL));
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, AreaRes);
    }
    gotSrc = 0;
    gotDst = 0;
    for (AreaRow = (mocaDataRow *) sqlGetRow(AreaRes); AreaRow;
	 AreaRow = (mocaDataRow *) sqlGetNextRow(AreaRow))
    {
	if (strncmp(misTrim(srcare_i),
                    misTrim(sqlGetString(AreaRes, AreaRow, "arecod")),
                    ARECOD_LEN) == 0 &&
            strncmp(wh_id,
                    misTrim(sqlGetString(AreaRes, AreaRow, "wh_id")),
                    WH_ID_LEN) == 0)
	{
	    gotSrc = 1;
	}
	if (strncmp(misTrim(dstare_i),
                    misTrim(sqlGetString(AreaRes, AreaRow, "arecod")),
                    ARECOD_LEN) == 0 &&
            strncmp(wh_id,
                    misTrim(sqlGetString(AreaRes, AreaRow, "wh_id")),
                    WH_ID_LEN) == 0)
	{
	    gotDst = 1;

            /* See if this inventory is returning to stock from a wip area */
            if (srcare_i)
            {
                sprintf(buffer,
                        "select wipflg from aremst "
                        " where arecod = '%s' and wh_id = '%s' ",
                        srcare_i, wh_id);
 
                ret_status = sqlExecStr(buffer, &res);
                if (ret_status != eOK &&
                    ret_status != eDB_NO_ROWS_AFFECTED)
                {
                    sqlFreeResults(res);
                    return (srvResults(ret_status, NULL));
                }
                else if (ret_status == eOK)
                {
                    row = sqlGetRow(res);
                    src_wip_area = sqlGetLong(res, row, "wipflg");
                    sqlFreeResults(res); 
       	        }
                else
                    sqlFreeResults(res);
            }   
        }   
    }

    /* If we didn't find an area or the source and dest are the same, exit.   */
    /* pr 6516 We needed to add a check to see if both source and dest are    */
    /* expected reciept areas. If they are, then exit. This covers id of      */
    /* serialized parts where truck is identifying in an area other than EXPR.*/
    /* pr 7996 Added a bypass trigger flag for future customizations to allow */
    /* them to move inventory without triggering this command. Also added a   */
    /* a check of the src area - if it is a wip area, we'll skip this trigger */
    /* so we can return component inventory to stock via the wip expr area.   */

    sprintf(buffer, "*'%s'*", STOLOC_PERM_CREATE);
    if ( (gotSrc==0 && gotDst==0) || strncmp(srcare_i, dstare_i, ARECOD_LEN) == 0 ||
	strncmp(srcloc_i, STOLOC_PERM_CREATE, strlen(STOLOC_PERM_CREATE)) == 0 ||
        (gotSrc==1 && gotDst==1) || bypass_trigger == BOOLEAN_TRUE ||
         src_wip_area == BOOLEAN_TRUE)
    {
	return (srvResults(eOK, NULL));
    }

    misTrc(T_FLOW, "In Receive Inventory...");

    if (gotSrc)
	prcmod = RCVMOD_RECEIVE;
    else
	prcmod = RCVMOD_REVERSE;

    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:10");

    ret_status = _intRcvInv(trknum_i, lodnum_i, subnum_i,
                            dtlnum_i, prtnum_i, prt_client_id_i,
                            orgcod_i,
                            revlvl_i, lotnum_i, invsts_i,
                            untqty_i, prcmod, wh_id, ovrrcptconf_i, CurPtr);

    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:1000");


    if (CurPtr == NULL)
        CurPtr = srvResults(ret_status, NULL);

    return (CurPtr);
}

static long _intRcvInv(char *trknum_i, char *lodnum_i, char *subnum_i,
                       char *dtlnum_i, char *prtnum_i, char *prt_client_id_i,
                       char *orgcod_i,
                       char *revlvl_i, char *lotnum_i, char *invsts_i,
                       long *untqty_i, int rcvmod, char *wh_id,
                       moca_bool_t *ovrrcptconf_i,
                       RETURN_STRUCT *CurPtr)
{
    long ret_status;
    char buffer[2000];
    mocaDataRes *res, *res2;
    mocaDataRow *row, *row2;
    static short gotPolicies = 0;
    static short gotAutoClosePolicies = 0;
    short foundOnMasterTables;
    short linesRemaining;
    char rtstr1[RTSTR1_LEN + 1];
    char polval[POLVAL_LEN + 1];
    char rcvkey[RCVKEY_LEN + 1];

    char trknum[TRKNUM_LEN + 1];
    char cur_trknum[TRKNUM_LEN + 1];
    char tmp_trknum[TRKNUM_LEN + 1];
    char lodnum[LODNUM_LEN + 1];
    char subnum[SUBNUM_LEN + 1];
    char dtlnum[DTLNUM_LEN + 1];
    char prtnum[PRTNUM_LEN + 1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char orgcod[ORGCOD_LEN + 1];
    char revlvl[REVLVL_LEN + 1];
    char lotnum[LOTNUM_LEN + 1];
    char invsts[INVSTS_LEN + 1];
    moca_bool_t wko_truck_flg = BOOLEAN_FALSE;
    long untqty;

    /* Make sure we know what we are starting with...  */
    memset(trknum, 0, sizeof(trknum));
    memset(cur_trknum, 0, sizeof(cur_trknum));
    memset(lodnum, 0, sizeof(lodnum));
    memset(subnum, 0, sizeof(subnum));
    memset(dtlnum, 0, sizeof(dtlnum));
    memset(prtnum, 0, sizeof(prtnum));
    memset(orgcod, 0, sizeof(orgcod));
    memset(revlvl, 0, sizeof(revlvl));
    memset(lotnum, 0, sizeof(lotnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(invsts, 0, sizeof(invsts));
    untqty = 0;

    /* Firewall the arguments coming in... */
    if (trknum_i)
	misTrimcpy(trknum, trknum_i, TRKNUM_LEN);
    if (lodnum_i)
	misTrimcpy(lodnum, lodnum_i, LODNUM_LEN);
    if (subnum_i)
	misTrimcpy(subnum, subnum_i, SUBNUM_LEN);
    if (dtlnum_i)
	misTrimcpy(dtlnum, dtlnum_i, DTLNUM_LEN);
    if (prtnum_i)
	misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);
    if (orgcod_i)
	misTrimcpy(orgcod, orgcod_i, ORGCOD_LEN);
    if (revlvl_i)
	misTrimcpy(revlvl, revlvl_i, REVLVL_LEN);
    if (lotnum_i)
	misTrimcpy(lotnum, lotnum_i, LOTNUM_LEN);
    if (invsts_i)
	misTrimcpy(invsts, invsts_i, INVSTS_LEN);
    if (untqty_i)
	untqty = *untqty_i;

    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:20");

    /*  If no part number is passed in, assume that we don't need the 
     *  prt_client_id validation.
    */
    if (prtnum_i && strlen (prtnum_i))
    {
	ret_status = appGetClient(prt_client_id_i, prt_client_id);
	if (ret_status != eOK)
	    return(ret_status);
    }

    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:30");

    ret_status = eOK;

    if (misTrimLen(trknum, TRKNUM_LEN) == 0 && rcvmod == RCVMOD_RECEIVE)
        return (eINVALID_ARGS);
    /* First let's see if this is a work order truck.  */
    /* Since work order trucks don't have a trailer associated */
    /* with them, we can't do any checks against the trailer status. */

    ret_status = _intRcvInvCheckWkoTruck(trknum_i, wh_id);
    if (ret_status != BOOLEAN_TRUE &&
        ret_status != BOOLEAN_FALSE)
    {
        return(ret_status);
    }
    else
        wko_truck_flg = ret_status;

    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:40");


    /* Now...we've got all the arguments...let see what we have to do... */
    if (rcvmod == RCVMOD_RECEIVE)
    {
    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:50");

        ret_status = _intRcvInvCheckTruckClosed(trknum, wh_id, 
                                                rcvmod, wko_truck_flg);

        if (ret_status != eOK)
            return (ret_status);

    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:60");

	if (strlen(dtlnum))
	{
    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:70");
	    /* Then pass the trknum, part, orgcod, revlvl, 
	       lotnum, and quantity to trnReceiveByDtl */
	    misTrc(T_FLOW, "Update receipt info by dtl: %s, trk: %s",
		   dtlnum, trknum);
	    ret_status = trnReceiveByDtl(trknum, dtlnum, wh_id, ovrrcptconf_i);
	    if (ret_status != eOK)
	    {
		misTrc(T_FLOW, "Failed update of receipt info");
		misLogError("Failed(%d) to perform receipt "
			    "operation for trk: %s, dtl: %s",
			    ret_status, trknum, dtlnum);
		return (ret_status);
	    }
	}
	else if (strlen(subnum))
	{
    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:80");
	    /* Then pass the trknum and subnum to trnReceiveBySub */
	    misTrc(T_FLOW, "Update receipt info by sub: %s, trk: %s",
		   subnum, trknum);
	    ret_status = trnReceiveBySub(trknum, subnum, wh_id, ovrrcptconf_i);
	    if (ret_status != eOK)
	    {
		misTrc(T_FLOW, "Failed update of receipt info");
		misLogError("Failed (%d) to perform receipt "
			    "operation for trk: %s, sub: %s",
			    ret_status, trknum, subnum);
		return (ret_status);
	    }

	}
	else if (strlen(lodnum))
	{
    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:90");
	    /* Then pass the trknum and lodnum to trnReceiveByLoad */
	    misTrc(T_FLOW, "Update receipt info by lod: %s, trk: %s",
		   lodnum, trknum);
	    ret_status = trnReceiveByLoad(trknum, lodnum, wh_id, ovrrcptconf_i);
	    if (ret_status != eOK)
	    {
		misTrc(T_FLOW, "Failed update of receipt info");
		misLogError("Failed (%d) to perform receipt "
			    "operation for trk: %s, lod: %s",
			    ret_status, trknum, lodnum);
		return (ret_status);
	    }
	}
	/* Now, if we are supposed to be auto-closing trucks, perform the */
	/* processing ... */
	
	if (!gotAutoClosePolicies)
	{
	    misTrc(T_FLOW, "Reading policies for Auto Close of Trucks");
	    sprintf(buffer,
		    "select * "
		    "  from poldat_view "
		    " where polcod = '%s' "
		    "   and polvar = '%s' "
		    "   and wh_id  = '%s' "
		    " order by polval ",
		    POLCOD_RECEIVE_INVENTORY,
		    POLVAR_RECEIVE_AUTO_CLOSE,
		    wh_id);
	    ret_status = sqlExecStr(buffer, &res);
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
		sqlFreeResults(res);
		return (ret_status);
	    }
	    ret_status = eOK;
	    strcpy(ReceivePolicies.autoCloseStrategy, AUTO_CLOSE_DISABLED);
	    ReceivePolicies.autoCloseReturnCloseError = FALSE;
	    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
		memset(rtstr1, 0, sizeof(rtstr1));
		memset(polval, 0, sizeof(polval));
		strcpy(rtstr1, 
		       misTrim(sqlGetString(res, row, "rtstr1")));
		strcpy(polval, 
		       misTrim(sqlGetString(res, row, "polval")));
		if (!misCiStrcmp(polval, POLVAL_RCV_AUTO_CLOSE_STRATEGY))
		{
		    if (!misCiStrcmp(rtstr1, RTSTR1_RCV_THIS_TRUCK_ONLY))
			strcpy(ReceivePolicies.autoCloseStrategy,
			       AUTO_CLOSE_THIS_TRUCK_ONLY);
		    else if (!misCiStrcmp(rtstr1, RTSTR1_RCV_INTERLEAVED))
			strcpy(ReceivePolicies.autoCloseStrategy,
			       AUTO_CLOSE_INTERLEAVED);
		    else
			strcpy(ReceivePolicies.autoCloseStrategy,
			       AUTO_CLOSE_DISABLED);
		}
		if (!misCiStrcmp(polval, 
				 POLVAL_RCV_AUTO_CLOSE_RETURN_CLOSE_ERR))
		{
		    if (!misCiStrcmp(rtstr1, YES_STR))
			ReceivePolicies.autoCloseReturnCloseError = TRUE;
		}
	    }
	    sqlFreeResults(res);
	    gotAutoClosePolicies = 1;
	}

	/* If we are supposed to process the auto close of trucks, then ... */
	if (strcmp(ReceivePolicies.autoCloseStrategy, AUTO_CLOSE_DISABLED) &&
            (!wko_truck_flg))
	{
	    misTrc(T_FLOW, "Performing Auto Close based on policies...");

	    ret_status = _intRcvInvCheckTruckStatus(trknum,
	                                            wh_id,
						    &foundOnMasterTables,
						    &linesRemaining);
	    if (ret_status != eOK)
		return (ret_status);

	    /* If there are not lines remaining, then we need to close this */
	    /* truck by performing a srvInitiate on CLOSE RECEIVE TRUCK */
	    if (linesRemaining == FALSE)
	    {
		ret_status = _intRcvInvCloseTruck(trknum, wh_id);
		if (ret_status != eOK &&
		    ReceivePolicies.autoCloseReturnCloseError == TRUE)
		    return (ret_status);
	    }

	    /* Finally, if we are supposed to check for interleaved closes */
	    /* get any open trucks (except this one), and close them if */
	    /* they are ready to be closed */

	    if (foundOnMasterTables == TRUE &&
		strcmp(ReceivePolicies.autoCloseStrategy, 
                       AUTO_CLOSE_INTERLEAVED) == 0)
	    {
		/* Select truck list of any open trucks */

		sprintf(buffer,
			"select distinct rcvtrk.trknum, "
			"                rcvtrk.wh_id "
			"  from rcvtrk, "
                        "       trlr, "
			"       rcvinv ri1"
			" where trlr.trlr_stat in ('%s', '%s', '%s', '%s') "
                        "   and rcvtrk.trlr_id = trlr.trlr_id "
			"   and rcvtrk.trknum = ri1.trknum "
			"   and rcvtrk.wh_id = ri1.wh_id "
			"   and exists (select 'x' "
			"                 from rcvinv ri2"
			"                where ri2.trknum = '%s' "
			"                  and ri2.wh_id = '%s' "
			"                  and ri2.supnum    = ri1.supnum "
			"                  and ri2.client_id = ri1.client_id "
			"                  and ri2.invnum    = ri1.invnum "
			"                  and ri2.wh_id     = ri1.wh_id) ",
                        TRLSTS_CHECKED_IN,
                        TRLSTS_OPEN_FOR_RCV,
                        TRLSTS_RECEIVING,
                        TRLSTS_SUSPENDED,
			trknum,
			wh_id);
		ret_status = sqlExecStr(buffer, &res);
		if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
		{
		    sqlFreeResults(res);
		    return (ret_status);
		}

		for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
		{
		    memset(tmp_trknum, 0, sizeof(tmp_trknum));
		    strncpy(tmp_trknum,
			    sqlGetString(res, row, "trknum"),
			    misTrimLen(sqlGetString(res, row, "trknum"),
                            TRKNUM_LEN));
		    
		    ret_status = 
			_intRcvInvCheckTruckStatus(tmp_trknum,
			                           wh_id,
						   &foundOnMasterTables,
						   &linesRemaining);
		    if (ret_status != eOK)
                    {
                        sqlFreeResults(res);
			return (ret_status);
                    }

		    /* If there are not lines remaining, 
		       then we need to close this 
		       truck by performing a 
		       srvInitiate on CLOSE RECEIVE TRUCK */
		    
		    if (linesRemaining == FALSE)
		    {
			ret_status = _intRcvInvCloseTruck(tmp_trknum, wh_id);
			if (ret_status != eOK &&
			    ReceivePolicies.autoCloseReturnCloseError == TRUE)
                        {
                            sqlFreeResults(res);
			    return (ret_status);
                        }
		    }
		}
		sqlFreeResults(res);
	    }
	    ret_status = eOK;
	}
    }
    else
	/* We are going in reverse mode ... */
    {
    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:200");

	if (!gotPolicies)
	{
	    misTrc(T_FLOW, "Reading policies for Receive-Undo Receipts");
	    sprintf(buffer,
		    "select * "
		    "  from poldat_view "
		    " where polcod = '%s' "
		    "   and polvar = '%s' "
		    "   and wh_id  = '%s' "
		    " order by polval ",
		    POLCOD_RECEIVE_INVENTORY,
		    POLVAR_RECEIVE_UNDO,
		    wh_id);
	    ret_status = sqlExecStr(buffer, &res);
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
		sqlFreeResults(res);
		return (ret_status);
	    }
	    ret_status = eOK;
	    ReceivePolicies.undoMethod = METHOD_IGNORE_RECEIPT;
	    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
		memset(rtstr1, 0, sizeof(rtstr1));
		memset(polval, 0, sizeof(polval));
		strcpy(rtstr1, 
		       misTrim(sqlGetString(res, row, "rtstr1")));
		strcpy(polval, 
		       misTrim(sqlGetString(res, row, "polval")));
		if (!misCiStrcmp(polval, POLVAL_RCV_UNDO_METHOD))
		{
		    if (!misCiStrcmp(rtstr1, RTSTR1_RCV_INHIBIT_MOVE))
			ReceivePolicies.undoMethod = METHOD_INHIBIT_MOVE;
		    else if (!misCiStrcmp(rtstr1, RTSTR1_RCV_UNDO_RECEIPT))
			ReceivePolicies.undoMethod = METHOD_UNDO_RECEIPT;
		    else if (!misCiStrcmp(rtstr1, RTSTR1_RCV_IGNORE_RECEIPT))
			ReceivePolicies.undoMethod = METHOD_IGNORE_RECEIPT;
		}
	    }
	    sqlFreeResults(res);
	    gotPolicies = 1;
	}
	/* Let's find out if we can do this move or not */
	if (ReceivePolicies.undoMethod == METHOD_INHIBIT_MOVE)
	{
	    return (eINT_MOVE_BACK_TO_EXPR_NOT_ALLOWED);
	}

	if (strlen(dtlnum))
	{
	    sprintf(buffer,
		    "[select invdtl.dtlnum, invdtl.untqty, "
		    "	    invdtl.catch_qty, invdtl.rcvkey "
		    "  from invdtl "
		    " where dtlnum = '%s'] | "
		    "[update invdtl set rcvdte = NULL "
		    " where dtlnum = @dtlnum] | "
		    "publish data "
                    "where dtlnum = @dtlnum "
                    "  and untqty = @untqty "
                    "  and rcvkey = @rcvkey "
                    "  and catch_qty = @catch_qty",
		    dtlnum);
	}
	else if (strlen(subnum))
	{
	    sprintf(buffer,
		    "[select invdtl.dtlnum, invdtl.untqty, "
		    "	    invdtl.catch_qty, invdtl.rcvkey "
		    "  from invdtl "
		    " where subnum = '%s'] | "
		    "[update invdtl set rcvdte = NULL "
		    " where dtlnum = @dtlnum] | "
		    "publish data "
                    "where dtlnum = @dtlnum "
                    "  and untqty = @untqty "
                    "  and rcvkey = @rcvkey "
                    "  and catch_qty = @catch_qty",
		    subnum);
	}
	else if (strlen(lodnum))
	{
	    sprintf(buffer,
		    "[select invdtl.dtlnum, invdtl.untqty, "
		    "	    invdtl.catch_qty, invdtl.rcvkey "
		    "  from invsub, invdtl "
		    " where invdtl.subnum = invsub.subnum "
		    "   and invsub.lodnum = '%s'] | "
		    "[update invdtl set rcvdte = NULL "
		    " where dtlnum = @dtlnum] | "
		    "publish data "
                    "where dtlnum = @dtlnum "
                    "  and untqty = @untqty "
                    "  and rcvkey = @rcvkey "
                    "  and catch_qty = @catch_qty",
		    lodnum);

	}
	ret_status = srvInitiateCommand(buffer, &CurPtr);
	if (ret_status != eOK)
	{
	    srvFreeMemory(SRVRET_STRUCT, CurPtr);
	}
	else
	{
            res = srvGetResults(CurPtr);
	    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
                ret_status = eOK;
		memset(rcvkey, 0, sizeof(rcvkey));
		strncpy(rcvkey, sqlGetString(res, row, "rcvkey"),
			misTrimLen(sqlGetString(res, row, "rcvkey"),
				   RCVKEY_LEN));
		/* Let's find out if we are going to the same truck. */
                sprintf(buffer,
                        "select trknum, wh_id "
                        "  from rcvlin "
                        " where rcvkey = '%s' ",
                        rcvkey);
                ret_status = sqlExecStr(buffer, &res2);
                if (eOK != ret_status)
                {
                    misTrc(T_FLOW, "Error finding receive line.");
                    sqlFreeResults(res);
                    sqlFreeResults(res2);
                    return(ret_status);
                }

                /* Get the current truck number from the assigned receive key */
                row2 = sqlGetRow(res2);
                misTrimcpy(cur_trknum, 
                           sqlGetString(res2, row2, "trknum"), TRKNUM_LEN);
                sqlFreeResults(res2);

                /* Make sure the truck is not already closed. */

                ret_status = _intRcvInvCheckTruckClosed(cur_trknum, wh_id,
                                                        rcvmod, wko_truck_flg);
                if (ret_status != eOK)
                    return (ret_status);

	        misTrc(T_FLOW, 
			   "Enforcing same truck return for rcvkey %s", rcvkey);

                /*
                 * Since the rcvkey is now put on at identify rather than
                 * rcv, it does not make sense to let the user put the 
                 * inventory back on a different truck than the one it
                 * was received on, because we would null out the rcvkey,
                 * and then we wouldn't be able to receive it in since the
                 * rcvkey is null, so the user would have to remove the load
                 * and re-identify it anyway (which we can now do with reverse
                 * receipt), so we should make it go back to the same truck.
                 */

	        if (strncmp(trknum, cur_trknum, TRKNUM_LEN) != 0)
		{
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    return (eINT_CANT_MOVE_TO_DIFFERENT_EXPR);
		}
		if (ReceivePolicies.undoMethod == METHOD_UNDO_RECEIPT)
		{
		    sprintf(buffer,
			    "update rcvlin "
			    "   set rcvqty = rcvqty - %ld, "
			    "       rcv_catch_qty = rcv_catch_qty - %f "
			    " where rcvkey = '%s' "
			    "   and rcvqty >= '%ld' ",
			    sqlGetLong(res, row, "untqty"),
			    sqlGetFloat(res, row, "catch_qty"),
			    sqlGetString(res, row, "rcvkey"),
			    sqlGetLong(res, row, "untqty"));
		    ret_status = sqlExecStr(buffer, NULL);
		    if (ret_status != eOK)
		    {
	                srvFreeMemory(SRVRET_STRUCT, CurPtr);
			if (ret_status == eDB_NO_ROWS_AFFECTED)
			    return (eINT_NO_NEGATIVES);
			else
			    return (ret_status);
		    }
		}
	    }
	    srvFreeMemory(SRVRET_STRUCT, CurPtr);
	}
    }
    misTrc(T_FLOW, "!!!!!!!!!!!!!JJDEBUG:700");
    return (ret_status);
}

static long _intRcvInvCloseTruck(char *trknum_i, char *wh_id)
{
    long ret_status;
    char buffer[200];

    misTrc(T_FLOW, "The truck will be closed");
    sprintf(buffer,
	    "close receive truck where trknum = \"%s\" and wh_id = \"%s\" ",
	    trknum_i, wh_id);
    ret_status = srvInitiateCommand(buffer, NULL);

    return (ret_status);

}

static long _intRcvInvCheckTruckClosed(char *trknum_i,
                                       char *wh_id,
                                       int rcvmod,
                                       moca_bool_t wko_truck_flg)
{
    mocaDataRes *res, *res2;
    mocaDataRow *row, *row2;
    long        ret_status;
    char        buffer[400];
    long        delv_flg;

    if (!wko_truck_flg)
    {
        /* Since we don't have trailers for work orders, we can't      */
        /*    check the trailer status.                                */

        misTrc(T_FLOW, "Checking if the truck is closed");
        sprintf(buffer, 
                "select trlr.trlr_stat, "
                "       trlr.trlr_cod,   "
                "       trlr.delv_flg   "
                "  from rcvtrk,        "
                "       trlr           "
                " where rcvtrk.trlr_id = trlr.trlr_id "
                "   and rcvtrk.trknum = '%s'  "
                "   and rcvtrk.wh_id = '%s' ",
                trknum_i,
                wh_id);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sqlFreeResults(res);
            return (ret_status);
        }
        row = sqlGetRow(res);
        /*
         * Delivery Changes
         * Changed to allow receiving on a delivery trailer.
         */
        delv_flg = sqlGetLong(res, row, "delv_flg");
        if (((strcmp(sqlGetString(res, row, "trlr_cod"), TRLR_COD_SHIP) == 0) &&
            delv_flg == MOCA_FALSE)      ||
            (strcmp(sqlGetString(res, row, "trlr_stat"), TRLSTS_CLOSED)==0) ||
            (strcmp(sqlGetString(res, row, "trlr_stat"), TRLSTS_DISPATCHED)==0))
        {
            /* Truck is closed or dispatched, can't receive or reverse */
            /* against it.  */

            sqlFreeResults(res);
            if (rcvmod == RCVMOD_RECEIVE)
                return (eINT_CANT_RECEIVE_AGAINST_CLOSED_TRUCK);
            else
            {
                /*
                 * Whether or not we can reverse against a closed truck
                 * is controlled by a policy.  So, let's check the value.
                 */

                sprintf(buffer,
                        " select rtnum1 "
                        "  from poldat_view "
                        " where polcod = '%s' "
                        "   and polvar = '%s' "
                        "   and polval = '%s' "
                        "   and wh_id  = '%s' ",
                        POLCOD_RECEIVE_INVENTORY,
                        POLVAR_RECEIVE_UNDO,
                        POLVAL_ALLOW_REVERSE_AFTER_CLOSE,
                        wh_id);
          
                ret_status = sqlExecStr(buffer, &res2);
                row2 = sqlGetRow(res2);
                if (sqlGetLong(res2, row2, "rtnum1") == BOOLEAN_FALSE)
                {
                    sqlFreeResults(res2);
                    return (eINT_CANT_REVERSE_AGAINST_CLOSED_TRUCK);
                }
                sqlFreeResults(res2);
            }
        }
        else
        {
            sqlFreeResults(res);
        }
    }
    return(eOK);
}

static long _intRcvInvCheckTruckStatus(char *trknum_i,
                                       char *wh_id,
				       short *foundOnMasterTables,
				       short *linesRemaining)
{
    long ret_status;
    char buffer[2000];
    mocaDataRes *res;
    mocaDataRow *row;

    /* First, try to summarize this truck against the RIM tables */
    /* If we are supposed to be processing in INTERLEAVED mode, then */
    /* we really need to summarize all of the invoices under our truck */
    /* to see if the invoices are fully satisfied ... */

    misTrc(T_FLOW, "Auto Close Strategy is %s ", 
           ReceivePolicies.autoCloseStrategy);

    if (strcmp(ReceivePolicies.autoCloseStrategy, AUTO_CLOSE_INTERLEAVED) == 0)
	sprintf(buffer,
		"select rimlin.wh_id, rimlin.client_id, "
		"       rimlin.supnum, rimlin.invnum, rimlin.invlin, "
		"       rimlin.invsln, rimlin.expqty, "
		"       sum(rcvlin.rcvqty) rcvqty "
		"  from rimlin, "
		"       rcvlin "
		" where rimlin.supnum    = rcvlin.supnum "
		"   and rimlin.wh_id     = rcvlin.wh_id "
		"   and rimlin.client_id = rcvlin.client_id "
		"   and rimlin.invnum    = rcvlin.invnum "
		"   and rimlin.invlin    = rcvlin.invlin "
		"   and rimlin.invsln    = rcvlin.invsln "
		"   and rcvlin.wh_id     = '%s' "
		"   and exists (select 'x' "
		"                from rcvinv ri "
		"               where ri.trknum    = '%s' "
		"                 and ri.wh_id     = '%s' "
		"                 and ri.client_id = rimlin.client_id "
		"                 and ri.supnum    = rimlin.supnum "
		"                 and ri.wh_id     = rimlin.wh_id "
		"                 and ri.invnum    = rimlin.invnum) "
		" group by rimlin.wh_id, rimlin.client_id, "
		"          rimlin.supnum, rimlin.invnum, rimlin.invlin, "
		"          rimlin.invsln, rimlin.expqty ",
		wh_id, trknum_i, wh_id);
    /* Otherwise, we only need to summarize what is under our truck ... */

    else
	sprintf(buffer,
		"select rimlin.wh_id, rimlin.client_id, "
		"       rimlin.supnum, rimlin.invnum, rimlin.invlin, "
		"       rimlin.invsln, rimlin.expqty, "
		"       sum(rcvlin.rcvqty) rcvqty "
		"  from rimlin, rcvlin "
		" where rimlin.supnum    = rcvlin.supnum "
		"   and rimlin.wh_id     = rcvlin.wh_id "
		"   and rimlin.client_id = rcvlin.client_id "
		"   and rimlin.invnum    = rcvlin.invnum "
		"   and rimlin.invlin    = rcvlin.invlin "
		"   and rimlin.invsln    = rcvlin.invsln "
		"   and rcvlin.trknum    = '%s' "
		"   and rcvlin.wh_id     = '%s' "
		" group by rimlin.wh_id, rimlin.client_id, "
		"          rimlin.supnum, rimlin.invnum, rimlin.invlin, "
		"          rimlin.invsln, rimlin.expqty ",
		trknum_i, wh_id);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
	return (ret_status);
    }
    else if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
	*foundOnMasterTables = FALSE;
	sprintf(buffer,
		"select rcvlin.wh_id, rcvlin.client_id, "
		"       rcvlin.supnum, rcvlin.invnum, rcvlin.invlin, "
		"       rcvlin.invsln, rcvlin.expqty, rcvlin.rcvqty "
		"  from rcvlin "
		" where rcvlin.trknum = '%s' "
		"   and rcvlin.wh_id = '%s' ",
		trknum_i, wh_id);
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}
    }
    else
    {
        misTrc(T_FLOW, "Master truck information was found. ");
	*foundOnMasterTables = TRUE;
    }

    *linesRemaining = FALSE;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	if (*foundOnMasterTables == TRUE)
	{
	    if (sqlGetLong(res, row, "rcvqty") <
		sqlGetLong(res, row, "expqty"))
	    {
                misTrc(T_FLOW, "Product left to receive for invnum: %s",
                       sqlGetString(res, row, "invnum"));
		*linesRemaining = TRUE;
		break;
	    }
	}
	else
	{
	    if (sqlGetLong(res, row, "rcvqty") <
		sqlGetLong(res, row, "expqty"))
	    {
                misTrc(T_FLOW, "Product left to receive for invnum: %s",
                       sqlGetString(res, row, "invnum"));
		*linesRemaining = TRUE;
		break;
	    }
	}
    }
    sqlFreeResults(res);

    return (ret_status);
}

static long _intRcvInvCheckWkoTruck(char *trknum_i, char *wh_id)
{
    long        ret_status;
    char        buffer[1000];

    /* We need to find out if this is a work order truck. */

    misTrc(T_FLOW, "Checking if this is a work order truck");

    /* Warehouse ID is not needed for poldat_view because this 
     * policy is is NOT overridable (POLVAL_WRKORD_RCV_DEFAULTS)
     * Instead we will pass the default warehouse ('----').
     */
    sprintf(buffer,
            "select 'x'                                      "
            "  from rcvinv                                   "
            " where trknum = '%s'                            "
            "   and wh_id  = '%s'                            "
            "   and invtyp in (select distinct rtstr2 invtyp "
            "                    from poldat_view                 "
            "                   where polcod = '%s'          "
            "                     and polvar = '%s'          "
            "                     and polval = '%s'          "
            "                     and wh_id  = '%s'          "
            "                     and rtstr1 = 'invtyp')     ",
            trknum_i,
            wh_id,
            POLCOD_WORK_ORDER_PROC,
            POLVAR_MISC,
            POLVAL_WRKORD_RCV_DEFAULTS,
            DEFAULT_WH_ID);

    ret_status = sqlExecStr(buffer, NULL);

    if (ret_status != eOK &&
        ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return (ret_status);
    }
    else if (ret_status == eOK)
    {
        /* It's a work order truck.*/
        misTrc(T_FLOW, "This is a work order truck.");
        return(BOOLEAN_TRUE);
    }
    else
    {
        /* Not a work order truck. */
        return(BOOLEAN_FALSE);
    }
}
 
