/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $URL: https://athena.redprairie.com/svn/prod/wmd/tags/2008.1.5/src/libsrc/dcsrf/rflib.h $
 *  $Revision: 1.3 $
 *  $Id: rflib.h,v 1.3 2010/11/11 18:39:14 dkells Exp $
 *
 *  Application:  Intrinsic Library
 *  Created:   31-Jan-1994
 *  $Author: dkells $
 *
 *  Purpose:   Structure defs for RFLIB
 *
 *#END************************************************************************/

#include <applib.h>

#ifndef RFLIB_H
#define RFLIB_H

/* RF length definitions */

#define RF_BUFFER_LEN		2000
#define RF_FRMNAM_LEN		30


/* Library function prototypes */

long rfGetLong (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);

char *rfGetString (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);

long rfLogMsg (char *header, char *format, ...);

#endif


