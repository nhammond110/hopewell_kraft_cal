static const char *rcsid = "$Id: varMoveInventory.c 430237 2013-06-24 18:17:10Z gbane $";
/*#START***********************************************************************
 *  Copyright (c) 2004 RedPrairie Corporation. All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <trnlib.h>

#include "intlib.h"

#define GLOM_NO_OVRRDE  0
#define GLOM_NEVER    1
#define GLOM_ANY    2
#define GLOM_IF_UNIQUE    3
#define GLOM_IF_EXISTS    4

/*
 *
 * HISTORY
 * SYIM 02/03/2010 - Added functionality to call 'get var lodnum pcktwr' to determine the proper lodnum numcod to use when
 *                   generating the next lodnum via appNextNum.
 *
 * RDR 06/21/2013 - Handling the creation of a new lpn via sCreateGlomLoad to support new SSCC standards
 *
 */

typedef struct _policies
{
    long            Loaded;
    mocaDataRes    *GlomRes;
    long            CreateDestination;
    long            GLOMOverride;
}
POLICY_STRUCT;

static POLICY_STRUCT Policies;


static long sCreateGlomSub(char *lodnum,
                           SUB_INFO * sub,
                           moca_bool_t phyflg,
                           char *subtag,
                           char *sub_asset_typ)
{
    long ret_status;
    char subnum[SUBNUM_LEN + 1];


    memset(subnum, 0, sizeof(subnum));
    appNextNum(NUMCOD_SUBNUM, subnum);

    ret_status = 
    trnCreateSub(subnum, lodnum, 0, phyflg, 0, 1,
                 osGetVar(LESENV_DEVCOD) ? osGetVar(LESENV_DEVCOD) : "", 
                 osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "",
                 subtag, sub_asset_typ);

    if (ret_status != eOK)
    {
        misTrc(T_FLOW, "Error creating new destination sub... ");
        return (ret_status);
    }
    memset(sub, 0, sizeof(SUB_INFO));
    strncpy(sub->subnum, subnum, SUBNUM_LEN);
    strncpy(sub->lodnum, lodnum, LODNUM_LEN);
    sub->prmflg = BOOLEAN_FALSE;
    sub->phyflg = BOOLEAN_FALSE;
    sub->idmflg = BOOLEAN_FALSE;
    sub->mvsflg = BOOLEAN_TRUE;
    return (eOK);
}

static long sGetDestSub(char *lodnum,
            SUB_INFO * sub,
            long strategy,
            moca_bool_t phyflg,
            char *subtag,
            char *sub_asset_typ)
{
    long            ret_status;
    char            buffer[1000];
    mocaDataRes    *res;
    mocaDataRow    *row;

    /* Simplest case of all is the never glom...go ahead and
       create... */
    if (strategy == GLOM_NEVER)
    {
        ret_status = sCreateGlomSub(lodnum, sub, phyflg, subtag, 
            sub_asset_typ);
        return (ret_status);
    }

    sqlSetSavepoint("sub_select_savepoint");

    /* Ok...so we need to know what is there...
       ideally, we attach to perm subs, so we get those first... */
    sprintf(buffer,
        "select subnum, prmflg, mvsflg, phyflg, idmflg "
        "  from invsub "
        " where lodnum = '%s'"
        "    and (prmflg = %d or (prmflg = %d and idmflg = %d))"
        "    and phyflg = %d "
        " order by decode(prmflg, %d, 1, 2)"
        " for update",
        lodnum, BOOLEAN_TRUE, 
        BOOLEAN_FALSE, BOOLEAN_FALSE, 
        BOOLEAN_FALSE, BOOLEAN_TRUE);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            if (strategy == GLOM_IF_EXISTS)
            {
                misTrc(T_FLOW, "No existing inventory to attach to...");
                sqlFreeResults(res);
                return (eINT_GLOM_EMPTY_DEST_FAILED);
            }

            /* At this point, strategy is either UNIQUE or ANY...in either
               case, create what we need... */
            ret_status = sCreateGlomSub(lodnum, sub, phyflg, subtag, 
                sub_asset_typ);
            sqlFreeResults(res);
            return (ret_status);
        }
        else
        {
            sqlFreeResults(res);
            return (ret_status);
        }
    }

    /* We have inventory structures existing in the location...
       see what we can do... */
    row = sqlGetRow(res);
    if (sqlGetBoolean(res, row, "prmflg") == BOOLEAN_TRUE)
    {
        sqlRollbackToSavepoint("pre_sub_select_savepoint");
    }

    if (strategy == GLOM_ANY)
    {
        memset(sub, 0, sizeof(SUB_INFO));
        strncpy(sub->lodnum, lodnum, LODNUM_LEN);
        strncpy(sub->subnum, 
                sqlGetString(res, row, "subnum"),
                misTrimLen(sqlGetString(res, row, "subnum"), SUBNUM_LEN));
        sub->prmflg = sqlGetBoolean(res, row, "prmflg");
        sub->mvsflg = sqlGetBoolean(res, row, "mvsflg");
        sub->phyflg = sqlGetBoolean(res, row, "phyflg");
        sub->idmflg = sqlGetBoolean(res, row, "idmflg");
        misTrc(T_FLOW, "Strategy is ANY, grabbing sub: %s", sub->subnum);
        sqlFreeResults(res);
        return (eOK);
    }

    /* From here, we're left with UNIQUE or EXISTS - in both cases,
       if we've got a prmflg=1 sub, then we're good to go...
       otherwise, we have to check the row count... */
    if (sqlGetBoolean(res, row, "prmflg") == BOOLEAN_TRUE)
    {
        memset(sub, 0, sizeof(SUB_INFO));
        strncpy(sub->lodnum, lodnum, LODNUM_LEN);
        strncpy(sub->subnum,
        sqlGetString(res, row, "subnum"),
                     misTrimLen(sqlGetString(res, row, "subnum"), 
                     SUBNUM_LEN));
        sub->prmflg = sqlGetBoolean(res, row, "prmflg");
        sub->mvsflg = sqlGetBoolean(res, row, "mvsflg");
        sub->phyflg = sqlGetBoolean(res, row, "phyflg");
        sub->idmflg = sqlGetBoolean(res, row, "idmflg");
        misTrc(T_FLOW, "Strategy is unique/exists, prmsub=%s", sub->subnum);
        sqlFreeResults(res);
        return (eOK);
    }
    if (res->NumOfRows > 1)
    {
        misTrc(T_FLOW, 
               "No perms found and more than one non-perm - too general");
        sqlFreeResults(res);
        return (eINT_DEST_TOO_GENERAL);
    }

    /* We're golden...copy the information.. */
    memset(sub, 0, sizeof(SUB_INFO));
    strncpy(sub->lodnum, lodnum, LODNUM_LEN);
    strncpy(sub->subnum,
            sqlGetString(res, row, "subnum"),
            misTrimLen(sqlGetString(res, row, "subnum"), 
                       SUBNUM_LEN));
    sub->prmflg = sqlGetBoolean(res, row, "prmflg");
    sub->mvsflg = sqlGetBoolean(res, row, "mvsflg");
    sub->phyflg = sqlGetBoolean(res, row, "phyflg");
    sub->idmflg = sqlGetBoolean(res, row, "idmflg");
    misTrc(T_FLOW, "Found non-perm sub: %s", sub->subnum);
    sqlFreeResults(res);
    return (eOK);
}

static long sCreateGlomLoad(char *stoloc,
                            char *wh_id,
                            LOD_INFO * lod,
                            char *lodtag,
                            char *asset_typ,
                            char *srcloc,
                            char *srclod)
{
    char lodnum[LODNUM_LEN + 1];
    long ret_status;

    misTrc(T_FLOW, "Glomming new load");
    memset(lodnum, 0, sizeof(lodnum));
	
    /* SYI - custom lodnum generated for picktower when applicable*/
    RETURN_STRUCT   *resultData=NULL;
    mocaDataRes     *resultSet;
    mocaDataRow     *resultRow;
    char cmdbuffer[1000];

    sprintf(cmdbuffer, " get var lodnum pcktwr where ldsrcloc = '%s' and wh_id = '%s' ", srcloc, wh_id);

    ret_status = srvInitiateCommand(cmdbuffer, &resultData);
    if (ret_status != eOK)
    {
       misTrc(T_FLOW, "Unexpected error...calling get next number.");
       srvFreeMemory (SRVRET_STRUCT, resultData);
       appNextNum(NUMCOD_LODNUM, lodnum);
    } else {
       resultSet = srvGetResults (resultData);
       resultRow = sqlGetRow (resultSet);

       if (sqlGetBoolean(resultSet, resultRow, "pcktwrflg") == BOOLEAN_TRUE)
       {
          misTrc(T_FLOW, "lodnum number generation for picktower");
          appNextNum("pcktwr_lodnum", lodnum);
       } else {
				sqlFreeResults (resultSet);
                sprintf(cmdbuffer, " generate next number where numcod = 'lodnum' and srclod = '%s' ", srclod);
                ret_status = srvInitiateCommand(cmdbuffer, &resultData);
                resultSet = srvGetResults (resultData);
                resultRow = sqlGetRow (resultSet);
                //&lodnum = NULL;
                //&lodnum = sqlGetString(resultSet, resultRow, "nxtnum");
                //strncpy(lodnum, sqlGetString(resultSet, resultRow, "nxtnum"), LODNUM_LEN);
                //appNextNum(NUMCOD_LODNUM, lodnum);
                sprintf(lodnum,sqlGetString(resultSet, resultRow, "nxtnum"));
      }
       sqlFreeResults (resultSet);
    }
    /* SYI - END Custom */

    ret_status = 
    trnCreateLoad(lodnum, wh_id, stoloc, 0, 0, 1,
                  osGetVar(LESENV_DEVCOD) ? osGetVar(LESENV_DEVCOD) : "",
                  osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "",
                  lodtag, asset_typ);
    if (ret_status != eOK)
    {
        misTrc(T_FLOW, "Error creating new destination load.");
        return (ret_status);
    }
    misTrc(T_FLOW, "New load glommed - %s", lodnum);

    memset(lod, 0, sizeof(LOD_INFO));
    strncpy(lod->lodnum, lodnum, LODNUM_LEN);
    strncpy(lod->wh_id, wh_id, WH_ID_LEN);
    strncpy(lod->stoloc, stoloc, STOLOC_LEN);
    lod->prmflg = BOOLEAN_FALSE;
    lod->mvlflg = BOOLEAN_TRUE;

    return (eOK);
}


static long sGetDestLoad(char *stoloc,
                         char *wh_id,
                         LOD_INFO * lod,
                         long strategy,
                         char *lodtag,
                         char *asset_typ,
                         char *srcloc,
                         char *srclod)
{
    long            ret_status;
    char            buffer[1000];
    mocaDataRes    *res;
    mocaDataRow    *row;

    /* Simplest case of all is the never glom...go ahead and
       create... */
       
    if (strategy == GLOM_NEVER)
    {
        ret_status = sCreateGlomLoad(stoloc, wh_id, lod, lodtag, asset_typ, srcloc, srclod);
        return (ret_status);
    }

    /* Ok...so we need to know what is there...
       ideally, we attach to perm loads, so we get those first... */
    sqlSetSavepoint("pre_load_select_savepoint");
    sprintf(buffer,
        "select lodnum, wh_id, prmflg, mvlflg "
        "  from invlod "
        " where stoloc = '%s'"
        "   and wh_id  = '%s'"
        " order by decode(prmflg, %d, 1, 2)"
        " for update of lodnum",
        stoloc, 
        wh_id, 
        BOOLEAN_TRUE);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        if (strategy == GLOM_IF_EXISTS)
        {
            misTrc(T_FLOW, "No existing inventory to attach to...");
            sqlFreeResults(res);
            return (eINT_GLOM_EMPTY_DEST_FAILED);
        }

        /* At this point, strategy is either UNIQUE or ANY...in either
           case, create what we need... */
        ret_status = sCreateGlomLoad(stoloc, wh_id, lod, lodtag, asset_typ, srcloc, srclod); 
        sqlFreeResults(res);
        return (ret_status);
    }

    row = sqlGetRow(res);
    if (sqlGetBoolean(res, row, "prmflg") == BOOLEAN_TRUE)
    {
        sqlRollbackToSavepoint("pre_load_select_savepoint");
    }

    /* We have inventory structures existing in the location...
       see what we can do... */
    if (strategy == GLOM_ANY)
    {
        memset(lod, 0, sizeof(LOD_INFO));
        strncpy(lod->stoloc, stoloc, STOLOC_LEN);
        strncpy(lod->lodnum,
                sqlGetString(res, row, "lodnum"),
                misTrimLen(sqlGetString(res, row, "lodnum"), LODNUM_LEN));
        strncpy(lod->wh_id,
                sqlGetString(res, row, "wh_id"),
                misTrimLen(sqlGetString(res, row, "wh_id"), WH_ID_LEN));
        lod->prmflg = sqlGetBoolean(res, row, "prmflg");
        lod->mvlflg = sqlGetBoolean(res, row, "mvlflg");
        misTrc(T_FLOW, "Strategy is ANY, grabbing load: %s", lod->lodnum);
        sqlFreeResults(res);
        return (eOK);
    }

    /* From here, we're left with UNIQUE or EXISTS - in both cases,
       if we've got a prmflg=1 load, then we're good to go...
       otherwise, we have to check the row count... */
    if (sqlGetLong(res, row, "prmflg") == BOOLEAN_TRUE)
    {
        memset(lod, 0, sizeof(LOD_INFO));
        strncpy(lod->stoloc, stoloc, STOLOC_LEN);
        strncpy(lod->lodnum,
                sqlGetString(res, row, "lodnum"),
                misTrimLen(sqlGetString(res, row, "lodnum"), LODNUM_LEN));
        strncpy(lod->wh_id,
                sqlGetString(res, row, "wh_id"),
                misTrimLen(sqlGetString(res, row, "wh_id"), WH_ID_LEN));
        lod->prmflg = sqlGetBoolean(res, row, "prmflg");
        lod->mvlflg = sqlGetBoolean(res, row, "mvlflg");
        misTrc(T_FLOW, "Strategy is unique/exists, prmlod=%s", lod->lodnum);
        sqlFreeResults(res);
        return (eOK);
    }
    if (res->NumOfRows > 1)
    {
        misTrc(T_FLOW, "No perms found and more than one non-perm - too general");
        sqlFreeResults(res);
        return (eINT_DEST_TOO_GENERAL);
    }

    /* We're golden...copy the information.. */
    memset(lod, 0, sizeof(LOD_INFO));
    strncpy(lod->stoloc, stoloc, STOLOC_LEN);
    strncpy(lod->lodnum,
            sqlGetString(res, row, "lodnum"),
            misTrimLen(sqlGetString(res, row, "lodnum"), LODNUM_LEN));
    strncpy(lod->wh_id,
            sqlGetString(res, row, "wh_id"),
            misTrimLen(sqlGetString(res, row, "wh_id"), WH_ID_LEN));
    lod->prmflg = sqlGetBoolean(res, row, "prmflg");
    lod->mvlflg = sqlGetBoolean(res, row, "mvlflg");
    misTrc(T_FLOW, "Found non-perm load: %s", lod->lodnum);
    sqlFreeResults(res);
    return (eOK);
}


static long sGetAreaGlomStrategy(char *arecod)
{
    mocaDataRow    *row;
    mocaDataRes    *res;
    char           *sp;
    long            cmp_val;

    /* If we have an override strategy, send that back */

    if (Policies.GLOMOverride != GLOM_NO_OVRRDE)
    {
        misTrc (T_FLOW, "Using glom_override strategy");
        return (Policies.GLOMOverride);
    }

    misTrim(arecod);

    sp = NULL;
    res = Policies.GlomRes;
    for (row = sqlGetRow(res); sp == NULL && row; row = sqlGetNextRow(row))
    {
        cmp_val = misCiStrncmp(sqlGetString(res, row, "rtstr1"),
                               arecod, ARECOD_LEN);

        if (cmp_val == 0)
            sp = sqlGetString(res, row, "polval");
        else if (cmp_val > 0)    /* we've passed it... */
            sp = POLVAL_ATTACH_IF_UNIQUE;
    }

    if (sp)
    {
        if (misCiStrncmp(sp, POLVAL_ATTACH_TO_ANY,
                 strlen(POLVAL_ATTACH_TO_ANY)) == 0)
            return (GLOM_ANY);
        else if (misCiStrncmp(sp, POLVAL_ATTACH_IF_EXISTS,
                      strlen(POLVAL_ATTACH_IF_EXISTS)) == 0)
            return (GLOM_IF_EXISTS);
        else if (misCiStrncmp(sp, POLVAL_ATTACH_IF_UNIQUE,
                      strlen(POLVAL_ATTACH_IF_UNIQUE)) == 0)
            return (GLOM_IF_UNIQUE);
        else if (misCiStrncmp(sp, POLVAL_NEVER_ATTACH,
                      strlen(POLVAL_NEVER_ATTACH)) == 0)
            return (GLOM_NEVER);
    }

    /* no policy match - return default... */
    return (GLOM_IF_UNIQUE);
}
static long sCreateDestLoad(char *dstloc,
                            char *dstlod,
                            char *wh_id,
                            LOD_INFO * lod,
                            LOC_INFO * loc,
                            ARE_INFO * are,
                            char *lodtag,
                            char *asset_typ)
{
    long            ret_status;

    ret_status = trnSelectInfoByLoc(dstloc,
                                    loc,
                                    are,
                                    wh_id);

    if (ret_status != eOK)
    {
        misTrc(T_FLOW, "Destination location is bad for load creation. - %ld",
                ret_status);
        return (eINT_DSTLOC_NOT_FOUND);
    }


    /* At this point, we have verified that our destloc is good and 
       we have all the information loaded for it.  Now we simply must
       create a load at the destination and load the info for it... */

    misTrc(T_FLOW, "Calling trnCreateLoad");
    ret_status = 
    trnCreateLoad(dstlod, wh_id, dstloc, 0, 0, 1,
                  osGetVar(LESENV_DEVCOD) ? osGetVar(LESENV_DEVCOD) : "",
                  osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "",
                  lodtag, asset_typ);
    if (ret_status != eOK)
    {
        misTrc(T_FLOW, "Error creating new destination load.");
        return (ret_status);
    }

    memset(lod, 0, sizeof(LOD_INFO));
    strncpy(lod->lodnum, dstlod, LODNUM_LEN);
    strncpy(lod->wh_id, wh_id, WH_ID_LEN);
    strncpy(lod->stoloc, dstloc, STOLOC_LEN);
    lod->prmflg = BOOLEAN_FALSE;
    lod->mvlflg = BOOLEAN_TRUE;

    return (eOK);
}


static long sCreateDestSub(char *dstloc,
                           char *dstlod,
                           char *wh_id,
                           char *dstsub,
                           SUB_INFO * sub,
                           LOD_INFO * lod,
                           LOC_INFO * loc,
                           ARE_INFO * are,
                           char *lodtag,
                           moca_bool_t phyflg,
                           char *subtag,
                           char *asset_typ,
                           char *sub_asset_typ,
                           char *srcloc)
{
    long            ret_status;
    long            strategy;


    if (!dstlod || misTrimLen(dstlod, LODNUM_LEN) == 0)
    {
        /* They didn't give us a dstlod, attempt to possibly
           create one if the glomming policies allow...first have
           to get the area code we are heading to.. */
        ret_status = trnSelectInfoByLoc(dstloc,
                                        loc,
                                        are,
                                        wh_id);

        if (ret_status != eOK)
        {
            misTrc(T_FLOW, 
                   "Destination location is bad for load creation. - %ld",
                   ret_status);
            return (eINT_DSTLOC_NOT_FOUND);
        }
        strategy = sGetAreaGlomStrategy(are->arecod);
        /* At end of switch, if still in routine, we should
           have a dstlod struct filled in... */

        ret_status = sGetDestLoad(dstloc,
                                  wh_id,
                                  lod,
                                  strategy,
                                  lodtag,
                                  asset_typ,
                                  srcloc);
        if (ret_status != eOK)
            return (ret_status);
    }
    else
    {
        /* Got a dest load - attempt to load info on it.. */
        ret_status = trnSelectInfoByLoad(dstlod,
                                         lod,
                                         loc,
                                         are,
                                         wh_id);
        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "dstloc not found - attempting create");
            ret_status = sCreateDestLoad(dstloc,
                                         dstlod,
                                         wh_id,
                                         lod,
                                         loc,
                                         are,
                                         lodtag,
                                         asset_typ);
            if (ret_status != eOK)
            {
                misTrc(T_FLOW, 
                       "Failed to create even the dstloc, bailing out");
                return (ret_status);
            }
        }
    }
    /* At this point, we have a dest load and all the information
       for it and above....just need to create a sub, and then we're out
       of here... */
    ret_status = 
    trnCreateSub(dstsub, lod->lodnum, 0, phyflg, 0, 1,
                 osGetVar(LESENV_DEVCOD) ? osGetVar(LESENV_DEVCOD) : "", 
                 osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "",
                 subtag, sub_asset_typ);

    if (ret_status != eOK)
    {
        misTrc(T_FLOW, "Error creating new destination sub... ");
        return (ret_status);
    }
    memset(sub, 0, sizeof(SUB_INFO));
    strncpy(sub->subnum, dstsub, SUBNUM_LEN);
    strncpy(sub->lodnum, lod->lodnum, LODNUM_LEN);
    sub->prmflg = BOOLEAN_FALSE;
    sub->phyflg = phyflg;
    sub->idmflg = BOOLEAN_FALSE;
    sub->mvsflg = BOOLEAN_TRUE;
    return (ret_status);

}

static long sLoadMovePolicies(char *wh_id)
{
    mocaDataRes    *pres;
    mocaDataRow    *prow;
    long            ret_status;
    char            buffer[1000];
    static char     cur_wh_id[WH_ID_LEN+1];

    if (Policies.Loaded == 1) 
    {
        /* 
         * If Policies are loaded for the right warehouse, all is well.
         * If the warehouse ids are different we need to reload the policies
         * for the new warehouse.
         */
        if (misTrimStrncmp(&cur_wh_id[0], wh_id, WH_ID_LEN) == 0)
        {
            return (eOK);
        }
    }
    
    Policies.Loaded = 1;
    misTrimcpy(cur_wh_id,wh_id,WH_ID_LEN);

    pres = NULL;
    /* First get the Glomming policies.... */

    /* We order by rtstr1 (arecod) so that we know
       when we can stop looking in the list for
       an area that isn't there... */
    sprintf(buffer,
	    "select polval, rtstr1 "
	    "  from poldat_view "
	    " where polcod = '%s' "
	    "	and polvar = '%s' "
	    "	and polval in ('%s','%s','%s','%s')"
	    "	and rtstr1 is not null "
	    "	and wh_id  ='%s'  "
	    " order by rtstr1",
	    POLCOD_MOVE_INVENTORY,
	    POLVAR_ATTACHMENT_STRATEGY,
	    POLVAL_NEVER_ATTACH, POLVAL_ATTACH_IF_UNIQUE,
	    POLVAL_ATTACH_IF_EXISTS, POLVAL_ATTACH_TO_ANY,
	    wh_id);

    ret_status = sqlExecStr(buffer, &pres);
    if (ret_status == eOK)
    {
        /* Go through and misTrim all our values - that helps 
           later on when we do strcmp.... */
        for (prow = sqlGetRow(pres); prow;
             misTrim(sqlGetString(pres, prow, "rtstr1")),
             misTrim(sqlGetString(pres, prow, "polval")),
             prow = sqlGetNextRow(prow)) ;
        Policies.GlomRes = pres;
    }
    else if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        misTrc(T_FLOW, "No glomming policies loaded - using defaults");
        sqlFreeResults(pres);
    }
    else
    {
        misLogError("Error loading glomming policies: %ld", ret_status);
        return (ret_status);
    }

    pres = NULL;

    sprintf(buffer,
	    "select polval, rtstr1 "
	    "  from poldat_view "
	    " where polcod = '%s' "
	    "	and polvar = '%s' "
	    "	and polval = '%s' "
	    "	and rtstr1 = '%s'"
	    "	and wh_id  = '%s'",
	    POLCOD_MOVE_INVENTORY,
	    POLVAR_MISC,
	    POLVAL_CREATE_DEST_IF_NEEDED,
	    YES_STR,
	    wh_id);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
    {
        Policies.CreateDestination = 1;
    }
    else if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        Policies.CreateDestination = 0;
    }
    else
    {
        misLogError("Error loading create dest policies: %ld", ret_status);
    }
    return (eOK);
}

/*
 *  Returns eOK if all arguments are OK...some error code otherwise...
 *
 */
static long sValidateArgs(char *srcloc, char *srclod,
                          char *srcsub, char *srcdtl,
                          char *dstloc, char *dstlod,
                          char *dstsub, long srcqty)
{

    /*
     * This routine performs some basic sanity checking on the 
     * arguments passed to intMoveInventory...
     * 
     * There are two basic types of moves:
     *   1.  Quantity moves where a specific 
     *       quantity is from one inventory
     *       item to another.  All of these
     *       moves involve moving an invdtl
     *       around.  Anytime the srcqty > 0,
     *       this type of move is performed.
     *   2.  Object moves.  This is a move of
     *       an entire inventory object w/o
     *       regard to it's details.  Moves 
     *       of this type involve taking the
     *       source object and attaching it
     *       to a new parent.  In order for
     *       these to be valid, the destination
     *       must not be specified for the
     *       same level of object and the parent
     *       object must be different.  I.e. on
     *       a LOAD move, there can be no dstlod
     *       specified and dstloc != srcloc.
     */
    if (strlen(srcdtl))
    {
    /* We have a source detail...the srcsub 
       should be different than the dstsub.... */
        if (strncmp(dstsub, srcsub, SUBNUM_LEN) == 0)
            return (eINT_DTL_TO_SAMESUB);
        else
            return (eOK);
    }

    if (strlen(srcsub))
    {
        /* We have a source sublod...now check if the */
        /* destination is different...                */

        if (strlen(dstsub))
        {
            if (srcqty > 0)    /* If a qty move, subs must be different */
            {
                if (strcmp(dstsub, srcsub) == 0)
                    return (eINT_SAMESUBS_WITH_QTY);
                else
                    return (eOK);
            }
            else /* Else it's an object move...can't have a dstsub */
                return (eINT_SUB_TO_SUB);
        }
        /* We didn't get a dstsub...then either the load or
           the locations must be different...if they aren't,
           then things aren't specific enough...  */

        if (strlen(dstlod))
        {
            if (strcmp(dstlod, srclod) == 0)
                return (eINT_LOC_NOT_DIFFERENT);
            else
                return (eOK);
        }
        if (strcmp(dstloc, srcloc) == 0)
            return (eINT_LOC_NOT_DIFFERENT);
        else
            return (eOK);
    }

    if (strlen(srclod))
    {

        if (strlen(dstlod))
        {
            if (srcqty > 0)
            {
                if (strcmp(dstlod, srclod) == 0)
                    return (eINT_SAMELOADS_WITH_QTY);
                else
                    return (eOK);
            }
            return (eINT_LOAD_TO_LOAD);
        }
        if (strcmp(dstloc, srcloc) == 0)
        {
            misTrc(T_FLOW, "Load is already at dest...");
            return (eINT_LOAD_ALREADY_THERE);
        }
        else
            return (eOK);
    }

    if (strlen(srcloc))
    {
        if (srcqty > 0)
        {
            if (strcmp(srcloc, dstloc) == 0)
                return (eINT_SAMELOCS_WITH_QTY);
            else
                return (eOK);
        }
        if (strcmp(srcloc, dstloc) == 0)
            return (eINT_LOC_NOT_DIFFERENT);
        else
            return (eOK);
    }
    return (eINT_NO_SOURCE);
}

RETURN_STRUCT *sSetupReturn(char *src_arecod,
                            moca_bool_t  src_praflg,
                            char *src_stoloc,
                            char *src_lodnum,
                            char *src_subnum,
                            char *src_dtlnum,
                            long  srcqty,
                            double src_catch_qty,
                            char *dst_arecod,
                            moca_bool_t  dst_praflg,
                            char *dst_stoloc,
                            char *dst_lodnum,
                            char *dst_subnum,
                            char *trknum,
                            char *sesnum,
                            char *actcod,
                            char *movref,
                            moca_bool_t  src_cipflg,
                            moca_bool_t  dst_cipflg,
                            char *src_pckcod,
                            char *dst_sublist,
                            char *src_dtllist,
                            char *dst_pckcod,
                            char *xdkref,
                            long xdkqty,
                            moca_bool_t src_put_to_sto_flg,
                            moca_bool_t dst_put_to_sto_flg,
                            char *wh_id)
{
    RETURN_STRUCT  *CurPtr;

    long            i;
    char            tmp_sub[SUBNUM_LEN + 1];
    char            tmp_dtl[DTLNUM_LEN + 1];
    
    CurPtr = srvResultsInit(eOK,
                             "srcare",     COMTYP_CHAR,    ARECOD_LEN,
                             "srcpra",     COMTYP_BOOLEAN, sizeof(long),
                             "srcloc",     COMTYP_CHAR,    STOLOC_LEN,
                             "lodnum",     COMTYP_CHAR,    LODNUM_LEN,
                             "subnum",     COMTYP_CHAR,    SUBNUM_LEN,
                             "dtlnum",     COMTYP_CHAR,    DTLNUM_LEN,
                             "untqty",     COMTYP_INT,     sizeof(int),
                             "catch_qty",  COMTYP_FLOAT,   sizeof(double),
                             "dstare",     COMTYP_CHAR,    ARECOD_LEN,
                             "dstpra",     COMTYP_BOOLEAN, sizeof(long),
                             "dstloc",     COMTYP_CHAR,    STOLOC_LEN,
                             "dstlod",     COMTYP_CHAR,    LODNUM_LEN,
                             "dstsub",     COMTYP_CHAR,    SUBNUM_LEN,
                             "trknum",     COMTYP_CHAR,    STOLOC_LEN,
                             "sesnum",     COMTYP_CHAR,    MOVREF_LEN,
                             "actcod",     COMTYP_CHAR,    ACTCOD_LEN,
                             "movref",     COMTYP_CHAR,    MOVREF_LEN,
                             "src_cipflg", COMTYP_BOOLEAN, sizeof(long),
                             "dst_cipflg", COMTYP_BOOLEAN, sizeof(long),
                             "pckcod",     COMTYP_CHAR,    PCKCOD_LEN,
                             "dst_pckcod", COMTYP_CHAR,    PCKCOD_LEN,
                             "xdkref",     COMTYP_CHAR,    XDKREF_LEN,
                             "xdkqty",     COMTYP_INT,     sizeof(int),
                             "src_put_to_sto_flg", COMTYP_BOOLEAN, sizeof(long),
                             "dst_put_to_sto_flg", COMTYP_BOOLEAN, sizeof(long),
                             "wh_id",      COMTYP_CHAR,     WH_ID_LEN,
                             NULL);

    if (!dst_sublist)
    {
        srvResultsAdd(CurPtr,
                      src_arecod,
                      src_praflg,
                      src_stoloc,
                      src_lodnum,
                      src_subnum,
                      src_dtlnum,
                      srcqty,
                      src_catch_qty,
                      dst_arecod,
                      dst_praflg,
                      dst_stoloc,
                      dst_lodnum,
                      dst_subnum,
                      trknum,
                      sesnum,
                      actcod,
                      movref,
                      src_cipflg,
                      dst_cipflg,
                      src_pckcod,
                      dst_pckcod,
                      xdkref,
                      xdkqty,
                      src_put_to_sto_flg,
                      dst_put_to_sto_flg,
                      wh_id);
    }
    else
    {
        for (i = 0; i < (int) (strlen(dst_sublist) / SUBNUM_LEN); i++)
        {
            memset(tmp_sub, 0, sizeof(tmp_sub));
            memset(tmp_dtl, 0, sizeof(tmp_dtl));
            strncpy(tmp_sub,
                &dst_sublist[i * SUBNUM_LEN], SUBNUM_LEN);
            strncpy(tmp_dtl,
                &src_dtllist[i * DTLNUM_LEN], DTLNUM_LEN);

                srvResultsAdd(CurPtr, 
                              src_arecod,
                              src_praflg,
                              src_stoloc,
                              src_lodnum,
                              src_subnum,
                              tmp_dtl,
                              srcqty,
                              src_catch_qty,
                              dst_arecod,
                              dst_praflg,
                              dst_stoloc,
                              dst_lodnum,
                              tmp_sub,
                              trknum,
                              sesnum,
                              actcod,
                              movref,
                              src_cipflg,
                              dst_cipflg,
                              src_pckcod,
                              dst_pckcod,
                              xdkref,
                              xdkqty,
                              src_put_to_sto_flg,
                              dst_put_to_sto_flg,
                              wh_id);
        }
    }
    return (CurPtr);
}
static long sIntMoveInv(char *srcloc_i, char *srclod_i, char *srcsub_i,
                        char *srcdtl_i, long  srcqty,   
                        double src_catch_qty_i, char *srcref_i,
                        char *dstloc_i, char *dstlod_i, char *dstsub_i,
                        char *usr_id_i, char *actcod_i, char *movref_i,
                        char *devcod_i, RETURN_STRUCT ** CurPtr, 
                        char *prtnum_i, char *prt_client_id_i,
                        char *orgcod_i, char *revlvl_i, char *lotnum_i,
                        char *invsts_i, long  untcas_i, char *spcind_i,
                        long  untpak_i, moca_bool_t *newflg_i, 
                        char *ship_line_id_i, char *trknum_i, 
                        char *glom_override_i, char *wh_id,
                        char *lodtag_i, char *subtag_i, moca_bool_t *phyflg_i,
                        char *asset_typ_i, char *sub_asset_typ_i, 
                        moca_bool_t *ign_restrict_i,
                        char *distro_id_i, char *supnum_i)
{
    RETURN_STRUCT *CmdRes = NULL;

    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;
         
         
    long  gotsrcsub, gotsrcdtl, gotsrclod, gotsrcloc;
    long  gotdstsub, gotdstlod, gotdstloc, gotprtnum;
    long  gotprt_client_id;

    long  moveby;
    long  need_detail;
    long  ret_status;
    long  CreateDestination;
    char  Key[100];

    double src_catch_qty;
    double pck_catch_qty;

    char  srcloc[STOLOC_LEN + 1];
    char  srclod[LODNUM_LEN + 1];
    char  srcsub[SUBNUM_LEN + 1];
    char  srcdtl[DTLNUM_LEN + 1], dstloc[STOLOC_LEN + 1];
    char  dstlod[LODNUM_LEN + 1];
    char  dstsub[SUBNUM_LEN + 1], usr_id[USR_ID_LEN + 1];
    char  actcod[ACTCOD_LEN + 1];
    char  devcod[DEVCOD_LEN + 1];
    char  movref[MOVREF_LEN + 1], srcref[WRKREF_LEN + 1];
    char  spcind[SPCIND_LEN + 1];
    char  asset_typ[ASSET_TYP_LEN + 1];
    char  sub_asset_typ[ASSET_TYP_LEN + 1];

    char  new_dtlnum[DTLNUM_LEN + 1];
    char *additional_new_dtlnum_p = NULL;
    char  dtlnum[DTLNUM_LEN + 1];
    char  prtnum[PRTNUM_LEN + 1];
    char  prt_client_id[CLIENT_ID_LEN + 1];
    char  prtnumIn[PRTNUM_LEN + 1];
    char  prt_client_idIn[CLIENT_ID_LEN + 1];
    char  orgcod[ORGCOD_LEN + 1];
    char  orgcodIn[ORGCOD_LEN + 1];
    char  revlvl[REVLVL_LEN + 1];
    char  supnum[SUPNUM_LEN + 1];
    char  revlvlIn[REVLVL_LEN + 1];
    char  supnumIn[SUPNUM_LEN + 1];
    char  lotnum[LOTNUM_LEN + 1];
    char  lotnumIn[LOTNUM_LEN + 1];
    char  invsts[INVSTS_LEN + 1];
    char  invstsIn[INVSTS_LEN + 1];
    char  ship_line_id[SHIP_LINE_ID_LEN + 1];
    char  ship_line_idIn[SHIP_LINE_ID_LEN + 1];
    char  trknum[TRKNUM_LEN+1];
    char  lodtag[LODTAG_LEN+1];
    char  subtag[SUBTAG_LEN+1];
    char  distro_id[DISTRO_ID_LEN + 1];
    char  distro_idIn[DISTRO_ID_LEN + 1];

    long  untcas;
    long  untpak;
    short moveByPartInfo = FALSE;
    long  QtyOnLod, QtyOnSub, QtyOnDtl;
    double CatchQtyOnLod, CatchQtyOnSub, CatchQtyOnDtl;
    char  *dtllist_p, *sublist_p,*dtlnumptr;
    char  xdkref[XDKREF_LEN + 1];
    long  xdkqty;
    moca_bool_t revflg;
    moca_bool_t  idmflg;
    moca_bool_t phyflg = BOOLEAN_FALSE;
    /* Ignore Restrictions Flag - Indicates if move restrictions should be ignored*/
    /* true (1) = ignore restrictions, false (0) = check move restrictions */
    moca_bool_t ign_restrict = BOOLEAN_FALSE;
    char  glom_override[POLVAL_LEN + 1];
    char  invtid[INVTID_LEN + 1];
    char  lodlvl[LODLVL_LEN + 1];
    char  buffer[1000];
    char cmbcod[CMBCOD_LEN];

    ARE_INFO        src_are, dst_are, tmp_are;
    LOC_INFO        src_loc, dst_loc, tmp_loc;
    LOD_INFO        src_lod, dst_lod;
    SUB_INFO        src_sub, dst_sub;
    DTL_INFO        src_dtl;

    ret_status = sLoadMovePolicies(wh_id);
    if (ret_status != eOK)
    {
        misLogError("Aborting move due to bad policy load.");
        return (ret_status);
    }

    /*
     * If a glomming override strategy was passed in, then set it on the
     * policy structure.  Because the policy structure is static, we need
     * to make sure we set the override strategy to 0 if not passed in, so that
     * an override from one move inventory doesn't carry to another call to
     * move inventory.  If the override strategy is not recognize, then don't
     * use it.
     */

    misTrc (T_FLOW, "Resetting glom override strategy to nothing");
    Policies.GLOMOverride = GLOM_NO_OVRRDE;
    if (glom_override_i && misTrimLen (glom_override_i, POLVAL_LEN))
    {
        misTrc (T_FLOW, "glom override sent in as %s", glom_override_i);


        misTrimcpy (glom_override, glom_override_i, POLVAL_LEN);

        if (misTrimStrncmp(glom_override, POLVAL_ATTACH_TO_ANY, 
                           POLVAL_LEN) == 0)
            Policies.GLOMOverride = GLOM_ANY;
        else if (misTrimStrncmp(glom_override, POLVAL_ATTACH_IF_EXISTS,
                                POLVAL_LEN) == 0)
            Policies.GLOMOverride = GLOM_IF_EXISTS;
        else if (misTrimStrncmp(glom_override, POLVAL_ATTACH_IF_UNIQUE,
                                POLVAL_LEN) == 0)
            Policies.GLOMOverride = GLOM_IF_UNIQUE;
        else if (misTrimStrncmp(glom_override, POLVAL_NEVER_ATTACH,
                                POLVAL_LEN) == 0)
            Policies.GLOMOverride = GLOM_NEVER;
        else
        {
            misTrc (T_FLOW, "glom override strategy not recognized.  "
                            "Ignoring.");
        }
    }

    CreateDestination = Policies.CreateDestination;
    if (newflg_i)
    {
        if (*newflg_i == BOOLEAN_TRUE)
            CreateDestination = 1;
        else 
            CreateDestination = 0;
    }

    memset(&src_are, 0, sizeof(src_are));
    memset(&dst_are, 0, sizeof(dst_are));
    memset(&src_loc, 0, sizeof(src_loc));
    memset(&dst_loc, 0, sizeof(dst_loc));
    memset(&src_lod, 0, sizeof(src_lod));
    memset(&dst_lod, 0, sizeof(dst_lod));
    memset(&src_sub, 0, sizeof(src_sub));
    memset(&dst_sub, 0, sizeof(dst_sub));
    memset(&src_dtl, 0, sizeof(src_dtl));

    memset(dtlnum, 0, sizeof(dtlnum));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(orgcod, 0, sizeof(orgcod));
    memset(revlvl, 0, sizeof(revlvl));
    memset(supnum, 0, sizeof(supnum));
    memset(lotnum, 0, sizeof(lotnum));
    memset(invsts, 0, sizeof(invsts));
    memset(ship_line_id, 0, sizeof(ship_line_id));
    memset(distro_id, 0, sizeof(distro_id));
    memset(prtnumIn, 0, sizeof(prtnumIn));
    memset(prt_client_idIn, 0, sizeof(prt_client_idIn));
    memset(orgcodIn, 0, sizeof(orgcodIn));
    memset(revlvlIn, 0, sizeof(revlvlIn));
    memset(supnumIn, 0, sizeof(supnumIn));
    memset(lotnumIn, 0, sizeof(lotnumIn));
    memset(invstsIn, 0, sizeof(invstsIn));
    memset(ship_line_idIn, 0, sizeof(ship_line_idIn));
    memset(distro_idIn, 0, sizeof(distro_idIn));

    gotsrcsub = gotsrcdtl = gotsrcloc = gotsrclod = FALSE;
    gotdstsub = gotdstloc = gotdstlod = FALSE;

    QtyOnLod = QtyOnSub = QtyOnDtl = 0;
    CatchQtyOnLod = CatchQtyOnSub = CatchQtyOnDtl = 0.0;

    /* This little firewall is here just so we can be reasonably
       sure what we are dealing with.... */
    memset(srcloc, 0, sizeof(srcloc));
    memset(srclod, 0, sizeof(srclod));
    memset(srcsub, 0, sizeof(srcsub));
    memset(srcdtl, 0, sizeof(srcdtl));
    memset(dstloc, 0, sizeof(dstloc));
    memset(dstlod, 0, sizeof(dstlod));
    memset(dstsub, 0, sizeof(dstsub));
    memset(usr_id, 0, sizeof(usr_id));
    memset(actcod, 0, sizeof(actcod));
    memset(movref, 0, sizeof(movref));
    memset(srcref, 0, sizeof(srcref));
    memset(devcod, 0, sizeof(devcod));
    memset(spcind, 0, sizeof(spcind));
    memset(xdkref, 0, sizeof(xdkref));
    memset(trknum, 0, sizeof(trknum));
    memset(invtid, 0, sizeof(invtid));
    memset(lodtag, 0, sizeof(lodtag));
    memset(subtag, 0, sizeof(subtag));
    memset(asset_typ, 0, sizeof(asset_typ));
    memset(sub_asset_typ, 0, sizeof(sub_asset_typ));
    memset(cmbcod, 0, sizeof(cmbcod));

    xdkqty=0;

    if (srcloc_i)
        strncpy(srcloc, srcloc_i, misTrimLen(srcloc_i, STOLOC_LEN));
    if (srclod_i)
        strncpy(srclod, srclod_i, misTrimLen(srclod_i, LODNUM_LEN));
    if (srcsub_i)
        strncpy(srcsub, srcsub_i, misTrimLen(srcsub_i, SUBNUM_LEN));
    if (srcdtl_i)
        strncpy(srcdtl, srcdtl_i, misTrimLen(srcdtl_i, DTLNUM_LEN));
    if (dstloc_i)
        strncpy(dstloc, dstloc_i, misTrimLen(dstloc_i, STOLOC_LEN));
    if (dstlod_i)
        strncpy(dstlod, dstlod_i, misTrimLen(dstlod_i, LODNUM_LEN));
    if (dstsub_i)
        strncpy(dstsub, dstsub_i, misTrimLen(dstsub_i, SUBNUM_LEN));

    if (usr_id_i)
        strncpy(usr_id, usr_id_i, misTrimLen(usr_id_i, USR_ID_LEN));
    if (devcod_i)
        strncpy(devcod, devcod_i, misTrimLen(devcod_i, DEVCOD_LEN));
    if (actcod_i)
        strncpy(actcod, actcod_i, misTrimLen(actcod_i, ACTCOD_LEN));
    if (trknum_i)
        strncpy(trknum, trknum_i, misTrimLen(trknum_i, TRKNUM_LEN));
    if (phyflg_i && (*phyflg_i == BOOLEAN_TRUE))
        phyflg = BOOLEAN_TRUE;
    if (ign_restrict_i && (*ign_restrict_i == BOOLEAN_TRUE))
        ign_restrict = BOOLEAN_TRUE;
    if (asset_typ_i)
        strncpy(asset_typ, asset_typ_i, 
            misTrimLen(asset_typ_i, ASSET_TYP_LEN));
    if (sub_asset_typ_i)
        strncpy(sub_asset_typ, sub_asset_typ_i, 
            misTrimLen(sub_asset_typ_i, ASSET_TYP_LEN));

    if (lodtag_i && misTrimLen(lodtag_i, LODTAG_LEN))
        misTrimcpy(lodtag, lodtag_i, LODTAG_LEN);
    if (subtag_i && misTrimLen(subtag_i, SUBTAG_LEN))
        misTrimcpy(subtag, subtag_i, SUBTAG_LEN);
    
    gotprtnum = 0;
    gotprt_client_id = 0;
    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
    {
        gotprtnum++;
        strncpy(prtnumIn, prtnum_i, misTrimLen(prtnum_i, PRTNUM_LEN));
    }
    if (prt_client_id_i && misTrimLen(prt_client_id_i, CLIENT_ID_LEN))
    {
        gotprt_client_id++;
        strncpy(prt_client_id, prt_client_id_i, CLIENT_ID_LEN);
    }

    if (appIsInstalled("THIRD-PARTY-LOGISTICS"))
    {
        if (gotprtnum && !gotprt_client_id)
        {
            /* OK...we have to be careful here..
               if we have 3PL installed, then we only want to call
               appGetClient if we also got a part number - otherwise, it
               could be a lod/sub/dtl move and we don't care about the
               prt_client_id, but appGetClient will whine if we go ahead 
               and call it blindly */
            ret_status = appGetClient(prt_client_id_i, prt_client_id);
            if (strlen(prt_client_id) == 0)
            {
                misTrc(T_FLOW, 
                       "We got a prtnum with no prt_client_id "
                       "in a 3PL env - error");
                return(eINVALID_ARGS);
            }
        }
    }
    else
    {
        ret_status = appGetClient(prt_client_id_i, prt_client_id);
        if (ret_status != eOK) 
        {    
            return (ret_status); 
        }    
    }
    
    strncpy(prt_client_idIn, prt_client_id,
            misTrimLen(prt_client_id, CLIENT_ID_LEN));
    
    misTrc(T_FLOW, " Part ->(%s) ClientIN->(%s) ClientOUT->(%s)",
           prtnum, prt_client_id_i?prt_client_id_i:"", prt_client_id);

    if (orgcod_i && misTrimLen(orgcod_i, ORGCOD_LEN))
        strncpy(orgcodIn, orgcod_i, misTrimLen(orgcod_i, ORGCOD_LEN));
    if (revlvl_i && misTrimLen(revlvl_i, REVLVL_LEN))
        strncpy(revlvlIn, revlvl_i, misTrimLen(revlvl_i, REVLVL_LEN));
    if (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN))
        strncpy(supnumIn, supnum_i, misTrimLen(supnum_i, SUPNUM_LEN));
    if (invsts_i && misTrimLen(invsts_i, INVSTS_LEN))
        strncpy(invstsIn, invsts_i, misTrimLen(invsts_i, INVSTS_LEN));
    if (lotnum_i && misTrimLen(lotnum_i, LOTNUM_LEN))
        strncpy(lotnumIn, lotnum_i, misTrimLen(lotnum_i, LOTNUM_LEN));
    if (ship_line_id_i && misTrimLen(ship_line_id_i, SHIP_LINE_ID_LEN))
        misTrimcpy(ship_line_idIn, ship_line_id_i, SHIP_LINE_ID_LEN);
    if (distro_id_i && misTrimLen(distro_id_i, DISTRO_ID_LEN))
        misTrimcpy(distro_idIn, distro_id_i, DISTRO_ID_LEN);

    /* We have to save the initial value that was passed in */
    src_catch_qty = src_catch_qty_i;
    pck_catch_qty = 0.0;

    if (srcqty < 0 || src_catch_qty < 0)
        return (eINT_NO_NEGATIVES);        

    if (movref_i && misTrimLen(movref_i, MOVREF_LEN))
        strncpy(movref, movref_i, misTrimLen(movref_i, MOVREF_LEN));
    else
        strncpy(movref, "INTERNAL", MOVREF_LEN);

    if (spcind_i && misTrimLen(spcind_i, SPCIND_LEN))
        strncpy(spcind, spcind_i, misTrimLen(spcind_i, SPCIND_LEN));
    
    /* PR 47894
     * If the source reference is not NULL and the input orgcod,
     * revlvl, invsts and lotnum is not existing, then using them
     * from pick work by this work reference.
     * Otherwise, when it do FindDetailByPartInfo, if only invsts
     * or orgcod is different, there will return more than 1 record.
     * It is not correct, we have to add these filter condition.
     */
    if (srcref_i && misTrimLen(srcref_i, WRKREF_LEN))
    {
        strncpy(srcref, srcref_i, misTrimLen(srcref_i, WRKREF_LEN));

        sprintf(buffer,
                "select p.orgcod, "
                "       p.revlvl, "
                "       p.supnum, "
                "       p.invsts, "
                "       p.lotnum, "
                "       (p.pck_catch_qty - p.app_catch_qty) pck_catch_qty, "
                "       p.cmbcod, "
                "       o.distro_id "
                "  from pckwrk p left outer join ord_line o "
                "    on p.ordnum = o.ordnum "
                "   and p.client_id = o.client_id "
                "   and p.ordlin = o.ordlin "
                "   and p.ordsln = o.ordsln "
                "   and p.wh_id = o.wh_id "
                " where p.wrkref = '%s' ",
                srcref);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status == eOK)
        {
            row = sqlGetRow(res);
            if (!strlen(orgcodIn))
            {
                misTrimcpy(orgcodIn, sqlGetString (res, row, "orgcod"),
                           ORGCOD_LEN);
            }
            if (!strlen(revlvlIn))
            {
                misTrimcpy(revlvlIn, sqlGetString (res, row, "revlvl"),
                           REVLVL_LEN);
            }
            if (!strlen(supnumIn))
            {
                misTrimcpy(supnumIn, sqlGetString (res, row, "supnum"),
                           SUPNUM_LEN);
            }
            if (!strlen(invstsIn))
            {
                misTrimcpy(invstsIn, sqlGetString (res, row, "invsts"),
                           INVSTS_LEN);
            }
            if (!strlen(lotnumIn))
            {
                misTrimcpy(lotnumIn, sqlGetString (res, row, "lotnum"),
                           LOTNUM_LEN);
            }
            if (!strlen(distro_idIn))
            {
                misTrimcpy(distro_idIn, sqlGetString(res, row, "distro_id"),
                           DISTRO_ID_LEN);
            }
            if(!sqlIsNull(res, row, "pck_catch_qty"))
            {
                pck_catch_qty = sqlGetFloat(res, row, "pck_catch_qty");
            }
            if(!sqlIsNull(res, row, "cmbcod"))
            {
                misTrimcpy(cmbcod, 
                    sqlGetString(res, row, "cmbcod"), 
                    CMBCOD_LEN);
            }
        }
        sqlFreeResults(res);
    }
    
    /* A move for a catch quantity was requested so we have to
     * move at least a single unit quantity even if we are 
     * creating it out of thin air.
     */
    if(src_catch_qty > 0 &&
       pck_catch_qty > 0)
    {
        srcqty = (srcqty <= 0 ? 1 : srcqty);
    }

    /* In the else section of this branch below, we used to 
       return eINT_SRC_TOO_GENERAL if the quantity was 0...
       while this is mostly valid, we instead change things to 
       just ignore the part number passed in so that if they've
       passed in something like a dtlnum, it has a chance of working... */

    if (strlen(prtnumIn) && srcqty > 0)
        moveByPartInfo = TRUE;
    else if (strlen(prtnumIn) && !srcqty)
        memset(prtnumIn, 0, sizeof(prtnumIn));

    
    /* Overpicking by catch quantity is not allowed */
    if(pck_catch_qty > 0 &&
       src_catch_qty > pck_catch_qty)
    {
       return (eOVERPICK_BY_CATCH_QTY_NOT_ALLOWED);
    }

    /* If we didn't get the usr_idber or device code, get it from the env... */
    if (strlen(usr_id) == 0 && osGetVar(LESENV_USR_ID))
        strncpy(usr_id, (char *) osGetVar(LESENV_USR_ID), USR_ID_LEN);
    if (strlen(devcod) == 0 && osGetVar(LESENV_DEVCOD))
        strncpy(devcod, (char *) osGetVar(LESENV_DEVCOD), DEVCOD_LEN);

    /* If the activity code was not passed in, set it to MOVE if 
     * srcref doesn't exists.  (Otherwise, the activity code is set to one
     * of the pick activity codes below. */

    if ( (!strlen(actcod)) && (!strlen(srcref)) )
        strcpy (actcod, ACTCOD_GENERIC_MOVE);
    
    /* For now, we hard code in non-serialized piece checks if we 
       break out by detail... */

    idmflg = BOOLEAN_FALSE;

    /********* Verify source information...   *********/

    /* 
     *        The approach here is that we start at the lowest level of
     *  detail that was passed in to us and attempt to retrieve
     *  all the information needed by the move as well as to
     *  verify the parameters that were passed in for sanity.  Each one
     *  of the transactions that gets data is going to lock it
     *  as well...this is to guarantee that no data changes during
     *  the course of this transaction...
     */

    /*
     * Also, the trnSelect... functions will store wh_id in those structures,
     * LOC_INFO, ARE_INFO, LOD_INFO... so we don't need to pass wh_id separately. 
     * We can get wh_id from the structure whenever we need it.
     */ 
    
    if (strlen(srcdtl))
    {
        /* Got a detail - try to select everything for the detail and 
           verify it against arguments passed in...   */

        moveby = LVL_DETAIL;
        gotsrcdtl = gotsrcsub = gotsrclod = gotsrcloc = TRUE;
        if (!strlen(actcod))
            strcpy(actcod, ACTCOD_PIECE_PICK);
        ret_status = trnSelectInfoByDtl(srcdtl,
                        &src_dtl,
                        &src_sub,
                        &src_lod,
                        &src_loc,
                        &src_are);


        /* Check status */
        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "Error (%d) in trnSelectInfoByDtl...", ret_status);
            return (eINT_SRCDTL_NOT_FOUND);
        }

        if (srcloc && misTrimLen(srcloc, STOLOC_LEN))
            if (misTrimStrncmp(srcloc, src_loc.stoloc, STOLOC_LEN) != 0 ||
                misTrimStrncmp(wh_id,  src_loc.wh_id,  WH_ID_LEN)  != 0)
                return (eINT_BAD_SRCLOC);

        if (srclod && misTrimLen(srclod, LODNUM_LEN))
            if (misTrimStrncmp(srclod, src_lod.lodnum, LODNUM_LEN) != 0)
                return (eINT_BAD_SRCLOD);

        if (srcsub && misTrimLen(srcsub, SUBNUM_LEN))
            if (misTrimStrncmp(srcsub, src_sub.subnum, SUBNUM_LEN) != 0)
                return (eINT_BAD_SRCSUB);

        /* If there was a requested pick catch quantity we should validate
         * that the location has enough catch quantity available otherwise
         * we may still want to apply a given catch quantity to the picked
         * inventory without validating.
         */
        if ((srcqty && srcqty > src_dtl.untqty) ||
            (src_catch_qty && 
             src_catch_qty > src_dtl.catch_qty && 
             pck_catch_qty > 0))
            return (eINT_NOT_ENOUGH_QTY);

        if ((srcqty && srcqty < src_dtl.untqty)
            ||
            (src_catch_qty &&
             src_catch_qty < src_dtl.catch_qty))
        {
            moveby = LVL_DETAIL_PARTIAL;
            if (!strlen(actcod))
                strcpy(actcod, ACTCOD_PIECE_PICK);
        }
        else
        {
            srcqty = 0;    /* Means we will take the whole detail so let us not
                   waste a make detail. */
        }

        strncpy(prtnum, misTrim(src_dtl.prtnum), PRTNUM_LEN);
        strncpy(prt_client_id, misTrim(src_dtl.prt_client_id), CLIENT_ID_LEN);
        strncpy(orgcod, misTrim(src_dtl.orgcod), ORGCOD_LEN);
        strncpy(revlvl, misTrim(src_dtl.revlvl), REVLVL_LEN);
        strncpy(supnum, misTrim(src_dtl.supnum), SUPNUM_LEN);
        strncpy(lotnum, misTrim(src_dtl.lotnum), LOTNUM_LEN);
        strncpy(invsts, misTrim(src_dtl.invsts), INVSTS_LEN);
        misTrimcpy(ship_line_id, src_dtl.ship_line_id, SHIP_LINE_ID_LEN);
        misTrimcpy(distro_id, src_dtl.distro_id, DISTRO_ID_LEN);
        untcas = src_dtl.untcas;
        untpak = src_dtl.untpak;

        if (moveByPartInfo)
        {
            if (strncmp(prtnum, prtnumIn, PRTNUM_LEN))
                return (eINT_SRC_IS_EMPTY);
            if (strncmp(prt_client_id, prt_client_idIn, CLIENT_ID_LEN))
                return (eINT_SRC_IS_EMPTY);
            if (strlen(orgcodIn) && strncmp(orgcod, orgcodIn, ORGCOD_LEN))
                return (eINT_SRC_IS_EMPTY);
            if (strlen(revlvlIn) && strncmp(revlvl, revlvlIn, REVLVL_LEN))
                return (eINT_SRC_IS_EMPTY);
            if (strlen(supnumIn) && strncmp(supnum, supnumIn, SUPNUM_LEN))
                return (eINT_SRC_IS_EMPTY);
            if (strlen(lotnumIn) && strncmp(lotnum, lotnumIn, LOTNUM_LEN))
                return (eINT_SRC_IS_EMPTY);
            if (strlen(invstsIn) && strncmp(invsts, invstsIn, INVSTS_LEN))
                return (eINT_SRC_IS_EMPTY);
            if (untcas_i > 0 && untcas_i != untcas)
                return (eINT_SRC_IS_EMPTY);
            if (untpak_i > 0 && untpak_i != untpak)
                return (eINT_SRC_IS_EMPTY);
            if (strlen(ship_line_idIn) && 
                strncmp(ship_line_id, ship_line_idIn, SHIP_LINE_ID_LEN))
                return (eINT_SRC_IS_EMPTY);

            /* if move by Part Info, distro_id and distro_idIn
             * should be exactly matching
             */
            if (strncmp(distro_id, distro_idIn, DISTRO_ID_LEN))
                return (eINT_SRC_IS_EMPTY);
        }
    }
    else if (strlen(srcsub))
    {
        gotsrcsub = gotsrclod = gotsrcloc = TRUE;

        moveby = LVL_SUBLOAD;
        if (!strlen(actcod))
            strcpy(actcod, ACTCOD_CASE_PICK);

        ret_status = trnSelectInfoBySub(srcsub,
                                        &src_sub,
                                        &src_lod,
                                        &src_loc,
                                        &src_are);

        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "Error (%d) in trnSelectInfoBySub...", ret_status);
            return (eINT_SRCSUB_NOT_FOUND);
        }
        if (srcloc && misTrimLen(srcloc, STOLOC_LEN))
            if (misTrimStrncmp(srcloc, src_loc.stoloc, STOLOC_LEN) != 0 ||
                misTrimStrncmp(wh_id,  src_loc.wh_id,  WH_ID_LEN)  != 0)
                return (eINT_BAD_SRCLOC);

        if (srclod && misTrimLen(srclod, STOLOC_LEN))
            if (misTrimStrncmp(srclod, src_lod.lodnum, LODNUM_LEN) != 0)
                return (eINT_BAD_SRCLOD);

        if (srcsub && misTrimLen(srcsub, STOLOC_LEN))
            if (misTrimStrncmp(srcsub, src_sub.subnum, SUBNUM_LEN) != 0)
                return (eINT_BAD_SRCSUB);
    }
    else if (strlen(srclod))
    {
        gotsrcloc = gotsrclod = TRUE;

        moveby = LVL_LOAD;
        if (!strlen(actcod))
            strcpy(actcod, ACTCOD_PALLET_PICK);

        ret_status = trnSelectInfoByLoad(srclod,
                                         &src_lod,
                                         &src_loc,
                                         &src_are,
                                         wh_id);
        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "Error (%d) in trnSelectInfoByLoad...", ret_status);
            return (eINT_SRCLOD_NOT_FOUND);
        }

        if (srcloc && misTrimLen(srcloc, STOLOC_LEN))
            if (misTrimStrncmp(srcloc, src_loc.stoloc, STOLOC_LEN) != 0 ||
                misTrimStrncmp(wh_id,  src_loc.wh_id,  WH_ID_LEN)  != 0)
                return (eINT_BAD_SRCLOC);

        if (srclod && misTrimLen(srclod, STOLOC_LEN))
            if (misTrimStrncmp(srclod, src_lod.lodnum, LODNUM_LEN) != 0)
                return (eINT_BAD_SRCLOD);

    }
    else if (strlen(srcloc))
    {
        gotsrcloc = TRUE;
        moveby = LVL_LOCATION;
        if (!strlen(actcod))
            strcpy(actcod, ACTCOD_OTHER_PICK);
        if (srcqty <= 0)
            return (eINT_NO_MOVE_QTY_SPECIFIED);

        ret_status = trnSelectInfoByLoc(srcloc,
                                        &src_loc,
                                        &src_are,
                                        wh_id);

        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "Error (%d) in trnSelectInfoByLoc...", ret_status);
            return (eINT_SRCLOC_NOT_FOUND);
        }

    }
    else
    {
        return (eINT_NO_SOURCE);
    }


    if (!gotsrcloc && !gotsrclod && !gotsrcsub && !gotsrcdtl)
    {
        if (gotprtnum)
            return (eINT_NO_MOVE_QTY_SPECIFIED);
        else
            return (eINVALID_ARGS);
    }

    /* Now check the destination... */

    if (strlen(dstsub))
    {
        ret_status = trnSelectInfoBySub(dstsub,
                        &dst_sub,
                        &dst_lod,
                        &dst_loc,
                        &dst_are);

        if (ret_status != eOK)
        {
            if (!CreateDestination)
            {
                return (eINT_DSTSUB_NOT_FOUND);
            }
            else
            {                   
                /* If asset_typ isn't transferred in, we should get
                 * it from source load to avoid missing asset_typ in
                 * split load.
                 */
                if(!strlen(asset_typ)&&
                   strlen(src_lod.lodnum))
                {
                    sprintf(buffer,
                        " select asset_typ "
                        "   from invlod "
                        "  where lodnum = '%s' ",
                        src_lod.lodnum);
                    ret_status = sqlExecStr(buffer, &res);
                    
                    if(ret_status != eDB_NO_ROWS_AFFECTED &&
                       ret_status != eOK)
                    {
                        sqlFreeResults(res);
                        return ret_status;
                    }
                    if(ret_status == eOK)
                    {
                        row = sqlGetRow(res);
                        misTrimcpy(asset_typ, 
                                   sqlGetString (res, row, "asset_typ"),
                                   ASSET_TYP_LEN);
                        sqlFreeResults(res);
                    }
                }
                ret_status = sCreateDestSub(dstloc,
                                            dstlod,
                                            wh_id,
                                            dstsub,
                                            &dst_sub,
                                            &dst_lod,
                                            &dst_loc,
                                            &dst_are,
                                            lodtag,
                                            phyflg,
                                            subtag,
                                            asset_typ,
                                            sub_asset_typ,
                                            srcloc);
                if (ret_status != eOK)
                {
                    misTrc(T_FLOW, "Error creating destination sub - %ld",
                        ret_status);
                    return (ret_status);
                }
            }
        }
        else
        {
            /* 
             * If the destination sub exists, then make sure it matches
             * with any other destination parameters we were given.
             */
            if (dstlod && misTrimLen(dstlod, STOLOC_LEN))
                if (misTrimStrncmp(dstlod, dst_lod.lodnum, LODNUM_LEN) != 0)
                    return (eINT_BAD_DSTLOD);
            if (dstloc && misTrimLen(dstloc, STOLOC_LEN))
                if (misTrimStrncmp(dstloc, dst_loc.stoloc, STOLOC_LEN) != 0 ||
                    misTrimStrncmp(wh_id,  dst_loc.wh_id , WH_ID_LEN)  != 0)
                    return (eINT_BAD_DSTLOC);       
        }
        gotdstsub = gotdstlod = gotdstloc = TRUE;
    }
    else if (strlen(dstlod))
    {
        ret_status = trnSelectInfoByLoad(dstlod,
                                         &dst_lod,
                                         &dst_loc,
                                         &dst_are,
                                         wh_id);
        if (ret_status != eOK)
        {
            if (!CreateDestination)
            {
                misTrc(T_FLOW, "Error (%d) selecting dst info by load...",
                        ret_status);
                return (eINT_DSTLOD_NOT_FOUND);
            }
            else
            {
                /* If asset_typ isn't transferred in, we should get
                 * it from source load to avoid missing asset_typ in
                 * split load.
                 */
                if(!strlen(asset_typ)&&
                   strlen(src_lod.lodnum))
                {
                    sprintf(buffer,
                        " select asset_typ "
                        "   from invlod "
                        "  where lodnum = '%s' ",
                        src_lod.lodnum);
                    ret_status = sqlExecStr(buffer, &res);
                    
                    if(ret_status != eDB_NO_ROWS_AFFECTED &&
                       ret_status != eOK)
                    {
                        sqlFreeResults(res);
                        return ret_status;
                    }
                    if(ret_status == eOK)
                    {
                        row = sqlGetRow(res);
                        misTrimcpy(asset_typ, 
                                   sqlGetString (res, row, "asset_typ"),
                                   ASSET_TYP_LEN);
                        sqlFreeResults(res);
                    }
                }
                ret_status = sCreateDestLoad(dstloc,
                                             dstlod,
                                             wh_id,
                                             &dst_lod,
                                             &dst_loc,
                                             &dst_are,
                                             lodtag,
                                             asset_typ);
                if (ret_status != eOK)
                {
                    misTrc(T_FLOW, "Error creating destination load - %ld",
                        ret_status);
                    return (ret_status);
                }
            }
        }
        else
        {
            /* 
             * If the destination load exists, then make sure it matches
             * with any other destination parameters we were given.
             */
            if (dstloc && misTrimLen(dstloc, STOLOC_LEN))
                if (misTrimStrncmp(dstloc, dst_loc.stoloc, STOLOC_LEN) != 0 ||
                    misTrimStrncmp(wh_id,  dst_loc.wh_id , WH_ID_LEN)  != 0)
                    return (eINT_BAD_DSTLOC);
        }
        gotdstlod = gotdstloc = TRUE;
    }
    else if (strlen(dstloc))
    {
        ret_status = trnSelectInfoByLoc(dstloc,
                                        &dst_loc,
                                        &dst_are,
                                        wh_id);
        if (ret_status != eOK)
        {
            misTrc(T_FLOW, "Error (%d) selecting dst info by location...",
                ret_status);
            return (eINT_DSTLOC_NOT_FOUND);
        }
        gotdstloc = TRUE;
    }
    else
    {
        return (eINT_NO_DEST);
    }

    /*************************************************************** 
      Now determine if we need to find a detail to move with...
      we'll need to determine a detail if:
      moveby is one of LOAD, LOCATION, SUBLOAD and srcqty > 0 ...
      
      The trnFindDetail routine will do a couple of things for
      us 

      1.  It will return the prtnum, orgcod, ..., information
      for that location if a pick of the type we requested
      is possible.  It is possible if the prtnum, orgcod...
      is homogenous from the level specified down (i.e. in
      the location, on the load, on the subload...)
      2.  If it does find a suitable detail, it will return that.
      ***************************************************************/

    need_detail = 0;
    if (srcqty > 0 && 
       (moveby == LVL_LOAD ||
        moveby == LVL_SUBLOAD ||
        moveby == LVL_LOCATION))
    {
        /* Before we do any making of details, let's first check
           what we are moving...if a load/sub/dtl is specified and
           the quantity contained on the item matches, then just
           call this a moveby without quantity. HOWEVER, we only
           want to do this if we don't have a destination subload when
           moving from subload and don't have a destination load when
           moving from a load */

        ret_status = eDB_NO_ROWS_AFFECTED;
        if ((moveby == LVL_DETAIL && !gotdstsub) ||
            (moveby == LVL_SUBLOAD && !gotdstsub) ||
            (moveby == LVL_LOAD && !gotdstlod))
        {
            QtyOnLod = QtyOnSub = QtyOnDtl = 0;
            CatchQtyOnLod = CatchQtyOnSub = CatchQtyOnDtl = 0.0;
            if (moveby == LVL_LOAD)
                ret_status = trnSelectQtyOnLoad(src_lod.lodnum, 
                                                &QtyOnLod, 
                                                &CatchQtyOnLod);
            else if (moveby == LVL_SUBLOAD)
                ret_status = trnSelectQtyOnSub(src_sub.subnum, 
                                               &QtyOnSub,
                                               &CatchQtyOnSub);
            else if (moveby == LVL_DETAIL)
                ret_status = trnSelectQtyOnDtl(src_dtl.dtlnum, 
                                               &QtyOnDtl,
                                               &CatchQtyOnDtl);
        }

        /* Since we set all the quantities to zero before our of tests
           and since we execute at most one of the if test results, 
           it is OK to add all the quantities together since at most
           one of them will have a non-zero value...this is kinda
           hokey but it saves us some efficiency later when we need to 
           know one or more of the quantities on lod/sub/dtl.  */
        if (ret_status == eOK && 
	        (pck_catch_qty > 0.0 ||
	         srcqty == (QtyOnLod + QtyOnSub + QtyOnDtl)) &&
	        (pck_catch_qty <= 0.0 || 
	         src_catch_qty == (CatchQtyOnLod + CatchQtyOnSub +
                CatchQtyOnDtl)))
        {
            /* We no longer need to make details as this really isn't a 
               source quantity move... */
            srcqty = 0;
        }
        else
        {
            memset(Key, 0, sizeof(Key));
            if (moveby == LVL_LOCATION)
            {
                strncpy(Key, src_loc.stoloc, STOLOC_LEN);
            }
            else if (moveby == LVL_LOAD)
            {
                strncpy(Key, src_lod.lodnum, LODNUM_LEN);
            }
            else if (moveby == LVL_SUBLOAD)
            {
                strncpy(Key, src_sub.subnum, SUBNUM_LEN);
                idmflg = src_sub.idmflg;
            }

            need_detail = 1;

            memset(dtlnum, 0, sizeof(dtlnum));
            memset(prtnum, 0, sizeof(prtnum));
            memset(prt_client_id, 0, sizeof(prt_client_id));
            memset(orgcod, 0, sizeof(orgcod));
            memset(revlvl, 0, sizeof(revlvl));
            memset(supnum, 0, sizeof(supnum));
            memset(lotnum, 0, sizeof(lotnum));
            memset(invsts, 0, sizeof(invsts));
            memset(ship_line_id, 0, sizeof(ship_line_id));

            if (!moveByPartInfo)
            {
                ret_status = trnFindDetail(moveby,
                               Key,
                               srcqty,
                               ((pck_catch_qty > 0.0) ? src_catch_qty : 0.0),
                               dtlnum,    /* output */
                               prtnum,
                               prt_client_id,
                               orgcod,
                               revlvl,
                               supnum,
                               lotnum,
                               invsts,
                               ship_line_id,
                               &untcas,
                               &untpak,
                               idmflg,
                               dst_loc.stoloc,
                               wh_id,
                               distro_id);
            }
            else
            {
                ret_status = 
                    trnFindDetailByPartInfo(moveby,
                                Key,
                                srcqty,
                               ((pck_catch_qty > 0.0) ? src_catch_qty : 0.0),
                                dtlnum,
                                prtnum,
                                prt_client_id,
                                orgcod,
                                revlvl,
                                supnum,
                                lotnum,
                                invsts,
                                ship_line_id,
                                &untcas,
                                &untpak,
                                distro_id,
                                prtnumIn,
                                prt_client_idIn,
                                orgcodIn,
                                revlvlIn,
                                supnumIn,
                                lotnumIn,
                                invstsIn,
                                ship_line_idIn,
                                untcas_i,
                                untpak_i,
                                distro_idIn,
                                idmflg,
                                dst_loc.stoloc,
                                wh_id);
            }

            /* If we got an error get out */
            if (ret_status != eOK)
            {
                misTrc(T_FLOW, 
                       "Error (%d) attempting to fnd a detail to move...",
                        ret_status);
                return (ret_status);
            }

            if (strlen(dtlnum))
            {
                need_detail = 0;
                memset(&src_dtl, 0, sizeof(src_dtl));
                memset(&src_sub, 0, sizeof(src_sub));
                memset(&src_lod, 0, sizeof(src_lod));
                memset(&src_loc, 0, sizeof(src_loc));
                memset(&src_are, 0, sizeof(src_are));

                ret_status = trnSelectInfoByDtl(dtlnum,
                                &src_dtl,
                                &src_sub,
                                &src_lod,
                                &src_loc,
                                &src_are);
            }
        }
    }

    /*********    Verify destination information ***************/

    /* 01-Sep-1994, MPV - the following check used to be done 
       just after we verified the source information and just 
       before we started to verify the destination...we really
       only want to check this if the source is different from
       the dest....so...it was moved to its current spot since
       at this point we have all the info about the src and dst */

    /* If we don't have a srcref and we are in a significant    
       area code that is pickable, then we have to make sure we 
       aren't stealing already allocated quantity...      */

    if (strncmp(src_loc.stoloc, dst_loc.stoloc, STOLOC_LEN) != 0 &&
        strncmp(src_are.pckcod, PCKCOD_NOT_PICK, PCKCOD_LEN) != 0 &&
        src_are.sigflg == BOOLEAN_TRUE && strlen(srcref) == 0)
    {
        /* This check used to guarentee that we didn't steal
           product for what we were moving */
    }

    /*********************************************************
     * Now that we have both the src and dst info, we want
     * to provide a hook to be able to validate that that
     * the move between the areas and locations are kosher.
     * This would be a good place to validate the move
     * restrictions.
     *
     * This is intentionally allowed to pickup arguments off
     * of the stack, so that projects can take advantage of
     * everything that is there.
     *********************************************************/
     ret_status = srvInitiateInlineFormat(NULL,
                    "check inventory move restrictions "
                    "   where srcare = '%s' "
                    "     and srcloc = '%s' "
                    "     and dstare = '%s' "
                    "     and dstloc = '%s' "
                    "     and ign_restrict = %d"
                    "     and wh_id  = '%s' "
                    "",
                    src_are.arecod,
                    src_loc.stoloc,
                    dst_are.arecod,
                    dst_loc.stoloc,
                    ign_restrict,
                    wh_id);

    if(ret_status != eOK)
    {
        /* Free what needs to be freed. */
        return(ret_status);
    }

    /*********************************************************
     * Now check if we are going to mess up any pallet picks *
     * ...this can only happen if we are moving to an area   *
     * which is pickable and the untpal = untqty = comqty    *
     ********************************************************/

    /******************************************************
     *    At this point, we may have all the information      *
     *    that we need about the source.    We perform one      *
     *    last check to determine if we need a detail in      *
     *    order to perform the move as dictated by the      *
     *    parameters passed in.  Since we have already      *
     *    checked for a detail which exists, we know at      *
     *    this point we need to create a new one...attempt  *
     *    that now...                      *
     *****************************************************/

    if (need_detail)
    {
        memset(Key, 0, sizeof(Key));
        if (moveby == LVL_LOCATION)
            strncpy(Key, src_loc.stoloc, STOLOC_LEN);
        else if (moveby == LVL_LOAD)
            strncpy(Key, src_lod.lodnum, LODNUM_LEN);
        else if (moveby == LVL_SUBLOAD)
            strncpy(Key, src_sub.subnum, SUBNUM_LEN);
    }

    if (moveby == LVL_DETAIL_PARTIAL)
    {
        /* Set the moveby so a new unit quantity is created
         * during the split of the host invdtl record
         */
         
        /* We will also need to increment pckqty since the pick 
         * was created with the original unit quantity in mind
         */
        if(src_catch_qty > 0)
        {
            /* If we are not moving the whole catch qty from srcdtl */
            ret_status = sprintf(buffer,
                           " select 'x' from invdtl "
                           "  where dtlnum = '%s' "
                           "    and catch_qty > '%f' ",
                           srcdtl,
                           src_catch_qty);
            ret_status = sqlExecStr(buffer, NULL);

            if(ret_status == eOK)
            {
                 moveby = LVL_CATCH_DETAIL;
            }
            else
            {
                moveby = LVL_DETAIL;
            }
        }
        else
        {
            moveby = LVL_DETAIL;
        }
        if (!strlen(actcod))
            strcpy(actcod, ACTCOD_PIECE_PICK);
        memset(Key, 0, sizeof(Key));
        strcpy(Key, srcdtl);
        need_detail = 1;
    }

    /* If need a detail, then let's attempt to build one...
       if we can't, then we have no choice but to bail from this one... */

    if (need_detail)
    {
        ret_status = appNextNum(NUMCOD_DTLNUM, new_dtlnum);
        if (ret_status != eOK)
            return (ret_status);

        ret_status = trnMakeDetail(new_dtlnum,
                                   moveby,
                                   Key,
                                   prtnum,
                                   prt_client_id,
                                   orgcod,
                                   revlvl,
                                   lotnum,
                                   invsts,
                                   ship_line_id,
                                   untpak,
                                   untcas,
                                   srcqty,
                                   src_catch_qty,
                                   idmflg,
                                   dst_loc.stoloc,
                                   wh_id,
                                   distro_id,
                                   &additional_new_dtlnum_p);
        if (ret_status == eDB_NO_ROWS_AFFECTED)
            ret_status = eINT_SRC_IS_EMPTY;

        if (ret_status != eOK)
        {
            if(additional_new_dtlnum_p)
            {
              free(additional_new_dtlnum_p);
            }
            return (ret_status);
        }

        /* 
         * Set the catch quantity to 0 since we've already taken it into 
         * account by creating a new detail.
         * If this is a move by catch quantity, i.e. the pck_catch_qty is
         * greater than 0 then we need to keep this value.
         */
        if(pck_catch_qty <= 0.0)
        {
            src_catch_qty = 0;
        }

        /* If we needed a detail, then we'll need to do another select... */
        ret_status = trnSelectInfoByDtl(new_dtlnum,
                                        &src_dtl,
                                        &src_sub,
                                        &src_lod,
                                        &tmp_loc,
                                        &tmp_are);
        if (ret_status != eOK)
            return (ret_status);
    }
    /* Now check if the arguments passed in and the information
       we've derived jive with not moving a load onto itself, etc... */

    ret_status = sValidateArgs(src_loc.stoloc,
                               src_lod.lodnum,
                               src_sub.subnum,
                               src_dtl.dtlnum,
                               dst_loc.stoloc,
                               dst_lod.lodnum,
                               dst_sub.subnum,
                               srcqty);

    if (ret_status != eOK)
        return (ret_status);


    /* At this point, if we are still here and we have a positive move 
       quantity, then we will be moving by detail...we know that we 
       have all the source information...the only other step is to make
       sure we have a place to attach the detail to on the destination... */

    if (srcqty > 0)
    {
        moveby = LVL_DETAIL;
        if (!strlen(actcod))
            strcpy(actcod, ACTCOD_PIECE_PICK);
    }

    if (moveby <= LVL_SUBLOAD && gotdstlod == FALSE)
    {
        /* Get the appropriate destination load as determined
           by the glomming policies... */

        ret_status = sGetDestLoad(dst_loc.stoloc,
                                  wh_id,
                                  &dst_lod,
                                  sGetAreaGlomStrategy(dst_are.arecod),
                                  lodtag,
                                  asset_typ,
                                  src_loc.stoloc,
                                  src_lod.lodnum);                          
        if (ret_status != eOK)
            return (eINT_DEST_TOO_GENERAL);
    }
    if (moveby <= LVL_DETAIL && gotdstsub == FALSE)
    {
        ret_status = sGetDestSub(dst_lod.lodnum,
                     &dst_sub,
                     sGetAreaGlomStrategy(dst_are.arecod),
                     phyflg, subtag, sub_asset_typ);
        if (ret_status != eOK)
        {
            if (ret_status == eDB_NO_ROWS_AFFECTED)
                return (eINT_DEST_TOO_GENERAL);
            else
                return (ret_status);
        }
    }

    if (strstr(spcind, SPCIND_PRA_REVERSE))
    {
        revflg = BOOLEAN_TRUE;
    }
    else
    {
        revflg = BOOLEAN_FALSE;
    }

    sublist_p = dtllist_p = NULL;
    
    if (moveby == LVL_LOAD)
    {
        if (src_lod.mvlflg == BOOLEAN_FALSE)
            return (eINT_NON_MOVABLE_ITEM);

        ret_status = trnMoveLoad(&src_loc,
                                 &src_are,
                                 &src_lod,
                                 &dst_loc,
                                 &dst_are,
                                 wh_id,
                                 srcref,
                                 usr_id,
                                 actcod,
                                 revflg,
                                 xdkref,
                                 &xdkqty,
                                 src_catch_qty);
        if (ret_status != eOK)
            return (ret_status);
        misTrimcpy (invtid, src_lod.lodnum, LODNUM_LEN);
        misTrimcpy (lodlvl, LODLVL_LOAD, LODLVL_LEN);
    }
    else if (moveby == LVL_SUBLOAD)
    {
        if (src_sub.mvsflg == BOOLEAN_FALSE)
            return (eINT_NON_MOVABLE_ITEM);

        ret_status = trnMoveSub(&src_loc,
                                &src_are,
                                &src_lod,
                                &src_sub,
                                &dst_loc,
                                &dst_are,
                                &dst_lod,
                                wh_id,
                                srcref,
                                actcod,
                                usr_id,
                                revflg,
                                &sublist_p,
                                &dtllist_p,
                                xdkref,
                                &xdkqty,
                                src_catch_qty);
        if (ret_status != eOK)
            return (ret_status);
        misTrimcpy (invtid, src_sub.subnum, SUBNUM_LEN);
        misTrimcpy (lodlvl, LODLVL_SUB, LODLVL_LEN);
    }
    else if (moveby == LVL_DETAIL)
    {
        /* If the detail move is inhibited, get out */
        if (src_sub.idmflg == BOOLEAN_TRUE)
            return (eINT_CANT_BREAK_SERIALIZED);

        if (dst_sub.idmflg == BOOLEAN_TRUE)
            return (eINT_CANT_MOVE_INTO_SERIALIZED);
        ret_status = trnMoveDetail(&src_loc,
                                   &src_are,
                                   &src_lod,
                                   &src_sub,
                                   &src_dtl,
                                   &dst_loc,
                                   &dst_are,
                                   &dst_sub,
                                   wh_id,
                                   srcref,
                                   actcod,
                                   usr_id,
                                   revflg,
                                   &sublist_p,
                                   &dtllist_p,
                                   xdkref,
                                   &xdkqty,
                                   src_catch_qty);

        if (ret_status != eOK)
            return (ret_status);
        misTrimcpy (invtid, src_dtl.dtlnum, DTLNUM_LEN);
        misTrimcpy (lodlvl, LODLVL_DTL, LODLVL_LEN);
    }
    /*
     * calling this command to update the lst_arecod of the inventory
     * ***************************************************************
     * if it's LVL_LOAD movement
     *      update all the invdtl entry's lst_arecod against 
     *      the load number
     * if it's LVL_SUBLOAD movement
     *      update all the invdtl entry's lst_arecod against
     *      the sub-load number
     * if it's LVL_DETAIL movement
     *      update all the invdtl entry's lst_arecod against
     *      the detail-load number
     * ***************************************************************
     */
    
    ret_status = srvInitiateCommandFormat(NULL, 
                             " process last area code for inventory movement "
                             "   where invtid = '%s' "
                             "     and lodlvl = '%s' "
                             "     and dstloc = '%s' "
                             "     and dstare = '%s' "
                             "     and wh_id  = '%s' ",
                             invtid,
                             lodlvl,
                             dst_loc.stoloc,
                             dst_are.arecod,
                             wh_id);
    if (ret_status != eOK)
    {
        if(additional_new_dtlnum_p)
        {
            free(additional_new_dtlnum_p);
        }
        return (ret_status);
    }
    if(additional_new_dtlnum_p && moveby == LVL_DETAIL)
    {
      for (dtlnumptr = strtok(additional_new_dtlnum_p, CONCAT_CHAR); 
                        dtlnumptr; dtlnumptr = strtok (NULL, CONCAT_CHAR))
        {
              misTrc(T_FLOW, 
                      "get additional detial(%s) to move...",dtlnumptr);
          /* If we have more details, then we'll need to do another select... */
              ret_status = trnSelectInfoByDtl(dtlnumptr,
                                              &src_dtl,
                                              &src_sub,
                                              &src_lod,
                                              &tmp_loc,
                                              &tmp_are);
              if (ret_status != eOK)
              {
                  free(additional_new_dtlnum_p);
                  return (ret_status);
              }
              
        ret_status = trnMoveDetail(&src_loc,
                                   &src_are,
                                   &src_lod,
                                   &src_sub,
                                   &src_dtl,
                                   &dst_loc,
                                   &dst_are,
                                   &dst_sub,
                                   wh_id,
                                   srcref,
                                   actcod,
                                   usr_id,
                                   revflg,
                                   &sublist_p,
                                   &dtllist_p,
                                   xdkref,
                                   &xdkqty,
                                   src_catch_qty);
              if (ret_status != eOK)
              {
                  free(additional_new_dtlnum_p);
                  return (ret_status);
              }
              misTrimcpy (invtid, src_dtl.dtlnum, DTLNUM_LEN);
              misTrimcpy (lodlvl, LODLVL_DTL, LODLVL_LEN);
              ret_status = srvInitiateCommandFormat(NULL, 
                           " process last area code for inventory movement "
                                       "   where invtid = '%s' "
                                       "     and lodlvl = '%s' "
                                       "     and dstloc = '%s' "
                                       "     and dstare = '%s' "
                                       "     and wh_id  = '%s' ",
                                       invtid,
                                       lodlvl,
                                       dst_loc.stoloc,
                                       dst_are.arecod,
                                       wh_id);              
              if (ret_status != eOK)
              {
                  free(additional_new_dtlnum_p);
                  return (ret_status);
              }
        }
      free(additional_new_dtlnum_p);
    }
    /* At this point, we possibly need to verify that we haven't
       mixed parts at this location... */

    if (CurPtr)
    {
        *CurPtr = sSetupReturn(src_are.arecod,
                               src_are.praflg,
                               src_loc.stoloc,
                               src_lod.lodnum,
                               src_sub.subnum,
                               src_dtl.dtlnum,
                               srcqty,
                               src_catch_qty_i,
                               dst_are.arecod,
                               dst_are.praflg,
                               dst_loc.stoloc,
                               dst_lod.lodnum,
                               dst_sub.subnum,
                               (strlen(trknum) ? trknum :
                                   (src_are.expflg == BOOLEAN_TRUE ?
                                       src_loc.stoloc :
                                           (dst_are.expflg == BOOLEAN_TRUE ?
                                               dst_loc.stoloc :
                                                    ""))),
                               movref,
                               actcod,
                               movref,
                               src_loc.cipflg,
                               dst_loc.cipflg,
                               src_are.pckcod,
                               sublist_p,
                               dtllist_p,
                               dst_are.pckcod,
                               xdkref,
                               xdkqty,
                               src_are.put_to_sto_flg,
                               dst_are.put_to_sto_flg,
                               wh_id);
        if (dtllist_p)
            free(dtllist_p);
        if (sublist_p)
            free(sublist_p);

        sublist_p = dtllist_p = NULL;
    }
    return (eOK);
}

LIBEXPORT 
RETURN_STRUCT *varMoveInventory(char *srcloc_i, char *srclod_i, char *srcsub_i,
                                char *srcdtl_i, 
                                long *srcqty,   double *src_catch_qty_i,
                                char *srcref_i,
                                char *dstloc_i, char *dstlod_i, char *dstsub_i,
                                char *usr_id_i, char *actcod_i, char *movref_i,
                                char *devcod_i, char *prtnum_i, 
                                char *prt_client_id_i,
                                char *orgcod_i,
                                char *revlvl_i, char *lotnum_i, char *invsts_i,
                                long *untcas_i, char *spcind_i, long *untpak_i,
                                moca_bool_t *newflg_i, char *ship_line_id_i,
                                char *trknum_i, char *glom_override_i,
                                char *wh_id_i, char *lodtag_i, char *subtag_i,
                                moca_bool_t *phyflg_i, 
                                char *asset_typ_i, 
                                char *sub_asset_typ_i, 
                                moca_bool_t *ign_restrict_i, char *distro_id_i,
                                char *supnum_i)
{
    long            ret_status;
    char            wh_id[WH_ID_LEN+1];
    RETURN_STRUCT  *CurPtr;

    misTrc(T_FLOW, "Calling intMoveInv...load: %s", srclod_i?srclod_i:"");

    memset(wh_id, 0, sizeof(wh_id));

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    else
        return APPMissingArg("wh_id");

    CurPtr = NULL;
    misTrc(T_FLOW, "Enabled output...");

    misTrc(T_FLOW, "ign_restrict: %d", (ign_restrict_i ? *ign_restrict_i : 5));
    
    ret_status = sIntMoveInv(srcloc_i, srclod_i, srcsub_i,
                             srcdtl_i, (srcqty ? *srcqty : 0), 
                             (src_catch_qty_i ? *src_catch_qty_i : 0), 
                             srcref_i,
                             dstloc_i, dstlod_i, dstsub_i,
                             usr_id_i, actcod_i, movref_i, devcod_i, &CurPtr,
                             prtnum_i, prt_client_id_i,
                             orgcod_i, revlvl_i, lotnum_i, invsts_i,
                             (untcas_i ? *untcas_i : 0), spcind_i,
                             (untpak_i ? *untpak_i : 0), newflg_i,
                             ship_line_id_i, trknum_i, glom_override_i,
                             wh_id, lodtag_i, subtag_i, phyflg_i, 
                             asset_typ_i, sub_asset_typ_i,
                             ign_restrict_i, distro_id_i, supnum_i);
    
    misTrc(T_FLOW, "intMoveIn returns: %d", ret_status);
    
    if (CurPtr == NULL)
        CurPtr = srvResults(ret_status, NULL); 

    return (CurPtr); 
}
