static const char *rcsid = "$Id: intGetStoragePolicySuffixes.c 158418 2008-06-03 19:30:02Z mzais $";
/*#START***********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "intlib.h"

static long sGet_Description(char *colval,
			     char *locale_id,
	                     char *short_dsc,
			     char *lngdsc)
{
    RETURN_STRUCT *CmdRes;
    mocaDataRes   *res;
    mocaDataRow   *row;
    char          buffer[1000];
    long          ret_status;

    memset(short_dsc, 0, sizeof(short_dsc));
    memset(lngdsc,    0, sizeof(lngdsc));

    sprintf(buffer,
	    "list descriptions"
	    " where colnam    = 'suffix'"
	    "   and colval    = '%s' "
	    "   and locale_id = '%s'",
	    colval,
	    locale_id ? locale_id : "");

    CmdRes = NULL;
    res = NULL;
    ret_status = srvInitiateCommand(buffer, &CmdRes);
    
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
	/*
	 * If a description doesn't exist, just show the column value
	 * again.  This is for VB screens so something will appear in the
	 * drop down list even if a description does not exist.
	 */

        misTrimcpy(short_dsc, colval, SHORT_DSC_LEN);
        misTrimcpy(lngdsc, colval, LNGDSC_LEN);
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        return (eOK);
    }
    else if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        return (ret_status);
    }

    res = CmdRes->ReturnedData;
    row = sqlGetRow(res);

    misTrimcpy(short_dsc, sqlGetString(res, row, "short_dsc"), SHORT_DSC_LEN);
    misTrimcpy(lngdsc, sqlGetString(res, row, "lngdsc"), LNGDSC_LEN);
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    return (eOK);
}

/*
 * it's called by intGetStoragePolicySuffixes to determine
 * if the inventory is return part or an asset (EMPTYPART), if it is,
 * the query statement won't join to the prtmst_view table as 
 * to EMPTYPART and RETURNPART are virtual parts
 */
static void isReturnOrEmptyPart(char * inventory_where,
                                long * isEmptyPrtnum_o, 
                                long *  isReturnPart_o)
{
    long ret_status;
    char buffer[512];

    mocaDataRes   *res;
    mocaDataRow   *row;


    memset (buffer, 0, sizeof(buffer));
        
    sprintf(buffer,
            " select prtnum "
            "   from invdtl, "
            "        invsub  "
            "  where invdtl.subnum = invsub.subnum  "
            "    and ( invdtl.prtnum = '%s' "
            "     or   invdtl.prtnum = '%s')"
            "    %s  ",
            PRTNUM_RETURN,
            PRTNUM_EMPTY,
            inventory_where);

    ret_status = sqlExecStr(buffer, &res);

    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(res);
        return;
    }
    else if(ret_status == eDB_NO_ROWS_AFFECTED)
    {
        *isEmptyPrtnum_o = 0;
        *isReturnPart_o = 0;
        sqlFreeResults(res);
        return;
    }

    row = sqlGetRow(res);
    if (strncmp(misTrim(sqlGetString(res, row, "prtnum")),
                PRTNUM_RETURN, PRTNUM_LEN) == 0)
    {
        *isReturnPart_o  = 1;
        *isEmptyPrtnum_o = 0;
    }
    else if (strncmp(misTrim(sqlGetString(res, row, "prtnum")),
                PRTNUM_EMPTY, PRTNUM_LEN) == 0)
    {
        *isReturnPart_o  = 0;
        *isEmptyPrtnum_o = 1;
    }
    sqlFreeResults(res);
    return;
}



LIBEXPORT 
RETURN_STRUCT *varGetStoragePolicySuffixes(char *lodnum_i,
					   char *arecod_i,
					   char *wrkzon_i,
					   char *stoloc_i,
					   char *prtnum_i,
					   char *prt_client_id_i,
					   char *lotnum_i,
					   char *revlvl_i,
					   char *orgcod_i,
					   char *invsts_i,
					   long *untcas_i,
					   long *untqty_i,
					   char *fifdte_i,
					   char *type_i,
					   char *ftpcod_i,
					   char *subnum_i,
					   char *dtlnum_i,
					   long *untpak_i,
					   char *wh_id_i)
{
    long ret_status = eOK;
    RETURN_STRUCT *CurPtr;
    RETURN_STRUCT *CmdRes;
    char stoloc[STOLOC_LEN + 1];
    char arecod[ARECOD_LEN + 1];
    char wrkzon[WRKZON_LEN + 1];
    char lodnum[LODNUM_LEN + 1];
    char prtnum[PRTNUM_LEN + 1];
    char prt_client_id[CLIENT_ID_LEN + 1];
    char lotnum[LOTNUM_LEN + 1];
    char revlvl[REVLVL_LEN + 1];
    char orgcod[ORGCOD_LEN + 1];
    char invsts[INVSTS_LEN + 1];
    char ftpcod[FTPCOD_LEN + 1];
    char subnum[SUBNUM_LEN + 1];
    char dtlnum[DTLNUM_LEN + 1];
    char tmpstr[RTSTR1_LEN + 1];
    char tmpstring[100];
    char tmprtstr1[RTSTR1_LEN + 1];
    char suffix[RTSTR1_LEN + 1];
    char wh_id[WH_ID_LEN + 1];
    char buffer[1500];
    char inventory_from[512];
    char inventory_where[512];
    char fifdte[20];
    char calltype[30];
    short ii = 0;
    mocaDataRes *Res;
    mocaDataRow *Row;
    mocaDataRes *cnsgRes = NULL;
    mocaDataRow *cnsgRow = NULL;
    long   untqty;
    long   tmpnum;
    double totwgt;
    long   totqty;
    double grswgt;
    long   palpct;
    long   untpal;
    long   untcas;
    static long  hvy_pal_wgt;
    static long  ful_pal_pct;
    static long  med_pal_pct;
    static long  chk_for_par_case;
    static short policies_read = FALSE;
    char         short_dsc[SHORT_DSC_LEN + 1];
    char         lngdsc[LNGDSC_LEN + 1];
    char         *locale_id;
    char	 dtype;
    short bGotInventorySpecifier;
    char trk_cnsg_cod[TRK_CNSG_COD_LEN];
    long cnsg = 0L;
    long  isEmptyPrtnum = 0;

    locale_id = NULL;
    srvGetNeededElement("locale_id", NULL, &dtype, (void **) &locale_id);
    if (locale_id == NULL)
	locale_id = osGetVar(LESENV_LOCALE_ID);

    memset(stoloc, 0, sizeof(stoloc));
    memset(ftpcod, 0, sizeof(ftpcod));
    memset(subnum, 0, sizeof(subnum));
    memset(dtlnum, 0, sizeof(dtlnum));
    memset(arecod, 0, sizeof(arecod));
    memset(wrkzon, 0, sizeof(wrkzon));
    memset(lodnum, 0, sizeof(lodnum));
    memset(prtnum, 0, sizeof(prtnum));
    memset(lotnum, 0, sizeof(lotnum));
    memset(revlvl, 0, sizeof(revlvl));
    memset(orgcod, 0, sizeof(orgcod));
    memset(invsts, 0, sizeof(invsts));
    memset(fifdte, 0, sizeof(fifdte));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    untqty = 0;
    memset(calltype, 0, sizeof(calltype));
    memset(short_dsc, 0, sizeof(short_dsc));
    memset(lngdsc, 0, sizeof(lngdsc));
    memset(wh_id, 0, sizeof(wh_id));
    memset(inventory_from, 0, sizeof(inventory_from));
    memset(inventory_where, 0, sizeof(inventory_where));
    memset(trk_cnsg_cod, 0, sizeof(trk_cnsg_cod));

    if (stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
	misTrimcpy(stoloc, stoloc_i, STOLOC_LEN);
    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
	misTrimcpy(arecod, arecod_i, ARECOD_LEN);
    if (wrkzon_i && misTrimLen(wrkzon_i, WRKZON_LEN))
	misTrimcpy(wrkzon, wrkzon_i, WRKZON_LEN);
    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
	misTrimcpy(lodnum, lodnum_i, LODNUM_LEN);
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
	misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    else
       return APPMissingArg("wh_id");

    bGotInventorySpecifier = 0;
    /*
     * If a part number was passed, then  we're going to need a client Id 
     * also. In a non-3PL environment, we will just get the default client.  
     * Otherwise, we will used the one passed in.  If one wasn't passed, 
     * it's an error, since in this context it doesn't make sense to have 
     * a prtnum without a client id.
     */

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
    {
	misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);
        ret_status = appGetClient(prt_client_id_i, prt_client_id);
	if (ret_status != eOK)
	    return (srvResults(ret_status, NULL));
        
        if (!wh_id_i && !misTrimLen(wh_id_i, WH_ID_LEN))
	     return APPMissingArg("wh_id");

    }

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    
    if (lotnum_i && misTrimLen(lotnum_i, LOTNUM_LEN))
	misTrimcpy(lotnum, lotnum_i, LOTNUM_LEN);
    if (revlvl_i && misTrimLen(revlvl_i, REVLVL_LEN))
	misTrimcpy(revlvl, revlvl_i, REVLVL_LEN);
    if (orgcod_i && misTrimLen(orgcod_i, ORGCOD_LEN))
	misTrimcpy(orgcod, orgcod_i, ORGCOD_LEN);
    if (invsts_i && misTrimLen(invsts_i, INVSTS_LEN))
	misTrimcpy(invsts, invsts_i, INVSTS_LEN);
    if (fifdte_i && misTrimLen(fifdte_i, 20))
	strncpy(fifdte, fifdte_i, misTrimLen(fifdte_i, 20));
    if (type_i && misTrimLen(type_i, 20))
	strncpy(calltype, type_i, misTrimLen(type_i, 20));
    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
	misTrimcpy(subnum, subnum_i, SUBNUM_LEN);
    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
	misTrimcpy(dtlnum, dtlnum_i, DTLNUM_LEN);
    if (ftpcod_i && misTrimLen(ftpcod_i, FTPCOD_LEN))
	misTrimcpy(ftpcod, ftpcod_i, FTPCOD_LEN);


    if (untqty_i)
	untqty = *(long *) untqty_i;
    else
	untqty = 0;
    
    CurPtr = NULL;

    /* They must provide one of the following:
     * lodnum, subnum, dtlnum or client_id/prtnum & quantity OR 
     * if they don't give us anything, return the list 
     */

    if (!(lodnum && misTrimLen(lodnum, LODNUM_LEN)) &&
	!(subnum && misTrimLen(subnum, SUBNUM_LEN)) &&
	!(dtlnum && misTrimLen(dtlnum, DTLNUM_LEN)) &&
	!(prtnum && misTrimLen(prtnum, PRTNUM_LEN)))
    {
	/* They didn't give us anything so return the list */
	if (untqty == 0)
	{
	    CurPtr = srvResultsInit(eOK,
				    "suffix",    COMTYP_CHAR, 
				    CODVAL_LEN,
				    "short_dsc", COMTYP_CHAR, 
				    SHORT_DSC_LEN,
				    "lngdsc",    COMTYP_CHAR, 
				    LNGDSC_LEN,
				    NULL);
	    sGet_Description(PARCAS_SUFFIX, locale_id, short_dsc, lngdsc);
	    srvResultsAdd(CurPtr, 
			  PARCAS_SUFFIX, short_dsc, lngdsc);
	    sGet_Description(HLFPAL_SUFFIX, locale_id, short_dsc, lngdsc);
	    srvResultsAdd(CurPtr, 
			  HLFPAL_SUFFIX, short_dsc, lngdsc);
	    sGet_Description(FULPAL_SUFFIX, locale_id, short_dsc, lngdsc);
	    srvResultsAdd(CurPtr, 
			  FULPAL_SUFFIX, short_dsc, lngdsc);
	    sGet_Description(HVYWGT_SUFFIX, locale_id, short_dsc, lngdsc);
	    srvResultsAdd(CurPtr, 
			  HVYWGT_SUFFIX, short_dsc, lngdsc);
	    sGet_Description(MIXLOD_SUFFIX, locale_id, short_dsc, lngdsc);
	    srvResultsAdd(CurPtr, 
			  MIXLOD_SUFFIX, short_dsc, lngdsc);
        sGet_Description(CONSIGNED_SUFFIX, locale_id, short_dsc, lngdsc);
        srvResultsAdd(CurPtr,
              CONSIGNED_SUFFIX, short_dsc, lngdsc);
	    
	    /*
	     * Now, add in all of the inventory statuses
	     */
	    
	    sprintf(buffer, 
		    "list code descriptions "
		    " where colnam  = 'invsts' "
		    " and locale_id = '%s'",
		    locale_id ? locale_id : "");
	    
	    CmdRes = NULL;
	    Res = NULL;
	    ret_status = srvInitiateCommand(buffer, &CmdRes);
	    Res = CmdRes->ReturnedData;
	    for (Row = sqlGetRow(Res); Row; Row = sqlGetNextRow(Row))
	    {
		memset(tmpstring, 0, sizeof(tmpstring));
		sprintf(tmpstring, "%s-%s",
		        INVSTS_SUFFIX,
		        misTrim(sqlGetString(Res, Row, "codval")));
		srvResultsAdd(CurPtr,
			      tmpstring,
			      sqlGetString(Res, Row, "short_dsc"),
			      sqlGetString(Res, Row, "lngdsc"));
	    }
	    return (CurPtr);
	}
	/* Something is wrong because they gave us qty w/o part */
	else
	{
	    return APPMissingArg("prtnum");
	}
    }

    /* Now lets get going... */
    /* If haven't been read yet, read the policies into Static Values */

    if (!policies_read)
    {
	/* select the policies and put the values into the variables */

	sprintf(buffer,
		"select polval, rtstr1, rtnum1 "
		"  from poldat_view "
		" where polcod = '%s' "
		"   and polvar = '%s' "
		"   and wh_id  = '%s' "
		"order by polcod, polvar, polval, srtseq ",
		POLCOD_STORE_DEFARE,
		POLVAR_SEL_PARMS,
		wh_id);

	ret_status = sqlExecStr(buffer, &Res);

	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(Res);
	    return (srvResults(ret_status, NULL));
	}

	hvy_pal_wgt = 0;
	ful_pal_pct = 0;
	med_pal_pct = 0;
	chk_for_par_case = 0;

	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    policies_read = TRUE;


	    /* Work through the result set */
	    Row = sqlGetRow(Res);

	    do
	    {
		strncpy(tmpstr,
		      misTrim(sqlGetValue(Res, Row, "polval")), POLVAL_LEN);
		strncpy(tmprtstr1,
		      misTrim(sqlGetValue(Res, Row, "rtstr1")), RTSTR1_LEN);
		tmpnum = sqlGetLong(Res, Row, "rtnum1");

		if (strncmp(tmpstr, HVYWGT_SUFFIX, POLVAL_LEN) == 0)
		{
		    hvy_pal_wgt = tmpnum;
		}
		else if (strncmp(tmpstr, FULPAL_SUFFIX, POLVAL_LEN) == 0)
		{
		    ful_pal_pct = tmpnum;
		}
		else if (strncmp(tmpstr, HLFPAL_SUFFIX, POLVAL_LEN) == 0)
		{
		    med_pal_pct = tmpnum;
		}
		else if (strncmp(tmpstr, POLVAL_CHECK_FOR_PARTIAL_CASE,
				 POLVAL_LEN) == 0)
		{
                    if (tmpnum == BOOLEAN_TRUE)
			chk_for_par_case = TRUE;
		}

	    } while ((Row = sqlGetNextRow(Row)));
	}

	sqlFreeResults(Res);
    }

    untpal = 0;
    untcas = 0;
    memset(suffix, 0, sizeof(suffix));

    /* Set up the return set.  The following return set is used by allocate
     * location, and currently is not presented to the user.  Therefore,
     * we will not bother taking the extra processing time that would required
     * to also publish out the description of the suffix.  If this list is
     * ever presented to the user, we will have to publish out descriptions
     * like we do in the above case, where nothing gets passed in.
     */

    CurPtr = srvResultsInit(eOK,
	                    "suffix",    COMTYP_CHAR, CODVAL_LEN,
			    NULL);
    

    if ((dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN)) ||
	(subnum_i && misTrimLen(subnum_i, SUBNUM_LEN)) ||
	(lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN)))
    {
	/* Lets get an Idea of what inventory we have on the pallet */
	/*   Go by detail, then sub then load */
	long bIsReturnPart = 0;
	bGotInventorySpecifier = 1;
	if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
	    sprintf(inventory_where, " and invdtl.dtlnum = '%s' ", dtlnum);
	else if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
	    sprintf(inventory_where, " and invsub.subnum = '%s' ", subnum);
	else if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
	    sprintf(inventory_where, " and invsub.lodnum = '%s' ", lodnum);
	    
        /* lets check if we are dealing with Asset EMPTYPART or RETURNPART */
        isReturnOrEmptyPart(inventory_where,
                            &isEmptyPrtnum,
                            &bIsReturnPart);
        
        /*
         * if it's a RETURNPART inventory, the query statement which will be
         * executed won't take account into the prtmst_view table; otherwize
         * do it. It's because the RETURNPART part is a virtual part number
         * which does not exist in the prtmst_view table.
         * if it's a asset , the query statement which will be
         * executed won't take into account the prtmst_view table;
         * It's because the assets are represented as EMPTYPART part 
         * which is virtual part number and does not exist 
         * in the prtmst_view table.
         */ 
        if (bIsReturnPart == 0 &&
            isEmptyPrtnum == 0)
        {
            sprintf(buffer,
        	    "select prtmst_view.prtnum, sum(invdtl.untqty) totqty, "
        	    "       prtmst_view.grswgt, prtmst_view.untpal, invdtl.untcas, "
        	    "       invdtl.invsts, "
        	    "       invdtl.prt_client_id||invdtl.prtnum|| "
        	    "       invdtl.orgcod||invdtl.revlvl "
        	    "       ||invdtl.invsts||invdtl.lotnum "
                    "       ||TO_CHAR(invdtl.untcas,'999') "
        	    "       ||TO_CHAR(invdtl.untpak, '999')  mykey "
        	    "   from prtmst_view, invdtl, invsub, invlod "
        	    "   where invdtl.subnum        = invsub.subnum "
                    "     and invlod.lodnum        = invsub.lodnum "
        	    "     and invdtl.prtnum        = prtmst_view.prtnum "
        	    "     and invdtl.prt_client_id = prtmst_view.prt_client_id "
                    "     and invlod.wh_id         = prtmst_view.wh_id "
        	    "     %s  "
        	    "group by prtmst_view.prtnum, prtmst_view.grswgt, prtmst_view.untpal, "
        	    "         invdtl.untcas, "
        	    "         invdtl.invsts, "
        	    "         invdtl.prt_client_id, invdtl.prtnum, "
        	    "         invdtl.orgcod, invdtl.revlvl, "
        	    "         invdtl.invsts, invdtl.lotnum, "
                    "         invdtl.untcas, "
        	    "         invdtl.untpak ",
        	    inventory_where);
        }
        else
        {
            sprintf(buffer,
                    "select invdtl.prtnum, sum(invdtl.untqty) totqty, "
        	    "       cast(null as float) grswgt, 1 untpal, "
		    "       invdtl.untcas, invdtl.invsts, "
        	    "       invdtl.prt_client_id||invdtl.prtnum|| "
        	    "       invdtl.orgcod||invdtl.revlvl "
        	    "       ||invdtl.invsts||invdtl.lotnum "
                    "       ||TO_CHAR(invdtl.untcas,'999') "
        	    "       ||TO_CHAR(invdtl.untpak, '999')  mykey "
        	    "   from  invdtl, invsub, invlod "
        	    "   where invdtl.subnum        = invsub.subnum "
        	    "     and invsub.lodnum        = invlod.lodnum ");
            if (isEmptyPrtnum)
            {
                sprintf(buffer, "%s "
                        "   and invdtl.prtnum = '%s' ",
                        buffer, PRTNUM_EMPTY);
            }
            else
            {
                sprintf(buffer, "%s "
                        "   and invdtl.prtnum = '%s' "
                        "   and invdtl.prt_client_id = '%s'",
                        buffer, PRTNUM_RETURN, DEFAULT_INV_ATTR);
            }
            sprintf(buffer, "%s "
        	    "     %s "
        	    "group by invdtl.prtnum, "
        	    "         invdtl.untcas, "
        	    "         invdtl.invsts, "
        	    "         invdtl.prt_client_id, invdtl.prtnum, "
        	    "         invdtl.orgcod, invdtl.revlvl, "
        	    "         invdtl.invsts, invdtl.lotnum, "
                    "         invdtl.untcas, "
        	    "         invdtl.untpak ",
        	    buffer, inventory_where);
        }
    }
    /* If they sent a part and quantity, this is the select */
    else
    {
	sprintf(buffer,
		"select prtmst_view.prtnum, '%ld' totqty, prtmst_view.rcvsts invsts,"
		"       prtmst_view.grswgt, prtmst_view.untpal, prtmst_view.untcas "
		"  from prtmst_view "
		" where prtnum        = '%s' "
		"   and prt_client_id = '%s' "
		"   and wh_id = '%s' ",
		untqty, 
		prtnum,
		prt_client_id,
		wh_id);
    }
    
    ret_status = sqlExecStr(buffer, &Res);
    
    if (ret_status != eOK)
    {
	sqlFreeResults(Res);
	return (srvResults(ret_status, NULL));
    }
    
    Row = sqlGetRow(Res);

    /* In the following, we check if consignment tracking is applicable to the
     * inventory identifier passed in. If so, we will set cnsg flag so that
     * Consignment suffix is returned. Location allocation can then check
     * the storage policies during putaway. */
    if (misTrimLen(dtlnum, DTLNUM_LEN) > 0)
    {
        sprintf(inventory_from, "");
        sprintf(inventory_where, " and invdtl.dtlnum='%s'", dtlnum);
    }
    else if (misTrimLen(subnum, SUBNUM_LEN) > 0)
    {
        sprintf(inventory_from, "");
        sprintf(inventory_where, " and invdtl.subnum='%s'", subnum);
    }
    else if (misTrimLen(lodnum, LODNUM_LEN) > 0)
    {
        sprintf(inventory_from, ", invsub");
        sprintf(inventory_where,
            " and invdtl.subnum=invsub.subnum and invsub.lodnum='%s'", lodnum);
    }
    /* LBS - 10/08/2009
	 * This starts a code merge from standard product to improve performance
	 * 
	 * If this command is called without the lodnum, subnum, or dtlnum we wont be
     * able to lookup the consignment flag so we should set it to cnsg to zero. The
     * if statement was added for better performance. It prevents the selecting every
     * row from the invdtl table .
     */
    if (misTrimLen(inventory_where, sizeof(inventory_where)) > 0)
    {
		/* Here we check only if a part is specified in sup_prt_cnsg_ovr rather than
		 * the specific prtnum. It is sufficient to determine the distinct
		 * combinations of trk_cnsg_cod and whether partnum of the inventory is
		 * specified in sup_prt_cnsg_ovr. */
		sprintf(buffer,
				"[select distinct supnum,"
				"        prtnum,"
				"        prt_client_id"
				"   from invdtl"
				"        %s"
				"  where 1=1 %s]"
				"|"
				"if (@? = 0)"
				"{"
				"    [select distinct supmst.trk_cnsg_cod,"
				"            decode(sup_prt_cnsg_ovr.prtnum, null, 0, 1)"
				"                prt_specified"
				"       from supmst"
				"       left outer join sup_prt_cnsg_ovr"
				"         on sup_prt_cnsg_ovr.supnum=supmst.supnum"
				"        and sup_prt_cnsg_ovr.client_id=supmst.client_id"
				"        and sup_prt_cnsg_ovr.prtnum=@prtnum"
				"        and sup_prt_cnsg_ovr.prt_client_id=@prt_client_id"
				"        and sup_prt_cnsg_ovr.wh_id='%s'"
				"      where supmst.supnum=@supnum"
				"        and supmst.client_id=@prt_client_id]"
				"}",
				inventory_from,
				inventory_where,
				wh_id);
		ret_status = srvInitiateCommand(buffer, &CmdRes);
		if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
		{
			misTrc(0, "Unable to determine consignment tracking requirements for"
				   " inventory.");
			sqlFreeResults(Res);
			return (srvResults(ret_status, NULL));
		}
		else if (ret_status == eDB_NO_ROWS_AFFECTED)
		{
			cnsg = 0L;
		}
		else
		{
			cnsgRes = srvGetResults(CmdRes);
			cnsgRow = sqlGetRow(cnsgRes);
			if (sqlGetNumRows(cnsgRes) > 1)
			{
				/* Load is mixed - do not process inventory for consignment
				 * suffix */
				cnsg = 0L;
			}
			else
			{
				misTrimcpy(trk_cnsg_cod,
					sqlGetString(cnsgRes, cnsgRow, "trk_cnsg_cod"),
					TRK_CNSG_COD_LEN);

				/* By default, set consigned suffix as not applicable. Then check
				 * for special cases where suffix would be applicable.
				 */
				cnsg = 0L;
				if (misTrimStrncmp(trk_cnsg_cod, TRK_CNSG_TRKALL,
					TRK_CNSG_COD_LEN) == 0)
				{
					/* We should track consignment for the supplier/client for all
					 * parts so no need to check sup_prt_cnsg_ovr entries.
					 */
					cnsg = 1L;
				}
				else if (misTrimStrncmp(trk_cnsg_cod, TRK_CNSG_TRKPARTS,
					TRK_CNSG_COD_LEN) == 0)
				{
					/* We should track consignment for the supplier/client only for
					 * parts specified in sup_prt_cnsg_ovr.
					 */
					if (sqlGetLong(cnsgRes, cnsgRow, "prt_specified") == 1)
						cnsg = 1L;
				}
				else if (misTrimStrncmp(trk_cnsg_cod, TRK_CNSG_TRKPRTSEXPT,
					TRK_CNSG_COD_LEN) == 0)
				{
					/* We should track consignment for the supplier/client only for
					 * parts not specified in sup_prt_cnsg_ovr.
					 */
					if (sqlGetLong(cnsgRes, cnsgRow, "prt_specified") == 0)
						cnsg = 1L;
				}
			}
		}
		if (CmdRes)
		{
			srvFreeMemory(SRVRET_STRUCT, CmdRes);
			CmdRes = NULL;
			cnsgRes = NULL;
			cnsgRow = NULL;
		}
	}
    else
    {
        cnsg = 0L;
    } /*End code merge*/
	
    misTrc(0, "Consignment tracking requirements met for inventory? %ld", cnsg);
    
    /* Get the values and save them off */
    
    if (bGotInventorySpecifier && !sqlIsNull(Res, Row, "invsts"))
    {
	/* If they specified by inventory identifier, then we
	   use the status from the query...otherwise we only use
	   it if we didn't get on passed in.... */
	memset(invsts, 0, sizeof(invsts));
	misTrimcpy(invsts, sqlGetString(Res, Row, "invsts"), INVSTS_LEN);
    }
    else if (strlen(invsts) == 0 && !sqlIsNull(Res, Row, "invsts"))
    {
	misTrimcpy(invsts, sqlGetString(Res, Row, "invsts"), INVSTS_LEN);
    }
    
    /* Mixed Parts in the load/subload? */
    if (Row && sqlGetNextRow(Row))
    {
	strncpy(suffix, MIXLOD_SUFFIX, sizeof(suffix));
    }
    
    /* If we don't already have a suffix, then look at hvy, full, etc...
       the policies have to be defined, however, for us to consider  */
    if (strlen(suffix) == 0 &&
	(hvy_pal_wgt > 0 || ful_pal_pct > 0 || 
	 med_pal_pct > 0 || chk_for_par_case))
    {
	Row = sqlGetRow(Res);
	totqty = sqlGetLong(Res, Row, "totqty");
	grswgt = sqlGetFloat(Res, Row, "grswgt");
	untpal = sqlGetLong(Res, Row, "untpal");
	untcas = sqlGetLong(Res, Row, "untcas");
	
	if (untcas)
	    totwgt = totqty * (grswgt / (double) untcas);
	if (untpal == 0)
	    palpct = 100;
	else
	    palpct = (long) ((totqty * 100) / untpal);
	
	
	/* Figure where it will fit.  
	   (Weight takes precedence over qty unless it is 0) */
	if (hvy_pal_wgt != 0 && totwgt >= hvy_pal_wgt)
	{
	    strncpy(suffix, HVYWGT_SUFFIX, sizeof(suffix));
	}
	else if (palpct >= ful_pal_pct && ful_pal_pct != 0)
	{
	    strncpy(suffix, FULPAL_SUFFIX, sizeof(suffix));
	}
	else if (palpct >= med_pal_pct && med_pal_pct != 0)
	{
	    strncpy(suffix, HLFPAL_SUFFIX, sizeof(suffix));
	}
	else if (totqty < untcas)
	{
	    strncpy(suffix, PARCAS_SUFFIX, sizeof(suffix));
	}
    }
    sqlFreeResults(Res);
    
    if (cnsg == 1L)
        srvResultsAdd(CurPtr, CONSIGNED_SUFFIX);
    if (strlen(invsts))
    {
	sprintf(tmpstr, "%s-%s", INVSTS_SUFFIX, invsts);
	srvResultsAdd(CurPtr,
		      tmpstr);
    }
    
    if (strlen(suffix))
    {
	srvResultsAdd(CurPtr, 
		      suffix); 
    }

    /* Tack on the Default suffix.  Even though we don't tack this on when
     * creating the list of all suffixes, we'll add it here so that allocate
     * inventory is reminded to take it into account.
     */
    
    srvResultsAdd(CurPtr,
	          DEFPOL_SUFFIX);
    
    /* Get outta here... */
    return (CurPtr);
}
