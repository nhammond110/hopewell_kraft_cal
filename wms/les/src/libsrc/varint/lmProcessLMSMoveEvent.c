static const char *rcsid = "$Id: lmProcessLMSMoveEvent.c,v 1.2 2011/07/27 14:34:37 bflynn Exp $";
/*#START***********************************************************************
 *  RedPrairie Corportation
 *  Copyright 2003
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "lmlib.h"


/*
 * This routine is used to further refine the defined activity that
 * we are performing.
 *
 * If the command returns an error, the actcod gets nulled out
 * if the command doesn't return an actcod, we use what was passed in
 * if the command returns an actcod, we use that
 *
 */
static char *sGetDeferredActivityCode(char *dstare,
				      char *dstloc,
				      char *oprcod,
				      char *actcod,
				      char *srcare,
				      char *srcloc,
                                      char *wh_id)
{
    RETURN_STRUCT *CmdRes;
    char buffer[2000];
    long ret_status;
    static char ret_actcod[ACTCOD_LEN+1];
    mocaDataRes *res;
    mocaDataRow *row;
	
    memset(ret_actcod, 0, sizeof(ret_actcod));

    sprintf(buffer,
	    "get lms deferred activity code "
	    " where actcod = '%s' "
	    "   and oprcod = '%s' "
	    "   and srcare = '%s' "
	    "   and srcloc = '%s' "
	    "   and dstare = '%s' "
	    "   and dstloc = '%s' "
            "   and wh_id  = '%s' ",
	    actcod, oprcod, srcare, srcloc, dstare, dstloc,
            wh_id);

    CmdRes = NULL;
    ret_status = srvInitiateInline(buffer, &CmdRes);
    if (ret_status != eOK)
    {
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	return(ret_actcod);
    }
    res = srvGetResults(CmdRes);

    row = sqlGetRow(res);
    if (!sqlIsNull(res, row, "actcod"))
	misTrimcpy(ret_actcod, sqlGetString(res, row, "actcod"), ACTCOD_LEN);
    else
    {
	if (actcod)
	    misTrimcpy(ret_actcod, actcod, ACTCOD_LEN);
    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    
    return(ret_actcod);
}

static long sProcessDeferredUpdate(LMS_TRANSACTION_INFO *lms,
				   char *new_actcod_o)
{
    char buffer[2000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    char *actcod_ptr;
    strcpy(new_actcod_o, lms->actcod);

    /*
     * Coming in, we have an LMS PLACE transaction. We need to do a 
     * couple of things:
     *   1.  Find out if there are any deferred transactions for this
     *       device.
     *   2.  For the deferred transactions, we must call "get lms deferred
     *       activity code"
     *   3.  Once we have a new activity code, update the actcod
     */

    sprintf(buffer,
	    "select * "
	    "  from lmstrn "
	    " where devcod = '%s' "
            "   and wh_id  = '%s' "
	    "   and hldflg = %d "
	    "   and ('%s' in (lodnum, dstlod) or "
	    "        '%s' in (subnum, dstsub)) "
	    " order by begdte ",
	    lms->devcod, 
            lms->wh_id,
            BOOLEAN_TRUE, 
            lms->lodnum, 
            lms->subnum);

    ret_status = sqlExecStr(buffer, &res);

    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return(eOK);
    }

    memset(new_actcod_o, 0, ACTCOD_LEN);
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	if (strlen(new_actcod_o) == 0)
	{
	    actcod_ptr =
		sGetDeferredActivityCode(lms->dstare, 
					 lms->dstloc,
					 lms->oprcod,
					 sqlGetString(res, row, "actcod"),
					 sqlGetString(res, row, "srcare"),
					 sqlGetString(res, row, "srcloc"),
                                         lms->wh_id);
	    if (strlen(actcod_ptr))
		strcpy(new_actcod_o, actcod_ptr);
	    else
		strcpy(new_actcod_o, sqlGetString(res, row, "actcod"));

	}
	sprintf(buffer, 
		"update lmstrn"
		"   set hldflg = %d, "
		"       actcod = '%s' "
		" where lms_trn_id = '%s' ",
		BOOLEAN_FALSE, new_actcod_o, 
		sqlGetString(res, row, "lms_trn_id"));
	ret_status = sqlExecStr(buffer, NULL);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return(ret_status);
	}
    }
    sqlFreeResults(res);
    return(eOK);
}

/*
 * This routine covers logging any indirect activity necessary for things
 * like picking up an empty pallet or empty carton to perform cartonization
 * or pick to pallet.
 */
static long sProcessIndirect(LMS_TRANSACTION_INFO *lms,
			     char *ctnnum,
			     char *lodlvl)
{
    char buffer[2000];
    long ret_status;

    LMS_TRANSACTION_INFO ind;

    memset(buffer, 0, sizeof(buffer));
    
    if (strcmp(lodlvl, LODLVL_SUB) == 0)
    {
	/* If this is the only other sub on this pallet, then
	   we know we have to register an indirect */
	sprintf(buffer,
		"select 1 "
		"  from dual "
		" where exists "
		"  (select 1 "
		"     from invsub "
		"    where lodnum = '%s' "
		"      and subnum != '%s')",
		lms->dstlod, lms->subnum);
    }
    else if (strcmp(lodlvl, LODLVL_DTL) == 0)
    {
	/* If this is the only other piece in this case, and
	   we are cartonizing, then we know we have to 
	   register an indirect */
	if (strcmp(ctnnum, lms->dstsub) == 0)
	{
	    sprintf(buffer,
		    "select 1 "
		    "  from dual "
		    " where exists "
		    "  (select 1 "
		    "     from invdtl "
		    "    where subnum = '%s' "
		    "      and dtlnum != '%s')",
		    lms->dstsub, lms->dtlnum);
	}
    }
    
    if (strlen(buffer))
    {
	ret_status = sqlExecStr(buffer, NULL);
	if (ret_status == eDB_NO_ROWS_AFFECTED)
	{
	    misTrc(T_FLOW, "Logging INDIRECT activity transaction");
	    memset(&ind, 0, sizeof(ind));

	    strcpy(ind.lmscod, LMSCOD_INDIRECT);
	    strcpy(ind.lmstyp, LMSTYP_OBTAIN);
	    strcpy(ind.srcare, lms->srcare);
	    strcpy(ind.dstare, lms->dstare);
	    strcpy(ind.srcloc, lms->srcloc);
	    strcpy(ind.dstloc, lms->dstloc);
	    strcpy(ind.usr_id, lms->usr_id);
	    strcpy(ind.actcod, lms->actcod);
	    strcpy(ind.oprcod, lms->oprcod);
	    strcpy(ind.devcod, lms->devcod);
            strcpy(ind.wh_id,  lms->wh_id);
	    strcpy(ind.prtnum,  
		   (strcmp(lodlvl, LODLVL_SUB) == 0 ? 
		    "Pallet Obtain" : "Carton Obtain"));
	    ind.palqty = ind.casqty = ind.untqty = 0;
	    ind.untwgt = ind.untvol = 0.0;

	    ret_status = lm_WriteLMSActuals(&ind);
	    if (ret_status != eOK)
		return(ret_status);
	}
	else if (ret_status != eOK)
	{
	    return(ret_status);
	}
    }
    return(eOK);
}    
static long sGetMoveEventQuantities(char *lodnum,
                                    char *subnum,
                                    char *dtlnum,
                                    char *dstlod,
                                    char *dstsub,
                                    char *actcod,
                                    char *prtnum_o,   /* out */
                                    char *prt_client_id_o,
                                    char *prtdsc_o,
                                    long *palqty_o,
                                    long *casqty_o,
                                    long *untqty_o,
                                    double *untvol_o,
                                    double *untwgt_o)
{
    char buffer[2000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *CmdRes;

    sprintf(buffer,
            "get lms move event quantities "
            " where lodnum = '%s' "
            "   and subnum = '%s' "
            "   and dtlnum = '%s' "
            "   and actcod = '%s' ",
            lodnum, subnum, dtlnum, actcod);
    CmdRes = NULL;
    ret_status = srvInitiateCommand(buffer, &CmdRes);

    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
    /* We need to check if we should use dstlod/dstsub/dstdtl instead of
     * lodnum/subnum/dtlnum.
     * That's because when we call this command, we have finished the inventory
     * movement, and the original load/sub/detail number could be deleted since
     * all the inventory has been moved to new load.
     * Conside that at this time, dstlod/dstsub/dstdtl should always has the
     * finial quantity, so we will use dstlod/dstsub/dstdtl if we have them
     * passed in.
     */

        if (dstlod && misTrimLen(dstlod, LODNUM_LEN) > 0 ||
            dstsub && misTrimLen(dstsub, LODNUM_LEN) > 0)
        {
            sprintf(buffer,
                    "get lms move event quantities "
                    " where lodnum = '%s' "
                    "   and subnum = '%s' "
                    "   and actcod = '%s' "
                    "   and dstflg = 1 ",
                    dstlod, dstsub, actcod);
        }
        CmdRes = NULL;
        ret_status = srvInitiateCommand(buffer, &CmdRes);
    }
    if (ret_status != eOK)
    {
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	return(ret_status);
    }
    res = srvGetResults(CmdRes);
    row = sqlGetRow(res);

    strcpy(prtnum_o, sqlGetString(res, row, "prtnum"));
    strcpy(prt_client_id_o, sqlGetString(res, row, "prt_client_id"));
    strcpy(prtdsc_o, sqlGetString(res, row, "prtdsc"));
    *palqty_o = sqlGetLong(res, row, "palqty");
    *casqty_o = sqlGetLong(res, row, "casqty");
    *untqty_o = sqlGetLong(res, row, "untqty");

    *untvol_o = sqlGetFloat(res, row, "untvol");
    *untwgt_o = sqlGetFloat(res, row, "untwgt");

    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    return(eOK);
}


LIBEXPORT 
RETURN_STRUCT *lmProcessLMSMoveEvent(char *usr_id_i, 
				      char *actcod_i,
				      char *wrkref_i,
				      char *oprcod_i, 
				      char *lodnum_i,
				      char *subnum_i, 
				      char *dtlnum_i, 
				      char *srcloc_i,
				      char *dstloc_i,
				      char *dstlod_i, 
				      char *dstsub_i,
				      char *srcare_i,
				      char *dstare_i,
				      char *wh_id_i)
{

    long ret_status;
    int  rowCount = 0;
    long need_obtain_and_place;
    long src_LMTracked;
    long dst_LMTracked;
    long src_DXDK;
    long dst_DXDK;
    long i;

    char *p;
    char buffer[8000];
    char wrkref[WRKREF_LEN+1];
    char lodlvl[LODLVL_LEN+1];

    char ctnnum[CTNNUM_LEN+1];
    char actcod[ACTCOD_LEN+1];
    long isPickForOrder = 0;

    mocaDataRes *res;
    mocaDataRow *row;

    LMS_TRANSACTION_INFO lms;

    static int isInstalled = -1;

    /*
     * The LMS_TRANSACTION_INFO is a structure which contains all the 
     * information necessary to log either a CALM actual or a deferred
     * transaction.
     *
     * A "deferred transaction" is one where we can't make the decision
     * on what type of activity it is until we get to our destination - 
     * i.e. rcv -> rdt   rdt->storage    CALM wants to view that as a 
     * move from rcv->storage, therefore it is a putaway.  Otherwise, if 
     * the second half of the move was rdt->stage, then it might be called
     * a 'receipt'.  Bottom line, if we're moving on to an RDT (DIRECT and 
     * OBTAIN), the we have to wait until the unit is placed in order to 
     * determine the activity - and yes...that is a bit of a pain in the 
     * shorts.
     */

    memset(wrkref, 0, sizeof(wrkref));
    memset(&lms,   0, sizeof(lms));

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))  
    {  
        misTrimcpy(lms.wh_id, wh_id_i, WH_ID_LEN);  
    }
    else
    {
        return(APPMissingArg("wh_id"));
    }  

    if (isInstalled == -1)
    {
	if (appIsInstalledWhID(POLCOD_LMS, lms.wh_id))
	    isInstalled = 1;
	else
	    isInstalled = 0;
    }
    
    if (!isInstalled)
    {
	misTrc(T_FLOW, "Skipping LMS processing as option is not installed. ");
	return(srvResults(eOK, NULL));
    }

    /* 
     * As we work our way through what was passed in, we'll accumulate
     * it all in the lms structure.  All elements of that structure are
     * written in this module.  It is then passed to some supporting 
     * library routines to be used to write out deferrals or actuals 
     */
    if (actcod_i && misTrimLen(actcod_i, ACTCOD_LEN) > 0) 
	misTrimcpy(lms.actcod, actcod_i, ACTCOD_LEN);

    lm_GetUserId(usr_id_i, lms.usr_id);

    if ((p = osGetVar(LESENV_DEVCOD)) != NULL)
	misTrimcpy(lms.devcod, p, DEVCOD_LEN);
    
    if (oprcod_i && misTrimLen(oprcod_i, OPRCOD_LEN) > 0) 
	misTrimcpy(lms.oprcod, oprcod_i, OPRCOD_LEN);

    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
	misTrimcpy(lms.lodnum, lodnum_i, LODNUM_LEN);

    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
	misTrimcpy(lms.subnum, subnum_i, SUBNUM_LEN);

    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
	misTrimcpy(lms.dtlnum, dtlnum_i, DTLNUM_LEN);

    if (!lms.lodnum[0] && !lms.subnum[0] && !lms.dtlnum[0])
    {
	misTrc(T_FLOW, "Did not receive load/sub/detail - unable to process!");
	return(srvResults(eINVALID_ARGS, NULL));
    }

    if (srcloc_i && misTrimLen(srcloc_i, SRCLOC_LEN) > 0) 
	misTrimcpy(lms.srcloc, srcloc_i, SRCLOC_LEN);
    else
    {
	misTrc(T_FLOW, 
	       "Did not receive source location - unable to process");
	return(APPMissingArg("srcloc"));
    }
    
    if (dstloc_i && misTrimLen(dstloc_i, DSTLOC_LEN) > 0) 
	misTrimcpy(lms.dstloc, dstloc_i, DSTLOC_LEN);
    else
    {
	misTrc(T_FLOW, 
	       "Did not receive destination location - unable to process");
	return(APPMissingArg("dstloc"));
    }

    if (dstlod_i && misTrimLen(dstlod_i, LODNUM_LEN) > 0) 
	misTrimcpy(lms.dstlod, dstlod_i, LODNUM_LEN);
    
    if (dstsub_i && misTrimLen(dstsub_i, SUBNUM_LEN) > 0) 
	misTrimcpy(lms.dstsub, dstsub_i, SUBNUM_LEN);

    ret_status = lm_GetArea(srcare_i, lms.srcloc, lms.srcare, lms.wh_id);
    if (ret_status != eOK)
    {
	return(APPMissingArg("srcare"));
    }

    ret_status = lm_GetArea(dstare_i, lms.dstloc, lms.dstare, lms.wh_id);
    if (ret_status != eOK)
    {
	return(APPMissingArg("dstare"));
    }

    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN) > 0) 
       misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);

    /*
    ** ------------------------------------------------------
    **  Skip automation moves
    ** ------------------------------------------------------
    */
    if (lm_IsFromAutomation(lms.usr_id))
    {
	misTrc(T_FLOW, "Got move event from automation - skipping");
	return(srvResults(eOK, NULL));
    }

    /*
    ** ---------------------------------------------------------- 
    ** Suppress MvE for workstation activities unless specifically
    ** requested (oprcod not null). 
    **
    ** NOTE:  This is a shaky check at best. We really should have
    **        something besides the implicit presence of an OPRCOD
    **        determine if we want to log something
    ** ---------------------------------------------------------- 
    */

    if (lm_IsWorkstation(lms.wh_id) && strlen(lms.oprcod) == 0)
    {
	misTrc(T_FLOW, 
	       "Got move event from workstation with no oprcod specified - "
	       " skipping");
	return(srvResults(eOK, NULL));
    }

    /*
    ** ---------------------------------------------------------- 
    ** Suppress MvE for cycle-counting IADJs 
    ** ---------------------------------------------------------- 
    */
    if (lm_IsCycleCountMove(lms.srcloc, lms.dstloc, lms.wh_id))
    {
	misTrc(T_FLOW, "Skipping cycle count move");
	return(srvResults(eOK, NULL));
    }


    /*
     * Identification moves get picked up by the Identification Event
     */
    if (lm_IsIdentificationMove(lms.srcare, lms.dstare, lms.wh_id))
    {
	misTrc(T_FLOW, 
	       "Skipping move event as it is part of identification event");
	return(srvResults(eOK, NULL));
    }

    /*
    ** ---------------------------------------------------- 
    ** Get PCKWRK data associated with WRKREF if not NULL.
    ** ---------------------------------------------------- 
    */

    if (wrkref[0])
    {
	misTrc(T_FLOW, 
	       "Move with wrkref - attempting to determine type of pick");

	memset(buffer, 0, sizeof(buffer));
	sprintf(buffer,
		"select prtnum, "
                "       prt_client_id, " 
		"       lodlvl, "
		"       ctnnum, "
		"       decode(wrktyp,"
		"             '%s', "
		"             decode(lodlvl, '%s', '%s', "
		"                            '%s', '%s', "
		"                            '%s', '%s', '%s'), "
		"             '%s', '%s', "
                "             '%s', "
                "             decode(lodlvl, '%s', '%s', "
                "                            '%s', '%s', '%s'), "
                "             '%s') actcod, "
                "       stcust adrnum, ship_id, "
                "       lm_assign_num, "
                "       lm_assign_seqnum "
		"  from pckwrk" 
		" where wrkref = '%s'", 
		WRKTYP_PICK, 
		LODLVL_LOAD, ACTCOD_PALLET_PICK,
		LODLVL_SUB,  ACTCOD_CASE_PICK,
		LODLVL_DTL,  ACTCOD_PIECE_PICK, ACTCOD_OTHER_PICK,
		WRKTYP_KIT,  ACTCOD_KIT_PICK,
                WRKTYP_REPLENISH, 
		LODLVL_LOAD, ACTCOD_PALLET_REPLENISHMENT,
		LODLVL_SUB,  ACTCOD_CASE_REPLENISHMENT, ACTCOD_OTHER_REPLENISHMENT,
		ACTCOD_GENERIC_MOVE,
		wrkref);
	
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
            if (ret_status == eDB_NO_ROWS_AFFECTED)
            {
                /* This is the case of a replenishment... */
                if (lms.subnum[0])
                    strcpy(lms.actcod, ACTCOD_CASE_REPLENISHMENT);
                else if (lms.lodnum[0])
                    strcpy(lms.actcod, ACTCOD_PALLET_REPLENISHMENT);
                else
                    strcpy(lms.actcod, ACTCOD_OTHER_REPLENISHMENT);
            }
            else
            {
                sqlFreeResults(res);
                return(srvResults(ret_status, NULL));
            }
	}
	else
	{
	    isPickForOrder = 1;

	    row = sqlGetRow(res);
	    memset(lodlvl, 0, sizeof(lodlvl));
	    memset(ctnnum, 0, sizeof(ctnnum));
	    misTrimcpy(lms.actcod,
		       sqlGetString(res, row, "actcod"), ACTCOD_LEN);
	    misTrimcpy(lms.adrnum, 
		       sqlGetString(res, row, "adrnum"), ADRNUM_LEN);
	    misTrimcpy(lodlvl,
		       sqlGetString(res, row, "lodlvl"), LODLVL_LEN);
	    if (!sqlIsNull(res, row, "ctnnum"))
		misTrimcpy(ctnnum, 
			   sqlGetString(res, row, "ctnnum"), CTNNUM_LEN);
            misTrimcpy(lms.lmsasn,
                       sqlGetString(res, row, "lm_assign_num"), LM_ASSIGN_NUM_LEN);
            lms.grpseq = sqlGetLong(res, row, "lm_assign_seqnum");
	}	
	sqlFreeResults(res);
    }
    else
    {
    /* Check if this is a trailer load  and current is not fluid loading */
    if (lm_IsShipStagingArea(lms.srcare, lms.wh_id) && 
        lm_IsShipmentArea(lms.dstare, lms.wh_id) && 
        strcmp(lms.actcod, ACTCOD_FLUID_LOAD))
	{
	    strcpy(lms.actcod, ACTCOD_SHIPMENT_LOAD);
	}
	else if (lm_IsShipAdjustmentMove(lms.dstare, lms.dstloc, lms.wh_id))
	{
	    strcpy(lms.actcod, ACTCOD_SHIPMENT_ADJUST);
	}
    /* current activity code is not fluid loading */
    else if(strcmp(lms.actcod, ACTCOD_FLUID_LOAD))
    {
	    /* 
	     * Didn't get a wrkref in, make a cursory check to see if 
	     * this pallet is already associated to an order.
	     */
	    if (strlen(lms.dtlnum))
	    {
		sprintf(buffer,
			"select distinct pw.stcust adrnum, pw.lodlvl, "
			"                pw.wrktyp "
			"  from pckwrk pw, invdtl id "
			" where id.dtlnum = '%s' "
			"   and pw.wrkref = id.wrkref ",
			lms.dtlnum);
	    }
	    else if (strlen(lms.subnum))
	    {
		sprintf(buffer,
			"select distinct pw.stcust adrnum, pw.lodlvl, "
			"                pw.wrktyp "
			"  from pckwrk pw, invdtl id "
			" where id.subnum = '%s' "
			"   and pw.wrkref = id.wrkref ",
			lms.subnum);
	    }
	    else
	    {
		sprintf(buffer,
			"select distinct pw.stcust adrnum, pw.lodlvl, "
			"                pw.wrktyp "
			"  from pckwrk pw, invdtl id, invsub ib "
			" where ib.lodnum = '%s' "
			"   and ib.subnum = id.subnum "
			"   and pw.wrkref = id.wrkref ",
			lms.lodnum);
	    }
	    ret_status = sqlExecStr(buffer, &res);
	    if (ret_status == eOK)
	    {
		row = sqlGetRow(res);
		if (!sqlIsNull(res, row, "adrnum"))
		{
		    if (sqlGetNumRows(res) == 1)
			strcpy(lms.adrnum, sqlGetString(res, row, "adrnum"));
		    else
			strcpy(lms.adrnum, LMS_MIXED_ADRNUM_STR);
		}
		if (sqlGetNumRows(res) == 1)
		{
		    if (strcmp(LODLVL_LOAD, 
			       sqlGetString(res, row, "lodlvl")) == 0)
			strcpy(lms.actcod, ACTCOD_PALPCK_MOV);
		    else if (strcmp(LODLVL_SUB, 
				    sqlGetString(res, row, "lodlvl")) == 0)
			strcpy(lms.actcod, ACTCOD_CASPCK_MOV);
		    else 
			strcpy(lms.actcod, ACTCOD_PCEPCK_MOV);
		}
		else 
		{
		    if (strlen(lms.subnum))
			strcpy(lms.actcod, ACTCOD_ORD_MIX_CASMOV);
		    else if (strlen(lms.lodnum))
			strcpy(lms.actcod, ACTCOD_ORD_MIX_PALMOV);
		}
	    }
	    sqlFreeResults(res);
	}
    }
		    

    /*
    ** --------------------------------------------------------
    ** Next, let's determine if we are doing a PLACE or OBTAIN
    ** We are doing an OBTAIN if our destination area is an RDT
    ** --------------------------------------------------------
    */
    need_obtain_and_place = 0;
    src_LMTracked = lm_IsLocationLMTracked(lms.srcloc, lms.wh_id);
    dst_LMTracked = lm_IsLocationLMTracked(lms.dstloc, lms.wh_id);
    src_DXDK = lm_IsDirectCrossDockMove(lms.srcare, lms.wh_id);
    dst_DXDK = lm_IsDirectCrossDockMove(lms.dstare, lms.wh_id);
    
   /* OBTAIN ONLY
    * NORMAL --> XDK
    * NORMAL --> RFT
    */
    if ((dst_LMTracked && !src_DXDK && !src_LMTracked) ||
        (dst_DXDK && !src_LMTracked && !src_DXDK))
    {
        strcpy(lms.lmstyp, LMSTYP_OBTAIN);
    }
    /* PLACE ONLY
    * RFT --> NORMAL
    * XDK --> NORMAL
    */
    else if ((src_LMTracked && !dst_DXDK && !dst_LMTracked) ||
             (src_DXDK && !dst_LMTracked && !dst_DXDK)) 
    {
        strcpy(lms.lmstyp, LMSTYP_PLACE);
    }
    /* BOTH PLACE AND OBTAIN
     * NORMAL --> NORMAL
     */
    else if (!src_LMTracked && !dst_LMTracked && !src_DXDK && !dst_DXDK)
    {
        need_obtain_and_place = 1;
    }
    /* IGNORE
     * RFT(XDK) -- >RFT(XDK)
     */
    else
    {
        misTrc(T_FLOW, 
	       "Skipping move event as it is move between Direct Cross Dock, "
	       " RDT or Voice.");
		return(srvResults(eOK, NULL));
    }

    strcpy(lms.lmscod, LMSCOD_DIRECT);

    
    /* Next determine the quantities to tell CALM about */
    misTrc(T_FLOW, "Determing move event quantities");

    ret_status = sGetMoveEventQuantities(lms.lodnum,
                                         lms.subnum,
                                         lms.dtlnum,
                                         lms.dstlod,
                                         lms.dstsub,
                                         lms.actcod,
                                         lms.prtnum,   /* out */
                                         lms.prt_client_id,
                                         lms.prtdsc,
                                         &lms.palqty,
                                         &lms.casqty,
                                         &lms.untqty,
                                         &lms.untvol,
                                         &lms.untwgt);
    

    /*
    **-------------------------------------------- 
    ** Check if we need to send an indirect pickup
    **-------------------------------------------- 
    */

    
    for (i = 0; i <= need_obtain_and_place; i++)
    {
	/*
	 * Here's the deal:  if we are doing a move that doesn't 
	 * involve an RF, we still want to play an OBTAIN and a
	 * PLACE...  we detected that case above and it is the
	 * reason we are now looping...the first time through
	 * this loop, we want an OBTAIN...the next time through
	 * we'll do the place
	 */
	if (need_obtain_and_place > 0)
	{
	    if (i == 0)
		strcpy(lms.lmstyp, LMSTYP_OBTAIN);
	    else
		strcpy(lms.lmstyp, LMSTYP_PLACE);
	}

	
	if (strcmp(lms.lmstyp, LMSTYP_OBTAIN) == 0)
	{
	    /*
	    ** In the event of an OBTAIN, we may need to send an indirect 
	    ** transaction for either an empty pallet pickup or a carton
	    ** pickup...this is true if we are picking (for order) and
	    ** we have a case or piece pick 
	    */

	    /********************************************************
	     * We no longer send indirects...the following code 
	     * has been commented out... 
	     ********************************************************
	if (isPickForOrder)
	{
	    ret_status = sProcessIndirect(&lms,
					  ctnnum,
					  lodlvl);
	    if (ret_status != eOK)
		return(srvResults(ret_status, NULL));
	}
	********************END OF COMMENTED OUT CODE***************/

	    if (strcmp(lms.lmscod, LMSCOD_DIRECT) == 0)
	    {
            /* 
             * This is a DIRECT, OBTAIN, which means we need to defer the
             * transaction so that a judgement can be made as to the activity
             * after the deposit is made. 
             * However we don't need write transaction if current if fluid
             * loading.
             */
             if (strcmp(lms.actcod, ACTCOD_FLUID_LOAD))
             {
                 lms.hldflg = 1;
                 ret_status = lm_WriteDeferredTransaction(&lms);
                 if (ret_status != eOK)
                     return(srvResults(ret_status, NULL));
             }
             else
             {
                 misTrc(T_FLOW, 
                   "Now is fluid loading, we set it FLDLD.");
                 strcpy(lms.actcod, ACTCOD_FLUID_LOAD);
             }
        }
    }
	else
	{
	    /* 
	     * This is a PLACE from an RDT - we need to first worry about
	     * catching up with any outstanding deferrals..
	     */

            /*
             * Since we have sent the weight and volume to LMS at the time
             * of the obtain, we don't need
             * to send them twice. See PR 49747
             */ 
            lms.untwgt = 0;
            lms.untvol = 0;
	    
	    memset(actcod, 0, sizeof(actcod));
	    ret_status = sProcessDeferredUpdate(&lms, actcod);
	    if (ret_status != eOK)
		return(srvResults(ret_status, NULL));
        /*
         * If current activity code is fluid loading, it must be different
         * from the previous one which was inserted while picking product, 
         * it might be PALPCK/SUBPCK/PCEPCK. But now we are setting it as 
         * SHIPLOAD because we will write 4 lmsact of srcloc(O)->stglane(P), 
         * stglane(O)->trlr(P), and what we only need is srcloc(O)->trlr(P) 
         * for fluid loading.
         */
        if (strcmp(lms.actcod, ACTCOD_FLUID_LOAD))
        {
            if (strcmp(actcod, lms.actcod) != 0)
            {
                misTrc(T_FLOW, 
                       "Deferred moves changed actcod - old: %s, new: %s",
                       lms.actcod, actcod);
                strcpy(lms.actcod, actcod);
            }
        }
        else
        {
            misTrc(T_FLOW, 
                   "Now is fluid loading, we won't write "
                   "current transaction.");
        }
        /*
         * Now write the actual for the place
         */
        ret_status = lm_WriteLMSActuals(&lms);
        if (ret_status != eOK)
            return(srvResults(ret_status, NULL));
	}
    }
    return(srvResults(eOK, 
		      "actcod", COMTYP_CHAR, ACTCOD_LEN, lms.actcod, NULL));
}

