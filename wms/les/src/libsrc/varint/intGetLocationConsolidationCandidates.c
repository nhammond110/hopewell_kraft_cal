static const char *rcsid = "$Id: intGetLocationConsolidationCandidates.c,v 1.3 2012/02/03 04:21:33 jliang Exp $";
/*#START***********************************************************************
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "intlib.h"

static void sGetInvsts(char *invsts_o,
                       char *prtnum,
                       char *prt_client_id,
                       char *srcloc,
                       char *wh_id)
{
    char buffer[1000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;

    sprintf(buffer,
            "select distinct id.invsts "
            "  from invdtl id, invsub ivs, invlod il"
            " where id.prtnum = '%s' "
            "   and id.prt_client_id = '%s' "
            "   and il.stoloc = '%s' "
            "   and il.wh_id = '%s' "
            "   and il.lodnum = ivs.lodnum "
            "   and id.subnum = ivs.subnum ",
            prtnum, prt_client_id, srcloc, wh_id);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        return;
    }
    row = sqlGetRow(res);
    strncpy(invsts_o, sqlGetString(res, row, "invsts"), INVSTS_LEN);
    return;
}

static void sGetDestinationArea(char *dstare_o,
                                char *dstloc,
                                char *wh_id)
{
    char buffer[1000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *Cmd;

    sprintf(buffer,
            " get location information "
            " where stoloc = '%s' "
            "   and wh_id = '%s' ",
            dstloc, wh_id);
            
    ret_status = srvInitiateInline(buffer, &Cmd);
    
    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, Cmd);
        return;
    }
    
    res = srvGetResults(Cmd);
    row = sqlGetRow(res);
    
    strncpy(dstare_o, sqlGetString(res, row, "arecod"), ARECOD_LEN);
    srvFreeMemory(SRVRET_STRUCT, Cmd);
    return;
}

static long sProcessSource(char *srcare,
                           char *dstare_i,
                           char *wh_id,
                           char *cons_rule_cod,
                           char *src_cmd,
                           long pceqty,
                           long casqty,
                           long palpct,
                           char *prtnum_i,
                           char *prtfam_i,
                           RETURN_STRUCT **CmdRes)
{
    mocaDataRes *res, *tres;
    mocaDataRow *row, *trow;

    RETURN_STRUCT *Cmd, *Rpl;
    char buffer[4000];
    long ret_status;
    long errcode;
    char lodnum[LODNUM_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char prtfam[PRTFAM_LEN+1];
    char prtnum_buffer[100];
    char prtfam_buffer[100];
    char dstare[ARECOD_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char srcloc[STOLOC_LEN+1];
    char invsts[INVSTS_LEN+1];
    long untqty;
    long pndqty;
    char dstloc[STOLOC_LEN+1];

    memset(prtnum_buffer, 0, sizeof(prtnum_buffer));
    memset(prtfam_buffer, 0, sizeof(prtfam_buffer));
    
    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
        sprintf(prtnum_buffer, " and prtnum = '%s' ", prtnum_i);
        
    if (prtfam_i && misTrimLen(prtfam_i, PRTFAM_LEN))
        sprintf(prtfam_buffer, " and prtfam = '%s' ", prtfam_i);
    	
    /*if (strncmp(cons_rule_cod, CONS_RULE_UNIT_COUNT, CONS_RULE_COD_LEN) == 0)
    {
        sprintf(buffer,
                "[select wh_id, stoloc, prtnum, prt_client_id, invsts,"
                "        untqty, pndqty "
                "   from invsum "
                "  where arecod = '%s' "
                "    and wh_id = '%s' "
                "    and untqty <= %d "
                "    and untqty > 0 "
                "    and comqty = 0] ",
                srcare, wh_id, pceqty);
    }
    else if (strncmp(cons_rule_cod,
                     CONS_RULE_PALLET_PCT, CONS_RULE_COD_LEN) == 0)
    {
        sprintf(buffer,
                "[select ivs.wh_id, ivs.stoloc, ivs.prtnum, ivs.prt_client_id, "
                "        ivs.invsts, "
                "        ivs.untqty, ivs.pndqty "
                "   from prtmst_view pm, invsum ivs"
                "  where ivs.arecod = '%s' "
                "    and ivs.wh_id = '%s' "
                "    and pm.prtnum = ivs.prtnum "
                "    and pm.prt_client_id = ivs.prt_client_id "
                "    and pm.wh_id  = ivs.wh_id "
                "    and (ivs.untqty / (pm.untpal * 1.0)) * 100 <= %d "
                "    and ivs.untqty > 0 "
                "    and ivs.comqty = 0] ",
                srcare, wh_id, palpct);
        

    }
    else if (strncmp(cons_rule_cod,
                     CONS_RULE_CASE_COUNT, CONS_RULE_COD_LEN) == 0)
    {

        sprintf(buffer,
                "[select ivs.wh_id, ivs.stoloc, ivs.prtnum, ivs.prt_client_id,  "
                "        ivs.invsts, ivs.untqty, ivs.pndqty "
                "   from prtmst_view pm, invsum ivs"
                "  where ivs.arecod = '%s' "
                "    and ivs.wh_id = '%s' "
                "    and pm.prtnum = ivs.prtnum "
                "    and pm.wh_id  = ivs.wh_id "
                "    and pm.prt_client_id = ivs.prt_client_id "
                "    and (ivs.untqty / pm.untcas * 1.0) <= %d "
                "    and ivs.untqty > 0 "
                "    and ivs.comqty = 0] ",
                srcare, wh_id, casqty);

    }*/
    if (strncmp(cons_rule_cod,
                     CONS_RULE_COMMAND, CONS_RULE_COD_LEN) == 0)
    {
        sprintf(buffer,
                "publish data "
        		"	where srcare = '%s' "
                "     and wh_id = '%s' "
                "     and casqty = %d "
                "     and pceqty = %d "
                "     and palpct = %d "
                "     %s "
                "     %s "
        		"| "
                " %s ",
                srcare, wh_id, casqty, pceqty, palpct, prtnum_buffer, prtfam_buffer, src_cmd);
    }

    misTrc( T_FLOW, "Execute the command as src_cmd->(%s) and parameters are: srcare ->(%s) wh_id->(%s) prtnum->(%s) prtfam->(%s) and palpct->(%ld) ",
		    src_cmd, srcare, wh_id, prtnum_i, prtfam_i, palpct );
    ret_status = srvInitiateInline(buffer, &Cmd);
    if (ret_status != eOK && 
        ret_status != eDB_NO_ROWS_AFFECTED &&
        ret_status != eSRV_NO_ROWS_AFFECTED)
    {
        srvFreeMemory(SRVRET_STRUCT, Cmd);
        return(ret_status);
    }

    if (ret_status == eDB_NO_ROWS_AFFECTED ||
        ret_status == eSRV_NO_ROWS_AFFECTED)
    {
        srvFreeMemory(SRVRET_STRUCT, Cmd);
        return(eOK);
    }

    if (*CmdRes == NULL)
    {
        /* Initialize the structure if it has not yet been initialized. */
        *CmdRes = srvResultsInit(eOK,
                             "srcloc", COMTYP_STRING, STOLOC_LEN,
                             "lodnum", COMTYP_STRING, LODNUM_LEN,
                             "srcare", COMTYP_STRING, ARECOD_LEN,
                             "wh_id",  COMTYP_STRING, WH_ID_LEN,
                             "prtnum", COMTYP_STRING, PRTNUM_LEN,
                             "prtfam", COMTYP_STRING, PRTFAM_LEN,
                             "prt_client_id", COMTYP_STRING, CLIENT_ID_LEN,
                             "invsts", COMTYP_STRING, INVSTS_LEN,
                             "dstloc", COMTYP_STRING, STOLOC_LEN,
                             "dstare", COMTYP_STRING, ARECOD_LEN,
                             "srcqty", COMTYP_LONG, sizeof(long),
                             "pndqty", COMTYP_LONG, sizeof(long),
                             "status", COMTYP_LONG, sizeof(long),
                             NULL);
    }

    res = srvGetResults(Cmd);

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        memset(srcloc, 0, sizeof(srcloc));
        memset(lodnum, 0, sizeof(lodnum));
        memset(prtnum, 0, sizeof(prtnum));
        memset(prtfam, 0, sizeof(prtfam));
        memset(prt_client_id, 0, sizeof(prt_client_id));
        memset(invsts, 0, sizeof(invsts));
        memset(dstloc, 0, sizeof(dstloc));
        
        misTrimcpy(srcloc, sqlGetString(res, row, "stoloc"), STOLOC_LEN);
        misTrimcpy(prtnum, sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
        misTrimcpy(prtfam, sqlGetString(res, row, "prtfam"), PRTFAM_LEN);
        misTrimcpy(lodnum, sqlGetString(res, row, "lodnum"), LODNUM_LEN);
        misTrimcpy(prt_client_id,
                   sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
        untqty = sqlGetLong(res, row, "untqty");
        pndqty = sqlGetLong(res, row, "pndqty");

        if (!sqlIsNull(res, row, "invsts"))
            misTrimcpy(invsts, sqlGetString(res, row, "invsts"), INVSTS_LEN);
        else
            sGetInvsts(invsts,
                       prtnum,
                       prt_client_id,
                       srcloc,
                       wh_id);
        /* double check the location with no pending quantity */
        /* to avoid moves back and forth between locations. */   
        sprintf(buffer,
				"select 'x' "
				"  from invsum "
				" where stoloc = '%s' "
				"   and wh_id = '%s' "
				" group by stoloc "
			    "having sum(invsum.pndqty) = 0 ",
				srcloc,
				wh_id);   
		
	    ret_status = sqlExecStr(buffer, NULL);
	    
	    if(ret_status == eOK)
	    {          
	        sprintf(buffer,
	            	" error location "
	            	" where stoloc = '%s'"
	            	"   and wh_id = '%s'",
	                srcloc,
	                wh_id);               
	                       
	        /* Don't really care about this status */
	        errcode = srvInitiateCommand(buffer, NULL);
	
	        misTrc( T_FLOW, "Calling allocate location: srcloc->(%s) wh_id->(%s) lodnum->(%s) "
	                " and invmov_type = 'RCV'", srcloc, wh_id, lodnum );
	        sprintf(buffer,
	                "allocate location "
	                "  where wh_id = '%s' "
	                "    and lodnum = '%s' "
	                "    and invmov_typ = 'RCV' ",
	                wh_id, lodnum);
	
	        memset(dstloc, 0, sizeof(dstloc));
	        memset(dstare, 0, sizeof(dstare));
	        
	        ret_status = srvInitiateInline(buffer, &Rpl);
	        
	        if (ret_status == eOK)
	        {
	            tres = srvGetResults(Rpl);
	            trow = sqlGetRow(tres);
	
	            if (!sqlIsNull(tres, trow, "nxtloc"))
	            {
	                misTrimcpy(dstloc, 
	                           sqlGetString(tres, trow, "nxtloc"), STOLOC_LEN);
	                
	                sGetDestinationArea(dstare, dstloc, wh_id);
	            }
	        }
	        srvFreeMemory(SRVRET_STRUCT, Rpl); 
	        
	        sprintf(buffer,
            	" reset location status "
            	" where stoloc = '%s' "
            	"   and wh_id = '%s' ",     
                srcloc,
                wh_id);  
                
            /* Don't really care about this status */
        	errcode = srvInitiateCommand(buffer, NULL);       
	    }
	    else
	    {
	    	ret_status = 90019;
	    }
        srvResultsAdd(*CmdRes,
                      srcloc,
                      lodnum,
                      srcare,
                      wh_id,
                      prtnum,
                      prtfam,
                      prt_client_id,
                      invsts,
                      dstloc,
                      dstare,
                      untqty,
                      pndqty,
                      ret_status);   
    }
    srvFreeMemory(SRVRET_STRUCT, Cmd);
            
    return(eOK);
}


static RETURN_STRUCT *initResults(long ret_status)
{
    RETURN_STRUCT *CurPtr;
    
        CurPtr = srvResultsInit(eOK,
                             "srcloc", COMTYP_STRING, STOLOC_LEN,
                             "lodnum", COMTYP_STRING, LODNUM_LEN,
                             "srcare", COMTYP_STRING, ARECOD_LEN,
                             "wh_id",  COMTYP_STRING, WH_ID_LEN,
                             "prtnum", COMTYP_STRING, PRTNUM_LEN,
                             "prtfam", COMTYP_STRING, PRTFAM_LEN,
                             "prt_client_id", COMTYP_STRING, CLIENT_ID_LEN,
                             "invsts", COMTYP_STRING, INVSTS_LEN,
                             "dstloc", COMTYP_STRING, STOLOC_LEN,
                             "dstare", COMTYP_STRING, ARECOD_LEN,
                             "srcqty", COMTYP_LONG, sizeof(long),
                             "pndqty", COMTYP_LONG, sizeof(long),
                             "status", COMTYP_LONG, sizeof(long),
                             NULL);

    return (CurPtr);
}

LIBEXPORT
RETURN_STRUCT *intGetLocationConsolidationCandidates(char *srcare_i,
                                                     char *wh_id_i,
                                                     char *prtnum_i,
                                                     char *prtfam_i)
{
    char buffer[4000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;

    char srcare_buffer[100];
    char srcare[ARECOD_LEN+1];
    char dstare[ARECOD_LEN+1];
    char wh_id[WH_ID_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char prtfam[PRTFAM_LEN+1];
    char cons_rule_cod[CONS_RULE_COD_LEN+1];
    char src_cmd[SRC_CMD_LEN+1];
    long casqty;
    long pceqty;
    long palpct;
    RETURN_STRUCT *CmdRes;
    char last_srcare[ARECOD_LEN+1];
    
    memset(srcare_buffer, 0, sizeof(srcare_buffer));
    memset(wh_id, 0, sizeof(wh_id));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prtfam, 0, sizeof(prtfam));
         
    if (srcare_i && misTrimLen(srcare_i, ARECOD_LEN))
        sprintf(srcare_buffer, " and srcare = '%s'",
                               srcare_i);

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
        sprintf(wh_id, " and wh_id = '%s' ", wh_id_i);
        
    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
        misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);
        
    if (prtfam_i && misTrimLen(prtfam_i, PRTFAM_LEN))
    	misTrimcpy(prtfam, prtfam_i, PRTFAM_LEN);

    sprintf(buffer,
            "select srcare, dstare, wh_id, cons_rule_cod, "
            "       casqty, pceqty, palpct, src_cmd "
            "  from are_cons_rule "
            " where 1 = 1 "
            "    %s "
            "    %s "
            "  and src_cmd is not null "
            " order by srcare",
            srcare_buffer,
            wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        return (initResults(ret_status));
    }

    CmdRes = NULL;
    
    if(sqlGetNumRows(res) > 0)
    {
        sqlSetSavepoint("process_dstare");
        memset(last_srcare, 0, sizeof(last_srcare));
        
	    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
	        memset(dstare, 0, sizeof(dstare));
	        memset(wh_id, 0, sizeof(wh_id));
	        memset(cons_rule_cod, 0, sizeof(cons_rule_cod));
	        memset(srcare, 0, sizeof(srcare));
	        memset(src_cmd, 0, sizeof(src_cmd));
	        casqty = 0;
	        pceqty = 0;
	        palpct = 0;
	
	        misTrimcpy(srcare, sqlGetString(res, row, "srcare"), ARECOD_LEN);
	        
	        //Only should have on source area from area consolidation rule. 
	        //If have dupliated source areas, just ignore the duplicated ones.
	        if (strcmp(last_srcare, srcare) == 0)
	        {
	            continue;
	        }
	        
	        /* Save the last source area for the next loop. */
            memset(last_srcare, 0, sizeof(last_srcare));
            misTrimcpy(last_srcare, srcare, ARECOD_LEN);
	        
	        misTrimcpy(dstare, sqlGetString(res, row, "dstare"), ARECOD_LEN);
	        misTrimcpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
	        misTrimcpy(cons_rule_cod, 
	                   sqlGetString(res, row, "cons_rule_cod"), CONS_RULE_COD_LEN);
	        misTrimcpy(src_cmd, sqlGetString(res, row, "src_cmd"), SRC_CMD_LEN);
	        if (!sqlIsNull(res, row, "casqty"))
	            casqty = sqlGetLong(res, row, "casqty");
	        if (!sqlIsNull(res, row, "pceqty"))
	            pceqty = sqlGetLong(res, row, "pceqty");
	        if (!sqlIsNull(res, row, "palpct"))
	            palpct = sqlGetLong(res, row, "palpct");
	
			misTrc( T_FLOW, "Calling sProcessSource function. srcare ->(%s) wh_id->(%s) cons_rule_cod->(%s) src_cmd->(%s) and palpct->(%ld) "
			        " pceqty->(%ld) casqty->(%ld) prtnum->(%s) prtfam->(%s) ",
			        srcare, wh_id, cons_rule_cod, src_cmd, palpct, pceqty, casqty, prtnum, prtfam );
			
		    if (strncmp(cons_rule_cod,
                     CONS_RULE_COMMAND, CONS_RULE_COD_LEN) == 0)
            {
		        ret_status = sProcessSource(srcare,
		                                    dstare,
		                                    wh_id,
		                                    cons_rule_cod,
		                                    src_cmd,
		                                    pceqty,
		                                    casqty,
		                                    palpct,
		                                    prtnum,
		                                    prtfam,
		                                    &CmdRes);
		        if (ret_status != eOK)
		        {
		            return (initResults(ret_status));
		        }
		    }
	    }
        
        sqlRollbackToSavepoint("process_dstare");
    }
            
    sqlFreeResults(res);

    if(!CmdRes)
    {
      return (initResults(eOK));
    }
    
    return(CmdRes);
    
}
