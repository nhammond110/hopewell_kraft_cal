static const char *rcsid = "$Id: intGenerateAuditCounts.c 709299 2014-11-12 18:42:22Z SSadigale $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2004 RedPrairie Corporation.  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <common.h>
#include "intlib.h"

LIBEXPORT 
RETURN_STRUCT *intGenerateAuditCounts(char *stoloc_i,
		                      char *cntbat_i, 
		                      char *wh_id_i, 
		                      char *cnttyp_i)
{

    long ret_status;
    long sum_untqty;
    double sum_catch_qty;
    char cntbat_string[200];
    char stoloc_string[200];
    char buffer[2000];
    char buffer2[1024];
    char wh_id[WH_ID_LEN+1];
    char cnttyp[CNTTYP_LEN];

    mocaDataRes *res, *qtyres;
    mocaDataRow *row, *qtyrow;

    RETURN_STRUCT *CurPtr;
    
    memset(stoloc_string, 0, sizeof(stoloc_string));
    memset(cntbat_string, 0, sizeof(cntbat_string));
    memset(wh_id, 0, sizeof(wh_id));
    memset(cnttyp, 0, sizeof(cnttyp));

    /* Check for valid warehouse id */
    
    if (!wh_id_i || misTrimLen(wh_id_i, WH_ID_LEN) == 0)
       return (APPMissingArg("wh_id"));
    else 
       misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);


    if (stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
        sprintf(stoloc_string, " and cntwrk.stoloc = '%s'", stoloc_i);
    if (cntbat_i && misTrimLen(cntbat_i, CNTBAT_LEN))
        sprintf(cntbat_string, " and cntwrk.cntbat = '%s'", cntbat_i);
    if (cnttyp_i && misTrimLen(cnttyp_i, CNTTYP_LEN))
        sprintf(cnttyp, "%s", cnttyp_i);
 
    sprintf(buffer,
        "select distinct cntwrk.stoloc, "
        "       cntwrk.cntbat, "
        "       cntwrk.wh_id "
        "  from cntwrk "
        " where cntqty != nvl(untqty, 0) "
        "   and cntsts = '%s' "
        "   and wh_id =  '%s' ",
        CNTSTS_COMPLETE, wh_id);
        
    /*
     * Make sure there is proper spacing around the union stateuemnt
     * to handle the strcats later on.
     */

    sprintf(buffer2,
        " union "
        "select distinct cntwrk.stoloc, "
        "       cntwrk.cntbat, "
        "       cntwrk.wh_id "
        "  from cntwrk "
        " where cnt_catch_qty != catch_qty "
        "   and catch_qty is not null "
        "   and cntsts = '%s' "
        "   and wh_id =  '%s' ",
        CNTSTS_COMPLETE, wh_id);

    if (strlen(stoloc_string))
    {
        strcat(buffer, stoloc_string);
        strcat(buffer2, stoloc_string);
    }
    if (strlen(cntbat_string))
    {
        strcat(buffer, cntbat_string); 
        strcat(buffer2, cntbat_string);
    }

    /*
     * We used to order by cntwrk.stoloc which gives an error in 
     * an Oracle environment.  Need to order by position.
     */

    strcat(buffer2, " order by 1 ");
    
    strcat(buffer, buffer2);

    ret_status = sqlExecStr(buffer, &res);
    if (eDB_NO_ROWS_AFFECTED != ret_status && eOK != ret_status)
    {
        sqlFreeResults(res);
        return(srvResults(ret_status, NULL));
    }
    else if (eDB_NO_ROWS_AFFECTED == ret_status)
    {
        sqlFreeResults(res);
        return(srvResults(eOK, NULL));
    }
    
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        /* Checking to see if audit already exists for this location. */
        /* If it does exist, we'll just get out of here. */

        sprintf(buffer,
                " select 'x' "
                "   from cntwrk "
                " where stoloc = '%s' "
                "   and cnttyp = '%s' "
                "   and cntsts not in ('%s', '%s') "
                "   and wh_id =  '%s' ",
                sqlGetString(res, row, "stoloc"),
                CNTTYP_AUDIT,
                CNTSTS_COMPLETE, 
                CNTSTS_CANCELLED, wh_id );

        ret_status = sqlExecStr(buffer, NULL);
        if (eOK == ret_status)
        {
            misTrc(T_FLOW, "Open audit already exists for %s, getting out.",
                   sqlGetString(res, row, "stoloc"));
            sqlFreeResults(res);
            return(srvResults(ret_status, NULL));
        }

        /* Retreive the total quantity of all of the parts in
               the location to populate into the cntwrk table.
        */
        sprintf(buffer,
                " select sum(invdtl.untqty) sum_untqty, "
                "        sum(invdtl.catch_qty) sum_catch_qty "
                "   from invdtl, "
                "        invsub, "
                "        invlod "
                " where invdtl.subnum = invsub.subnum "
                "   and invsub.lodnum = invlod.lodnum "
                "   and invlod.stoloc = '%s' "
	        "   and invlod.wh_id = '%s'",
                sqlGetString(res, row, "stoloc"),
                wh_id);

        ret_status = sqlExecStr(buffer, &qtyres);
        if (eOK != ret_status)
        {
            misTrc(T_FLOW, 
           "Error retrieving inventory quantities for location %s",
                   sqlGetString(res, row, "stoloc"));
            sqlFreeResults(res);
            sqlFreeResults(qtyres);
            return(srvResults(ret_status, NULL));
        }

        qtyrow = sqlGetRow(qtyres);

        sum_untqty = sqlGetLong(qtyres, qtyrow, "sum_untqty");
        sum_catch_qty = sqlGetFloat(qtyres, qtyrow, "sum_catch_qty");

        sqlFreeResults(qtyres);
        qtyrow = NULL;
  
        sprintf(buffer,
            "insert into cntwrk "
            "       (cntbat, stoloc, cntsts, cnttyp, printed_flg, "
                    "        untqty, catch_qty, moddte, mod_usr_id, wh_id) "
            "values ('%s', '%s', '%s', '%s', '%d', %ld, %lf, sysdate, '%s', '%s')",
            (char *)sqlGetValue(res, row, "cntbat"),
            (char *)sqlGetValue(res, row, "stoloc"),
            CNTSTS_GENERATED,
            cnttyp,
            BOOLEAN_FALSE, 
            sum_untqty,
            sum_catch_qty,
            DEFAULT_ENV_USER,
            wh_id );

        ret_status = sqlExecStr(buffer, NULL);
        if (eOK != ret_status)
        {
            sqlFreeResults(res);
            return(srvResults(ret_status, NULL));
        }

    }

    misTrc(T_FLOW, "Building result set for generated audits");

    CurPtr = NULL;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
    if (!CurPtr)
    {
        CurPtr = srvResultsInit(eOK,
                            "cntbat",  COMTYP_CHAR, CNTBAT_LEN,
                            "stoloc",  COMTYP_CHAR, STOLOC_LEN,
                            "wh_id",   COMTYP_CHAR, WH_ID_LEN,
                             NULL);
    }    

        srvResultsAdd(CurPtr,
                  sqlGetValue(res, row, "cntbat"),
                  sqlGetValue(res, row, "stoloc"),
                  sqlGetValue(res, row, "wh_id"));

    }
    sqlFreeResults(res);
    return(CurPtr);
}
