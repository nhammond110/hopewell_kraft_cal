#include <applib.h>

#define LMSCOD_DIRECT 	"D"
#define LMSCOD_INDIRECT "I"
#define LMSTYP_PLACE 	"P"
#define LMSTYP_OBTAIN	"O"

#define LMS_MIXED_ADRNUM_STR "MIXDCUST"

#define LMS_WORKSTATION_EQPTYP "WORKSTN"

typedef struct _lms_transaction_info
{
    /* Actcod/oprcod combo drives lmswrk */
    char actcod[ACTCOD_LEN+1];
    char oprcod[OPRCOD_LEN+1];

    char lmswrk[LMSWRK_LEN+1];
    char lmsasn[LMSASN_LEN+1];
    long grpseq;

    char lmstyp[LMSTYP_LEN+1];  /* Obtain/Place    */
    char lmscod[LMSCOD_LEN+1];  /* Direct/Indirect */


    /* Basic info for tracking inventory moves */
    char lodnum[LODNUM_LEN+1];
    char subnum[SUBNUM_LEN+1];
    char dtlnum[DTLNUM_LEN+1];
    char dstlod[LODNUM_LEN+1];
    char dstsub[SUBNUM_LEN+1];
    char wh_id[WH_ID_LEN+1];
    char srcare[ARECOD_LEN+1];
    char dstare[ARECOD_LEN+1];
    char srcloc[STOLOC_LEN+1];
    char dstloc[STOLOC_LEN+1];

    /* What got moved */
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char prtdsc[LNGDSC_LEN+1];
    long palqty;
    long casqty;
    long inpqty;
    long untqty;
    double untwgt;
    double untvol;

    /* Order information */
    char adrnum[ADRNUM_LEN+1];

    /* Who / What moved it */
    char usr_id[USR_ID_LEN+1];
    char devcod[DEVCOD_LEN+1];

    /* When started and completed */
    char enddte[DB_STD_DATE_LEN+1]; /* Date trn complete */
    char begdte[DB_STD_DATE_LEN+1]; /* Date trn started */
    char plndte[DB_STD_DATE_LEN+1]; /* Date trn planned */

    long hldflg; /* hold transaction until full actcod known (after place) */
    long lstflg;  /* Last transaction in work group assignment */
    long accum_flg;
} LMS_TRANSACTION_INFO;


long lm_IsRdt(char *devcod_i, char*wh_id_i);
long lm_IsWorkstation(char *wh_id_i);
long lm_IsFromAutomation(char *usr_id_i);
long lm_IsCycleCountMove(char *srcloc, char *dstloc, char *wh_id);
long lm_IsDynamicArea(char *arecod, char *wh_id);
long lm_GetWorkType(char *actcod, char *oprcod, char *lmswrk_o);
long lm_IsLocationLMTracked(char *stoloc, char *wh_id);
long lm_IsDirectCrossDockMove(char *arecod, char *wh_id);
long lm_IsIdentificationMove(char *srcare, char *dstare, char *wh_id);
long lm_GetArea(char *arecod_i, char *stoloc_i, char *arecod_o, char *wh_id_i);
long lm_WriteLMSActuals(LMS_TRANSACTION_INFO *lms);
long lm_GetUserId(char *usr_id_i, char *usr_id_o);
long lm_WriteDeferredTransaction(LMS_TRANSACTION_INFO *lms);
long lm_IsRDTEmpty(char *devcod, char *wh_id);
long lm_IsShipStagingArea(char *arecod, char *wh_id);
long lm_IsShipmentArea(char *arecod, char *wh_id);
long lm_IsShipAdjustmentMove(char *arecod, char *stoloc, char *wh_id);

