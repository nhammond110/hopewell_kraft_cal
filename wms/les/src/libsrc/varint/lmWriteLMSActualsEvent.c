static const char *rcsid = "$Id$";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "lmlib.h"

static char *sGetEquipmentTypeForLMS(char *devcod, 
                                     char *wh_id)
{
    static char machine_type[LMS_EQPTYP_LEN+1];
    static char last_devcod[DEVCOD_LEN+1];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    char buffer[1000];

    if (!strlen(last_devcod) && devcod &&
	strcmp(last_devcod, devcod) == 0)
    {
	return(machine_type);
    }

    if (!devcod || strlen(devcod) == 0)
    {
	memset(last_devcod, 0, sizeof(last_devcod));
	memset(machine_type, 0, sizeof(machine_type));

	strncpy(machine_type, 
		LMS_WORKSTATION_EQPTYP, LMS_EQPTYP_LEN);
	return(machine_type);
    }

    strcpy(last_devcod, devcod);
    memset(machine_type, 0, sizeof(machine_type));
	
    sprintf (buffer, 
	    "select vehtyp "
	    " from rftmst "
	    " where devcod = '%.*s' "
            "   and wh_id  = '%s' ",
	    DEVCOD_LEN, devcod,
            wh_id); 

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	strncpy(machine_type, LMS_WORKSTATION_EQPTYP, LMS_EQPTYP_LEN);
	return(machine_type);
    }
    else
    {
	row = sqlGetRow(res);
	memset(machine_type, 0, sizeof(machine_type));
	misTrimcpy (machine_type, 
		    sqlGetString(res, row, "vehtyp"), LMS_EQPTYP_LEN);
    }
    sqlFreeResults(res);
    return(machine_type);
}
    
  
LIBEXPORT
RETURN_STRUCT *lmWriteLMSActualsEvent(char *actcod_i,
				       char *oprcod_i,
				       char *lmswrk_i,
				       char *lmsasn_i,
				       long *grpseq_i,
				       char *lmstyp_i,
				       char *lmscod_i,
				       char *lodnum_i,
				       char *subnum_i,
				       char *dtlnum_i,
				       char *dstlod_i,
				       char *dstsub_i,
				       char *srcare_i,
				       char *srcloc_i,
				       char *dstare_i,
				       char *dstloc_i,
				       char *prtnum_i,
				       char *prt_client_id_i,
				       char *prtdsc_i,
				       long *palqty_i,
				       long *casqty_i,
				       long *inpqty_i,
				       long *untqty_i,
				       double *untvol_i,
				       double *untwgt_i,
				       char *adrnum_i,
				       char *usr_id_i,
				       char *devcod_i,
				       char *begdte_i,
				       char *plndte_i,
				       char *enddte_i,
				       long *lstflg_i,
				       long *frcflg_i,
				       long *accum_flg_i,
                                       long *hldflg_i,
                                       char *wh_id_i)
{
    char buffer[5000];
    char stoloc[STOLOC_LEN+1];

    RETURN_STRUCT *CmdRes;
    long ret_status;
    long frcflg;
    long accum_flg;
    long defer_and_play_transactions;
    long defer_transactions;

    char lmsasn[LMSASN_LEN+1];
    long grpseq;

    char last_lmswrk[LMSWRK_LEN+1];
    char first_begdte[DB_STD_DATE_LEN+1];
    mocaDataRes *res;
    mocaDataRow *row, *lrow;

    LMS_TRANSACTION_INFO lms;

    memset(&lms, 0, sizeof(lms));
    memset(stoloc, 0, sizeof(stoloc));
    memset(last_lmswrk, 0, sizeof(last_lmswrk));
    memset(lmsasn, 0, sizeof(lmsasn));

    frcflg = frcflg_i ? *frcflg_i : 0;
    accum_flg = accum_flg_i ? *accum_flg_i : 0;

    defer_transactions = defer_and_play_transactions = 0;

    if (actcod_i)
	misTrimcpy(lms.actcod, actcod_i, ACTCOD_LEN);
    if (oprcod_i)
	misTrimcpy(lms.oprcod, oprcod_i, OPRCOD_LEN);
    if (lmswrk_i)
	misTrimcpy(lms.lmswrk, lmswrk_i, LMSWRK_LEN);
    if (lmsasn_i)
	misTrimcpy(lms.lmsasn, lmsasn_i, LMSASN_LEN);
    if (lmstyp_i)
	misTrimcpy(lms.lmstyp, lmstyp_i, LMSTYP_LEN);
    if (lmscod_i)
	misTrimcpy(lms.lmscod, lmscod_i, LMSCOD_LEN);
    if (lodnum_i)
	misTrimcpy(lms.lodnum, lodnum_i, LODNUM_LEN);
    if (subnum_i)
	misTrimcpy(lms.subnum, subnum_i, SUBNUM_LEN);
    if (dtlnum_i)
	misTrimcpy(lms.dtlnum, dtlnum_i, DTLNUM_LEN);
    if (dstlod_i)
	misTrimcpy(lms.dstlod, dstlod_i, LODNUM_LEN);
    if (dstsub_i)
	misTrimcpy(lms.dstsub, dstsub_i, SUBNUM_LEN);
    if (srcare_i)
	misTrimcpy(lms.srcare, srcare_i, ARECOD_LEN);
    if (srcloc_i)
	misTrimcpy(lms.srcloc, srcloc_i, STOLOC_LEN);
    if (dstare_i)
	misTrimcpy(lms.dstare, dstare_i, ARECOD_LEN);
    if (dstloc_i)
	misTrimcpy(lms.dstloc, dstloc_i, STOLOC_LEN);
    if (prtnum_i)
	misTrimcpy(lms.prtnum, prtnum_i, PRTNUM_LEN);
    if (prt_client_id_i)
	misTrimcpy(lms.prt_client_id, prt_client_id_i, CLIENT_ID_LEN);
    if (prtdsc_i)
	misTrimcpy(lms.prtdsc, prtdsc_i, LNGDSC_LEN);
    if (adrnum_i)
	misTrimcpy(lms.adrnum, adrnum_i, ADRNUM_LEN);
    if (usr_id_i)
	misTrimcpy(lms.usr_id, usr_id_i, USR_ID_LEN);
    if (devcod_i)
	misTrimcpy(lms.devcod, devcod_i, DEVCOD_LEN);
    if (begdte_i)
	misTrimcpy(lms.begdte, begdte_i, DB_STD_DATE_LEN);
    if (enddte_i)
	misTrimcpy(lms.enddte, enddte_i, DB_STD_DATE_LEN);
    if (plndte_i)
	misTrimcpy(lms.plndte, plndte_i, DB_STD_DATE_LEN);
    else
    {
        /* Set lms.plndte to sysdate */
        sprintf(buffer,
            " select to_char(sysdate, '%s') cur_date "
            "   from dual ", STD_DATE_FMT);

        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sqlFreeResults(res);
            return(srvResults(ret_status,NULL));
        }
        
        row = sqlGetRow(res);
        misTrimcpy(lms.plndte, sqlGetString(res, row, "cur_date"), DB_STD_DATE_LEN);
        sqlFreeResults(res);
        res = NULL;
    }
    if (wh_id_i)
        misTrimcpy(lms.wh_id, wh_id_i, WH_ID_LEN);

    lms.grpseq = grpseq_i ? *grpseq_i : 0;
    lms.lstflg = lstflg_i ? *lstflg_i : 0;
    lms.palqty = palqty_i ? *palqty_i : 0;
    lms.casqty = casqty_i ? *casqty_i : 0;
    lms.untqty = untqty_i ? *untqty_i : 0;
    lms.inpqty = inpqty_i ? *inpqty_i : 0;
    lms.untvol = untvol_i ? *untvol_i : 0.0;
    lms.untwgt = untwgt_i ? *untwgt_i : 0.0;
    lms.hldflg = hldflg_i ? *hldflg_i : 0;

    if (!frcflg)
    {
	/* There are a couple of situations we are trying to deal with...
	 * 
	 * 1.  We're specifically being asked to defer our transactions.
	 * 2.  We're an RDT and we need to decide if we defer or are done
	 *     accumulating.
	 *
	 * If we are an RDT and our forks are not empty, then we 
	 * must defer our transactions.  If our forks are empty, 
	 * then we must play any deferred transactions first, and 
	 * then play this transaction.
	 *
	 */

	if (lm_IsRdt(lms.devcod, lms.wh_id))
	{
	    if (lm_IsRDTEmpty(lms.devcod, lms.wh_id))
		defer_and_play_transactions = 1;
	    else
		defer_transactions = 1;
	}
	else
	{
	    if (accum_flg)
		defer_transactions = 1;
	    else
		defer_and_play_transactions = 1;
	}
    }   

    /*
     * The frcflg is set if we are forcing the transaction through 
     */
    if (!frcflg && (defer_transactions || defer_and_play_transactions))
    {
	if (defer_and_play_transactions)
	{
		/* 
		 * First write out the current transaction as
		 * a deferral.  Then go back and process the 
		 * transactions for building assignment groups.
		 * However, if current is fluid loading, we don't
		 * need write current transaction.
		 */

		ret_status = lm_WriteDeferredTransaction(&lms);
		if (ret_status != eOK)
		{
			return(srvResults(ret_status, NULL));
		}

	    misTrc(T_FLOW, "Taking care of deferred transactions");
	    sprintf(buffer,
		    "[select * "
		    "   from lmstrn "
		    "  where devcod = '%s' "
                    "    and wh_id  = '%s' "
		    "    and lmswrk is null ] "
		    " catch (-1403) | "
		    ""
		    " if (@? = 0) "
		    " { "
		    "     get lms work type catch (-1403) | "
		    "     if (@lmswrk)  "
		    "     {  "
		    "         [update lmstrn "
		    "             set lmswrk = @lmswrk "
		    "           where lms_trn_id = @lms_trn_id ] "
		    "     } "
		    "     else "
		    "     { "
		    "         [delete from lmstrn "
		    "           where lms_trn_id = @lms_trn_id ]"
		    "     } "
		    " } ",
		    lms.devcod,
                    lms.wh_id);
	    
	    CmdRes = NULL;
	    ret_status = srvInitiateInline(buffer, &CmdRes);
		
	    if (ret_status != eOK)
	    {
			return CmdRes;
	    }
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);

	    /*
	     * Now go back through and build our work assignments.
	     */
	    sprintf(buffer,
		    "select * "
		    "  from lmstrn "
		    " where devcod = '%s' "
                    "   and wh_id  = '%s' "
		    "   and lmswrk is not null "
		    " order by lms_trn_id ",
		    lms.devcod,
                    lms.wh_id);

	    ret_status = sqlExecStr(buffer, &res);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
		if (ret_status == eDB_NO_ROWS_AFFECTED)
		{
		    misTrc(T_FLOW, 
			   "Failed to find any transactions to play - assuming"
			   " they were eliminated due to no lmswrk defn");
		    return(srvResults(eOK,
				      "lmswrk", COMTYP_CHAR, 
				      LMSWRK_LEN, "", NULL));
		}
		else
		{
		    return(srvResults(ret_status, NULL));
		}
	    }
	   
	    misTrc(T_FLOW, 
		   "Building work assignments for all deferred transactions");

	    grpseq = 1;
	    memset(last_lmswrk, 0, sizeof(last_lmswrk));
	    memset(first_begdte, 0, sizeof(first_begdte));

	    for (lrow = NULL, row = sqlGetRow(res); row; 
		 lrow = row, row = sqlGetNextRow(row))
	    {
		if (strcmp(last_lmswrk,
			   sqlGetString(res, row, "lmswrk")) != 0)
		{
		    memset(last_lmswrk, 0, sizeof(last_lmswrk));
		    strncpy(last_lmswrk, 
			    sqlGetString(res, row, "lmswrk"),
			    LMSWRK_LEN);
		    
		    memset(first_begdte, 0, sizeof(first_begdte));
		    misTrimcpy(first_begdte, 
			       sqlGetString(res, row, "begdte"), 
			       DB_STD_DATE_LEN);
			
		    memset(lmsasn, 0, sizeof(lmsasn));
		    appNextNum(NUMCOD_LMSASN, lmsasn);

		    misTrc(T_FLOW, 
			   "Generating new assignment - lmsasn = %s",
			   lmsasn);

		    if (lrow)
		    {
			/* Have to update the previous row to 
			 * state that this is the last record in this
			 * work assignemnt
			 */
			sprintf(buffer, 
				"update lmstrn "
				"   set lstflg = '%d'"
				"  where lms_trn_id = '%s' ",
				BOOLEAN_TRUE,
				sqlGetString(res, lrow, "lms_trn_id"));
			ret_status = sqlExecStr(buffer, NULL);
			if (ret_status != eOK)
			{
			    sqlFreeResults(res);
			    return(srvResults(ret_status, NULL));
			}
			misTrc(T_FLOW, 
			       "Set previous record as last "
			       "in assignment set");
		    }
		    grpseq = 1;
		}
		sprintf(buffer,
			"update lmstrn "
			"   set lmsasn = '%s', "
			"       lstflg = %d, "
			"       grpseq = %d, "
			"       plndte = to_date('%s', '%s') "
			" where lms_trn_id = '%s' ",
			lmsasn, BOOLEAN_FALSE, grpseq++, 
			first_begdte, STD_DATE_FMT,
			sqlGetString(res, row, "lms_trn_id"));
		ret_status = sqlExecStr(buffer, NULL);
		if (ret_status != eOK)
		{
		    sqlFreeResults(res);
		    return(srvResults(ret_status, NULL));
		}

	    }

	    if (lrow)
	    {
		/* Have to update the the previous row to 
		 * state that this is the last record in this
		 * work assignemnt
		 */
		sprintf(buffer, 
			"update lmstrn "
			"   set lstflg = '%d'"
			"  where lms_trn_id = '%s' ",
			BOOLEAN_TRUE,
			sqlGetString(res, lrow, "lms_trn_id"));
		ret_status = sqlExecStr(buffer, NULL);
		if (ret_status != eOK)
		{
		    sqlFreeResults(res);
		    return(srvResults(ret_status, NULL));
		}
		misTrc(T_FLOW, "Set final record as last in assignment set");
	    }

	    sqlFreeResults(res);

	    misTrc(T_FLOW, "Playing all deferred transactions");
	    
	    sprintf(buffer,
		    "[select * from lmstrn "
		    "  where devcod = '%s' "
                    "    and wh_id  = '%s' "
                    " order by lms_trn_id] | "
		    "  write lms actuals event where frcflg = 1 | "
		    " [delete from lmstrn where lms_trn_id = @lms_trn_id] ",
		    lms.devcod,
                    lms.wh_id);

	    CmdRes = NULL;
	    ret_status = srvInitiateInline(buffer, &CmdRes);
	    if (ret_status != eOK)
	    {
		return(CmdRes);
	    }
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);

	    return(srvResults(eOK, 
			      "lmswrk", COMTYP_CHAR,
			      LMSWRK_LEN, "", NULL));
	}
	else
	{ 
	    /* Defer either explicity requested (accum_flg) or 
	     * implicitly due to forks not empty 
	     */
	    misTrc(T_FLOW, "Deferring transaction");
	    ret_status = lm_WriteDeferredTransaction(&lms);
	    return(srvResults(eOK,
			      "lmswrk", COMTYP_CHAR, 
			      LMSWRK_LEN, "", NULL));
	}
    }

    /*
    ** --------------------------------------
    ** Set the movement location and type. 
    ** --------------------------------------
    ** (1) If the source or destination is a 
    **     "dynamic" area, we must use the 
    **     area code as the location as CALM
    **     doesn't deal well with new locations...
    ** --------------------------------------
    */
    if (lm_IsDynamicArea(lms.srcare, lms.wh_id))
    {
	strcpy(lms.srcloc, lms.srcare);
    }

    if (lm_IsDynamicArea(lms.dstare, lms.wh_id))
    {
	strcpy(lms.dstloc, lms.dstare);
    }
    
    /* Determine if we tell LMS about the source or dstloc */
    memset(stoloc, 0, sizeof(stoloc));
    if (strcmp(lms.lmstyp, LMSTYP_OBTAIN) == 0)
	strcpy(stoloc, lms.srcloc);
    else
	strcpy(stoloc, lms.dstloc);
	

    /* if we don't have a begdte, then call "complete activity"
     * and publish out our info. */

    if (strlen(lms.begdte) == 0)
    {
	misTrc(T_FLOW, "Setting completion time for LMS activity");
	sprintf(buffer,
		"complete lms activity where usr_id = '%s' ",
		lms.usr_id);
	CmdRes = NULL;
	ret_status = srvInitiateInline(buffer, &CmdRes);
	if (ret_status != eOK)
	    return(CmdRes);

	res = srvGetResults(CmdRes);
	row = sqlGetRow(res);
	if (!sqlIsNull(res, row, "begdte"))
	{
	    strncpy(lms.begdte, 
		    sqlGetString(res, row, "begdte"), DB_STD_DATE_LEN);
	    misTrc(T_FLOW, "Got new beginning date: %s", 
		   sqlGetString(res, row, "begdte"));
	}
	else
	{
	    misTrc(T_FLOW, "Failed to get begdte from complete command");
	}
	if (!sqlIsNull(res, row, "enddte"))
	{
	    strncpy(lms.enddte, 
		    sqlGetString(res, row, "enddte"), DB_STD_DATE_LEN);
	    misTrc(T_FLOW, "Got new end date: %s", 
		   sqlGetString(res, row, "enddte"));
	}
	else
	{
	    misTrc(T_FLOW, "Failed to get enddte from complete command");
	} 
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
    }

    if (strlen(lms.lmswrk) == 0)
    {
	CmdRes = NULL;
	ret_status = srvInitiateInline("get lms work type", &CmdRes);
	if (ret_status == eDB_NO_ROWS_AFFECTED)
	{
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);
	    return(srvResults(eOK, NULL));
	}
	else if (ret_status != eOK)
	{
	    return(CmdRes);
	}

	res = srvGetResults(CmdRes);
	row = sqlGetRow(res);
	if (!sqlIsNull(res, row, "lmswrk"))
	{
	    misTrimcpy(lms.lmswrk, 
		       sqlGetString(res, row, "lmswrk"), LMSWRK_LEN);
	}
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
    }
    if (strlen(lms.lmsasn) == 0)
    {
	appNextNum(NUMCOD_LMSASN, lms.lmsasn);
	lms.grpseq = 1;
	lms.lstflg = 1;
    }

    misTrc(T_FLOW,
	   "ACTUALS PUBLISHING: begdte: %s, enddte: %s",
	   lms.begdte, lms.enddte);

    CmdRes = srvResults(eOK,
			"actcod", COMTYP_CHAR, ACTCOD_LEN, lms.actcod,
			"oprcod", COMTYP_CHAR, OPRCOD_LEN, lms.oprcod,
			"lmswrk", COMTYP_CHAR, LMSWRK_LEN, lms.lmswrk,
			"lmsasn", COMTYP_CHAR, LMSASN_LEN, lms.lmsasn,
			"grpseq", COMTYP_INT, sizeof(long), lms.grpseq,
			"lmstyp", COMTYP_CHAR, LMSTYP_LEN, lms.lmstyp,
			"lmscod", COMTYP_CHAR, LMSCOD_LEN, lms.lmscod,
                        "wh_id",  COMTYP_CHAR, WH_ID_LEN,  lms.wh_id,
			"stoloc", COMTYP_CHAR, STOLOC_LEN, stoloc,
 			"prtnum", COMTYP_CHAR, PRTNUM_LEN, lms.prtnum,
			"prt_client_id", COMTYP_CHAR, 
			CLIENT_ID_LEN, lms.prt_client_id,
			"prtdsc", COMTYP_CHAR, LNGDSC_LEN, lms.prtdsc,
			"adrnum", COMTYP_CHAR, ADRNUM_LEN, lms.adrnum,
			"devcod", COMTYP_CHAR, DEVCOD_LEN, lms.devcod,
			"enddte", COMTYP_CHAR, DB_STD_DATE_LEN, lms.enddte,
			"begdte", COMTYP_CHAR, DB_STD_DATE_LEN, lms.begdte,
			"plndte", COMTYP_CHAR, DB_STD_DATE_LEN, lms.plndte,
			"palqty", COMTYP_INT, sizeof(long), lms.palqty,
			"casqty", COMTYP_INT, sizeof(long), lms.casqty,
			"inpqty", COMTYP_INT, sizeof(long), lms.inpqty,
			"untqty", COMTYP_INT, sizeof(long), lms.untqty,
			"untvol", COMTYP_FLOAT, sizeof(double), lms.untvol,
			"untwgt", COMTYP_FLOAT, sizeof(double), lms.untwgt,
			"usr_id", COMTYP_CHAR, USR_ID_LEN, lms.usr_id,
			"lstflg", COMTYP_INT, sizeof(long), lms.lstflg,
			"eqptyp", COMTYP_CHAR, LMS_EQPTYP_LEN, 
                         sGetEquipmentTypeForLMS(lms.devcod, lms.wh_id),
			"lodnum", COMTYP_CHAR, LODNUM_LEN, lms.lodnum,
			NULL);
    
    return(CmdRes);

}
