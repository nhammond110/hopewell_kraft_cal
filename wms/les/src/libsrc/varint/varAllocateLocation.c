static const char *rcsid = "$Id: varAllocateLocation.c,v 1.1 2010/01/19 14:28:35 jswick Exp $";
/*#START***********************************************************************
 *  Copyright (c) 2004 RedPrairie Corporation. All rights reserved.
 *
 *  $URL: https://athena.redprairie.com/svn/prod/wmd/tags/2008.1.2/src/libsrc/dcsint/intAllocateLocation.c $
 *  $Revision: 1.1 $
 *
 *  Application:   intlib
 *  Created:       04-Jan-1994
 *  $Author: jswick $
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <trnlib.h>

#include "intlib.h"

/*
 *
 * HISTORY
 * JJS 01/15/2010 - Creating a VAR version so that custom code in
 *                  trnAllocateLocation() will be called.  Note that
 *                  as of this version, there are no differences with
 *                  intAllocateLocation() in this file.
 *
 */

LIBEXPORT 
RETURN_STRUCT *varAllocateLocation(char *lodnum_i, char *subnum_i, 
				   char *dtlnum_i,
				   char *arecod_i, char *wrkzon_i, 
				   moca_bool_t *trcflg_i,
				   char *stoloc_i, char *prtnum_i, 
				   char *prt_client_id_i,
				   char *lotnum_i,
				   char *revlvl_i, char *orgcod_i,
				   char *invsts_i,
				   long *untcas_i, long *untqty_i,
				   char *fifdte_i,
				   char *type_i,   char *ftpcod_i, 
				   long *untpak_i,
				   char *cur_bldg_id_i,
			 	   char *xdkref_i, long *xdkqty_i,
			 	   char *invmov_typ_i,
                                   char *asset_typ_i,
                                   char *wh_id_i)
{
    STOLOC_LIST *LocList;
    long return_status = eOK;
    RETURN_STRUCT *CurPtr;
    char buffer[1000];
    char stoloc[STOLOC_LEN + 1];
    char arecod[ARECOD_LEN + 1];
    char wrkzon[WRKZON_LEN + 1];
    char lodnum[LODNUM_LEN + 1];
    char subnum[SUBNUM_LEN + 1];
    char dtlnum[DTLNUM_LEN + 1];
    moca_bool_t trcflg = BOOLEAN_NOTSET;
    char prtnum[PRTNUM_LEN + 1];
    char prt_client_id[CLIENT_ID_LEN + 1];
    char lotnum[LOTNUM_LEN + 1];
    char revlvl[REVLVL_LEN + 1];
    char orgcod[ORGCOD_LEN + 1];
    char invsts[INVSTS_LEN + 1];
    char ftpcod[FTPCOD_LEN + 1];
    char fifdte[MOCA_STD_DATE_LEN + 1];
    char cur_bldg_id[BLDG_ID_LEN + 1];
    char xdkref[XDKREF_LEN + 1];
    char calltype[RTSTR1_LEN + 1];
    char save_lodnum[LODNUM_LEN + 1];
    char save_lodlvl[LODLVL_LEN + 1];
    char invmov_typ[INVMOV_TYP_LEN + 1];
    char asset_typ[ASSET_TYP_LEN + 1];
    char wh_id[WH_ID_LEN+1];
    double lodhgt = 0.0;

    char trcfil[500];
    short seqnum=0;
    long ret_status;
    STOLOC_LIST *ptr;
    mocaDataRes *res;
    mocaDataRow *row;
   
    mocaDataRow *RetRow = 0;

    memset(buffer, 0, sizeof(buffer));
    memset(stoloc, 0, sizeof(stoloc));
    memset(ftpcod, 0, sizeof(ftpcod));
    memset(arecod, 0, sizeof(arecod));
    memset(wrkzon, 0, sizeof(wrkzon));
    memset(lodnum, 0, sizeof(lodnum));
    memset(subnum, 0, sizeof(subnum));
    memset(dtlnum, 0, sizeof(dtlnum));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(lotnum, 0, sizeof(lotnum));
    memset(revlvl, 0, sizeof(revlvl));
    memset(orgcod, 0, sizeof(orgcod));
    memset(invsts, 0, sizeof(invsts));
    memset(fifdte, 0, sizeof(fifdte));
    memset(calltype, 0, sizeof(calltype));
    memset(cur_bldg_id, 0, sizeof(cur_bldg_id));
    memset(xdkref, 0, sizeof(xdkref));
    memset(invmov_typ, 0, sizeof(invmov_typ));
    memset(asset_typ, 0, sizeof(asset_typ));
    memset(wh_id, 0, sizeof(wh_id)); 

    if (stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
	misTrimcpy(stoloc, stoloc_i, STOLOC_LEN);
    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
	misTrimcpy(arecod, arecod_i, ARECOD_LEN);
    if (wrkzon_i && misTrimLen(wrkzon_i, WRKZON_LEN))
	misTrimcpy(wrkzon, wrkzon_i, WRKZON_LEN);
    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
	misTrimcpy(lodnum, lodnum_i, LODNUM_LEN);
    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
	misTrimcpy(subnum, subnum_i, SUBNUM_LEN);
    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
	misTrimcpy(dtlnum, dtlnum_i, DTLNUM_LEN);
    if (trcflg_i)
        trcflg = *trcflg_i;
    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
	misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);
    if (lotnum_i && misTrimLen(lotnum_i, LOTNUM_LEN))
	misTrimcpy(lotnum, lotnum_i, LOTNUM_LEN);
    if (revlvl_i && misTrimLen(revlvl_i, REVLVL_LEN))
	misTrimcpy(revlvl, revlvl_i, REVLVL_LEN);
    if (orgcod_i && misTrimLen(orgcod_i, ORGCOD_LEN))
	misTrimcpy(orgcod, orgcod_i, ORGCOD_LEN);
    if (invsts_i && misTrimLen(invsts_i, INVSTS_LEN))
	misTrimcpy(invsts, invsts_i, INVSTS_LEN);
    if (fifdte_i && misTrimLen(fifdte_i, MOCA_STD_DATE_LEN))
	misTrimcpy(fifdte, fifdte_i, MOCA_STD_DATE_LEN);
    if (cur_bldg_id_i && misTrimLen(cur_bldg_id_i, BLDG_ID_LEN))
        misTrimcpy(cur_bldg_id, cur_bldg_id_i, BLDG_ID_LEN);	
    if (type_i && misTrimLen(type_i, RTSTR1_LEN))
	misTrimcpy(calltype, type_i, RTSTR1_LEN);
    if (ftpcod_i && misTrimLen(ftpcod_i, FTPCOD_LEN))
	misTrimcpy(ftpcod, ftpcod_i, FTPCOD_LEN);
    if (xdkref_i && misTrimLen(xdkref_i, XDKREF_LEN))
	misTrimcpy(xdkref, xdkref_i, XDKREF_LEN);

    /* 
     * If argument wh_id is passed in, validate it.
     * If any of lodnum/subnum/dtlnum is passed in, get inventory wh_id by 
     *     the inventory identifier.
     * If get both argument wh_id and inventory wh_id, then compare them,
     *     if not matched, return an error.
     * If only get argument wh_id or inventory wh_id, use it for future.
     * If get neither of them, return Missing Argument error.
     */
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
    {
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);

        sprintf(buffer,
                " get warehouse id "
                " where wh_id = '%s' ",
                wh_id);
    
        ret_status = srvInitiateCommand(buffer, NULL);
        if (ret_status != eOK)
            return (srvResults(ret_status, NULL));
    }
   
    /* If lodnum/subnum/dtlnum is passed in, get the wh_id by the inventory */
    memset(buffer, 0, sizeof(buffer));
    
    if (misTrimLen(lodnum, LODNUM_LEN))
    {
        sprintf(buffer,
                "select invlod.wh_id, invlod.lodhgt "
                "  from invlod "
                " where invlod.lodnum = '%s' ",
                lodnum);
    }
    else if (misTrimLen(subnum, SUBNUM_LEN))
    {
        sprintf(buffer,
                "select invlod.wh_id, invlod.lodhgt "
                "  from invlod, "
                "       invsub "
                " where invlod.lodnum = invsub.lodnum "
                "   and invsub.subnum = '%s' ",
                subnum);
    }
    else if (misTrimLen(dtlnum, DTLNUM_LEN))
    {
        sprintf(buffer,
                "select invlod.wh_id, invlod.lodhgt "
                "  from invlod, "
                "       invsub, "
                "       invdtl "
                " where invlod.lodnum = invsub.lodnum "
                "   and invsub.subnum = invdtl.subnum "
                "   and invdtl.dtlnum = '%s' ",
                dtlnum);
    }
    
    if (strlen(buffer))
    {
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status == eOK)
        {
            row = sqlGetRow(res);
            
            /* If get inventory wh_id and argument wh_id, compare them */
            if (misTrimLen(wh_id, WH_ID_LEN) &&
                misCiStrncmp(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN))
            {
                misTrc(T_FLOW,
                       "Inventory warehouse (%s) does not match with "
                       "working warehouse (%s).",
                       sqlGetString(res, row, "wh_id"),
                       wh_id);
                
                if (res) sqlFreeResults(res);
                return (srvResults(eINVENTORY_NOT_IN_THIS_WAREHOUSE, NULL));
            }

            misTrimcpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
            lodhgt = sqlGetFloat(res,row,"lodhgt");
            if (res) sqlFreeResults(res);
        }
        else
        {
            if (res) sqlFreeResults(res);
            return (srvResults(ret_status, NULL));
        }
    }
    
    /* Can't get wh_id, return error */
    if (misTrimLen(wh_id, WH_ID_LEN) == 0)
        return (APPMissingArg("wh_id"));

    /*
     * check that the pass invmov_typ was passed in,
     * the invmov_typ will show why the inventory move
     * created, invmov_typ includes: PICK, TRNS, RCV, RPLN
     */
    if (invmov_typ_i && misTrimLen(invmov_typ_i, INVMOV_TYP_LEN))
    {
        /*
         * check that the invmov_typ is passed in, if the 
         * invmov_typ is not invalid, will return invalid error
         */
        sprintf(buffer,
                " list code descriptions " 
                "where colnam = 'invmov_typ' "
                "  and codval = '%s'",
                invmov_typ_i);
        
        return_status = srvInitiateCommand(buffer, NULL);
        if (return_status != eOK)
            return(APPInvalidArg (invmov_typ_i, "invmov_typ"));
        misTrimcpy(invmov_typ, invmov_typ_i, INVMOV_TYP_LEN);
    }

    /* validate asset_typ if it is passed in */
    if (asset_typ_i && 
        misTrimLen(asset_typ_i, ASSET_TYP_LEN))
    {
        misTrimcpy(asset_typ, asset_typ_i, ASSET_TYP_LEN);

        sprintf(buffer,
                " select 'X' "
                "   from asset_typ "
                "  where asset_typ = '%s' ",
                asset_typ);

        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
            return (APPInvalidArg (asset_typ, "asset_typ"));
    }
    
    CurPtr = NULL;
    if (!(lodnum && misTrimLen(lodnum, LODNUM_LEN)) &&
	!(subnum && misTrimLen(subnum, SUBNUM_LEN)) &&
	!(dtlnum && misTrimLen(dtlnum, DTLNUM_LEN)))
    {
      /*
       * Now, try to get the part client Id value with the next function.  If
       * 3PL is not installed, it will just return a default value.
       * If 3PL is installed, the function 
       * will return INVALID_ARGS if there is no client ID.  
       */

      return_status = appGetClient(prt_client_id_i, prt_client_id);
      if (return_status != eOK) 
      {	
          return(srvResults(return_status, NULL));
      }	

      misTrc(T_FLOW, " Part ->(%s) ClientIN->(%s) ClientOUT->(%s)",
	      prtnum, prt_client_id_i, prt_client_id);


	if (!(prtnum && misTrimLen(prtnum, PRTNUM_LEN)) ||
	    !(prt_client_id && misTrimLen(prt_client_id, CLIENT_ID_LEN)) )
	{
            return APPMissingArg("prtnum");
	}

	/*
	 * Now, since the part was passed into the function, try to get
	 * the part client Id value with the next function.  If
	 * 3PL is not installed, it will just return a default value.
	 * If 3PL is installed, the function 
	 * will return INVALID_ARGS if there is no client ID.  
	 */

	return_status = appGetClient(prt_client_id_i, prt_client_id);
	if (return_status != eOK) 
	{	
	    return (srvResults(return_status, NULL));
	}	

	misTrc(T_FLOW, " Part ->(%s) ClientIN->(%s) ClientOUT->(%s)",
		prtnum, prt_client_id_i, prt_client_id);


	/*
	 * Since we are specifying by part, we need either a current building
	 * or destination area.  Otherwise, we will not know which set of
	 * building polcies to use.
	 */

	if (misTrimLen(cur_bldg_id, BLDG_ID_LEN) == 0 &&
	    misTrimLen(arecod, ARECOD_LEN) == 0 &&
	    misTrimLen(stoloc, STOLOC_LEN) == 0)
	{
	    misTrc(T_FLOW, "When specifying by part, need either a current"
		           "building or desintation area to be specified");
            return APPMissingArg("bldg_id");
	}
    }

    memset(trcfil, '\0', sizeof(trcfil));

    LocList = NULL;
    return_status = trnAllocateLocation(lodnum,
					subnum,
					dtlnum,
					arecod,
					wrkzon,
					stoloc,
					trcflg,
					trcfil,
					prtnum,
					prt_client_id,
					lotnum,
					revlvl,
					orgcod,
					invsts,
					(untcas_i ? *untcas_i : 0),
					(untqty_i ? *untqty_i : 0),
					fifdte,
					calltype,
					ftpcod,
					&LocList,
					(untpak_i ? *untpak_i : 0),
					cur_bldg_id,
					xdkref,
					(xdkqty_i ? *xdkqty_i : 0),
					invmov_typ,
                                        asset_typ,
                                        wh_id,
                                        lodhgt);

    if (return_status != eOK || LocList == NULL)
    {
            /* Check if we need to free the list or if it is already null. */
            if(LocList) 
                trnFreeStolocList(LocList);
                
	    return(srvResults(return_status, NULL));
    }
    
    /* Initialize the return pointer. */
    CurPtr = srvResultsInit(eOK,
                "seqnum", COMTYP_INT, sizeof(long),
                "wh_id", COMTYP_CHAR, WH_ID_LEN,
                "nxtloc", COMTYP_CHAR, STOLOC_LEN,
                "lodnum", COMTYP_CHAR, LODNUM_LEN,
                "lodlvl", COMTYP_CHAR, LODLVL_LEN,
                "trcfil", COMTYP_CHAR, sizeof(trcfil)-1,
                NULL);

    ptr = NULL;
    ptr = LocList;
    memset(save_lodnum, 0, sizeof(save_lodnum));
    memset(save_lodlvl, 0, sizeof(save_lodlvl));
    while (ptr != NULL)
    {
        if (strncmp(save_lodnum, ptr->lodnum, LODNUM_LEN) != 0 ||
            strncmp(save_lodlvl, ptr->lodlvl, LODLVL_LEN) != 0)
        {
            seqnum = 0;
            strncpy(save_lodnum, ptr->lodnum, LODNUM_LEN);
            strncpy(save_lodlvl, ptr->lodlvl, LODLVL_LEN);
        }
        
        /* Publish the results. */
        return_status = srvResultsAdd(CurPtr,
                                   ++seqnum,
                                   ptr->wh_id,
                                   ptr->nxtloc,
                                   ptr->lodnum,
                                   ptr->lodlvl,
                                   trcfil);
        ptr = ptr->next;
    }

    /* Free the locations list. */
    trnFreeStolocList(LocList);

    return (CurPtr);
}
