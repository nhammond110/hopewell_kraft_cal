static const char *rcsid = "$Id: intCancelPick.c 164324 2008-07-23 16:33:14Z pflanzer $";
/*#START***********************************************************************
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "intlib.h"

/*
 *
 * HISTORY
 * LBS 09/30/2009 - Performance enhancments.  Remove cartoniziation
 *                  sub-selects since Hopewell does not cartonize
 *                  and these were found to significantly degrade
 *                  the time it takes to cancel a pick.
 *
 *
 */

/* logPalletControlEvent
 * This method is used to do all the processing
 * before the Pallet Control event is triggered.
 * This method checks if a carrier move exists for
 * the schbat, If so trigger the pallet control event
 * to get a plan from pallet control
*/
static long logPalletControlEvent(char *schbat,
                                  char *wh_id,
                                  moca_bool_t *palctl_flg,
                                  char *cancod)
{
    mocaDataRes    *palres;
    mocaDataRow    *palrow;

    char            pallet_id[PALLET_ID_LEN + 1];
    char            palletsts[PALCTLSTS_LEN + 1];
    char            car_move_id[CAR_MOVE_ID_LEN + 1];
    char            carcod[CARCOD_LEN + 1];
    char            srvlvl[SRVLVL_LEN + 1];
    char            buffer[2000];
    moca_bool_t log_palctl_flg;
    moca_bool_t records_found_flg;
    long status;
    memset(pallet_id, 0, sizeof(pallet_id));
    memset(palletsts, 0, sizeof(palletsts));
    memset(car_move_id, 0, sizeof(car_move_id));
    memset(carcod, 0, sizeof(carcod));
    memset(srvlvl, 0, sizeof(srvlvl));
    memset(buffer, 0, sizeof(buffer));
    
    /* This function is simply trying to get a car_move_id 
     * that is enabled for PalletCtl and is associated to a schbat 
     * passed in.  If we end up getting a car_move_id either at 
     * the PCKWRK, RPLWRK, or XDKWRK levels, we can then log 
     * the event to PalletCtl.
     */
     
    log_palctl_flg = BOOLEAN_FALSE;
    records_found_flg = BOOLEAN_FALSE;

    /* If the palctl_flg_i is null, we are setting the 
     * log_palctl_flag  to true, because we want to 
     * trigger the pallet_control event, when the 
     * palctl_flg_i flag is not explicitly set to false.
     */
    if (!palctl_flg)
    {
       log_palctl_flg = BOOLEAN_TRUE;
    }
    else 
    {
       log_palctl_flg = *palctl_flg;
    }

    /* We are checking if there is a valid cancod ,
     * if there's one, we need to find out the reaflg is
     * set to true,so that each cancelled pick is reallocated,
     * if the cancelled pick is reallocated, we dont need to
     * trigger Pallet control event while cancelling, We just dont 
     * trigger it in cancel pick, set the log_palctl_flag to false, 
     * and let it trigger while reallocating.
     */
    if (cancod && misTrimLen(cancod, CODVAL_LEN) != 0)
    {
        sprintf(buffer,
                "select * "
                "  from cancod "
                " where codval = '%s' "
                "   and reaflg = %d ",
                cancod,
                BOOLEAN_TRUE);
        status = sqlExecStr(buffer,NULL);

        if (status == eOK)
        {
            /* We have a cancod, whose realloc flag is set 
             * so, we'll not trigger pallet control here
             * let the realloc component trigger it
             */
            log_palctl_flg = BOOLEAN_FALSE;
        }
    }
    
    /* If we have a valid cancod or palctl_flg is set we continue */
    if (log_palctl_flg == BOOLEAN_TRUE)
    {
        /* PCKWRK:
         * Check if the palletctl_flg is set for the carrier type
         * trnsp_mode table.if it's set, get the carrier move id 
         * tied to the carrier pallet Control requires carrier 
         * move id to make a pallet plan.
         */
        sprintf(buffer,
                "select stop.car_move_id,"
                "       car_move.carcod,"
                "       max(cardtl.srvlvl) srvlvl"
                "  from trnsp_mode,"
                "       cardtl,"
                "       car_move,"
                "       stop,"
                "       shipment,"
                "       pckwrk "
                " where trnsp_mode.palletctl_flg = 1 "
                "   and trnsp_mode.trnsp_mode = cardtl.cartyp "
                "   and cardtl.carcod         = car_move.carcod "
                "   and car_move.car_move_id  = stop.car_move_id "
                "   and stop.stop_id          = shipment.stop_id "
                "   and shipment.ship_id      = pckwrk.ship_id "
                "   and pckwrk.schbat         = '%s'  "
                " group by stop.car_move_id,"
                "       car_move.carcod",
                schbat);

        status = sqlExecStr(buffer, &palres);

        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
        {
            /* Error needs to be returned */
            sqlFreeResults(palres);
            return status;
        }

        if (status == eDB_NO_ROWS_AFFECTED) 
        {
            /* If the carrier move id does not exist,
             *  We are not returning because we
             * would like to check rplwrk and cross docks now
             */
             misTrc(T_FLOW,
                    " Could not retrieve a Carrier Move ID  for "
                    " the pick work records for the schbat [%s] ",
                    schbat);
                    
            sqlFreeResults(palres);
        }
        
        if (status == eOK)
        {
            /* pckwrk records found */
            records_found_flg = BOOLEAN_TRUE;
            
            palrow = sqlGetRow(palres);
            strcpy(car_move_id, sqlGetString(palres, palrow, "car_move_id"));
            strcpy(carcod, sqlGetString(palres, palrow, "carcod"));
            strcpy(srvlvl, sqlGetString(palres, palrow, "srvlvl"));
            sqlFreeResults(palres);

            /* Update palctlsts with WPPP/WPPH based on the pcksts */
            sprintf(buffer,
                   "[select decode(pckwrk.pcksts,'%s','%s','%s','%s') sts,"
                   "        cmbcod "
                   "   from stop, "
                   "        shipment,"
                   "        pckwrk "
                   "  where pckwrk.ship_id   = shipment.ship_id "
                   "    and shipment.stop_id = stop.stop_id "
                   "    and stop.car_move_id = '%s'"
                   "    and pckwrk.pcksts in ('%s','%s' )]"
                   "| "
                   "[update pckwrk "
                   "   set palctlsts     = @sts "
                   " where pckwrk.cmbcod = @cmbcod ]",
                   PCKSTS_PENDING,
                   WAITING_PLTCTL_PCKWRK_PEND,
                   PCKSTS_HOLD,
                   WAITING_PLTCTL_PCKWRK_HOLD,
                   car_move_id,
                   PCKSTS_PENDING,
                   PCKSTS_HOLD);

            status = srvInitiateCommand(buffer, NULL);
            
            if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
                misTrc(T_FLOW,"Error in updating the palctlsts in pckwrk...");
                return status;
            }
        }
        
        /* Check if short processing is enabled for PalletCtl */
        status = appIsPolicyEnabled(POLCOD_PALLETCTL,
                                    POLVAR_PALLETCTL_CONFIG,
                                    POLVAL_ENABLE_SHORT_PROCESSING,
                                    wh_id);

        if (status == BOOLEAN_TRUE)
        {
            if (records_found_flg == BOOLEAN_FALSE)
            {                
                /* RPLWRK:
                 * If pickwrk records where not found for
                 * sending to pallet control, then we need 
                 * to look for replenishment records. */
                sprintf(buffer,
                        "select stop.car_move_id,"
                        "       car_move.carcod,"
                        "       max(cardtl.srvlvl) srvlvl"
                        "  from trnsp_mode,"
                        "       cardtl,"
                        "       car_move,"
                        "       stop,"
                        "       shipment,"
                        "       rplwrk "
                        " where trnsp_mode.palletctl_flg = 1 "
                        "   and trnsp_mode.trnsp_mode = cardtl.cartyp "
                        "   and cardtl.carcod         = car_move.carcod "
                        "   and car_move.car_move_id  = stop.car_move_id "
                        "   and stop.stop_id          = shipment.stop_id "
                        "   and shipment.ship_id      = rplwrk.ship_id "
                        "   and rplwrk.schbat         = '%s'  "
                        " group by stop.car_move_id,"
                        "       car_move.carcod",
                        schbat);
                        
                status = sqlExecStr(buffer, &palres);
                
                if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
                {
                    /* Error needs to be returned */
                    sqlFreeResults(palres);
                    return status;
                }

                if (status == eDB_NO_ROWS_AFFECTED) 
                {
                    /* If the carrier move id does not exist,
                    * We are not returning ,We want to check
                    * any cross docks there.
                    */
                    misTrc(T_FLOW,
                           " Could not retrieve a Carrier Move ID  for "
                           " the replenishment records for the schbat [%s]",
                           schbat);
                           
                    sqlFreeResults(palres);
                }
                 
                if (status == eOK)
                {
                    /* replenishment records were found */
                    records_found_flg = BOOLEAN_TRUE;
                    
                    palrow = sqlGetRow(palres);
                    strcpy(car_move_id, sqlGetString(palres, palrow, 
                           "car_move_id"));
                    strcpy(carcod, sqlGetString(palres, palrow, "carcod"));
                    strcpy(srvlvl, sqlGetString(palres, palrow, "srvlvl"));
                    sqlFreeResults(palres);
                }   
            }
            
            /* 
             * If either pckwork or rplwrk records were found,
             * try updating only any replenishment status records 
             */
             
            if (records_found_flg == BOOLEAN_TRUE)
            {
                sprintf(buffer,
                        "[select decode(rplwrk.rplsts,'%s','%s') sts,"
                        "        rplref "
                        "   from stop , "
                        "        shipment,"
                        "        rplwrk "
                        "  where rplwrk.ship_id = shipment.ship_id "
                        "    and shipment.stop_id = stop.stop_id "
                        "    and stop.car_move_id = '%s' "
                        "    and rplwrk.rplsts = '%s']catch (-1403)"
                        " | "
                        " if (@? = 0) "
                        " { "
                        "     [update rplwrk "
                        "        set rplsts = @sts "
                        "      where rplref = @rplref ] "
                        " } ",
                        RPLSTS_ISSUED,
                        RPLSTS_WAIT_ORTEC,
                        car_move_id,
                        RPLSTS_ISSUED);
                        
                status = srvInitiateCommand(buffer, NULL);
                
                if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
                {
                    /* Error needs to be returned */
                    sqlFreeResults(palres);
                    return status;
                }
                
                if (status == eDB_NO_ROWS_AFFECTED)
                {
                    /* We are not raising error here,because there could be 
                     * no rplwrk records, we need to continue because we have 
                     * some pick works 
                     */
                    misTrc(T_FLOW,"Error in updating the rplsts in rplwrk...");
                }
            }
        }
        
        /* XDKWRK:
         * If not pick or replenishment work has been found yet,
         * we need to look for cross dock records (if the cross dock is enabled)
         */
        if (records_found_flg == BOOLEAN_FALSE)
        {
            if (appIsInstalledWhID(POLCOD_CROSS_DOCKING,wh_id))
            {
                /* Check if cross dock processing is enabled for PalletCtl */
                status = appIsPolicyEnabled(POLCOD_PALLETCTL,
                                            POLVAR_PALLETCTL_CONFIG,
                                            POLVAL_ENABLE_XDCK_PROCESSING,
                                            wh_id);

                if (status == BOOLEAN_TRUE )
                {
                    /* Looking for cross dock records. */
                    sprintf(buffer,
                            "select stop.car_move_id,"
                            "       car_move.carcod,"
                            "       max(cardtl.srvlvl) srvlvl"
                            "  from trnsp_mode,"
                            "       cardtl,"
                            "       car_move,"
                            "       stop,"
                            "       shipment,"
                            "       xdkwrk "
                            " where trnsp_mode.palletctl_flg = 1 "
                            "   and trnsp_mode.trnsp_mode = cardtl.cartyp "
                            "   and cardtl.carcod         = car_move.carcod "
                            "   and car_move.car_move_id  = stop.car_move_id "
                            "   and stop.stop_id          = shipment.stop_id "
                            "   and shipment.ship_id      = xdkwrk.ship_id "
                            "   and xdkwrk.schbat         = '%s'  "
                            " group by stop.car_move_id,"
                            "       car_move.carcod",
                            schbat);
                            
                    status = sqlExecStr(buffer, &palres);
                    
                    if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
                    {
                        /* Error needs to be returned */
                        sqlFreeResults(palres);
                        return status;
                    }

                    if (status == eDB_NO_ROWS_AFFECTED) 
                    {
                         misTrc(T_FLOW,
                               " Could not retrieve a Carrier Move ID  for "
                               " the cross dock records for the schbat [%s]",
                               schbat);
                        sqlFreeResults(palres);
                    }
                    
                    if(status == eOK)
                    {
                        /* found xdck record */
                        records_found_flg = BOOLEAN_TRUE;
                        
                        palrow = sqlGetRow(palres);
                        strcpy(car_move_id, sqlGetString(palres, palrow, 
                               "car_move_id"));
                        strcpy(carcod, sqlGetString(palres, palrow, "carcod"));
                        strcpy(srvlvl, sqlGetString(palres, palrow, "srvlvl"));
                        sqlFreeResults(palres);
                    }
                }
            }
        }
        
        /* 
         * Log the PalletCtl event if there exists either records for
         * picks, replenishments or cross docks for this carrier move
         */
        if (records_found_flg == BOOLEAN_TRUE)
        {
            sprintf(buffer,
                    "get integrator system id "
                    "    where systyp = 'WMD' "
                    "      and wh_id  = '%s'"
                    " | "
                    " publish data "
                    "   where car_move_id = '%s' "
                    "     and carcod      = '%s' "
                    "     and srvlvl      = '%s'"
                    "     and sys_id      = @sys_id"
                    " | "
                    " sl_log event "
                    "    where evt_id       = 'PALLETCTL_SEND' "
                    "      and ifd_data_ptr = NULL "
                    "      and sys_id       = @sys_id",
                    wh_id,
                    car_move_id,
                    carcod,
                    srvlvl);

            status = srvInitiateCommand(buffer, NULL);
            
            if (status != eOK)
            {
                misTrc(T_FLOW,"Error logging PALLETCTL_SEND event...");
                return status;
            }
        }        
        else
        {
          misTrc(T_FLOW,
              " There are no records to be planned by Pallet Control  ...");
        }
    }
    
    return eOK;
}
static long logCannedPicks(char *cangrp, char *wrktyp, 
               char *ctnnum, char *wrkref, 
               char *can_usr_id, char *cancod,
               char *wh_id)
{
    char            buffer[2000];
    char            where_clause[2000];
    long            ret_status;

    memset(where_clause, 0, sizeof(where_clause));
    memset(buffer, 0, sizeof(buffer));

    if (strncmp(wrktyp, WRKTYP_KIT, 1) == 0) 
    {
        sprintf(where_clause,
          "select * "
          " from pckwrk "
          " where cmbcod in "
          "   (select cmbcod from pckwrk "
          /* include the picks to boxes and exclude the new box picks to carton*/
          "     where ctnnum in (select nvl(p1.subnum, p1.ctnnum) subnum "
          "                       from pckwrk p1 "
          "                      where p1.ctnnum = '%s') "
          "   ) "
          "  and prtnum != '%s' ",
          ctnnum, PRTNUM_KIT);
    }
    else 
    {
		/*
		 * LBS - Removed cartonization cmbcod derivation as Hopewell does not use it.
		 * For Improved Performance when cancelling Picks
		 */
        sprintf(where_clause,
                "select * "
                " from pckwrk "
                " where cmbcod in "
        "   (select cmbcod from pckwrk "
        "                 where wrkref = '%s') "
        "   and prtnum != '%s' ",
        wrkref,  PRTNUM_KIT);
    }
 
    /* Select everything from the pckwrk table and push it into the
       canpck.  This pulls everything for the cmbcod because trnDeallocate
       inventory deallocates everything for the cmbcod associated with the
       wrkref.  */

    sprintf(buffer,
        "[%s] |  "
        "  create cancelled pick "
        "  where cangrp = '%s' "
        "    and can_usr_id = '%s' "
        "    and cancod     = '%s' "
        "    and remqty     = (@pckqty - @appqty) ",
        where_clause, 
            cangrp, 
            can_usr_id, 
            cancod);

    ret_status = srvInitiateCommand(buffer, NULL);

    /* if we got down this path all pick have been completed for the kit 
        and we cannot cancel the header record */

    if ((ret_status == eDB_NO_ROWS_AFFECTED) &&  
    (strncmp(wrktyp, WRKTYP_KIT, 1) == 0))  
          return (ret_status = eINT_CANT_CANCEL_KIT_PICK);

    if (ret_status != eOK)
    return (ret_status);

    /* If PalletCtl is installed,we need to update the
     * Pallet control information to the canpck table
     */
    if (appIsInstalledWhID(POLCOD_PALLETCTL,wh_id) && 
        strncmp(wrktyp, WRKTYP_PICK, WRKTYP_LEN) == 0)
    {
         sprintf(buffer,
                " [select * "
                "    from pckwrk "
                "   where cmbcod in "
                "        (select cmbcod from pckwrk where wrkref = '%s') ]"
                " | "
                " [update canpck "
                "     set pallet_id       = @unique_pallet_id,"
                "         pallet_pos      = @pallet_pos,"
                "         pallet_load_seq = @pallet_load_seq "
                "   where cmbcod          = @cmbcod ]",
                wrkref);
         ret_status = srvInitiateCommand(buffer, NULL);

        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }
    return (eOK);
}

static long clearLocation(char *stoloc, float qvl, char *wh_id)
{
    char            buffer[500];
    long            ret_status;

    sprintf(buffer, 
        "update locmst "
        "    set pndqvl = pndqvl - %f "
        " where stoloc = '%s' "
            "   and wh_id = '%s'",
        qvl, stoloc, 
            wh_id);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK)
    return (ret_status);

    return (eOK);
}


static long removeFromWorkQueue(char *wrkref)
{
    long        ret_status;
    char    buffer[1000];
    char        lblbat[LBLBAT_LEN+1];

    mocaDataRes    *pckwrk_res;
    mocaDataRes       *wrkque_res;
    mocaDataRow    *pckwrk_row;
    mocaDataRow    *wrkque_row;
    
    memset(lblbat, 0, sizeof(lblbat));

    /*  Determine if pick is on a label batch */     
    
    sprintf(buffer,
        "select lblbat "
        "  from pckwrk "
        " where wrkref = '%s' "
        "   and lblbat is not null",
        wrkref);
    ret_status = sqlExecStr(buffer, &pckwrk_res);
    
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
        return (ret_status);
    }
    if ((ret_status == eOK))
    {
        /* Grab the lblbat for use later on. */
        pckwrk_row = sqlGetRow(pckwrk_res);
        misTrimcpy(lblbat, sqlGetString(pckwrk_res, pckwrk_row, "lblbat"),
                            LBLBAT_LEN);
    }
    sqlFreeResults(pckwrk_res);

    /* If pick work is not on a label batch, complete any existing work. */
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        sprintf(buffer, 
            "select reqnum "
            "  from wrkque "
            " where wrkref = '%s'",
            wrkref);
    ret_status = sqlExecStr(buffer, &wrkque_res);
            
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults(wrkque_res);
        return (ret_status);
    }
            
    if (ret_status == eOK)
    {
        wrkque_row = sqlGetRow(wrkque_res);
        sprintf(buffer,
            "[update wrkque "
            "    set wrksts = '%s' "
            "  where reqnum = %d]"
            " | "
            "remove work request "
        " where reqnum = %d " 
        "   and wrkref = '%s' "
        "   and prcmod = '%s' ",
            WRKSTS_CANL,
             sqlGetLong(wrkque_res, wrkque_row, "reqnum"),
            sqlGetLong(wrkque_res, wrkque_row, "reqnum"),
            wrkref, PRCMOD_NOMOVE);
         ret_status = srvInitiateInline(buffer, NULL);
         if (ret_status != eOK)
         {
             sqlFreeResults(wrkque_res);
             return (ret_status);
         }
    }
    sqlFreeResults(wrkque_res);
    }
    else
    {
    /* If pick work is on a label batch, 
         * check if there are any picks on the label batch
         * that have not yet been picked.
         * */
        sprintf(buffer,
        "select 'x' "
                "  from dual "
                " where exists "
                "     (select 'x' "
        "       from pckwrk "
        "      where lblbat = '%s' "
        "        and pckqty > appqty "
        "        and wrkref != '%s') ", 
        lblbat, wrkref);
        ret_status = sqlExecStr(buffer, NULL);

        if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
        {
        return (ret_status);
    }

    /* If all picked or deleted, delete all work on label batch. */
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        /* Because the wrkref could be set to a pick that has been
             * cancelled, we need to use the lblbat to get the reqnum for
             * the work that we need to cancel.
             */
        sprintf(buffer,
                        "[select reqnum "
                        "  from wrkque "
                        " where lblbat = '%s'] "
                        " | "
            "complete work "
            "   where reqnum = @reqnum " 
            "     and prcmod = '%s' ",
                        lblbat, PRCMOD_NOMOVE);
         ret_status = srvInitiateInline(buffer, NULL);
             if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
         {
             return (ret_status);
         }
    }
    }
    return (eOK);
}    


static long getStdPalletVolume(char *wh_id)
{
    char            buffer[300];
    long            ret_status;
    static long     volume;
    mocaDataRes    *res;
    mocaDataRow    *row;

    if (volume != 0)
    return (volume);

    sprintf(buffer,
        "select rtnum1 "
        "  from poldat_view "
        " where polcod = '%s' "
        "    and polvar = '%s' "
        "    and polval = '%s' "
        "    and wh_id  = '%s' ",
            POLCOD_PCKRELMGR,
            POLVAR_MISC,
        POLVAL_REL_PALLET_VOLUME,
        wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
    sqlFreeResults(res);
    volume = 120968;    /* just in case the def policy is missing */
    return (volume);
    }
    row = sqlGetRow(res);
    if (!sqlIsNull(res, row, "rtnum1"))
    volume = sqlGetLong(res, row, "rtnum1");
    else
    volume = 120968;

    sqlFreeResults(res);
    return (volume);
}

static long UnallocateIntermediate(char *wrkref)
{
    long            ret_status;
    char            buffer[1000];
    mocaDataRes    *res, *tres;
    mocaDataRow    *row, *trow;
    double          wid, hgt, len;
    long            qty, stdpal, untcas;
    float           pallet_volume;
    float           product_volume;
    float           product_length;
    long            NumRows;
    
    if (!wrkref || misTrimLen(wrkref, WRKREF_LEN) == 0)
    return (eOK);

    /* We cannot say a location is a hop or intermediate location 
     * if its area's praflg is TRUE, because a location could be a hop 
     * location even if the praflg is FALSE. Instead, we should check 
     * the pckmov table.
     */
    sprintf(buffer,
        "select pckmov.arecod, pckmov.stoloc, pckmov.seqnum, "
        "        pckwrk.lodlvl, pckwrk.ftpcod, aremst.wh_id, "
        "        pckwrk.untcas,  "
        "        sum(pckwrk.pckqty) pckqty, "
        "        sum(pckwrk.appqty) appqty "
        "  from aremst, pckwrk, pckmov "
        " where pckwrk.cmbcod = (select cmbcod "
        "                   from pckwrk where wrkref = '%s') "
        "    and pckmov.cmbcod = pckwrk.cmbcod "
        "   and aremst.arecod = pckmov.arecod "
        "   and aremst.wh_id = pckmov.wh_id "
        " group by aremst.wh_id, pckmov.arecod, pckmov.stoloc, "
        "           pckmov.seqnum, pckwrk.lodlvl, pckwrk.ftpcod,  "
        "           pckwrk.untcas  "
        " order by pckmov.seqnum ",
        wrkref);

    ret_status = sqlExecStr(buffer, &res);
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
        sqlFreeResults(res);
        return (ret_status == eDB_NO_ROWS_AFFECTED ? eOK : ret_status);
    }

    /* We will handle all intermediate locations first. So we leave the 
     * last row, which is the destnation location, unprocessed, because 
     * it will be handled later.
     */
    tres = NULL;
    NumRows = 0;
    NumRows = sqlGetNumRows(res);
    for (row = sqlGetRow(res); NumRows > 1 && row; 
        NumRows--, row = sqlGetNextRow(row))
    {
        if (!sqlIsNull(res, row, "stoloc"))
        {
            qty = (long) ( sqlGetFloat(res, row, "pckqty")
                  -  sqlGetFloat(res, row, "appqty"));
            untcas = sqlGetLong(res, row, "untcas");
            stdpal = getStdPalletVolume(sqlGetString(res, row, "wh_id"));
    
            sprintf(buffer,
                "select ftpmst.caslen, ftpmst.caswid, ftpmst.cashgt, "
                "        aremst.loccod, aremst.arecod, aremst.sigflg "
                "  from aremst, ftpmst "
                " where aremst.arecod = '%s' "
                "   and aremst.wh_id = '%s' "
                "    and ftpmst.ftpcod = '%s' ",
                sqlGetString(res, row, "arecod"),
                sqlGetString(res, row, "wh_id"),
                sqlGetString(res, row, "ftpcod"));
    
            ret_status = sqlExecStr(buffer, &tres);
            if (ret_status != eOK)
            {
                sqlFreeResults(tres);
                sqlFreeResults(res);
                return (ret_status);
            }
            trow = sqlGetRow(tres);
            hgt = sqlGetFloat(tres, trow, "cashgt");
            wid = sqlGetFloat(tres, trow, "caswid");
            len = sqlGetFloat(tres, trow, "caslen");
    
            product_length = (float) (len * ((float) qty / untcas));
            product_volume = (float) ((hgt * wid * len) *
                          ((float) qty / untcas));
    
            if (strncmp(sqlGetString(res, row, "lodlvl"),
                LODLVL_LOAD, LODLVL_LEN) == 0)
                pallet_volume = (float) 1;
            else
                pallet_volume = (float) (product_volume / stdpal);
    
            if (trow &&
              sqlGetBoolean(tres, trow, "sigflg") == BOOLEAN_TRUE)
            {
                if (strncmp(sqlGetString(tres, trow, "loccod"),
                        LOCCOD_PALLET, LOCCOD_LEN) == 0)
                {
                    ret_status = clearLocation(sqlGetString(res,
                                       row, "stoloc"),
                                   pallet_volume,
                                   sqlGetString(res, row, "wh_id"));
                }
                else if (strncmp(sqlGetString(tres, trow, "loccod"),
                         LOCCOD_VOLUME, LOCCOD_LEN) == 0)
                {
                    ret_status = clearLocation(sqlGetString(res,
                                       row, "stoloc"),
                                   product_volume,
                                   sqlGetString(res, row, "wh_id"));
                }
                else if (strncmp(sqlGetString(tres, trow, "loccod"),
                         LOCCOD_EACHES, LOCCOD_LEN) == 0)
                {
                    ret_status = clearLocation(sqlGetString(res,
                                       row, "stoloc"),
                                   (float) qty,
                                   sqlGetString(res, row, "wh_id"));
                }
                else if (strncmp(sqlGetString(tres, trow, "loccod"),
                         LOCCOD_LENGTH, LOCCOD_LEN) == 0)
                {
                    ret_status = clearLocation(sqlGetString(res,
                                       row, "stoloc"),
                                   product_length,
                                   sqlGetString(res, row, "wh_id"));
                }
                if (ret_status != eOK)
                {
                    sqlFreeResults(tres);
                    sqlFreeResults(res);
                    return (ret_status);
                }
            }
        }
    }
    if (tres) sqlFreeResults(tres);
    sqlFreeResults(res);
    return (eOK);

}


/*
 **   intCancelPick will Cancel pick for a specified pick request.
 **     Optionally, it will also error the location being picked on.
 */
LIBEXPORT 
RETURN_STRUCT *varCancelPick(char *wrkref_i, moca_bool_t *errflg_i,
                 char *devcod_i, char *usr_id_i,
                 char *cancod_i,
                 moca_bool_t *palctl_flg_i)
{
    long            status = eERROR;
    RETURN_STRUCT  *CurPtr, *CurPtr1;
    mocaDataRes    *res, *wrkres, *pckres, *canres, *shpres;
    mocaDataRow    *row, *wrkrow, *pckrow, *canrow, *shprow;
    char            buffer[2000];
    char            usr_id[USR_ID_LEN + 1];
    char            cancod[CODVAL_LEN + 1];
    char            devcod[DEVCOD_LEN + 1];
    char            wrktyp[WRKTYP_LEN + 1];
    char            cangrp[CANGRP_LEN + 1];
    moca_bool_t     flag_error;
    long            delqty;

    char            wrkref[WRKREF_LEN + 1];
    char            ctnnum[CTNNUM_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    char            ship_line_id[SHIP_LINE_ID_LEN + 1];
    char            command[500];
    char            schbat[SCHBAT_LEN + 1];

    memset(wrkref, 0, sizeof(wrkref));
    memset(devcod, 0, sizeof(devcod));
    memset(cancod, 0, sizeof(cancod));
    memset(cangrp, 0, sizeof(cangrp));
    memset(ship_line_id, 0, sizeof(ship_line_id));
    memset(usr_id, 0, sizeof(usr_id));
    memset(wh_id, 0, sizeof(wh_id));
    memset(schbat, 0, sizeof(schbat));

    if (!wrkref_i || misTrimLen(wrkref_i, WRKREF_LEN) == 0)
    {
        return (APPMissingArg("wrkref"));
    }
    misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);

    /*
     * If we got a user Id passed, we'll use it, otherwise we'll
     * get one from the envrionment.
     */

    if (usr_id_i && misTrimLen(usr_id_i, USR_ID_LEN) != 0)
    {
        misTrimcpy(usr_id, usr_id_i, USR_ID_LEN);
    }
    else
    {
        strcpy(usr_id, osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "");
    }

    /*
     * If a cancel code is not passed in, then go and get the default.  If
     * there are multiple defaults, then just get the first one.
     */

    if (cancod_i && misTrimLen(cancod_i, CODVAL_LEN) != 0)
    {
        misTrimcpy(cancod, cancod_i, CODVAL_LEN);
    }
    else
    {
        CurPtr = NULL;
        sprintf (buffer,
                 "list cancel codes "
                 " where defflg = '%ld' ",
                 BOOLEAN_TRUE);

        status = srvInitiateCommand(buffer, &CurPtr); 
        if (eOK != status)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr); 

            /* 
             * If we couldn't find a cancel code, we'll just keep going.
             * We'll rely on subsequent functionality to error if the cancel
             * code is needed later.
             */

            if ((eDB_NO_ROWS_AFFECTED != status) &&
                (eSRV_NO_ROWS_AFFECTED != status))
            {
                return (srvResults (status, NULL));
            }
        }
        else
        {
            res = srvGetResults(CurPtr);
            row = sqlGetRow (res);
            misTrimcpy (cancod, sqlGetString (res, row, "cancod"), CODVAL_LEN);
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
        }
    }

    flag_error = BOOLEAN_FALSE;
    if (errflg_i && *errflg_i == BOOLEAN_TRUE)
       flag_error = BOOLEAN_TRUE;

    /* See what type of work this is ... */
    memset(wrktyp, 0, sizeof(wrktyp));
    memset(ctnnum, 0, sizeof(ctnnum));
    memset(wh_id, 0, sizeof(wh_id));

    sprintf(buffer,
        "select wrktyp, wh_id, ctnnum, subnum , schbat"
        "  from pckwrk "
        " where wrkref = '%s'", 
        wrkref);

    status = sqlExecStr(buffer, &res);
    if (status != eOK)
    {
    sqlFreeResults(res);
    return(srvResults(status, NULL));
    }
    row = sqlGetRow(res);
    strncpy(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
    strncpy(wrktyp, sqlGetString(res, row, "wrktyp"), WRKTYP_LEN);
    strncpy(schbat, sqlGetString(res, row, "schbat"), SCHBAT_LEN);
    
    /* Validate if we are trying to cancel and reallocate an emergency 
     * replenishment pick. If yes, we throw an error, short allocations 
     * cannot be cancelled and automatically reallocated, because we do not
     * support this in allocate inventory.
     */ 

    if (strncmp(wrktyp, WRKTYP_EMERGENCY, WRKTYP_LEN) == 0)
    {  
        sprintf(buffer,
            " select 1 "
            "   from cancod "
            "  where codval = '%s' "
            "    and reaflg = 1 ",
        cancod);
        status = sqlExecStr(buffer, NULL);

        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            return(srvResults(status, NULL));

        if (status == eOK)
            return(srvResults(eINT_SHORT_ALLOC_CANT_BE_CANCELLED_AND_REALLOC, 
                   NULL));
    }
    
    if (strncmp(wrktyp, WRKTYP_KIT, WRKTYP_LEN) == 0)
    {
    /*
     * If it's a kit pick...
     */

    misTrc(T_FLOW, "Processing a Kit pick...");
    if (!sqlIsNull(res, row, "subnum"))
        misTrimcpy(ctnnum, sqlGetString(res, row, "subnum"), CTNNUM_LEN);
    }
    else
    {
    misTrc(T_FLOW, "Not processing a Kit pick...");
    if (!sqlIsNull(res, row, "ctnnum"))
        misTrimcpy(ctnnum, sqlGetString(res, row, "ctnnum"), CTNNUM_LEN);
    }
    sqlFreeResults(res);

    /* Populate the canceled pick table */
    memset(cangrp, 0, sizeof(cangrp));
    appNextNum(NUMCOD_CANGRP, cangrp);

    status = logCannedPicks (cangrp, wrktyp, ctnnum, wrkref,
                             usr_id, cancod, wh_id);
    if (status != eOK)
    {
        return (srvResults(status, NULL));
    }

    status = removeFromWorkQueue(wrkref);
    if (status != eOK)
    {
    return (srvResults(status, NULL));
    }

    /* Now, based on the work type ... either just select the item or all */
    /* This also selects the picks to boxes for the given ctnnum/wrkref*/
    if (strncmp(wrktyp, WRKTYP_KIT, WRKTYP_LEN) == 0)
    sprintf(buffer,
        "select wrkref, srcloc, cmbcod "
        "  from pckwrk "
        " where ctnnum in (select nvl(p1.subnum, p1.ctnnum) subnum "
        "                    from pckwrk p1 "
        "                   where p1.ctnnum = '%s')"
        "   and prtnum != '%s'",
        ctnnum, PRTNUM_KIT);
    else
	/*
	 * LBS - Removed cartonization cmbcod derivation as Hopewell does not use it.
	 * For Improved Performance when cancelling Picks
	 */
    sprintf(buffer,
        "select wrkref, srcloc, cmbcod, ship_line_id "
        "  from pckwrk "
        " where wrkref = '%s'"
        "   and prtnum != '%s' ",
        wrkref, PRTNUM_KIT);

    status = sqlExecStr(buffer, &wrkres);
    if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(wrkres);
        return(srvResults(status, NULL));
    }

    /* Now, loop through each (or just one, in the case of the kit pick) 
     * of them 
     */
    CurPtr = CurPtr1 = NULL;
    for (wrkrow = sqlGetRow(wrkres); wrkrow; wrkrow = sqlGetNextRow(wrkrow))
    {
    memset(wrkref, 0, sizeof(wrkref));
        memset(ship_line_id, 0, sizeof(ship_line_id));

    strncpy(wrkref, sqlGetString(wrkres, wrkrow, "wrkref"), WRKREF_LEN);

        if (!sqlIsNull(wrkres, wrkrow, "ship_line_id"))
        {
            misTrimcpy(ship_line_id,
                       sqlGetString(wrkres, wrkrow, "ship_line_id"),
            SHIP_LINE_ID_LEN);
        }

    /* First, we get a list of all intermediate locations,
       after we're through processing, we call 
       deallocate resource location to clear the
       rescod if necessary... */
    pckres = NULL;
    sprintf(buffer,
        "select stoloc, wh_id from pckmov "
        " where cmbcod = '%s' and stoloc is not null",
        sqlGetString(wrkres, wrkrow, "cmbcod"));
    status = sqlExecStr(buffer, &pckres);
    if (status != eOK)
    {
        sqlFreeResults(pckres);
        pckres = NULL;
    }

    status = UnallocateIntermediate(wrkref);
    if (status != eOK)
    {
        sqlFreeResults(wrkres);
        if (pckres) sqlFreeResults(pckres);
        return (srvResults(status, NULL));
    }

    /*
     ** Deallocate the destination location
     */
    sprintf(buffer, "deallocate location where wrkref = '%s'", wrkref);
    CurPtr1 = NULL;
    status = srvInitiateCommand(buffer, &CurPtr1);
    srvFreeMemory(SRVRET_STRUCT, CurPtr1);
    if (status != eOK)
    {
        sqlFreeResults(wrkres);
        if (pckres) sqlFreeResults(pckres);
        return (srvResults(status, NULL));
    }
    CurPtr1 = NULL;

    /*
     ** Deallocate the inventory
     */
    misTrc(T_FLOW,"Calling trnDeallocateInventory...wrkref: %s", wrkref);
    sprintf(command,
        "deallocate inventory where wrkref='%s'", wrkref);
    CurPtr1 = NULL;
    status = srvInitiateCommand(command, &CurPtr1);
    srvFreeMemory(SRVRET_STRUCT, CurPtr1);
    if (status != eOK)
    {
        sqlFreeResults(wrkres);
        if (pckres) sqlFreeResults(pckres);
        return (srvResults(status, NULL));
    }
    /* If PalletCtl is installed,log Pallet control
     * event to get a fresh plan from pallet control
     */
    if (appIsInstalledWhID(POLCOD_PALLETCTL,wh_id) && 
        strncmp(wrktyp, WRKTYP_PICK, WRKTYP_LEN) == 0)
    {
        status = logPalletControlEvent(schbat,
                                       wh_id,
                                       palctl_flg_i,
                                       cancod_i);
        if (status != eOK)
        {
            return (srvResults(status, NULL));
        }
    }

        /* If this pick is for an order line that is planned into a
         * tms shipment, and there are no longer any picks, replens,
         * or cross dock work allocated for the tms_ordnum, then
         * then we may need to reset the tms_ord_stat back to "1".
         */

        if (strlen(ship_line_id))
        {    
            sprintf(buffer,
                " reset tms order status "
                " where ship_line_id = '%s' ",
                ship_line_id);

            status = srvInitiateCommand(buffer, NULL);
            if (status != eOK)
            {
                sqlFreeResults(wrkres);
                if (pckres) sqlFreeResults(pckres);
                return(srvResults(status, NULL));
            }
        }

    /* Now...with everything deallocated, let's make an
       attempt to clear the resource location...note: we
       don't check the return status as it may error
       if there is nothing to clear... */

    for (pckrow = sqlGetRow(pckres);
         pckrow; pckrow = sqlGetNextRow(pckrow))
    {
        sqlSetSavepoint("before_resource_deallocate");
        sprintf(buffer,
            "deallocate resource location "
            "where stoloc = '%s'"
            "  and wh_id = '%s'",
            sqlGetString(pckres, pckrow, "stoloc"), 
            sqlGetString(pckres, pckrow, "wh_id"));
        CurPtr1 = NULL;
        status = srvInitiateCommand(buffer, &CurPtr1);
        srvFreeMemory(SRVRET_STRUCT, CurPtr1);
        CurPtr1 = NULL;
        if (status != eOK)
        sqlRollbackToSavepoint("before_resource_deallocate");
    }

    /*
     *  Error the source location, if required
     */
    if (flag_error == BOOLEAN_TRUE && !sqlIsNull(wrkres, wrkrow, "srcloc"))
    {
        sprintf(buffer,
            " error location "
            " where stoloc = '%s'"
            "   and wh_id = '%s'",
            sqlGetString(wrkres, wrkrow, "srcloc"), 
                    wh_id);

        status = srvInitiateCommand(buffer, NULL);
        if (status != eOK)
        {
        sqlFreeResults(wrkres);
        return(srvResults(status, NULL));
        }
    }
    }
    sqlFreeResults(wrkres);

    /* Finally, if this was cartonized, clean up the kits */


    if (strlen(ctnnum))
    {
    sprintf(buffer,
        "select wrkref "
        "  from pckwrk "
        " where ctnnum = '%s' "
        "   and prtnum != '%s'", /* Exclude the box pick for this carton */
        ctnnum, PRTNUM_KIT);

    status = sqlExecStr(buffer, NULL);
    if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
    {
        return(srvResults(status, NULL));
    }
    if (status == eDB_NO_ROWS_AFFECTED)
    {
        /* 
         * No more detail picks for this carton exist.
         * Let's clean up the kit pick.
         */

        /*
         * First we need to save off the shipment_line associated with
         * the kit pick (if there is one), because we'll try to reset
         * the shipment status once the kit pick is deleted.  Doing in
         * before will do no good, since the kit pick will prevent a 
         * shipment from going back to Ready.
         */

        sprintf(buffer,
            "select ship_line_id "
            "  from pckwrk "
            " where subnum = '%s' ",
            ctnnum);
        status = sqlExecStr(buffer, &shpres);
        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
        {
        return(srvResults(status, NULL));
        }
        if (status == eOK)
            {
                shprow = sqlGetRow(shpres);
            if (!sqlIsNull(shpres, shprow, "ship_line_id"))
                misTrimcpy(ship_line_id, 
                  sqlGetString(shpres, shprow, "ship_line_id"), 
                  SHIP_LINE_ID_LEN);
        sqlFreeResults(shpres);
            }
        /* At this point there are no detail picks for the carton.
         * The only remaining picks are 
         *            kit picks (if any) wrktyp = K  and prtnum = KITPART
         *        and box picks(if any) wrktyp = P and prtnum = KITPART 
         * we need to delete the pckmovs and pckwrks(below) created for 
         * these picks also. 
         */
        sprintf(buffer,
            "delete from pckmov "
            " where cmbcod in "
            " (select cmbcod from pckwrk "
            "   where subnum in ( select nvl(p1.subnum, p1.ctnnum) subnum "
            "                       from pckwrk p1 "
            "                      where nvl(p1.ctnnum, p1.subnum) = '%s') "
            "     and wrktyp in ('%s', '%s')) ", 
            ctnnum, WRKTYP_KIT, WRKTYP_PICK);
        status = sqlExecStr(buffer, NULL);
        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
        {
        return(srvResults(status, NULL));
        }
        sprintf(buffer,
            "delete from pckwrk "
            " where subnum in ( select nvl(p1.subnum, p1.ctnnum) subnum "
            "                     from pckwrk p1 "
            "                    where nvl(p1.ctnnum, p1.subnum) = '%s') "
            "   and wrktyp in ('%s', '%s') ", 
            ctnnum, WRKTYP_KIT, WRKTYP_PICK);
        status = sqlExecStr(buffer, NULL);
        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
        {
        return(srvResults(status, NULL));
        }

        /*
         * First, we may need to reset the shipment status.
         * Normally, this happens in deallocate inventory.  However, since
         * we call deallocate inventory on all picks for the kit before
         * removing the kit, the call to reset shipment status will never
         * success because of the kit pick that we're just processing now.
         * Therefore, now that we've cleaned up the kit pick, let's see if
         * the shipment status needs to be reset. 
         */

       if (strlen(ship_line_id))
           {
               sprintf(buffer,
                   " reset shipment status "
                   " where ship_line_id = '%s' ",
                   ship_line_id);

               status = srvInitiateCommand(buffer, NULL);
               if (status != eOK)
               {
                   return(srvResults(status, NULL));
               }
           }
      
        /* Now...with everything deallocated, let's make an
               attempt to clear the resource location...note: we
               don't check the return status as it may error
               if there is nothing to clear... 
         */

           for (pckrow = sqlGetRow(pckres);
                pckrow; pckrow = sqlGetNextRow(pckrow))
           {
               sqlSetSavepoint("before_resource_deallocate");
               sprintf(buffer,
                       "deallocate resource location "
                       "where stoloc = '%s'"
                       "  and wh_id = '%s'",
                       sqlGetString(pckres, pckrow, "stoloc"), 
                       sqlGetString(pckres, pckrow, "wh_id"));
               CurPtr1 = NULL;
               status = srvInitiateCommand(buffer, &CurPtr1);
               srvFreeMemory(SRVRET_STRUCT, CurPtr1);
               CurPtr1 = NULL;
               if (status != eOK)
                   sqlRollbackToSavepoint("before_resource_deallocate");
           }
    }
        else
        {
            /* since there are picks left for the carton, see if all of them
             * have been completed, if so, we need to call complete overpack
             * kit to update the appqty for the pick, delete any work that
             * exists for the kit pick and set the wrkref on the invsub
             * record
             */
            sprintf(buffer,
                    " select 1 "
                    "   from pckwrk p "
                    "  where p.ctnnum = '%s' "
                    "    and appqty < pckqty ",
                    ctnnum);
    
            status = sqlExecStr(buffer, NULL);
    
            if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
                if (pckres) sqlFreeResults(pckres);
                misTrc(T_FLOW,
                       "Error (%ld) determining if "
                       "carton had been picked complete",
                       status);
                return(srvResults(status, NULL));
            }
            else if (status == eDB_NO_ROWS_AFFECTED)
            {
                sprintf(buffer,
                        " complete overpack kit "
                        "    where kitnum = '%s' ",
                        ctnnum);
        
                status = srvInitiateCommand(buffer, NULL);
        
                if (status != eOK)
                {
                    if (pckres) sqlFreeResults(pckres);
                    misTrc(T_FLOW,
                           "Error (%ld) completing overpack kit",
                           status);
                    return(srvResults(status, NULL));
                }
    
                /* make sure that the carton is movable - during pick and pass,
                 * a carton is made non-movable when it is deposited into the 
                 * Pick N Pass drop area.  We need to make sure the carton is
                 * movable so that it can move the rest of the way through
                 * the warehouse
                 */
                sprintf(buffer,
                        " update invsub "
                        "    set mvsflg = '%ld' "
                        "  where subnum = '%s' ",
                        BOOLEAN_TRUE, ctnnum);

                status = sqlExecStr(buffer, NULL);

                if (status != eOK)
                {
                    misTrc(T_FLOW,
                           "Error (%ld) setting case movable",
                           status);
                    if (pckres) sqlFreeResults(pckres);
                    return(srvResults(status, NULL));
                }
            }
        }
    }

    if (pckres) sqlFreeResults(pckres);
    pckres = NULL;

    /* select everything that was written to the canpck table so it can
       be returned again. */

    sprintf(buffer,
           "select ship_id, ship_line_id, "
            "       client_id, ordnum, ordlin, ordsln, "
            "       (pckqty - appqty) delqty, srcloc, srcare, "
            "       ctnnum, cancod, wh_id, wrkref "
            " from canpck "
            " where cangrp = '%s' ",
        cangrp);
   status = sqlExecStr(buffer, &canres);
   
   if (status != eOK)
   {
      sqlFreeResults(canres);
      return srvResults(status, NULL);
   }

   /* Initialize the result set */
    
   CurPtr = NULL;
   CurPtr = srvResultsInit(eOK,
                           "ship_id",    COMTYP_CHAR, SHIP_ID_LEN,
                           "ship_line_id",    COMTYP_CHAR, SHIP_LINE_ID_LEN,
                           "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                           "wkonum",    COMTYP_CHAR, WKONUM_LEN,
                           "wkorev",    COMTYP_CHAR, WKOREV_LEN,
                           "wkolin",    COMTYP_CHAR, WKOLIN_LEN,
                           "ordnum",    COMTYP_CHAR, ORDNUM_LEN,
                           "ordlin",    COMTYP_CHAR, ORDLIN_LEN,
                           "ordsln",    COMTYP_CHAR, ORDSLN_LEN,
                           "delqty",    COMTYP_INT,  sizeof(long),
                           "srcloc",    COMTYP_CHAR, STOLOC_LEN,
                           "srcare",    COMTYP_CHAR, SRCARE_LEN,
                           "ctnnum",    COMTYP_CHAR, CTNNUM_LEN,
                           "cangrp",    COMTYP_CHAR, CANGRP_LEN,
                           "cancod",    COMTYP_CHAR, CODVAL_LEN,
                           "wh_id",    COMTYP_STRING, WH_ID_LEN,
                           "wrkref",    COMTYP_CHAR, WRKREF_LEN,
                           NULL);
      
   canrow = sqlGetRow(canres);

   while (canrow != NULL)
   {
       /* Publish everything  that used to be published and include the 
        * cangrp so people can reference the canpck in triggers 
    * if they want. 
    */

      delqty = sqlGetLong(canres, canrow, "delqty");

      srvResultsAdd(CurPtr,
               sqlIsNull(canres, canrow, "ship_id") ? ""
                : sqlGetString(canres, canrow, "ship_id"),
            sqlIsNull(canres, canrow, "ship_line_id") ? ""
                : sqlGetString(canres, canrow, "ship_line_id"),
            sqlIsNull(canres, canrow, "client_id") ? ""
                        : sqlGetString(canres, canrow, "client_id"),
            sqlIsNull(canres, canrow, "wkonum") ? ""
                : sqlGetString(canres, canrow, "wkonum"),
            sqlIsNull(canres, canrow, "wkorev") ? ""
                : sqlGetString(canres, canrow, "wkorev"),
            sqlIsNull(canres, canrow, "wkolin") ? ""
                : sqlGetString(canres, canrow, "wkolin"),
            sqlIsNull(canres, canrow, "ordnum") ? ""
                : sqlGetString(canres, canrow, "ordnum"),
            sqlIsNull(canres, canrow, "ordlin") ? ""
                : sqlGetString(canres, canrow, "ordlin"),
            sqlIsNull(canres, canrow, "ordsln") ? ""
                : sqlGetString(canres, canrow, "ordsln"),
            delqty,
            sqlGetString(canres, canrow, "srcloc"),
            sqlIsNull(canres, canrow, "srcare") ? ""
                : sqlGetString(canres, canrow, "srcare"),
            sqlIsNull(canres, canrow, "ctnnum") ? ""
                : sqlGetString(canres, canrow, "ctnnum"), 
                    cangrp,
                    sqlIsNull(canres, canrow, "cancod") ? ""
                                : sqlGetString(canres, canrow, "cancod"),
            sqlIsNull(canres, canrow, "wh_id") ? ""
                : sqlGetString(canres, canrow, "wh_id"),
            sqlIsNull(canres, canrow, "wrkref") ? ""
                : sqlGetString(canres, canrow, "wrkref"));

       canrow = sqlGetNextRow(canrow);  
    }

    sqlFreeResults (canres);
    return (CurPtr);
}
