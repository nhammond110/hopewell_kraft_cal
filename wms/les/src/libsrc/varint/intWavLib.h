/*#START***********************************************************************
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved.
 *
 *  Purpose: Miscellaneous common functions for wave processing.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <applib.h>

#include "intlib.h"


long swaveBuildWhere(char *rule_nam,
                     char **ret_where_clause,
                     char *entity);


