#include <moca_app.h>
#include <mocaerr.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

RETURN_STRUCT *usrSystemPerf(void)
{
	struct tm starttime,currenttime,timeDiff;
    char sql[255];
    char CTRY_NAME[3];
	char PRINT_TIME[10];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *ret;

	// Start
    gettimeofday(&starttime, NULL);

    sprintf(sql, "select ISO_3_CTRY_NAME from CTRY_MST");
    ret_status = dbExecStr(sql, &res);

    if (ret_status != eOK)
    {
        return ret;
    }
	
    ret = srvResultsInit(eOK, "VALUE", COMTYP_CHAR, 3, "TIME", COMTYP_CHAR, 10, NULL);
	
	sprintf(PRINT_TIME, "%ld\n", starttime.tm_sec);
	srvResultsAdd(ret,"---",PRINT_TIME);
	
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row)){
        misTrimcpy(CTRY_NAME, sqlGetString(res,row,"ISO_3_CTRY_NAME"), 3);
        
		// Now
		gettimeofday(&currenttime, NULL);

		sprintf(PRINT_TIME, "%ld\n", starttime.tm_sec);
		srvResultsAdd(ret,CTRY_NAME,PRINT_TIME);
    }
	
    sqlFreeResults(res);
    
	// End
	gettimeofday(&currenttime, NULL);

	sprintf(PRINT_TIME, "%ld\n", starttime.tm_sec);
    srvResultsAdd(ret,"---",PRINT_TIME);
	
	return ret;
}
