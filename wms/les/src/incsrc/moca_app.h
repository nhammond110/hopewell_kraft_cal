/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $URL: https://athena.redprairie.com/svn/prod/wmd/tags/2008.1.2/src/incsrc/moca_app.h $ 
 *  $Revision: 1.1 $
 *  $Id: moca_app.h,v 1.1 2011/07/21 19:28:15 joworkma Exp $ 
 *
 *  Application:   mocaapp.h 
 *  Created:       23-Mar-1997
 *  $Author: joworkma $
 *
 *  Purpose:       Typedefs, prototypes for App-specific development.
 *
 *#END************************************************************************/

#ifndef MOCA_APP_H
#define MOCA_APP_H

#include <moca.h>

#include <mislib.h>
#include <oslib.h>
#include <sqllib.h>
#include <srvlib.h>

#endif
