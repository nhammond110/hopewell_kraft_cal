/*#START***********************************************************************
 *
 *  $URL: https://athena.redprairie.com/svn/prod/env/tags/2008.1.2/src/incsrc/usrddl.h $
 *  $Revision: 51252 $
 *  $Author: mlange $
 *
 *  Description: User-level DDL header file.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END*************************************************************************/

#use $LESDIR/include
#use $LESDIR/db/ddl/Indexes

#use $DCSDIR/include
#use $SLOTDIR/include
#use $SLDIR/include
#use $SALDIR/include
#use $MCSDIR/include
#use $MOCADIR/include
