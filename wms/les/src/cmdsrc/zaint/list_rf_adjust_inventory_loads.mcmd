<command>

<name>list rf adjust inventory loads</name>

<description>MGP custom command to show the final ten digits of the LPN versus entire LPN.</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[
/* MGP JRZ 12/7/13 Start -  This command has been changed to display the final ten digits of the LPN, versus the entire *
 *                          LPN. This change will lead to changes downstream to make sure the partial LPN is translated *
 *                          appropriately as the adjustment process continues.                                          */
[select substr(rf_invadj.lodnum, 8) lodnum,
        rf_invadj.lodnum orig_lodnum,
        rf_invadj.prtnum,
        rf_invadj.prt_client_id,
        rf_invadj.invsts,
        rf_invadj.lotnum,
        rf_invadj.orgcod,
        rf_invadj.revlvl,
        rf_invadj.supnum,
        rf_invadj.untcas,
        rf_invadj.untpak,
        sum(rf_invadj.untqty) untqty,
        sum(rf_invadj.catch_qty) catch_qty,
        sum(rf_invadj.scanned_qty) scanned_qty,
        sum(rf_invadj.scanned_ctch_qty) scanned_ctch_qty,
        prtmst_view.age_pflnam,
        prtmst_view.catch_cod,
        prtmst_view.catch_unttyp,
        prtmst_view.dtcflg,
        rf_invadj.adj_ref1,
        rf_invadj.adj_ref2,
        rf_invadj.audit_lvl
   from rf_invadj,
        prtmst_view
  where rf_invadj.prtnum = prtmst_view.prtnum
    and rf_invadj.prt_client_id = prtmst_view.prt_client_id
    and rf_invadj.wh_id = prtmst_view.wh_id
    and rf_invadj.stoloc = @stoloc
    and rf_invadj.wh_id = @wh_id
    and @+rf_invadj.lodnum
    and @+rf_invadj.prtnum
    and @+rf_invadj.prt_client_id
    and @*
  group by rf_invadj.lodnum,
        rf_invadj.prtnum,
        rf_invadj.prt_client_id,
        rf_invadj.invsts,
        rf_invadj.lotnum,
        rf_invadj.orgcod,
        rf_invadj.revlvl,
        rf_invadj.untcas,
        rf_invadj.untpak,
        prtmst_view.age_pflnam,
        prtmst_view.catch_cod,
        prtmst_view.catch_unttyp,
        prtmst_view.dtcflg,
        rf_invadj.adj_ref1,
        rf_invadj.adj_ref2,
        rf_invadj.audit_lvl,
        rf_invadj.supnum
  order by rf_invadj.audit_lvl,
        rf_invadj.lodnum,
        rf_invadj.prtnum,
        rf_invadj.prt_client_id]
]]>
</local-syntax>

<argument name="@*">Any rf_invadj field value</argument>

<documentation>

<remarks>
<![CDATA[
  <p>
  This command is used by the Rf Inventory Adjust process to list the load infomation for the location. 
  </p>
]]>
</remarks>

<exception value="eOK">Normal successful completion</exception>

<seealso cref="list rf adjust inventory"> </seealso>

</documentation>

</command>
