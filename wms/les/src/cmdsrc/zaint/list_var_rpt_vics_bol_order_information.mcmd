<command>

<name>list rpt vics bol order information</name>

<description>List RPT VICS Bill Of Lading Order Information </description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[
 
 if (dbtype = 'ORACLE' or dbtype = 'DB2')
{
    if (@called_from_supp_report = 'N')
    {
        /* 12112013 - added ord.vc_host_move_id in criteria for first select */
        [select 17 - count(distinct ord.ordnum || ord.client_id) number_rows
           from shipment inner
           join shipment_line on shipment_line.ship_id = shipment.ship_id inner
           join ord on ord.ordnum = shipment_line.ordnum
            and ord.client_id = shipment_line.client_id
            and ord.wh_id = shipment_line.wh_id
          where shipment.ship_id = @ship_id
             or (ord.vc_host_move_id = @vc_host_move_id and ord.vc_sold_to_num = @vc_sold_to_num)]
        |
        [select @ship_id ship_id,
		        @vc_host_move_id vc_host_move_id,
				@vc_sold_to_num vc_sold_to_num,
                client_id,
                ordnum,
                wh_id,
                cponum,
                sum(pkgcnt) pkgcnt,
                sum(weight) weight
           from (select client_id,
                        ordnum,
                        wh_id,
                        cponum,
                        case when casuom = 'CS' then sum_untqty / untcas
                             else sum_untqty
                        end pkgcnt,
                        case when casuom = 'CS' then decode(sum_catch_qty, 0, (sum_untqty / untcas * 1.0) * grswgt, sum_catch_qty) / 16
                             else decode(sum_catch_qty, 0, (sum_untqty / 1.0)
                                             * grswgt, sum_catch_qty) / 16
                        end weight
                   from (select client_id,
                                ordnum,
                                wh_id,
                                cponum,
                                prtnum,
                                casuom,
                                untcas,
                                grswgt,
                                sum(sum_catch_qty) sum_catch_qty,
                                sum(sum_untqty) sum_untqty
                           from (select ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt,
                                        sum(invdtl.catch_qty) sum_catch_qty,
                                        sum(invdtl.untqty) sum_untqty
                                   from shipment inner
                                   join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                   join invdtl on invdtl.ship_line_id = shipment_line.ship_line_id inner
                                   join invsub on invsub.subnum = invdtl.subnum inner
                                   join invlod on invlod.lodnum = invsub.lodnum inner
                                   join prtmst_view on prtmst_view.prtnum = invdtl.prtnum
                                    and prtmst_view.prt_client_id = invdtl.prt_client_id
                                    and prtmst_view.wh_id = shipment_line.wh_id inner
                                   join ord on ord.ordnum = shipment_line.ordnum
                                    and ord.client_id = shipment_line.client_id
                                    and ord.wh_id = shipment_line.wh_id
                                  where shipment.ship_id = @ship_id
                                     or (ord.vc_host_move_id = @vc_host_move_id and ord.vc_sold_to_num = @vc_sold_to_num)
                                  group by ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt
                                 union all
                                 /* if inventory are intransit, we get inventory information from inv_intransit
                                  * since the inventory information has been moved into inv_intransit after
                                  * the trailer dispatched.
                                  */
                                 select ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt,
                                        sum(inv_intransit.catch_qty) sum_catch_qty,
                                        sum(inv_intransit.untqty) sum_untqty
                                   from shipment inner
                                   join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                   join inv_intransit on inv_intransit.ship_line_id = shipment_line.ship_line_id inner
                                   join prtmst_view on prtmst_view.prtnum = inv_intransit.prtnum
                                    and prtmst_view.prt_client_id = inv_intransit.prt_client_id
                                    and prtmst_view.wh_id = shipment_line.wh_id inner
                                   join ord on ord.ordnum = shipment_line.ordnum
                                    and ord.client_id = shipment_line.client_id
                                    and ord.wh_id = shipment_line.wh_id
                                  where shipment.ship_id = @ship_id
                                     or (ord.vc_host_move_id = @vc_host_move_id and ord.vc_sold_to_num = @vc_sold_to_num)
                                  group by ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt)
                          group by client_id,
                                ordnum,
                                wh_id,
                                cponum,
                                prtnum,
                                casuom,
                                untcas,
                                grswgt))
          group by @ship_id,
		        @vc_host_move_id,
				@vc_sold_to_num,
                client_id,
                ordnum,
                wh_id,
                cponum
         union all
         select @ship_id ship_id,
				@vc_host_move_id vc_host_move_id,
				@vc_sold_to_num vc_sold_to_num,
                null client_id,
                null ordnum,
                null wh_id,
                null cponum,
                cast(null as float) pkgcnt,
                cast(null as float) weight
           from shipment
          where rownum < @number_rows
          order by client_id]
    }
    else if (@called_from_supp_report = 'Y')
    {
        [select 34 - mod(count(distinct ord.ordnum || ord.client_id), 34) number_rows
           from shipment inner
           join shipment_line on shipment_line.ship_id = shipment.ship_id inner
           join ord on ord.ordnum = shipment_line.ordnum
            and ord.client_id = shipment_line.client_id
            and ord.wh_id = shipment_line.wh_id
          where @+shipment.ship_id
             or (@+ord.vc_host_move_id and @+ord.vc_sold_to_num)]
        |
        [select doc_num,
                bolnum,
                client_id,
                ordnum,
                wh_id,
                cponum,
                sum(pkgcnt) pkgcnt,
                sum(weight) weight
           from (select doc_num,
                        bolnum,
                        client_id,
                        ordnum,
                        wh_id,
                        cponum,
                        case when casuom = 'CS' then sum_untqty / untcas
                             else sum_untqty
                        end pkgcnt,
                        case when casuom = 'CS' then decode(sum_catch_qty, 0, (sum_untqty / untcas * 1.0) * grswgt, sum_catch_qty) / 16
                             else decode(sum_catch_qty, 0, (sum_untqty / 1.0)
                                             * grswgt, sum_catch_qty) / 16
                        end weight
                   from (select doc_num,
                                bolnum,
                                client_id,
                                ordnum,
                                wh_id,
                                cponum,
                                prtnum,
                                casuom,
                                untcas,
                                grswgt,
                                sum(sum_catch_qty) sum_catch_qty,
                                sum(sum_untqty) sum_untqty
                           from (select shipment.doc_num,
                                        ord.vc_host_move_id || '-' || ord.vc_host_stop_seq bolnum,
                                        ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt,
                                        sum(invdtl.catch_qty) sum_catch_qty,
                                        sum(invdtl.untqty) sum_untqty
                                   from shipment inner
                                   join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                   join invdtl on invdtl.ship_line_id = shipment_line.ship_line_id inner
                                   join invsub on invsub.subnum = invdtl.subnum inner
                                   join invlod on invlod.lodnum = invsub.lodnum inner
                                   join prtmst_view on prtmst_view.prtnum = invdtl.prtnum
                                    and prtmst_view.prt_client_id = invdtl.prt_client_id
                                    and prtmst_view.wh_id = shipment_line.wh_id inner
                                   join ord on ord.ordnum = shipment_line.ordnum
                                    and ord.client_id = shipment_line.client_id
                                    and ord.wh_id = shipment_line.wh_id
                                  where @+shipment.ship_id
								     or ( @+ord.vc_host_move_id and @+ord.vc_sold_to_num)
                                  group by shipment.doc_num,
                                        ord.vc_host_move_id || '-' || ord.vc_host_stop_seq,
                                        ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt
                                 /* if inventory are intransit, we get inventory information from inv_intransit
                                  * since the inventory information has been moved into inv_intransit after
                                  * the trailer dispatched.
                                  */
                                 union all
                                 select shipment.doc_num,
                                        ord.vc_host_move_id || '-' || ord.vc_host_stop_seq bolnum,
                                        ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt,
                                        sum(inv_intransit.catch_qty) sum_catch_qty,
                                        sum(inv_intransit.untqty) sum_untqty
                                   from shipment inner
                                   join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                   join inv_intransit on inv_intransit.ship_line_id = shipment_line.ship_line_id inner
                                   join prtmst_view on prtmst_view.prtnum = inv_intransit.prtnum
                                    and prtmst_view.prt_client_id = inv_intransit.prt_client_id
                                    and prtmst_view.wh_id = shipment_line.wh_id inner
                                   join ord on ord.ordnum = shipment_line.ordnum
                                    and ord.client_id = shipment_line.client_id
                                    and ord.wh_id = shipment_line.wh_id
                                  where @+shipment.ship_id
								     or ( @+ord.vc_host_move_id and @+ord.vc_sold_to_num)
                                  group by shipment.doc_num,
                                        ord.vc_host_move_id || '-' || ord.vc_host_stop_seq,
                                        ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt)
                          group by doc_num,
                                bolnum,
                                client_id,
                                ordnum,
                                wh_id,
                                cponum,
                                prtnum,
                                casuom,
                                untcas,
                                grswgt))
          group by doc_num,
                bolnum,
                client_id,
                ordnum,
                wh_id,
                cponum
         union all
         select null doc_num,
                null bolnum,
                null client_id,
                null ordnum,
                null wh_id,
                null cponum,
                cast(null as float) pkgcnt,
                cast(null as float) weight
           from shipment
          where rownum < @number_rows
          order by client_id
         /* The order by is necessary to get */
         /* the non-null rows to the top */]
    }
    else
    {
        [select doc_num,
                bolnum,
                client_id,
                ordnum,
                wh_id,
                cponum,
                sum(pkgcnt) pkgcnt,
                sum(weight) weight
           from (select doc_num,
                        bolnum,
                        client_id,
                        ordnum,
                        wh_id,
                        cponum,
                        case when casuom = 'CS' then sum_untqty / untcas
                             else sum_untqty
                        end pkgcnt,
                        case when casuom = 'CS' then decode(sum_catch_qty, 0, (sum_untqty / untcas * 1.0) * grswgt, sum_catch_qty) / 16
                             else decode(sum_catch_qty, 0, (sum_untqty / 1.0)
                                             * grswgt, sum_catch_qty) / 16
                        end weight
                   from (select doc_num,
                                bolnum,
                                client_id,
                                ordnum,
                                wh_id,
                                cponum,
                                prtnum,
                                casuom,
                                untcas,
                                grswgt,
                                sum(sum_catch_qty) sum_catch_qty,
                                sum(sum_untqty) sum_untqty
                           from (select nvl(shipment.doc_num, nvl(stop.doc_num, car_move.doc_num)) doc_num,
                                        ord.vc_host_move_id || '-' || ord.vc_host_stop_seq bolnum,
                                        ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt,
                                        sum(invdtl.catch_qty) sum_catch_qty,
                                        sum(invdtl.untqty) sum_untqty
                                   from shipment inner
                                   join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                   join invdtl on invdtl.ship_line_id = shipment_line.ship_line_id inner
                                   join invsub on invsub.subnum = invdtl.subnum inner
                                   join invlod on invlod.lodnum = invsub.lodnum inner
                                   join prtmst_view on prtmst_view.prtnum = invdtl.prtnum
                                    and prtmst_view.prt_client_id = invdtl.prt_client_id
                                    and prtmst_view.wh_id = shipment_line.wh_id inner
                                   join ord on ord.ordnum = shipment_line.ordnum
                                    and ord.client_id = shipment_line.client_id
                                    and ord.wh_id = shipment_line.wh_id inner
                                   join stop on stop.stop_id = shipment.stop_id inner
                                   join car_move on car_move.car_move_id = stop.car_move_id inner
                                   join trlr on trlr.trlr_id = car_move.trlr_id
                                  where @+shipment.ship_id
                                    and @+stop.stop_id
                                    and @+car_move.car_move_id
                                    and @+trlr.trlr_num
                                  group by nvl(shipment.doc_num, nvl(stop.doc_num, car_move.doc_num)),
                                        ord.vc_host_move_id || '-' || ord.vc_host_stop_seq,
                                        ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt
                                 union all
                                 select nvl(shipment.doc_num, nvl(stop.doc_num, car_move.doc_num)) doc_num,
                                        ord.vc_host_move_id || '-' || ord.vc_host_stop_seq bolnum,
                                        ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt,
                                        sum(inv_intransit.catch_qty) sum_catch_qty,
                                        sum(inv_intransit.untqty) sum_untqty
                                   from shipment inner
                                   join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                   join inv_intransit on inv_intransit.ship_line_id = shipment_line.ship_line_id inner
                                   join prtmst_view on prtmst_view.prtnum = inv_intransit.prtnum
                                    and prtmst_view.prt_client_id = inv_intransit.prt_client_id
                                    and prtmst_view.wh_id = shipment_line.wh_id inner
                                   join ord on ord.ordnum = shipment_line.ordnum
                                    and ord.client_id = shipment_line.client_id
                                    and ord.wh_id = shipment_line.wh_id inner
                                   join stop on stop.stop_id = shipment.stop_id inner
                                   join car_move on car_move.car_move_id = stop.car_move_id inner
                                   join trlr on trlr.trlr_id = car_move.trlr_id
                                  where @+shipment.ship_id
                                    and @+stop.stop_id
                                    and @+car_move.car_move_id
                                    and @+trlr.trlr_num
                                  group by nvl(shipment.doc_num, nvl(stop.doc_num, car_move.doc_num)),
                                        ord.vc_host_move_id || '-' || ord.vc_host_stop_seq,
                                        ord.client_id,
                                        ord.ordnum,
                                        ord.wh_id,
                                        ord.cponum,
                                        prtmst_view.prtnum,
                                        prtmst_view.casuom,
                                        prtmst_view.untcas,
                                        prtmst_view.grswgt)
                          group by doc_num,
                                bolnum,
                                client_id,
                                ordnum,
                                wh_id,
                                cponum,
                                prtnum,
                                casuom,
                                untcas,
                                grswgt))
          group by doc_num,
                bolnum,
                client_id,
                ordnum,
                wh_id,
                cponum]
    }
}

]]>
</local-syntax>

<documentation>

<remarks>
<![CDATA[
    <p>
    This command is used to list bill of lading order information.  It can be 
    used to get the supplement customer order info in 
    Sub-VICS-BOL-SupplementCustomerOrderInfo report or to get the customer 
    order info in Sub-VICS-BOL-CustomerOrderInfo report or in the 
    Std-VICS-BillOfLading report to determine the number orders in a shipment.

    Currently, the report itself is laid out to print about 5 order rows in
    the main report and about 34 order rows in the addendum pages.  The
    query above does a subtraction from the above numbers to force out the
    publishing of null rows whether the report is called from the main page
    or the supplemental page.
    </p>
]]>
</remarks>

<retcol name="client_id" type="COMTYP_STRING">Client Id</retcol>
<retcol name="ordnum" type="COMTYP_STRING">Order number</retcol>
<retcol name="cponum" type="COMTYP_STRING">Customer order number</retcol>
<retcol name="cascnt" type="COMTYP_INT">Case count for purchase order</retcol>
<retcol name="weight" type="COMTYP_FLOAT">Total weight for purchase order</retcol>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">No rows found</exception>

</documentation>

</command>
