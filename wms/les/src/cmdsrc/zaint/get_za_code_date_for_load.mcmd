<command>
<name>get za code date for load</name>
<description>For a given load number, get the appropriate code date</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[

/* check if code date functionality is enabled*/
if (@codeDateEnabled is null)
    [select rtnum1 codeDateEnabled
       from poldat_view
      where polcod = 'VAR'
        and polvar = 'PICKING-CODE-DATE'
        and polval = 'ENABLED'
        and wh_id = nvl(@wh_id, @@wh_id)]
|
/* Check prtnum item code date */
[select count(distinct prtmst_view.vc_itm_cod_dte_typ || prtmst_view.vc_lbl_date_type) numberOfRows
   from prtmst_view,
        inventory_view
  where prtmst_view.prtnum = inventory_view.prtnum
    and prtmst_view.prt_client_id = inventory_view.prt_client_id
    and prtmst_view.wh_id_tmpl = nvl(@wh_id, @@wh_id)
    and inventory_view.lodnum = @lodnum]
|
if (@codeDateEnabled = 1)
{
    if (@numberOfRows = 1)
    {
        /* Only continue if number of distinct code date types is 1 */
        [select distinct prtmst_view.vc_itm_cod_dte_typ,
                prtmst_view.vc_lbl_date_type
           from prtmst_view,
                inventory_view
          where prtmst_view.prtnum = inventory_view.prtnum
            and prtmst_view.prt_client_id = inventory_view.prt_client_id
            and prtmst_view.wh_id_tmpl = nvl(@wh_id, @@wh_id)
            and inventory_view.lodnum = @lodnum] catch(-1403)
        |
        /* Our item is configured to use the mandte as our code date */
        if (@vc_itm_cod_dte_typ = 1)
        {
            /* Grab the mandte to use for the code date */
            [select min(invdtl.mandte) pick_dte,
                    count(distinct invdtl.mandte) dates
               from invlod,
                    invsub,
                    invdtl
              where invlod.lodnum = invsub.lodnum
                and invsub.subnum = invdtl.subnum
                and invlod.lodnum = @lodnum]
        }
        /* Our item is configured to use the expire_dte as the code date */
        else if (@vc_itm_cod_dte_typ = 2)
        {
            /* Grab the expire_dte to use for the code date */
            [select min(invdtl.expire_dte) pick_dte,
                    count(distinct invdtl.expire_dte) dates
               from invlod,
                    invsub,
                    invdtl
              where invlod.lodnum = invsub.lodnum
                and invsub.subnum = invdtl.subnum
                and invlod.lodnum = @lodnum]
        }
        |
        /* Grab the lngdsc (date format) for the current part */
        list code descriptions
         where colnam = 'vc_lbl_date_type'
           and colval = @vc_lbl_date_type catch(@?)
        |
        /* if number of dates on the load are more than 1, code date should be set to MIXED */
        publish data
         where lodnum = @lodnum
           and codeDate = decode(@dates, 1, upper(to_char(@pick_dte, nvl(@lngdsc, 'DDMONYY'))), 'MIXED')
    }
}
|
/* if number of date types is not 1, code date should be MIXED */
publish data
 where lodnum = @lodnum
   and codeDate = decode(@codeDateEnabled, 1, decode(@numberOfRows, 1, @codeDate, 'MIXED'), '')
   and part = nvl(@prtnum, 'X')
   and codeDate7WithPart7 = iif(len(@part) > 7, substr(@part, length(@part) -6, 7), @part) || ' ' || substr(@codeDate, 0, 7)
   
]]>
</local-syntax>
</command>
