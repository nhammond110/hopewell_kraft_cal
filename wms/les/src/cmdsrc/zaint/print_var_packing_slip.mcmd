<command>
<name>print var packing slip</name>
<description>Print a packing slip</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
/*
 * This command prints a Packing Slip for a
 * shipment.
 *
 *
 * Arguments
 *    - srclod
 *    - dstloc
 *
 * HISTORY
 * NML 02/13/2009 - Creation.
 *
 *
 * VP 12/09/13 -    Adding logic to ensure at least one of the parameters is supplied to the packing 
 *                  slip report generation command
 *
 * JRZ 1/31/14 -    Adding the trlr_seal1 logic to pass the seal from the trailer close screen to solve the 
 *                  commit context issue when printing the report before the session committed the seal
 *
 * CM 5/22/14 -    Adding @+vc_sold_to_num to ensure value is passed to the report if it exists on the stack
 *
 * 
 */
 [select trlr_cod
   from trlr
  where trlr_id = @trlr_id] catch(@?)
|
if (@trlr_cod = 'SHIP')
{
if (@doc_dum is null and @track_num is null and @car_move_id is null and @stop_id is null and @trlr_num is null and @vc_host_move_id is null)
    [select car_move_id
       from car_move
      where trlr_id = @trlr_id]catch(-1403)
|
/* Is auto-printing of the Packing Slip enabled? */
[select distinct 'x'
   from poldat_view
  where poldat_view.polcod = 'VAR-REPORTS-LABELS'
    and poldat_view.polvar = 'PACKINGSLIP'
    and poldat_view.polval = 'ENABLED'
    and poldat_view.wh_id = nvl(@wh_id, @@wh_id)
    and poldat_view.rtnum1 = 1] catch(-1403)
|
if (@? = 0)
{
    /* We're supposed to print a packing slip for the shipment
     * when the trailer is dispatched
     */
    [select rtstr1 packingslip_rpt_prtadr
       from poldat_view
      where poldat_view.polcod = 'VAR-REPORTS-LABELS'
        and poldat_view.polvar = 'PACKINGSLIP'
        and poldat_view.polval = 'PRINTER'
        and wh_id = nvl(@wh_id, @@wh_id)] catch(-1403)
    |
    [select rtstr1 packingslip_rpt_name
       from poldat_view
      where poldat_view.polcod = 'VAR-REPORTS-LABELS'
        and poldat_view.polvar = 'PACKINGSLIP'
        and poldat_view.polval = 'REPORT'
        and wh_id = nvl(@wh_id, @@wh_id)] catch(-1403)
    |
    /* Make sure we've got everything we need */
    [select distinct 'x'
       from dual
      where length(@packingslip_rpt_prtadr) > 0
        and length(@packingslip_rpt_name) > 0] catch(-1403)
    |
    if (@? = 0)
    {
        publish data
         where lodnum = @srclod
           and prtadr = @packingslip_rpt_prtadr
        /* !!!!!!!!!AND WHAT ELSE - won't know until they develop their label !!!!!!! */
        |
        /* 12112013 - Removing the logic to export report to a file and printing the report instead.
           publish data where rptdst = 'C:\RedPrairie\rpt_bra\LES\reports\emailed_reports\' || @packingslip_rpt_name || '-' || to_char(sysdate,'YYYYMMDD') || '-' || to_char (sysdate,'HH24MISS') || '.pdf'
           |
         */
		create deferred execution
		where deferred_cmd = "generate moca report
								 where dest_typ = 'printer'
								   and dest = '" || @packingslip_rpt_prtadr || "'
								   and num_copies = 1
								   and gen_usr_id = 'SUPER'
								   and rpt_id = '" || @packingslip_rpt_name || "'
								   and descr = 'Packing Slip'
								   and keep_days = '14'
								   and log_event = '0'
								   and locale_id = '" || nvl(@locale_id, @@locale_id) || "'
								   and ship_id ='" || @ship_id || "'
								   and wh_id ='" || @wh_id || "'
								   and vc_host_move_id ='" || @vc_host_move_id || "'
								   and vc_sold_to_num ='" || @vc_sold_to_num || "'
								   and trlr_id ='"||@trlr_id||"'
                                   |
                                    write daily transaction
                                     where actcod = 'PRTINT-PACKING'
                                       and ship_id = '" || @ship_id || "'
                                       and wh_id = '" || @wh_id || "'
                                       and adj_ref1 = '" || @vc_host_move_id || "'
                                       and adj_ref2 = '" || @vc_sold_to_num || "'"
    }
}
}
]]>
</local-syntax>
<documentation>
<summary>Print shipping label for a pallet.
</summary>
</documentation>
</command>
