<command>
<name>list var replen issues</name>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[

/*
 * This is the command for the Replenishment Reason Summary DDA
 *
 * HISTORY
 * JJS 03/05/2009 - This was borrowed from some other project and
 *                  has been modified for use here at Hopewell.
 *
 */

[ select r.prtnum,
         r.prt_client_id,
         ol.invsts_prg invsts,
         sum(r.pckqty) rpl_cnt,
         min(r.pckqty) min_pckqty
    from ord_line ol,
         shipment_line sl,
         rplwrk r
   where r.rplsts in ('I', 'F')
     and sl.ship_line_id = r.ship_line_id
     and ol.ordnum = sl.ordnum
     and ol.client_id = sl.client_id
     and ol.ordlin = sl.ordlin
     and ol.ordsln = sl.ordsln
     and @+r.prtnum
   group by r.prtnum,
            r.prt_client_id,
            ol.invsts_prg ] catch (-1403)
|
if (@prtnum is null)
{
    publish data where prtnum = 'NONE' and err_stat = 'No Replenishment Issues'
}
else
{

    [select count(distinct invlod.stoloc) non_prime_locs
       from invlod,
            invsub,
            invdtl,
            locmst,
            aremst,
            rplcfg
      where invdtl.subnum = invsub.subnum
        and invsub.lodnum = invlod.lodnum
        and invlod.stoloc = locmst.stoloc
        and locmst.arecod = aremst.arecod
        and aremst.wh_id = locmst.wh_id
        and invlod.wh_id = locmst.wh_id
        and aremst.fwiflg = 1
        and locmst.pckflg = 1
        and invdtl.prtnum = @prtnum
        and locmst.wh_id = nvl(@wh_id, @@wh_id)
        and rplcfg.stoloc != locmst.stoloc(+)] catch(-1403)
    |
    [select count(distinct invlod.stoloc) non_prime_locs_bad_status
       from invlod,
            invsub,
            invdtl,
            locmst,
            aremst,
            rplcfg
      where invdtl.subnum = invsub.subnum
        and invsub.lodnum = invlod.lodnum
        and invlod.stoloc = locmst.stoloc
        and locmst.arecod = aremst.arecod
        and aremst.wh_id = locmst.wh_id
        and invlod.wh_id = locmst.wh_id
        and aremst.fwiflg = 1
        and locmst.pckflg = 1
        and (locmst.useflg = 0 or locmst.cipflg = 1 or locmst.locsts = 'I')
        and invdtl.prtnum = @prtnum
        and locmst.wh_id = nvl(@wh_id, @@wh_id)
        and rplcfg.stoloc != locmst.stoloc(+)] catch(-1403)
    |
    if (@non_prime_locs > 0 and (@non_prime_locs - @non_prime_locs_bad_status < 1))
    {
        /* There's inventory but all the locations are either not useable,
         * have counts in progress, or are in error
         */
        publish data where err_stat = 'Bad Location Statuses or Counts in Progress'
    }
    else
    {
        [ select 'X' from rplcfg where prtnum = @prtnum] catch (-1403)
        |
        if (@? = -1403)
        {
            publish data where err_stat = 'No Assigned Location'
        }
        else
        {
            [ select count(*) err_loc_cnt
                from invsum i,
                     locmst l,
                     rplcfg r
               where r.prtnum = @prtnum
                 and r.prt_client_id = @prt_client_id
                 and l.stoloc = r.stoloc
                 and i.stoloc (+) = r.stoloc
                 and (l.locsts != 'I'
                   or i.invsts != @invsts ) ]
            |
            if (@err_loc_cnt = 0)
            {
                publish data
                  where err_stat = 'Error with Prime Location'
            }
        }
    }
    |
    publish data where err_stat = @err_stat
}
/* !!!!!!!!!!this has vc_ stuff we don't have... is this fixable???
|
if (@err_stat = '')
{
    [ select distinct(f.caslen * f.caswid * f.cashgt * @min_pckqty) min_volume
        from ftpmst f,
             prtmst p,
             rplwrk r
       where r.prtnum = @prtnum
         and r.prt_client_id = @prt_client_id
         and p.prtnum = r.prtnum
         and p.prt_client_id = r.prt_client_id
         and f.ftpcod = p.ftpcod ]
    |
    [ select maxqvl,
             curqvl,
             nvl(r.vc_thrhld, z.vc_thrhld) vc_thrhld,
             z.vc_ovrare
        from zonmst z,
             locmst l,
             rplcfg r
       where r.prtnum = @prtnum
         and r.prt_client_id = @prt_client_id
         and l.stoloc = r.stoloc
         and z.wrkzon = l.wrkzon
         and l.arecod = r.arecod ]
    |
    if (@min_volume / @maxqvl < @vc_thrhld / 100)
    {
        if (@min_volume > @maxqvl - @curqvl)
        {
            publish data
              where err_stat = 'Prime Is Full'
        }
    }
    else
    {
        [ select count(stoloc) cnt
            from locmst l
           where arecod = @vc_ovrare
             and locsts = 'E' ]
        |
        if (@cnt = 0)
        {
            publish data
              where err_stat = 'Surplus Primes Are Full'
        }
    }
}
********************/
|
if (@err_stat = '')
{
    [ select sum(invdtl.untqty) totqty
        from prtmst_view,
             aremst,
             locmst,
             invdtl,
             invsub,
             invlod
       where aremst.fwiflg = 1
         and aremst.arecod = locmst.arecod
         and locmst.stoloc = invlod.stoloc
         and invlod.lodnum = invsub.lodnum
         and invsub.subnum = invdtl.subnum
         and invdtl.prtnum = prtmst_view.prtnum
         and invdtl.prt_client_id = prtmst_view.prt_client_id
         and prtmst_view.wh_id = nvl(@wh_id, @@wh_id)
         and prtmst_view.wh_id = invlod.wh_id
         and prtmst_view.wh_id = aremst.wh_id
         and prtmst_view.wh_id = locmst.wh_id
         and invdtl.prtnum = @prtnum
         and invdtl.prt_client_id = @prt_client_id
         and invdtl.invsts = @invsts
         and invdtl.ship_line_id is null ]
    |
    if (@rpl_cnt > @totqty)
    {
        publish data
          where err_stat = 'No Inventory In Warehouse'
    }
}
/* !!!!!!!!!!This was custom - don't think we can use...
|
if (@err_stat = '')
{
    [ select count(*) cnt
        from prtmst,
             aremst,
             locmst,
             invdtl,
             invsub,
             invlod
       where aremst.fwiflg = 1
         and aremst.arecod = locmst.arecod
         and aremst.bldg_id = 'X5'
         and locmst.stoloc = invlod.stoloc
         and locmst.locsts != 'I'
         and invlod.lodnum = invsub.lodnum
         and invsub.subnum = invdtl.subnum
         and invdtl.prtnum = prtmst.prtnum
         and invdtl.prt_client_id = prtmst.prt_client_id
         and invdtl.prtnum = @prtnum
         and invdtl.prt_client_id = @prt_client_id
         and invdtl.invsts = @invsts
         and invdtl.ship_line_id is null ]
    |
    if (@cnt = 0)
    {
        publish data
          where err_stat = 'X5 Surplus Inventory in Error'
    }
}
***************************/
/* !!!!!!!!!!This was custom - don't think we can use...
|
if (@err_stat = '')
{
    [ select count(*) cnt
        from prtmst,
             aremst,
             locmst,
             invdtl,
             invsub,
             invlod
       where aremst.arecod in ('REC100', 'RMA200', 'PKDEP', 'PROB99M', 'KITS300')
         and aremst.arecod = locmst.arecod
         and locmst.stoloc = invlod.stoloc
         and invlod.lodnum = invsub.lodnum
         and invsub.subnum = invdtl.subnum
         and invdtl.prtnum = prtmst.prtnum
         and invdtl.prt_client_id = prtmst.prt_client_id
         and invdtl.prtnum = @prtnum
         and invdtl.prt_client_id = @prt_client_id
         and invdtl.invsts = @invsts
         and invdtl.ship_line_id is null ]
    |
    if (@cnt > 0)
    {
        publish data
          where err_stat = 'Inventory in REC100 or RMA200'
    }
}
***************************/
|
if (@err_stat = '')
{
    [ select count(*) cnt
        from invsum
       where prtnum = @prtnum
         and prt_client_id = @prt_client_id
         and invsts = @invsts
         and untqty > comqty ]
    |
    if (@cnt = 0)
    {
        publish data
          where err_stat = 'Existing Inventory All Committed'
    }
}
/* !!!!!!!!!!This was custom - don't think we can use...
|
if (@err_stat = '')
{
    [ select count(*) cnt
        from prtmst,
             aremst,
             locmst,
             invdtl,
             invsub,
             invlod
       where aremst.fwiflg = 1
         and aremst.arecod = locmst.arecod
         and aremst.bldg_id != 'X5'
         and locmst.stoloc = invlod.stoloc
         and invlod.lodnum = invsub.lodnum
         and invsub.subnum = invdtl.subnum
         and invdtl.prtnum = prtmst.prtnum
         and invdtl.prt_client_id = prtmst.prt_client_id
         and invdtl.prtnum = @prtnum
         and invdtl.prt_client_id = @prt_client_id
         and invdtl.invsts = @invsts
         and invdtl.ship_line_id is null ]
    |
    if (@cnt > 0)
    {
        publish data
          where err_stat = 'Inventory Not In X5'
    }
}
********************************/
|
publish data
  where prtnum = @prtnum
    and err_stat = nvl(@err_stat, 'Needs Research')

]]>
</local-syntax>

</command>

