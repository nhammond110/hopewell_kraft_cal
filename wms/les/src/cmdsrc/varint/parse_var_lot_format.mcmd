<command>
<name>parse var lot format</name>
<description>Takes a lotcod and returns mandte or expire_dte</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
/*
 * This command takes a prtnum and lotnum and outputs
 * mandte and expire_dte, based on parsing of the lotnum.
 * NOTE: invsts no longer required as an argument - does nothing
 *
 * Two custom fields in prtmst control how the lot number gets parsed:
 * - vc_lbl_date_type - 2 char indicator of date format (see below)
 * - vc_itm_cod_dte_typ - {1, 2} The date in the lot code is:
 *                         the mandte when 1, the expire_dte when 2
 *
 * The following lotnum statuses (vc_lbl_date_type) are supported by this command:
 *
 * CY=YYMMDD / ex.08JL09, 
 * CM=MMDDYY / ex.JL0908, 
 * CD=DDMMYY / ex.09JL08, 
 * DY=YYMMDD / ex.080709, 
 * DM=MMDDYY / ex.070908, 
 * DD=DDMMYY / ex.090708,              
 * JU=julian day+M+Y / ex.191M8,             
 * J2=Y+M+julian day / ex.8M191,                  
 * J3=Y+julian day+M / ex.8191M, update: also Y+julian day+X[XX...XX], with
 *     the latter format being at least 5 chars but indefinite length overall
 * N1=DDMMYYKPSL / ex.09JL08KE12, NOTE: changed from original 090708KE12
 * N2=YYMMDDKPSL / ex.08JL09KE12, NOTE: changed from original 080709KE12
 * N3=Y+julian day+KPSL / ex.8191KE12
 * 
 * M = manufacturing site
 * KPSL (K=Kraft, P=manufacturing plant, S=shift, L=packaging line)
 *
 * HISTORY
 * JJS 12/03/2008 - Creation.
 * JJS 01/20/2008 - Don't base date calculations on invsts.  Use the max
 *                  max agehrs from the aging profile always.
 * JJS 02/17/2009 - On third thought, they want to use prtmst.time_to_warn_for_exp
 *                  as shelf life instead of max(agehrs)
 * JJS 03/09/2009 - Update to J3 format (indefinite length).
 *                  Also, changes to J3, N3, JU, and J2 to account for the "10
 *                  year issue"...i.e. these are the ones with single digit
 *                  years (see "convert var 1digit year to 4digit" for more info).
 * JJS 03/23/2009 - Check lotnum length using "validate var lotnum".
 * LBS 09/29/2009 - Added check so screen doesn't error on entry
 */
if (@prtnum != '<><>' AND @lotnum != '<><>')
{
    if (! @lotnum)
    {
        /* 80170 = Required field missing:  lotnum */
        set return status
         where status = 80170
           and varnam = 'Lot Number'
    }
    |
    /* Some minimal validation on the lotnum (max length) */
    validate var lotnum
     where lotnum = @lotnum
    |
    if (! @prtnum)
    {
        /* 80170 = Required field missing:  prtnum */
        set return status
         where status = 80170
           and varnam = 'Part Number'
    }
    |
    [select distinct vc_lbl_date_type,
            vc_itm_cod_dte_typ,
            time_to_warn_for_exp agedays
       from prtmst_view
      where prtnum = @prtnum
        and wh_id = nvl(@wh_id, @@wh_id)] catch(-1403)
    |
    if (@? = -1403)
    {
        /* 80171 = Part Number lookup failed */
        set return status
         where status = 80171
    }
    |
    if (@vc_lbl_date_type = 'CY')
    {
        /* Ex. CY=YYMMDD / ex.08JL09 */
        if (length(@lotnum) != 6)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where YY = substr(@lotnum, 1, 2)
        |
        convert var 2char month to 3char
         where month_2char = substr(@lotnum, 3, 2)
        |
        publish data
         where MM = @month_3char
        |
        publish data
         where DD = substr(@lotnum, 5, 2)
        |
        [select to_date(@YY || @MM || @DD, 'YYMONDD') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'DM')
    {
        /* Ex. DM=MMDDYY / ex.070908 */
        if (length(@lotnum) != 6)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where MM = substr(@lotnum, 1, 2)
        |
        publish data
         where DD = substr(@lotnum, 3, 2)
        |
        publish data
         where YY = substr(@lotnum, 5, 2)
        |
        [select to_date(@MM || @DD || @YY, 'MMDDYY') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'J3')
    {
        /* Ex. J3=Y+julian day+M / ex.8191M 
         * Update: also Y+julian day+X[XX...XX], with
         *         the latter format being at least 5 chars but indefinite length overall
         */
        if (length(@lotnum) < 5)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where one_digit_year = substr(@lotnum, 1, 1)
        |
        convert var 1digit year to 4digit
         where one_digit_year = @one_digit_year
        |
        publish data
         where YYYY = @four_digit_year
        |
        publish data
         where JD = substr(@lotnum, 2, 3)
        |
        [select to_date(@YYYY || @JD, 'YYYYDDD') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'N2')
    {
        /* Ex. N2=YYMMDDKPSL / ex.08JL09KE12 */
        if (length(@lotnum) != 10)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where YY = substr(@lotnum, 1, 2)
        |
        convert var 2char month to 3char
         where month_2char = substr(@lotnum, 3, 2)
        |
        publish data
         where MM = @month_3char
        |
        publish data
         where DD = substr(@lotnum, 5, 2)
        |
        [select to_date(@YY || @MM || @DD, 'YYMONDD') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'N3')
    {
        /* Ex. N3=Y+julian day+KPSL / ex.8191KE12 */
        if (length(@lotnum) < 6 or length(@lotnum) > 8)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where one_digit_year = substr(@lotnum, 1, 1)
        |
        convert var 1digit year to 4digit
         where one_digit_year = @one_digit_year
        |
        publish data
         where YYYY = @four_digit_year
        |
        publish data
         where JD = substr(@lotnum, 2, length(@lotnum) - 5)
        |
        [select to_date(@YYYY || @JD, 'YYYYDDD') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'CM')
    {
        /* Ex. CM=MMDDYY / ex.JL0908 */
        if (length(@lotnum) != 6)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        convert var 2char month to 3char
         where month_2char = substr(@lotnum, 1, 2)
        |
        publish data
         where MM = @month_3char
        |
        publish data
         where DD = substr(@lotnum, 3, 2)
        |
        publish data
         where YY = substr(@lotnum, 5, 2)
        |
        [select to_date(@MM || @DD || @YY, 'MONDDYY') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'CD')
    {
        /* Ex. CD=DDMMYY / ex.09JL08 */
        if (length(@lotnum) != 6)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where DD = substr(@lotnum, 1, 2)
        |
        convert var 2char month to 3char
         where month_2char = substr(@lotnum, 3, 2)
        |
        publish data
         where MM = @month_3char
        |
        publish data
         where YY = substr(@lotnum, 5, 2)
        |
        [select to_date(@DD || @MM || @YY, 'DDMONYY') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'DY')
    {
        /* Ex. DY=YYMMDD / ex.080709 */
        if (length(@lotnum) != 6)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where YY = substr(@lotnum, 1, 2)
        |
        publish data
         where MM = substr(@lotnum, 3, 2)
        |
        publish data
         where DD = substr(@lotnum, 5, 2)
        |
        [select to_date(@YY || @MM || @DD, 'YYMMDD') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'DD')
    {
        /* Ex. DD=DDMMYY / ex.090708 */
        if (length(@lotnum) != 6)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where DD = substr(@lotnum, 1, 2)
        |
        publish data
         where MM = substr(@lotnum, 3, 2)
        |
        publish data
         where YY = substr(@lotnum, 5, 2)
        |
        [select to_date(@DD || @MM || @YY, 'DDMMYY') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'JU')
    {
        /* Ex. JU=julian day+M+Y / ex.191M8 */
        if (length(@lotnum) < 3 or length(@lotnum) > 5)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where JD = substr(@lotnum, 1, length(@lotnum) - 2)
        |
        publish data
         where one_digit_year = substr(@lotnum, length(@lotnum), 1)
        |
        convert var 1digit year to 4digit
         where one_digit_year = @one_digit_year
        |
        publish data
         where YYYY = @four_digit_year
        |
        [select to_date(@JD || @YYYY, 'DDDYYYY') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'J2')
    {
        /* Ex. J2=Y+M+julian day / ex.8M191 */
        if (length(@lotnum) < 3 or length(@lotnum) > 5)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where one_digit_year = substr(@lotnum, 1, 1)
        |
        convert var 1digit year to 4digit
         where one_digit_year = @one_digit_year
        |
        publish data
         where YYYY = @four_digit_year
        |
        publish data
         where tmp_JD = substr(@lotnum, 3, length(@lotnum) - 2)
        |
        [select lpad(@tmp_JD, 3, '0') JD
           from dual]
        |
        [select to_date(@JD || @YYYY, 'DDDYYYY') my_date
           from dual]
    }
    else if (@vc_lbl_date_type = 'N1')
    {
        /* Ex. N1=DDMMYYKPSL / ex.09JL08KE12 */
        if (length(@lotnum) != 10)
        {
            /* 80173 = Bad lotnum for part - lotnum: ^lotnum^ vc_lbl_date_type: ^vc_lbl_date_type^ */
            set return status
             where status = 80173
               and lotnum = @lotnum
               and vc_lbl_date_type = @vc_lbl_date_type
        }
        |
        publish data
         where DD = substr(@lotnum, 1, 2)
        |
        convert var 2char month to 3char
         where month_2char = substr(@lotnum, 3, 2)
        |
        publish data
         where MM = @month_3char
        |
        publish data
         where YY = substr(@lotnum, 5, 2)
        |
        [select to_date(@DD || @MM || @YY, 'DDMONYY') my_date
           from dual]
    }
    else
    {
        /* 80174 = vc_lbl_date_type not supported - ^vc_lbl_date_type^ */
        set return status
         where status = 80174
           and vc_lbl_date_type = @vc_lbl_date_type
    }
    |
    /* Good - now we have @my_date which is the date that was parsed
     * from the lotnum string.  @my_date is the "Code Date"
     */
    /*****************comment out
     * NO!  Do not calculate shelf life this way!
     *  Use prtmst.time_to_warn_for_exp instead
     * [select nvl((max(agehrs) / 24), 0) agedays
     *   from agepfl
     *  where age_pflnam = @age_pflnam] catch(-1403)
     * |
     ******************/
    if (@vc_itm_cod_dte_typ = '1')
    {
        /* lotnum represents manufacture date */
        publish data
         where mandte = @my_date
           and expire_dte = @my_date + @agedays
           and coddte = @my_date
    }
    else if (@vc_itm_cod_dte_typ = '2')
    {
        /* lotnum represents expiration date */
        publish data
         where mandte = @my_date - @agedays
           and expire_dte = @my_date
           and coddte = @my_date
    }
    else
    {
        /* 80172 = Part Number has unrecognized vc_itm_cod_dte_typ value */
        set return status
         where status = 80172
           and vc_itm_cod_dte_typ = @vc_itm_cod_dte_typ
    }
}
]]>
</local-syntax>
</command>
