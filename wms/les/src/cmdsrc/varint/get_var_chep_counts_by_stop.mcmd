<command>
<name>get var chep counts by stop</name>
<description>Find command for Chep Counts DDA</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[

/*
 * This command is for the "Chep Counts" DDA which will allow querying
 * and/or updating Chep Counts (which are stored in table stop).
 *
 * Parameters:  (Anywhere from 1 to all of these may be entered)
 *    trlr_id
 *    stop_id
 *    ordnum
 *    ship_id
 *
 * HISTORY
 * JJS 12/18/2008 - Creation.
 * JJS 01/06/2008 - Changes to only return one row per stop (i.e. return
 *                  order_list and shipment_list instead of invidual
 *                  rows for each shipment and order).  Also, Find
 *                  everything for the trailer no matter the user entered.
 *
 *
 */

/* First get the trlr_id we are dealing with because then we will turn
 * right around and grab all the stops/orders/shipments for this trlr.
 * Per agreement with Hopewell, they want to see EVERYTHING on the
 * trailer, regardless of whether they just entered a single order,
 * shipment or stop.
 */
[select distinct trlr.trlr_id
   from trlr,
        car_move,
        stop,
        shipment,
        shipment_line
  where trlr.trlr_id = car_move.trlr_id
    and car_move.car_move_id = stop.car_move_id
    and stop.stop_id = shipment.stop_id
    and shipment.ship_id = shipment_line.ship_id
    and @+trlr.trlr_id
    and @+stop.stop_id
    and @+shipment_line.ordnum
    and @+shipment_line.ship_id]
|
[select distinct stop.vc_chep_full,
        stop.vc_chep_half,
        trlr.trlr_id,
        stop.stop_id
   from trlr,
        car_move,
        stop,
        shipment,
        shipment_line
  where trlr.trlr_id = car_move.trlr_id
    and car_move.car_move_id = stop.car_move_id
    and stop.stop_id = shipment.stop_id
    and shipment.ship_id = shipment_line.ship_id
    and trlr.trlr_id = @trlr_id
  order by stop.stop_id]
|
/* Now build an order_list and shipment_list for each stop returned above */
[select distinct shipment_line.ordnum
   from trlr,
        car_move,
        stop,
        shipment,
        shipment_line
  where trlr.trlr_id = car_move.trlr_id
    and car_move.car_move_id = stop.car_move_id
    and stop.stop_id = shipment.stop_id
    and shipment.ship_id = shipment_line.ship_id
    and trlr.trlr_id = @trlr_id
    and stop.stop_id = @stop_id
] catch(-1403) >> order_list_res
|
if (rowcount(@order_list_res) > 0)
{
    convert column results to string
     where resultset = @order_list_res
       and colnam = 'ordnum'
       and separator = ','
    |
    publish data where order_list = @result_string
}
else
{
    publish data where order_list = ''
}
|
[select distinct shipment.ship_id
   from trlr,
        car_move,
        stop,
        shipment
  where trlr.trlr_id = car_move.trlr_id
    and car_move.car_move_id = stop.car_move_id
    and stop.stop_id = shipment.stop_id
    and trlr.trlr_id = @trlr_id
    and stop.stop_id = @stop_id
] catch(-1403) >> shipment_list_res
|
if (rowcount(@shipment_list_res) > 0)
{
    convert column results to string
     where resultset = @shipment_list_res
       and colnam = 'ship_id'
       and separator = ','
    |
    publish data
     where shipment_list = @result_string
}
else
{
    publish data
     where shipment_list = ''
}
|
publish data
   where vc_chep_full  = @vc_chep_full
     and vc_chep_half  = @vc_chep_half
     and trlr_id       = @trlr_id
     and stop_id       = @stop_id
     and order_list    = @order_list
     and shipment_list = @shipment_list

]]>
</local-syntax>
<documentation>
<summary>This command is called by the Chep Counts DDA.
</summary>
</documentation>
</command>
