<command>

<name>get var pallet build lodnum</name>

<description>Find a existing lodnum already started in pallet build for the passed-in inventory id</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[

/*
 * Find a existing lodnum already started in pallet build for the
 * passed-in inventory id.
 * 
 * Argument
 *    - wh_id
 *    - palpos
 *    - subucc (inventory ID)
 *
 * Returns:
 *    * lodnum (if found)
 *    * -1403  (if not found, or not enabled via policy)
 *
 * HISTORY
 * JJS 08/11/2009 - Creation.
 *
 */

[select distinct 'x'
   from poldat_view
  where polcod = 'VAR-PALLET-BUILD'
    and polvar = 'AUTO-DEPOSIT-TO-LODNUM'
    and polval = 'ENABLED'
    and wh_id = nvl(@wh_id, @@wh_id)
    and rtnum1 = 1]
|
/* Find the ship_id of the subucc that was passed in */
[select distinct ship_id
   from shipment_line,
        invdtl,
        invsub
  where shipment_line.ship_line_id = invdtl.ship_line_id
    and invdtl.subnum = invsub.subnum
    and invsub.subucc = @subucc]
|
/* Find a lodnum already in progress with the same ship_id */
[select distinct invlod.lodnum
   from invlod,
        invsub,
        invdtl,
        shipment_line
  where invlod.lodnum = invsub.lodnum
    and invsub.subnum = invdtl.subnum
    and invlod.palpos = @palpos
    and invlod.wh_id = @wh_id
    and invdtl.ship_line_id = shipment_line.ship_line_id
    and shipment_line.ship_id = @ship_id]


]]>
</local-syntax>


<documentation>
<remarks>
<![CDATA[
  <p>
  Find a existing lodnum already started in pallet build for the passed-in inventory id
  </p>
]]>
</remarks>

</documentation>

</command>
