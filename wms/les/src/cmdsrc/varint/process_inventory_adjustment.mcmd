<command>
<name>process inventory adjustment</name>
<description>pretrigger for the standard product command</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[

/*
* This is a pre-trigger on the standard product command "process inventory adjustment".
*
*
* HISTORY
* JJS 12/20/2008 - Creation.
* JJS 01/23/2009 - Temporarily skipping checks in pre-trigger until we figure out
*                  how to handle when this is called from Inventory Adjustment
*                  Operations vs. Receive Truck Operations > Identify.  The latter
*                  doesn't even have @adj_ref2 so is always erroring out.
*                  I added 1 = 2 to these checks to skip them.
* JJS 03/24/2009 - Validate the lotnum (if passed in).  We need to do this here
*                  because even though we've added an les_var_valdt entry to
*                  validate lotnum's, GUI inventory adjustments will validate,
*                  pop up a message saying lotnum is bad, and then allow the user
*                  to continue with the bad lotnum.  So, this will stop them from
*                  actually saving bad data.
* JJS 03/27/2009 - Validate ADJ_REF2 for Inter-Branch return reason codes.
*                  (Change request 290109-2)
* JJS 04/26/2009 - Set vc_lodnum session variable for all transactions (not just
*                  inter-branch returns, as we did before... just didn't realize
*                  this change was needed for all INV-ADJ that are adds)
*
* TTT 02/24/2011 - Retrieve manufacture and expiration date from existing INVDTL
*                  for same load number, item number, and lot number rather than
*                  recalculating when adding inventory.
*                  Support Case 655517
*
*
*/


/* !!!!!!!!!skipping for now via 1 = 2 */
if ((!@adj_ref2 or length(@adj_ref2) < 1) and 1 = 2)
{
   /* !!!!!! JJS note: At first I thought they only wanted this validation for
    *        certain reason codes, but now they seem to be saying they ALWAYS
    *        want to make this field has been entered... O.K. - code it that
    *        way for now...
    */

   /* 81555 = 'Must enter ORDER # / RECEIPT # (aka Adjustment Reference Two)' */
   set return status where status = 81555
}
|
publish data where adding_inventory = 'N'
|
/* Are they adding this pallet to the system? */
[select distinct 'x' from invlod where lodnum = @lodnum] catch(-1403)
|
if (@? = -1403)
{
   /* lodnum does not exist, so we are adding inventory */
   publish data where adding_inventory = 'Y'
}
|
/* !!!!!!!!!skipping for now via 1 = 2 */
if (@adding_inventory = 'Y'
    and (@reacod = 'RORO' or
         @reacod = 'MIRN' or
         @reacod = 'MIRO' or
         @reacod = 'SH')
    and 1 = 2
   )
{
   /* Mod - special checks for the following reason codes.  If we are adding
    * a pallet under these reason codes, we can assume that this pallet is
    * one that shipped logically but got physically left in the warehouse
    * These are the reason codes -- for further enhancement, we could perhaps
    * put these in a policy:   !!!!!!!!!!!!
    *
    * RORO    Customer Return
    * MIRN    PO Return to Vendor
    * MIRO    Reverse Customer Return
    * SH      Transfer Order Correction
    */
   if (!@adj_ref2 or length(@adj_ref2) < 1)
   {
      /* 81555 = 'Must enter ORDER # / RECEIPT # (aka Adjustment Reference Two)' */
      set return status where status = 81555
   }
   |
   [select distinct 'x'
      from invlod, invsub, invdtl, shipment_line
     where invlod.lodnum like @lodnum || 'X%'
       and invlod.lodnum = invsub.lodnum
       and invsub.subnum = invdtl.subnum
       and invdtl.ship_line_id = shipment_line.ship_line_id
       and shipment_line.ordnum = @adj_ref2] catch(-1403)
   |
   if (@? = -1403)
   {
      /* 81556 = 'The ORDER # entered does not match the original order number for that inventory' */
      set return status where status = 81556
   }
}
|
if (@lotnum)
{
   validate var lotnum where lotnum = @lotnum
}
|
[select distinct 'x'
   from poldat_view
  where polcod = 'VAR-INTEGRATION'
    and polvar = 'INV-ADJ-TRNTYP'
    and polval = @reacod
    and rtstr2 = 'IB'
    and wh_id  = nvl(@wh_id, @@wh_id)] catch(-1403)
|
if (@? = 0)
{
   /* This reason code is one of the "Inter-Branch return" reason codes.
    * Do some extra checking.  ADJ_REF2 is of the format CR12345, that is
    * <2 digit reason code><5 digit number>.
    */
   if (!@adj_ref2 or length(@adj_ref2) != 7)
   {
      /* 81333 = 'Adjustment Reference2 must be 7 characters for
       *          Inter-Branch returns'
       */
      set return status where status = 81333
   }
   |
   if (substr(@adj_ref2, 1, 2) != @reacod)
   {
      /* 81334 = 'Adjustment Reference2 must have the first two characters
       *          matching the reason code given for Inter-Branch returns'
       */
      set return status where status = 81334
   }
   |
   [select to_number(substr(@adj_ref2, 3, 5)) from dual] catch(@?)
   |
   if (@? != 0)
   {
      /* 81335 = 'Adjustment Reference2 must end in a 5 digit number
       *          for Inter-Branch returns'
       */
      set return status where status = 81335
   }
}
|
/* For some reason, the INV-ADJ is being passed PERM-ADJ-LOD-0020 as
* the lodnum (which then becomes VC_LODNUM in the transaction), so
* we set a session variable for receive_inventory-write_host_transaction.mtrg
* to read
*/
save session variable where name = 'vc_lodnum' and value = @lodnum
|
/* Retrieve dates from previous INVDTL */
[select mandte, expire_dte
from invsub, invdtl
where invsub.lodnum = @lodnum
    and invsub.subnum = invdtl.subnum
    and invdtl.prtnum = @prtnum
    and invdtl.lotnum = @lotnum
    and rownum = 1] catch(510, -1403)
|
if (@? = '0')
   {
      publish data 
         where whereclause = @whereclause ||
            " and mandte = '" || @mandte || "'" ||
            " and expire_dte = '" || @expire_dte || "'"
   }
|
^process inventory adjustment


]]>
</local-syntax>
<documentation>
<summary>This is a pretrigger on the standard product command to do some up-front validations.
</summary>
</documentation>
</command>