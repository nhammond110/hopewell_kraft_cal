<command>

<name>write var wave table entry</name>

<description>Insert a row into the vc_wave table</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[

/*
 * Insert a row into the vc_wave table.
 * 
 * Argument
 *    - wrkref
 *
 * HISTORY
 * JJS 06/18/2009 - Creation.
 * JJS 06/30/2009 - Fix a typo/bug, change "if (@? != 0)" to "if (@? = 0)"
 *                  after the select from pckwrk.  It *should* be inserting
 *                  into the wave table if we get data back.  This isn't a
 *                  huge deal because Dematic will insert the row if it scans
 *                  a carton which isn't in the table.  However, this code is
 *                  bombing out on pallet picks which happen to be in the wave
 *                  because they don't have a subucc and it tries to insert
 *                  based on a NULL subucc.
 *
 */


/* Pull the information from the pckwrk.subucc.  The vc_wave
 * table is used by Dematic as the case gets scanned.  We are
 * completely, totally and utterly relying on the previous call to
 * "generate ucc128 identifier" (see the VAR version) to have produced
 * a subucc # in the correct format.
 */
[select distinct substr(subucc, 1, 6)  pad_car_move_id,
                 substr(subucc, 7, 8)  pad_schbat,
                 substr(subucc, 15, 6) pad_new_seq_ctn_num,
                 wh_id my_wh_id
   from pckwrk
  where wrkref = @wrkref
    and subucc is not null] catch(-1403)
|
if (@? = 0)
{
   /* Write this carton (case actually) to the vc_wave table...make
    * sure this exact row doesn't exist yet
    */
   [select distinct 'x' from vc_wave
     where car_move_id = @pad_car_move_id
       and schbat      = @pad_schbat
       and carton_id   = @pad_new_seq_ctn_num
       and wh_id       = @my_wh_id] catch(-1403)
   |
   if (@? != 0)
   {
      [insert into vc_wave (car_move_id, schbat, carton_id,
                            wh_id, divert_lane, divert_dte, scanned_cnt,
                            adddte, moddte)
                     values (@pad_car_move_id, @pad_schbat, @pad_new_seq_ctn_num,
                             @my_wh_id, '', '', 0,
                             sysdate, '')]
   }
}


]]>
</local-syntax>


<documentation>
<remarks>
<![CDATA[
  <p>
  Insert a row into the vc_wave table.
  </p>
]]>
</remarks>

</documentation>

</command>
