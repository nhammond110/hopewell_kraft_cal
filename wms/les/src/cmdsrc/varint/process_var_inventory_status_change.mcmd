<command>

<name>process var inventory status change</name>

<description>Changes the inventory status for inventory details or work order lines</description>

<type>C Function</type>

<function>varProcessInventoryStatusChange</function>
<argument name="to_invsts" datatype="string"> </argument>
<argument name="prc_hldpfx" datatype="string"> </argument>
<argument name="prc_hldnum" datatype="string"> </argument>
<argument name="dtlnum" datatype="string"> </argument>
<argument name="wkonum" datatype="string"> </argument>
<argument name="client_id" datatype="string"> </argument>
<argument name="wkorev" datatype="string"> </argument>
<argument name="wkolin" datatype="string"> </argument>
<argument name="seqnum" datatype="integer"> </argument>
<argument name="dtlnum_list" datatype="string"> </argument>
<argument name="wkonum_list" datatype="string"> </argument>
<argument name="client_id_list" datatype="string"> </argument>
<argument name="wkorev_list" datatype="string"> </argument>
<argument name="wkolin_list" datatype="string"> </argument>
<argument name="seqnum_list" datatype="string"> </argument>
<argument name="eval_dtcflg" datatype="integer"> </argument>
<argument name="validate_invsum" datatype="integer"> </argument>
<argument name="prc_reacod" datatype="string"> </argument>
<argument name="wh_id" datatype="string"> </argument>

<documentation>

<remarks>
<![CDATA[
  <p>
    *****This is a VAR version of the standard product command ******
    *****We have a Local Syntax pre-trigger on process inventory status change, *****
    *****so this points to the VAR C code *****
    This command changes the inventory status of one or more pieces of 
    inventory.  This command also can search the component tracking 
    tables, and make changes based on the contents of assembled inventory.
    This command will only make changes to inventory in SIGNIFICANT,
    PICKABLE locations, plus work order expected receipts locations.
  </p>
  <p>
    <str>NOTE:</str> This command will pass all parameters along to 
    "list inventory change information" if needed.  If the proper parameters
    weren't passed in to be able to get the details, the list command will
    be called and those results are used to perform the change.
  </p>
]]>
</remarks>

<retcol name="exec_sts" type="COMTYP_INT">Execution status.  This command will return all rows that it attempts
  to change.  If it fails, it will not fail the entire command, but 
  will return the status in this field.  All triggers need to take this
  into account as this command will normally succeed regardless of what
  fails in the internal processing.  A status of 1 means that the change
  failed due to the fact that the to_status did not fit into the aging 
  profile progression of inventory statuses for this inventory.</retcol>
<retcol name="bldg_id" type="COMTYP_CHAR">Building Id</retcol>
<retcol name="arecod" type="COMTYP_CHAR">Area Code</retcol>
<retcol name="stoloc" type="COMTYP_CHAR">Storage Location</retcol>
<retcol name="wh_id" type="COMTYP_CHAR">Storage Location</retcol>
<retcol name="lodnum" type="COMTYP_CHAR">Load Number</retcol>
<retcol name="subnum" type="COMTYP_CHAR">Subload Number</retcol>
<retcol name="dtlnum" type="COMTYP_CHAR">Detail Number</retcol>
<retcol name="prtnum" type="COMTYP_CHAR">Part Number</retcol>
<retcol name="prt_client_id" type="COMTYP_CHAR">Part Client ID</retcol>
<retcol name="trnqty" type="COMTYP_INT">Transaction quantity</retcol>
<retcol name="fr_invsts" type="COMTYP_CHAR">From inventory status</retcol>
<retcol name="to_invsts" type="COMTYP_CHAR">To inventory status</retcol>

<exception value="eOK">Normal successful completion</exception>

<seealso cref="list inventory change information"> </seealso>
<seealso cref="process inventory aging profile change"> </seealso>

</documentation>

</command>
