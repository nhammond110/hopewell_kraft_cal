<command>
    <name>change address</name>
    <description>When an address is changed, this command will go through and change all of the relative addresses for each client id</description>
    <type>Local Syntax</type>
    <local-syntax>
        <![CDATA[
            /* Execute the initial address change */
            ^change address
            |
            publish data
             where wh_id = nvl(@wh_id, nvl(@whse_id, nvl(@@wh_id, '----')))
               and old_client_id = @client_id
            |
            /* Check if the interim policy is enabled */
            [select 'x'
               from poldat_view
              where polcod = 'VC-INTERIM'
                and polvar = 'INSTALLED'
                and polval = 'INSTALLED'
                and rtnum1 = 1
                and wh_id = @wh_id] catch(510, -1403)
            |
            if (@? = 0)
            {
                /* We need to find all suppliers and customers with the address
                   id that was changed. To do this, we will union the cstmst and 
                   supmst tables and store the table name in vc_tabl */
                [select *
                   from (select supnum vc_num,
                                'supmst' vc_tabl,
                                adr_id adr_id
                           from supmst
                         union
                         select cstnum vc_num,
                                'cstmst' vc_tabl,
                                adr_id adr_id
                           from cstmst)
                  where adr_id = @adr_id] catch(510, -1403)
                |
				/* We only want to execute this logic if the address being
				 * changed is for a customer or a supplier.
				 */
				if(@? = 0)
				{
					/* The policy is enabled and we have an address for a customer/supplier so we need to process the previous 
					   change now, before we continue */
					{   
						set site specific columns
						|
						process table action
					} >> prc_prv_adr_chg
					|
					/* If the address was from a supplier, find any other matching
					   suppliers and grab their address id */
					if (@vc_tabl = 'supmst')
					{
						[select adr_id vc_adr_id,
								client_id new_client_id
						   from supmst
						  where supnum = @vc_num
							and exists(select 'x'
										 from supmst s
										where s.client_id in (select rtstr1
																from poldat_view
															   where polcod = 'VC-INTERIM'
																 and polvar = 'SUPPLIERS'
																 and polval = 'CLIENT_ID'
																 and wh_id = @wh_id)
										  and s.supnum = supmst.supnum)
							and client_id != @old_client_id] catch(510, -1403)
					}
					/* If the address was from a customer, find any other matching
					   customers and grab their address id */
					else if (@vc_tabl = 'cstmst')
					{
						[select adr_id vc_adr_id,
								client_id new_client_id
						   from cstmst
						  where cstnum = @vc_num
							and exists(select 'x'
										 from cstmst c
										where c.client_id in (select rtstr1
																from poldat_view
															   where polcod = 'VC-INTERIM'
																 and polvar = 'SUPPLIERS'
																 and polval = 'CLIENT_ID'
																 and wh_id = @wh_id)
										  and c.cstnum = cstmst.cstnum)
							and client_id != @old_client_id] catch(510, -1403)
					}
					|
					if (@? = 0)
					{
						publish data
						 where adr_id = @adr_id
						   and vc_adr_id = @vc_adr_id
						   and new_client_id = @new_client_id
						|
						/* Select the changed address id's params */
						[select adr_district vc_adr_adr_district,
								adr_id vc_adr_adr_id,
								adrcty vc_adr_adrcty,
								adrln1 vc_adr_adrln1,
								adrln2 vc_adr_adrln2,
								adrln3 vc_adr_adrln3,
								adrnam vc_adr_adrnam,
								adrpsz vc_adr_adrpsz,
								adrstc vc_adr_adrstc,
								adrtyp vc_adr_adrtyp,
								attn_name vc_adr_attn_name,
								attn_tel vc_adr_attn_tel,
								client_id vc_adr_client_id,
								cont_name vc_adr_cont_name,
								cont_tel vc_adr_cont_tel,
								cont_title vc_adr_cont_title,
								ctry_name vc_adr_ctry_name,
								email_adr vc_adr_email_adr,
								faxnum vc_adr_faxnum,
								first_name vc_adr_first_name,
								grp_nam vc_adr_grp_nam,
								honorific vc_adr_honorific,
								host_ext_id vc_adr_host_ext_id,
								last_name vc_adr_last_name,
								latitude vc_adr_latitude,
								locale_id vc_adr_locale_id,
								longitude vc_adr_longitude,
								pagnum vc_adr_pagnum,
								phnnum vc_adr_phnnum,
								po_box_flg vc_adr_po_box_flg,
								pool_flg vc_adr_pool_flg,
								pool_rate_serv_nam vc_adr_pool_rate_serv_nam,
								rgncod vc_adr_rgncod,
								rqst_state_cod vc_adr_rqst_state_cod,
								rsaflg vc_adr_rsaflg,
								ship_attn_name vc_adr_ship_attn_name,
								ship_attn_phnnum vc_adr_ship_attn_phnnum,
								ship_cont_name vc_adr_ship_cont_name,
								ship_cont_tel vc_adr_ship_cont_tel,
								ship_cont_title vc_adr_ship_cont_title,
								ship_email_adr vc_adr_ship_email_adr,
								ship_faxnum vc_adr_ship_faxnum,
								ship_phnnum vc_adr_ship_phnnum,
								ship_web_adr vc_adr_ship_web_adr,
								temp_flg vc_adr_temp_flg,
								tim_zon_cd vc_adr_tim_zon_cd,
								usr_dsp vc_adr_usr_dsp,
								web_adr vc_adr_web_adr
						   from adrmst
						  where adr_id = @adr_id] catch(510, -1403)
						|
						if (@? = 0)
						{
							/* Change the addresses that are relative to the address
							   that was originally changed */
							hide stack variable
							  where name = 'adr_id'
							|
							^change address
							  where client_id = @new_client_id
								and adr_id = @vc_adr_id
								and adr_district = @vc_adr_adr_district
								and adrcty = @vc_adr_adrcty
								and adrln1 = @vc_adr_adrln1
								and adrln2 = @vc_adr_adrln2
								and adrln3 = @vc_adr_adrln3
								and adrnam = @vc_adr_adrnam
								and adrpsz = @vc_adr_adrpsz
								and adrstc = @vc_adr_adrstc
								and adrtyp = @vc_adr_adrtyp
								and attn_name = @vc_adr_attn_name
								and attn_tel = @vc_adr_attn_tel
								and cont_name = @vc_adr_cont_name
								and cont_tel = @vc_adr_cont_tel
								and cont_title = @vc_adr_cont_title
								and ctry_name = @vc_adr_ctry_name
								and email_adr = @vc_adr_email_adr
								and faxnum = @vc_adr_faxnum
								and first_name = @vc_adr_first_name
								and grp_nam = @vc_adr_grp_nam
								and honorific = @vc_adr_honorific
								and host_ext_id = @vc_adr_host_ext_id
								and last_name = @vc_adr_last_name
								and latitude = @vc_adr_latitude
								and locale_id = @vc_adr_locale_id
								and longitude = @vc_adr_longitude
								and pagnum = @vc_adr_pagnum
								and phnnum = @vc_adr_phnnum
								and po_box_flg = @vc_adr_po_box_flg
								and pool_flg = @vc_adr_pool_flg
								and pool_rate_serv_nam = @vc_adr_pool_rate_serv_nam
								and rgncod = @vc_adr_rgncod
								and rqst_state_cod = @vc_adr_rqst_state_cod
								and rsaflg = @vc_adr_rsaflg
								and ship_attn_name = @vc_adr_ship_attn_name
								and ship_attn_phnnum = @vc_adr_ship_attn_phnnum
								and ship_cont_name = @vc_adr_ship_cont_name
								and ship_cont_tel = @vc_adr_ship_cont_tel
								and ship_cont_title = @vc_adr_ship_cont_title
								and ship_email_adr = @vc_adr_ship_email_adr
								and ship_faxnum = @vc_adr_ship_faxnum
								and ship_phnnum = @vc_adr_ship_phnnum
								and ship_web_adr = @vc_adr_ship_web_adr
								and temp_flg = @vc_adr_temp_flg
								and tim_zon_cd = @vc_adr_tim_zon_cd
								and usr_dsp = @vc_adr_usr_dsp
								and web_adr = @vc_adr_web_adr catch(510, -1403)
								|
								publish data 
								  where acttyp = @acttyp
									and tblnam = @tblnam
									and updlst = @updlst
									and whrlst = @whrlst
									and adr_id = @adr_id
						}
					}
				}
				else
				{
					/* Publish this data out so the trigger takes effect */
					publish data 
					  where acttyp = @acttyp
						and tblnam = @tblnam
						and updlst = @updlst
						and whrlst = @whrlst
						and adr_id = @adr_id
				}
            }
            else
            {
                /* Publish this data out so the trigger takes effect */
                publish data 
                  where acttyp = @acttyp
                    and tblnam = @tblnam
                    and updlst = @updlst
                    and whrlst = @whrlst
                    and adr_id = @adr_id
            }
        ]]>
    </local-syntax>
</command>
