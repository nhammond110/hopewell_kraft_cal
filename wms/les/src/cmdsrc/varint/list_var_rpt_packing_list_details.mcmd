<command>

<name>list var rpt packing list details</name>

<description>List Var RPT Packing List Details</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[
get client in clause for user
 where usr_id = @@usr_id
   and prt_client_id_flg = 1
   and table_prefix = 'invdtl'
|
[select pf.wh_id,
        pf.prtnum,
        pf.prt_client_id,
        pf.prtnum_dsc,
        sum(pf.ship_unit) prtshpqty,
        sum(pf.ship_weight) prtshpwgt,
        sum(pf.ship_vol) prtshpvol,
        sum(pf.pal_cnt) palcnt,
        sum(pf.lay_cnt) laycnt,
        sum(pf.cas_cnt) cascnt,
        pf.ordnum,
        pf.ordlin
   from (select pk.wh_id,
                pk.prtnum,
                pk.prt_client_id,
                pk.prtnum_dsc,
                pk.untqty,
                pk.untcst,
                pk.subtotal,
                pk.prtfam,
                sum(pk.vc_tot_shp_unt) ship_unit,
                sum(pk.vc_tot_shp_wgt) ship_weight,
                sum(pk.vc_tot_shp_vol) ship_vol,
                sum(pk.pal_cnt) pal_cnt,
                sum(pk.lay_cnt) lay_cnt,
                sum(pk.cas_cnt) cas_cnt,
                pk.ordnum,
                pk.ordlin
           from (select shipment_line.wh_id wh_id,
                        invdtl.subnum subnum,
                        invdtl.prtnum prtnum,
                        invdtl.prt_client_id prt_client_id,
                        prtdsc.lngdsc prtnum_dsc,
                        sum(invdtl.untqty) untqty,
                        prtmst_view.untcst untcst,
                        sum(invdtl.untqty) * prtmst_view.untcst subtotal,
                        prtmst_view.prtfam,
                        prtmst_view.untcas,
                        prtmst_view.grswgt,
                        ftpmst.caslvl,
                        sum(untqty),
                        sum(ord_line.ordqty) myordqty,
                        (untqty / decode(prtmst_view.stkuom, 'CS', prtmst_view.untcas, 'PA', prtmst_view.untpal, 1)) vc_tot_shp_unt,
                        ((untqty / prtmst_view.untcas) * prtmst_view.grswgt) vc_tot_shp_wgt,
                        untqty *(ftpmst.caslen / 12) *(ftpmst.caswid / 12) *(ftpmst.cashgt / 12) vc_tot_shp_vol,
                        case when mod(untqty, prtmst_view.untpal) = 0 then(untqty / prtmst_view.untpal)
                             else floor(untqty / prtmst_view.untpal)
                        end pal_cnt,
                        case when (mod(untqty, prtmst_view.untpal) <> 0 and mod(mod(untqty, prtmst_view.untpal), ftpmst.caslvl) = 0) then mod(untqty, prtmst_view.untpal) / ftpmst.caslvl
                             else floor(mod(untqty, prtmst_view.untpal) / ftpmst.caslvl)
                        end lay_cnt,
                        mod(mod(untqty, prtmst_view.untpal), ftpmst.caslvl) cas_cnt,
                        ord_line.ordnum,
                        ord_line.ordlin
                   from prtdsc,
                        prtmst_view,
                        invdtl,
                        shipment_line,
                        ord_line,
                        ftpmst
                  where prtdsc.colnam =
                 /*=varchar(*/
                 'prtnum' || '|' || 'prt_client_id' || '|' || 'wh_id_tmpl'
                 /*=)*/
                    and prtdsc.colval =
                 /*=varchar(*/
                 invdtl.prtnum || '|' || invdtl.prt_client_id || '|' || prtmst_view.wh_id_tmpl
                 /*=)*/
                    and prtdsc.locale_id = nvl(@locale_id, @@locale_id)
                    and prtmst_view.prtnum = invdtl.prtnum
                    and prtmst_view.prt_client_id = invdtl.prt_client_id
                    and invdtl.ship_line_id = shipment_line.ship_line_id
                    and shipment_line.ordnum = ord_line.ordnum
                    and shipment_line.client_id = ord_line.client_id
                    and shipment_line.ordlin = ord_line.ordlin
                    and shipment_line.ordsln = ord_line.ordsln
                    and shipment_line.wh_id = ord_line.wh_id
                    and shipment_line.wh_id = prtmst_view.wh_id
                    and ord_line.wh_id = nvl(@wh_id, ord_line.wh_id)
                    and ord_line.ordnum = nvl(@ordnum, ord_line.ordnum)
                    and ord_line.client_id = nvl(@client_id, ord_line.client_id)
                    and shipment_line.ship_id = nvl(@ship_id, shipment_line.ship_id)
                    and @client_in_clause:raw
                    and ftpmst.ftpcod = prtmst_view.ftpcod
                  group by shipment_line.wh_id,
                        invdtl.prt_client_id,
                        invdtl.subnum,
                        invdtl.prtnum,
                        prtdsc.lngdsc,
                        prtmst_view.untcst,
                        prtmst_view.prtfam,
                        prtmst_view.stkuom,
                        prtmst_view.untpal,
                        prtmst_view.untcas,
                        prtmst_view.grswgt,
                        ftpmst.caslen,
                        ftpmst.caswid,
                        ftpmst.cashgt,
                        ftpmst.caslvl,
                        prtmst_view.untpal,
                        untqty,
                        ord_line.ordqty,
                        ord_line.ordnum,
                        ord_line.ordlin) pk
          group by pk.wh_id,
                pk.prtnum,
                pk.prt_client_id,
                pk.prtnum_dsc,
                pk.untcst,
                pk.untqty,
                pk.subtotal,
                pk.prtfam,
                pk.vc_tot_shp_unt,
                pk.vc_tot_shp_wgt,
                pk.vc_tot_shp_vol,
                pk.pal_cnt,
                pk.lay_cnt,
                pk.ordnum,
                pk.ordlin) pf
  group by pf.wh_id,
        pf.prtnum,
        pf.prt_client_id,
        pf.prtnum_dsc,
        pf.ordnum,
        pf.ordlin]
|
[select distinct ord_line.ordnum,
        ord_line.ordlin,
        ord_line.ordsln,
        ord_line.wh_id,
        ord_line.ordqty,
        ord_line.ordqty - @prtshpqty srtqty,
        ord_line.prtnum
   from ord_line,
        shipment_line,
        invdtl
  where invdtl.ship_line_id = shipment_line.ship_line_id
    and shipment_line.ordnum = ord_line.ordnum
    and shipment_line.client_id = ord_line.client_id
    and shipment_line.ordlin = ord_line.ordlin
    and shipment_line.ordsln = ord_line.ordsln
    and shipment_line.wh_id = ord_line.wh_id
    and shipment_line.ship_id = @ship_id
    and shipment_line.wh_id = @wh_id
    and ord_line.ordnum = @ordnum
    and ord_line.ordlin = @ordlin]
|
[select alt_prtmst.alt_prtnum prtcodes
   from alt_prtmst
  where alt_prtmst.prtnum = @prtnum
    and alt_prtmst.prt_client_id = @prt_client_id
    and alt_prtmst.alt_prt_typ = 'UPCCOD'] catch(-1403)
|
[select inv_ser_num.ser_num
   from inv_ser_num
  where inv_ser_num.invtid = @subnum
    and inv_ser_num.ser_lvl = 'S'] catch(-1403)
|
publish data
 where wh_id = @wh_id
   AND prtnum = @prtnum
   AND prt_client_id = @prt_client_id
   AND prtnum_dsc = @prtnum_dsc
   AND prtcodes = @prtcodes
   AND prtshpqty = @prtshpqty
   AND prtshpwgt = @prtshpwgt
   AND prtshpvol = @prtshpvol
   AND palcnt = @palcnt
   AND laycnt = @laycnt
   AND cascnt = @cascnt
   AND srtqty = @srtqty
]]>
</local-syntax>

<documentation>
<remarks>
<![CDATA[
    <p>
    This command is used to display order line information for 
    Sub-PackingListDetails.
    </p>
    <p>
    Added the nvl statements to the where clause so that the report
    would generate correctly if the fields were on the stack, but
    were null.  This occurred when the report was being generated
    automatically after write stage shipment
    </p>
]]>
</remarks>

<retcol name="*" type="varies according to field">All fields from the ord table</retcol>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">No data found</exception>

</documentation>

</command>
