<command>
<name>print var picksheet report</name>
<description>Print the custom picksheet report</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[

/*
 * This command prints the custom Picksheet Report for a
 * shipment.
 *
 * Arguments
 *    - ship_id
 *
 * HISTORY
 * SYIM 01/15/2010 - Creation.
 *
 */


   /* Is auto-printing of the Picksheet Report enabled? */
   [select distinct 'x'
      from poldat_view
     where poldat_view.polcod = 'VAR-PCKST-PRINT'
       and poldat_view.polvar = 'PICKSHEET'
       and poldat_view.polval = 'ENABLED'
       and poldat_view.wh_id = nvl(@wh_id, @@wh_id)
       and poldat_view.rtnum1 = 1] catch(-1403)
   |
   if (@? = 0)
   {
      /* Get report printing params from policy*/
      [select rtstr1 dest
         from poldat_view
        where poldat_view.polcod = 'VAR-PCKST-PRINT'
          and poldat_view.polvar = 'PICKSHEET'
          and poldat_view.polval = 'PRINTER'
          and wh_id  = nvl(@wh_id, @@wh_id)] catch(-1403)
      |
      [select rtstr1 rpt_id
         from poldat_view
        where poldat_view.polcod = 'VAR-PCKST-PRINT'
          and poldat_view.polvar = 'PICKSHEET'
          and poldat_view.polval = 'REPORT'
          and wh_id  = nvl(@wh_id, @@wh_id)] catch(-1403)
      |
      /* Make sure we've got everything we need */
      [select distinct 'x' from dual
        where length(@rpt_id) > 0
          and length(@schbat) > 0] catch(-1403)
      |
      if (@? = 0)
      {
         publish data
           where @*
         |
         /* Get carrier moves out of schbat for shipments with picks from policy defined source areas and that have not yet been printed */
         [select distinct st.car_move_id
          from pckwrk pw, shipment sh, stop st, car_move cm
          where pw.schbat = @schbat
            and pw.ship_id = sh.ship_id
            and sh.stop_id = st.stop_id
            and sh.vc_prnt_pckst = 0
            and pw.srcare in (select rtstr1
                               from poldat_view
                              where polcod = 'VAR-PCKST-AREAS'
                                and (
					(polvar = 'RELEASE-CHECK')

					or

					(polvar = 'PRINT-CHECK')
				    )					
                                and polval = 'ARECOD'
                                and wh_id  = pw.wh_id)] >> carmov_res
         |
         /* Loop through each row of result set */
         do loop where count = rowcount(@carmov_res)
         |
         {
             /* Get Current Row Data */
             publish rows
               where resultset = @carmov_res
                 and rows = 1
                 and startrow = @i
             |
             print report
             where rpt_id = @rpt_id
               and dest_typ = 'printer'
               and gen_usr_id = 'SUPER'
               and @* catch(@?)
             |
             /* update printed flag */
             [update shipment set vc_prnt_pckst = 1
              where ship_id in (select distinct sh.ship_id
                                 from stop st, shipment sh
                                 where st.car_move_id = @car_move_id
                                   and st.stop_id = sh.stop_id
                                   and sh.vc_prnt_pckst = 0)]
         }
      }
   }

]]>
</local-syntax>
<documentation>
<summary>Print shipping label for a pallet.
</summary>
</documentation>
</command>
