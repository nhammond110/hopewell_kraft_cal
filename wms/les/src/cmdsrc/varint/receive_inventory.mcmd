<command>
<name>receive inventory</name>
<description>pretrigger for the standard product command</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[

/*
 * This is a pre-trigger on the standard product command "receive inventory".
 *
 *
 * HISTORY
 * JJS 04/01/2008 - Creation.
 *
 */

if (@trknum)
{
   [select sum(rcvqty) my_rcvqty
      from rcvlin
     where trknum = @trknum
       and exists (select distinct 'x'
                     from poldat_view
                    where polcod = 'VAR-INV-RCV'
                      and polvar = 'TRACK-AT-RECEIPT'
                      and polval = 'ENABLED'
                      and rtnum1 = 1
                      and wh_id = nvl(@wh_id, @@wh_id))
       and not exists (select distinct 'x'
                         from rcvtrk
                        where trknum = @trknum
                          and vc_invrcv_flg = 1)
    having sum(rcvqty) = 0] catch(-1403)
   |
   if (@? = 0 and @my_rcvqty = 0)
   {
      /* No lines received - this must be the first receipt for this
       * trknum, so we set the vc_invrcv_flg so that down the line we
       * know this truck was received under the "new" system... i.e. all
       * the pallets received got written into table vc_invrcv.
       */
      [update rcvtrk set vc_invrcv_flg = 1 where trknum = @trknum]
   }
}
|
^receive inventory


]]>
</local-syntax>
<documentation>
<summary>This is a pretrigger on the standard product command.
</summary>
</documentation>
</command>
