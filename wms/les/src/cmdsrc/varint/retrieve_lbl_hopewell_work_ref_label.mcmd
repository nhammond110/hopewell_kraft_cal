<command>

<name>retrieve lbl hopewell work ref label</name>

<description>retrieve lbl hopewell work ref label - MOCA command to retrieve data for the hopewell work ref label</description>

<type>Local Syntax</type>
<local-syntax>
<![CDATA[
[select adrmst.adrnam CUS_NAM,
        adrmst.adr_id,
        adrmst.adrln1 ADRLN1,
        adrmst.adrln2 ADRLN2,
        adrmst.adrln3 ADRLN3,
        (adrmst.adrcty || ', ' || adrmst.adrstc || ' ' || adrmst.adrpsz) ADRLN4,
        car_move.carcod CARRIER,
        car_move.car_move_id LOD_NUM,
        ord.ordnum DEL_NUM,
        to_char(shipment.late_shpdte, 'MM/DD/YYYY') SHIP_DATE,
        shipment_line.shpqty SHIP_QTY,
        ord.cponum PO_NUM,
        pckwrk.wrkref WRK_REF,
        pckwrk.ship_id SHIP_ID,
        pckwrk.srcloc PCK_LOC,
        pckwrk.prtnum PRT_NUM,
        substr(pckwrk.schbat,7,4) WAVE_NUM,
        locmst.wrkzon WRK_ZON,
        stop.stop_seq STOP_ID,
        alt_prtmst.alt_prtnum ALT_PART_NUM,
        pckwrk.subucc SUBUCC,
        prtdsc.short_dsc SHORT_DESC
   from alt_prtmst,
        shipment_line,
        shipment,
        stop,
        prtmst_view,
        prtdsc,
        car_move,
        ord_line,
        ord,
        adrmst,
        pckwrk,
        locmst
  where pckwrk.prtnum = prtmst_view.prtnum
    and pckwrk.prt_client_id = prtmst_view.prt_client_id
    and pckwrk.wh_id = prtmst_view.wh_id_tmpl
    and shipment.ship_id = shipment_line.ship_id
    and shipment.stop_id = stop.stop_id
    and stop.car_move_id = car_move.car_move_id
    and shipment.rt_adr_id = adrmst.adr_id
    and shipment_line.ordnum = ord_line.ordnum
    and shipment_line.ordlin = ord_line.ordlin
    and shipment_line.ordsln = ord_line.ordsln
    and shipment_line.client_id = ord_line.client_id
    and shipment_line.wh_id = ord_line.wh_id
    and ord_line.ordnum = ord.ordnum
    and ord_line.client_id = ord.client_id
    and ord_line.wh_id = ord.wh_id
    and shipment_line.ship_line_id = pckwrk.ship_line_id
    and shipment_line.wh_id = pckwrk.wh_id
    and pckwrk.srcloc = locmst.stoloc
    and pckwrk.prtnum = alt_prtmst.prtnum(+)
    and alt_prtmst.alt_prt_typ(+) = 'CONS'
    and @+pckwrk.wrkref
    and prtdsc.colnam = 'prtnum|prt_client_id|wh_id_tmpl'
    and prtdsc.colval = nvl(prtmst_view.prtnum, rtrim(' ')) || '|' || nvl(prtmst_view.prt_client_id, rtrim(' ')) || '|' || nvl(prtmst_view.wh_id_tmpl, rtrim(' '))
    and prtdsc.locale_id = nvl(@locale_id, @@locale_id)] catch(-1403) 
|
if (@adrln3 <= '')
{
    publish data
     where adrln3 = @adrln4
       and adrln4 = ''
}
|
if (@adrln2 <= '')
{
    if (@adrln1 <= '')
    {
        publish data
         where adrln1 = @adrln2
           and adrln2 = @adrln3
           and adrln3 = ''
    }
    else
    {
        publish data
         where adrln2 = @adrln3
           and adrln3 = ''
    }
}
|
publish data
 where CUS_NAM = @CUS_NAM
   and adr_id = @adr_id
   and ADRLN1 = @ADRLN1
   and ADRLN2 = @ADRLN2
   and ADRLN3 = @ADRLN3
   and ADRLN4 = @ADRLN4
   and CARRIER = @CARRIER
   and LOD_NUM = @LOD_NUM
   and DEL_NUM = @DEL_NUM
   and SHIP_DATE = @SHIP_DATE
   and SHIP_QTY = @SHIP_QTY
   and PO_NUM = @PO_NUM
   and WRK_REF = @WRK_REF
   and SHIP_ID = @SHIP_ID
   and PCK_LOC = @PCK_LOC
   and PRT_NUM = @PRT_NUM
   and WAVE_NUM = @WAVE_NUM
   and WRK_ZON = @WRK_ZON
   and STOP_ID = @STOP_ID
   and ALT_PART_NUM = @ALT_PART_NUM
   and SHORT_DESC = @SHORT_DESC
   and SUBUCC = @SUBUCC
]]>
</local-syntax>

<documentation>

<remarks>
<![CDATA[
  <p>
    Custom work ref label for hopewell.  A wrkref must be passed in.
  </p>
]]>
</remarks>

</documentation>

</command>