<command>
   <name>get pick release generation queue</name>

   <description>Get Pick Release Generation Queue</description>

   <type>Local Syntax</type>

   <local-syntax>
      <![CDATA[
            if (@ship_id)
                publish data where use_ship_id=@ship_id
            |
            if (@cmbcod)
                publish data where use_cmbcod=@cmbcod
            |
            if (@wrkref)
                publish data where use_wrkref=@wrkref
            |
            if (@schbat_list)
                publish data where schbat_in=" and pckwrk.schbat in ("||@schbat_list||")"
            else
                publish data where schbat_in=" and 1=1"
            |
            if (@lblseq_fld)
                publish data where lblseq_fld=@lblseq_fld
            else
                publish data where lblseq_fld="pckwrk.srcloc"
            |
		exec sql with lock
		where main_table   = "pckwrk "
		and   main_pk      = "pckwrk.wrkref"
		and   sql_select   = "select pckwrk.schbat, pckwrk.srcare, pckwrk.wh_id, pckwrk.cmbcod,
					     pckwrk.wrktyp, pckwrk.ship_id, pckwrk.srcloc,
					     pckmov.arecod pm_arecod, pckmov.stoloc pm_stoloc,
					     pckwrk.dstare, pckwrk.dstloc,
					     pckwrk.pckqty, pckwrk.untcas,
					     pckwrk.wrkref, pckwrk.lodlvl,
					     pckwrk.devcod, pckwrk.prtnum,
					     pckwrk.prt_client_id,
					     NVL(pb.pricod, '?') pricod,
					     NVL(@lblseq_fld:raw, pckwrk.srcloc) lblseq,
					     pckwrk.wkonum, pckwrk.wkorev, pckwrk.client_id, pckwrk.wh_id"
		and sql_from_where = "  from pckwrk
                             left outer join pckbat pb
                                          on pckwrk.schbat = pb.schbat
                             left outer join shipment
                                          on pckwrk.ship_id = shipment.ship_id
                             left outer join stop
                                          on shipment.stop_id = stop.stop_id
                             left outer join car_move
                                          on stop.car_move_id = car_move.car_move_id
                             left outer join trlr
                                          on car_move.trlr_id = trlr.trlr_id
                                        join pckmov
                                          on pckwrk.cmbcod = pckmov.cmbcod
                             left outer join locmst
                                          on pckmov.stoloc = locmst.stoloc
                                         and pckmov.wh_id = locmst.wh_id
                                       where pckwrk.pipflg != 1
                                         and pb.batsts != 'AINP'
                                         -- PR 44517 skip shipment lines with an outstanding replen from a policy defined area
                                         and ((pckwrk.srcare in (select rtstr1
                                                              from poldat_view
                                                             where polcod = 'VAR-PCKST-AREAS'
                                                               and polvar = 'RELEASE-CHECK'
                                                               and polval = 'ARECOD'
                                                               and wh_id  = pckwrk.wh_id)
                                               and not exists (select 'x'
                                                               from rplwrk
                                                              where ship_line_id = pckwrk.ship_line_id))
                                              or
                                             (pckwrk.srcare not in (select rtstr1
                                                              from poldat_view
                                                             where polcod = 'VAR-PCKST-AREAS'
                                                               and polvar = 'RELEASE-CHECK'
                                                               and polval = 'ARECOD'
                                                               and wh_id  = pckwrk.wh_id))
                                             )
                                         and pckwrk.pcksts = @pcksts
                                         and @+pckwrk.wrkref^use_wrkref
                                         and @+pckwrk.ship_id^use_ship_id
                                         and @+pckwrk.cmbcod^use_cmbcod
                                         and @+pckwrk.wh_id
                                         @schbat_in:raw"
                and sql_remainder  =" order by pb.pricod,
                                               pckwrk.schbat,
                                               pckwrk.ship_id,
                                               pckwrk.wh_id,
                                               pckwrk.srcare,
                                               pckwrk.cmbcod,
                                               pckmov.seqnum desc"
            ]]>
   </local-syntax>

   <documentation>
      <remarks>
         <![CDATA[
                    <p>
                          This command is used to return a list of pckwrk entries which
                          require work generation processing for the pick
                          release process.
                          If this command is customized, then it is necessary for the
                          customized command to return at least the set of
                          columns returned
                          by this command.  This command is called by process
                          pick release
                          generation.
                    </p>
                  ]]>
      </remarks>

      <exception value="eOK">Normal successful completion</exception>

      <exception value="eDB_NO_ROWS_AFFECTED">No orders found</exception>

      <seealso cref="process pick release generation">
      </seealso>
   </documentation>
</command>

