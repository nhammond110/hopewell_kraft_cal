<command>

<name>list var rpt vics bill of lading information</name>

<description>List Var RPT VICS Bill Of Lading</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[
[select ord.st_adr_id,
        ord.vc_host_bol_num,
        ord.vc_host_move_id,
	  ord.bt_adr_id,
        ord.vc_host_move_id bolnum,
        ord.vc_host_stop_seq,
        ord.wh_id,

        shipment.ship_id,
	  nvl(shipment.track_num, nvl(stop.track_num, car_move.track_num)) track_num,
        shipment.wh_id,
        shipment.srvlvl,
	  shipment.late_shpdte,
	  shipment.late_dlvdte,
	  shipment.frtchg,
        
        car_move.car_move_id,
	  car_move.track_num,
	  car_move.vc_max_temp || ' ' || d_trailer_typ.lngdsc vc_max_temp,
	  car_move.vc_sched_pkup_dte,
	  

	  d_paytrm.lngdsc vc_paytrm,

        carhdr.carnam,
	  carhdr.accnum,
	  carhdr.scacod,

        trlr.trlr_id,
        trlr.trlr_num,
	  trlr.carcod,
        trlr.trlr_seal1,
        trlr.trlr_seal2,
        trlr.trlr_seal3,
        trlr.trlr_seal4,
        trlr.dispatch_dte,
        trlr.appt_id,

	d_trailer_typ.lngdsc vc_bol_temp,
         
        adrmst.adrnam,
        
	  cstmst.cstnum bt_cstnum,

	  min(ord_note.nottxt) nottxt,
      trlr.vc_paps
   from shipment
   inner join shipment_line on shipment_line.ship_id = shipment.ship_id
   inner join ord on ord.ordnum = shipment_line.ordnum
		and ord.client_id = shipment_line.client_id
		and ord.wh_id = shipment_line.wh_id 
   inner join stop on stop.stop_id = shipment.stop_id
   inner join car_move on car_move.car_move_id = stop.car_move_id
   inner join trlr on trlr.trlr_id = car_move.trlr_id 
   left outer join ord_note on ord_note.client_id = shipment_line.client_id
		and ord_note.ordnum = shipment_line.ordnum
		and ord_note.nottyp = 'TRA'
		and ord_note.wh_id = shipment_line.wh_id
   inner join adrmst on adrmst.adr_id = ord.st_adr_id
   inner join carhdr on carhdr.carcod = car_move.carcod
   inner join cstmst on cstmst.adr_id = ord.bt_adr_id
   left outer join dscmst d_paytrm on d_paytrm.colval = car_move.vc_paytrm 
        and d_paytrm.colnam = 'VC_PAYTRM'
        and d_paytrm.locale_id = nvl(@locale_id, @@locale_id)
   left outer join dscmst d_trailer_typ on d_trailer_typ.colval = trlr.trlr_typ
        and d_trailer_typ.colnam = 'trlr_typ'
        and d_trailer_typ.locale_id = nvl(@locale_id, @@locale_id)

  where @+shipment.ship_id
    and @+shipment.doc_num
    and @+shipment.track_num
    and @+stop.stop_id
    and @+car_move.car_move_id
    and @+trlr.trlr_num
  group by ord.st_adr_id,
        ord.vc_host_bol_num,
        ord.vc_host_move_id,
	  ord.bt_adr_id,
        ord.vc_host_move_id,
        ord.vc_host_stop_seq,
        ord.wh_id,

        shipment.ship_id,
	  nvl(shipment.track_num, nvl(stop.track_num, car_move.track_num)),
        shipment.wh_id,
        shipment.srvlvl,
	  shipment.late_shpdte,
	  shipment.late_dlvdte,
	  shipment.frtchg,
        
        car_move.car_move_id,
	  car_move.track_num,
	  car_move.vc_max_temp || ' ' || d_trailer_typ.lngdsc,
	  car_move.vc_sched_pkup_dte,

	  d_paytrm.lngdsc,

        carhdr.carnam,
	  carhdr.accnum,
	  carhdr.scacod,

        trlr.trlr_id,
        trlr.trlr_num,
	  trlr.carcod,
        trlr.trlr_seal1,
        trlr.trlr_seal2,
        trlr.trlr_seal3,
        trlr.trlr_seal4,
        trlr.dispatch_dte,
        trlr.appt_id,
	
	d_trailer_typ.lngdsc,
         
        adrmst.adrnam,
	  cstmst.cstnum,
      trlr.vc_paps
  order by ord.wh_id,
        car_move.car_move_id,
	  ord.vc_host_move_id || '-' || ord.vc_host_stop_seq asc]
]]>
</local-syntax>

<documentation>

<remarks>
<![CDATA[
    <p>
    This command is used to list bill of lading information.  It is designed
    for use with the Std-VICSBillOfLading report.  This should only publish
    one row but with the recent join to car_move_note it has the potential to
    publish multiple rows since multiple note lines can be associated with
    a carier move. for every carrier move, we will publish only one note and all
    shipments associated.
    </p>
]]>
</remarks>

<retcol name="st_adr_id" type="COMTYP_STRING">Stop Address ID</retcol>
<retcol name="doc_num" type="COMTYP_STRING">Document number</retcol>
<retcol name="track_num" type="COMTYP_STRING">Tracking number</retcol>
<retcol name="ship_id" type="COMTYP_STRING">Shipment ID</retcol>
<retcol name="srvlvl" type="COMTYP_STRING">Service level</retcol>
<retcol name="carcod" type="COMTYP_STRING">Carrier code</retcol>
<retcol name="car_move_id" type="COMTYP_STRING">Carrier move ID</retcol>
<retcol name="carnam" type="COMTYP_STRING">Carrier name</retcol>
<retcol name="trlr_id" type="COMTYP_STRING">Trailer ID</retcol>
<retcol name="trlr_num" type="COMTYP_STRING">Trailer number</retcol>
<retcol name="trlr_seal1" type="COMTYP_STRING">Trailer seal 1</retcol>
<retcol name="trlr_seal2" type="COMTYP_STRING">Trailer seal 2</retcol>
<retcol name="trlr_seal3" type="COMTYP_STRING">Trailer seal 3</retcol>
<retcol name="trlr_seal4" type="COMTYP_STRING">Trailer seal 4</retcol>
<retcol name="client_id" type="COMTYP_STRING">client ID</retcol>
<retcol name="ordnum" type="COMTYP_STRING">Order number</retcol>
<retcol name="cponum" type="COMTYP_STRING">Customer order number</retcol>
<retcol name="bt_adr_id" type="COMTYP_STRING">Bill-to address ID</retcol>
<retcol name="cascnt" type="COMTYP_INT">Case count for purchase order</retcol>
<retcol name="weight" type="COMTYP_FLOAT">Total weight for purchase order</retcol>
<retcol name="wh_id" type="COMTYP_STRING">Warehouse ID</retcol>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">No rows found</exception>

</documentation>

</command>
