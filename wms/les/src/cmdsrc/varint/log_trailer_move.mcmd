<command>

<name>log trailer move</name>

<description>Log Trailer Move</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[
        if ((@oldloc != '') and (@yard_loc != '') and (@oldloc != @yard_loc))
        {
            list trailers
            where trlr_id = @trlr_id
            |
            if (@trlr_cod = 'RCV')
            {
                [select trknum rcv_trknum,
                        wh_id
                   from rcvtrk
                  where trlr_id = @trlr_id] catch (@?)
            }
            else
            {
                [select car_move_id
                   from car_move
                  where trlr_id = @trlr_id] catch (@?)
            }
            |
            write trailer activity 
            where actcod = 'TMOVE' 
              and mod_collst = @updlst
            |
            write daily transaction for trailer activity
            where actcod = 'TMOVE'
              and trlr_num = @trlr_num 
              and carcod = @carcod
              and frstol = @oldloc
              and tostol = @yard_loc
              and wh_id = @stoloc_wh_id
        }
        /* HOPWLL-211 - If a trailer is checked into a yard location rather than a dock door the standard code doesnt log the trailer move */
        else
        {
            /* Verify the yard_loc is in a yard area */
            [SELECT 'x'
               FROM locmst,
                    aremst
              WHERE locmst.stoloc = @yard_loc
                AND locmst.wh_id = NVL(@wh_id,@@wh_id)
                AND locmst.wh_id = aremst.wh_id
                AND locmst.arecod = aremst.arecod
                AND aremst.yrdflg = 1]catch(-1403,510)
            |
            if(@? = 0)
            {
                /* Verify that trailer is in a checked in status */
                [SELECT 'x'
                   FROM trlr
                  WHERE trlr_id = @trlr_id
                    AND trlr_stat = 'CI']catch(-1403,510)
                |
                if(@? = 0)
                {
                    list trailers
                    where trlr_id = @trlr_id
                    |
                    if (@trlr_cod = 'RCV')
                    {
                        [select trknum rcv_trknum,
                                wh_id
                           from rcvtrk
                          where trlr_id = @trlr_id] catch (@?)
                    }
                    else
                    {
                        [select car_move_id
                           from car_move
                          where trlr_id = @trlr_id] catch (@?)
                    }
                    |
                    write trailer activity 
                    where actcod = 'TMOVE' 
                      and mod_collst = @updlst
                    |
                    write daily transaction for trailer activity
                    where actcod = 'TMOVE'
                      and trlr_num = @trlr_num 
                      and carcod = @carcod
                      and frstol = @oldloc
                      and tostol = @yard_loc
                      and wh_id = @stoloc_wh_id
                }
            }
        }
        /* End HOPWLL-211 */
]]>
</local-syntax>

<documentation>
<remarks>
<![CDATA[
  <p>
  This command will log a trailer activity record whenever a trailer is moved to a new yard or dock location in the system. In addition, this command creates a daily transaction record for this trailer activity.
  </p>
]]>
</remarks>

<exception value="eOK">Normal successful completion</exception>

<seealso cref="write trailer activity"></seealso>
<seealso cref="write daily transaction for trailer activity"></seealso>

</documentation>
</command>