<command>
<name>convert var lodnum mh10 label</name>
<description>Converts the 10 digit LODNUM to MH10 Label format.

Remove X if applicable.  Format to 20 digit Load Number.  Concat Kraft Prefix 0010044710.  Use leftmost 9 digits from LODNUM and calculate check digit for 20th position</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[

/* if the lodnum is already 20 digits no need to calcuate it */
if (length(@lodnum) <> 20)
{
    publish data
     where db_lodnum = @lodnum
    |
    /* When loads are shipped, an X is appended to LODNUM.  It needs to be removed on ASN upload */
    [select substr(@db_lodnum, length(@db_lodnum)) lastLodnumChar
       from dual]
    |
    if (@lastLodnumChar = 'X')
    {
        [select substr(@db_lodnum, 1, length(@db_lodnum) -1) lodnum_without_x
           from dual]
        |
        publish data
         where lodnum = @lodnum_without_x
    }
    else
    {
        publish data
         where lodnum = @db_lodnum
    }
    |
    publish data
     where lodnum = @lodnum
    |
    /* Make sure LODNUM is size 10 */
    [select length(@lodnum) as lodnum_length
       from dual]
    |
    if (@lodnum_length < 10)
    {
        [select lpad(@lodnum, 10, '0') as lodnum
           from dual]
    }
    else
    {
        if (@lodnum_length > 10)
        {
            [select substr(@lodnum, @lodnum_length - 9, 10) as lodnum
               from dual]
        }
    }
    |
    publish data
     where lodnum = @lodnum
    |
    /*********************************************************/
    /* Retrieve the SSCC value for the lodnum now.  If there */
    /* is a value already populated, we will use it.  If not */
    /* we will calculate one.                                */
    /*********************************************************/
    [select vc_temp_sscc AS load_sscc
       from invlod
      where lodnum = @db_lodnum] catch(-1403)
    |
    if (@? = 0)
    {
        /************************************************/
        /* Retrieved SSCC value cannot be null or blank */
        /************************************************/
        if (nvl(@load_sscc, '') <> '')
        {
            /*********************************************************/
            /* A valid SSCC value is 20 characters in length so only */
            /* used the retrieved value if it is, otherwise we are   */
            /* going to calculate a SSCC value.                      */
            /*********************************************************/
            if (length(@load_sscc) = 20)
            {
                publish data
                 where lodnum = @load_sscc
                   and calc_sscc = 'N'
            }
            else
            {
                publish data
                 where calc_sscc = 'Y'
            }
        }
        else
        {
            publish data
             where calc_sscc = 'Y'
        }
    }
    else
    {
        publish data
         where calc_sscc = 'Y'
    }
    |
    /******************************************************/
    /* If we need to calculate the SSCC value, do it now. */
    /******************************************************/
    if (@calc_sscc = 'Y')
    {
        /*********************************************************/
        /* We now know only one prt_client_id exists for the LPN */
        /* so go ahead and retrieve it now.                      */
        /*********************************************************/
        [select prt_client_id as load_prt_client_id
           from invlod,
                invsub,
                invdtl
          where invlod.lodnum = invsub.lodnum
            and invsub.subnum = invdtl.subnum
            and invlod.wh_id = nvl(@wh_id, @@wh_id)
            and invlod.lodnum = @lodnum] catch(-1403)
        |
        /*********************************************************/
        /* Retrieve the lodnum prefix based on the prt_client_id */
        /* from the policy.                                      */
        /*********************************************************/
        [select rtstr2 as iln_mapping
           from poldat_view
          where wh_id = nvl(@wh_id, @@wh_id)
            and polcod = 'VAR-CLIENT-ILN'
            and polvar = 'CLIENT-ID'
            and polval = 'VALUE'
            and rtstr1 = @load_prt_client_id
            and rownum < 2] catch(-1403)
        |
        /* concat Kraft control number with first 9 digits of LODNUM */
        [select @iln_mapping || substr(@lodnum, 1, 9) as lodnum
           from dual]
        |
        /* calculate check digit using odd and even number positions */
        [select @lodnum as lodnum,
                substr(@lodnum, 1, 1) as lodnum1,
                substr(@lodnum, 2, 1) as lodnum2,
                substr(@lodnum, 3, 1) as lodnum3,
                substr(@lodnum, 4, 1) as lodnum4,
                substr(@lodnum, 5, 1) as lodnum5,
                substr(@lodnum, 6, 1) as lodnum6,
                substr(@lodnum, 7, 1) as lodnum7,
                substr(@lodnum, 8, 1) as lodnum8,
                substr(@lodnum, 9, 1) as lodnum9,
                substr(@lodnum, 10, 1) as lodnum10,
                substr(@lodnum, 11, 1) as lodnum11,
                substr(@lodnum, 12, 1) as lodnum12,
                substr(@lodnum, 13, 1) as lodnum13,
                substr(@lodnum, 14, 1) as lodnum14,
                substr(@lodnum, 15, 1) as lodnum15,
                substr(@lodnum, 16, 1) as lodnum16,
                substr(@lodnum, 17, 1) as lodnum17,
                substr(@lodnum, 18, 1) as lodnum18,
                substr(@lodnum, 19, 1) as lodnum19
           from dual]
        |
        [select ((@lodnum1 + @lodnum3 + @lodnum5 + @lodnum7 + @lodnum9 + @lodnum11 + @lodnum13 + @lodnum15 + @lodnum17 + @lodnum19) * 3) as lodnumb,
                (@lodnum2 + @lodnum4 + @lodnum6 + @lodnum8 + @lodnum10 + @lodnum12 + @lodnum14 + @lodnum16 + @lodnum18) as lodnumc
           from dual]
        |
        [select (@lodnumb + @lodnumc) as lodnumd
           from dual]
        |
        [select ceil(@lodnumd / 10) * 10 as lodnume,
                @lodnumd as lodnumd,
                @lodnumb as lodnumb,
                @lodnumc as lodnumc,
                @lodnumd as lodnumd
           from dual]
        |
        [select (@lodnume - @lodnumd) as lodnumf
           from dual]
        |
        [select (@lodnum || @lodnumf) as lodnum
           from dual]
    }
}
|
publish data
 where vc_mh10_lodnum = @lodnum
   and vc_mh10_lodnum_display = '(' || substr(@lodnum, 0, 2) || ')' || substr(@lodnum, 3, 1) || ' ' || substr(@lodnum, 4, 7) || ' ' || substr(@lodnum, 11, 9) || ' ' || substr(@lodnum, 20, 1)
   
]]>
</local-syntax>
<argument alias="Load Number" datatype="string" name="LODNUM" required="yes">
</argument>
</command>
