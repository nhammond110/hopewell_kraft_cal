<command>
<name>list var shipping platform assets</name>
<description>list var shipping platform assets</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
/* Verify that a parameter was passed into the function. */
if (!(@ship_id = '' and @stop_id = '' and @trlr_id = '' and @ordnum = ''))
{
    [select actualAssets.ship_id,
            actualAssets.wh_id,
            actualAssets.stop_id,
            decode(actualAssets.vc_pltfrm_override, 0, 'FALSE', 'TRUE') as vc_pltfrm_override,
            actualAssets.vc_platform_asset,
            prtdsc.lngdsc,
            decode(actualAssets.vc_platform_asset, null, '', actualAssets.qty) as qty
       from (select ship_id,
                    wh_id,
                    stop_id,
                    vc_pltfrm_override,
                    vc_platform_asset,
                    sum(qty) as qty,
                    client_id
               from (select distinct shipment.ship_id,
                            shipment.wh_id,
                            shipment.stop_id,
                            shipment.vc_pltfrm_override,
                            invlod.lodnum,
                            vc_invlod_pltfrm.prtnum vc_platform_asset,
                            pltfrm_qty qty,
                            shipment_line.client_id
                       from shipment inner
                       join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                       join invdtl on invdtl.ship_line_id = shipment_line.ship_line_id inner
                       join invsub on invsub.subnum = invdtl.subnum inner
                       join invlod on invlod.lodnum = invsub.lodnum left outer
                       join vc_invlod_pltfrm on vc_invlod_pltfrm.lodnum = invlod.lodnum
					    and vc_invlod_pltfrm.ship_id = shipment.ship_id
                      where shipment.ship_id in (select distinct shipment.ship_id
                                                   from trlr inner
                                                   join car_move on car_move.trlr_id = trlr.trlr_id inner
                                                   join stop on stop.car_move_id = car_move.car_move_id inner
                                                   join shipment on shipment.stop_id = stop.stop_id inner
                                                   join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                                   join ord on ord.client_id = shipment_line.client_id
                                                    and ord.ordnum = shipment_line.ordnum
                                                    and ord.wh_id = shipment_line.wh_id
                                                  where @%trlr.trlr_id
                                                    and @%stop.stop_id
                                                    and @%shipment.ship_id
                                                    and @%ord.ordnum)
                        and shipment.vc_pltfrm_override = 0) actualAssets2
              group by ship_id,
                    wh_id,
                    stop_id,
                    vc_pltfrm_override,
                    vc_platform_asset) actualAssets left outer
       join prtmst_view on prtmst_view.prtnum = actualAssets.vc_platform_asset
        and prtmst_view.wh_id = actualAssets.wh_id
        and prtmst_view.prt_client_id = actualAssets.client_id left outer
       join prtdsc on prtdsc.colnam = 'prtnum|prt_client_id|wh_id_tmpl'
        and prtdsc.colval =
     /*=varchar(*/
     nvl(prtmst_view.prtnum, rtrim(' ')) || '|' || nvl(prtmst_view.prt_client_id, rtrim(' ')) || '|' || nvl(prtmst_view.wh_id_tmpl, rtrim(' '))
     /*=)*/
        and prtdsc.locale_id = nvl(@locale_id, @@locale_id)
     union all
     select shipment.ship_id,
            shipment.wh_id,
            shipment.stop_id,
            decode(shipment.vc_pltfrm_override, 0, 'FALSE', 'TRUE') as vc_pltfrm_override,
            vc_ship_pltfrm_override.prtnum as vc_platform_asset,
            prtdsc.lngdsc,
            to_char(vc_ship_pltfrm_override.override_qty) as qty
       from shipment left outer
       join vc_ship_pltfrm_override on vc_ship_pltfrm_override.ship_id = shipment.ship_id left outer
       join prtmst_view on prtmst_view.prtnum = vc_ship_pltfrm_override.prtnum
        and prtmst_view.wh_id = shipment.wh_id left outer
       join prtdsc on prtdsc.colnam = 'prtnum|prt_client_id|wh_id_tmpl'
        and prtdsc.colval =
     /*=varchar(*/
     nvl(prtmst_view.prtnum, rtrim(' ')) || '|' || nvl(prtmst_view.prt_client_id, rtrim(' ')) || '|' || nvl(prtmst_view.wh_id_tmpl, rtrim(' '))
     /*=)*/
        and prtdsc.locale_id = nvl(@locale_id, @@locale_id)
      where shipment.ship_id in (select distinct shipment.ship_id
                                   from trlr inner
                                   join car_move on car_move.trlr_id = trlr.trlr_id inner
                                   join stop on stop.car_move_id = car_move.car_move_id inner
                                   join shipment on shipment.stop_id = stop.stop_id inner
                                   join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                   join ord on ord.client_id = shipment_line.client_id
                                    and ord.ordnum = shipment_line.ordnum
                                    and ord.wh_id = shipment_line.wh_id
                                  where @%trlr.trlr_id
                                    and @%stop.stop_id
                                    and @%shipment.ship_id
                                    and @%ord.ordnum)
        and prtmst_view.prt_client_id in (select distinct shipment_line.client_id prt_client_id
                                            from trlr inner
                                            join car_move on car_move.trlr_id = trlr.trlr_id inner
                                            join stop on stop.car_move_id = car_move.car_move_id inner
                                            join shipment on shipment.stop_id = stop.stop_id inner
                                            join shipment_line on shipment_line.ship_id = shipment.ship_id inner
                                            join ord on ord.client_id = shipment_line.client_id
                                             and ord.ordnum = shipment_line.ordnum
                                             and ord.wh_id = shipment_line.wh_id
                                           where @%trlr.trlr_id
                                             and @%stop.stop_id
                                             and @%shipment.ship_id
                                             and @%ord.ordnum)
        and shipment.vc_pltfrm_override = 1
      order by wh_id,
            stop_id,
            ship_id,
            vc_platform_asset]
}
]]>
</local-syntax>
</command>
