<command>

<name>list var rpt packing list headers</name>

<description>List Var RPT Packing List Headers</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[
if (@ship_id is null and @car_move_id is not null)
{
    [select sh.ship_id
       FROM car_move cm,
            shipment sh,
            stop st
      where sh.stop_id = st.stop_id
        AND st.car_move_id = cm.car_move_id
        and cm.car_move_id = @car_move_id]
    |
    publish data
     where ship_id = @ship_id
}
|
get client in clause for user
 where usr_id = @@usr_id
   and table_prefix = 'sl'
|
publish data
 where sl_client_in_clause = @client_in_clause
|
get client in clause for user
 where usr_id = @@usr_id
   and table_prefix = 'shipment_line'
|
publish data
 where shipment_line_client_in_clause = @client_in_clause
|
get client in clause for user
 where usr_id = @@usr_id
   and table_prefix = 'ord'
|
publish data
 where ord_client_in_clause = @client_in_clause
|
[select distinct s.ship_id,
        s.late_shpdte,
        sl.wh_id,
        sl.client_id,
        tr.trlr_id,
        tr.trlr_num,
        tr.trlr_seal1,
        tr.dispatch_dte,
        st.stop_id,
        st.stop_nam,
        st.stop_seq,
        mf.shpdte,
        cm.carcod,
        cm.trlr_id,
        tr.trlr_seal1,
        cm.car_move_id
   from shipment s,
        car_move cm,
        stop st,
        shipment_line sl,
        trlr tr,
        manfst mf
  where sl.ship_id = s.ship_id
    and s.stop_id = st.stop_id
    and st.car_move_id = cm.car_move_id
    and s.shpsts != 'B'
    and sl.ordnum = nvl(@ordnum, sl.ordnum)
    and sl.client_id = nvl(@client_id, sl.client_id)
    and s.ship_id = nvl(@ship_id, s.ship_id)
    and st.stop_id = nvl(@stop_id, st.stop_id)
    and cm.car_move_id = nvl(@car_move_id, cm.car_move_id)
    and cm.trlr_id = tr.trlr_id
    and s.ship_id = mf.ship_id(+)
    and @+sl.wh_id
    and @sl_client_in_clause:raw
  order by sl.wh_id,
        sl.client_id]
|
[select distinct ordnum,
        client_id,
        ship_id,
        wh_id
   from shipment_line
  where shipment_line.ordnum = nvl(@ordnum, shipment_line.ordnum)
    and shipment_line.client_id = nvl(@client_id, shipment_line.client_id)
    and shipment_line.ship_id = nvl(@ship_id, shipment_line.ship_id)
    and shipment_line.linsts != 'B'
    and @+shipment_line.wh_id
    and @shipment_line_client_in_clause:raw
  order by wh_id,
        client_id] catch(-1403)
|
if (@? = 0)
{
    [select ord.*
       from ord
      where ord.ordnum = @ordnum
        and ord.client_id = @client_id
        and @ord_client_in_clause:raw
        and ord.wh_id = @wh_id
      order by ord.wh_id,
            ord.client_id]
    |
    list rpt ship to address
     where @+st_adr_id
    |
    [select distinct pm.stoloc stgloc
       from pckwrk pw,
            pckmov pm
      where pw.cmbcod = pm.cmbcod
        and pw.dstare = pm.arecod
        and pw.ship_id = @ship_id
    and rownum = 1] catch(@?)
    |
    [select ord.*,
            ord_note.nottxt,
            @ship_id ship_id,
            @trlr_id trlr_id,
            @trlr_num trlr_num,
            @trlr_seal1 trlr_seal1,
            @dispatch_dte dispatch_dte,
            @stop_id stop_id,
            @stop_nam stop_nam,
            @shpdte shpdte,
            @carcod carcod,
            cstmst.cstnum,
            @adrnam adrnam,
            @adrln1 adrln1,
            @adrln2 adrln2,
            @adrln3 adrln3,
            @adrcty adrcty,
            @adrstc adrstc,
            @adrpsz adrpsz,
            @late_shpdte late_shpdte,
            @stop_seq stop_seq,
            @stgloc stgloc,
            @car_move_id car_move_id
       from ord
       inner join cstmst on cstmst.adr_id = ord.bt_adr_id
       left outer join ord_note on ord_note.ordnum = ord.ordnum
                  and ord_note.wh_id = ord.wh_id
                  and ord_note.nottyp = 'TRA' 
      where ord.ordnum = @ordnum
        and ord.client_id = @client_id
        and @ord_client_in_clause:raw
        and ord.wh_id = @wh_id
      order by ord.wh_id,
            ord.client_id]
}
]]>
</local-syntax>

<documentation>
<remarks>
<![CDATA[
    <p>
    This command is used to display order information for Var-PackingList.
	</p>
]]>
</remarks>

<retcol name="*" type="varies according to field">All fields from the ord table</retcol>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">No data found</exception>

</documentation>

</command>
