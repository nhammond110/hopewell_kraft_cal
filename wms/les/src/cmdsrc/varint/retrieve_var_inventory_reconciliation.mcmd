<command>

<name>retrieve var inventory reconciliation</name>

<description>Retrieve Var Inventory Reconciliation</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[


/*
 * This command is the custom retrieve method for the INVENTORY_RECONCILIATION
 * transaction.  It grabs a certain set of rows from table vc_invrec and those
 * are then sent in the transaction.  The "write var inventory reconciliation"
 * command writes to table vc_invrec.  It is expected that the way this will
 * work is that to log the transaction, the user should call the command
 * "log var inventory reconciliation", which will log from 1 to n of these
 * INVENTORY_RECONCILIATION transactions (typically it will probably be just
 * one but the onus for this is that the EDI can only handle 10,000 lines, so
 * "log var inventory reconciliation" will break it up into groups of approximately
 * 9,000 rows if need be.  So...
 *   - "log var inventory reconciliation" calls "write var inventory reconciliation"
 *      (data is written to table)
 *   - "log var inventory reconciliation" determines how many chunks of 9,000 rows
 *      should be sent to the host
 *   - calls sl_log event where first_row = x and last_row = y for each chunk
 * 
 *
 * Arguments
 * - first_row, last_row - The first and last rows (and everything in between)
 *   this command should select from table vc_invrec (note that the rows are
 *   already numbered by column vc_invrec_row).  That column is padded with
 *   left zeroes, but the user can just pass un-padded numbers to this command
 *   and we'll figure it out here.  These BOTH must be non-zero to choose
 *   a range, otherwise the whole table is selected.
 *
 *
 * HISTORY
 * JJS 01/14/2009 - Changed to support 10,000 line limit.
 *
 */

[select to_number(nvl(@first_row, 0)) first_row,
        to_number(nvl(@last_row,  0)) last_row
   from dual]
|
if (@first_row = 0 or @last_row = 0)
{
   [select * from vc_invrec order by vc_invrec_row]
}
else
{
   [select * from vc_invrec
     where to_number(vc_invrec_row) >= @first_row
       and to_number(vc_invrec_row) <= @last_row
     order by vc_invrec_row]
}


]]>
</local-syntax>

<documentation>

<remarks>
<![CDATA[
  <p>
  This component is copied and modified from the Retrieve Inventory Reconciliation.

  This component will return a list of all of the inventory that is flagged
  as production inventory and exists within the four-walls. The component 
  summarizes the inventory by area, part, prt_client_id, invsts, lotnum, 
  orgcod, revlvl. If a summary field is defined in the 
  HOST-TRANS>>AREA-HOST-LEVEL policy, the hstacc will be set to the value
  defined for this summary field.</p>
]]>
</remarks>

<retcol name="arecod" type="COMTYP_CHAR"></retcol>
<retcol name="hstacc" type="COMTYP_CHAR"></retcol>
<retcol name="prtnum" type="COMTYP_CHAR"></retcol>
<retcol name="prt_client_id" type="COMTYP_CHAR"></retcol>
<retcol name="invsts" type="COMTYP_CHAR"></retcol>
<retcol name="lotnum" type="COMTYP_CHAR"></retcol>
<retcol name="orgcod" type="COMTYP_CHAR"></retcol>
<retcol name="revlvl" type="COMTYP_LONG"></retcol>
<retcol name="untqty" type="COMTYP_LONG"></retcol>
<retcol name="catch_qty" type="COMTYP_FLOAT"></retcol>
<retcol name="catch_unttyp" type="COMTYP_CHAR"></retcol>
<retcol name="wh_id" type="COMTYP_CHAR"></retcol>
<retcol name="vc_wh_name" type="COMTYP_CHAR"></retcol>
<retcol name="vc_itm_cod_dte" type="COMTYP_CHAR"></retcol>

<exception value="eDB_NO_ROWS_AFFECTED">No inventory found matching the criteria.</exception>

</documentation>

</command>
