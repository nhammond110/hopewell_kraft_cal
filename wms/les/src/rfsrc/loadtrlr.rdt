/* Include standard RF definitions */

#include "rf_format.h"
#include "rf_err.h"

/*
 * Following lines where in load_stg_lanes form prior to calling this form
 *                    Set (LOAD_TRLR.dock, wrkque.dstloc)
 *                    Set (LOAD_TRLR.waybil, wrkque.refloc)
 */

/* Text & Message definitions */

#define FORM_HELPTEXT		hlpLoadTrlr,		"scan pallet from lane"

#define MSG_CLR_STG	   	"errClrStgFail",	"errClrStgFail"
#define MSG_COMPL_WRK	   	"errCompleteWork",	"error compeleting work"
#define MSG_INV_LOAD		"errInvalidLodnum",	"invalid load number"
#define MSG_INV_DOCK		"errInvalidDockDoor",	"invalid dock door"
#define MSG_MOV_LOAD		"errMoveLoad",		"error moving Load"
#define MSG_SCAN_LOAD		"stsScanLodnum",	"scan load number ..."
#define MSG_WRITING		"stsWriteLoadShip",	"stsWriteLoadShip"

#define DLG_OK_COMPLETE_STOP    "dlgOkToCompleteStop",  "dlgOkToCompleteStop"

/* Field & Label positioning definitions */
#define TITLE_CAPTION		ttlLoadTrlr,		__FILE__	/*"load trailer" */
#define TITLE_TAG               ttlLoadTrlr
#define TITLE_LEFT              RF_TITLE_LEFT
#define TITLE_TOP               RF_TITLE_TOP
#define TITLE_WIDTH             RF_TITLE_WIDTH
#define TITLE_JUSTIFY           Center

#ifdef RF_FORMAT_40X8

#define LABEL1_CAPTION		lblTo,			"to:"
#define LABEL1_LEFT             1
#define LABEL1_TOP              1
#define LABEL1_WIDTH            4
#define LABEL1_JUSTIFY          Right
#define LABEL2_CAPTION		lblTrailerInfo,		"info:"
#define LABEL2_LEFT             20
#define LABEL2_TOP              1
#define LABEL2_WIDTH            8
#define LABEL2_JUSTIFY          Right
#define LABEL3_CAPTION          lblPickFromLanes,	"pick from lanes:"
#define LABEL3_LEFT             2
#define LABEL3_TOP              2
#define LABEL3_WIDTH            20
#define LABEL3_JUSTIFY          Left
#define LABEL4_CAPTION          lblScanLodnum,		"scan load #:"
#define LABEL4_LEFT             1
#define LABEL4_TOP              5
#define LABEL4_WIDTH            13
#define LABEL4_JUSTIFY          Right
#define LABEL5_CAPTION          lblScanDock,		"scan dock #:"
#define LABEL5_LEFT             1
#define LABEL5_TOP              6
#define LABEL5_WIDTH            13
#define LABEL5_JUSTIFY          Right
#define LABEL6_CAPTION          pal_slot,		"Pal Slot:"
#define LABEL6_TAG              pal_slot
#define LABEL6_LEFT             1
#define LABEL6_TOP              7
#define LABEL6_WIDTH            13
#define LABEL6_JUSTIFY          Right
#define DOCK_LEFT               6
#define DOCK_TOP                1
#define WAYBIL_LEFT             29
#define WAYBIL_TOP              1
#define DOCK2_LEFT              15
#define DOCK2_TOP               6
#define STGLN0_LEFT             2
#define STGLN0_TOP              3
#define STGLN1_LEFT             2
#define STGLN1_TOP              4
#define STGLN2_LEFT             15
#define STGLN2_TOP              3
#define STGLN3_LEFT             15
#define STGLN3_TOP              4
#define LODNUM_LEFT             15
#define LODNUM_TOP              5
#define PAL_SLOT_LEFT           15
#define PAL_SLOT_TOP            7
#define PAL_SLOT_TAG            pal_slot

#define DOCK2_TAG               dock2

#endif

#ifdef RF_FORMAT_20X16

#define LABEL1_CAPTION		lblTo,			"to"
#define LABEL1_LEFT             1
#define LABEL1_TOP              1
#define LABEL1_WIDTH            10
#define LABEL1_JUSTIFY          Left
#define LABEL2_CAPTION		lblTrailerInfo,		"info"
#define LABEL2_LEFT             1
#define LABEL2_TOP              3
#define LABEL2_WIDTH            10
#define LABEL2_JUSTIFY          Left
#define LABEL3_CAPTION          lblPickFromLanes,	"pick from lanes:"
#define LABEL3_LEFT             1
#define LABEL3_TOP              5
#define LABEL3_WIDTH            20
#define LABEL3_JUSTIFY          Left
#define LABEL4_CAPTION          lblScanLodnum,		"load:"
#define LABEL4_LEFT             1
#define LABEL4_TOP              10
#define LABEL4_WIDTH            20
#define LABEL4_JUSTIFY          Left
#define LABEL5_CAPTION          lblScanDock,		"dock:"
#define LABEL5_LEFT             1
#define LABEL5_TOP              12
#define LABEL5_WIDTH            20
#define LABEL5_JUSTIFY          Left
#define LABEL6_CAPTION          pal_slot,		"Pal Slot:"
#define LABEL6_TAG              pal_slot
#define LABEL6_LEFT             1
#define LABEL6_TOP              14
#define LABEL6_WIDTH            12
#define LABEL6_JUSTIFY          Left
#define DOCK_LEFT               2
#define DOCK_TOP                2
#define WAYBIL_LEFT             2
#define WAYBIL_TOP              4
#define DOCK2_LEFT              2
#define DOCK2_TOP               13
#define STGLN0_LEFT             2
#define STGLN0_TOP              6
#define STGLN1_LEFT             2
#define STGLN1_TOP              7
#define STGLN2_LEFT             2
#define STGLN2_TOP              8
#define STGLN3_LEFT             2
#define STGLN3_TOP              9
#define LODNUM_LEFT             2
#define LODNUM_TOP              11
#define PAL_SLOT_LEFT           2
#define PAL_SLOT_TOP            14
#define PAL_SLOT_TAG            pal_slot

#define DOCK2_TAG               dock2

#endif


/* Form definition */

Begin Form LOAD_TRLR
    Left      RF_TERMINAL_LEFT
    Top       RF_TERMINAL_TOP
    Width     RF_TERMINAL_WIDTH
    Height    RF_TERMINAL_HEIGHT
    Caption   RF_FORM_CAPTION
    HelpText  HELPTEXT_FORM
    
    /* Function key definitions */

    Begin Fkey FKEY_BACK
        Caption FKEY_BACK_CAPTION
        Begin Action
            Back ()
        End 
    End

    
    Begin Fkey FKEY_HELP
        Caption FKEY_HELP_CAPTION
        Begin Action 
            DisplayHelp ()
        End     /* End Action */
    End     /* End Fkey HELP */

    /* Label definitions */

    Begin Label TITLE
        Caption TITLE_CAPTION
        Left    TITLE_LEFT
        Top     TITLE_TOP
        Width   TITLE_WIDTH
        Justify TITLE_JUSTIFY
        Height  RF_FIELD_HEIGHT
    End

    Begin Label LABEL1
        Caption LABEL1_CAPTION
        Left    LABEL1_LEFT
        Top     LABEL1_TOP
        Width   LABEL1_WIDTH
        Justify LABEL1_JUSTIFY
        Height  RF_FIELD_HEIGHT
    End
    
    Begin Label LABEL2
        Caption LABEL2_CAPTION
        Left    LABEL2_LEFT
        Top     LABEL2_TOP
        Width   LABEL2_WIDTH
        Justify LABEL2_JUSTIFY
        Height  RF_FIELD_HEIGHT
    End
    
    Begin Label LABEL3
        Caption LABEL3_CAPTION
        Left    LABEL3_LEFT
        Top     LABEL3_TOP
        Width   LABEL3_WIDTH
        Justify LABEL3_JUSTIFY
        Height  RF_FIELD_HEIGHT
    End

    Begin Label LABEL4
        Caption LABEL4_CAPTION
        Left    LABEL4_LEFT
        Top     LABEL4_TOP
        Width   LABEL4_WIDTH
        Justify LABEL4_JUSTIFY
        Height  RF_FIELD_HEIGHT
    End

    Begin Label LABEL5
        Caption LABEL5_CAPTION
        Left    LABEL5_LEFT
        Top     LABEL5_TOP
        Width   LABEL5_WIDTH
        Justify LABEL5_JUSTIFY
        Height  RF_FIELD_HEIGHT
    End

    Begin Label LABEL6
        Caption LABEL6_CAPTION
        Left    LABEL6_LEFT
        Top     LABEL6_TOP
        Width   LABEL6_WIDTH
        Justify LABEL6_JUSTIFY
        Height  RF_FIELD_HEIGHT
    End
    
    /* Local field definitions */

    Begin Field inv_loc
#	include "local_string.h"
        Width     STOLOC_LEN
    End

    Begin Field load_info
#	include "local_string.h"
        Width     WAYBIL_LEN
    End

    Begin Field srcloc
#	include "local_string.h"
        Width     STOLOC_LEN
    End
    
    Begin Field dstloc
#	include "local_string.h"
        Width     STOLOC_LEN
    End

    Begin Field stop_id
#        include "local_string.h"
        Width    STOP_ID_LEN
    End
    
    Begin Field dummy
#	include "local_string.h"
        Width          100
    End       /* End Field */

    Begin Field stoloc
#        include "local_string.h"
        Width     STOLOC_LEN
    End

    Begin Field trlr_id
#       include "local_string.h"
        Width   TRLR_ID_LEN
    End

    Begin Field pal_slot_flg
#       include "local_integer.h"
        Width   RF_FLAG_LEN
    End

    Begin Field pallet_sys
#       include "local_string.h"
        Width  9 
    End
    
    /* flg of whether pal_slot_flg has been set */
    Begin Field psf_set_flg
#       include "local_integer.h"
        Width 4
    End
/*
** Translated storage location: input could be location verification code. The
** translated storage location will be used later instead of the value entered
** in the location field.
*/

    Begin Field dock2_translated
#       include "local_string.h"
        Width    STOLOC_LEN
    End

    Begin Field dock2_inpval
#       include "local_string.h"
        Width    100
    End

    /* Field definitions */
    
    Begin Field waybil
        Left        WAYBIL_LEFT
        Top         WAYBIL_TOP
        Width       WAYBIL_LEN
        Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_PROTECTED
    End
    
    Begin Field dock 
        Left        DOCK_LEFT
        Top         DOCK_TOP
        Width       14
	Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_PROTECTED
    End
    
    Begin Field stgln0
        Left        STGLN0_LEFT
        Top         STGLN0_TOP
        Width       STOLOC_LEN
        Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_PROTECTED
    End
    
    Begin Field stgln1
        Left        STGLN1_LEFT
        Top         STGLN1_TOP
        Width       STOLOC_LEN
        Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_PROTECTED
    End
    
    Begin Field stgln2
        Left        STGLN2_LEFT
        Top         STGLN2_TOP
        Width       STOLOC_LEN
        Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_PROTECTED
    End
    
    Begin Field stgln3
        Left        STGLN3_LEFT
        Top         STGLN3_TOP
        Width       STOLOC_LEN
        Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_PROTECTED
    End

    Begin Field lodnum
        Left        LODNUM_LEFT
        Top         LODNUM_TOP
        Width       LODNUM_LEN
        Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask 0
        DefaultData 0
        EntryMask   ENTRY_MASK_INPUT_REQUIRED
        
	Begin Action ExitAction
	    ExecuteDSQL ("get translated inventory identifier where id = '@lodnum' and wh_id = '@global.wh_id' into :dummy, :dummy, :dummy, :dummy, :lodnum")
	    
	    GetResults ()
            Begin Result !eOK
                Beep (3)
                GetResponse (MSG_INV_LOAD)
                Reenter ()
            End

            Length(lodnum)
            Begin Result 0
                Beep (3)
                GetResponse (MSG_INV_LOAD)
                Reenter ()
            End

            /* We removed the where condition that st.stop_id = '@stop_od',
             * Because since we now use brk_stop_seq_cod to load inventories.
             * So some time we need to load inventories that are not belong 
             * one stop. See the design documents of PR 47071.
             */

            ExecuteDSQL ("[select il.stoloc, sh.ship_id into :srcloc, :dstloc from shipment sh, shipment_line sl, stop st, invdtl id, invsub sb, invlod il, aremst ar, locmst lm where lm.arecod = ar.arecod and lm.wh_id = ar.wh_id and ar.stgflg = 1 and lm.stoloc = il.stoloc and lm.wh_id = il.wh_id and il.lodnum = '@lodnum' and sl.ship_id = sh.ship_id and sh.stop_id = st.stop_id and sl.ship_line_id = id.ship_line_id and id.subnum = sb.subnum and sb.lodnum = il.lodnum]")
            GetResults()
            Begin Result !eOK
                Beep(3)
                GetResponse(MSG_INV_LOAD)
                Reenter()
            End

            /*
             * run any shipping validations against this load.  Basically we
             * want to make sure we're allowed to put this on the truck.
             */

            ExecuteDSQL ("check inventory for trailer load where invtid='@lodnum' and oprcod='LOAD' and wh_id = '@global.wh_id' and stop_id = '@stop_id'");

            /*
             * If we found some non-FBFRONT inventory have been loaded when
             * user try to load a FBFRONT inventory, we will give user a 
             * warning message to allow user chose to continue or not.
             * Please see PR 50268.
             */

            Begin Result eFOUND_SOME_INVENTORY_SHOULD_NOT_BE_LOAD_FIRST
                Beep(3)
                promptYN (DLG_LOAD_FBFRONT_AFTER)
                Begin Result 0
                    Reenter()
                End
            End

            Begin Result !eOK
                Beep(3)
                DisplayMessageText()
                Reenter()
            End

           /* We are checking if Pallet control is installed,
            * if so, we need to display the pallet position sent
            * by pallet control on the pal_slot field.We dont 
            * want to user to enter the value there as pallet
            * control has already planned a position for it.
            */
            ExecuteDSQL ("validate rf prompt for pallet position into :pallet_sys where stop_id = '@stop_id' and wh_id = '@global.wh_id'")            
            Begin Result eOK
                GetResults() 

                Length(pallet_sys)
                Begin Result 0
                   SetFieldAttr ("pal_slot", ENTRY_MASK_INVISIBLE)
                End

                Begin Result 1
                   Compare(pallet_sys,POLCOD_PALLETCTL)
                   Begin Result 0
                       ExecuteDSQL("get pallet position where lodnum = '@lodnum'")
                       Begin Result eOK   
                           GetResults ()
                           GetValue("pal_slot","pallet_pos")
                           DisplayField ("pal_slot")
                           SetFieldAttr ("pal_slot",ENTRY_MASK_PROTECTED_AND_NO_DEFAULT_CHAR) 
                           Refresh()
                       End  
                   End 
                End


            End 
	End
    End

    Begin Field dock2 
        Tag         DOCK2_TAG
        Left        DOCK2_LEFT
        Top         DOCK2_TOP
        Width       STOLOC_LEN
        Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask 0
        DefaultData 0
        EntryMask   ENTRY_MASK_INPUT_REQUIRED

        Begin Action ExitAction            
            Set(dock2_translated, dock2)
            Length(dock2)
            Begin Result 1
                /* Get actual dock2 (could be verification code) */
                ExecuteDSQL ("get translated storage location into :dock2_inpval, :dock2_translated where inpval = '@dock2' and matloc = '@dock' and wh_id = '@global.wh_id' ")
                Begin Result !eOK
                    /* Not a valid location */
                    GetResponse(MSG_INV_LOC)
                    Reenter()
                End
                GetResults()
            End

            Verify(dock, dock2_translated)
            Begin Result 0  /* Different Dock Doors */
                Beep(3)
                GetResponse(MSG_INV_DOCK)
                Reenter()
            End
            
            /* 
             * if validate rf prompt for pallet position returns 
             * true, ask the user
             *   "Will pallet positions for this trailer be captured?"
             *       If yes, then set  the pal_slot_flg on trlr table to 1
             *       If no, then set  the pal_slot_flg on trlr table to 0
             * Else if the pal_slot_flg on the  on the trailer table is 0 
             *      make sure the pallet slot field is hidden.
             * Else if the pal_slot_flg on the trailer table is 1, 
             *      make sure the pallet slot field is enabled and 
             *      the user is required to enter a value.
             */
   
            ExecuteDSQL ("validate rf prompt for pallet position into :pallet_sys where stop_id = '@stop_id' and wh_id = '@global.wh_id'")            
            Begin Result eOK
                GetResults() 

                Length(pallet_sys)
                Begin Result 0
                    SetFieldAttr ("pal_slot", ENTRY_MASK_INVISIBLE)
                End

                Length(pallet_sys) 
                Begin Result 1
                    /* check whether pal_slot_flg is set*/
                    ExecuteDSQL("[select 'x' from trlr where pal_slot_flg is NOT NULL and yard_loc = '@dock2_translated' and trlr_stat != 'D' and trlr_cod = 'SHIP' and yard_loc_wh_id = '@global.wh_id']")
                    Begin Result eOK
                        Set (psf_set_flg, 1)
                    End
                    /* Assume it is No Row Affected */
                    Begin Result !eOK
                        Set (psf_set_flg, 0)
                    End
                    
                    ExecuteDSQL("[select pal_slot_flg, trlr_id into :pal_slot_flg, :trlr_id from trlr where yard_loc = '@dock2_translated' and trlr_stat != 'D' and trlr_cod = 'SHIP' and yard_loc_wh_id = '@global.wh_id']")
                    Begin Result eOK
                        GetResults () 
                        Compare(pallet_sys, "RDTALLOPR") 
                        Begin Result 0 
                            Verify (psf_set_flg, 1)
                            Begin Result 0
                                /* psf_set_flg is 0, 
                                 * that means it is the first time 
                                 */
                                /* Prompt user if he want to capture 
                                 * the slot 
                                 */
                                PromptYN(DLG_OK_CAPTURE_SLOT)
                                Begin Result 1              /* it's OK */
                                    /* Change the pal_slot_flg */
                                    ExecuteDSQL ("change trailer where pal_slot_flg = 1 and trlr_id = '@trlr_id'")
                                    SetFieldAttr ("pal_slot", ENTRY_MASK_INPUT_REQUIRED)
                                    Set(pal_slot_flg,1)
                                End
                                Begin Result 0
                                    /* Change the pal_slot_flg */
                                    ExecuteDSQL ("change trailer where pal_slot_flg = 0 and trlr_id = '@trlr_id'")
                                    SetFieldAttr ("pal_slot", ENTRY_MASK_INVISIBLE)
                                    Set(pal_slot_flg,0)                            
                                End
                            End
                            /* pal_slot_flg is set to 1 or 0*/
                            Begin Result 1
                                Compare(pal_slot_flg, 0) 
                                /* Compare pal_slot_flg to 0 */
                                Begin Result 0 
                                    /* If it is 0, then disable the field */
                                    SetFieldAttr ("pal_slot", ENTRY_MASK_INVISIBLE)
                                End
                                Begin Result 1 
                                    /* If it is 1, then set the field 
                                     * to be required 
                                     */
                                    SetFieldAttr ("pal_slot", ENTRY_MASK_INPUT_REQUIRED)                    
                                End
                            End
                        End
                        Compare(pallet_sys, POLCOD_PALLETCTL)
                        Begin Result 0
                            /* Change the pal_slot_flg */
                            ExecuteDSQL ("change trailer where pal_slot_flg = 0 and trlr_id = '@trlr_id'")
                            SetFieldAttr ("pal_slot", ENTRY_MASK_INVISIBLE)
                            Set(pal_slot_flg,0)
                        End 
                    End
                End
            End
        End
    End 

    Begin Field pal_slot 
        Tag         PAL_SLOT_TAG
        Left        PAL_SLOT_LEFT
        Top         PAL_SLOT_TOP
        Width       PAL_SLOT_LEN
        Height      RF_FIELD_HEIGHT
        DataType    String
        TabOrder    1
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_INVISIBLE
        
        Begin Action EntryAction
            /* Get the Next number of slot */
            ExecuteDSQL ("get next trailer pallet slot where trlr_id = '@trlr_id' into :pal_slot")
            GetResults ()
            DisplayField ("pal_slot")                            
        End

        Begin Action ExitAction          
            Verify(pal_slot_flg,1)
            Begin Result 1
                Length(pal_slot)
                Begin Result 0
                    Reenter()
                End
            End
        End
    End     

    /* Form action definitions */
    
    Begin Action EntryAction
        /*
         * Do not clearForm here, because fields like stop_id get set from 
         * caller. 
         */

        /*
         * Clear the staging lane fields, because we'll reload them with
         *whatever staging lanes have inventory left to load.
         */

        ClearField("stgln0")
        ClearField("stgln1")
        ClearField("stgln2")
        ClearField("stgln3")
        ClearField("stoloc")

        /*
         *Clear the slot, because user should have to scan it every
         *time.
         */
        ClearField("pal_slot")

        /*
         *Clear the dock door, because user should have to scan it every
         *time.
         */

        ClearField("dock2")
        Set (dock2_translated, dock2)
        ResetMessageLine()

        ExecuteDSQL ("check inventory exists for trailer load where stop_id = '@stop_id' and wh_id = '@global.wh_id' into :stoloc")

        /* Load staging lane information */

        Begin Result eOK 
            GetResults ()
            Begin Result eOK
                Set (stgln0, stoloc)
                GetResults()
                Begin Result eOK
                    Set (stgln1, stoloc)
                    GetResults()
                    Begin Result eOK
                        Set (stgln2, stoloc)
                        GetResults()
                        Begin Result eOK
                            Set (stgln3, stoloc)
                        End
                    End
                End
            End
        End
        
        /*
         * Here we changed something that since we will nor load inventory
         * in the order of stop_seq, we can not use old way to tell if there
         * are some inventory left or not. So we can not use Length (stgln0)
         * to do that, instead we just come here and check stop for every 
         * inventory, See the design documents of PR 47071.
         */

	ExecuteDSQL("[select stop_cmpl_flg from stop where stop_id = '@stop_id' and stop_cmpl_flg = 1]")
	Begin Result eOK
	    Back()
	End
	
	/*
	 * There is no more product in a staging lane to load 
	 * See if the stop is fully loaded, if so, we'll try to complete.
	 * Now, whether we complete the stop is based on the policy.
	 * 0 - do not allow to complete
	 * 1 - prompt to complete
	 * 2 - automatically complete
	 */

	ExecuteDSQL("check stop loaded where stop_id = '@stop_id'")

        Begin Result eINT_ADD_INV_EXISTS_NOT_READY_TO_LOAD
            Beep(3)
            GetResponse(ERR_ADD_INV_EXISTS_NOT_READY_TO_LOAD)
            ExecuteForm("UNDIR_LOAD_TRAILER")
        End
           
        /* If the stop has been loaded then complete it. */  
	Begin Result eOK
            Compare(INIT_POLICIES.autocompletestop, 1)
            Begin Result 0     /* rtnum1 value is 1, prompt user */
                /*Ask if we should complete sto> */
            
                PromptYN(DLG_OK_COMPLETE_STOP)
                Begin Result 0              /* No, it's not OK */
                    Back()
                
                End
                Begin Result 1
                    Set (COMPLETE_STOP.stop_id, stop_id)
                    ExecuteForm("COMPLETE_STOP")
                End
            End  /* Compare Result 0 */

            Begin Result 1    /* rtnum1 value is 2, auto-complete */
                Set (COMPLETE_STOP.stop_id, stop_id)
                ExecuteForm("COMPLETE_STOP")
            End

            Begin Result -1   /*rtnum1 value is 0, do not complete */
                Back()
            End
        End

        Set (lodnum, "")

        /*
         * The following calls a command to get the information to fill
         * in the waybil field.  The purpose of this command is to provide
         * a hook point to allow custom code to put whatever they want on
         * this field in the screen.
         */

        ExecuteDSQL("list RF trailer load information where stop_id = '@stop_id'")
        GetResults()
        Begin Result eOK
            GetValue("load_info", "trailer_info")
            Length(load_info)
            Begin Result 1
            Set(waybil, load_info)
            End 
        End
    End /* End of Action EntryAction */

    Begin Action ExitAction
	    
        DisplayMessage (RF_MSG_PROCESSING)

        /* Here something we changed...
         * Because now we will not load inventory just in the order of
         * stop_seq, we must get current stop_id by lodnum.
         * See the design documents of Break Stop Sequence PR 47071.
         */
        ExecuteDSQL ("[select distinct stop.stop_id into :stop_id from stop, shipment, shipment_line, invdtl, invsub, invlod  where stop.stop_id = shipment.stop_id  and shipment.ship_id = shipment_line.ship_id and shipment_line.ship_line_id  = invdtl.ship_line_id  and invdtl.subnum = invsub.subnum  and invsub.lodnum = invlod.lodnum and invlod.lodnum = '@lodnum']")
        GetResults()
        Begin Result !eOK
            Beep(3)
            DisplayMessageText()
            Reenter()
        End
        Beep(2)


        /* Verify that the load has a unique carrier move */
        ExecuteDSQL ("validate rf load trailer into :inv_loc where lodnum='@lodnum'")
        Begin Result !eOK
            Beep(3)
            /* Could Not Validate Load Number */
            DisplayMessageText()
            Reenter()
        End
        GetResults()
        DisplayField ("inv_loc")

        ExecuteDSQL ("validate rf required service for load where invtid = '@lodnum' and wh_id = '@global.wh_id'")
        Begin Result eOK
            Beep(3)
            GetResponse (ERR_SERVICE_REQ)
            Reenter()
        End

        ExecuteDSQL ("load trailer where srcloc = '@srcloc' and lodnum = '@lodnum' and dstloc = '@inv_loc' and pal_slot = '@pal_slot' and wh_id = '@global.wh_id' ")
        Begin Result !eOK
            Beep(3)
            DisplayMessageText()
            GetResponse(MSG_MOV_LOAD)
            Reenter()
        End
        Beep(2)
        
        ExecuteDSQL ("validate rf processing area for location where stoloc = '@srcloc' and wh_id = '@global.wh_id'")
        Begin Result eOK
                
            Verify (INIT_POLICIES.exitpntmovfrmprcare, RF_FLAG_TRUE)
            Begin Result 1
                Set (GET_SERVICES.invtid, lodnum)
                Set (GET_SERVICES.stoloc, srcloc)
                Set (GET_SERVICES.exitpnt_typ, EXITPNT_TYP_SERVICE_OUB)
                Set (GET_SERVICES.exitpnt, EXITPNT_MOVEFRMPRCARE)
                
                ShowForm("GET_SERVICES")
            End
            
            Verify (INIT_POLICIES.exitpntmvlstfrmprcord, RF_FLAG_TRUE)
            Begin Result 1
                Set (GET_SERVICES.invtid, lodnum)
                Set (GET_SERVICES.stoloc, srcloc)
                Set (GET_SERVICES.exitpnt_typ, EXITPNT_TYP_SERVICE_OUB)
                Set (GET_SERVICES.exitpnt, EXITPNT_MVLSTFRMPRCORD)
                
                ShowForm("GET_SERVICES")
            End
        End
            
        Verify (INIT_POLICIES.exitpntloadtrlr, RF_FLAG_TRUE)
        Begin Result 1
            Set (GET_SERVICES.invtid, lodnum)
            Set (GET_SERVICES.stoloc, inv_loc)
            Set (GET_SERVICES.exitpnt_typ, EXITPNT_TYP_SERVICE_OUB)
            Set (GET_SERVICES.exitpnt, EXITPNT_LOADTRLR)
            
            ShowForm("GET_SERVICES")
        End 

        /* Get the platform asset for loading this pallet */
        Set(VAR_PLATFORM_ASSET.lodnum, lodnum)
        ShowForm("VAR_PLATFORM_ASSET")

        ExecuteForm ("LOAD_TRLR")

    End /* End of Action EntryAction */

End /* End of Form LOAD_TRLR */

