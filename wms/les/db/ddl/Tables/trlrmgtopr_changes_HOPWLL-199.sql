#use $LESDIR/include
#include <varddl.h>
#include <varcolwid.h>
#include <vartbldef.h>
#include <sqlDataTypes.h>

mset command on

[ALTER_TABLE_TABLE_INFO(trlr)
ALTER_TABLE_ADD_COLUMN_START(vc_cube)
    STRING_TY(20)
ALTER_TABLE_ADD_COLUMN_END] catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

[ALTER_TABLE_TABLE_INFO(trlr)
ALTER_TABLE_ADD_COLUMN_START(vc_num_axle)
    NUMBER(1)
ALTER_TABLE_ADD_COLUMN_END] catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

mset command off

#include <../Docs/trlrmgtopr_changes_HOPWLL-199.tdoc>