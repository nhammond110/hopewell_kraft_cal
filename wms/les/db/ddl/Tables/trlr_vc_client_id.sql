#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#include <sqlDataTypes.h>

mset command on

[ALTER_TABLE_ADD_COLUMN_BEGIN(trlr, vc_client_id)
    STRING_TY(20)
ALTER_TABLE_ADD_COLUMN_END]
catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

mset command off