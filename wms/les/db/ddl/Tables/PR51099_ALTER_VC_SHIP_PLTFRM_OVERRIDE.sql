#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#include <vartbldef.h>
#include <varcolwid.h>

mset command on
[
ALTER_TABLE_TABLE_INFO(vc_ship_pltfrm_override)
ALTER_TABLE_ADD_COLUMN_START (adddte)
	DATE_TY Default SYSDATE_VAL
ALTER_TABLE_ADD_COLUMN_END
] catch (ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL
mset command off