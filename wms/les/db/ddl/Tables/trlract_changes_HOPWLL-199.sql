#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
# $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#use $LESDIR/db/ddl/Docs
#include <vartbldef.h>
#include <varcolwid.h>

mset command on

[ALTER_TABLE_ADD_COLUMN_BEGIN(trlract, vc_trailer_num)
    STRING_TY(20)
ALTER_TABLE_ADD_COLUMN_END] 
catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

mset command off

#include <../Docs/trlract_changes_HOPWLL-199.tdoc>