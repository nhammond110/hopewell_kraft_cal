#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
# $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#use $LESDIR/db/ddl/Docs
#include <vartbldef.h>
#include <varcolwid.h>

#define TRLR_TABLE trlr

mset command on


[ALTER_TABLE_ADD_COLUMN_BEGIN(TRLR_TABLE, vc_pndloc)
    STRING_TY(STOLOC_LEN)
ALTER_TABLE_ADD_COLUMN_END] 
catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL


mset command off

#include <../Docs/trlr_vc_pndloc_HOPWLL-199_tmove.tdoc>