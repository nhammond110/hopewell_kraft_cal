#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#include <sqlDataTypes.h>
#include <varcolwid.h>

#define TABLE_NAME wkodtl

mset command on
[ALTER_TABLE_ADD_COLUMN_BEGIN(TABLE_NAME, uc_rpt_scp_reacod)
	STRING_TY(REACOD_LEN)
ALTER_TABLE_ADD_COLUMN_END]
catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL
mset command off
