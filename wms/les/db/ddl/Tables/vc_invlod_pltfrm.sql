#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#include <vartbldef.h>
#include <varcolwid.h>


mset command on
[
CREATE_TABLE(vc_invlod_pltfrm)
(
    lodnum		STRING_TY(LODNUM_LEN),  /* PK */
    prtnum		STRING_TY(PRTNUM_LEN),   /* PK */
    pltfrm_qty	INTEGER_TY, 
    adddte		DATE_TY Default SYSDATE_VAL
) ] catch (ERR_TABLE_ALREADY_EXISTS)
RUN_DDL
#use $LESDIR/db/ddl/Indexes
#include <vc_invlod_pltfrm_pk.idx>
mset command off