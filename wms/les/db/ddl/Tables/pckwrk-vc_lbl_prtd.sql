#use $MOCADIR/include
#include <sqlDataTypes.h>
alter table pckwrk  add vc_lbl_prtd  number(1) default 0
                    constraint pckwrk_vc_lbl_prtd_ck check (vc_lbl_prtd in (1, 0))
RUN_SQL
