#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#use $LESDIR/db/ddl/Docs
#include <vartbldef.h>
#include <varcolwid.h>

mset command on

[ALTER_TABLE_TABLE_INFO(trlr)
ALTER_TABLE_ADD_COLUMN_START(vc_trailer_num)
    STRING_TY(20)
ALTER_TABLE_ADD_COLUMN_END] catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

[ALTER_TABLE_TABLE_INFO(trlr)
ALTER_TABLE_ADD_COLUMN_START(vc_license_plate)
    STRING_TY(20)
ALTER_TABLE_ADD_COLUMN_END] catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

mset command off

#include <../Docs/trlr_changes_HOPWLL-199.tdoc>