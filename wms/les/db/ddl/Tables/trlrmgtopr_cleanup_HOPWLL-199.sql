#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#use $LESDIR/db/ddl/Docs
#include <vartbldef.h>
#include <varcolwid.h>

mset command on

[delete 
   from les_cust_fld 
  where var_nam = ''] catch(510, -1403)
RUN_DDL

[ALTER_TABLE_DROP_COLUMN_BEGIN(trlr, uc_scacod) 
ALTER_TABLE_DROP_COLUMN_END] catch(ERR_CANNOT_DROP_COLUMN_NOT_FOUND)
RUN_DDL

mset command off