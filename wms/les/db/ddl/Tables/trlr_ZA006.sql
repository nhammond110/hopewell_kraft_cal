#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
# $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#use $LESDIR/db/ddl/Docs
#include <vartbldef.h>
#include <varcolwid.h>

#define TRLR_TABLE trlr

#define UC_PREVNOT_LEN        		80

mset command on

[ALTER_TABLE_ADD_COLUMN_BEGIN(TRLR_TABLE, UC_PREVNOT)
    STRING_TY(UC_PREVNOT_LEN)
ALTER_TABLE_ADD_COLUMN_END] 
catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

mset command off

#include <../Docs/trlr_ZA006.tdoc>