#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#include <vartbldef.h>
#include <varcolwid.h>


mset command on
[
CREATE_TABLE(vc_ship_pltfrm_override)
(
    ship_id		STRING_TY(SHIP_ID_LEN),  /* PK */
    prtnum		STRING_TY(PRTNUM_LEN),   /* PK */
    override_qty	INTEGER_TY 
) ] catch (ERR_TABLE_ALREADY_EXISTS)
RUN_DDL
#use $LESDIR/db/ddl/Indexes
#include <vc_ship_pltfrm_override_pk.idx>
mset command off