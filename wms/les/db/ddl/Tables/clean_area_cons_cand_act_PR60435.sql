#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#include <vartbldef.h>
#include <varcolwid.h>

mset command on

[delete dda_field where dda_id = 'AREA-CONS-CAND-ACT' and var_nam = 'dstare'] catch(@?);
[delete dda_field where dda_id = 'AREA-CONS-CAND-ACT' and var_nam = 'prtnum'] catch(@?);
[delete dda_field where dda_id = 'AREA-CONS-CAND-ACT' and var_nam = 'invsts'] catch(@?);
[delete dda_field where dda_id = 'AREA-CONS-CAND-ACT' and var_nam = 'prt_client_id'] catch(@?);
[delete dda_field where dda_id = 'AREA-CONS-CAND-ACT' and var_nam = 'srcqty'] catch(@?);
[delete dda_field where dda_id = 'AREA-CONS-CAND-ACT' and var_nam = 'srcloc'] catch(@?);
[delete dda_field where dda_id = 'AREA-CONS-CAND-ACT' and var_nam = 'wh_id'] catch(@?);
[delete dda_field where dda_id = 'AREA-CONS-CAND-ACT' and var_nam = 'status'] catch(@?);

RUN_DDL
mset command off