#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#include <vartbldef.h>
#include <varcolwid.h>

mset command on

[delete grd_view_detail where frm_id = 'VAR-SCHBAT-STS'] catch(@?);

[delete grd_view_def where frm_id = 'VAR-SCHBAT-STS'] catch(@?);

RUN_DDL
mset command off