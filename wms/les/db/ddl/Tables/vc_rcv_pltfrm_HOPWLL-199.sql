#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#include <vartbldef.h>
#include <varcolwid.h>

mset command on
[
CREATE_TABLE(vc_rcv_pltfrm)
(
    trlr_id      STRING_TY(TRLR_ID_LEN),
    trknum       STRING_TY(20),
    invnum       STRING_TY(35),
    client_id    STRING_TY(32),
    wh_id        STRING_TY(32),
    prtnum       STRING_TY(PRTNUM_LEN),
    platform_qty INTEGER_TY,
    moddte       DATE_TY Default SYSDATE_VAL,
    usr_id       STRING_TY(USR_ID_LEN)
) ] catch (ERR_TABLE_ALREADY_EXISTS)
RUN_DDL

mset command off