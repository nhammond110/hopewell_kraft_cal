#use $LESDIR/include
#include <varddl.h>
#include <varcolwid.h>
#include <vartbldef.h>

#include <sqlDataTypes.h>
/* add new columns to invdtl*/
ALTER_TABLE_TABLE_INFO(invdtl)
ALTER_TABLE_ADD_COLUMN_START (vc_var_wgt)
	FLOAT_TY
ALTER_TABLE_ADD_COLUMN_END
RUN_DDL