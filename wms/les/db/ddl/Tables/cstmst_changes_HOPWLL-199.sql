#use $LESDIR/include
#include <varddl.h>
#include <varcolwid.h>
#include <vartbldef.h>
#include <sqlDataTypes.h>

mset command on

[ALTER_TABLE_TABLE_INFO(cstmst)
ALTER_TABLE_ADD_COLUMN_START(vc_par_pal_est)
    STRING_TY(4)
ALTER_TABLE_ADD_COLUMN_END] catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

mset command off

#include <../Docs/cstmst_changes_HOPWLL-199.tdoc>