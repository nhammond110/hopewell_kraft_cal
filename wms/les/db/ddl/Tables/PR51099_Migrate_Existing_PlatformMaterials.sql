#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#include <vartbldef.h>
#include <varcolwid.h>

mset command on

[select lodnum,
	vc_platform_asset
from invlod 
where vc_platform_asset is not null]
|
if (@? = 0)
{
	[insert into vc_invlod_pltfrm(lodnum,prtnum,pltfrm_qty) values (@lodnum,@vc_platform_asset,1)] catch(@?)
}

RUN_DDL
mset command off