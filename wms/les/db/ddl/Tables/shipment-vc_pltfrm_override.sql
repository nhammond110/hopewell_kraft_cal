#use $MOCADIR/include
#include <sqlDataTypes.h>

mset command on
[
ALTER_TABLE_TABLE_INFO(shipment)
ALTER_TABLE_ADD_COLUMN_START (vc_pltfrm_override)
	FLAG_TY Default 0
ALTER_TABLE_ADD_COLUMN_END
] catch (ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL
mset command off