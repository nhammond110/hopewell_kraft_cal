#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
# $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#use $LESDIR/db/ddl/Docs
#include <vartbldef.h>
#include <varcolwid.h>

#define TRLRACT_TABLE trlract

#define UC_NOTTXT_LEN        		300

mset command on

[ALTER_TABLE_MODIFY_COLUMN_BEGIN(TRLRACT_TABLE, UC_NOTTXT)
    STRING_TY(UC_NOTTXT_LEN)
ALTER_TABLE_MODIFY_COLUMN_END] 
catch(ERR_COLUMN_ALREADY_EXISTS)
RUN_DDL

mset command off
