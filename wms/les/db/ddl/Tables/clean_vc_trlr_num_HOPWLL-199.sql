#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>
#use $MOCADIR/include
#include <sqlDataTypes.h>
#use $LESDIR/include
#use $LESDIR/db/ddl/Docs
#include <vartbldef.h>
#include <varcolwid.h>

mset command on

[delete 
   from les_cust_fld 
  where var_nam = 'vc_trlr_num'] catch(510, -1403)
RUN_DDL

[delete 
   from les_var_config 
  where var_nam = 'vc_trlr_num'] catch(510, -1403)
RUN_DDL

[delete 
   from les_var_inp
  where var_nam = 'vc_trlr_num'] catch(510, -1403)
RUN_DDL

[delete 
   from les_mls_cat 
  where mls_id = 'vc_trlr_num'] catch(510, -1403)
RUN_DDL

[ALTER_TABLE_DROP_COLUMN_BEGIN(trlract, vc_trlr_num) 
ALTER_TABLE_DROP_COLUMN_END] catch(ERR_CANNOT_DROP_COLUMN_NOT_FOUND)
RUN_DDL

[ALTER_TABLE_DROP_COLUMN_BEGIN(trlr, vc_trlr_num) 
ALTER_TABLE_DROP_COLUMN_END] catch(ERR_CANNOT_DROP_COLUMN_NOT_FOUND)
RUN_DDL

mset command off
