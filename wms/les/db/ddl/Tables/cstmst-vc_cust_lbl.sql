#use $DCSDIR/include
#include <dcsddl.h>
#include <dcscolwid.h>
#include <dcstbldef.h>

#use $MOCADIR/include
#include <sqlDataTypes.h>


ALTER_TABLE_ADD_COLUMN_BEGIN(cstmst, vc_cust_lbl) 
              STRING_TY(40)
ALTER_TABLE_ADD_COLUMN_END
RUN_DDL

ALTER_TABLE_ADD_COLUMN_BEGIN(cstmst, vc_num_copies) 
              FIXED_INT_TY(1)
ALTER_TABLE_ADD_COLUMN_END
RUN_DDL
