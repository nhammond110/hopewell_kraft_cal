#use $MOCADIR/include
#include <sqlDataTypes.h>
alter table pckwrk  add vc_from_rpl  number(1) default 0
                    constraint pckwrk_vc_from_rpl_ck check (vc_from_rpl in (1, 0))
RUN_SQL
