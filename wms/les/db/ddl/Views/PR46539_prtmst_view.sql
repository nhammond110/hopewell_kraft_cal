#use $LESDIR/include
#include <varddl.h>

#use $DCSDIR/include
#include <sqlDataTypes.h>
#include <dcscolwid.h>

mset command on

[
 DROP_VIEW(prtmst_view)
] catch(-204, -942, -3701)
RUN_DDL

mset command off

/*
 * This view will return everything from the prtmst
 * with the wh_id_tmpl renamed to wh_id.
 */

CREATE_VIEW(prtmst_view)
  select prtmst.*, 
         prtmst.wh_id_tmpl wh_id
    from prtmst

RUN_DDL
