#use $LESDIR/include
#include <varddl.h>
#include <sqlDataTypes.h>

mset command on

[
DROP_VIEW(ship_struct_view)
] catch(-204, -942, -3701)
RUN_DDL

mset command off

CREATE_VIEW(ship_struct_view)
    select h.*,
           stp.stop_nam,
           stp.stop_seq,
           stp.car_move_id,
           stp.adr_id,
           stp.stop_cmpl_flg,
           stp.stop_seal,
           cm.trlr_id,
           cm.tms_load_id,
           cm.trans_mode,
           tl.trlr_num,
           tl.trlr_stat,
           tl.trlr_typ,
           tl.trlr_size,
           tl.refrig_flg,
           tl.trlr_broker,
           tl.trlr_seal1,
           tl.trlr_seal2,
           tl.trlr_seal3,
           tl.trlr_seal4,
           tl.stoloc,
           tl.yard_loc,
           tl.yard_stat,
           tl.trlr_wgt,
           tl.driver_nam,
           tl.driver_lic_num,
           tl.close_dte,
           tl.pend_dte,
           tl.dispatch_dte,
           tl.autogen_flg,
           tl.turn_flg,
           tl.arrdte,
           tl.tractor_num,
           tl.trlr_cond,
           tl.trlr_cod,
           tl.hot_flg
      from shipment h
      left outer join stop stp
        on stp.stop_id = h.stop_id
      left outer join car_move cm
        on cm.car_move_id = stp.car_move_id
      left outer join trlr tl
        on tl.trlr_id = cm.trlr_id
       and tl.carcod = cm.carcod
RUN_DDL
