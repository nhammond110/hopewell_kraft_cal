[ select count(*) row_count from web_field where
    service = '@service@' and cust_lvl = @cust_lvl@ and web_fld_typ = '@web_fld_typ@' and var_nam = '@var_nam@' ] | if (@row_count > 0) {
       [ update web_field set
          service = '@service@'
,          cust_lvl = @cust_lvl@
,          web_fld_typ = '@web_fld_typ@'
,          var_nam = '@var_nam@'
,          srtseq = to_number('@srtseq@')
,          child_service = '@child_service@'
,          child_flds = '@child_flds@'
,          ena_flg = to_number('@ena_flg@')
,          grp_nam = '@grp_nam@'
,          dflt_flg = '@dflt_flg@'
,          dspl_only_flg = '@dspl_only_flg@'
,          totflg = '@totflg@'
             where  service = '@service@' and cust_lvl = @cust_lvl@ and web_fld_typ = '@web_fld_typ@' and var_nam = '@var_nam@' ] }
             else { [ insert into web_field
                      (service, cust_lvl, web_fld_typ, var_nam, srtseq, child_service, child_flds, ena_flg, grp_nam, dflt_flg, dspl_only_flg, totflg)
                      VALUES
                      ('@service@', @cust_lvl@, '@web_fld_typ@', '@var_nam@', to_number('@srtseq@'), '@child_service@', '@child_flds@', to_number('@ena_flg@'), '@grp_nam@', to_number('@dflt_flg@'), to_number('@dspl_only_flg@'), to_number('@totflg@')) ] }
