[ select count(*) row_count 
    from grd_view_def 
   where lvl_id = '@lvl_id@' 
     and appl_id = '@appl_id@' 
     and frm_id = '@frm_id@' 
     and addon_id = '@addon_id@' 
     and usr_id = '@usr_id@'
     and view_nam = '@view_nam@'
     and grd_var_nam = '@grd_var_nam@'
] 
| 
   if (@row_count > 0) 
   {
       [ update grd_view_def 
            set lvl_id = '@lvl_id@',          
                appl_id = '@appl_id@',          
                frm_id = '@frm_id@',          
                addon_id = '@addon_id@',          
	        usr_id = '@usr_id@',    
                view_nam = '@view_nam@',
                grd_var_nam = '@grd_var_nam@',
                sys_flg = @sys_flg@,
                grp_flg = @grp_flg@,
                grp_ttl_formula = '@grp_ttl_formula@',
                grp_nam = '@grp_nam@'
	      where lvl_id = '@lvl_id@' 
            and appl_id = '@appl_id@' 
            and frm_id = '@frm_id@' 
            and addon_id = '@addon_id@' 
            and usr_id = '@usr_id@' 
            and view_nam = '@view_nam@'
            and grd_var_nam = '@grd_var_nam@'
	    ] 
    }
    else 
    { 
        [insert into 
                grd_view_def(lvl_id,         
                        appl_id,          
                        frm_id,          
                        addon_id,          
	                    usr_id,    
                        view_nam,
                        grd_var_nam,
             		sys_flg,
                        grp_flg,
                        grp_ttl_formula,
                        grp_nam)
                VALUES('@lvl_id@',          
                        '@appl_id@',          
                        '@frm_id@',          
                        '@addon_id@',          
	                '@usr_id@',    
                        '@view_nam@',
                        '@grd_var_nam@',
                         @sys_flg@,
                         @grp_flg@,
                        '@grp_ttl_formula@',
                        '@grp_nam@') 
	] 
    }
|
    create cache time stamp
      where obj_nam = "MCSGrdViewDef"
        and idx_val = "LES"
