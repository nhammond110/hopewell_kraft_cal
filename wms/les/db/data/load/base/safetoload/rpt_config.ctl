[ select count(*) row_count 
    from rpt_config 
   where rpt_id = '@rpt_id@' ]  
| 
if (@row_count > 0) 
{ 
    [ update rpt_config 
         set rpt_id = '@rpt_id@',
             prod_id = '@prod_id@',
             rpt_layout_file = '@rpt_layout_file@',
             def_printer = '@def_printer@',
             rpt_typ = '@rpt_typ@',
             keep_days = '@keep_days@',
             ena_ems_flg = '@ena_ems_flg@',
             ems_evt_nam = '@ems_evt_nam@',
             func_area = '@func_area@',
             grp_nam = '@grp_nam@'
         where  rpt_id = '@rpt_id@' ] 
}  
else 
{ 
    [ insert into rpt_config 
        (rpt_id, 
         prod_id, 
         rpt_layout_file, 
         def_printer, 
         rpt_typ,
         keep_days,
         ena_ems_flg,
         ems_evt_nam,
         func_area,
         grp_nam) 
      values 
        ('@rpt_id@', 
         '@prod_id@', 
         '@rpt_layout_file@',  
         '@def_printer@', 
         '@rpt_typ@',
         '@keep_days@',
         '@ena_ems_flg@',
         '@ems_evt_nam@',
         '@func_area@',
         '@grp_nam@') ] 
} 
