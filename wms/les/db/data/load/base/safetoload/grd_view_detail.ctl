[ select count(*) row_count 
    from grd_view_detail 
   where lvl_id = '@lvl_id@' 
     and appl_id = '@appl_id@' 
     and frm_id = '@frm_id@' 
     and addon_id = '@addon_id@' 
     and usr_id = '@usr_id@'
     and view_nam = '@view_nam@'
     and grd_var_nam = '@grd_var_nam@'
     and view_type = '@view_type@'
     and view_fld_nam = '@view_fld_nam@'
] 
| 
   if (@row_count > 0) 
   {
       [ update grd_view_detail 
            set lvl_id = '@lvl_id@',          
                appl_id = '@appl_id@',          
                frm_id = '@frm_id@',          
                addon_id = '@addon_id@',          
	        usr_id = '@usr_id@',    
                view_nam = '@view_nam@',
                grd_var_nam = '@grd_var_nam@',
		view_type = '@view_type@',
		view_fld_nam = '@view_fld_nam@',
                view_fld_width = @view_fld_width@,
    		view_fld_seq  = @view_fld_seq@,
    		view_fld_vis = @view_fld_vis@, 
    		view_fld_sort_ord = @view_fld_sort_ord@,
    		view_fld_sort_typ = @view_fld_sort_typ@,
    		view_grp_ord = @view_grp_ord@,
    		view_formula = '@view_formula@',
                grp_nam = '@grp_nam@'
	      where lvl_id = '@lvl_id@' 
            and appl_id = '@appl_id@' 
            and frm_id = '@frm_id@' 
            and addon_id = '@addon_id@' 
            and usr_id = '@usr_id@' 
            and view_nam = '@view_nam@'
            and grd_var_nam = '@grd_var_nam@'
	    and view_type = '@view_type@'
	    and view_fld_nam = '@view_fld_nam@'
	    ] 
    }
    else 
    { 
        [insert into 
                grd_view_detail(lvl_id,         
                        appl_id,          
                        frm_id,          
                        addon_id,          
	                usr_id,    
                        view_nam,
                        grd_var_nam,
			view_type,
			view_fld_nam,
			view_fld_width,
			view_fld_seq,
			view_fld_vis,
			view_fld_sort_ord,
			view_fld_sort_typ,
			view_grp_ord,
			view_formula,
                        grp_nam)

                VALUES('@lvl_id@',          
                        '@appl_id@',          
                        '@frm_id@',          
                        '@addon_id@',          
	                '@usr_id@',    
                        '@view_nam@',
                        '@grd_var_nam@',
			'@view_type@',
			'@view_fld_nam@',
			 @view_fld_width@,
			 @view_fld_seq@,
			 @view_fld_vis@,
			 @view_fld_sort_ord@,
			 @view_fld_sort_typ@,
			 @view_grp_ord@,
			'@view_formula@',
                        '@grp_nam@') 
	] 
    }
|
    create cache time stamp
      where obj_nam = "MCSGrdViewDetails"
        and idx_val = "LES"
