[ select count(*) row_count
    from trnsp_mode 
   where trnsp_mode = 'M' ]
|
if (@row_count = 0)
{
    [ insert into trnsp_mode 
         (trnsp_mode,
          sml_pkg_flg,
          dir_flg,
          tst_trans_serv_id,
	  palletctl_flg,
          u_version,
          ins_dt,
          last_upd_dt,
          ins_user_id,
          last_upd_user_id) 
        values 
          ('M',
           0,
           0,
           null,
	   0,
           1,
           sysdate,
           sysdate,
           'LOAD',
           'LOAD') ]
}
