[ select count(*) row_count 
    from exitpnt 
   where exitpnt_typ = '@exitpnt_typ@' 
     and exitpnt = '@exitpnt@']
   | 
    if (@row_count = 0)     
    { 
        [ insert into exitpnt 
              (exitpnt_typ, exitpnt) 
            values 
              ('@exitpnt_typ@', '@exitpnt@') ] 
    } 
