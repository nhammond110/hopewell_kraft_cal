[ select count(*) row_count 
    from devmst 
   where wh_id = '@wh_id@'
     and devcod = '@devcod@' ] | 

    if (@row_count > 0) 
    { 
        [ update devmst 
             set wh_id = '@wh_id@',
                 devcod = '@devcod@',
                 devcls = '@devcls@',
                 devnam = '@devnam@',
                 lbl_prtadr = '@lbl_prtadr@'
           where wh_id = '@wh_id@'
             and devcod = '@devcod@' ] 
    }  
    else 
    { 
        [ insert into devmst 
              (wh_id, devcod, devcls, devnam, lbl_prtadr) 
            values 
              ('@wh_id@', '@devcod@', '@devcls@', '@devnam@', '@lbl_prtadr@') ]
    }
