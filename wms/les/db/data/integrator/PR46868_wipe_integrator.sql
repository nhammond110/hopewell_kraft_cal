mset command on

    /* clear the result ifd */
    sl_wipe ifd_def
      where ifd_id = 'VC_ORDER_DCS_IFD'
        and ifd_ver = 'WMD_2007.2'
            catch(-1403,510)
			
/

    sl_wipe ifd_def
      where ifd_id = 'VC_ORDER_DCS_DEL_IFD'
        and ifd_ver = 'DCS V5.0'
            catch(-1403,510)

/

    sl_wipe ifd_def
      where ifd_id = 'VC_DEASSIGN_SHIPMENT'
        and ifd_ver = 'V1.0'
            catch(-1403,510)
/

    sl_wipe ifd_def
      where ifd_id = 'VC_MNT_TRAFFIC_PLAN ADD/CHANGE'
        and ifd_ver = 'V2005.2'
            catch(-1403,510)

/

    /* Now we wipe the event outputs */
    sl_wipe eo_def
      where eo_id = 'VC_ORDER_TRAN'
        and eo_ver = 'WMD_2007.2' 
            catch(-1403,510)
            
/

    sl_wipe eo_def
      where eo_id = 'VC_TRAFFIC_PLAN_TRAN'
        and eo_ver = 'V2005.2' 
            catch(-1403,510)
            
/
            
    /* Next we wipe the event */
    sl_wipe evt_def 
      where evt_id = 'VC_ORDER_MANAGEMENT' 
            catch(-1403,510)
			
/
            
    sl_wipe evt_def 
      where evt_id = 'VC_MNT_TRAFFIC_PLAN' 
            catch(-1403,510)
			
/

    /* Now we wipe the IFDs */
    sl_wipe ifd_def
      where ifd_id = 'VC_ORDER_INB_IFD'
        and ifd_ver = '2008.1'
            catch(-1403,510)

/
        
	sl_wipe ifd_def 
	  where ifd_id = 'VC_TRAFFIC_PLAN_INB_IFD'
		and ifd_ver = 'V2005.2'
		catch(-1403,510)

mset command off

