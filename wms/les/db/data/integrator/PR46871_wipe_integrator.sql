mset command on

    /* First we wipe the Result IFD Segs */
        sl_wipe ifd_seg 
          where ifd_id = 'VC_ASN_OUB_IFD'
            and ifd_ver = 'SAP V1.0'
            and ifd_seg_id = 'VC_ASN_LOAD_DETAIL_SEG'
            catch(-1403,510)
/
        
        sl_wipe ifd_seg 
	    where ifd_id = 'VC_ASN_OUB_IFD'
	      and ifd_ver = 'SAP V1.0'
	      and ifd_seg_id = 'VC_ASN_SHIPMENT_ORDLIN_SEG'
            catch(-1403,510)
/

       sl_wipe ifd_seg 
            where ifd_id = 'VC_ASN_OUB_IFD'
              and ifd_ver = 'SAP V1.0'
              and ifd_seg_id = 'VC_ASN_SHIPMENT_ORDER_SEG'
            catch(-1403,510)
/
            
       sl_wipe ifd_seg 
            where ifd_id = 'VC_ASN_OUB_IFD'
               and ifd_ver = 'SAP V1.0'
               and ifd_seg_id = 'VC_ASN_SHIPMENT_SEG'
            catch(-1403,510)
/
            
      sl_wipe ifd_seg 
             where ifd_id = 'VC_ASN_OUB_IFD'
               and ifd_ver = 'SAP V1.0'
               and ifd_seg_id = 'VC_ASN_STOP_SEG'
            catch(-1403,510)
/
      sl_wipe ifd_seg 
             where ifd_id = 'VC_ASN_OUB_IFD'
               and ifd_ver = 'SAP V1.0'
               and ifd_seg_id = 'VC_ASN_MOVE_SEG'
            catch(-1403,510)
/
      sl_wipe ifd_seg 
	     where ifd_id = 'VC_ASN_OUB_IFD'
	       and ifd_ver = 'SAP V1.0'
	       and ifd_seg_id = 'VC_ASN_CTRL_SEG'
            catch(-1403,510)
/
      sl_wipe eo_def 
	     where eo_id = 'VC_ASN_OUB'
	       and eo_ver = 'SAP V1.0'
	    catch(-1403,510)
/

    /* Now we wipe the event outputs */
    sl_wipe eo_def
      where eo_id = 'VC_ASN_OUB'
        and eo_ver = 'SAP V1.0' 
            catch(-1403,510)
/

    /* Now we wipe the event outputs */
    sl_wipe eo_def
      where eo_id = 'VC_ASN_OUB'
        and eo_ver = 'SAP V1.0' 
            catch(-1403,510)
            
mset command off

