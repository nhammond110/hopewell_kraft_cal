mset command on

    /* First we wipe the PART Result IFD Segs */
        sl_wipe ifd_seg 
          where ifd_id = 'MNT-ITM-OB'
            and ifd_ver = 'WMD_2005.2'
            and ifd_seg_id = 'PART'
            catch(-1403,510)
/
            
    /* First we wipe the event */
    sl_wipe evt_def 
      where evt_id = 'MNT-ITM-FOOT' 
            catch(-1403,510)
/

    /* Now we wipe the event outputs */
    sl_wipe eo_def
      where eo_id = 'MNT-ITM-FOOT'
        and eo_ver = 'WMD_2005.2' 
            catch(-1403,510)
/

    /* Now we wipe the IFDs */
    sl_wipe ifd_def
      where ifd_id = 'ITEM_FOOTPRINT_INB_IFD'
        and ifd_ver = '2008.1'
            catch(-1403,510)       
/
    /* Now we wipe the IFDs */
    sl_wipe ifd_def
      where ifd_id = 'ITEM_FOOTPRINT_INB_IFD'
        and ifd_ver = '2008.1'
            catch(-1403,510)

mset command off

