mset command on

    /* First we wipe the Result IFD Segs */
        sl_wipe ifd_seg 
          where ifd_id = 'VC_INVENTORY_ADJUST'
            and ifd_ver = 'SAP V1.0'
            and ifd_seg_id = 'INVENTORY_ADJUST_IFD'
            catch(-1403,510)
/
        
        sl_wipe ifd_seg 
	    where ifd_id = 'VC_INVENTORY_STATUS_CHANGE_IFD'
	      and ifd_ver = 'SAP V1.0'
	      and ifd_seg_id = 'VC_INVENTORY_STATUS_CHANGE_SEG'
            catch(-1403,510)
/
	
	sl_wipe ifd_seg 
	    where ifd_id = 'VC_INV_CODE_DATE_CHANGE_IFD'
	      and ifd_ver = 'SAP V2010'
	      and ifd_seg_id = 'VC_INV_CODE_DATE_CHG_RIFD'
	    catch(-1403,510)
	    
/
	sl_wipe ifd_seg 
	    where ifd_id = 'VC_INV_LOT_CHANGE_IFD'
	      and ifd_ver = 'SAP V2010'
	      and ifd_seg_id = 'VC_INV_LOT_CHG_RIFD'
	    catch(-1403,510)
	    
/
	sl_wipe ifd_seg 
	    where ifd_id = 'VC_INV_LOT_CHANGE_IFD'
	      and ifd_ver = 'SAP V2010'
	      and ifd_seg_id = 'VC_INV_LOT_CHG_RIFD'
	    catch(-1403,510)
            
mset command off

