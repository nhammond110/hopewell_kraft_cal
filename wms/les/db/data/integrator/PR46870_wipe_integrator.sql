mset command on

    /* First we wipe the Result IFD Segs */
        sl_wipe ifd_seg 
          where ifd_id = 'VC_INVENTORY_RECEIPT_IFD'
            and ifd_ver = 'SAP V1.0'
            and ifd_seg_id = 'VC_INVENTORY_RECEIPT_SEG'
            catch(-1403,510)
/
        
        sl_wipe ifd_seg 
	    where ifd_id = 'VC_INVENTORY_RECEIPT_IFD'
	      and ifd_ver = 'SAP V1.0'
	      and ifd_seg_id = 'VC_CTRL_SEG'
            catch(-1403,510)
/

    /* Now we wipe the event outputs */
       sl_wipe eo_def
          where eo_id = 'VC_INV-RCV'
             and eo_ver = 'SAP V1.0' 
            catch(-1403,510)
/
    /* Now we wipe the event outputs */
       sl_wipe eo_def
          where eo_id = 'VC_INV-RCV'
             and eo_ver = 'SAP V1.0' 
            catch(-1403,510)
            
mset command off

