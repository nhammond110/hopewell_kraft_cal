mset command on

    /* First we wipe the Result IFD Segs */
        sl_wipe ifd_seg 
          where ifd_id = 'VC_ASN_RCPT_DCS_IFD'
            and ifd_ver = 'SAP V2010'
            and ifd_seg_id = 'VC_ASN_RCPT_ASN_IFD'
            catch(-1403,510)
/
        
        sl_wipe ifd_seg 
          where ifd_id = 'VC_ASN_RCPT_DCS_IFD'
            and ifd_ver = 'SAP V2010'
            and ifd_seg_id = 'VC_ASN_RCPT_LINE_IFD'
            catch(-1403,510)
/

       sl_wipe ifd_seg 
          where ifd_id = 'VC_ASN_RCPT_DCS_IFD'
            and ifd_ver = 'SAP V2010'
            and ifd_seg_id = 'VC_ASN_RCPT_INVOICE_IFD'
            catch(-1403,510)
/
            
       sl_wipe ifd_seg 
	   where ifd_id = 'VC_ASN_RCPT_DCS_IFD'
            and ifd_ver = 'SAP V2010'
	      and ifd_seg_id = 'VC_ASN_RCPT_TRUCK_IFD'
	   catch(-1403,510)
/
            
    /* Next we wipe the event */
    sl_wipe evt_def 
      where evt_id = 'VC_ASN_RCPT_MANAGEMENT' 
            catch(-1403,510)
/

    /* Now we wipe the event outputs */
    sl_wipe eo_def
      where eo_id = 'VC_ASN_RCPT_TRAN'
        and eo_ver = 'SAP V2010' 
            catch(-1403,510)
/

    /* Now we wipe the IFDs */
    sl_wipe ifd_def
      where ifd_id = 'VC_ASN_RCPT_INB_IFD'
        and ifd_ver = 'SAP V2010'
            catch(-1403,510)      
/

    /* Now we wipe the IFDs */
    sl_wipe ifd_def
      where ifd_id = 'VC_ASN_RCPT_INB_IFD'
        and ifd_ver = 'SAP V2010'
            catch(-1403,510)

mset command off

