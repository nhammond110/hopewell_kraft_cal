mset command on

    /* First we wipe the Result IFD Segs */
        sl_wipe ifd_seg 
          where ifd_id = 'VC_ORDER_DCS_IFD'
            and ifd_ver = 'WMD_2007.2'
            and ifd_seg_id = 'ORDER_SEG'
            catch(-1403,510)
/
            
    /* First we wipe the event */
    sl_wipe evt_def 
      where evt_id = 'VC_ORDER_MANAGEMENT' 
            catch(-1403,510)
/

    /* Now we wipe the event outputs */
    sl_wipe eo_def
      where eo_id = 'VC_ORDER_TRAN'
        and eo_ver = 'WMD_2007.2' 
            catch(-1403,510)

mset command off

